// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: l2proto802.3.h 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - Layer 2 802.3 class
// George F. Riley.  Georgia Tech, Spring 2002

#ifndef __l2proto_802_3_h
#define __l2proto_802_3_h

#include "l2proto.h"
#include "tfstream.h"
#include "macaddr.h"

class LinkEvent;

//Doc:ClassXRef
class L2Proto802_3 : public L2Proto {
//Doc:Class Class {\tt L2Proto802\_3} defines the 802.3 layer 2 protocol. 
//Doc:Class This is derived from class {\tt L2Proto}. It besically implements
//Doc:Class the Medium Access Control portion of layer 2. 
public:
  typedef enum { TYPE, PROTO, LENGTH, SRC, DST, UID } L2Trace_t;  // Tracing options
  //Doc:Method
  L2Proto802_3(); // Constructor
  //Doc:Desc The Default Constructor

  //Doc:Method
  virtual   void BuildDataPDU(MACAddr, MACAddr, Packet*);
    //Doc:Desc Builds the layer2 pdu and appends it to the specified packet.
    //Doc:Arg1 Source MAC address
    //Doc:Arg2 Destination MAC address
    //Doc:Arg3 Packet pointer to append this pdu to.

  //Doc:Method
  void DataRequest(Packet*);
  //Doc:Desc This is the real member function is called by the higher layer 
  //Doc:Desc requesting a transfer of  data to a peer for 802.3
  //Doc:Arg1 The packet to be sent
  
  //Doc:Method
  void DataIndication(Packet*, LinkEvent* = 0);    // from lower layer
  //Doc:Desc This method is called when the direction of protocol stack
  //Doc:Desc traversal is upwards, i.e, a packet has just been received
  //Doc:Desc at the underlying link
  //Doc:Arg1 The LinkEvent with the received packet (may be nil)
  //Doc:Arg1  If non-nil, the DataIndication must delete or re-use the event
  //Doc:Arg2 The received packet

  //Doc:Method
  virtual   bool Busy();                     // True if proto/link is busy
    //Doc:Desc Test if the protocol (or link) is busy processing a packet.
    //Doc:Return True if currently busy.

  //Doc:Method
   L2Proto802_3*  Copy() const { return new L2Proto802_3 (*this); }
  //Doc:Desc This method makes a copy of the protocol object and returns the
  //Doc:Desc pointer to the copy
  //Doc:Return pointer to the copy

  //Doc:Method
   bool IsWireless() const { return false;}
    //Doc:Desc Query if this protocol needs a wireless interface.
    //Doc:Return Always returns false

   void Bootstrap() {}
   
public:
  Word_t       length;

};

//Doc:ClassXRef
class L2802_3Header : public L2PDU {
//Doc:Class Class {\ttL2802_3Header } defines a PDU (Protocol Data Unit) 
//Doc:Class structure that defines the 802.3 header for the packet
public :
  //Doc:Method
  L2802_3Header() : src(MACAddr::NONE), dst(MACAddr::NONE), length(0) { };
  //Doc:Desc This method is the default constructor for the header. It fills 
  //Doc:Desc all the fields with zeros.
  //Doc:Method
  L2802_3Header(MACAddr s, MACAddr d, Word_t len)
    : src(s), dst(d), length(len) { };
  //Doc:Desc This method takes in three arguements namely, source MAC address
  //Doc:Desc destination MAC address and the layer 3 protocol type and creates
  //Doc:Desc a header PDU object from the same.
  //Doc:Arg1 Source MAC address
  //Doc:Arg2 Destination MAC Address
  //Doc:Arg3 Layer 3 protocol type

  //Doc:Method
  L2802_3Header(char*, Size_t&, Packet*); // Construct from serialized buffer
  //Doc:Desc This method creates an L2 802.3 Header from a serialized character
  //Doc:Desc buffer and pushes it into a packet. This is useful only while
  //Doc:Desc using distributed simulations over a network of workstations.
  //Doc:Arg1 the character buffer pointer
  //Doc:Arg2 the size of the buffer
  //Doc:Arg3 the pointer to the packet 

  //Doc:Method
  Size_t  Size() const { return txSize;}
  //Doc:Desc This method returns the size of the layer 2 header
  //Doc:Return the size of the header

  //Doc:Method
  PDU*    Copy() const { return new L2802_3Header(*this);}
  //Doc:Desc This method makes a copy of itself and returns the copy
  //Doc:Return A PDU pointer to a copy of self

  //Doc:Method
  void    Trace(Tfstream& tos, Bitmap_t b,   // Trace the contents of this pdu 
                Packet* = nil, const char* = nil); 
  //Doc:Desc This method is used to trace the protocol data to a file (whose
  //Doc:Desc stream descriptor is tos) according to the level of detail 
  //Doc:Desc specified byt the bitmap
  //Doc:Arg1 the file I/O stream descriptor
  //Doc:Arg2 the bitmap
  //Doc:Arg3 Packet the contains this PDU (optional)
  //Doc:Arg4 Extra text information for trace file (optional)

  // Serialization
  //Doc:Method
  Size_t SSize();                   // Size needed for serialization
  //Doc:Method
  char*  Serialize(char*, Size_t&); // Serialize to a buffer
  //Doc:Desc This method serializes the header into a character buffer
  //Doc:Arg1 the pointer to the character buffer
  //Doc:Arg2 the current size of the buffer

  //Doc:Method
  char*  Construct(char*, Size_t&); // Construct from buffer
  //Doc:Desc This method is a helper method for the constructor. It actually 
  //Doc:Desc does the job of constructing a L2 802.3 header from the serialized
  //Doc:Desc character buffer obtained.
  //Doc:Arg1 the pointer to the character buffer
  //Doc:Arg2 the current size of the buffer.

  // Inherited from L2PDU
  //Doc:Method
  MACAddr GetSrcMac() { return src;}
    //Doc:Desc Return the source MAC address.
    //Doc:Return source MAC Address
  
  //Doc:Method
  MACAddr GetDstMac() { return dst;}
    //Doc:Desc Return the destination MAC address.
    //Doc:Return destination MAC Address
  
public:
  MACAddr   src;
  MACAddr   dst;
  Word_t    length;
public:
  static Size_t txSize;  // Sie of header when transmitting on link
  static void TxSize(Size_t txs) { txSize = txs; }
  
};

#endif
