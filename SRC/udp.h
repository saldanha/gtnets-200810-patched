// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: udp.h 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - UDP Base Class
// George F. Riley.  Georgia Tech, Spring 2002

// Base class for all UDP variants

#ifndef __udp_h__
#define __udp_h__

#include <deque>

#include "pdu.h"
#include "l4protocol.h"
#include "datapdu.h"
#include "notifier.h"

//Doc:ClassXRef
class UDPHeader : public L4PDU {
public :
  UDPHeader();
  UDPHeader(PortId_t, PortId_t, Word_t, Word_t);
  UDPHeader(PortId_t, PortId_t, Word_t, Word_t, FId_t);
  UDPHeader(char* b, Size_t& sz, Packet* p);
  UDPHeader(const UDPHeader&);
  ~UDPHeader();
  Size_t  Size() const;
  PDU*    Copy() const;
  Proto_t Proto() { return 17;}        // UDP is protocol 17
  // Serialization
  Size_t  SSize();                     // Size needed for serialization
  char*   Serialize(char*, Size_t&);   // Serialize to a buffer
  char*   Construct(char*, Size_t&);   // Construct from buffer
  void    Trace(Tfstream&, Bitmap_t, // Trace the contents of this pdu
                Packet* = nil, const char* = nil);
public:
  PortId_t sourcePort;
  PortId_t destPort;
  Word_t   messageLength;
  Word_t   checksum;
  FId_t    fid;           // Flow id's are not part of UDP, but useful
  Seq_t    seq;           // Sequences are also not part of UDP, but useful
};

// Typedefs for the pending data queue
//Doc:ClassXRef
class UDPPending {
public:
  UDPPending();
  UDPPending(Data*, IPAddr_t, PortId_t) ;
  UDPPending(const UDPPending&);   // Copy constructor
  ~UDPPending();                   // Destructor
public:
  Data*    data;
  IPAddr_t ip;
  PortId_t port;
  Size_t   sent;   // Sent so far
};
typedef std::deque<UDPPending> Pending_t;

//Doc:ClassXRef
class UDP : public L4Protocol, public NotifyHandler {
  //Doc:Class Class {\tt UDP} defines a model of the User Datagram Protocol.
  //Doc:Class {\tt UDP} inherits from {\tt L4Protocol} and implements most of
  //Doc:Class the methods defined there.
public:
  typedef enum { ProtoNumber =  17 } UDPProto_t; // UDP Layer4 Protocol number
  typedef enum { SOURCE_PORT, DEST_PORT, MSG_LENGTH,
                 CHECKSUM, FID, SEQ } UDPTrace_t; // Tracing options
  // Constructors
  //Doc:Method
  UDP();
    //Doc:Desc Default constructor, no arguments.

  //Doc:Method
  UDP(Node* n);
    //Doc:Desc Constructor, specify a node to attached to.
    //Doc:Arg1 Node this {\tt UPD} protocol is attached to.

  //Doc:Method
  UDP(const UDP&);
    //Doc:Desc Copy constructor.
    //Doc:Arg1 {\tt UDP} object to copy.

  // Destructor
  virtual ~UDP(); 
public:
  // Notifier Methods
  void Notify(void*);

  // L4Protocol methods
  // Upcall From lower layer
  void DataIndication(Node*, Packet*, IPAddr_t, Interface*); 
  NCount_t Send(Size_t);                         // Send empty data
  NCount_t Send(char*, Size_t);                  // Send with data
  NCount_t Send(Data&);                          // Send with data
  NCount_t SendTo(Size_t, IPAddr_t, PortId_t);   // Send to specified target
  NCount_t SendTo(char*, Size_t, IPAddr_t, PortId_t); // Send w/data, to target
  NCount_t SendTo(Data&, IPAddr_t, PortId_t);    // Send w/ data
  Proto_t  Proto() const { return 17;}           // Return the layer 4 protoco
  bool     Connect(IPAddr_t, PortId_t);          // Connect to remote host
  bool     Close();                              // Close connection
  L4Protocol* Copy() const;                      // Create a copy of protocol
  Proto_t  ProtocolNumber();                     // Return UDP protocol number
  bool     IsTCP();                              // True if TCP
  //Doc:Method
  void     PacketSize(Size_t);                   // Set packet size
    //Doc:Desc Set the packet size for this {\tt UDP} endpoint.
    //Doc:Arg1 Packet size (bytes).

  //Doc:Method
  Size_t   PacketSize() const { return pktSize;} // Get packet size
    //Doc:Return Query packet size for this {\tt UDP} endpoint.

private:
  NCount_t Send(Packet*,IPAddr_t,PortId_t);      // Common send code
  void     SendPending();                        // Send pending data
private:
  Size_t    pktSize;                             // Size of packets
  Seq_t     seq;                                 // Seq number of next pkt
protected:
  Pending_t pendingData;                         // Data to be sent
public:
  // Static methods/members
  //Doc:Method
  static void   DefaultPacketSize(Size_t);       // Set the default pkt size
    //Doc:Desc Set default packet size for all {\tt UDP} protocol objects.
    //Doc:Arg1 Default packet size (bytes).  Default size is 512 bytes unless
    //Doc:Arg1 otherwise specified.

  static Size_t defaultPacketSize;               // Global default
  static Count_t totalUDP;                       // Debug
  static Count_t totalUDPDel;                    // Debug

};



#endif


