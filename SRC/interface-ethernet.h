// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: interface-ethernet.h 450 2005-11-30 20:35:18Z mofta7 $



// Georgia Tech Network Simulator - InterfaceEthernet class
// Mohamed Abd Elhafez.  Georgia Tech, Spring 2004


// Define class to model network link interfaces
// Implements the Layer 2 protocols

#ifndef __interface_ethernet_h__
#define __interface_ethernet_h__

#include "common-defs.h"
#include "simulator.h"
#include "protocol.h"
#include "packet.h"
#include "handler.h"
#include "ipaddr.h"
#include "notifier.h"
#include "macaddr.h"
#include "node.h"
#include "link.h"
#include "link-real.h"
#include "eventcq.h"
#include "interface.h"
#include "interface-real.h"
#include "ethernet.h"

#define INITIAL_BACKOFF 1           // Initial value for the max contention window
#define SLOT_TIME 512               // Slot time in bit times
#define BACKOFF_LIMIT 1024          // maximum allowable contention window
#define ATTEMPT_LIMIT 16            // number of retransmit before giving up
#define JAM_TIME 32                 // Jam time in bit times
#define INTER_FRAME_GAP 96          // Slot time in bit times

// Define the event subclass for link events
//Doc:ClassXRef
class EthernetEvent : public LinkEvent {
public:
typedef enum { 
  PACKET_RX,     // Packet receive event
  PACKET_TX,     // Packet transmit completed succesfuly 
  NOTIFY,        // Notify clients of available buffer space
  FBR,           // First Bit Received   
  CLR,           // Packet transmit aborted, link free
  RETRANSMIT,      // Start packet transmit
  UPDATE_NODES,  // Updae Node List
  CHAN_ACQ       // Channel acquired event 
} EthernetEvent_t;
public:
  EthernetEvent() 
    :  mac(MACAddr::NONE), type(0), size(0)
    { }
  EthernetEvent(Event_t ev) 
    : LinkEvent(ev), mac(MACAddr::NONE), type(0), size(0)
    { }
  EthernetEvent(Event_t ev, Packet* pkt) 
    : LinkEvent(ev, pkt), mac(MACAddr::NONE), type(0), size(pkt->Size())
    { }
 EthernetEvent(Event_t ev,  Size_t s) 
    : LinkEvent(ev),  mac(MACAddr::NONE), type(0), size(s)
    { }
 EthernetEvent(Event_t ev, const MACAddr& dst, Size_t s) 
    : LinkEvent(ev),  mac(dst), type(0), size(s)
    { }
  EthernetEvent(Event_t ev, Packet* pkt, const MACAddr& dst, int t)
    : LinkEvent(ev, pkt),  mac(dst), type(t)
    { }

  virtual ~EthernetEvent() { }

public:
  MACAddr mac;         // Receiver Mac address
  int type;            // Type for sending the packet
  Size_t size;         // The size of the packet
  Interface* sender;   // The sender interface, used to notify the sender that it has acquired the chanell succesfuly 
};

//Doc:ClassXRef
class NodeIfTime {
public:
  NodeIfTime(Node* n, Interface* i, Time_t t)
    : node(n), iface(i), time(t) { }
public:
  Node*      node;
  Interface* iface;
  Time_t   time;
};

typedef std::vector<NodeIfTime> NodeTimeVec_t; // Neighbors,Iface w/time
typedef std::vector<LinkEvent*>  LinkEventVec_t; 

//Doc:ClassXRef
class InterfaceEthernet : public InterfaceReal
{
  //Doc:Class InterfaceEthernet is an interface that handles the collision 
  //Doc:Class detection algorithm in the 802.3 standrad
  
 public:
  //Doc:Method
  InterfaceEthernet(const L2Proto& l2 = L2Proto802_3(),
		    IPAddr_t i   = IPADDR_NONE,
		    Mask_t   m   = MASK_ALL,
		    MACAddr  mac = MACAddr::NONE,
		    bool bootstrap = false);
  //Doc:Desc Constructs a new ethernet interface object.
  //Doc:Arg1 The layer 2 protocol type to associate with this interface.
  //Doc:Arg2 The \IPA\ to assign to this interface.
  //Doc:Arg3 The address mask to assign to this interface.
  //Doc:Arg4 The {\tt MAC} address to assign to this interface.
  
  virtual ~InterfaceEthernet();
  
  virtual bool IsEthernet() const { return true;}              // True if ethernet interface

  // Handler
  virtual void Handle(Event*, Time_t);
  
  //Doc:Method
  bool Send(Packet*, const MACAddr&, int); // Enque and transmit
  //Doc:Desc Send a packet on this interface.
  //Doc:Arg1 Packet to send.
  //Doc:Arg2 MAC address of destination
  //Doc:Arg3 LLC/SNAP type
  //Doc:Return True if packet sent or enqued, false if dropped.
  
   
  //Doc:Method
  virtual void RegisterRxEvent(LinkEvent*); // Register the last rx event issued by this interface      
  //Doc:Desc Keep a reference of the last Rx event made by a packet from this interfcae.
  //Doc:Arg1 Last PACKET_RX event 
  
  //Doc:Method
  virtual void     AddRxEvent(LinkEvent*);   // add to the list of rx events , used in case of broadcast
  //Doc:Desc Add to the list of rx events, This method is used in case the last packet was broadcast.
  //Doc:Arg1 Last PACKET_RX event 

  virtual void Notify(void* v);


  //Doc:Method
  //virtual void     LasPktIsBcast(bool);      // true of last packet is broad cast
  //Doc:Desc Set the bCast flag indicating wether the last packet was broadcast or not


//Doc:Method
  bool Retransmit(Packet* p);
  //Doc:Desc Tries to retransmit the packet on the link
  //Doc:Arg1 The packet to be retransmitted

//Doc:Method
  bool SenseChannel();
  //Doc:Desc Tries to retransmit the packet on the link
  //Doc:Arg1 The packet to be retransmitted

 private:
  
  //Doc:Method
  Time_t  TimeDelay(Node*);
  //Doc:Desc Calculate time between this node and specified peer node.
  //Doc:Arg1 Peer node pointer.
  //Doc:Return Time (Time_t).
  
  //Doc:Method
  void NeighbourList(NodeTimeVec_t& ntv);
  //Doc:Desc Get a list of all neighbor nodes on the same ethernet link
  //Doc:Arg1 vector will be populated with nodes and their delay times
  
  //Doc:Method
  void HandleFBR(EthernetEvent* e, Time_t t);
  //Doc:Desc Handles the first bit received event
  //Doc:Arg1 The Ethernet event
  //Doc:Arg1 Time of the event
  
 //Doc:Method
  Time_t  SlotTime();
  //Doc:Desc Calculate the slot time for this link.
  //Doc:Return slot Time (Time_t).


 private: 
  Time_t              busyendtime;     // Time when the link will be free again
  Time_t              holdtime;        // Time to hold back all packets when collision occurs
  Time_t              maxbackoff;      // Maximum backoff timer
  Time_t              backofftimer;    // Backoff timer
  Time_t              maxwaittime;     // maximum delay
  Count_t             busycount;       // How many stations are transmitting
  Packet*             lastpacketsent;  // last packet sent by this interface 
 
  LinkEvent*          lastrxevent;     // Last rx event scheduled by the link from this interface
  LinkEvent*          lastchanacq;     // Last channel acquired event scheduled by this interface
  LinkEvent*          lasttransmit;    // Last retransmit event scheduled by this interface
  NodeTimeVec_t       nodeTimes;       // vector containing neighbour nodes and their time delay
  Time_t              tx_finishtime;   // Transmit finish time
  bool                bCast;           // True if the last packet sent was a broadcast
  bool                collision;
  LinkEventVec_t      rxEvents;        // A list of rx event pointers used when broadcasting
  Time_t              slottime;        // the slot time in seconds
  Time_t              rtime;           // time to retransmit
  Uniform*            generator;       // random number generator
  
};

#endif
