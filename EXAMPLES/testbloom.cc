// Test the Bloom Filter
// George F. Riley, Georgia Tech, Spring 2005

#include <stdlib.h>
#include <iostream>
#include <set>

#include "simulator.h"
#include "rng.h"
#include "bloom-filter.h"

using namespace std;

class RandomHash : public HashGenerator {
public:
  RandomHash(Count_t); // Size of value to be hashed, bits
  virtual ~RandomHash();
  void SetRandom();
  virtual unsigned long Hash(const unsigned char*, Count_t, bool = false);
private:
  unsigned char*  r; // Random values
  unsigned char*  b; // Intermediate buffer
  Count_t         s; // Size in bytes of r and b
  static Uniform* u; // Random number generator
};

RandomHash::RandomHash(Count_t bits)
{
  s = (bits - 1) / 8 + 1; // size in bytes
  r = new unsigned char[s];
  b = new unsigned char[s];
  SetRandom(); // Load random values
}

RandomHash::~RandomHash()
{
  delete r;
  delete b;
}

void RandomHash::SetRandom()
{ // Sets the random values for the hash
  if (!u) u = new Uniform(0, 0x100);
  for (Count_t i = 0; i < s; ++i)
    {
      r[i] = u->IntValue();
    }
}

unsigned long RandomHash::Hash(const unsigned char* v, Count_t l, bool db)
{
  Count_t lb = (l - 1) / 8 + 1;
  if (lb != s)
    {
      cout << "HuH?  Length mismatch on RandomHash::Hash()" << endl;
      cout << "lb " << lb << " s " << s << endl;
      return 0;
    }
  for (Count_t i = 0; i < s; ++i)
    { // Compute XOR of all values
      b[i] = v[i] ^ r[i];
      if (db) cout << "b[" << dec << i << "] = " 
                   << hex << (unsigned int)b[i]
                   << " v " << (unsigned int)v[i]
                   << " r " << (unsigned int)r[i] << endl;
    }
  unsigned long ret = 0;

  Count_t lc = s / 4; // number of longs
  // Add them all up
  // THis is a bit bogus since it requires size to be integral mult of 32
  unsigned long* pl = (unsigned long*)b;
  for (Count_t i = 0; i < lc; ++i)
    {
      ret += pl[i];
    }

#ifdef NOT_WORKING
  for (Count_t i = 0; i < s; ++i)
    { // DO a 32 bit folding to produce 32 bit hash
      Count_t m = i % 4;
      ret ^= (b[i] << (m * 8));
      //ret ^= (((unsigned long)b[i]) << i);
      //ret <<= (32 - i);
      if(db) cout << "pass " << dec << i << " ret " << hex << ret << endl;
    }
  if (db)
    {
      for (Count_t i = 0; i < 8; ++i)
        {
          Count_t n = 0;
          for (Count_t j = 0; j < s; ++j)
            {
              if ((j % 4) == 3) continue;
              if (v[j] & (0x01 << i)) n++;
            }
          cout << "1s at position " << i << " " << n << endl;
        }
    }
#endif
  return ret;
}

// Single random variable for all RandomHash objects
Uniform* RandomHash::u = nil;

class Bit128 {
public:
  Bit128();
  operator unsigned char*() const { return d;}
public:
  unsigned char* d;
private:
  static Uniform* u;
};

Uniform* Bit128::u = nil;

Bit128::Bit128()
    : d(nil)
{
  if (!u) u = new Uniform(0, 0x100);
  d = new unsigned char[128/8];
  
  for (Count_t i = 0; i < 128/8; ++i)
    {
      d[i] = u->IntValue();
    }
}

bool operator<(const Bit128& l, const Bit128& r)
{
  for (Count_t i = 0; i < 128/8; ++i)
    {
      if (l.d[i] < r.d[i]) return true;
      if (l.d[i] > r.d[i]) return false;
    }
  return false;
}

set<Bit128> values; // set of values in the filter
  
int main(int argc, char** argv)
{
  // Reproducible for debugging
  //Random::GlobalSeed(31731,44543,425345,19367,48201,72333);
  Count_t oBits = 0x800000;
  Count_t stages = 3;
  Count_t maxSamples = 1000000;
  if (argc > 1) oBits = (Count_t)strtod(argv[1], nil);
  if (argc > 2) stages = atol(argv[2]);
  if (argc > 3) maxSamples = atol(argv[3]);
  
  cout << "oBits 0x" << hex << oBits << " stages " << dec << stages
       << " maxSamples " << maxSamples << endl;
  
  BloomFilter  bf(128, oBits, stages);
  cout << "Bytes used in filter " << oBits / 8 << endl;
  for (Count_t i = 0; i < bf.Stages(); ++i)
    {
      RandomHash* h = new RandomHash(128);
      bf.AddHash(h, i);
    }

  Count_t falsePositives = 0;
  for (Count_t i = 0; i < maxSamples; ++i)
    { // put "maxSamples" random values in the filter
      Bit128 b;
      bool r = bf.AddFilter(b);
      if (r)
        { // Already there, might be collision (unlikely)
          set<Bit128>::iterator f = values.find(b);
          if (f == values.end())
            {
              falsePositives++;
              //cout << "False positive " << falsePositives 
              //     << " on sample " << i << endl;
            }
        }
      //bf.DBStatsShort();
      if (i && ((i % 100000) == 0))
        {
          cout << "Processed " << i << " samples, false positives "
               << falsePositives << " pct " << (double)falsePositives/i*100.0
               << endl;
          //bf.DBStats();
        }
      values.insert(b);
    }
  cout << "Processed " << maxSamples << " samples, false positives "
       << falsePositives << " pct " << (double)falsePositives/maxSamples*100.0
       << endl;
#ifdef CHECK_FALSE_NEGATIVES
  // Now test for false negatives (should be 0)
  cout << "Checking for false negatives" << endl;
  Count_t falseNegatives = 0;
  for (set<Bit128>::iterator i = values.begin(); i != values.end(); ++i)
    {
      if (!bf.InFilter(*i))
        {
          cout << "Oops!  False negative " << ++falseNegatives << endl;
        }
    }
  cout << "False negatives " << falseNegatives << endl;
#endif
  bf.DBStats();
}

          
      

  

  
      
  
  
  
