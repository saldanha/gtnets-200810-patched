// Georgia Tech Network Simulator - Test Main Program
// George F. Riley.  Georgia Tech, Spring 2002

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#include <iostream>
#include <fstream>

#include "debug.h"
#include "simulator.h"
#include "scheduler.h"
#include "node.h"
#include "duplexlink.h"
#include "application-cbr.h"
#include "ratetimeparse.h"
#include "trace.h"
#include "timer.h"
#include "rng.h"
#include "http-distributions.h"
#include "rtt-estimator.h"
#include "tcp-tahoe.h"
#include "routing-manual.h"
#include "routing-nixvector.h"
#include "ftp-client.h"
#include "histogram.h"
#include "seq.h"
#include "udp.h"
#include "mask.h"
#include "validation.h"

using namespace std;

static int pagesize = 0;
unsigned long ReportMemUsage()
{ // Returns usage in bytes
  pid_t  pid;
  char   work[4096];
  FILE*  f;
  char*  pCh;

  if (!pagesize)
    {
      pagesize = getpagesize();
    }
  pid = getpid();
  sprintf(work, "/proc/%d/stat", (int)pid);
  f = fopen(work, "r");
  if (f == NULL)
    {
      printf("Can't open %s\n", work);
      return(0);    
    }
  fgets(work, sizeof(work), f);
  fclose(f);
  strtok(work, " ");
  for (int i = 1; i < 23; i++) 
    {
      pCh = strtok(NULL, " ");
    }
  return(atol(pCh));
}


unsigned long ReportMemUsageMB()
{ // Returns usage in MB!
  return((ReportMemUsage() + 500000) / 1000000 );
}


Random_t AverageRandom(Count_t n, Empirical* r)
{
  Random_t v = 0.0;
  for (Count_t i = 0; i < n; ++i)
    v += r->Value();
  return v / n; // Return the average
}

void tstf(ostream& os)
{
  os << "Hello world" << endl;
}

void tstr(Rate_t r)
{
  cout << " r is " << r << endl;
}

void tstt(Time_t t)
{
  cout << " t is " << t << endl;
}

class MyTimer : public Timer {
public:
  virtual void Timeout(TimerEvent*);  // Called when timer expires
};

void MyTimer::Timeout(TimerEvent* ev)
{
  Time_t now = Simulator::Now();
  cout << "Hello from MyTimer::Timeout time " << now << endl;
  Schedule(new TimerEvent, 0.5);
}

// Code for TCP Tester Applicatoino
class TApp : public Application {
public:
  typedef enum { CLOSED, CONNECTING, CONNECTED } State_t;
public:
  TApp(L4Protocol*, IPAddr_t, PortId_t);// L4 protocol to use, peer ip
  ~TApp();

  // Upcalls from L4 protocol
  void Receive(Packet*,L4Protocol*);    // Data received
  void Sent(Count_t, L4Protocol*);      // Data has been sent
  void CloseRequest(L4Protocol*);       // Peer requested close
  void Closed(L4Protocol*);             // Connection has closed
  void ConnectionComplete(L4Protocol*); // Connection request succeeded
  void ConnectionFailed(L4Protocol*);   // Connection request failed
  void LogResults();                    // Write seq/time plots
public:
  virtual void StartApp();              // Called at time specified by Start
  virtual void StopApp();               // Called at time specified by Stop
  virtual Application* Copy() const;    // Make a copy of this application
private:
  L4Protocol* l4proto;                  // Associated l4 proto
  IPAddr_t    peerIp;
  PortId_t    peerPort;  
  State_t     state;
  Count_t     toSend;
  Count_t     totalSent;

};

TApp::TApp(L4Protocol* l4, IPAddr_t ip, PortId_t p)
  : l4proto(l4), peerIp(ip), peerPort(p), state(CLOSED),
    toSend(100000), totalSent(0)
{
  l4->AttachApplication(this);
}

TApp::~TApp()
{
  delete l4proto;
}

// Upcalls from L4 protocol
void TApp::Receive(Packet* p,L4Protocol*)
{ // Data received
  delete p;
}

void TApp::Sent(Count_t s, L4Protocol*)
{ // Data has been sent
  totalSent += s;
  DEBUG0((cout << "Tapp " << Name() << " Sent " << s << " bytes" 
          << " totalSent " << totalSent << endl));
  if (totalSent == toSend)
    { // Sent all, time to close
      DEBUG0((cout << "Tapp " << Name() << " sent " << totalSent 
              << ", closing" << endl));
      l4proto->Close();
    }
}

void TApp::CloseRequest(L4Protocol* l4)
{ // Connection has closed
  l4->Close();  // Just close back
}

void TApp::Closed(L4Protocol*)
{ // Connection has closed
  DEBUG0((cout << "TApp " << Name() << " closed" << endl));
}

void TApp::ConnectionComplete(L4Protocol* l4)
{ // Connection request succeeded
  //cout << "TApp::ConnectionComplete " << Name() << endl;
  state = CONNECTED;
  l4->Send(toSend); // Send specified number of bytes
}

void TApp::ConnectionFailed(L4Protocol*)
{  // Connection request failed
  DEBUG0((cout << "TApp::ConnectionFailed " << Name() << endl));
  state = CLOSED;
}

void TApp::LogResults()
{
  string n = l4proto->Name() + string("txseq.txt");
  ofstream f(n.c_str());
  ((TCP*)(l4proto))->LogTimeSeq(TCP::LOG_SEQ_TX, f); // Log to file
  f.close();
  string n1 = l4proto->Name() + string("rxack.txt");
  ofstream f1(n1.c_str());
  ((TCP*)(l4proto))->LogTimeSeq(TCP::LOG_ACK_RX, f1); // Log to file
  f1.close();
  string n2 = l4proto->Name() + string("cwin.txt");
  ofstream f2(n2.c_str());
  ((TCP*)(l4proto))->LogTimeSeq(TCP::LOG_CWIN, f2); // Log to file
  f2.close();
}

void TApp::StartApp()       // Called at time specified by Start
{
  l4proto->Connect(peerIp, peerPort);
  state = CONNECTING;
  //cout << "TApp Connecting to " << (string)IPAddr(peerIp) << endl;
}

void TApp::StopApp()        // Called at time specified by Stop
{
}

Application* TApp::Copy() const 
{
  return new TApp(*this);
}

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;

// Set manual routing 
 Routing::SetRouting(new RoutingManual());

#define TEST_SEQ_RNG
#ifdef  TEST_SEQ_RNG
 Sequential seqrng(0, 10, 3);
 for (int i = 0; i < 30; ++i)
   {
     cout << "Next Seqrng " << seqrng.IntValue() << endl;
   }
 exit(0);
#endif

#undef  TEST_SERIALIZE
#ifdef  TEST_SERIALIZE
 Packet* p = new Packet();
 p->time = 10.0; // Just testing
 L2802_3Header* l2pdu = new L2802_3Header(MACAddr(0, 0x10),
                                          MACAddr(0, 0x11),
                                          0x0800);
 p->PushPDU(l2pdu);
 IPV4Header* l3pdu = new IPV4Header();
 l3pdu->version = 4;
 l3pdu->headerLength = 5;
 l3pdu->serviceType = 6;
 l3pdu->totalLength = 7;
 l3pdu->identification = 8;
 l3pdu->flags = 9;
 l3pdu->fragmentOffset = 10;
 l3pdu->ttl = 11;
 l3pdu->protocol = 6;
 l3pdu->headerChecksum = 12;
 l3pdu->src = IPAddr("192.168.0.1");
 l3pdu->dst = IPAddr("192.168.1.1");
 l3pdu->uid = 13;
 // Add a nixvector to the l3pdu
 NixVectorOption* nv = new NixVectorOption();
 nv->size = 32;
 nv->used = 10;
 nv->nixVector = (NixBits_t*)0x01020304;
 l3pdu->options.push_back(nv);
 // And add the l3pdu to the packet
 p->PushPDU(l3pdu);

 TCPHeader* l4pdu = new TCPHeader();
 l4pdu->sourcePort = 0x100;
 l4pdu->destPort = 0x101;
 l4pdu->sequenceNumber = 0x102;
 l4pdu->ackNumber = 0x103;
 l4pdu->headerLength = 0x104;
 l4pdu->flags = 0x105;
 l4pdu->window = 0x106;
 l4pdu->checksum = 0x107;
 l4pdu->urgentPointer = 0x108;
 l4pdu->fid = 0x109;
 l4pdu->dataLength = 0x10a;
 p->PushPDU(l4pdu);

 char l5data[16];
 memset(l5data, 0x0f, sizeof(l5data));
 Data* l5pdu = new Data(16, l5data);
 p->PushPDU(l5pdu);

 // Validate
 Serializable::Validate(p);

 Size_t  sz = p->SSize() + sizeof(Size_t);
 Size_t  savesz = sz;
 char*   b = new char[sz];
 char*   saveb = b;
 cout << "before b " << (void*)b << " sz " << sz << endl;
 b = Serializable::PutSize(b, sz, sz);
 cout << "after  b " << (void*)b << " sz " << sz << endl;
 p->Serialize(b,sz); // Serialize to buffer
 cout << "after1 b " << (void*)b << " sz " << sz << endl;
 Serializable::Dump(saveb,savesz); // debug

 // Now try to reconstruct it
 Size_t  newsz = 0;
 char* newb = Serializable::GetSize(saveb, savesz, newsz);

 Packet* p1 = new Packet(newb,savesz); // Construct a new one
 cout << "p1 uid " << p1->uid
      << " top " << p1->top
      << " time " << p1->time
      << endl;
 exit(0);
#endif
 
#undef  TEST_SEQ
#ifdef  TEST_SEQ
 Seq s1(1);
 Seq s2(2);
 Seq s3(3);
 Seq s4(4);
 cout << "s1 " << s1 << endl;
 cout << "s1++ " << s1++ << endl;
 cout << "s1++ " << s1++ << endl;
 cout << "++s1 " << ++s1 << endl;
 s1 += 5;
 cout << "s1 should be 9 " << s1 << endl;
 s1 += s2;
 cout << "s1 should be 11 " << s1 << endl;
 s1 = s1+ s3 + s4;
 cout << "s1 should be 18 " << s1 << endl;
 cout << "Should be true  " << (s2 <  s1) << endl;
 cout << "Should be true  " << (s2 <= s1) << endl;
 cout << "Should be false " << (s1 <  s2) << endl;
 cout << "Should be false " << (s1 <= s2) << endl;
 s2 = 0xffffff00;
 s3 = 0x10;
 cout << "Should be true  " << (s2 <  s3) << endl;
 cout << "Should be true  " << (s2 <= s3) << endl;
 cout << "Should be false " << (s2 >  s3) << endl;
 cout << "Should be false " << (s2 >= s3) << endl;
 cout << "Should be 272 " << (s3-s2) << endl;
 cout << "Should be 14  " << (s1-s4) << endl;

 s2 = 0x100;
 cout << "Should be false " << (s2 <  s3) << endl;
 cout << "Should be false " << (s2 <= s3) << endl;
 cout << "Should be true  " << (s2 >  s3) << endl;
 cout << "Should be true  " << (s2 >= s3) << endl;
 s2 = 0x100;
 exit(0);
#endif

#undef TEST_HTTP_DIST
#ifdef TEST_HTTP_DIST
 cout << "Average HttpPrimaryRequest " 
      <<  AverageRandom(100000, new HttpPrimaryRequest) << endl;
 cout << "Average HttpSecondaryRequest " 
      <<  AverageRandom(100000, new HttpSecondaryRequest) << endl;
 cout << "Average HttpPrimaryReply " 
      <<  AverageRandom(100000, new HttpPrimaryReply) << endl;
 cout << "Average HttpSecondaryReply " 
      <<  AverageRandom(100000, new HttpSecondaryReply) << endl;
 cout << "Average HttpFilesPerPage " 
      <<  AverageRandom(100000, new HttpFilesPerPage) << endl;
 cout << "Average HttpConsecutivePages " 
      <<  AverageRandom(100000, new HttpConsecutivePages) << endl;
 HttpThinkTime* tt = new HttpThinkTime;
 cout << "Average HttpThinkTime " 
      <<  AverageRandom(100000, tt) << endl;
 tt->Bound(600.0);
 cout << "Average HttpThinkTime (bounded at 600) " 
      <<  AverageRandom(100000, tt) << endl;
#endif

#undef TEST_TIMER
#ifdef TEST_TIMER
 MyTimer t;
 t.Schedule(new TimerEvent, 0.5);
#endif

#undef  TEST_RTT
#ifdef  TEST_RTT
 RTTMDev rtt(0.1, 6.0);
 for (Time_t t = 1.0; t < 10.0; ++t)
   {
     rtt.Measurement(t); // Add dummy measurement
     cout << "rtt Measurement " << t
          << " Est " << rtt.Estimate()
          << " Var " << rtt.Variance()
          << " timeout " << rtt.RetransmitTimeout() << endl;
   }
 for (Time_t t = 10.0; t > 0.0; --t)
   {
     rtt.Measurement(t); // Add dummy measurement
     cout << "rtt Measurement "<< t
          << " Est " << rtt.Estimate()
          << " Var " << rtt.Variance()
          << " timeout " << rtt.RetransmitTimeout() << endl;
   }
#endif

#undef TEST_RATE_TIME_CONVERSION
#ifdef TEST_RATE_TIME_CONVERSION
 tstr(Rate("10Mb"));
 tstt(Time("10ns"));
#endif

 // Open the trace file
 Trace* gs = Trace::Instance();
 gs->Open("trace.txt");
 gs->IPDotted(true);

 Node* n0 = new Node();
 Node* n1 = new Node();
 //cout << "Node n0 name is " << n0->Name() << endl;
 //cout << "n0 " << n0 << " n1 " << n1 << endl;
 // Use default tracing on these nodes (due to commenting out below)
 // Uncomment to force on or off
 //n0->SetTrace(Trace::ENABLED);  // Trace everything on this node
 //n1->SetTrace(Trace::ENABLED);  // Trace everything on this node
 //n0->SetTrace(Trace::DISABLED); // No tracing on this node
 //n1->SetTrace(Trace::DISABLED); // No tracing on this node
 Node* pn = n0; // Prior
 Interface* pi = pn->AddInterface(L2Proto802_3(), 
                                  0xc0010001, Mask(24), MACAddr::Allocate());
 Link::DefaultRate(Rate("10Mb")); // Set to 10Megabits /sec 
 //Link::DefaultRate(Rate("1Mb"));  // Set to 1Megabits /sec 
 // Create 3 intermediate nodes
 for (int i = 0; i < 3; ++i)
   {
     Node* nn = new Node();
     //cout << "nn " << nn << endl;
     //nn->SetTrace(Trace::ENABLED);  // Trace everything on this node
     nn->SetTrace(Trace::DISABLED);  // Trace nothing on this node
     Interface* ni = nn->AddInterface();
     DuplexLink(pi, ni);
     pi = nn->AddInterface(); // add right side interface
     // Set the manual routing for interior routers
     nn->DefaultRoute(RoutingEntry(pi, 0));
     nn->AddRoute(0xc0010001, 24, ni, IPADDR_NONE);
     nn->AddRoute(0x0a000000, 8,  ni, IPADDR_NONE);
     nn->AddRoute(0xc0020001, 24, pi, IPADDR_NONE);
     nn->AddRoute(0x0b000000, 8,  pi, IPADDR_NONE);
   }
 Interface* ni = n1->AddInterface(L2Proto802_3(),
                                  0xc0020001, Mask(24), MACAddr::Allocate());
 DuplexLink(pi, ni);

 // Reduce detail of L2 trace messages
 //L2Proto802_3::Instance()->DetailOff(L2Proto802_3::TYPE);
 //L2Proto802_3::Instance()->DetailOff(L2Proto802_3::PROTO);
 // Delete l2 tracing altogether
 //L2Proto802_3::Instance()->SetTrace(Trace::DISABLED);
 // Enable l2 tracing
 //L2Proto802_3::Instance()->SetTrace(Trace::ENABLED);

 // Increase detail of L3 trace messages
 IPV4::Instance()->DetailOn(IPV4::TOTALLENGTH);
 // Delete l3 tracing altogether
 //IPV4::instance->SetTrace(Trace::DISABLED);
 // Enable all l3 tracing
 IPV4::Instance()->SetTrace(Trace::ENABLED);

#undef TEST_SCALE
#ifdef TEST_SCALE
 // Routing::SetRouting(new RoutingNixvector());
 Node* p = NULL;
 for (int i = 0; i < 500000; ++i)
   { // Create 1/2 million nodes and links
     Node* n = new Node();
     if (p)
       {
         DuplexLink(p, n);
       }
     p = n;
     if (i && ((i % 10000) == 0))
       {
         cout << "Count " << i
              << " memusage " << ReportMemUsageMB() << "Mb" << endl;
       }
   }
#endif

#undef TEST_CBR_APP
#ifdef TEST_CBR_APP
 CBRApplication cbr(n0, n1); // Create the cbr application and protocols
 cbr.Start(0.0);             // Start sending at time 0
#endif

#undef TEST_TCP
#ifdef TEST_TCP
 TCPTahoe* t0 = new TCPTahoe(n0);      // TCP Endpoint on node n0
 TCPTahoe* t1 = new TCPTahoe(n1);      // TCP Endpoint on node n0
 t0->Bind(1000);
 t1->Bind(2000);
 t1->Listen();               // t1 accepts connections
 t0->Connect(n1->IPAddr(), 2000); // Connect to peer
 
 // Test no-reply connection
 TCPTahoe* t2 = new TCPTahoe(n0);
 t2->Bind(1001);
 t2->Connect(IPAddr("10.10.0.1"), 2000);
#endif

#undef  TEST_UDP1
#ifdef  TEST_UDP1
 UDP* u0 = new UDP(n0);
 UDP* u1 = new UDP(n1);
 u0->Bind();
 u1->Bind(20000);
 n0->SetTrace(Trace::ENABLED); // Trace information to/from this protocol
 n1->SetTrace(Trace::ENABLED); // Trace information to/from this protocol
 u0->SendTo(100000, IPAddr("192.2.0.1"), 20000);
#endif

#define TEST_TCP1
#ifdef  TEST_TCP1
 TCP::LogFlagsText(true);    // Log flags in text mode
 TCPTahoe* t0 = new TCPTahoe(n0);      // TCP Endpoint on node n0
 TCPTahoe* t1 = new TCPTahoe(n1);      // TCP Endpoint on node n0
 t0->SetTrace(Trace::ENABLED); // Trace information to/from this protocol
 t1->SetTrace(Trace::ENABLED); // Trace information to/from this protocol
 t0->Name("T0");
 t1->Name("T1");
 cout << "t0 is " << t0 << endl;
 cout << "t1 is " << t1 << endl;
 t0->FlowId(110);            // Flowid for tracing
 t0->Bind(0);
 t0->EnableTimeSeq(TCP::LOG_SEQ_TX);
 t0->EnableTimeSeq(TCP::LOG_ACK_RX);
 t0->EnableTimeSeq(TCP::LOG_CWIN);
 t1->FlowId(111);
 t1->Bind(80);
 t1->Listen();               // t1 accepts connections
 TApp tapp(t0, IPAddr("192.2.0.1"), 80);
 tapp.Name("GoodApp");
 tapp.Start(0.5);

 // Try a failed connection
 TCPTahoe* t2 = new TCPTahoe(n0);
 t2->Name("T2");
 TApp  tapp1(t2, IPAddr("192.5.0.1"), 80); // Test connection failed code
 tapp1.Name("BadApp");
 tapp1.Start(1.0);

 // Try another to same server, later time
 TCPTahoe* t3 = new TCPTahoe(n0);
 t3->SetTrace(Trace::ENABLED); // Trace information to/from this protocol
 t3->Name("T3");
 TApp  tapp2(t3, IPAddr("192.2.0.1"), 80);
 tapp2.Name("SecondApp");
 tapp2.Start(9.98);

 // Test no-reply connection
 //TCPTahoe* t2 = new TCPTahoe(n0);
 //t2->Bind(1001);
 //t2->Connect(IPAddr("10.10.0.1"), 2000);
#endif

#undef  TEST_FTP
#ifdef  TEST_FTP
 FTPClient* ftp = new FTPClient(new L4Dummy(Time("10ms"), Time("1us")));
 ftp->Open(IPAddr("10.0.0.1"));
 Count_t r = ftp->Put(new Uniform(50,250), new Uniform(500, 5000));
 ftp->Put(200, 2000);
 ftp->Sleep(new Uniform(0.5, 1.5));
 ftp->Repeat(r, 3);
 ftp->Close();
 ftp->Start(0.1);
 ftp->Stop(5.0);
 ftp->Start(6.0);
 ftp->Stop(9.0);
#endif

#undef  TEST_HISTO
#ifdef  TEST_HISTO
 Histogram h(20, 10);
 h.Count(5);
 h.Count(5);
 h.Count(6);
 h.Count(6);
 h.Count(8);
 h.Count(22); // Out of range for testing
 h.Log(cout, "#Histogram Testing", ',');
 h.CDF(cout, "#CDF Testing", ',');
#endif

 s.Progress(5.0);            // Request progress messages on stdout

 // Let simulator know when to stop
 s.StopAt(20.0);

 // Start the simulation
 s.Run();
#ifdef TEST_TCP1
 tapp.LogResults();
#endif
 cout << "Finished!" << endl; 
 cout << "Setup time " << s.SetupTime() << endl;
 cout << "Execution Time " << s.RunTime() << endl;
 //cout<<"Link utilization is   "<<pDl->local->Utilization() << endl;
 //cout<<"Average Queue Size is "<<pDl->local->GetQueue()->Average() << endl;
 // Debug..print some sizes
 cout << "Total Events Scheduled "
      << Scheduler::Instance()->TotalEventsScheduled() << endl;
 cout << "Total Events Processed "
      << Scheduler::Instance()->TotalEventsProcessed() << endl;
 cout << "Sizeof Node  " << sizeof(Node) << endl;
 cout << "Size of I/F  " << sizeof(Interface) << endl;
 cout << "Size of Lp2p " << sizeof(Linkp2p) << endl;
 cout << "Size of RtMn " << sizeof(RoutingManual) << endl;
 cout << "Size of RtNv " << sizeof(RoutingNixVector) << endl;
}
