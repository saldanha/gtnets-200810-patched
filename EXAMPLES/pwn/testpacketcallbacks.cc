// Simple tester for TCP
// George F. Riley.  Georgia Tech, Spring 2002

#include <iostream>

#include "simulator.h"
#include "application-tcpsend.h"
#include "duplexlink.h"
#include "queue.h"
#include "ratetimeparse.h"
#include "tcp-tahoe.h"
#include "dumbbell.h"
#include "args.h"
#include "validation.h"  
#include "rng.h"
#include "droppdu.h"

using namespace std;

Uniform dropRNG;

// Define the callback function
bool MyCallback(Layer_t l, Proto_t p, PacketCallbacks::Type_t t,
                Packet* pkt, Node* n, Interface* iface)
{
  cout << "Hello from callback layer " << l << " proto " << p
       << " type " << t 
       << " iface " << iface
       << endl;
  if (l == 3 && t == PacketCallbacks::TX)
    { // For testing, drop 10% of transmitted packets at l3
      if (dropRNG.Value() < 0.1)
        { // Make a trace entry for this drop, drop the packet, return false
          cout << "Callback dropping packet" << endl;
          DropPDU d("L3-CB", pkt);
          n->TracePDU(nil, &d);
          delete pkt;
          return false;
        }
    }
  return true;
}

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  // Create the simulator object
  Simulator s;
  Count_t nleaf = 1;
  
  Arg("nleaf", nleaf, 1);
  Arg::ProcessArgs(argc, argv);
  
  // Open the trace file
  Trace* tf = Trace::Instance();
  if (!Arg::Specified("nolog")) tf->Open("testtcp.txt");
  tf->IPDotted(true);

  // Increase detail of L3 trace messages
  IPV4::Instance()->DetailOn(IPV4::TOTALLENGTH);
  // Enable all l3 tracing
  IPV4::Instance()->SetTrace(Trace::ENABLED);


  // Set some defaults
  Queue::Default();
  Queue::DefaultLength(200000);    // 200,000 bytes
  Link::DefaultRate(Rate("10Mb")); // 10 Mega-bits/sec
  Link::DefaultDelay(Time("10ms"));// 10 milliseconds delay

  // Create the topology, using default link
  Dumbbell b(nleaf, nleaf, 0.1, IPAddr("192.168.1.1"), IPAddr("192.168.2.1"));
  b.BoundingBox(Location(0,0), Location(1,1));
  
  // Restrict queue limit on bottleneck
  if (!Arg::Specified("unrestricted")) b.LeftQueue()->SetLimit(10000);

  // If "forcedloss" specified, force 20 losses at time 1.0
  if (Arg::Specified("forcedloss")) b.LeftQueue()->AddForcedLoss(1.0, 20);

  // Set bottleneck link delay a bit longer
  b.LeftLink()->Delay(Time("100ms"));
  b.RightLink()->Delay(Time("100ms"));
  
  // Create the TCP endpoints
  TCP::LogFlagsText(true);    // Log flags in text mode
  for (Count_t k = 0; k < nleaf; ++k)
    {
      TCPTahoe* server = new TCPTahoe(b.Right(k));
      server->Bind(80);
      server->Listen();
      // Enable tracing
      server->SetTrace(Trace::ENABLED);
    }
  
  // Create the TCP sending applications using TCPTahoe
  Uniform startRNG(0, 0.1);
  TCP* firstTCP = nil;
  for (Count_t k = 0; k < nleaf; ++k)
    {
      Node* n = b.Left(k);
      TCPSend* pApp = (TCPSend*)n->AddApplication(
          TCPSend(b.Right(k)->GetIPAddr(),// Peer IP Address
                  80,                     // Peer port number
                  Constant(100000)));     // Number of bytes to send
      // Ask for callbacks
      n->AddCallback(2, 0, PacketCallbacks::RX, nil, MyCallback);
      n->AddCallback(2, 0, PacketCallbacks::TX, nil, MyCallback);
      n->AddCallback(3, 0, PacketCallbacks::RX, nil, MyCallback);
      n->AddCallback(3, 0, PacketCallbacks::TX, nil, MyCallback);
      n->AddCallback(4, 0, PacketCallbacks::RX, nil, MyCallback);
      n->AddCallback(4, 0, PacketCallbacks::TX, nil, MyCallback);
      
      // Parameterize the tcp endpoint
      TCP* ctcp = pApp->l4Proto;
      ctcp->SetTrace(Trace::ENABLED);
      ctcp->Name("TestTCPClient");
      if (Arg::Specified("extradelay"))
        {
          cout << "Adding extra delays" << endl;
          ctcp->AddExtraRxDelay(Time("10ms"));
          ctcp->AddExtraTxDelay(Time("10ms"));
        }
      pApp->Start(startRNG.Value());
      if (!firstTCP) firstTCP = ctcp;
    }

  s.StopAt(100.0);
  if (Arg::Specified("animate") && !Validation::noAnimation)
    { // Enable animiation if specified on command line
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Run();
}

