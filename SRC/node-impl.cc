// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: node-impl.cc,v 1.22 2006/01/31 20:48:39 riley Exp $



// Georgia Tech Network Simulator - Node Implementation class
// George F. Riley.  Georgia Tech, Spring 2002

// Defines some common functionality between Real nodes
// and Ghost nodes.

#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif

#ifdef HAVE_QT
#include <qcanvas.h>
#include <qpoint.h>
#endif

//#define DEBUG_MASK 0x02
#include "debug.h"
#include "node-real.h"
#include "protograph.h"
#include "portdemux.h"
#include "ipaddr.h"
#include "duplexlink.h"
#include "application.h"
#include "mobility.h"
#include "link-rti.h"
#include "mask.h"
#include "interface-real.h"
#include "interface-wireless.h"
#include "interface-ghost.h"
#include "interface-ethernet.h"

using namespace std;

NodeImpl::NodeImpl(Node* n)
    : pNode(n), ipAddr(IPADDR_NONE), neighborCount(0), down(false)
{
}

NodeImpl::~NodeImpl()
{
}

// IP Address Management
IPAddr_t    NodeImpl::GetIPAddr()
{  // Get the IPAddres of this node 
  return ipAddr;   // Return IP Address....may be IPADDR_NONE
}

void        NodeImpl::SetIPAddr(IPAddr_t i)
{ // Set the IPAddres of this node
  ipAddr = i;
  if (interfaces.size() == 1)
    { // Only one iface, set the same IPAddr there
      interfaces[0]->SetIPAddr(i);
    }
}

void NodeImpl::IPAddrs(IPMaskVec_t& a)
{ // Create a list of IP Addresses
  bool localFound = false;  // Insure the nodes "ipAddr" is included
  for (IFVec_t::size_type j = 0; j < interfaces.size(); ++j)
    {
      Interface* i = interfaces[j];
      if (i->GetIPAddr() != IPADDR_NONE)
        {
          a.push_back(IpMask(i->GetIPAddr(), i->GetIPMask()));
          if ((ipAddr & i->GetIPMask()) == (i->GetIPAddr() & i->GetIPMask()))
            {
              localFound = true;
            }
        }
    }
  if (!localFound && (ipAddr != IPADDR_NONE))
    { // Need to add node's also
      a.push_back(IpMask(ipAddr, MASK_ALL));
    }
}

bool NodeImpl::LocalIP( IPAddr_t ip)
{ // Check if specified ip address is local
  if (ipAddr == IPADDR_NONE) return false; // This node has no ip address
  if (ip == ipAddr) return true; // Found specified local
  NodeReal::count4 += interfaces.size();
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      if ((IPAddr_t)interfaces[i]->GetIPAddr() == ip)
        {
          return true; // Found local
        }
    }
  return false; // Not found, not local
}

bool NodeImpl::IPKnown()
{ // True if non-default ip is assigned
  return ipAddr != IPADDR_NONE;
}

// Interfaces

Interface* NodeImpl::AddInterface(Interface* pif)
{ // Add a pre-allocated interface
  // Add to list  of interface by Mac addresses
  Interface::Add(pif, pif->GetMACAddr());
  pif->SetNode(pNode);
  interfaces.push_back(pif);
  if (!IPKnown() && (pif->GetIPAddr() != IPADDR_NONE))
    { // Default IP address to first interface
      SetIPAddr(pif->GetIPAddr());
    }
  return pif;
}

  
Interface* NodeImpl::AddInterface(const L2Proto& l2, bool bootstrap)
{
  return AddInterface(l2, IPADDR_NONE, MASK_ALL, MACAddr::Allocate(),
		      bootstrap);
}

Interface* NodeImpl::AddInterface(const L2Proto& l2,
                                  IPAddr_t i, Mask_t mask,
                                  MACAddr m, bool bootstrap)
{
  Interface* pif;

  if (IsReal())
    { // Real node, use either wired or wireless interface
      if (l2.IsWireless())
        {
          DEBUG(2,(cout << "Node " << pNode->Id()
                    << " adding wireless interface" << endl));
          pif = new InterfaceWireless(l2, i, mask, m, bootstrap);
        }
      else
        {
          DEBUG(2,(cout << "Node " << pNode->Id()
                   << " adding real interface" << hex << i << endl));
          pif = new InterfaceReal(l2, i, mask, m, bootstrap);
        }
    }
  else
    { // Use a ghost interface
      DEBUG(2,(cout << "Node " << pNode->Id()
               << " adding ghost interface" << endl));
      pif = new InterfaceGhost(l2, i, mask, m, bootstrap);
    }

  // If firstbitrx node, note first bit rx interface
  if (FirstBitRx())
    {
      pif->FirstBitRx(true);
    }
  
  
  // Add to list  of interface by Mac addresses
  Interface::Add(pif, m.macAddr);
  pif->SetNode(pNode);
  interfaces.push_back(pif);
  if (!IPKnown())
    { // Default IP address to first interface
      SetIPAddr(i);
    }
  return pif;
}

Interface* NodeImpl::AddInterface(const L2Proto& l2, const Interface& iface,
                                  IPAddr_t i, Mask_t mask,
                                  MACAddr m, bool bootstrap)
{
  Interface* pif;
  // if the interface parameter is of type interface ethernet
  if(iface.IsEthernet())
     {
        DEBUG(2,(cout << "Node " << pNode->Id()
                   << " adding Ethernet interface" << hex << i << endl));
       pif = new InterfaceEthernet(l2, i, mask, m, bootstrap);
    }
  else
    {
      return AddInterface(l2, i, mask, m, bootstrap);
    }

  pif->SetNode(pNode);
  interfaces.push_back(pif);
  if (!IPKnown())
    { // Default IP address to first interface
      SetIPAddr(i);
    }
  return pif;
}

Count_t NodeImpl::InterfaceCount()
{
  return interfaces.size();
}

const IFVec_t& NodeImpl::Interfaces()
{ // Get the complete list of interfaces
  return interfaces;
}

Interface* NodeImpl::GetIfByLink(Link* l)
{
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      if (interfaces[i]->GetLink() == l) return interfaces[i]; // Found it
    }
  return nil;  // Does not exist
}

void NodeImpl::UseWormContainment(bool b) 
{
  usewormcontainment = b;
}

Interface* NodeImpl::GetIfByNode(Node* n)
{ // Find the interface used to get to the specified node
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      Link* pLink = interfaces[i]->GetLink();
      if (!pLink) continue;
      if (pLink->NodeIsPeer(n)) return interfaces[i]; // found
    }
  return nil;
}

Interface* NodeImpl::GetIfByIP(IPAddr_t ip)
{ // Find the interface with the specified IP Address
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      if ((IPAddr_t)interfaces[i]->GetIPAddr() == ip)
        {
          return interfaces[i]; // found
        }
    }
  return nil;
}

// Links.
// All of the following are just a syntactic shortcut to the DuplexLink object.
// Each creates a local DuplexLink object, which is destroyed on exit.
Interface* NodeImpl::AddDuplexLink(Node* rn)
{ // Add duplex link to/from remote
  DuplexLink dl(pNode, rn);
  return dl.localif;
}

Interface* NodeImpl::AddDuplexLink(Node* rn, const Linkp2p& l)
{ // Add duplex link to/from remote
  DuplexLink dl(pNode, l, rn, l);
  return dl.localif;
}

Interface* NodeImpl::AddDuplexLink(Interface* li, Interface* ri)
{ // Add a link between existing interfaces
  DuplexLink dl(li, ri);
  return dl.localif;
}

Interface* NodeImpl::AddDuplexLink(Interface* li, Interface* ri,
                               const Linkp2p& l)
{
  DuplexLink dl(li, l, ri, l);
  return dl.localif;
}

// Interface* NodeImpl::AddDuplexLink(Interface* li, Interface* ri, const Linkp2p& l, Node* rn)
// {
//   DuplexLink dl(pNode, li, nil, nil, l, rn, ri, nil, nil, l);
//   return dl.localif;
// }

Interface* NodeImpl::AddDuplexLink(Node* rn,
                            IPAddr_t li, Mask_t lm,
                            IPAddr_t ri, Mask_t rm)
{
  DuplexLink dl(pNode, li, lm, rn, ri, rm);
  return dl.localif;
}

Interface* NodeImpl::AddDuplexLink(Node* rn, const Linkp2p& l,
                            IPAddr_t li, Mask_t lm,
                            IPAddr_t ri, Mask_t rm)
{
  DuplexLink dl(pNode, li, lm, l, rn, ri, rm, l);
  return dl.localif;
}

Link* NodeImpl::GetLink(Node* n)
{
  Interface* i = GetIfByNode(n); // Interface to specified node
  if (!i) return nil;            // Not found
  return i->GetLink();           // Get the associated link
}

Interface* NodeImpl::AddRemoteLink(IPAddr_t i, Mask_t m)
{
  Interface* iFace = AddInterface(L2Proto802_3(), i, m);
  iFace->SetLink(new LinkRTI(i, m, iFace));
  return iFace;
}

Interface* NodeImpl::AddRemoteLink(IPAddr_t i, Mask_t m, Rate_t r, Time_t d)
{
  Interface* iFace = AddInterface(L2Proto802_3(), i, m);
  iFace->SetLink(new LinkRTI(i, m, iFace, r, d, 0));
  return iFace;
}

// Simplex links, for modeling one-way links
Interface* NodeImpl::AddSimplexLink(Node* n)
{
  return AddSimplexLink(n, Linkp2p::Default(), IPADDR_NONE, Mask(32));
}

Interface* NodeImpl::AddSimplexLink(Node* n, const Linkp2p& l)
{
  return AddSimplexLink(n, l, IPADDR_NONE, Mask(32));
}

Interface* NodeImpl::AddSimplexLink(Node* n, const Linkp2p& l,
                                    IPAddr_t i,Mask_t m)
{
  Interface* localif = AddInterface(L2Proto802_3(), i, m);
  Interface* remoteif = n->AddInterface(L2Proto802_3(), IPADDR_NONE, Mask(32));
  Linkp2p* lp = (Linkp2p*)l.Copy();
  localif->SetLink(lp);
  lp->SetPeer(remoteif);
  lp->SetLocalIf(localif);
  return localif;
}

// Neighbor management
void NodeImpl::Neighbors(NodeWeightVec_t& n, bool noLeaf)
{ // Ask each interface to add to neighbor list
  NodeReal::count3++;
  for (IFVec_t::size_type i = 0; i != interfaces.size(); ++i)
    {
      Interface* iface = interfaces[i];
      if (iface->IsWireless()) continue; // ? Is this right?
      if (noLeaf)
        { // Check for leaf neighbors with local route,
          // Skip if so
          if (iface->NeighborCount(pNode) <= 1)
            { // Only one neighbor, find it
              // ?? Why is this so hard?
              Link* l = iface->GetLink();
              if (l)
                {
                  InterfaceBasic* pPeer = l->GetPeer((Packet*)nil);
                  if (pPeer)
                    {
                      Node* n = pPeer->GetNode();
                      if (n)
                        {
                          if (n->NeighborCount() <= 1)
                            { // Finally, found a leaf
                              IPAddr_t ip = n->GetIPAddr();
                              // If it's a local route from here, ignore it
                              if (iface->IsLocalIP(ip)) continue;
                            }
                        }
                    }
                }
            }
        }
      DEBUG(1,(cout << "Node " << pNode->Id() 
               << " asking if about neighbors " << endl));
      iface->Neighbors(n);
    }
}

Count_t NodeImpl::NeighborCount()
{ // Count number of routing neighbors
  // Done by summing routing neighbors for each interface
  // First see if previously calcualted
  if (neighborCount) return neighborCount;
   
  Count_t t = 0;
  for (IFVec_t::iterator i = interfaces.begin(); i != interfaces.end(); ++i)
    {
      t += (*i)->NeighborCount(pNode);
    }
  neighborCount = t;
  return t;
}

void NodeImpl::NeighborsByIf(Interface* i, IPAddrVec_t& v)
{
  NodeReal::count2++;
  NodeWeightVec_t nwv;
  // Get neighbors for this interface
  i->Neighbors(nwv, true); // Force all even for single-gateway ethernet
  for (NodeWeightVec_t::iterator j = nwv.begin(); j != nwv.end(); ++j)
    {
      v.push_back(j->node->GetIPAddr()); // Return list of neighbor IP's
    }
}


// Routing Interface
Interface* NodeImpl::LocalRoute(IPAddr_t ip)
{ // Find if destination ip is directly connected on any local interace
  DEBUG0((cout << "NodeImpl::LocalRoute, id " << pNode->Id() << endl));
  NodeReal::count1 += interfaces.size();
  for (IFVec_t::iterator i = interfaces.begin(); i != interfaces.end(); ++i)
    {
      if ((*i)->IsLocalIP(ip)) return *i; // found it
    }
  return nil; // Not local
}

Count_t    NodeImpl::GetNix(Node* n) const
{ // Get Neighbor Index for spec'd node
  Count_t nix = 0;
  // Scan the interfaces looking for specified neighbor
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      if (interfaces[i]->NodeIsPeer(n))
        { // Found it
          return nix + interfaces[i]->NodePeerIndex(n);
        }
      nix += interfaces[i]->NeighborCount(pNode); // Advance neighbor count
    }
  return MAX_COUNT; // Not found
}

#ifdef HAVE_QT

CustomShape_t NodeImpl::CustomShape()
{
  return nil; // Default to no custom shape if not overridden.
}

void NodeImpl::CustomShape(CustomShape_t)
{ // Ignored if not overridden
}

bool NodeImpl::CustomShapeFile(const char*)
{ // Overridden by node-real
  return true;
}

bool NodeImpl::CustomShapeImage(const Image&)
{ // Overridden by node-real
  return true;
}

#endif

bool NodeImpl::ICMPEnabled() const
{
  return false; // Over-ridden by node-real
}

void NodeImpl::DisableICMP()
{
  // Over-ridden by node-real
}

// Failure management
void NodeImpl::Down()
{ // specify each interface is down
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      interfaces[i]->Down(true);
    }
  down = true;
  Simulator::instance->TopologyChanged(false);
#ifdef HAVE_QT  
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  Display(qtw);
#endif
}

void NodeImpl::Up()
{ // Node has recovered from failure
  // specify each interface is up
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      interfaces[i]->Up();
    }
  down = false;
  Simulator::instance->TopologyChanged(true);
#ifdef HAVE_QT
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  Display(qtw);
#endif
}

bool NodeImpl::IsDown()
{
  return down;
}



  
