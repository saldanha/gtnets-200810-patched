// Test the memory usage of real and ghost nodes
// George F. Riley, Georgia Tech, Summer 2003

#include <iostream>

#include "simulator.h"
#include "star.h"
#include "ipaddr.h"
#include "node.h"
#include "node-impl.h"
#include "node-real.h"
#include "node-ghost.h"
#include "interface-real.h"
#include "interface-ghost.h"
#include "linkp2p.h"
#include "link-ghost.h"
#include "validation.h"  

using namespace std;


int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Count_t n = 1000;
  if (argc > 1) n = atol(argv[1]);
  
  Simulator s;
  
  Size_t initialUsage = s.ReportMemoryUsage();
  
  cout << "Initial Memory is " << initialUsage << endl;

  // Create a line of nodes
  Node* p = new Node(1);
  for(Count_t i = 1; i < n; ++i)
    {
      Node* c = new Node(1);
      p->AddDuplexLink(c);
      p = c;
    }
  
  Size_t finalUsage = s.ReportMemoryUsage();
  
  cout << "Final   Memory is " << finalUsage
       <<  " for " << n << " ghosts " << endl;
  cout << "Topology Memory is " << finalUsage - initialUsage << endl;
  cout << "Memory per ghost is " << (finalUsage - initialUsage) / n << endl;
  cout << "Ghost node  count " << NodeGhost::count << endl;
  cout << "Ghost iface count " << InterfaceGhost::count << endl;
  cout << "Ghost link  count " << LinkGhost::count << endl;
  cout << "Real  node  count " << NodeReal::count << endl;
  cout << "Real  iface count " << InterfaceReal::count << endl;
  cout << "Real  link  count " << Linkp2p::count << endl;

  Node nd;
  NodeReal nr(&nd);
  NodeGhost ng(&nd);
  InterfaceReal ir;
  InterfaceGhost ig;
  Linkp2p lr;
  LinkGhost lg;

  cout << "Size of node       " << sizeof(nd) << endl;
  cout << "Size of nodereal   " << sizeof(nr) << endl;
  cout << "Size of nodeghost  " << sizeof(ng) << endl;
  cout << "Size of ifacereal  " << sizeof(ir) << endl;
  cout << "Size of ifaceghost " << sizeof(ig) << endl;
  cout << "Size of linkreal   " << sizeof(lr) << endl;
  cout << "Size of linkghost  " << sizeof(lg) << endl;
}
