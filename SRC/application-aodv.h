// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-aodv.h 465 2006-02-15 15:33:22Z riley $



// Georgia Tech Network Simulator - AODV Routing Protcol Application class
// George F. Riley.  Georgia Tech, Spring 2002

// Define the AODV routing protocol

#ifndef __aodv_application_h__
#define __aodv_application_h__

#include <list>
#include "assert.h"

#include "common-defs.h"
#include "application.h"
#include "udp.h"
#include "routing-aodv.h"
#include "aodv-queue.h" //for queue.

#include "timer.h" //for timers

//typedef the packet type for AODV
#define AODV_REQUEST  100
#define AODV_REPLY    101
#define AODV_HELLO    102
#define AODV_ERROR    103

typedef unsigned char   Byte; // define the byte. 
							  // Don't understand why common-defs.h use short?
#define MY_ROUTE_TIMEOUT        10     // 10 s?
#define ACTIVE_ROUTE_TIMEOUT    10     // 10 s
#define REV_ROUTE_LIFE          6      // 6 seconds
#define BCAST_ID_SAVE           6      // 6 seconds
#define HELLO_INTERVAL          1      // 1000 ms
#define ALLOWED_HELLO_LOSS      3      // 3 packets
#define MinHelloInterval        (0.75 * HELLO_INTERVAL)
#define MaxHelloInterval        (1.25 * HELLO_INTERVAL)
#define RREQ_RETRIES            3      //Number of times to do network-wide search before timeout
#define MAX_RREQ_TIMEOUT        10.0   //sec. timeout after doing network-wide search.

#define TTL_START               5      //various constants used for expanding ring search
#define TTL_THRESHOLD           7
#define TTL_INCREMENT           2

// This should be somewhat related to arp timeout
 #define NODE_TRAVERSAL_TIME     0.03   // 30 ms
 #define LOCAL_REPAIR_WAIT_TIME  0.15   //sec
 

#define NETWORK_DIAMETER        30     //should be set by user using best guess
#define AODV_MAX_ERRORS         100

//Timer events
class AODVEvent : public TimerEvent {
	public:
		// the types of the AODV timer events
		typedef enum {BROADCAST_TIMEOUT, HELLO_TIMEOUT, NEIGHBOR_TIMEOUT
				, ROUTECACHE_TIMEOUT, LOCALREPAIR_TIMEOUT}AODVTimeout_t;
	public:
		AODVEvent(Event_t ev) : TimerEvent(ev) { };
		virtual ~AODVEvent();
};


class BroadcastID {
    public: 
	     BroadcastID(IPAddr_t i, Long_t b) { src = i; id = b;  } 
    public: 
	     IPAddr_t     src;
	     Long_t       id;
	     double       expire;         // now + BCAST_ID_SAVE s
};

typedef std::list<BroadcastID *> BroadcastIDList_t;

struct AODVRequestHdr {
        Byte            Type;	    // Packet Type
        Byte            Reserved[2];
        Byte            HopCount;   // Hop Count
        Long_t          BcastId;    // Broadcast ID

        IPAddr_t        Dst;         // Destination IP Address
        Seq_t       	DstSeqNo;   // Destination Sequence Number
        IPAddr_t        Src;         // Source IP Address
        Seq_t           SrcSeqNo;   // Source Sequence Number

        double          TimeStamp;   // when REQUEST sent;
					// used to compute route discovery latency

  inline int size() { 
  int sz = 0;
  /*
  	sz = sizeof(Byte)		// rq_type
	     + 2*sizeof(Byte) 	// reserved
	     + sizeof(Byte)		// rq_hop_count
	     + sizeof(double)	// rq_timestamp
	     + sizeof(Long_t)	// rq_bcast_id
	     + sizeof(IPAddr_t)	// rq_dst
	     + sizeof(Seq_t)	// rq_dst_seqno
	     + sizeof(IPAddr_t)	// rq_src
	     + sizeof(Seq_t);	// rq_src_seqno
  */
  	sz = 7*sizeof(Long_t);
  	assert (sz >= 0);
	return sz;
  }
};

struct AODVReplyHdr {
        Byte            Type;       // Packet Type
        Byte            Reserved[2];
        Byte            HopCount;   // Hop Count
        IPAddr_t        Dst;        // Destination IP Address
        Seq_t           DstSeqNo;   // Destination Sequence Number
        IPAddr_t        Src;        // Source IP Address
        double	        LifeTime;   // Lifetime

        double          TimeStamp;  // when corresponding REQ sent;
						// used to compute route discovery latency
						
  inline int size() { 
  int sz = 0;
  /*
  	sz = sizeof(Byte)		// rp_type
	     + 2*sizeof(Byte) 	// rp_flags + reserved
	     + sizeof(Byte)		// rp_hop_count
	     + sizeof(double)	// rp_timestamp
	     + sizeof(IPAddr_t)	// rp_dst
	     + sizeof(Seq_t)	// rp_dst_seqno
	     + sizeof(IPAddr_t)	// rp_src
	     + sizeof(Long_t);	// rp_lifetime
  */
  	sz = 6*sizeof(Long_t);
  	assert (sz >= 0);
	return sz;
  }

};

struct AODVErrorHdr {
        Byte        Type;            // Type
        Byte        Reserved[2];     // Reserved
        Byte        DestCount;       // DestCount
        // List of Unreachable destination IP addresses and sequence numbers
        IPAddr_t     UnreachableDst[AODV_MAX_ERRORS];   
        Seq_t        UnreachableDstSeqNo[AODV_MAX_ERRORS];   

  inline int size() { 
  int sz = 0;
  /*
  	sz = sizeof(Byte)		// type
	     + 2*sizeof(Byte) 	// reserved
	     + sizeof(Byte)		// length
	     + length*sizeof(IPAddr_t); // unreachable destinations
  */
  	sz = (DestCount*2 + 1)*sizeof(Long_t);
	assert(sz);
        return sz;
  }

};

struct AODVRrep_ackHdr {
	Byte	rpack_type;
	Byte	reserved;
};


#define AODV_PORT 564

//static void RouteResolveCallbackAODV(Packet *pPacket, void *data);

class AODVApplication : public Application, public TimerHandler {
public:
  AODVApplication(Node*);
  AODVApplication(const AODVApplication&); // Copy constructor
  //void Handle(Event*, Time_t); // Uncomment if specific event handling needed
  // Upcalls from L4 protocol
  virtual void Receive(Packet*,L4Protocol*, Seq_t = 0);   // Data received
  // Called from application base class
  virtual void StartApp();    // Called at time specified by Start
  virtual void StopApp();     // Called at time specified by Stop
  virtual Application* Copy() const;// Make a copy of the application
public:
  UDP*          l4Proto;      // Points to the specified Layer 4 protocol
  // Must certainly need more state here
public:
  // Timer handle methods
  virtual void Timeout(TimerEvent*); // Called when timers expire
  AODVQueue		rQueue; //used to buffer data which has no route.
  
  void RouteResolve(Packet *pPacket); //call back from low layer.
  
public:
  // Timers
  Timer          timer;           // Timer for various AODV timer events
  AODVEvent*     BroadcastTimeout;  // Broadcast timer timeout event
  AODVEvent*     HelloTimeout;      // Hello timer timeout
  AODVEvent*     NeighborTimeout;   // Neighbor timer timeout
  AODVEvent*     RouteCacheTimeout; // Route cache timer timeout 
  AODVEvent*     LocalRepairTimeout;  // LocalRepair timer Timeout
  bool 			 useTimerBuckets;

  // Timer management
  void ScheduleTimer(Event_t, AODVEvent*&, Time_t);
  void CancelTimer(AODVEvent*&, bool delTimer = false); 
  void CancelAllTimers();
  
protected:
  Seq_t          SeqNo;				//Sequence number.??
  int            BroadcastId;       //Really should add this??
  BroadcastIDList_t BroadcastIdList;//TODO:
  AODVNeighborList_t  NeighborList;
  RoutingAODV	 *pAODVRouting;     //used to refer routing AODV object.

  double  PerHopTime(AODVRoutingEntry *pRt);


  //utilites to maintain.
  //Broadcast ID Management
  void BroadcastIDPurge(void);
  bool BroadcastIDLookup(IPAddr_t id, Long_t bid);
  void BroadcastIDInsert(IPAddr_t id, Long_t bid);
  
  //TODO: implement the following 3 routines in .cc file.

  //when lower layer finds link down call the following
  //TODO: about its parameter.
  void RouteLinkLayerFailed(IPAddr_t);
  void HandleLinkFailure(IPAddr_t);
		  
  void RoutePurge();
  void RouteUpdate(AODVRoutingEntry *rt, Seq_t seqnum,Word_t metric
				  ,IPAddr_t nexthop, double expire_time);
  void RouteDown(AODVRoutingEntry *rt);

  //Neighbor management.
  void NeighborPurge();
  AODVNeighbor * NeighborLookup(IPAddr_t id); 
  void NeighborInsert(IPAddr_t id);
  void NeighborDelete(IPAddr_t id);

  //Packet TX Routines
  void Forward(AODVRoutingEntry *rt, Packet *p, double delay);
  void SendHello(void);
  void SendRequest(IPAddr_t dst);

  void SendReply(IPAddr_t ipdst, Long_t hop_count,
                 IPAddr_t rpdst, Seq_t rpseq, 
				 Long_t lifetime, double timestamp);
  void SendError(struct AODVErrorHdr *p, bool jitter = true);

  // Packet RX Routines
  void RecvHello(Packet *p);
  void RecvRequest(Packet *p);
  void RecvReply(Packet *p); 
  void RecvError(Packet *p);
  
  
};

#endif

