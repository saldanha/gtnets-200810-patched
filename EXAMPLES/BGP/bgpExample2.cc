// Sample test program to instantiate a BGP router
// Sunitha Beeram, Georgia Tech, Spring 2005

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "bgp.h"


using namespace std;

int main(int argc,char ** argv)
{
  Simulator Sim;

  char *command = "show ip bgp";

  Node::DefaultShape(Node::CIRCLE);
  Linkp2p lk(Rate("500Kb"), Time("20ms"));
  Uniform startRng(0,0.1);

  /*First BGP Router*/
  Node* node1 = new Node();
  BGP* bgpRouter1 = new BGP(0);

  bgpRouter1->AttachNode(node1);
  bgpRouter1->config_file("./bgpd1.conf");
  node1->SetLocation(4,0);

  /*Second BGP Router*/
  Node *node2 = new Node();
  BGP* bgpRouter2 = new BGP(0);
  
  bgpRouter2->AttachNode(node2);
  bgpRouter2->config_file("./bgpd2.conf");
  node2->SetLocation(6,0);
  node1->AddDuplexLink(node2,lk);

  BGPEvent *ev1 = new BGPEvent(BGPEvent::COMMAND);
  ev1->argv= (char*)malloc(sizeof(char)*strlen(command));
  strcpy(ev1->argv,command);

  BGPEvent *ev2 = new BGPEvent(BGPEvent::COMMAND);
  ev2->argv= (char*)malloc(sizeof(char)*strlen(command));
  strcpy(ev2->argv,command);

  bgpRouter1->timer->Schedule(399,ev1);
  bgpRouter2->timer->Schedule(399,ev2);

  bgpRouter1->StartAt(0.1);
  bgpRouter2->StartAt(0.1);
  bgpRouter1->StopAt(400);
  bgpRouter2->StopAt(400);
  
  Sim.Progress(1);
  Sim.StopAt(400);
  Sim.Run();
}
 
