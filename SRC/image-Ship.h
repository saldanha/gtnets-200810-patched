// Generated automatically by make-gtimage from ../IMAGES/Aircraft.png
// DO NOT EDIT

#include "image.h"

class ShipImage : public Image {
public:
  ShipImage(){}
  const char* Data() const { return data;}
  int Size() const { return size;}
  operator const char*() { return data;}
  static const char* data;
  static int size;
};
