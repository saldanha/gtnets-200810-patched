// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


// Georgia Tech Network Simulator - Bluetooth Application class
// George F. Riley, Georgia Tech, Spring 2004

// Define an on/off data source application class

#ifndef __blue_application_h__
#define __blue_application_h__

#include "common-defs.h"
#include "application.h"
#include "bnep.h"
#include "node-blue.h"

class Bnep;
// Define the start/stop events
//Doc:ClassXRef
class AppBlueEvent : public Event {
public:
  typedef enum { SEND_PKT = 100, START_GEN, STOP_GEN, CHECK_CONN } AppBlueEvent_t;
  AppBlueEvent() { }
  AppBlueEvent(Event_t ev) : Event(ev) { }
};

//Doc:ClassXRef
class BlueApplication : public Application {
  //Doc:Class Defines an bluetooth application that uses the on--off data generator
  //Doc:Class model.

public:
  // Define a number of constructors for different needs
  // Endpoints, on/off rng.
  //Doc:Method
  BlueApplication(BlueNode*, BlueNode*,
                   const Random&, Bnep*, //const Bnep&,
                   Rate_t r = defaultRate,
                   Size_t s = defaultSize,
				   Count_t c = defaultCount);
    //Doc:Desc Constructor specifying both endpoint nodes, a single random
    //Doc:Desc number generator for both the {\em On} and {\em Off} 
    //Doc:Desc distributions, and a Bnep layer protocol object.  Optionally
    //Doc:Desc specifies bit rate (when {\em On} (defaults to a globally
    //Doc:Desc specified default rate), and optionally a packet size
    //Doc:Desc (defaults to a globally specified default size).  See
    //Doc:Desc {\tt SetDefaultRate} and {\tt SetDefaultSize} below.
    //Doc:Arg1 Pointer to first node endpoint
    //Doc:Arg2 Pointer to second node endpoint
    //Doc:Arg3 Random number generator to use for both {\em On} and {\em Off}
    //Doc:Arg3 time periods.
    //Doc:Arg4 A Bnep layer protocol object to copy.
    //Doc:Arg5 Data rate to generate when {\em On}.
    //Doc:Arg6 Packet size.

  // Endpoints, on rng, off rng, Bnep protocol
  //Doc:Method
  BlueApplication(BlueNode*, BlueNode*,
                   const Random&, const Random&,
                   Bnep*, //const Bnep&,
                   Rate_t r = defaultRate,
                   Size_t s = defaultSize,
				   Count_t c = defaultCount);
    //Doc:Desc Constructor specifying both endpoint nodes, a random
    //Doc:Desc number generator for the {\em On} and a second
    //Doc:Desc random number generator for the {\em Off} 
    //Doc:Desc distribution, and a Bnep layer protocol object.  Optionally
    //Doc:Desc specifies bit rate (when {\em On} (defaults to a globally
    //Doc:Desc specified default rate), and optionally a packet size
    //Doc:Desc (defaults to a globally specified default size).  See
    //Doc:Desc {\tt SetDefaultRate} and {\tt SetDefaultSize} below.
    //Doc:Arg1 Pointer to first node endpoint
    //Doc:Arg2 Pointer to second node endpoint
    //Doc:Arg3 Random number generator to use for {\em On} period.
    //Doc:Arg4 Random number generator to use for {\em Off} period.
    //Doc:Arg5 A Bnep layer protocol object to copy.
    //Doc:Arg6 Data rate to generate when {\em On}.
    //Doc:Arg7 Packet size.

  BlueApplication(const BlueApplication&); // Copy constructor
  
  ~BlueApplication() {
	  if(offTime!=NULL)
		  delete offTime;
	  if(onTime!=NULL)
		  delete onTime;
  }
  void Handle(Event*, Time_t);
  virtual void StartApp();    // Called at time specified by Start
  virtual void StopApp();     // Called at time specified by Stop
  //virtual void AttachNode(BlueNode*); // Note which node attached to
  virtual Application* Copy() const;// Make a copy of the application
  /*
  virtual L4Protocol*  GetL4() const { return l4Proto;}
  virtual L4Protocol*  GetLocalProto() { return l4Proto;}
  virtual L4Protocol*  GetRemoteProto() { return peerProto;}
  */
  //Doc:Method
  virtual void MaxBytes(Count_t m) { maxBytes = m;}
    //Doc:Desc Specify a maximum number of bytes to send, after which the
    //Doc:Desc application shuts down.
    //Doc:Arg1 Maximum number of bytes to send.

  //stat method
  void StatOutput();
public: // Static methods
  //Doc:Method
  static void DefaultRate(Rate_t r) { defaultRate = r;}
    //Doc:Desc Specify a default data rate to use for all On/Off applications
    //Doc:Desc by default
    //Doc:Arg1 The default data rate (when the application is {\em On}.

  //Doc:Method
  static void DefaultSize(Size_t s) { defaultSize = s;}
    //Doc:Desc Specifies a default packet size to use for all
    //Doc:Desc On/Off applcations
    //Doc:Desc by default.
    //Doc:Arg1 The default packet size.

  static void DefaultCount(Count_t c) { defaultCount = c;}

public:
  /*
  L4Protocol*   l4Proto;      // Points to the specified Layer 4 protocol
  L4Protocol*   peerProto;    // Points to l4 proto object on node 2
  IPAddr_t      peerIP;       // PeerIP
  PortId_t      peerPort;     // Peer port
  */
  Bnep*   		bnep;      // Points to the specified Bnep protocol
  Bnep*   		peerProto;    // Points to Bnep proto object on node 2
  BdAddr	    peerBdAddr;       // Peer BdAddr
  
  bool          connected;    // True if connected
  Random*       onTime;       // rng for On Time
  Random*       offTime;      // rng for Off Time
  Rate_t        cbrRate;      // Rate that data is generated
  Size_t        pktSize;      // Size of packets
  AppBlueEvent*   pendingEvent; // Pending SendPkt event
  AppBlueEvent*   pendingBlue;    // Pending on or off event
  Size_t        residualBits; // Number of generated, but not sent, bits
  Time_t        lastStartTime;// Time last packet sent
  Count_t       maxBytes;     // Limit total number of bytes sent
  Count_t       totBytes;     // Total bytes sent so far
public:
  static Rate_t defaultRate;
  static Size_t defaultSize;
  static Count_t defaultCount;
private:
  void ConstructorHelper(BlueNode*, BlueNode*);
  void ScheduleNextTx();
};

#endif

