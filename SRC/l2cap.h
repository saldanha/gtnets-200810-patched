// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


// Georgia Tech Network Simulator - Bluetooth L2CAP class
// George F. Riley, Georgia Tech, Spring 2004

#ifndef __l2cap_h
#define __l2cap_h

#include <assert.h>

#include <list>
#include <iostream> //for cout
#include "bluetus.h"
#include "baseband.h"
#include "bnep.h"

//Signalling channel ID
#define SIGNAL_CHANNEL 			0x0001
//Connection-less channel ID
#define CONNLESS_CHANNEL		0x0002

//Command reject reason
#define REASON_NOTUNDERSTAND	0x0000
#define REASON_MTUEXCEED		0x0001
#define REASON_INVALIDCID		0x0002

//PSM values
#define SDP					0x0001
#define RFCOMM				0x0003
#define TCS_BIN				0x0005
#define TCS_BIN_CORDLESS	0x0007
#define BNEP				0x000F
#define HID_CONTROL			0x0011
#define HID_INTERRUPT		0x0013
#define UPNP				0x0015
#define AVCTP				0x0017
#define AVDTP				0x0019
#define UDI_C_PLANE			0x001D


//signalling command codes
#define COMMAND_RESERVED 		0x00 
#define COMMAND_REJECT			0x01
#define COMMAND_CONN_REQ		0x02
#define COMMAND_CONN_RESP		0x03
#define COMMAND_CFG_REQ			0x04
#define COMMAND_CFG_RESP		0x05
#define COMMAND_DISCONN_REQ		0x06
#define COMMAND_DISCONN_RESP	0x07
#define COMMAND_ECHO_REQ		0x08
#define COMMAND_ECHO_RESP		0x09
#define COMMAND_INFO_REQ		0x0A
#define COMMAND_INFO_RESP		0x0B


//Connection oriented channel state

// The timer bound is 10, request can try 5 times
// before giving up.
//
#define TIMEOUT_VALUE			10 //10 seconds.
#define MAX_TRY					5

using namespace std;

typedef enum {CLOSED, W4_L2CAP_CONNECT_RSP, W4_L2CA_CONNECT_RSP, CONFIG,
		OPEN,  W4_L2CAP_DISCONNECT_RSP, W4_L2CA_DISCONNECT_RSP
	     } L2CAP_state;


//packet format of L2CAP within a connection-oriented channel
//
struct L2CAPPacket {
	uShort usLength;
	uShort usChannelID;
	uChar ucPayload[1];

	int HdrSize() {
		return 2*sizeof(uShort);
	}
};

//structure related to Signalling
struct CommandPacket {
	uChar ucCode;
	uChar ucID;
	uShort usLength;
	uChar ucData[1];

	int HdrSize(){
		return 2*sizeof(uChar) + sizeof(uShort);
	}
};

struct CmdReject {
	uShort usReason;
	//optional data follows if any
};
//** COMMAND REJECT**
// code: 0x01;     nLength>=0x0002; pData contains reason + data(optional)
// Reason: 0x0000 command not understood; 0x0001 signalling MTU exceeded;
//         0x0002 invalid CID in request; other reserved.
// Data:   reason==0x0000 --> N/A
// 		   reason==0x0001 --> data length is 2 octs, actual MTU
// 		   reason==0x0002 --> data length is 4 octs, requested CID;

struct CmdConnReq {
	uShort usPSM;
	uShort usSrcCID;
};
//** CONNECTION REQUEST**
// code: 0x02;    nLength>=0x0004; pData contains PSM + SourceID
// PSM:  (Protocol/Service Multiplexor) at least 2 octs.
// 		   0x0001 Service Discovery Protocol
// 		   0x0003 RFCOMM
// 		   0x0005 Telephony COntrol Protocol
// 		   <0x100 RESERVED.
// SourceID: source local ID represents a channel endpoint on the device 
//         sending the request.

struct CmdConnResp {
	uShort usDstCID;
	uShort usSrcCID;
	uShort usResult;
	uShort usStatus;
};
//** CONNECTION RESPSONSE**
// code: 0x03;    nLength=0x0008; pData contains Dest CID + SRC CID + Result +Status
// Dest CID: contains the channel endpoint on the device receiving the request and
// 			 sending the response.
// SRC CID:  contains the channel endpoint to receive this response packet.
// Result:   2 octs
// 		     0x0000 connection successful
// 			 0x0001 connection pending
// 			 0x0002 connection refused --PSM not supported
// 			 0x0003 connection refused --Security block
// 			 0x0004 connection refused --no resources available
// 			 other  Reserved.
// Status:   2 octs
// 			 0x0000 no further informatin available
// 			 0x0001 authentication pending
// 			 0x0002 authorization pending
// 			 other  Reserved.

struct CmdCfgReq {
	uShort usDstCID;
	uShort usFlags;
	//options follows if any
};

struct CfgParamOpt {
	uChar ucType;
	uChar ucLength;
};
#define TYPE_MTU		0x01
#define TYPE_FLUSHTIMEO	0x02
#define TYPE_QOS		0x03

struct QosOpt {
	uChar ucFlags; //reserved. set 0;
	uChar ucServiceType;
	
	uLong ulTokenRate;
	uLong ulTokenBucketSize;
	uLong ulPeakBandw;
	uLong ulLatency;
	uLong ulDelayVar;
};
//** CONFIGURATION REQUEST**
//code: 0x04; nLength>=0x0004; pData contains Dest CID + Flags + Options
//Dest CID:  2 octs,contains the channel end-point on the device receiving this Request
//Flags:     2 octs,only LSB is used. C-- continuation flag.
//Options:   the list of the parameters and their values to be negotiated.
//		##configuration formate: LSB---->MSB
//							type(1 oct) length(1 oct) option data(depends on length)
//					MTU:    0x01        2             MTU
//		Flush timeout:      0x02        2             flush timeout(ms)
//					QoS:    0x03		22			  Flags(1)+ServiceType(1)+TokenRate(4)
//													  +TokenBucketSize(4)+PeakBandWidth(4)+
//													  Latency(4)+DelayVariation(4)
//	    in which ServiceType: 0x00 no traffic; 0x01 Best effort; 0x02 Guaranteed
//	    					  other reserved.
//		

struct CmdCfgResp {
	uShort usSrcCID;
	uShort usFlags;
	uShort usResult;
	uShort usConfig;
};
//** CONFIGURE RESPONSE**
//code: 0x05;    nLength>=0x0006; pData contains SRC CID + Flags + Result + Config
//SRC CID:   2 octs,contains the channel end-point on the device receiving this Response
//Flags:     2 octs,only LSB is used. C-- more configurgation responses will follow 
//			 when set to 1
//Result:    2 octs, indicates whether or not the request was acceptable.
//           0x0000 Success
//           0x0001 Failure --unacceptable parameters
//           0x0002 Failure --rejected(no reason provided)
//           0x0003 Failure --unknown options
//           other  Reserved
//Config:    contains configuration options being negotiated.

struct CmdDisconnReq {
	uShort usDstCID;
	uShort usSrcCID;
};
//** DISCONNECTION REQUEST**
//code: 0x06;    nLength=0x0004; pData contains Dest CID + SRC CID
//Dest CID:  2 octs,contains the end-point of the channel to be shutdown on the device 
//			 receiving this request
//SRC CID:   2 octs, contains the end-point of the channel on the device sending this 
//	         request

struct CmdDisconnResp {
	uShort usDstCID;
	uShort usSrcCID;
};
//** DISCONNECTION RESPONSE**
//code: 0x07;    nLength=0x0004; pData contains Dest CID + SRC CID
//Dest CID:  2 octs, identifies the channel end-point on the device sending the response
//SRC CID:   2 octs, identifies the channel end-point on the device receiving the resps.


//** ECHO REQUEST**
//code: 0x08;     nLength>=0; pData is optional

//** ECHO RESPONSE**
//code: 0x09;     nLength>=0; pData is optional

struct CmdInfoReq {
	uShort usInfoType;
};
//** INFORMATION REQUEST**
//code: 0x0a;     nLength=0x0002; pData contains Infotype
//InfoType:   0x0001 connectionless MTU
//			  other Reserved

struct CmdInfoResp {
	uShort usInfoType;
	uShort usResult;
	//options follows
};	
//** INFORMATION RESPONSE**
//code: 0x0b;     nLength>=0x0004; pData contains InfoType + Result + Data(optional)
//Infotype:   2 octs, the same as Request
//Result:     2 octs. 0x0000 Success
//					  0x0001 Not supported
//					  other  Reserved
//Data:       0 or more octs. if infoType==0x0001, Data contains connectionless MTU and 
//			  data length is 2 octs.

#define MAX_QUEUE_LEN	128


//Timer events
class L2CAPTimerEvent : public TimerEvent {
	public:
		// the types of the L2CAP timer events
		typedef enum {CONNREQ_TIMEOUT, CFGREQ_TIMEOUT, DISCONNREQ_TIMEOUT
				} L2CAPTimerEvent_t;
	public:
		L2CAPTimerEvent(Event_t ev, uChar ucId) 
				: TimerEvent(ev) {
							ucRequestId = ucId;
				}
		virtual ~L2CAPTimerEvent() {}
		
		uChar ucRequestId;    	 //the request command identifier.
};

typedef std::list<L2CAPPacket *> L2CAPPacketList;

class L2capQueue {
public:
	L2capQueue(){ usQueLen = 0;}
	bool Enque(L2CAPPacket *pPacket); //pPacket is pass-in parameter.
	bool DeQue(L2CAPPacket *pPacket); //pPacket is output parameter.
	~L2capQueue() {
	}
private:
	uShort usQueLen;
	L2CAPPacketList L2capPktList;
};


//L2CAP channel.
class L2capChannel {
public:
	L2capChannel(uShort usRemoteChannelID = 0x0000,
				uShort usLocalChannelID = 0x0000) {
		pRxQueue = NULL;
		this->usRemoteChannelID = usRemoteChannelID;
		this->usLocalChannelID = usLocalChannelID;
		pLocalAddr = NULL;
		pRemoteAddr = NULL;
	}
	
	virtual ~L2capChannel() {
		delete pLocalAddr;
		delete pRemoteAddr;
		if(pRxQueue) 
			delete pRxQueue;
	}
	
	virtual uShort DataIndication(L2CAPPacket *pPacket) = 0;
	
	void SetLocalChannelID(uShort usLocalChannelID) {
		this->usLocalChannelID = usLocalChannelID;
		return;
	}
	
	uShort GetLocalChannelID() {
		return usLocalChannelID;
	}
	void SetRemoteChannelID(uShort usRemoteChannelID) {
		this->usRemoteChannelID = usRemoteChannelID;
		return;
	}
	uShort GetRemoteChannelID() {
		return usRemoteChannelID;
	}

	BdAddr * GetRemoteAddr() {
		assert(pRemoteAddr!=NULL);
		return pRemoteAddr;
	}
	void SetRemoteAddr(BdAddr addr) {
		if(pRemoteAddr)
			delete pRemoteAddr;
		pRemoteAddr = new BdAddr;
		memcpy(pRemoteAddr, &addr, sizeof(BdAddr));
		return ;
	}
	
	BdAddr * GetLocalAddr() {
		assert(pLocalAddr!=NULL);
		return pLocalAddr;
	}
	void SetLocalAddr(BdAddr addr) {
		if(pLocalAddr)
			delete pLocalAddr;
		pLocalAddr = new BdAddr;
		memcpy(pLocalAddr, &addr, sizeof(BdAddr));
		return ;
	}
	
	virtual uShort GetMTU() {
		return 0;
	}	
public:
	uShort usOutMTU; //outgoing MTU 
	uShort usInFlushTO;// incoming flush timeout
	
private:
	uChar ucChannelType; //conn-oriented, conn-less and signalling
	
	BdAddr 	*pLocalAddr;
	BdAddr  *pRemoteAddr; //remote device address.

	L2capQueue 	*pRxQueue;

	//local and remote ChannelID;
	uShort usRemoteChannelID;
	uShort usLocalChannelID;

};

//360 seconds later, it can be reused.
struct L2capCmdId {
	uChar ucIdentifier;
	//uChar ucState; // 1 "remote request" answered : 0 "remote request" not answered; 
						   // 3 "local request" answered : 2 "local request" not answered.
	double dExpire; //after this time it can be recyled.
};

typedef std::list<L2capCmdId> L2capCmdIdList;

struct ConfigPara {
	uShort usCID;
	uShort usInMTU;
	void *pOutFlow;
	uShort usOutFlushTO;
	uShort usLinkTO;
	uShort *pInMTU_O;
	void *pOutFlow_O;
	uShort *pOutFlushTO_O;
};	
	
//
//Class Signalling channel
//
class L2cap;
class L2capConnChannel;

class L2capSignalChannel: public L2capChannel{

public:	
	//constructor
	L2capSignalChannel(L2cap *pL2CAP = NULL);
	//destructor
	~L2capSignalChannel();

	//define an empty virtual function
	virtual uShort DataIndication(L2CAPPacket *pPacket) {return 0x0000;};
	//receive packet from lower layer.
	uShort DataIndication(L2CAPPacket *pPacket, L2capConnChannel *pChannel);
	
	//send signal packet, called by upper layer.
	uShort DataRequest(void *pData);

	//CmdID manage function.
	bool CmdIdRemove(L2capConnChannel *pChannel, uChar ucId);
	void CmdIdPurge(L2capConnChannel *pChannel);
	bool CmdIdLookup(L2capConnChannel *pChannel, uChar ucId);
	L2capCmdId CmdIdAlloc(L2capConnChannel *pChannel);
	bool CmdIdInsert(L2capConnChannel *pChannel, uChar ucID);

	virtual uShort GetMTU() {
		return usMTU;
	}	
	//Helper function.
	uShort BuildCmd(uChar code, uChar identifier, 
			uShort length, void *data, void *commandBuf);

	uShort L2CA_ConnectReq(uShort PSM, BdAddr BD_Addr, uShort *pLCID, 
					uChar ucId, uShort *pStatus, 
					L2capConnChannel *pChannel);
	uShort L2CA_ConnectRsp(BdAddr BD_Addr,uChar ucIdentifier,uShort 
					LCID, uShort response, uShort status);
	uShort L2CA_ConfigReq(uShort CID, uShort InMTU,void *pOutFlow, 
					uShort OutFlushTO, uShort LinkTO,uShort *pInMTU_O,
				   	void *pOutFlow_O,uShort *pOutFlushTO_O,uChar ucId,
					L2capConnChannel *pChannel);
	uShort L2CA_ConfigRsp(uShort LCID, uShort OutMTU,uChar ucIdentifier,
				   	uShort Result, void *pInFlow,
				   	L2capConnChannel *pChannel);
	uShort L2CA_DisconnectReq(uShort usLocalCID, uChar ucId);
	uShort L2CA_DisconnectRsp(uShort usLocalCID, uChar ucIdentifier);

	uShort Dispatch(CommandPacket *pCommand, L2capConnChannel *pChannel);
	//Action of receiving signalling.
	uShort RecvReject(CommandPacket *pCommand);
	uShort RecvConnReq(CommandPacket *pCommand);
	uShort RecvConnResp(CommandPacket *pCommand);
	uShort RecvCfgReq(CommandPacket *pCommand);
	uShort RecvCfgResp(CommandPacket *pCommand,L2capConnChannel *pChannel);
	uShort RecvDisconnReq(CommandPacket *pCommand);
	uShort RecvDisconnResp(CommandPacket *pCommand);
	
	void SaveCfgPara(uShort CID, uShort InMTU, void *pOutFlow,
					uShort OutFlushTO, uShort LinkTO, uShort *pInMTU_O,
					void *pOutFlow_O, uShort *pOutFlushTO_O) {
		CfgParameter.usCID = CID;
		CfgParameter.usInMTU = InMTU;
		CfgParameter.pOutFlow = pOutFlow;
		CfgParameter.usOutFlushTO = OutFlushTO;
		CfgParameter.usLinkTO = LinkTO;
		CfgParameter.pInMTU_O = pInMTU_O;
		CfgParameter.pOutFlow_O = pOutFlow_O;
		CfgParameter.pOutFlushTO_O = pOutFlushTO_O;
		return;
	}
	
	ConfigPara GetCfgPara() {
		return CfgParameter;
	}
	//ConfigureMTU
private:
	L2cap *pL2CAP; //pointer back to L2CAP entity.
	uShort usMTU; 
	
	ConfigPara CfgParameter;
	L2capQueue 	*pTxQueue;
};

//
//Class Connection oriented channel
//
class L2capConnChannel: public L2capChannel , public TimerHandler {

public:
#define DEFAULT_MTU		672
#define DEFAULT_TIMEO	0xFFFF //msec
	//constructor	
	L2capConnChannel(uShort usPSM = 0x0000,uShort usOutMTU = DEFAULT_MTU,
					 uShort usInFlushTO = DEFAULT_TIMEO,
					 uShort usInMTU = DEFAULT_MTU,
					 uShort usOutFlushTO = DEFAULT_TIMEO);
	//destructor
	~L2capConnChannel();

	//called by L2CAP::Demultiplxr	
	int Demultiplxr(L2CAPPacket *pPacket);

	//receive packet from lower layer.
	virtual uShort DataIndication(L2CAPPacket *pPacket);
	
	virtual uShort GetMTU() {
		return usInMTU;
	}	
	//send packet
	uShort DataRequest(uShort usLength, void *pData);

	L2CAP_state usState; //channel state.
	uShort usCfgState; //configure state.
	
	L2cap *pL2CAP ; //pointer back to L2CAP entity.
	L2capSignalChannel *pSignalChannel; //pointer to signal channel
	
#define ILLEGAL_COMMAND_ID	0x00
	uChar ucPendingCmdId; //Before processing the current request, no other allows!
	
	L2capCmdIdList CmdIdList; //command Identifier list for this channel.
	
public:
	Timer timer; //timer for various timer event in signalling.
	
	L2CAPTimerEvent *pConnReqTimeout;
	L2CAPTimerEvent *pCfgReqTimeout;
	L2CAPTimerEvent *pDisconnReqTimeout;

  	// Timer handle methods
	virtual void Timeout(TimerEvent*); // Called when timers expire	
	// Timer management
	void ScheduleTimer(Event_t, uChar, L2CAPTimerEvent*&, Time_t);
	void CancelTimer(L2CAPTimerEvent*&, bool delTimer = false);
	void CancelAllTimers();	

public:
	//max retransmit of a signalling request before terminate
	//the channel
	uChar ucMaxRetryReq; // 5 in our case.
	
private:		
	L2capQueue 	*pTxQueue;
	
	uShort usPSM; //Protocol/Service Multiplextor


	//options
	uShort usInMTU; //incoming MTU
	uShort usOutFlushTO;//outgoing flush timeout

	
	//QoS only for connection.
	QosOpt RequestQoS;  //local QoS, to negotiated with remote peer.
	QosOpt RequiredQoS; //remote QoS, proposed by remote entity

};

struct AssembleBuf {
	uShort usTotalLen;
	uShort usOccupied;
	uShort usChannelID;
	uChar *pBuf;
};

typedef std::list<L2capConnChannel *> L2capChannelList;

class BaseBand;
class Bnep;

// class L2cap.
class  L2cap {
public:
	L2cap();
	 
	~L2cap();
		
public:

	uShort DataIndication(uChar ucLogicChannel, uChar ucFlow,
					uShort usLen, uChar *pData);

	void Segmentation(uShort usLen, uChar *pData);
	void Reassembly();

	void DataRequest(L2CAPPacket *pPacket);
	uShort DataRequest(uShort usLen, uShort usLocalCID, void *pData);

	//int Demultiplxr(L2capChannel *pChannel, L2CAPPacket *pPacket);

	//Build and send L2CapPacket.
	uShort SendL2CAPPacket(uShort usLength, uShort usChannelID,
				void * payload, L2capConnChannel *pChannel);


	uShort GetMTU(uShort usChannelID) {
		L2capConnChannel *pChannel;
		pChannel = ChannelLookupByLocalCID(usChannelID);
		return pChannel->usOutMTU;
	}
	uShort GetFlushTimeout(uShort usChannelID) {
		L2capConnChannel *pChannel;
		pChannel = ChannelLookupByLocalCID(usChannelID);
		return pChannel->usInFlushTO;
	}
	//routines related to channel id
	uShort AllocateChannelID();
	bool ChannelInsert(L2capConnChannel *pChannel);
	L2capConnChannel * ChannelLookupByLocalCID(uShort usLocalChannelID);
	L2capConnChannel * ChannelLookupByRemoteCID(uShort usRemoteChannelID);
	bool ChannelClose(uShort usChannelID);

	uShort L2CA_ConnectReq(uShort PSM, BdAddr BD_Addr,
			        uShort *pLCID, uShort *pStatus);
	uShort L2CA_ConnectRsp(BdAddr BD_Addr,uChar ucIdentifier,
		            uShort LCID, uShort response, uShort status);
	
	uShort L2CA_ConfigReq(uShort CID, uShort InMTU, void *pOutFlow, 
					uShort OutFlushTO, uShort LinkTO, uShort *pInMTU_O,
				   	void *pOutFlow_O, uShort *pOutFlushTO_O);
	uShort L2CA_ConfigRsp(uShort LCID, uShort OutMTU, uChar ucId, void *pInFlow, 
					uShort result);
	uShort L2CA_DisconnectReq(uShort CID);
	uShort L2CA_DisconnectRsp(uShort usLocalCID, uChar ucIdentifier);

	void L2CA_DataWriteReq(uShort usCID, uShort usLength,
	                void *OutBuffer);
	
	uShort L2CA_DataWriteCfm(uLong *size );
	uShort L2CA_DataRead(uLong CID, uLong length, void *pInBuffer, uLong *pN);
	uShort L2CA_GroupCreate(uLong PSM);
	uShort L2CA_GroupClose(uLong CID);
   	uShort L2CA_GroupAddMember(uLong CID, BdAddr BD_Addr);
	uShort L2CA_GroupRemoveMember(uLong CID, BdAddr BD_Addr);
	uShort L2CA_GroupMembership(uLong CID, uLong *pN,
					                void *BD_Addr_List);
	uShort L2CA_Ping(BdAddr BD_ADDR, uLong length,void *pEchoData,
					                uLong size);
	uShort L2CA_DisableCLT(uLong PSM);
	uShort L2CA_EnableCLT(uLong PSM);
  
	void AttachLMP(LMP *pLMPStack) {
		if((pLMP==NULL)&&(pLMPStack!=NULL))
			pLMP = pLMPStack;
		else
			cout<<"LMP already attached or parameter invalid."<<endl;
	}
#define UPPER_PROTOCOL	Bnep
#define LOWER_PROTOCOL	BaseBand
	Bnep  * pUpperProtocol;  // upper layer protocol pointer.
	BaseBand  * pLowerProtocol; // lower layer protocol pointer.
	LMP *pLMP; //point to LMP protocol object
	 
/*
#define UPPER_PROTOCOL	Bnep
#define LOWER_PROTOCOL	BaseBand
	UPPER_PROTOCOL  * pUpperProtocol;  // upper layer protocol pointer.
	LOWER_PROTOCOL  * pLowerProtocol; // lower layer protocol pointer.
*/

private:

	AssembleBuf	assembleBuf;
	Node *	pNode; //local bluetooth node.
	BdAddr	localAddr; //can be obtained by pNode->GetLocalAddr;
	
	//receive event from lower layer
	int EventIndication(Event_t);

	//TODO: ACL link or link state?
	
	L2capSignalChannel *pSignalChannel; //Signal channel pointer.
	
	L2capChannelList ChannelList; //for connection-oriented channel only!
};

#endif
