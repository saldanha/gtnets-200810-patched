// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: l2proto802.11.h 515 2007-10-02 13:18:51Z johnece $



// Georgia Tech Network Simulator - Layer 2 802.11 class
// George F. Riley.  Georgia Tech, Spring 2002

#ifndef __l2proto_802_11_h
#define __l2proto_802_11_h
#include <sys/types.h>
#include <string>
#include <iostream>

#include "common-defs.h"
#include "math.h"
#include "l2proto.h"
#include "tfstream.h"
#include "macaddr.h"
#include "timer.h"
#include "rng.h"
#include "interface-wireless.h"

#define MAC_ProtocolVersion     0x00

#define MAC_Type_Management     0x00
#define MAC_Type_Control        0x01
#define MAC_Type_Data           0x02
#define MAC_Type_Reserved       0x03

#define MAC_Subtype_RTS         0x0B
#define MAC_Subtype_CTS         0x0C
#define MAC_Subtype_ACK         0x0D
#define MAC_Subtype_DATA        0x00

using namespace std;

class NotifyHandler;

//Doc:ClassXRef
class MACTimerEvent : public TimerEvent {
  //Doc:Class The class {\tt MACTimerEvent} defines the MAC timer events for
  //Doc:Class the 802.11 class which are essential for managing the various
  //Doc:Class timeouts
public:
  // The types of 802.11 timer events
  typedef enum { BACKOFF_TIMEOUT,
		 NAV_TIMEOUT,
		 CTS_TIMEOUT,
                 ACK_TIMEOUT,
		 DATA_TIMEOUT,
                 ASSOCIATION_TIMEOUT,
                 LH_TIMEOUT,
                 BEACON_TIMEOUT} MACTimeout_t;
public:
  //Doc:Method
  MACTimerEvent(Event_t ev) : TimerEvent(ev) { };
  //Doc:Desc This is the default constructor for this class
  //Doc:Arg1 The event type object

  virtual ~MACTimerEvent() {}
public:
  MACAddr xmt;  // mac address of the mobile host
};

//Doc:ClassXRef
class MACEvent : public Event {
  //Doc:Class The class {\tt MACEvent} defines all other events that are
  //Doc:Class associated with the packet itself.
public:
  typedef enum { PACKET_TX, 
                 BACKOFF, 
                 SEND_PROBE, 
                 CFP_BEGIN, 
                 CFP_END } MACEvent_t;
public:
  //Doc:Method
  MACEvent() : p(nil) {}
  //Doc:Desc This method is the default constructor

  //Doc:Method
  MACEvent(Event_t ev, Packet* pkt=nil)
    //: Event(ev) { p = pkt ? pkt->Copy() : nil; }
    : Event(ev), p(pkt) {}
  //Doc:Desc This constructor creates a MACEvent object out of an {\tt Event_t}
  //Doc:Desc type and an associated packet object
  //Doc:Arg1 Event_t ev
  //Doc:Arg2 Pointer to the associated packet object

public:
  Packet* p;
};

#define SEQ_CACHE_SIZE	10

class SeqCacheEntry {
public:
  SeqCacheEntry() : ta(MACAddr::NONE), sn(0) {}
  SeqCacheEntry(MACAddr mac, Word_t no) : ta(mac), sn(no) {}

public:
  MACAddr ta;  // tx address, empty slot if NONE
  Word_t  sn;  // sequence number
};

/* right now we just store distance but
   additional info can be stored if required later */
typedef std::map<IPAddr_t, MACAddr> BridgeTable_t;
//typedef std::map<MACAddr, double> BeaconCache_t;

/*
class LHPair {
 public:
  LHPair() : lastHeard(0), lhTimeout(nil) {}
  LHPair(double lh): lastHeard(lh), lhTimeout(nil) {}
 public:
  double lastHeard;
  MACTimerEvent* lhTimeout;
};
typedef std::map<MACAddr, LHPair*> LastHeardMap_t;
*/

typedef std::map<MACAddr, MACTimerEvent*> LastHeardMap_t;


//Doc:ClassXRef
class L2Proto802_11 : public L2Proto, public TimerHandler, public Handler {
  //Doc:Class The {\tt L2Proto802_11 } class derives from the {\tt L2Proto}
  //Doc:Class class and defines the behavior of the 802.11 MAC protocol

public:
  //Doc:Method
  L2Proto802_11(); // Constructor
  //Doc:Desc This method is the default constructor

  //Doc:Method
  virtual ~L2Proto802_11();
  //Doc:Desc This method is the default destructor

  //Doc:Method
  virtual void BuildDataPDU(MACAddr, MACAddr, Packet*);
  //Doc:Desc This method builds a Data PDU using the packet that the upper
  //Doc:Desc layers hand over before pushing it in the interface queue.
  //Doc:Arg1 Source MAC address
  //Doc:Arg2 Destination MAC Address

  //Doc:Method
  virtual void DataRequest(Packet*);
  //Doc:Desc This member function is called by the higher layer requesting
  //Doc:Desc a transfer of  data to a peer.
  //Doc:Arg1 The packet to be sent

  //Doc:Method
  virtual void DataIndication(Packet*, LinkEvent* = nil);// from lower layer
  //Doc:Desc This method is called when the direction of protocol stack
  //Doc:Desc traversal is upwards, i.e, a packet has just been received
  //Doc:Desc at the underlying link
  //Doc:Arg1 The packet that has been received

  //Doc:Method
  virtual bool Busy();                     // True if proto/link is busy
  //Doc:Desc Check if the protocol is busy doing processing
  //Doc:Return true if busy; false otherwise

  bool VcsBusy();
  bool PcsBusy();
  bool MediumBusy();

  //Doc:Method
  L2Proto* Copy() const;
  //Doc:Desc Check if the protocol is busy doing processing
  //Doc:Return true if busy; false otherwise

  //Doc:Method
   bool IsWireless() const { return true;}
    //Doc:Desc Query if this protocol needs a wireless interface.
    //Doc:Return Always returns true

  //Doc:Method
  bool SendRTS(MACAddr , MACAddr , Word_t dur, bool imm=false);
  //Doc:Desc This method builds and sends the RTS frame with a give src and
  //Doc:Desc dest MAC address with a given duration
  //Doc:Arg1 source MAC Address
  //Doc:Arg2 destination MAC Address
  //Doc:Arg3 duration in microsecs
  //Doc:Arg4 Donot wait for difs if true
  //Doc:Return true if send is successful

  //Doc:Method
  bool SendCTS(MACAddr, Word_t dur );
  //Doc:Desc This method builds and sends the RTS frame with a give src and
  //Doc:Desc dest MAC address with a given duration
  //Doc:Arg1 destination MAC Address
  //Doc:Arg2 duration in microsecs
  //Doc:Return true if send is successful

  //Doc:Method
  bool SendDATA(bool imm=false);
  //Doc:Desc This method  sends the DATA Frame
  //Doc:Arg1 Do not wait for ifs if true
  //Doc:Return true if send is successful

  //Doc:Method
  bool SendACK(MACAddr );
  //Doc:Desc This method sends the ACK message
  //Doc:Arg1 The destination MAC address
  //Doc:Return true if send is successful

  //Doc:Method
  bool SendBeacon();
  //Doc:Desc This method sends periodic beacon messages in the interface is
  //Doc:Desc part of an access point
  //Doc:Return Return true on a successful send
  
  //Doc:Method
  bool SendAssocReq(Time_t time, MACAddr dst);
  //Doc:Desc This method sends an association request message in the interface is
  //Doc:Desc part of a wireless endHost
  //Doc:Return Return true on a successful send

  //Doc:Method
  bool SendAssocResp(Time_t time, MACAddr dst );
  //Doc:Desc This method sends an association response message in the interface is
  //Doc:Desc part of an access point
  //Doc:Return Return true on a successful send

  //Doc:Method
  bool SendDisassocFrame(Time_t time);
  //Doc:Desc This method sends an disassociation message in the interface is
  //Doc:Desc part of an access point
  //Doc:Return Return true on a successful send

  //Doc:Method
  bool SendDisassocFrame(MACAddr addr_, Time_t time);
  //Doc:Desc This method sends an disassociation message in the interface is
  //Doc:Desc part of an access point
  //Doc:Return Return true on a successful send

  //Doc:Method
  bool SendReassocReq(Time_t time, MACAddr dst);
  //Doc:Desc This method sends an re-association request message in the interface is
  //Doc:Desc part of a wireless endHost
  //Doc:Return Return true on a successful send

  //Doc:Method
  void ForcedDisassociation(MACAddr);
    //Doc:Desc Called when RTS/CTS or DATA/ACK times out.
    //Doc:Desc In BSS mode, will cancel any current BS asociation.
    //Doc:Desc In HOSTAP mode, will remove the mac from the BTV.
    //Doc:Arg1 MACAddress of destination that failed.

  // To wait for an IFS (DIFS/EIFS) before sending the packet
  //Doc:Method
  bool ScheduleSendAfterIFS(Packet* p, Time_t t);
  //Doc:Desc This method schedules an actual sending of the frame after a given
  //Doc:Desc time duration. (generally after either SIFS/DIFS).
  //Doc:Arg1 The Packet to be sent
  //Doc:Arg2 The time to wait before sending
  //Doc:Return true if send is successful

  // The Timerhandler method << copying from tcp.h
  //Doc:Method
  void Timeout(TimerEvent*);
  //Doc:Desc This method is the timeout handler for the timer event passed as
  //Doc:Desc the arguement
  //Doc:Arg1 the timer event

  //Doc:Method
  void ScheduleTimer(Event_t, MACTimerEvent*&, Time_t);
  //Doc:Desc This method schedules a MAC timer with a timeout value
  //Doc:Arg1 The event to be used
  //Doc:Arg2 The MAC Timer event reference
  //Doc:Arg3 The time duration

  //Doc:Method
  void CancelTimer(MACTimerEvent*& ev, bool deltimer = false);
  //Doc:Desc This method cacels the scheduled MAC timer
  //Doc:Arg1 Reference to the MACTimer object
  //Doc:Arg2 bool to indicate whether we need to delete the timer

  //Doc:Method
  void BackoffProcedure();
  //Doc:Desc This method is the generic backoff procedure

  // to handle IFS delays
  //Doc:Method
  virtual void Handle(Event* ev, Time_t t);
  //Doc:Desc This method handles the actual MACEvents for sending packets
  //Doc:Arg1 Pointer to the event object
  //Doc:Arg2 The time at which this event is being handled

  //Doc:Method
  void SetRTSThreshold(Count_t thresh) {RTSThreshold=thresh;}
  //Doc:Desc This method is the generic backoff procedure


  void Idle2Busy();

  void Busy2Idle();

  void InitiateDataSequence();

  void InitiateFirstAttempt();

  void InitiateBackoff();

  void SuspendTimer(MACTimerEvent* ev);

  void ResumeTimer(MACTimerEvent* ev);

  void DataRequestFromIFQ();

  void DropPacket(const std::string&, bool = true);

  void AddSeqCache(MACAddr, Word_t);

  bool FindSeqCache(MACAddr, Word_t);

  void Notify(void*);

  void Bootstrap();

  void AddToBTV(IPAddr_t, MACAddr);

  void RemoveFromBTV(IPAddr_t);

  void AddNotify(NotifyHandler*,void*);      // Add a notify client

  void SendNotification(IPAddr_t ip);        // Notify interface is up

  Packet* BuildBeaconPacket();               // Create a beacon packet
  void    ScheduleBeaconTimer(Time_t);       // Schedule the beacon timer

private:
  enum {
    IDLE, RTS_RCVD, CTS_WAIT, CTS_RCVD, DATA_WAIT, DATA_RCVD, ACK_WAIT
  } mac_state;
  enum { NONE, SUSPENDED, RESUMED } backoffTimerState;
  typedef vector<SeqCacheEntry> SeqCache_t;

  SeqCache_t  seqCache;
  Word_t      nextCacheEntry;
  int failCount;             // used as a failure counter to force disassociation at either ends

protected:
  NList_t  notifications;    // Deque of pending notifications
  NotifyHandler* notify;     // Object to notify when interface is up
  
public:
  Packet*         pendingPkt; // as obtained from the upper layer + l2pdu
                              // buffered to be sent after rts/cts/xchange
  
  Word_t          cw;
  bool            firstAttempt;
  bool            backingOff;
  MACTimerEvent*  backoffTimeout;
  MACTimerEvent*  navTimeout;
  MACTimerEvent*  ctsTimeout;
  MACTimerEvent*  ackTimeout;
  MACTimerEvent*  dataTimeout;
  MACTimerEvent*  associationTimeout;
  MACTimerEvent*  beaconTimeout;  // Time to send another beacon
  bool            mevFirstAttempt;
  MACEvent*       mevBackoff;
  Time_t          backoffTime;
  Time_t          navTimer;
  Time_t          backoffTimer;
  Time_t          difs;
  Time_t          eifs;
  Time_t          sifs;
  Timer           timer;

  Rate_t          basicRate;
  Rate_t          dataRate;
  Word_t          shortRetryCount;
  Word_t          longRetryCount;
  Word_t          seqNo;  // data frame sequence number

  /* DS for Hostap mode  */
  Word_t          bseqNo; // beacon sequence number
  Word_t          ibfTime; // inter beacon frame spacing in 1000 usecs
  BridgeTable_t   btv;
  LastHeardMap_t lastH;

  /* DS for BSS mode */
  bool            associated;
  bool            associationPending;
  MACAddr         assocBssid;
  Meters_t        currDist;   // Current approx dist from the AP
  Time_t          lastBeacon; // Time of last beacon from our bss
  Time_t          cfp_dur; // Contention free period ; get from beacon frame
  bool            inCfp;
  bool            pendingBeacon;  // True if need to send beacon asap
  Packet*         pendingAssocReq;
  Packet*         pendingAssocResp;
  Packet*         pendingDissoc;
  BridgeTable_t   bct;
  Count_t         beaconRxCount;
  NotifyHandler*  xmitFailHandler;    // xmit failure handler

  static Word_t   defaultSeqCacheSize;
 
  /* RTS - CTS */
  Count_t RTSThreshold; 
  static Count_t RTSTHRESHOLD;
private:
  long   usec(double t) { return long(floor((t *= 1e6)+0.5)); }
  Time_t txtime(Size_t a, Rate_t b) { return 0.000192 + (a*8)/b; }
  void   incr_cw();

  static Random*  backoffRng;

public:
  static Rate_t   defaultBasicRate;
  static Rate_t   defaultDataRate;

  static void DefaultBasicRate(Rate_t);
  static void DefaultDataRate(Rate_t);

  Rate_t   DataRate(void);
  void     DataRate(Rate_t);
  Rate_t   BasicRate(void);
  void     BasicRate(Rate_t);
  
public:
  // Statistics
  // Count of times each frame type is created
  static Count_t  rtsCount;
  static Count_t  ctsCount;
  static Count_t  dataCount;
  static Count_t  ackCount;
  // Count of times each frame type is received
  static Count_t  rtsRxCount;
  static Count_t  ctsRxCount;
  static Count_t  dataRxCount;
  static Count_t  ackRxCount;
  // Count of times each frame type is deleted (destructor)
  static Count_t  rtsDelCount;
  static Count_t  ctsDelCount;
  static Count_t  dataDelCount;
  static Count_t  ackDelCount;
  static Count_t  beaconCount;
  static Count_t  beaconDelCount;
  static Count_t  assocReqCount;
  static Count_t  assocRepCount;
  static Count_t  disassocCount;
  static Count_t  ctsTOCount;
  static Count_t  ackTOCount;
  static Count_t  navTimerCount;  // Number of times NAV Timer scheduled
  static Count_t  durationRx;     // Number of times a duration processed
  static Count_t  totPacketsRx;   // Number of packets received
};

struct Frame_control
{
public:
  u_char fc_subtype         : 4;
  u_char fc_type            : 2;
  u_char fc_proto_ver       : 2;

  u_char fc_order           : 1;
  u_char fc_wep             : 1;
  u_char fc_more_data       : 1;
  u_char fc_pwr_mgt         : 1;
  u_char fc_retry           : 1;
  u_char fc_more_frag       : 1;
  u_char fc_from_ds         : 1;
  u_char fc_to_ds           : 1;
};


// Add other types as the need arises.
// Its easier this way than to guess them from frame control

typedef enum {
	NONE,
	RTS,
	CTS,
	DATA,
	ACK,
	ASSOCREQ,
	ASSOCRESP,
	REASSOCREQ,
	REASSOCRESP,
	PROBEREQ,
	PROBERESP,
	BEACON,
	DISASSOC,
	AUTH,
	DEAUTH
} Frametype_t;

// framecontrol, duration and fcs are common to all frames

//Doc:ClassXRef
class L2Header802_11 : public PDU  // base class for all 802.11 frames
//Doc:Class The class {\tt L2Header802_11 } defines the base class containing
//Doc:Class all the commonality in the 802.11 frames
{
public:
  //Doc:Method
  L2Header802_11 () : duration(0), fcs(0), type(NONE) {}
  //Doc:Desc Default constructor

  L2Header802_11 (Word_t dur_, Long_t fcs_)
	: duration(dur_), fcs(fcs_), type(NONE) {}
  L2Header802_11 (Word_t dur_, Long_t fcs_, Frametype_t typ)
	: duration(dur_), fcs(fcs_), type(typ) {}
  L2Header802_11 (MACAddr ta_, MACAddr ra_)
    : duration(0), fcs(0), type(NONE), ra(ra_), ta(ta_) {}
  
  virtual Size_t Size() const = 0;
  // No Copy() needed for base class, as each subclass provides one
  //  PDU* Copy() {return new L2Header802_11(*this);}
  void    CommonPDUTrace(Tfstream&, Bitmap_t, const char* s, const char* v);
  void    setDuration(Word_t dur) { duration = dur; }

public:
  Frame_control fc;
  Word_t        duration;
  Long_t        fcs;

  Frametype_t   type; // its not needed if type/subtype fields are correct
  MACAddr       ra;   // Receiver address
  MACAddr       ta;   // Transmitter address
};


//Doc:ClassXRef
class RTS_frame : public L2Header802_11
//Doc:Class The class {\tt RTS_frame} defines the RTS frame
{
public:
  //Doc:Method
  RTS_frame (): L2Header802_11() { type = RTS; L2Proto802_11::rtsCount++;}
  //Doc:Desc This is the default constructor

  //Doc:Method
  RTS_frame (Word_t dur, MACAddr rcv, MACAddr xmt, Long_t fcs_);
  //Doc:Desc This method constructs the RTS frame PDU with a give set of
  //Doc:Desc arguments
  //Doc:Arg1 duration field in micro seconds
  //Doc:Arg2 Dest MAC Address
  //Doc:Arg3 Source MAC Address
  //Doc:Arg4 the checksum

  // Copy constructor
  RTS_frame(const RTS_frame&);

  // For debug, count deletions
  ~RTS_frame() {L2Proto802_11::rtsDelCount++;}
      
  //Doc:Method
  Size_t  Size() const;
  //Doc:Desc The Size of the PDU
  //Doc:Return size of the pdu in bytes

  //Doc:Method
  PDU*    Copy() const { return new RTS_frame(*this); }
  //Doc:Desc This method returns a pointer to the copy of the PDU
  //Doc:Return Pointer to the copy

  void    Trace(Tfstream&, Bitmap_t, Packet*, const char*);
};

//Doc:ClassXRef
class CTS_frame : public L2Header802_11
//Doc:Class The class {\tt CTS_frame} defines the CTS frame
{
public:
  //Doc:Method
  CTS_frame() : L2Header802_11() { type = CTS; L2Proto802_11::ctsCount++;}
  //Doc:Desc  Default Constructor

  //Doc:Method
  CTS_frame(Word_t dur, MACAddr rcv, Long_t fcs_);
  //Doc:Desc This method constructs the CTS frame PDU with a give set of
  //Doc:Desc arguments
  //Doc:Arg1 duration field in micro seconds
  //Doc:Arg2 Dest MAC Address
  //Doc:Arg3 the checksum

  // Copy constructor
  CTS_frame(const CTS_frame&);

  // For debug, count deletions
  ~CTS_frame() {L2Proto802_11::ctsDelCount++;}

  //Doc:Method
  Size_t  Size() const;
  //Doc:Desc The Size of the PDU
  //Doc:Return size of the pdu in bytes

  //Doc:Method
  PDU*    Copy() const { return new CTS_frame(*this); }
  //Doc:Desc This method returns a pointer to the copy of the PDU
  //Doc:Return Pointer to the copy
  void    Trace(Tfstream&, Bitmap_t, Packet*, const char*);
};

//Doc:ClassXRef
class ACK_frame : public L2Header802_11
//Doc:Class The class {\tt ACK_frame} defines the ACK frame
{
public:
  //Doc:Method
  ACK_frame() : L2Header802_11() { type = ACK; L2Proto802_11::ackCount++;}
  //Doc:Desc Default Constructor

  //Doc:Method
  ACK_frame(Word_t dur, MACAddr rcv, Long_t fcs_);
  //Doc:Desc This method constructs the ACK frame PDU with a give set of
  //Doc:Desc arguments
  //Doc:Arg1 duration field in micro seconds
  //Doc:Arg2 Dest MAC Address
  //Doc:Arg3 the checksum

  // Copy constructor
  ACK_frame(const ACK_frame&);

  // For debug, count deletions
  ~ACK_frame() {L2Proto802_11::ackDelCount++;}

  //Doc:Method
  Size_t  Size() const;
  //Doc:Desc The Size of the PDU
  //Doc:Return size of the pdu in bytes

  //Doc:Method
  PDU*    Copy() const { return new ACK_frame(*this); }
  //Doc:Desc This method returns a pointer to the copy of the PDU
  //Doc:Return Pointer to the copy
  void    Trace(Tfstream&, Bitmap_t, Packet*, const char*);
};


//Doc:ClassXRef
class DATA_frame : public L2Header802_11
//Doc:Class The class {\tt DATA_frame} defines the DATA frame
{
public:
  //Doc:Method
  DATA_frame() : L2Header802_11() { type = DATA; L2Proto802_11::dataCount++; }
  //Doc:Desc Default constructor

  //Doc:Method
  DATA_frame(Word_t dur, MACAddr rcv, MACAddr xmt, Word_t sqn, Long_t fcs_);
  //Doc:Desc This constructs a data frame from a give set of arguments
  //Doc:Arg1 Duration in us
  //Doc:Arg2 Destination MAC Address
  //Doc:Arg3 Source MAC Address
  //Doc:Arg4 the sequence number in case of fragmented frames
  //Doc:Arg5 The checksum length

  // Copy constructor
  DATA_frame(const DATA_frame&);

  // For debug, count deletions
  ~DATA_frame() {L2Proto802_11::dataDelCount++;}

  //Doc:Method
  Size_t  Size() const;
  //Doc:Desc The Size of the PDU
  //Doc:Return size of the pdu in bytes

  //Doc:Method
  PDU*    Copy() const { return new DATA_frame(*this); }
  //Doc:Desc This method returns a pointer to the copy of the PDU
  //Doc:Return Pointer to the copy
  void    Trace(Tfstream&, Bitmap_t, Packet*, const char*);

public:
  MACAddr  addr1;  /* is dst for to_ds = 0 ; GENERALLY DEST
					  ra if to_ds = from_ds = 1;
					  else bssid */
  MACAddr  addr2;  /* is src for from_ds = 0; GENERALLY SRC
					  ta for to_ds=from_ds=1;
					  else bssid */
  MACAddr  addr3;  /* is da  for to_ds = 1 ;
					  bssid if to_ds = from_ds = 0 ;else sa*/
  MACAddr  addr4;  /* is sa  for to_ds = from_ds = 1 ; else NULL*/
  Word_t   seqno;
};



/*
  The Management frames' MAC header can be abstracted out but there is
  little to be gained from that.
*/

/*
  Information elements
 */

class Info_element {
 public:
	u_char el_id;
	u_char len;
};

class supp_rates_element : public Info_element {
 public:
  u_char supp[8];
};

class ssid_element: public Info_element {
 public:
  string ssid; /* not to exceed 32 chars */
};

class fh_paramset_element: public Info_element {
 public:
	Word_t dwell_time;
	u_char hop_set;
	u_char hop_pattern;
	u_char hop_index;
};

class ds_paramset_element: public Info_element {
 public:
	u_char currChan;
};

class cf_paramset_element: public Info_element {
 public:
	u_char cfp_count;
	u_char cfp_period;
	Word_t cfp_maxdur;
	Word_t cfp_remdur;
};

class tim_element : public Info_element {
 public:
	u_char dtim_count;
	u_char dtim_period;
	u_char bitmap_control;
	u_char pv_bitmap[256];
};

class ibss_paramset_element : public Info_element {
 public:
	Word_t aim_window;
};

class chall_text_element : public Info_element {
 public:
	u_char text[253];
};


/* The actual frames */

class BSSBaseFrame : public L2Header802_11 
{ // Base class for all base station association messages
public:
  BSSBaseFrame() : L2Header802_11(), seqno(0) {}
  BSSBaseFrame(MACAddr s, MACAddr d) : L2Header802_11(s, d), seqno(0) {}
  void CommonTrace(Tfstream&, Bitmap_t, const char*, const char* v);
public:
  //  MACAddr sa;    Source address (why do we need this?  Already in 802.11 hdr
  //  MACAddr da;    Dest address (why do we need this?  Already in 802.11 hdr
  Word_t  seqno;
};
  
class Beacon_frame: public BSSBaseFrame {
public:
  Beacon_frame() : BSSBaseFrame() { type = BEACON; L2Proto802_11::beaconCount++;}

  Beacon_frame(MACAddr maddr) : BSSBaseFrame(maddr, MACAddr::Broadcast()) {
    type = BEACON;
    L2Proto802_11::beaconCount++;
    //    da.SetBroadcast();
  }
  // Copy constructor
  Beacon_frame(const Beacon_frame&);

  ~Beacon_frame() { L2Proto802_11::beaconDelCount++; }
  

  Size_t  Size() const;
  PDU*    Copy() const { return new Beacon_frame(*this); }
  void    Trace(Tfstream&, Bitmap_t, Packet* = nil, const char* = nil);
 public:
  double timestamp;
  Word_t beacon_int;
  Word_t cap_info;
  ssid_element ssidel; /* size is 32 and is taken care on using Size() */
  supp_rates_element ssrel;
  fh_paramset_element fhps;
  ds_paramset_element dsps;
  ibss_paramset_element ibssps;
  tim_element  tim;
};

class Disassoc_frame: public BSSBaseFrame {
 public:
  Disassoc_frame() : BSSBaseFrame() {
    type = DISASSOC;
    reason_code = 0;
    L2Proto802_11::disassocCount++;
  }

  Disassoc_frame(MACAddr src, IPAddr_t srcIP, MACAddr dst)
      : BSSBaseFrame(src, dst) {
    type = DISASSOC;
    sip = srcIP;
    reason_code=0;
    L2Proto802_11::disassocCount++;
  }
  Size_t  Size() const;
  PDU*    Copy() const { return new Disassoc_frame(*this); }
  void    Trace(Tfstream&, Bitmap_t, Packet* = nil, const char* = nil);
 public:
        IPAddr_t sip;
	Word_t   reason_code;
};

class Assocreq_frame: public BSSBaseFrame {
 public:
  Assocreq_frame() : BSSBaseFrame() {
    type = ASSOCREQ;
    L2Proto802_11::assocReqCount++;
  }

  Assocreq_frame(MACAddr src, IPAddr_t srcIP, MACAddr dst)
      : BSSBaseFrame(src, dst) {
    type = ASSOCREQ;
    sip = srcIP;
    L2Proto802_11::assocReqCount++;
  }
  Size_t  Size() const;
  PDU*    Copy() const { return new Assocreq_frame(*this); }
  void    Trace(Tfstream&, Bitmap_t, Packet* = nil, const char* = nil);
 public:
        IPAddr_t sip;
	Word_t  cap_info;
	Word_t  listen_int;
	ssid_element ssid;
	supp_rates_element ssrel;
};

class Assocresp_frame: public BSSBaseFrame {
 public:
  Assocresp_frame() : BSSBaseFrame() {
    type = ASSOCRESP;
    L2Proto802_11::assocRepCount++;
  }

  Assocresp_frame(MACAddr src, MACAddr dst)
      : BSSBaseFrame(src, dst) {
    type = ASSOCRESP;
    L2Proto802_11::assocRepCount++;
  }
  Size_t  Size() const;
  PDU*    Copy() const { return new Assocresp_frame(*this); }
  void    Trace(Tfstream&, Bitmap_t, Packet* = nil, const char* = nil);
 public:
	Word_t  seqno;
	Word_t  cap_info;
	Word_t  status_code;
	Word_t  aid;
	supp_rates_element ssrel;
};

class Reassocreq_frame: public BSSBaseFrame {
 public:
  Reassocreq_frame() : BSSBaseFrame() {
    type = REASSOCREQ;
  }
  Reassocreq_frame(MACAddr src, MACAddr dst)
      : BSSBaseFrame(src, dst) {
    type = REASSOCREQ;
  }
  Size_t  Size() const;
  PDU*    Copy() const { return new Reassocreq_frame(*this); }
  void    Trace(Tfstream&, Bitmap_t, Packet* = nil, const char* = nil);
 public:
	Word_t  cap_info;
	Word_t  listen_int;
	MACAddr current_ap;
	ssid_element ssid;
	supp_rates_element ssrel;
};

class Reassocresp_frame: public BSSBaseFrame {
 public:
  Reassocresp_frame() : BSSBaseFrame() {
    type = REASSOCRESP;
  }
  Size_t  Size() const;
  PDU*    Copy() const { return new Reassocresp_frame(*this); }
  void    Trace(Tfstream&, Bitmap_t, Packet* = nil, const char* = nil);
 public:
	Word_t  cap_info;
	Word_t  status_code;
	Word_t  aid;
	supp_rates_element ssrel;
};

class Probereq_frame: public BSSBaseFrame {
 public:
  Probereq_frame() : BSSBaseFrame() {
    type = PROBEREQ;
  }
  Size_t  Size() const;
  PDU*    Copy() const { return new Probereq_frame(*this); }
  void    Trace(Tfstream&, Bitmap_t, Packet* = nil, const char* = nil);
 public:
	ssid_element ssid;
	supp_rates_element ssrel;
};

class Proberesp_frame: public BSSBaseFrame {
 public:
  Proberesp_frame() : BSSBaseFrame() {
    type = PROBERESP;
  }
  Size_t  Size() const;
  PDU*    Copy() const { return new Proberesp_frame(*this); }
  void    Trace(Tfstream&, Bitmap_t, Packet* = nil, const char* = nil);
 public:
	Word_t  timestamp;
	Word_t  beacon_int;
	Word_t  cap_info;
	ssid_element ssid;
	supp_rates_element ssrel;
	fh_paramset_element fhps;
	ds_paramset_element dsps;
	cf_paramset_element cfps;
	ibss_paramset_element ibssps;
};


class Auth_frame: public BSSBaseFrame {
 public:
  Auth_frame() : BSSBaseFrame() {
    type = AUTH;
  }
  Size_t  Size() const;
  PDU*    Copy() const { return new Auth_frame(*this); }
  void    Trace(Tfstream&, Bitmap_t, Packet* = nil, const char* = nil);
 public:
	Word_t algo_nr;
	Word_t algo_trans_nr;
	Word_t status;
	chall_text_element cte;
};

class Deauth_frame: public BSSBaseFrame {
 public:
  Deauth_frame() : BSSBaseFrame () {
    type = DEAUTH;
  }
  Size_t  Size() const;
  PDU*    Copy() const { return new Deauth_frame(*this); }
  void    Trace(Tfstream&, Bitmap_t, Packet* = nil, const char* = nil);
 public:
	Word_t  reason;
};

#endif
