// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: rtt-estimator.h 92 2004-09-14 18:18:43Z dheeraj $



// Georgia Tech Network Simulator - Round Trip Time Estimation Class
// George F. Riley.  Georgia Tech, Spring 2002

// Implements several variations of round trip time estimators

#ifndef __rtt_estimator_h__
#define __rtt_estimator_h__

#include "common-defs.h"

#include <deque>

//Doc:ClassXRef
class RTTHistory {
public:
  RTTHistory(Seq_t s, Count_t c, Time_t t);
  RTTHistory(const RTTHistory& h); // Copy constructor
public:
  Seq_t   seq;    // First sequence number in packet sent
  Count_t count;  // Number of bytes sent
  Time_t  time;   // Time this one was sent
  bool    retx;   // True if this has been retransmitted
};

typedef std::deque<RTTHistory> RTTHistory_t;

class Statistics;
//Doc:ClassXRef
class RTTEstimator {  //  Base class for all RTT Estimators
  //Doc:Class Class {\tt RTTEstimator} is a virtual base class which
  //Doc:Class defines the behavior of a round trip time estimator used
  //Doc:Class by {\tt TCP}.
public:
  // Default constructor  
  //Doc:Method
  RTTEstimator() : next(0), history(), est(initialEstimate),
    nSamples(0), multiplier(1.0), privateStats(nil) { } 
    //Doc:Desc {\tt RTTEstimator} constructor, no arguments.

  //Doc:Method
  RTTEstimator(Time_t e) : next(0), history(), est(e),
    nSamples(0), multiplier(1.0), privateStats(nil) { }
    //Doc:Desc {\tt RTTEstimator} constructor specifying
    //Doc:Desc the initial RTT estimate.
    //Doc:Arg1 Initial RTT Estimate.

  //Doc:Method
  RTTEstimator(const RTTEstimator&); // Copy constructor
    //Doc:Desc Copy constructor.
    //Doc:Arg1 {\tt RTTEstimator} object to copy.

  virtual ~RTTEstimator();

  // Note that a particular sequence was sent  
  //Doc:Method
  virtual void SentSeq(Seq_t, Count_t);
    //Doc:Desc Notify the RTT estimator that a particular sequence number
    //Doc:Desc was sent.
    //Doc:Arg1 First sequence number in the block of data sent.
    //Doc:Arg2 Length of block sent (bytes).

  // Note a particular seq was acknowledged
  // Return measured rtt (0 if invalid)
  //Doc:Method
  virtual Time_t AckSeq(Seq_t);
    //Doc:Desc Notify the RTT estimator that a particular sequence number
    //Doc:Desc has been acknowledged.
    //Doc:Arg1 Sequence number acknowledged.
    //Doc:Return Reasured RTT for this sample.

  // Clear the sent-seq list
  //Doc:Method
  virtual void ClearSent();
    //Doc:Desc Clear the list of pending sequence numbers sent.

  // Add a measurement to the filter
  //Doc:Method
  virtual void   Measurement(Time_t t) = 0;
    //Doc:Desc Add a round trip time measurement to the filter.
    //Doc:Arg1 Individual measurement (seconds).

  // Get the current estimate
  //Doc:Method
  virtual Time_t Estimate() = 0;
    //Doc:Desc Return the current RTT estimate.
    //Doc:Return Current RTT estimate (seconds).

  // Get the timeout period
  //Doc:Method
  virtual Time_t RetransmitTimeout() = 0;
    //Doc:Desc Return the computed retransmit timeout period.
    //Doc:Return Retransmit timeout period (seconds).

  // Set the initial sequence number
  //Doc:Method
  void Init(Seq_t s) { next = s;}
    //Doc:Desc Set the initial sequence number for this estimator.
    //Doc:Arg1 Initial sequence number.

  // Make a copy of this estimator
  //Doc:Method
  virtual RTTEstimator* Copy() const = 0;
    //Doc:Desc Make a copy of this estimator.
    //Doc:Return Pointer to copy of this estimator.

  //Doc:Method
  virtual void IncreaseMultiplier();
    //Doc:Desc Increase the multiplier for the retransmit timeout calculation.
    //Doc:Desc Many {\tt TCP} implementations exponentially increase
    //Doc:Desc the retransmit timeout period on each timeout action.

  //Doc:Method
  virtual void ResetMultiplier();
    //Doc:Desc Reset the retransmit multiplier to the initial value.

  // Reset to initial state
  //Doc:Method
  virtual void Reset();
    //Doc:Desc Reset this RTT estimator to its initial state.

  //Doc:Method
  void SetPrivateStatistics(const Statistics&);
    //Doc:Desc Specify a statistics object to track the behavior  of this
    //Doc:Desc estimator object.
    //Doc:Arg1 Reference to any statistics object.

  //Doc:Method
  Statistics* GetPrivateStatistics() { return privateStats;}
    //Doc:Desc Get a pointer to the statistics object for this estimator.
    //Doc:Return Statistics pointer to current private statistics object,
    //Doc:Return {\tt nil} if none.

private:
  Seq_t        next;    // Next expected sequence to be sent
  RTTHistory_t history; // List of sent packet
public:
  Time_t       est;     // Current estimate
  Count_t      nSamples;// Number of samples
  Mult_t       multiplier;   // RTO Multiplier
  Statistics*  privateStats; // Statistics for this rtt only
public:
  //Doc:Method
  static void InitialEstimate(Time_t);
    //Doc:Desc Set a global default initial estimate.
    //Doc:Arg1 Initial estimate for all rtt estimators.

  //Doc:Method
  static void Default(const RTTEstimator&);  // Set new default
    //Doc:Desc Set a new global default RTT estimator.
    //Doc:Arg1 Reference to an RTT estimator to use for global default.

  //Doc:Method
  static RTTEstimator* Default();      // Retrieve current default
    //Doc:Desc Get a pointer to the current default RTT estimator.
    //Doc:Return Pointer to current default RTT estimator.

  //Doc:Method
  static void SetStatistics(Statistics*); // Set global statistics object
    //Doc:Desc Set a statistics object for global statistics.  This
    //Doc:Desc collects the statistics from all RTT estimators in a
    //Doc:Desc single statistics object.
    //Doc:Arg1 Reference to a statistics object for global data collection.

private:
  static Time_t initialEstimate;       // Default initial estimate
  static RTTEstimator* defaultRTT;     // Default RTT Estimator
  static Statistics*   globalStats;    // Non-null if global statistics kept
  static Mult_t        maxMultiplier;  // Maximum value of multiplier
};

// The "Mean-Deviation" estimator, as discussed by Van Jacobson
// "Congestion Avoidance and Control", SIGCOMM 88, Appendix A

//Doc:ClassXRef
class RTTMDev : public RTTEstimator {
  //Doc:Class Class {\tt RTTMDev} implements the "Mean--Deviation" estimator
  //Doc:Class as described by Van Jacobson 
  //Doc:Class "Congestion Avoidance and Control", SIGCOMM 88, Appendix A
public :
  //Doc:Method
  RTTMDev(Mult_t g) : gain(g), variance(0) { }
    //Doc:Desc Constructor for {\tt RTTMDev} specifying the gain factor for the
    //Doc:Desc estimator.
    //Doc:Arg1 Gain factor.

  //Doc:Method
  RTTMDev(Mult_t g, Time_t e) : RTTEstimator(e), gain(g), variance(0) { }
    //Doc:Desc Constructor for {\tt RTTMDev} specifying the gain factor
    //Doc:Desc and the initial estimate.
    //Doc:Arg1 Gain factor.
    //Doc:Arg2 Initial estimate.

  //Doc:Method
  RTTMDev(const RTTMDev&); // Copy constructor
    //Doc:Desc Copy constructor.
    //Doc:Arg1 {\tt RTTMDev} object to copy.

  void   Measurement(Time_t);
  Time_t Estimate() { return est;}
  Time_t Variance() { return variance;}
  Time_t RetransmitTimeout();
  RTTEstimator* Copy() const;
  void   Reset();
  // Set the filter gain
  //Doc:Method
  void Gain(Mult_t g) { gain = g;}
    //Doc:Desc Set the filter gain for this estimator.
    //Doc:Arg1 Gain factor.

public:
  Mult_t       gain;       // Filter gain
  Time_t       variance;   // Current variance
};

#endif



