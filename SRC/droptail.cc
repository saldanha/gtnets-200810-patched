// GENERAL PUBLIC LICENSE AGREEMENT
//
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
//
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
//
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
//
// 1.  This License allows you to:
//
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
//
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//
//
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
//
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
//
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
//
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
//
//
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
//
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
//
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
//
//
// 3.  Export Law Assurance.
//
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//
// 4.  Termination.
//
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
//
//
// 5.  Disclaimer of Warranties and Limitation on Liability.
//
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
//
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
//
// GTRC shall have no obligation for support or maintenance of Program.
//
// 6.  Copyright Notice.
//
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
//
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
//
// Said copyright notice shall read as follows:
//
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: droptail.cc 346 2005-04-20 18:32:32Z riley $



// Georgia Tech Network Simulator - DropTail Queue class
// George F. Riley.  Georgia Tech, Spring 2002

//#define DEBUG_MASK 0x08
#include "debug.h"
#include "droptail.h"
#include "interface.h"
#include "link.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

using namespace std;

bool DropTail::Enque(Packet* p)
{
  if (!Detailed()) UpdateSize(); // Maintain virtual size
  UpdateAverage();
  totEnq++;
  if (verbose && 0)
    {
      cout << "Enquing pkt time " << Simulator::Now()
           << " size " << size
           << " pktsize " << p->Size()
           << " pktCount " << pktCount
           << " pktLimit " << pktLimit << endl;
    }
  bool full = false;
  if (pktLimit)
    { // Queuing in packets
      full = (pktCount == pktLimit);
    }
  else
    { // Queuing in bytes
      full = (size + p->Size()) > Limit();
    }
  
  DEBUG(0,(cout << "Enquing pkt time " << Simulator::Now()
           << " size " << size
           << " pktsize " << p->Size()
           << " pktCount " << pktCount
           << " pktLimit " << pktLimit << endl));
  pktCount++; // Count number of packets in queue
  if (CheckForcedLoss() || full)
    {
      DEBUG(2,(cout << "Dropping pkt, size " << size
               << " pSize " << p->Size()
               << " limit " << Limit()
               << " time " << Simulator::Now()
               << endl));
      dropCount++;
      pktCount--;
      globalQueueDrop++;
      Stats::pktsDropped++;
      if (verbose)
        {
          cout << "Dropping pkt, size " << size
               << " pSize " << p->Size()
               << " limit " << Limit()
               << " time " << Simulator::Now()
               << endl;
        }
      return false; // Full
    }
  DBG((Stats::pktsEnqued++));
  size += p->Size();                     // Note byte count added
  if (Detailed())
    {
      pkts.push_back(p);      // Save the packet if detailed
    }
  else
    { // Non-Detailed
      if (!interface) return false;      // Can't handle w/o interface
      if (!ndDeq) ndDeq = new NDDeq_t(); // Allocate the Deque
      Time_t startTx = Simulator::Now(); // Time that this pkt starts transmit
      if (!ndDeq->empty())
        { // Not empty, starts after last one finishes
          NDInfo& nd = ndDeq->back();    // Last entry in queue
          startTx = nd.stopTx;           // Time that last one finishes
        }
      Link* link = interface->GetLink();
      if (!link) return false;           // Link must exist
      Time_t txTime =  ((Rate_t)p->Size() * 8) / link->Bandwidth();
      NDInfo  newND(startTx, startTx + txTime, p->Size());
      ndDeq->push_back(newND);           // Add to the deque
    }
  DEBUG(1,(cout << " newsize " << size << endl));
  return true;                           // Not full, enqued
}

Packet* DropTail::Deque()
{ // Deque is never called for non-detailed queues
  if (!Detailed()) return nil;            // Always nil if not detailed
  UpdateAverage();
  if (Length() == 0) return nil;         // Nothing enqued
  DBG((Stats::pktsDequed++));
  Packet* p = pkts.front();              // Get head of queue
  pkts.pop_front();                      // Remove it
  size -= p->Size();                     // Note byte count removed
  pktCount--;                            // Note pkt removed
  return p;                              // Return the packet
}

Packet* DropTail::PeekDeque()
{
  if (Length() == 0) return nil;         // Nothing enqued
  return pkts.front();                   // Return head of queue without remove
}

Count_t DropTail::DequeAllDstMac(MACAddr m)
{
  Count_t count = 0;
  for (PktList_t::iterator i = pkts.begin(); i != pkts.end(); )
    {
      PktList_t::iterator next = i;
      next++;
      Packet* p = *i;
      if (p->GetDstMac() == m)
        {
          count++;
          pkts.erase(i);
          size -= p->Size();
          pktCount--;
          delete p;
        }
      i = next;
    }
  return count;
}

Count_t DropTail::DequeAllDstIP(IPAddr_t ip)
{
  Count_t count = 0;
  for (PktList_t::iterator i = pkts.begin(); i != pkts.end(); )
    {
      PktList_t::iterator next = i;
      next++;
      Packet* p = *i;
      if (p->GetDstIP() == ip)
        {
          count++;
          pkts.erase(i);
          size -= p->Size();
          pktCount--;
          delete p;
        }
      i = next;
    }
  return count;
}

Packet* DropTail::DequeOneDstMac(MACAddr m)
{
  for (PktList_t::iterator i = pkts.begin(); i != pkts.end(); ++i)
    {
      Packet* p = *i;
      if (p->GetDstMac() == m)
        {
          pkts.erase(i);
          size -= p->Size();
          pktCount--;
          return p;
        }
    }
  return nil;
}

Packet* DropTail::DequeOneDstIP(IPAddr_t ip)
{
  for (PktList_t::iterator i = pkts.begin(); i != pkts.end(); ++i)
    {
      Packet* p = *i;
      if (p->GetDstIP() == ip)
        {
          pkts.erase(i);
          size -= p->Size();
          pktCount--;
          return p;
        }
    }
  return nil;
}

Count_t DropTail::Length()
{
  if (!Detailed())UpdateSize();
  return (Count_t)size;
}

Count_t DropTail::LengthPkts()
{
  if (!Detailed())UpdateSize();
  return pktCount;
}

void   DropTail::SetInterface(Interface* i)
{
  interface = i;
}

Queue* DropTail::Copy() const
{ // Make a copy of this queue
  return new DropTail(*this);
}

void DropTail::SetLimit(Count_t l)
{
  limit = l;
}

Time_t   DropTail::QueuingDelay()
{
  if (Detailed()) return Queue::QueuingDelay();
  UpdateSize();
  // Not detailed, use ndDeq list
  Time_t now = Simulator::Now();
  // Find when last packet will finish
  if (!ndDeq) return 0;  // No pending pkts
  if (ndDeq->empty()) return 0; // No pending pkts
  NDInfo& nd = ndDeq->back();   // Newest entry
  return (nd.stopTx - now);     // Time till this one finishes
}


bool DropTail::Check(Size_t s, Packet*)
{ // Check if sufficient buffer space available
  if(!Detailed())UpdateSize();
  if (Length() == 0) return true; // Always true if empty
  if (pktLimit)
    { // Using packet limit, not bytes
      return pktCount < pktLimit;
    }
  // Queue limit in bytes
  return ((Length() + s) < Limit());
}

Packet* DropTail::GetPacket(Count_t k)
{
  if (pkts.empty()) return nil;
  // Get the k'th packet
  PktList_t::iterator i = pkts.begin();

  for (Count_t j = 0; j < k; ++j)
    {
      ++i;
      if (i == pkts.end()) return nil; // Does not exist
    }
  return *i;
}

void    DropTail::GetPacketColors(ColorVec_t& cv)
{ // Build a list of packet colors from head to tail
#ifdef HAVE_QT
  for (PktList_t::iterator i = pkts.begin(); i != pkts.end(); ++i)
    {
      Packet* p = *i;
      QColor c;
      if (p->IsColored())
        c = QColor(p->R(), p->G(), p->B());
      else
        c = Qt::blue;
      cv.push_back(c.rgb());
    }
#endif
}

// Private methods
void DropTail::UpdateSize()
{ // Adjust size to account for packets transmitted since last update
  if (!ndDeq) return; // Nothing to do if no non-detailed deque
  Time_t now = Simulator::Now();
  while(!ndDeq->empty())
    {
      NDInfo& nd = ndDeq->front(); // Oldest entry
      if (nd.startTx > now) return;      // Starts in future
      if (nd.stopTx  > now)
        { // Stops in future.  This means the packet would have
          // been dequeued at this point, but the link is still
          // busy transmitting it.  We should reduce the size
          // of the queue by the size of the packet, but leave
          // this entry in the ndDeq so we know when the link is
          // free.
          if (!nd.updated)
            {
              size -= nd.size;
              pktCount--;
              nd.updated = true;
            }
          return; // done for now
        }
      // This packet has finished, remove it
      if (!nd.updated)
        {
          size -= nd.size;
          pktCount--;
        }
      ndDeq->pop_front(); // Remove from queue and continue searching
    }
}



