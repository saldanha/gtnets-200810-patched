ifndef _OS_CHECK_
_OS_CHECK	:= DONE


OSNAME          ?= $(shell uname -s)
ARCH            ?= $(shell uname -m)
SUPPORTED_OS    ?= "SunOS Linux Darwin"

ifeq ($(OSNAME), $(findstring $(OSNAME), $(SUPPORTED_OS)))

ifeq (SunOS,$(OSNAME))
CFLAGS		:= -D$(OSNAME) -Wall
STATIC_EXT	:= a
SHARED_EXT	:= so
GLDFLAGS        := -R
LLDFLAGS	:= "-lsocket -lnsl"
SHFLAGS		:= -G
ifeq (CC, $(findstring CC, $(CXX)))
DFLAGS          := -xM1 -library=%none
PICFLAGS        := 
COPTS		:= -xO2
else
DFLAGS          := -MM
PICFLAGS        := -fPIC
COPTS		:= -O2
endif
endif

ifeq (Darwin,$(OSNAME))
CFLAGS		:= -D$(OSNAME) -Wall
STATIC_EXT	:= a
SHARED_EXT	:= dylib
GLDFLAGS        := ""
LLDFLAGS	:= -bind_at_load
SHFLAGS		:= -dynamiclib -install_name
DFLAGS          := -MM
PICFLAGS        := -fPIC
COPTS           := -O2
endif


ifeq (Linux,$(OSNAME))
CFLAGS		:= -D$(OSNAME) -Wall
STATIC_EXT	:= a
SHARED_EXT	:= so
GLDFLAGS        := -Wl,--rpath -Wl,
LLDFLAGS	:= ""
SHFLAGS         := -shared
DFLAGS          := -MM
PICFLAGS        := -fPIC
COPTS           := -O2
endif

else

## Unsupported OS, assuming GNU toolchain
CFLAGS		:= -D$(OSNAME) -Wall
STATIC_EXT	:= a
SHARED_EXT	:= so
GLDFLAGS        := -Wl,--rpath -Wl,
LLDFLAGS	:= ""
SHFLAGS		:= -shared
DFLAGS          := -MM
PICFLAGS        := -fPIC
COPTS           := -O2
endif

# this target is for debugging only 
#oscheck:
#	@echo $(STATIC_EXT)
#	@echo $(SHARED_EXT)
#	@echo $(GLDFLAGS)
#	@echo $(DFLAGS)
#	@echo $(PICFLAGS)

endif
