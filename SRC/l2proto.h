// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: l2proto.h 460 2006-01-31 20:48:39Z riley $



// Georgia Tech Network Simulator - Layer 2 Protocol base class
// George F. Riley.  Georgia Tech, Spring 2002

#ifndef __l2proto_h__
#define __l2proto_h__

#include "common-defs.h"
#include "protocol.h"
#include "notifier.h"
#include "pdu.h"

class Interface;
class LinkEvent;

//Doc:ClassXRef
class L2Proto : public Protocol, public NotifyHandler {
  //Doc:Class Class {\tt L2Proto} is the base class for all layer two 
  //Doc:Class protocols. these implement specifically the Medium Access 
  //Doc:Class Control and associated functions. This class closely 
  //Doc:Class interacts with the {\tt Link} class to provide the low 
  //Doc:Class level details of a network stack
public:

  //Doc:Method
  L2Proto();
    //Doc:Desc Constructor.

  //Doc:Method
  virtual Layer_t Layer () { return 2; }
    //Doc:Desc This member function returns the layer number of the protocol
    //Doc:Desc stack corresponding to the ISO model.
    //Doc:Return the layer number (in this case 2).

  //Doc:Method
  virtual void BuildDataPDU(MACAddr, MACAddr, Packet*) = 0;
    //Doc:Desc Build and append a layer2 pdu to the specified packet.
    //Doc:Arg1 Source MAC address
    //Doc:Arg2 Destination MAC address
    //Doc:Arg3 Packet pointer to append this pdu to.


  //Doc:Method
  virtual void DataRequest( Packet*) = 0;
  //Doc:Desc This member function is called by the higher layer requesting 
  //Doc:Desc a transfer of  data to a peer.
  //Doc:Arg1 The packet to be sent, with l2 pdu already appended/

  //Doc:Method
  virtual void DataIndication( Packet*, LinkEvent* = nil ) = 0;
  //Doc:Desc This method is called when the direction of protocol stack
  //Doc:Desc traversal is upwards, i.e, a packet has just been received
  //Doc:Desc at the underlying link
  //Doc:Arg1 The LinkEvent with the received packet (may be nil)
  //Doc:Arg1  If non-nil, the DataIndication must delete or re-use the event
  //Doc:Arg2 The received packet

  //Doc:Method
  virtual void SetInterface(Interface* i) { iface = i; }
    //Doc:Desc Specify which interface this protocol is attached to.
    //Doc:Arg1 Interface pointer for interface.

  //Doc:Method
  virtual bool Busy() = 0;                  // True if proto/link is busy
    //Doc:Desc Determine if the protocol is busy processing a packet.
    //Doc:Return True if busy.

  //Doc:Method
  virtual L2Proto* Copy() const = 0;                      // Make a copy
  //Doc:Desc This method is called to make a copy of the layer 2 protocol
  //Doc:Desc object

  //Doc:Method
  virtual bool IsWireless() const = 0;
    //Doc:Desc Query if this protocol needs a wireless interface.
    //Doc:Return True if wireless protocol.

  virtual void Notify(void*) {}

  virtual void Bootstrap() = 0;

   // Over-rides from the tracing methods from protocol.h
   // We do those a bit differently for l2 protocols, since
   // there is not a single "instance" that can globally set the
   // trace on/off status.  Instead we use a static variable
   // common to all instances.
  void    SetTrace(Trace::TraceStatus ts)// Set trace level this proto
    { globalTraceStatus = ts; }
  Trace::TraceStatus GetTrace()          // Get trace level this proto
    { return globalTraceStatus;}
  void DetailOn(Bitmap_t b)
    { globalTraceDetails |= (1L << b);}
  void DetailOff(Bitmap_t b)
    { globalTraceDetails &= (ALLBITS ^ (1L << b));}
  Bitmap_t Details()
    { return globalTraceDetails;}


  // Static methods for managing global trace
  static void GlobalSetTrace(Trace::TraceStatus ts)
    { globalTraceStatus = ts; }
  static Trace::TraceStatus GlobalGetTrace()
    { return globalTraceStatus;}
  static void GlobalDetailOn(Bitmap_t b)
    { globalTraceDetails |= (1L << b);}
  static void GlobalDetailOff(Bitmap_t b)
    { globalTraceDetails &= (ALLBITS ^ (1L << b));}
  static Bitmap_t GlobalDetails()
    { return globalTraceDetails;}
  // Since l2 packets are often (wireless) received at places other
  // than the adressee, this flag allows for reducing trace file size
  // by tracing only if the packet is addressed to the receiver
  // or a broadcast.
  static void TraceDestinationOnly(bool dto = true)
    { destinationOnly = dto; }
  
 public:
  Interface* iface;  // Interface for this protocol
 private:
  static Trace::TraceStatus globalTraceStatus;
  static Bitmap_t           globalTraceDetails;  // Bitmap of trace info
public:
  static bool               destinationOnly;     // True if trace dest only
};

//Doc:ClassXRef
class L2PDU : public PDU {
  //Doc:Class Class {\tt L2PDU} is the base class for all layer two 
  //Doc:Class protocol data units.  All L2PDU's must implement
  //Doc:Class the GetSrcMac and GetDstMac methods.
public:
  virtual Layer_t Layer()  {return 2;}
  //Doc:Method
  virtual MACAddr GetSrcMac() = 0;
    //Doc:Desc Return the source MAC address.
    //Doc:Return Source MAC Address

  //Doc:Method
  virtual MACAddr GetDstMac() = 0;
    //Doc:Desc Return the destination MAC address.
    //Doc:Return destination MAC Address
};

#endif

