// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: node.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - Node class
// George F. Riley.  Georgia Tech, Spring 2002

#include <iostream>
#include <math.h>

//#define DEBUG_MASK 0x02
#include "debug.h"
#include "node.h"
#include "node-impl.h"
#include "node-real.h"
#include "node-ghost.h"
#include "protograph.h"
#include "portdemux.h"
#include "ipaddr.h"
#include "duplexlink.h"
#include "application.h"
#include "mobility.h"
#include "routing-nixvector.h"
#include "interface-wireless.h"
#include "distributed-simulator.h"

using namespace std;

NodeId_t  Node::nextId = 0;  // Next id to assign
NodeVec_t Node::nodes;       // Vector of all known nodes
Count_t   Node::defaultPixelSize = 10;
Node::Shape_t Node::defaultShape = Node::SQUARE;
double    Node::defaultMaxSpeed = 20;  // Default max speed of a node

// Comparison operator for locations
bool operator==(const Location& l, const Location& r)
{
  return (l.X() == r.X() && l.Y() == r.Y() && l.Z() == r.Z());
}

// Constructors.
Node::Node(SystemId_t sysId) : id(nextId++),
  proxyIP(IPADDR_NONE), proxyMask(0)
{
  if (sysId != DistributedSimulator::systemId)
    { // Mapped to a different logical process.  Use a ghost.
      pImpl = new NodeGhost(this);
    }
  else
    { // Real node
      pImpl = new NodeReal(this);
    }
  nodes.push_back(this);                  // Add to vector of known nodes
}

// Protected constructor used by node subclasses
Node::Node(NodeImpl* ni, SystemId_t sysId)
    : id(nextId++), proxyIP(IPADDR_NONE), proxyMask(MASK_NONE)
{
  if (sysId != DistributedSimulator::systemId)
    { // Mapped to a different logical process.  Use a ghost.
      pImpl = new NodeGhost(this);
      delete ni; // The impl passed in is not needed
    }
  else
    { // Real node, use specified implementation
      pImpl = ni;
    }
  nodes.push_back(this);                  // Add to vector of known nodes
}

Node::~Node()
{
  delete pImpl;                           // Delete the corresponding impl
}

// IP Address Management
IPAddr_t    Node::GetIPAddr()
{  // Get the IPAddres of this node
  return pImpl->GetIPAddr();   // Return IP Address....may be IPADDR_NONE
}

void        Node::SetIPAddr(IPAddr_t i)
{ // Set the IPAddres of this node
  pImpl->SetIPAddr(i);
}

void Node::IPAddrs(IPMaskVec_t& a)
{ // Create a list of IP Addresses
  pImpl->IPAddrs(a);
}

bool Node::LocalIP( IPAddr_t ip)
{ // Check if specified ip address is local
  return pImpl->LocalIP(ip);
}

bool Node::IPKnown()
{ // True if non-default ip is assigned
  return pImpl->IPKnown();
}

bool Node::IsReal()
{ // True if real node
  return pImpl->IsReal();
}

void Node::IsSwitchNode(bool b)
{ // True if real node
  return pImpl->IsSwitchNode(b);
}

bool Node::IsSwitchNode()
{ // True if real node
  return pImpl->IsSwitchNode();
}

void Node::PacketRX(Packet* p, Interface* iface)
{ // True if real node
  return pImpl->PacketRX(p, iface);
}

// Interfaces

Interface* Node::AddInterface(Interface* pif)
{
  return pImpl->AddInterface(pif);
}

Interface* Node::AddInterface(const L2Proto& l2, bool bootstrap)
{
  return pImpl->AddInterface(l2, bootstrap);
}

Interface* Node::AddInterface(const L2Proto& l2,
                              IPAddr_t i, Mask_t mask,
			      MACAddr m, bool bootstrap)
{
  return pImpl->AddInterface(l2, i, mask, m, bootstrap);
}

Interface* Node::AddInterface(const L2Proto& l2,const Interface& iface,
                              IPAddr_t i, Mask_t mask, MACAddr m,
			      bool bootstrap)
{
  return pImpl->AddInterface(l2, iface, i, mask, m, bootstrap);
}

Count_t Node::InterfaceCount()
{
  return pImpl->InterfaceCount();
}

const IFVec_t& Node::Interfaces()
{ // Get the complete list of interfaces
  return pImpl->Interfaces();
}

Interface* Node::GetIfByLink(Link* l)
{
  return pImpl->GetIfByLink(l);
}

Interface* Node::GetIfByNode(Node* n)
{ // Find the interface used to get to the specified node
  return pImpl->GetIfByNode(n);
}

Interface* Node::GetIfByIP(IPAddr_t ip)
{ // Find the interface used to get to the specified node
  return pImpl->GetIfByIP(ip);
}

void       Node::Broadcast(Packet* p, Proto_t proto)
{ // Broadcast a packet on all interfaces
  return pImpl->Broadcast(p, proto);
}

// Links.
// All of the following are just syntactic shortcuts to the DuplexLink object.
// Each creates a local DuplexLink object, which is destroyed on exit.
Interface* Node::AddDuplexLink(Node* rn)
{ // Add duplex link to/from remote
  return pImpl->AddDuplexLink(rn);
}

Interface* Node::AddDuplexLink(Node* rn, const Linkp2p& l)
{ // Add duplex link to/from remote
  return pImpl->AddDuplexLink(rn, l);
}

Interface* Node::AddDuplexLink(Interface* li, Interface* ri)
{ // Add a link between existing interfaces
  return pImpl->AddDuplexLink(li, ri);
}

Interface* Node::AddDuplexLink(Interface* li, Interface* ri,
                               const Linkp2p& l)
{
  return pImpl->AddDuplexLink(li, ri, l);
}

// Interface* Node::AddDuplexLink(Interface* li, Interface* ri,
//                                const Linkp2p& l, Node* rn)
// {
//   return pImpl->AddDuplexLink(li, ri, l, rn);
// }

Interface* Node::AddDuplexLink(Node* rn,
                            IPAddr_t li, Mask_t lm,
                            IPAddr_t ri, Mask_t rm)
{
  return pImpl->AddDuplexLink(rn, li, lm, ri, rm);
}

Interface* Node::AddDuplexLink(Node* rn, const Linkp2p& l,
                            IPAddr_t li, Mask_t lm,
                            IPAddr_t ri, Mask_t rm)
{
  return pImpl->AddDuplexLink(rn, l, li, lm, ri, rm);
}

Link* Node::GetLink(Node* n)
{
  return pImpl->GetLink(n);
}

Interface* Node::AddRemoteLink(IPAddr_t i, Mask_t m)
{
  return pImpl->AddRemoteLink(i, m);
}

Interface* Node::AddRemoteLink(IPAddr_t i, Mask_t m, Rate_t r, Time_t d)
{
  return pImpl->AddRemoteLink(i, m, r, d);
}

// Simplex links, for modeling one-way links
Interface* Node::AddSimplexLink(Node* n)
{
  return pImpl->AddSimplexLink(n);
}

Interface* Node::AddSimplexLink(Node* n, const Linkp2p& l)
{
  return pImpl->AddSimplexLink(n, l);
}

Interface* Node::AddSimplexLink(Node* n, const Linkp2p& l, IPAddr_t i,Mask_t m)
{
  return pImpl->AddSimplexLink(n, l, i, m);
}

// Queue management
Queue*     Node::GetQueue()
{ // Return queue if only one i/f
  // This is simply a syntactic shortcut to GetQueue(Node*) below
  return pImpl->GetQueue();
}

Queue*     Node::GetQueue(Node* n)
{ // Get the queue to specified node
  return pImpl->GetQueue(n);
}

// Applications
Application* Node::AddApplication(const Application& a)
{
  return pImpl->AddApplication(a);
}


void Node::UseWormContainment(bool b) 
{
  return pImpl->UseWormContainment(b);
}


bool Node::UseWormContainment()
{ 
  return pImpl->UseWormContainment();
}

void Node::SetWormContainment(WormContainment* cont)
{
  return pImpl->SetWormContainment(cont);
}

WormContainment* Node::GetWormContainment()
{
  return pImpl->GetWormContainment();
}

bool Node::FirstBitRx()
{
  return pImpl->FirstBitRx();
}

void Node::FirstBitRx(bool is)
{
  pImpl->FirstBitRx(is);
}

void Node::UseARP(bool b)
{
  return pImpl->UseARP(b);
}

// Neighbor management
void Node::Neighbors(NodeWeightVec_t& n, bool noLeaf)
{ // Ask each interface to add to neighbor list
  pImpl->Neighbors(n, noLeaf);
}

void Node::AddNeighbor(Node*, Weight_t)
{ // Non-ghost nodes do not keep neighbor lists
  // Need to think about this a bit more
}

Count_t Node::NeighborCount()
{ // Count number of routing neighbors
  // Done by summing routing neighbors for each interface
  return pImpl->NeighborCount();
}


void Node::NeighborsByIf(Interface* i, IPAddrVec_t& v)
{ // Get list of neighbors for a given interface
  return pImpl->NeighborsByIf(i, v);
}


// Routing Interface
void Node::DefaultRoute( RoutingEntry r)
{ // Specify default route
  pImpl->DefaultRoute(r);
}

void Node::DefaultRoute( Node* n)
{ // Specify default route
  pImpl->DefaultRoute(n);
}

void Node::AddRoute( IPAddr_t ip, Count_t mask, Interface* i, IPAddr_t ip1)
{ // Routing entry
  pImpl->AddRoute(ip, mask, i, ip1);
}

RoutingEntry Node::LookupRoute(IPAddr_t ip)
{ // Find a routing entry
  return pImpl->LookupRoute(ip);
}

RoutingEntry Node::LookupRouteNix(Count_t nix)
{ // Lookup route by Neighbor Index
  return pImpl->LookupRouteNix(nix);
}

//Specially for AODV routing
//
AODVRoutingEntry *
Node::LookupRouteAODV(IPAddr_t ip) {
	return pImpl->LookupRouteAODV(ip);
}

void
Node::SetRoutingAODV(void *pRouting){
	return pImpl->SetRoutingAODV(pRouting);
}

RoutingAODV *
Node::GetRoutingAODV(){
	return pImpl->GetRoutingAODV();
}

Routing::RType_t Node::RoutingType()
{
  return pImpl->RoutingType();
}

Interface* Node::LocalRoute(IPAddr_t ip)
{ // Find if destination ip is directly connected on any local interace
  return pImpl->LocalRoute(ip);
}

void       Node::InitializeRoutes()
{ // Routing initialization (if any)
  pImpl->InitializeRoutes();
}

void       Node::ReInitializeRoutes(bool  u)
{ // Routing re-initialization (if any)
  pImpl->ReInitializeRoutes(u);
}

Count_t    Node::RoutingFibSize() const
{ // Get the size of the routing FIB for statistics measurement
  return pImpl->RoutingFibSize();
}

Count_t    Node::GetNix(Node* n) const
{ // Get Neighbor Index for spec'd node
  return pImpl->GetNix(n);
}

Routing* Node::GetRouting()
{
  return pImpl->GetRouting();
}

RoutingNixVector* Node::GetNixRouting()
{ // Returns a pointer to the routing object if it's NixVector routing,
  // otherwise return nil
  return pImpl->GetNixRouting();
}

void Node::DumpNV()
{
  RoutingNixVector* r = pImpl->GetNixRouting();
  if (!r) return;
  r->DBDump(this);
}

// Protocol Graph Interface
#ifdef COMPACT
// No private protocol graphs in compact mode
Protocol* Node::LookupProto(Layer_t l, Proto_t p) // Lookup protocol by layer
{
  return pImpl->LookupProto(l, p);
}

void      Node::InsertProto(Layer_t l, Proto_t p, Protocol* pp) // Insert proto
{
  pImpl->InsertProto(l, p, pp);
}
#else
Protocol* Node::LookupProto(Layer_t l, Proto_t proto)
{ // Lookup protocol by layer
  return pImpl->LookupProto(l, proto);
}

void Node::InsertProto(Layer_t l, Proto_t p, Protocol* pp)
{ // Insert proto
  pImpl->InsertProto(l, p, pp);
}
#endif

// Port Demux Interface
bool Node::Bind(Proto_t proto, PortId_t port , Protocol* p)
{
  return pImpl->Bind(proto, port, p);
}

bool Node::Bind(Proto_t proto,
                PortId_t lport, IPAddr_t laddr,
                PortId_t rport, IPAddr_t raddr,
                Protocol* p)
{ // Register port usage
  return pImpl->Bind(proto, lport, laddr, rport, raddr, p);
}

PortId_t Node::Bind(Proto_t proto, Protocol* p)
{ // Choose available port and register
  return pImpl->Bind(proto, p);
}

bool  Node::Unbind(Proto_t proto, PortId_t port, Protocol* p)
{ // Remove port binding
  return pImpl->Unbind(proto, port, p);
}

bool  Node::Unbind(Proto_t proto,
                PortId_t lport, IPAddr_t laddr,
                PortId_t rport, IPAddr_t raddr,
                Protocol* p)
{ // Remove port binding
  return pImpl->Unbind(proto, lport, laddr, rport, raddr, p);
}

Protocol* Node::LookupByPort(Proto_t proto, PortId_t port)
{
  return pImpl->LookupByPort(proto, port);
}

Protocol* Node::LookupByPort(Proto_t proto,
                             PortId_t sport, IPAddr_t sip,
                             PortId_t dport, IPAddr_t dip)
{
  return pImpl->LookupByPort(proto, sport, sip, dport, dip);
}

// Packet tracing interface
// Trace header if enabled
bool Node::TracePDU(Protocol* proto, PDU* h, Packet* p, char* s)
{
  return pImpl->TracePDU(proto, h, p, s);
}

void Node::SetTrace(Trace::TraceStatus t)        // Set trace level this node
{
  pImpl->SetTrace(t);
}

// Node Location Interface
void Node::SetLocation(Meters_t xl, Meters_t yl, Meters_t zl)
{ // Set x/y/z location
  pImpl->SetLocation(xl, yl, zl);
}

void Node::SetLocation(const Location& l)
{
  pImpl->SetLocation(l);
}

void Node::SetLocationLongLat(const Location& l)
{
  pImpl->SetLocationLongLat(l);
}

bool Node::HasLocation()
{
  return pImpl->HasLocation();
}

Meters_t Node::LocationX()
{
  return pImpl->LocationX();
}

Meters_t Node::LocationY()
{
  return pImpl->LocationY();
}

Meters_t Node::LocationZ()
{
  return pImpl->LocationZ();
}

Location Node::GetLocation()
{
  return pImpl->GetLocation();
}

Location Node::UpdateLocation()
{
  return pImpl->UpdateLocation();
}

Mobility* Node::AddMobility(const Mobility& m)
{ // Add a new mobility model for this node
  return pImpl->AddMobility(m);
}

Mobility* Node::GetMobility()
{
  return pImpl->GetMobility();
}

bool Node::IsMoving()
{
  return pImpl->IsMoving();
}

bool Node::IsMobile()
{
  return pImpl->IsMobile();
}

void Node::Show(bool s)                          // Turn on/off display
{
  pImpl->Show(s);
}

bool Node::Show()                                // True if display enabled
{
  return pImpl->Show();
}

QCanvasItem* Node::Display(QTWindow* qc)
{
  return pImpl->Display(qc);
}

QCanvasItem* Node::Display(const QPoint& qp, QTWindow* qc)
{
  return pImpl->Display(qp, qc);
}

void Node::WirelessTxColor(const QColor& c)
{
  pImpl->WirelessTxColor(c);
}

const QColor& Node::WirelessTxColor()
{
  return pImpl->WirelessTxColor();
}

bool Node::PushWirelessTx(QCanvasItem* c)
{
  return pImpl->PushWirelessTx(c);
}

QCanvasItem* Node::PopWirelessTx()
{
  return pImpl->PopWirelessTx();
}

void Node::PixelSize(Count_t nps)  // Set node size
{
  pImpl->PixelSize(nps);
}

Count_t Node::PixelSizeX()
{
  return pImpl->PixelSizeX();
}

Count_t Node::PixelSizeY()
{
  return pImpl->PixelSizeY();
}

void Node::Shape(Shape_t ns) // Set node shape
{
  pImpl->Shape(ns);
}

Node::Shape_t Node::Shape()
{
  return pImpl->Shape();
}

CustomShape_t Node::CustomShape()
{
  return pImpl->CustomShape();
}

void Node::CustomShape(CustomShape_t cs)
{
  pImpl->CustomShape(cs);
}

bool Node::CustomShapeFile(const char* fn)
{
  return pImpl->CustomShapeFile(fn);
}

bool Node::CustomShapeImage(const Image& im)
{
  return pImpl->CustomShapeImage(im);
}

void Node::Color(const QColor& c)
{
  pImpl->Color(c);
}

bool Node::HasColor()
{
  return pImpl->HasColor();
}

QColor& Node::Color()
{
  return pImpl->Color();
}

NodeAnimation* Node::GetNodeAnimation() const
{
  return pImpl->GetNodeAnimation();
}

bool Node::ICMPEnabled() const
{
  return pImpl->ICMPEnabled();
}

void Node::DisableICMP()
{
  pImpl->DisableICMP();
}

void Node::Down()
{
  pImpl->Down();
}

void Node::Up()
{
  pImpl->Up();
}

bool Node::IsDown()
{
  return pImpl->IsDown();
}

void Node::AddCallback(Layer_t l, Proto_t p,
                       PacketCallbacks::Type_t t,
                       Interface* i,
                       PacketCallbacks::Function_t f)
{
  pImpl->AddCallback(l, p, t, i, f);
}

void Node::AddCallbackHead(Layer_t l, Proto_t p,
                           PacketCallbacks::Type_t t,
                           Interface* i,
                           PacketCallbacks::Function_t f)
{
  pImpl->AddCallbackHead(l, p, t, i, f);
}

void Node::DeleteCallback(Layer_t l, Proto_t p, PacketCallbacks::Type_t t,
                          Interface* i)
{
  pImpl->DeleteCallback(l, p, t, i);
}

bool Node::CallCallbacks(Layer_t l, Proto_t p,
                         PacketCallbacks::Type_t t, Packet* pkt,
                         Interface* i)
{
  return pImpl->CallCallbacks(l, p, t, pkt, i);
}

#ifdef HAVE_QT

void Node::UserInformation(void* p)
{
  pImpl->UserInformation(p);
}

void* Node::UserInformation()
{
  return pImpl->UserInformation();
}


bool Node::WirelessTx()
{
  return pImpl->WirelessTx();
}

bool Node::WirelessRx()
{
  return pImpl->WirelessRx();
}

bool Node::WirelessCx()
{
  return pImpl->WirelessCx();
}

bool Node::WirelessRxMe()
{
  return pImpl->WirelessRxMe();
}

bool Node::WirelessRxZz()
{
  return pImpl->WirelessRxZz();
}

#endif

Meters_t Node::Distance(Node* n)
{
  return pImpl->Distance(n);
}

void Node::BuildRadioInterfaceList(WirelessLink* l)
{
  pImpl->BuildRadioInterfaceList(l);
}

const RadioVec_t& Node::GetRadioInterfaceList()
{
  return pImpl->GetRadioInterfaceList();
}

void Node::SetRadioRange(Meters_t range)
{
  pImpl->SetRadioRange(range);
}

Meters_t Node::GetRadioRange()
{
  return pImpl->GetRadioRange();
}

Joules_t Node::getBattery()
{
  return pImpl->getBattery();
}

void Node::setBattery(Joules_t joules)
{
  return pImpl->setBattery(joules);
}

void Node::setComputePower(double pow)
{
  return pImpl->setComputePower(pow);
}

double Node::getComputePower(void)
{
  return pImpl->getComputePower();
}

// Static Methods

Node* Node::GetNode(NodeId_t i)
{
  if (i >= nodes.size()) return nil; // Out of range
  return nodes[i];
}

void Node::Clear()
{
  nodes.clear();
}

#ifdef HAVE_QT

void Node::DefaultPixelSize(Count_t dps)
{
  defaultPixelSize = dps;
}

Count_t Node::DefaultPixelSize()
{
  return defaultPixelSize;
}

void Node::DefaultShape(Shape_t ds)
{
  defaultShape = ds;
}

Node::Shape_t Node::DefaultShape()
{
  return defaultShape;
}

#endif

void Node::DefaultMaxSpeed(double speed)
{
  defaultMaxSpeed = speed;
}

double Node::DefaultMaxSpeed()
{
  return defaultMaxSpeed;
}


// The following is for Proxy Routing alogorithms

void Node::SetProxyRoutingConfig(IPAddr_t ip, Mask mask)
{
  proxyIP = ip;
  proxyMask = mask;
}

bool Node::CanProxyRouteIP(IPAddr_t ip)
{
  return (proxyIP & proxyMask) == (ip & proxyMask);
}

IPAddr_t Node::GetProxyIP()
{
  return proxyIP;
}

Mask Node::GetProxyMask()
{
  return proxyMask;
}

int Node::GetLongestPrefixLength(IPAddr_t ip)
{
  int left = 0, right = proxyMask.NBits(), m;
  Mask_t mask1, mask2;

  if (!HasProxyRoutingConfig()) return 0;

  // I have used a binary search method that works in O(logN) time (max(N)=32)

  do {
    m=(left+right)/2;
    if (m==32) break;

    mask1 = 0xFFFFFFFF << (32-m);
    if ((proxyIP & mask1) != (ip & mask1))
      right = m-1;
    else {
      mask2 = 0xFFFFFFFF << (32- (m+1));
      if ((proxyIP & mask2) == (ip & mask2))
        left = m+1;
      else
        break; // we have found it
    }
  } while (left<=right);
  return m;
}

void Node::setRouteTable(RouteTable* rtbl)
{
  return pImpl->setRouteTable(rtbl);
}

RouteTable* Node::getRouteTable(void)
{
  return pImpl->getRouteTable();
}
