// Test animation of with custom images.
// Displays a traditional "router" symbol for the dumbbell routers
// George F. Riley, Georgia Tech, Summer 2004

#include <iostream>

#include "debug.h"
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "dumbbell.h"
#include "star.h"
#include "tcp-tahoe.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "l2proto802.3.h"
#include "validation.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

#define N_LEAF 5

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  Count_t nl = N_LEAF; // Number leaf nodes for dumbbell
  Count_t ns = N_LEAF; // Number leaf nodes for each star
  
  if (argc > 1) nl = atol(argv[1]);
  if (argc > 2) ns = atol(argv[2]);

  if (!Validation::noAnimation)
    { // Call StartAnimation early, since we need this prior to
      // the customIimageFile  calls
      s.StartAnimation(0, true);
    }
  
  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);

  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->Open("testdb-star.txt");
  IPV4::Instance()->SetTrace(Trace::ENABLED);
  L2Proto802_3::GlobalSetTrace(Trace::ENABLED);
  
  TCP::LogFlagsText(true);       // Log TCP flags in text mode
  Linkp2p lk(Rate("10Mb"), Time("10ms"));
  Dumbbell b(nl, nl, 0.1,
             IPAddr("192.168.0.1"), IPAddr("192.169.0.1"), lk);
  // Specify the bounding box
  b.BoundingBox(Location(0,0), Location(10,10));
  
  Uniform startRng(0, 0.1); // Random number generator for start times
  // Create the Star networks and assign clients/servers
  Angle_t adderL = -M_PI / (b.LeftCount() + 1.0);
  Angle_t adderR = M_PI / (b.RightCount() + 1.0);
  Angle_t thetaL = -M_PI_2 + adderL;
  Angle_t thetaR = -M_PI_2 + adderR;

#ifdef HAVE_QT
  b.Left()->Color(Qt::blue);
  b.Right()->Color(Qt::blue);
  // Here are the calls to use the custom image files for node animation
  //if (!b.Left()->CustomShapeFile("RedRouter.png"))
  if (!b.Left()->CustomShapeFile("Satellite.png"))
    cout << "Can't set router image for Left() router" << endl;
  if (!b.Right()->CustomShapeFile("Router.png"))
    cout << "Can't set router image for Right() router" << endl;
      
#endif
  for (Count_t i = 0; i < b.LeftCount(); ++i)
    {
      Node* l = b.Left(i);
      Node* r = b.Right(i);
      // Create a star networks with this each leaf as hub
      Star* sl = new Star(ns,l,lk, IPAddr("192.168.1.1")+IPAddr("0.0.1.0")*i);
      Star* sr = new Star(ns,r,lk, IPAddr("192.169.1.1")+IPAddr("0.0.1.0")*i);
      
      sl->BoundingBox(Location(0,0), Location(2,2), thetaL, M_PI / 2);
      sr->BoundingBox(Location(0,0), Location(2,2), thetaR, M_PI / 2);

#ifdef HAVE_QT
      l->Color(Qt::blue);
      r->Color(Qt::blue);
      if (!l->CustomShapeFile("Router.png"))
        cout << "Can't set router image for b.Left(i) router" << endl;
      if (!r->CustomShapeFile("Router.png"))
        cout << "Can't set router image for b.Right(i) router" << endl;
#endif
      thetaL += adderL;
      thetaR += adderR;
      
      // Add a tcp server at each left side node and client at right side
      for (Count_t j = 0; j < sl->LeafCount(); ++j)
        {
          Node* ll = sl->GetLeaf(j); // Left leaf
          TCPServer* tcpApp =
            (TCPServer*)ll->AddApplication(TCPServer());
          tcpApp->BindAndListen(ll, 80);
          tcpApp->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
#ifdef HAVE_QT
          ll->Color(Qt::yellow);
          if (!ll->CustomShapeFile("ClientPC.png"))
            cout << "Can't set client image for b.Left(i) leaf" << endl;
#endif          
          Node* rl = sr->GetLeaf(j); // Right leaf
          TCPSend* ts = (TCPSend*)rl->AddApplication(
              TCPSend(ll->GetIPAddr(),80,
                      Constant(100000)));
#ifdef HAVE_QT
          rl->Color(Qt::green);
          if (!rl->CustomShapeFile("ClientPC.png"))
            cout << "Can't set client image for b.Right(i) leaf" << endl;
          if (j == 0)
            { // Debug..set packets to blue for this flow
              ts->GetL4()->SetColor(Qt::blue);
            }
#endif
          ts->SetTrace(Trace::ENABLED);
          ts->Start(startRng.Value());
        }
    }

  // Specify animation
  if (!Validation::noAnimation)
    {
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  
  s.Progress(1);
  s.StopAt(100);
  s.Run();
  tr->Close();
}
