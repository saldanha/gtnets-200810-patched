// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: ipv4.h 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - Internet Protocol base class
// George F. Riley.  Georgia Tech, Spring 2002

// Implements the Internet Protocol Version 4, Layer 3

#ifndef __ipv4_h__
#define __ipv4_h__

#include <list>

#include "common-defs.h"
#include "l3protocol.h"
#include "pdu.h"
#include "ipaddr.h"
#include "node.h"
#include "backplane.h"

class IPV4Header;

class IPV4ReqInfo;
//Doc:ClassXRef
class IPV4 : public L3Protocol {
//Doc:Class The class {\tt IPV4} implements the Internet Protocol Version 4
//Doc:Class It derives 	from the class {\tt L3Protocol}. Since IPV4 is a
//Doc:Class a static instance of this is enough for a simulation that uses
//Doc:Class IPv4.
	
public:
  typedef enum { DefaultTTL = 64 } DefTTL_t;
  typedef enum { VERSION, HEADERLENGTH, SERVICETYPE,
                 TOTALLENGTH, IDENTIFICATION,
                 FLAGS, FRAGMENTOFFSET,
                 TTL, PROTOCOL, HEADERCHECKSUM,
                 SRC, DST, UID, OPTIONS } IPV4Trace_t; // Tracing options
  //Doc:Method
  IPV4();
  //Doc:Desc Default constructor

  //Doc:Method
  void DataRequest(Node*, Packet*, void*);     // From upper layer
  //Doc:Desc This method is used by layer 4 protocols like udp/tcp to
  //Doc:Desc hand a packet to IP.
  //Doc:Arg1 Pointer to the node requesting the data transfer
  //Doc:Arg2 Pointer to the packet to be transmitted
  //Doc:Arg3 Any auxiliary information that layer 4 might want to convey
  
  //Doc:Method
  void DataIndication(Interface*, Packet*);    // From lower layer
  //Doc:Desc This method is used to pass on a packet from any layer 2 protocol
  //Doc:Desc up the stack.
  //Doc:Arg1 A pointer to the interface at which this packet is received
  //Doc:Arg2 A pointer to the packet that was received
  
  //Doc:Method
  Interface* PeekInterface(Node*, void*);      // Find forwarding interface
  //Doc:Desc This method returns the interface pointer. This interface is
  //Doc:Desc chosen based on the node pointer and some arbitrary data
  //Doc:Desc passed to it.
  //Doc:Arg1 the pointer to the assocaited node
  //Doc:Arg2 the data based on which we return the address
  //Doc:Return the pointer to the interface object
  
  //Doc:Method
  virtual Count_t Version() { return 4;}       // Get IP version number
  //Doc:Desc This method returns the version of Internet Protocol
  //DOc:Return returns the version
  
  //Doc:Method
  virtual Proto_t Proto()   { return 0x800;}   // Get the protocol number
  //Doc:Desc This method returns the IANA assigned protocol number
  //Doc:Return the protocol number
  
private:
  void Broadcast(Node*, Packet*, IPV4ReqInfo*);// Network layer broadcast
private:
  static IPV4* instance; // Points to single instance of proto object
public:
  //Doc:Method
  static IPV4* Instance();   // Get a pointer to the single global instance
  //Doc:Desc This method returns the pointer to the global instance
  //Doc:Desc of the IPV4 object
  //Doc:Return the pointer to the global instance

  // Register data items with the dynamic simulation backplane
  static void  RegisterBackplane(DSHandle_t);
  static int   IPV4ExportQuery(char*, int, void*);
  static int   IPV4DefaultCallback(char*, void*);
  static int   IPV4ExportCallback(char*, char*, int, void*);
  static int   IPV4ImportCallback(char*, int, char*, void*);
public:
  // Static members
  static IPV4Header* bpHeader; // IPV4 header imported from backplane
  
  bool route_locally; // Allows transport layer protocols on the same
                      // node to communicate with each other. Off by
                      // default.
};

//Doc:ClassXRef
class IPV4Header : public L3PDU {
//Doc:Class	
public :
  //Doc:Method
  IPV4Header();
  //Doc:Desc This is the default constructor
 
  //Doc:Method 
  IPV4Header(const IPV4Header& r);
  //Doc:Desc This constructor takes the reference to an IPV4 header
  
  //Doc:Method
  IPV4Header(char*, Size_t&, Packet*); // Construct from serialized buffer
  //Doc:Desc This method creates a PDU and inserts in the packet from
  //Doc:Desc a serialized buffer
  //Doc:Arg1 pointer to the buffer
  //Doc:Arg2 size of the buffer
  //Doc:Arg3 the pointer to the packet
  
  //Doc:Method
  virtual ~IPV4Header();
  //Doc:Desc Destructor
  
  //Doc:Method
  Size_t  Size() const;
  //Doc:Desc This method returns the size of the PDU
  //Doc:Return the size of the PDU
  
  //Doc:Method
  PDU*    Copy() const{ return new IPV4Header(*this);}
  //Doc:Desc This method creates a copy of this PDU
  //Doc:Return A pointer to the copy of this PDU
  
  //Doc:Method
  Count_t Version() { return 4;}       // Indicate IPV4
  //Doc:Desc Returns the version number of the protocol
  //Doc:Return version 4 of IP
  
  //Doc:Method
  Proto_t Proto() { return 0x800;}        // IPV4 is protocol 0x800
  //Doc:Method Return the IPV4 protocol number
  //Doc:Return 0x800 for IPV4

  //Doc:Method
  Priority_t Priority();
  //Doc:Desc   Get the priority for this header. For IPV4, it is the
  //Doc:Desc diffserv codepoint field, which overlays the
  //Doc:Desc type-of-service field.
  //Doc:Return Priority for this packet.

  void    Trace(Tfstream&, Bitmap_t,  // Trace the contents of this pdu
                     Packet* = nil, const char* = nil);
  //Doc:Desc This method traces the contents of the PDU
  //Doc:Arg1 File output stream descriptor
  //Doc:Arg2 Bitmap that specifies the level of detail of the trace
  //Doc:Arg3 Packet that contains this PDU (optional)
  //Doc:Arg4 Extra text information for trace file (optional)
  
  // Serialization
  //Doc:Method
  Size_t SSize();                   // Size needed for serialization
  //Doc:Desc The actual size of all the PDU contents in terms of bytes for
  //Doc:Desc serialization of the contents.
  //Doc:Return the size
  
  //Doc:Method
  char*  Serialize(char*, Size_t&); // Serialize to a buffer
  //Doc:Desc This method is used to serialize the contents of the PDU
  //Doc:Desc into a serialized character buffer b . the size of this buffer is
  //Doc:Desc in the size arguement
  //Doc:Arg1 This is the pointer to the character buffer
  //Doc:Arg2 a reference to the size variable
  //Doc:Return the pointer to the updated character buffer
  
  //Doc:Method
  char*  Construct(char*, Size_t&); // Construct from buffer
  //Doc:Desc This method is the reverse of the Serialize . It constructs the
  //Doc:Desc PDU object from the character buffer that has been received over
  //Doc:Desc the network
  //Doc:Arg1 This is the pointer to the character buffer
  //Doc:Arg2 a reference to the size variable
  //Doc:Return the pointer to the character buffer of the remaining data

  // Inherited from L3PDU
  //Doc:Method
  virtual IPAddr_t GetSrcIP(){return src;}
  //Doc:Desc Return the source IP address.
  //Doc:Return source IP Address

  //Doc:Method
  virtual IPAddr_t GetDstIP() { return dst;}
  //Doc:Desc Return the destination IP address.
  //Doc:Return destination IP Address

public:
  Byte_t   version;
  Byte_t   headerLength;
  Byte_t   serviceType;
  Word_t   totalLength;
  Word_t   identification;
  Byte_t   flags;
  Word_t   fragmentOffset;
  Byte_t   ttl;       // Time to live
  Byte_t   protocol;  // Layer 4 protocols
  Word_t   headerChecksum;
  IPAddr   src;
  IPAddr   dst;
  PDUVec_t options;   // IP Options
};

//Doc:ClassXRef
class IPV4Options : public PDU { // Base class for ipv4 options
//Doc:Class The Class {\tt IPV4Options } defines the IP options part
//Doc:Class of the IP Header. As all other headers this is also derived from
//Doc:Class the class {\tt PDU}
public:
  //Doc:Method
  IPV4Options(Byte_t o) : optionNumber(o) { }
  //Doc:Desc Constructor takes a byte as the arguement
  
public:
  //Doc:Method
  virtual Size_t  Size() const { return 0;}
  //Doc:Desc Returns the size of the PDU
  //Doc:Return size of the PDU
  
  //Doc:Method  
  virtual PDU*    Copy() const { return new IPV4Options(*this);}
  //Doc:Desc This method creates a copy of this PDU
  //Doc:Return A pointer to the copy of this PDU
  
  //Doc:Method
  virtual void  Trace(Tfstream&, Bitmap_t,     // Trace this pdu 
                     Packet* = nil, const char* = nil) { }
  //Doc:Desc This method traces the contents of the PDU
  //Doc:Arg1 File output stream descriptor
  //Doc:Arg2 Bitmap that specifies the level of detail of the trace
  //Doc:Arg3 Packet that contains this PDU (optional)
  //Doc:Arg4 Extra text information for trace file (optional)
  
  //Doc:Method
  Byte_t  Option() { return optionNumber;}
  //Doc:Desc returns the options of the PDU
  //Doc:Return the options
public:
  Byte_t  optionNumber;
};

//Doc:ClassXRef
class IPV4ReqInfo {  // Information passed from L4 for IP Data Request
//Doc:Class This class defines the data structure that defines the information
//Doc:Class that is passed from layer 4 to layer 3
public:
  IPV4ReqInfo() : dst(IPADDR_NONE), src(IPADDR_NONE) { };
  IPV4ReqInfo(IPAddr_t d, IPAddr_t s, Count_t t, Proto_t p) 
      : dst(d), src(s), ttl(t), l4proto(p), tos(0) { }
  IPV4ReqInfo(IPAddr_t d, IPAddr_t s, Count_t t, Proto_t p, Byte_t ts) 
      : dst(d), src(s), ttl(t), l4proto(p), tos(ts) { }
  IPAddr   dst;    // Destination
  IPAddr   src;    // Src IP (if spoofed, 0 if normal lookup)
  Count_t  ttl;    // Time to Live
  Proto_t  l4proto;// Layer 4 protocol
  Byte_t   tos;    // Value for l3 Type-of-Service field
  // Probably need more later
};

#endif

