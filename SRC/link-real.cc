// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: link-real.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - Link base class
// George F. Riley.  Georgia Tech, Spring 2002

#include <math.h>
#include <stdlib.h>

//#define DEBUG_MASK 0x10
#include "debug.h"
#include "link.h"
#include "simulator.h"
#include "event.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "droptail.h"
#include "hex.h"
#include "linkmonitor.h"

using namespace std;

// Constructors
LinkReal::LinkReal()
    : Link(), notify(nil), linkMonitor(nil)
{
  ConstructorHelper(Link::DefaultRate(), Link::DefaultDelay());
}

LinkReal::LinkReal(Rate_t b, Time_t d)
    : Link(b, d), notify(nil), linkMonitor(nil)
{
  ConstructorHelper( b, d);
}

LinkReal::LinkReal(const LinkReal& c)
    : Link(c), notify(c.notify), linkMonitor(nil)
{ // Copy constructor
  busy = false;
  jitter = c.jitter->Copy();
  if (c.linkMonitor) linkMonitor = c.linkMonitor->Copy();
  bw = c.bw;
  delay = c.delay;
  weight = c.weight;
  bitErrorRate = c.bitErrorRate;
  bytesSent = 0;
  pktsSent = 0;
  utilizationStart= 0.0;
  debugVerbose = c.debugVerbose;
  DBG((Stats::linksCreated++));
}

// Destructor
LinkReal::~LinkReal()
{
  if (jitter) delete jitter;
  if (linkMonitor) delete linkMonitor;
}

// Assignment operator
LinkReal& LinkReal::operator=(const LinkReal& rhs)
{
  if (&rhs == this) return *this; // Self-Assignment
  cout << "Hello fro LR assignment" << endl;
  busy = false;
  jitter = rhs.jitter->Copy();
  if (rhs.linkMonitor) linkMonitor = rhs.linkMonitor->Copy();
  bw = rhs.bw;
  delay = rhs.delay;
  weight = rhs.weight;
  bitErrorRate = rhs.bitErrorRate;
  bytesSent = 0;
  pktsSent = 0;
  utilizationStart= 0.0;
  debugVerbose = rhs.debugVerbose;
  DBG((Stats::linksCreated++));
  return *this;
}

bool LinkReal::Transmit(LinkEvent* evRx, Interface* i, Time_t rxTime)
{
  if (useSeqEvents)
    { // Bypass central event list, using a fifo queue at receiver
      i->ScheduleRx(evRx,rxTime);
    }
  else
    { 
      Scheduler::Schedule(evRx, rxTime, i); 
    }
  return true;
}

bool LinkReal::TransmitWithDelay(Packet* p, Time_t extraDelay)
{
  // Schedule receipt of new packet
  // at time extraDelay + txTime + delay + jitter;
  Time_t txTime = ((Rate_t)p->Size() * 8) / Bandwidth(); // Time to transmit
  Time_t txDelay = Delay();
  Time_t jit = jitter->Value();
  DEBUG0((cout << "Link " << this
          << " transmitting with extraDelay " << extraDelay
          << " txtime " << txTime
          << " delay " << txDelay
          << " bandwidth " << Bandwidth() << endl));
  Time_t rxTime = extraDelay + txTime + txDelay + jit;
  InterfaceBasic* i = GetPeer(p);
  // Create a new packet receipt event
  LinkEvent* evRx = new LinkEvent(LinkEvent::PACKET_RX, p);
  if(bitErrorRate != 0.0)
    { // random loss requested
      Mult_t lossProb = 1.0 - pow(1.0 - bitErrorRate, (double)(p->Size() * 8));
      if (!berRng) berRng = new Uniform();
      Random_t rv = berRng->Value();
      evRx->hasBitError = (rv < lossProb);
    }
  DEBUG0((cout << "Scheduling RX interface " << i
          << " now " << Simulator::Now()
          << " rxTime " << rxTime
          << " time " << Simulator::Now() + rxTime << endl));
  i->ScheduleRx(evRx,rxTime);
  Stats::pktsTransmitted++;
  return true;
}

void LinkReal::Handle(Event* e, Time_t)
{
  LinkEvent* le = (LinkEvent*)e;
  switch(le->event) {
  case LinkEvent::PACKET_TX :
    TransmitComplete(le->numberBytes);
    break;
  default:
    break;
  }
  delete le;
}

void LinkReal::ResetUtilization()
{ // Start a new utilization measurement interval
  bytesSent = 0;
  pktsSent = 0;
  utilizationStart = Simulator::Now();
}

Mult_t LinkReal::Utilization()
{
  Time_t now = Simulator::Now();
  if (now == utilizationStart) return 0.0; // Avoid divide by zero
  return (bytesSent * 8) / ((now - utilizationStart) * bw);
}

void LinkReal::AddNotify(NotifyHandler* n, void* v)
{ // Add a notify client
  //notifications.push_back(Notification(n,v));
  if (notifications.empty())
    notifications.push_back(Notification(n,v));
  else {
    NList_t::iterator i;
    for (i = notifications.begin(); i != notifications.end(); ++i)
      if (*((Time_t*)v) < *((Time_t*)i->userData)) break;         
    notifications.insert(i, Notification(n,v));
  }
}

void LinkReal::Jitter(const Random& j)
{ // Set the desired transmit time jitter random variable
  if (jitter) delete jitter;
  jitter = j.Copy();
}

Time_t LinkReal::NextFreeTime()
{
  Time_t now = Simulator::Now();
  if (now > nextFreeTime)
    { // Time is in the past, adjust to now
      nextFreeTime = now;
      return now;
    }
  return nextFreeTime;
}

// Protected methods

void   LinkReal::TransmitComplete(Count_t b) // Previous transmit completed
{
  DEBUG0((cout << "adding " << b << " to bytesSent " << bytesSent << endl));
  bytesSent += b;     // Count total bytes sent
  pktsSent++;         // Count total packets sent
  busy = false;       // No longer busy
  // The SendNotification() below may result in another packet being
  // transmitted, which may overwrite the notification request.  Therefore
  // we need to copy it locally before calling SendNotificatiion()
  NotifyHandler* copyNotify = notify;


  SendNotification(); // Notify the interface(s) link not busy

  if (copyNotify)
    {
      copyNotify->Notify(nil); // Notify sender that tx is complete
    }


  DEBUG0((cout << "LinkReal::TransmitComplete on link " << endl));
  if (debugVerbose) cout << "LinkReal::TXC time " << Simulator::Now() << endl;
}


void LinkReal::SendNotification()
{ // Notify the entry in the notificatin list
  if (notifications.size() == 0) return;   // No pending
  Notification n = notifications.front();  // Get next notifier
  notifications.pop_front();               // And remove it
  n.handler->Notify(n.userData);           // Call the notification handler
}


#ifdef  TEST_BACKPLANE
Packet* TestImportExport(Packet* p);
#endif

bool LinkReal::TransmitHelper(Packet* p, Interface* self_if, Node* n, 
                              Interface* i, bool bCast)
{
  bool lost = false;
  if(bitErrorRate != 0.0)
    { // random loss requested
      Mult_t lossProb = 1.0 - pow(1.0 - bitErrorRate, (double)(p->Size() * 8));
      if (!berRng) berRng = new Uniform();
      Random_t rv = berRng->Value();
      lost = (rv < lossProb);
      //if (lost) cout << "Setting loss due to BER" << endl;
    }
  // Log with the link monitor if defined
  if (linkMonitor)
    {
      linkMonitor->Transmit(p);
    }
  
  
  // Note the object to notify when tx complete
  notify = p->notification;
  p->notification = nil;
  
  busy = true; // Set link busy
  Time_t txTime = ((Rate_t)p->Size() * 8) / Bandwidth(); // Time to transmit
  // Create and schedule the packet transmit complete event
  LinkEvent* evTx = new LinkEvent(LinkEvent::PACKET_TX);
  evTx->numberBytes = p->Size();
  Scheduler::Schedule(evTx, txTime, this);
  DEBUG(4,(cout << "Link txTime " << txTime
           << " bandwidth " << Bandwidth()
           << " delay " << Delay() 
           << " pktSize " << p->Size() 
           << " pktUid " << p->uid << endl));
  if (debugVerbose) cout << "Link scheduling link free at " 
                         << Simulator::Now() << " for future "
                         << txTime << endl;
  if (debugVerbose) cout << "pktSize " << p->Size()
                         << " bw " << Bandwidth()
                         << endl;
  nextFreeTime = Simulator::Now() + txTime; // Note time link will be free
  // Packet arrives at destination at the sum of three times
  // 1) Time to transmit the bits (size/bw)
  // 2) Propagation delay
  // 3) Jitter (random variation specified by caller)
  Time_t rxTime = txTime + Delay() + jitter->Value();
  //Scheduler::Schedule(evRx, rxTime, i);
  // New way below...more efficient than old way
  // Pass this directly to the interface for inclusion in fifo event queue
  // This works since p2p and ethernet links are FIFO.  Packets are received
  // at the far end of the link in the order sent.

  Time_t* ptime = new Time_t(nextFreeTime);
  //Interface* self_if = n->GetIfByLink(this); // INEFFICIENT!
  AddNotify(self_if, ptime); // GFR Comment out later

  if (!bCast)
    { // Not broadcast, just send single packet
      // Create and schedule the packet receive event
#ifdef  TEST_BACKPLANE
      // For testing the backplane, export and import the message
      Packet* p1 = TestImportExport(p);
      p = p1;
#endif
      // If the peer interface is a fbrx, we need a first bit event
      if (i->FirstBitRx())
        {
          LinkEvent* evRxFirst =
              new LinkEvent(LinkEvent::PACKET_FIRSTBIT_RX, p);
          evRxFirst->hasBitError = lost;
          Scheduler::Schedule(evRxFirst, Delay(), i);
        }
      LinkEvent* evRx = new LinkEvent(LinkEvent::PACKET_RX, p);
      evRx->hasBitError = lost; // Note possible bit error
      self_if->RegisterRxEvent(evRx);
      Transmit(evRx, i, rxTime); // Transmit the packet
    }
  else
    { // Send to all neighbors on this link
      NodeWeightVec_t nwv;
      AllNeighbors(nwv);
      //Node* thisNode = i->GetNode(); // For ignoring self
      for (NodeWeightVec_t::size_type j = 0; j < nwv.size(); ++j)
        {
          Node* node = nwv[j].node;
          Interface* iface = (Interface*)nwv[j].iface;
          if (!iface)
            {
              cout << "HuH?  Link broadcast has nil interface" << endl;
              continue;
            }
          if (node != n || RxBroadcast())
            { // Don't sent to self unless we receive our own broadcasts
              LinkEvent* evRx = new LinkEvent(LinkEvent::PACKET_RX, p->Copy());
              evRx->hasBitError = lost; // Note possible bit error
              Transmit(evRx, iface, rxTime);
            }
        }
      delete p; // Since we sent only copies of the packet, we must delete
    }
  Stats::pktsTransmitted++;
  return true;
}

LinkMonitor* LinkReal::AddLinkMonitor(const LinkMonitor& lm)
{ // Add a link monitor object to this link
  if (linkMonitor) delete linkMonitor;
  linkMonitor = lm.Copy();
  return linkMonitor;
}

// Private methods
void LinkReal::ConstructorHelper(Rate_t r, Time_t d)
{
  busy = false;
  if (defaultJitter)
    jitter = defaultJitter->Copy();
  else
    jitter = new Constant(0);
  bw = r;
  delay = d;
  weight = 1.0;
  bitErrorRate = 0.0;
  bytesSent = 0;
  pktsSent = 0;
  utilizationStart= 0.0;
  nextFreeTime = 0.0;
  debugVerbose = false;
  DBG((Stats::linksCreated++));
}

#ifdef  TEST_BACKPLANE
#include "backplane.h"
#include "tcp.h"
#include "ipv4.h"
#include "llcsnap.h"
#include "l2proto802.3.h"

#include "packet.h"
char*  bpBuf = nil;
int    bpBufSize = 0;

Packet* TestImportExport(Packet* p)
{
  DEBUG0((cout << "Hello from TestImportExport" << endl));
  if (!GetDSimHandle())
    { // Not initialized
      DSHandle_t dsimHandle = InitializeDSim(0);
      // Register the TCP and IP data items
      TCP::RegisterBackplane(dsimHandle);
      IPV4::RegisterBackplane(dsimHandle);
      cout << "Done Registering Items " << endl;
    }

  DEBUG0((cout << "Inquiring Message Size " << endl));
  int mSize = InquireMessageSize(); // Number of bytes needed
  DEBUG0((cout << "Message size is " << mSize << endl));
  if (bpBufSize < mSize)
    { // Existing buffer not big enough
      delete bpBuf;
      cout << "Allocating new bpbuf size " << mSize << endl;
      bpBuf = new char[mSize];
      bpBufSize = mSize;
    }
  DEBUG0((cout << "bpBuf is " << Hex8((unsigned long)bpBuf)
          << " size " << bpBufSize << endl));
  int outSize = mSize;
  DEBUG0((cout << "Exporting message" << endl));
#ifdef VERBOSE
  cout << "Exporting message, packet pdu size " << p->PDUs.size()
       << " pkt size " << p->Size()
       << " top " << p->top
       << endl;

  for (PDUVec_t::size_type k = 0; k < p->PDUs.size(); ++k)
    {
      PDU* pdu = p->PDUs[k];
      cout << "PDU " << k << " layer " << pdu->Layer()
           << " size " << pdu->Size() << endl;
    }
#endif
  ExportMessageWrapper((char*)p, nil, 0, bpBuf, &outSize);

  // Now import it back
  int outlth1 = 0; // no baggage
  Packet* p1 = new Packet();
  ImportMessageWrapper(bpBuf, outSize, (char*)p1,  nil, &outlth1);
  // The import creates the TCP and IP headers..now recreate the packet
  TCPHeader* tcp = TCP::bpHeader;
  IPV4Header* ip = IPV4::bpHeader;
  Size_t totalLength = ip->totalLength;
  Size_t dataLength = totalLength - tcp->Size() - ip->Size();
  // Add the data
  if (dataLength) p1->PushPDU(new Data(dataLength)); // Application data
  p1->PushPDU(tcp);                  // TCP Header
  p1->PushPDU(ip);                   // IP Header
  p1->PushPDU(new LLCSNAPHeader());  // LLCSNap header
  // L2 Header (802.3)
  p1->PushPDU(new L2802_3Header(MACAddr(), MACAddr(), p1->Size()));
#ifdef VERBOSE
  cout << "Imported message, packet pdu size " << p1->PDUs.size()
       << " pkt size " << p1->Size()
       << " top " << p1->top
       << endl;
  for (PDUVec_t::size_type k = 0; k < p1->PDUs.size(); ++k)
    {
      PDU* pdu = p1->PDUs[k];
      cout << "PDU " << k << " layer " << pdu->Layer()
           << " size " << pdu->Size() << endl;
    }
#endif
  delete p;
  // We need to nil out the backplane  headers for the next import
  TCP::bpHeader = nil;
  IPV4::bpHeader = nil;
  return p1;
}
#endif

