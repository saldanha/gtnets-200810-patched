// Convert to/from u-law and a-law voice data compression.
// George F. Riley, Georgia Tech, Summer 2003

#ifndef  __UA_LAW_H__
#define  __UA_LAW_H__
unsigned char ShortToULaw(short);
unsigned char ShortToALaw(short);
short         ULawToShort(unsigned char);
short         ALawToShort(unsigned char);
#endif
