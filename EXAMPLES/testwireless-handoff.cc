// Test the Wireless Handoff feature with animation.
// George F. Riley, Talal Jaafar, Georgia Tech, Spring 2004

#include <iostream>
#include <sstream>
#include <string>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "star.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "eigrp.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "tcp-tahoe.h"
#include "routing-eigrp.h"
#include "interface-wireless.h"
#include "wlan.h"
#include "mobility-specific-waypoint.h"
#include "mobility-random-waypoint.h"
#include "wireless-grid-rectangular.h"
#include "l2proto802.11.h"
#include "application-cbr.h"
#include "application-udpsink.h"
#include "qtwindow.h"
#include "globalstats.h"
#include "udp.h"
#include "queue.h"

#ifdef HAVE_QT
#include <qnamespace.h>
#include <qcolor.h>
#include <qcanvas.h>
#endif

using namespace std;

Meters_t gridX = 50;
Meters_t gridY = 50;

class TextEvent : public  Event 
{
public:
  TextEvent(QCanvasText* ct) 
      : t(ct) {}
public:
  QCanvasText* t;
};

class TextHandler : public Handler
{
public:
  void Handle(Event*, Time_t);
};

void TextHandler::Handle(Event* e, Time_t)
{
  TextEvent* te = (TextEvent*)e;
  // Just delete the text event and update the canvas
  delete te->t;
  Simulator::instance->GetQTWindow()->Canvas()->update();
  // and delete the event as we don't need it anymore
  delete e;
}

// Define the texthandler object
TextHandler textHandler;

void NodeSelected(Node* n)
{
  //cout << "Hello from NodeSelected, node " << n->Id() << endl;
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  if (!qtw) return; // Can't happen
  QCanvas* canvas = qtw->Canvas();
  string* nodeName = (string*)n->UserInformation();
  QCanvasText* qct;
  ostringstream ostr;
  ostr << "Node " << n->Id();
  string nn = ostr.str();
  if (nodeName)
    {
      nn += "\n" + *nodeName;
    }

  qct = new QCanvasText(nn.c_str(), canvas);
  // Place the text item near the node on the canvas
  QPoint p = qtw->LocationToPixels(n->GetLocation());
  qct->move(p.x() + 10, p.y() - 10);
  qct->show();
  // Schedule an event 1 sec in future to remove it
  TextEvent* te = new TextEvent(qct);
  Scheduler::Schedule(te, 1.0, textHandler);
  // Update the canvas
  canvas->update();
  qtw->ProcessEvents();
}

int main(int argc, char** argv)
{
  Random::GlobalSeed(31731,44543,425345,19367,48201,72333);
  Simulator s;
  s.HasMobility(true);
  int nLeaf = 8;
  int nWireless = 8;
  
  if (argc > 1) nLeaf = atol(argv[1]);
  if (argc > 2) nWireless = atol(argv[2]);
  
  bool noAnim = false;
  if (argc > 3) noAnim = true;

  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);
  
  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->Open("testwireless-handoff.txt");
  IPV4::Instance()->SetTrace(Trace::ENABLED);
  L2Proto802_3::GlobalSetTrace(Trace::ENABLED);
  
  Linkp2p lk(Rate("500Kb"), Time("10ms"));
  
  // Set size of animation region
  s.NewLocation(Location(0,0));
  s.NewLocation(Location(gridX, gridY));

  // Create the wireless grid with random mobility
  IPAddr lanIP("192.168.2.1");
  WirelessGridRectangular g(Location(0,0),  Constant(nWireless),
                            Uniform(0, gridX), Uniform(0, gridY),
                            lanIP);
  g.AddMobility(RandomWaypoint(g, Uniform(1,2), Uniform(1,5)));

  // Create the wired infrastructure
  Star star(nLeaf, lk, IPAddr("192.168.1.1"));
  Meters_t xBorder = gridX / 10.0;
  Meters_t yBorder = gridY / 10.0;
  star.BoundingBox(Location(xBorder, yBorder),
                   Location(gridX - xBorder, gridY - yBorder));
  
  // Random number generator for start times
  Uniform startRng(0, 0.1);   // For beacons
  Uniform udpStartRng(1, 2);  // For UDP apps
  
  
  // Get the wireless link object
  WirelessLink*  wlink = g.GetWLink();
  for (Count_t i = 0; i < wlink->NodeCount(); ++i)
    { // Set each mobile to wireless BSS mode
      Node* n = wlink->GetNode(i);
      n->SetRadioRange(gridX / 10);
      InterfaceWireless* wif = (InterfaceWireless*)wlink->GetIf(i);
      wif->SetOpMode(InterfaceWireless::BSS);
      // Start a CBR application at about 10 packets / sec
      Application* cbr = n->AddApplication(
          CBRApplication(IPAddr("192.168.1.1"), // Remote endpoint
                         10000,                 // Remote port
                         NO_PORT,               // No local port needed
                         Rate("40Kb"),          // 40kb is about 10 pkts/sec
                         512));                 // Packet size (bytes);
      cbr->Start(udpStartRng.Value());
    }
  
  // Now add a new wireless interface to each of the star leaf
  // nodes and set it to HOSTAP mode, to make the base stations.
  // Also add these interfaces to the wlan object
  IPAddr baseIP(lanIP & Mask(24) + 128);
  for (Count_t i = 0; i < star.LeafCount(); ++i)
    {
      Node* n = star.GetLeaf(i);
      InterfaceWireless* wif = wlink->AddNode(n, lanIP + i, Mask(24));
      wif->SetOpMode(InterfaceWireless::HOSTAP);
      n->SetRadioRange(gridX / 10);
      // Start a sink app so we don't get dropped pkts all the time
      Application* udp = n->AddApplication(UDPSink(10000));
      udp->Start(0); // Start immediately
    }

  if (!noAnim)
    {
      // Specify animation
      s.AnimateWirelessTx(true);
      s.PauseOnWirelessTx(false);
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("100us")); // 100us initial update rate
      // Specify the "NodeSelected" callback
      s.NodeSelectedCallback(NodeSelected);
    }
  s.Progress(1);
  s.StopAt(500);
  cout << "Starting Simulation" << endl;
  s.Run();
  cout << "Simulation complete" << endl;
  s.PrintStats();
  Stats::Print();
  cout << " rtsCount       " << L2Proto802_11::rtsCount << endl;
  cout << " rtsDelCount    " << L2Proto802_11::rtsDelCount << endl;
  cout << " ctsCount       " << L2Proto802_11::ctsCount << endl;
  cout << " ctsDelCount    " << L2Proto802_11::ctsDelCount << endl;
  cout << " dataCount      " << L2Proto802_11::dataCount << endl;
  cout << " dataDelCount   " << L2Proto802_11::dataDelCount << endl;
  cout << " ackCount       " << L2Proto802_11::ackCount << endl;
  cout << " ackDelCount    " << L2Proto802_11::ackDelCount << endl;
  cout << " beaconCount    " << L2Proto802_11::beaconCount << endl;
  cout << " beaconDelCount " << L2Proto802_11::beaconDelCount << endl;
  cout << " assocReqCount; " << L2Proto802_11::assocReqCount << endl;
  cout << " assocRepCount; " << L2Proto802_11::assocRepCount << endl;
  cout << " disassocCount; " << L2Proto802_11::disassocCount << endl;
  cout << " ctsTOCount; " << L2Proto802_11::ctsTOCount << endl;
  cout << " ackTOCount; " << L2Proto802_11::ackTOCount << endl;
  extern Count_t dfCopies;
  cout << " dfcopies    " << dfCopies << endl;
  cout << " udp headers " << UDP::totalUDP << endl;
  cout << " udp deleete " << UDP::totalUDPDel << endl;
  cout << " total qdrop " << Queue::globalQueueDrop << endl;
}
