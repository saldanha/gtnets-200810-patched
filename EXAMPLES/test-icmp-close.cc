// Test connections closing in response to ICMP unreachable messages
// George F. Riley.  Georgia Tech, Spring 2002

#include <iostream>

#include "simulator.h"
#include "application-tcpsend.h"
#include "duplexlink.h"
#include "queue.h"
#include "ratetimeparse.h"
#include "tcp-tahoe.h"
#include "dumbbell.h"
#include "icmp.h"
#include "args.h"
#include "validation.h"  

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  // Create the simulator object
  Simulator s;
  Count_t nleaf = 1;
  
  Arg("nleaf", nleaf, 1);
  Arg::ProcessArgs(argc, argv);
  
  // Open the trace file
  Trace* tf = Trace::Instance();
  if (!Arg::Specified("nolog")) tf->Open("test-icmp-close.txt");
  tf->IPDotted(true);

  // Increase detail of L3 trace messages
  IPV4::Instance()->DetailOn(IPV4::TOTALLENGTH);
  // Enable all l3 tracing
  IPV4::Instance()->SetTrace(Trace::ENABLED);


  // Create the topology, using default link
  Dumbbell b(nleaf, nleaf, 0.5, IPAddr("192.168.1.1"), IPAddr("192.168.2.1"));
  b.BoundingBox(Location(0,0), Location(1,1));
  
  // Enable ICMP on all nodes
  ICMP::Enable();
  TCP::LogFlagsText(true);    // Log flags in text mode
  
  // Create the TCP sending applications using TCPTahoe
  // But we have no servers, so we should get ICMP unreachable messages
  for (Count_t k = 0; k < nleaf; ++k)
    {
      TCPSend* pApp = (TCPSend*)b.Left(k)->AddApplication(
          TCPSend(b.Right(k)->GetIPAddr(),// Peer IP Address
                  80,                     // Peer port number
                  Constant(100000)));     // Number of bytes to send
      TCP* ctcp = pApp->l4Proto;
      ctcp->SetTrace(Trace::ENABLED);
      pApp->Start(k * 0.01);
    }

  s.StopAt(100.0);
  s.Progress(1.0);
  if (!Arg::Specified("noanimate") && !Validation::noAnimation)
    { // Enable animiation if specified on command line
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Run();
}

