#ifndef __node_taclane_h__
#define __node_taclane_h__

#include "node.h"
#include "node-taclane-impl.h"
#include "security_association.h"
#include "interface.h"


//Need to manually setup Security Associations
//Need to manually setup Routing

//Duplexlink.h has been modified so that interfaces cannot be added to a
//TACLANE node.
//This limits the TACLANE to 1 plain text interface and 1 cypher text
//interface each.

typedef enum TACLANEType { RB, GB, RG };

// Not much needed, just slightly different constructor.
// Everything else inherited from class Node
class TACLANENode : public Node, public SecurityAssociation {
public:
  TACLANENode(IPAddr_t, IPAddr_t, TACLANEType, SystemId_t sysId = 0);
  // Enabling or disabling the taclane allows easy testing of affect
  // of having taclane (or not) in the sim.  When disabled, the
  // taclane simply passes the packet unmodified (as any router does)
  void Enable(bool b) { enabled = b;}
  bool enabled;
};


#endif
