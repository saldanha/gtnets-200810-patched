// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: qtwindow.h 512 2007-08-22 20:27:39Z johnece $



// Georgia Tech Network Simulator - QT Graphics Window Interface
// George F. Riley.  Georgia Tech, Fall 2002

#ifndef __qtwindow_h__
#define __qtwindow_h__

#include <vector>
#include <list>

#include "common-defs.h"
#include "simulator.h"

#ifdef HAVE_QT
#include <qobject.h>
#include <qapplication.h>
#include <qcanvas.h>
#include <qlcdnumber.h>
#include <qslider.h>
#include <qvbox.h>
#include <qpushbutton.h>
#include <qpainter.h>
#endif

class Node;
class BlueNode;
class Interface;
class InterfaceWireless;
class InterfaceBasic;
class Link;
class Queue;
class QApplication;
class QTChildWindow;
class QCanvasItem;
class QCanvasPolygonalItem;
class QMainWindow;

typedef std::list<QTChildWindow*> ChildWindowList_t;

//Doc:ClassXRef
class NNIfLink { // Two nodes, link and receiving interface, and canvasitem
public:
  NNIfLink(Node* n1, Node* n2, InterfaceBasic* is, InterfaceBasic* id,
           Link* ls,Link* ld, QCanvasPolygonalItem* item)
    : s(n1), d(n2), ifaceSource(is), ifaceDest(id),
      linkSource(ls), linkDest(ld), canvasItem(item) { }
public:
  Node* s;
  Node* d;
  InterfaceBasic* ifaceSource;
  InterfaceBasic* ifaceDest;
  Link*      linkSource;
  Link*      linkDest;
  QCanvasPolygonalItem* canvasItem;
};

class NNQueue 
{
public:
  NNQueue(Queue* qq, Node* nn1, Node* nn2) : q(qq), n1(nn1), n2(nn2) {}
public:
  Queue* q;
  Node*  n1;
  Node*  n2;
};
  
typedef std::vector<QCanvasPolygonalItem*> CPItemVec_t;
typedef std::vector<QCanvasItem*>          CItemVec_t;
typedef std::vector<NNIfLink>              NNIfLVec_t;
typedef std::vector<NNQueue>               QueueVec_t;


#include "handler.h"
#include "event.h"

typedef int    QTCoord_t;
typedef double QTDCoord_t;

#define MAX_SLIDER 30

// Define the QT Update events
//Doc:ClassXRef
class QTEvent : public Event {
public:
  typedef enum { START, STOP, PAUSE, UPDATE,
                 WIRELESS_TX_START, WIRELESS_TX_END, 
                 BASEBAND_TX_START, BASEBAND_TX_END,
                 RECORD } QTEvent_t;
  QTEvent(QTEvent_t ev) : Event(ev), bluesrc(nil), src(nil), dst(nil), iFace(nil),
      range(0), iter(0) { };
public:
  BlueNode*    bluesrc;     // Bluetooth source node  for wireless transmit animation
  Node*    src;     // Source node  for wireless transmit animation
  Node*    dst;     // Destination node  for wireless transmit animation
  InterfaceWireless* iFace; // Interface for wireless  animation
  Meters_t range;   // Range of wireless transmission
  Count_t  iter;    // Iteration for wireless tx
};


class Node;
class QTEvent;

#ifdef HAVE_QT
// Define a non-filled circle canvas item. Used for wireless animation.
class QCanvasCircle : public QCanvasEllipse
{
public:
  QCanvasCircle(int radius, QCanvas * canvas) :
      QCanvasEllipse(2 * radius, 2 * radius, canvas)
  {
  }
  QCanvasCircle(QCanvas * canvas) : QCanvasEllipse(canvas)
  {
  }

  void setDiameter(int diameter)
  {
    setSize(diameter, diameter);
  }

  void setRadius(int radius)
  {
    setSize(2 * radius, 2 * radius);
  }

protected:
  void draw(QPainter & p)
  {
    p.setPen(pen());
    drawShape(p);
  }

  void drawShape(QPainter & p)
  {
    int penWidth = pen().width();
    if (penWidth > 4)
      {
        p.drawArc((int)(x() - width()/2 + penWidth /2),
                  (int)(y() - height()/2 + penWidth/2),
                  width() - penWidth,
                  height() - penWidth , 0,16*360);
      }
    else
      {
        p.drawArc((int)(x() - width()/2), int(y() - height()/2),
                  width(), height(), 0, 16*360);
      }
  }
};


//Doc:ClassXRef
class QTWindow : public QObject, public Handler {
  Q_OBJECT
public:
  QTWindow(bool);
  ~QTWindow();
public:
  void Handle(Event*, Time_t);
  void DisplayTopology();
  void DisplayTopologyAndReturn();
  void ProcessEvents();
  void AnimationUpdateRate(Time_t);
  void UpdateTopology(bool = true); // True if forced topo redisplay
  void AddBackgroundLines(const LocationVec_t&);
  bool HasQuit() {return quit;}
  void PlaybackMode(bool);
  bool PlaybackMode(){return playbackMode;}
  Mult_t PlaybackRate(){return currentUpdateRate;}
  void PlaybackPause();
  QPoint  LocationToPixels(const Location&); // Convert a location to pixels
  QPoint  LocationToPixelsAbs(const Location&); // Convert a location to pixels
  QTCoord_t NodePixelSize() const { return nodePixelSize; }
#ifdef MOVED_TO_WLAN
  void    WirelessTxStart(Node*, InterfaceWireless*, Node*, Meters_t);
  void    WirelessTxEnd(Node*, InterfaceWireless*, Node*, Meters_t);
#endif
  void    BasebandTxStart(BlueNode*, Meters_t);
  void    BasebandTxEnd(BlueNode*, Meters_t);
  QTCoord_t CanvasX() { return canvasX;}
  QTCoord_t CanvasY() { return canvasY;}
  QCanvas*  Canvas()  { return canvas; }
  QApplication* GetApp() { return app; }
 
 
public slots:
  void NewSliderValue(int);
  void Save();
  void Record(bool);
  void Play();
  void Stop();
  void Pause();
  void Quit();
  void Exit();
  void TimerDone();
public:
  void    Initialize();        // Initialize constants
  QPoint  NodeLocation(Node*); // Get the pixel coords, CENTER of the node
  void    DrawNode(Node*, Count_t);     // Draw the node
  void    DrawP2P(Node*, Node*);        // Draw a point to point link  
  // Redraw after failure/recovery
  void    RedrawP2P(InterfaceBasic*, InterfaceBasic*);
  void    AddChild(QTChildWindow*);     // Add a child
  void    DeleteChildWindow(QTChildWindow*);  // Remove a child
  void    UpdateSimTime();              // Update the simulation time display
  void    WaitWhilePaused();            // Wait until un-paused
private:
  int argc ;
private:
  void    DisplayAllNodes(bool = false);// Draw all nodes
#ifdef MOVED_TO_LINK
  void    DisplayAllPackets();          // Draw all in-flight packets
#endif
  void    DisplayAllQueues();           // Draw queues
  void    Clear();                      // Clear the display
#ifdef MOVED_TO_LINK
  void    DisplayOnePacket(Node*, Node*, Mult_t, Mult_t, const QColor&);
#endif
#ifdef MOVED_TO_WLAN
  void    WirelessTxStart(Node*, InterfaceWireless*, Node*, Meters_t, Count_t);
  void    WirelessTxEnd(Node*, InterfaceWireless*, Node*, Meters_t, Count_t);
#endif
  void    BasebandTxStart(BlueNode*, Meters_t, Count_t);
  void    BasebandTxEnd(BlueNode*, Meters_t, Count_t);
  void    RecordNextFrame(QTEvent*);
  // Private members
  QApplication* app;
  QCanvas*      canvas;
  QCanvasView*  view;
  QLCDNumber*   updateRate;
  QSlider*      slider;
  QLCDNumber*   simTime;
  QTimer*       readyTimer;
  QMainWindow*  mw;       // Main window 
  QVBox*        box;      // The container for the other windows
  QPushButton*  quitButton;
  QPushButton*  exitButton;
  QPushButton*  record;
  QPushButton*  save;
  QPushButton*  stop;
  QPushButton*  play;
  QPushButton*  pause;
  QTCoord_t     nodePixelSize;
  QTDCoord_t    zoomMult;
  QTCoord_t     border;   // size of border, x and y, both pixels
  QTCoord_t     canvasX;
  QTCoord_t     canvasY;
  QTEvent*      qtEvent;
  ChildWindowList_t childWindows;
  // Calculated by Initialize()
  QTDCoord_t    metersToPixelsX;
  QTDCoord_t    metersToPixelsY;
  Meters_t      smallestX;
  Meters_t      smallestY;
  Meters_t      largestX;
  Meters_t      largestY;
  bool          topologyDisplayed;
  bool          updateScheduled;
  bool          ready;
  // Controlling the animation
  Time_t        currentUpdateRate;
  bool          paused;
  bool          quit;
  bool          oneShot;
  bool          playbackMode;
  bool          recording;
  Count_t       recordingFrame;
  CItemVec_t    nodeItems;
  CPItemVec_t   linkItems;
  CPItemVec_t   packetItems;
  NNIfLVec_t    displayedLinks;
  QueueVec_t    displayedQueues;
  double        updateRates[MAX_SLIDER];
};

#else
class QTEvent;
//Doc:ClassXRef
    class QTWindow : public Handler {
public:
  QTWindow();
  ~QTWindow();
public:
  void Handle(Event*, Time_t);
  void DisplayTopology();
  void DisplayTopologyAndReturn();
  void ProcessEvents();
  void AnimationUpdateRate(Time_t);
private:
  int argc ;

/*
  QApplication* GetApp() ;
  void    AddChild(QTChildWindow*);     // Add a child
  void    DeleteChildWindow(QTChildWindow*);  // Remove a child
*/
};
#endif

#endif
