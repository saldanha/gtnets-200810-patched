// A simple two node, one flow GTNetS simulation
// George F. Riley, Georgia Tech, Summer 2003

#include "simulator.h"
#include "node.h"
#include "linkp2p.h"
#include "ratetimeparse.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "rng.h"
#include "tcp-tahoe.h"
#include "validation.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  Node* n1 = new Node();
  Node* n2 = new Node();
  n1->SetLocation(0,0);
  n2->SetLocation(1,0);
#ifdef HAVE_QT
  n1->Color(Qt::blue);
  n2->Color(Qt::red);
#endif
  
  Linkp2p link(Rate("1Mb"), Time("10ms"));
  
  n1->AddDuplexLink(n2, link, 
                    IPAddr("192.168.1.1"), Mask(32),
                    IPAddr("192.168.1.2"), Mask(32));
  TCPServer* server = (TCPServer*)n1->AddApplication(TCPServer());
  server->BindAndListen(80); // Bind to port 80
  TCPSend*   client = (TCPSend*)n2->AddApplication(
      TCPSend(n1->GetIPAddr(), 80, Constant(10000)));
  client->Start(0);  // Start the client at time 0
  s.StopAt(10.0);
  if (!Validation::noAnimation)
    {
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
      s.StartAnimation(0, true);               // Start the animation at time 0
    }
  s.Run();                                 // Run the simulation
  std::cout << "Simulation Complete" << std::endl;
  s.PrintStats();
}

  
  
