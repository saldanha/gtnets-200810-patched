// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


// Georgia Tech Network Simulator - Bluetooth LMP class
// George F. Riley, Georgia Tech, Spring 2004

#include <iostream>
#include <string.h>
#include <assert.h>

#include "lmp.h"
#include "l2cap.h"

using namespace std;
/////////////////////
//
//LMPPacket class
//
////////////////////
LMPPacket::LMPPacket(bool bTranId, uChar ucOpCode){
	
	ucOpCodeTransId = (char) bTranId;
	ucOpCodeTransId = ucOpCodeTransId | ( ucOpCode << 1);
}
	
LMPPacket::~LMPPacket() {
	
}
///////////////////////////
//*************************
//LMP class implementation
//*************************
//////////////////////////

//General response that accepts.
void
LMP::SendAccepted(bool bTranId, uChar ucOpCode) {
	DataRequest(OPCODE_ACCEPTED, bTranId, sizeof(ucOpCode), &ucOpCode);
	cout <<"Send Accept at LMP layer. " <<endl;
	return;
}

void
LMP::RecvAccepted(LMPPacket *pPacket) {
	//switch case
	uChar ucOpCode;
	
	memcpy(&ucOpCode, &(pPacket->Content), sizeof(ucOpCode));
	cout <<"Received ACCEPT with code = "<<dec <<(int)ucOpCode <<endl;
	switch(ucOpCode) {
		case OPCODE_HOST_CONNECTIONREQ:
			{
			//begin the procedure for pairing, authentication, encription.

			//uChar ucRandom = (1+rand())%255;
			uChar ucRandom[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8
					,9, 10, 11, 12, 13, 14, 15};
			bool bTranId;
			if(pBaseBand->GetRole() == MASTER)
				bTranId = 0;
			else
				bTranId = 1;
			SendLMPAuRand(16, bTranId, (uChar *)&ucRandom);
			}
			break;
		case OPCODE_QOS_REQ:
			//validate the QoS
			usTpoll = usTpollReq;
			ucNbc = ucNbcReq;
			break;
		case OPCODE_MAX_SLOTREQ:
			//validate the Max Slot from now on.
			ucMaxSlot = ucMaxSlotReq;
			break;
		case OPCODE_SNIFF_REQ:
			//validate sniff mode
			bSniffEnabled = true;
			ScheduleTimer(LMPEvent::SNIFF_TIMEOUT, SniffTimeout, (usDsniff-1)*SLOTTIME);
			ulSniffClk = pBaseBand->GetClk() + (usDsniff - 1)*2;		
			break;
		case OPCODE_HOLD_REQ:
			//validate hold mode
			ScheduleTimer(LMPEvent::HOLD_TIMEOUT, HoldTimeout, (usSlotStart-1)*SLOTTIME);
			break;
		case OPCODE_PARK_REQ:
			//validate park mode
			bParkEnabled = true;
			ScheduleTimer(LMPEvent::PARK_TIMEOUT, ParkTimeout, (usDsniff-1)*SLOTTIME);
			break;
		default:
			cout << "unknown opcode in contents. " <<endl;
	}
	
	return;		
}

void
LMP::SendNotAccepted(bool bTranId, uChar ucOpCode,uChar ucReason) {
	uChar ucContent[2];
	ucContent[0] = ucOpCode;
	ucContent[1] = ucReason;

	DataRequest(OPCODE_NOT_ACCEPTED, bTranId, (uChar)sizeof(ucContent),
				   	(uChar *)&ucContent);
	return;
}

void
LMP::RecvNotAccepted(LMPPacket *pPacket) {
	uChar ucLocalOpt;
	uChar ucReason;

	memcpy(&ucLocalOpt, pPacket->Content, sizeof(ucLocalOpt));
	memcpy(&ucReason, (pPacket->Content) + sizeof(ucLocalOpt), 
					sizeof(ucReason));
	cout<<"Operation code: "<< (int) ucLocalOpt<<" rejected. Reason: "
			<<(int)ucReason<<endl;
	return;
}

////////////////////////
//
//Authentication session
//
////////////////////////

//Send authentication random. 
//
void
LMP::SendLMPAuRand(uChar ucLen, bool bTranId, uChar *pRandom) {
	assert(ucLen==16);
	cout << "Send Authentication random " <<endl;
	DataRequest(OPCODE_AU_RAND, bTranId, ucLen, pRandom);
	return;
}

void
LMP::RecvAuRand(LMPPacket *pPacket, uChar ucAllLen) {
	
	cout << "Receive a Authentication random" <<endl;
	BdAddr LocalAddr = pBaseBand->GetMyAddr();
	uLong ulRsp = 0; // the calculated response.
	uChar ucLen = ucAllLen - pPacket->GetHdrLength();
	uChar ucOpCode = pPacket->GetOpCode();
	bool bTranId = 0;
	if(pBaseBand->GetRole() == MASTER)
		bTranId = 1; //this transcation initiated by slave.
	if(pLinkKey == NULL) {
		SendNotAccepted(bTranId, ucOpCode, KEY_MISSING);
		cout << "Reject because of key-missing." <<endl;
		return;
	}
	CalRsp(ucLen, pPacket->Content, LocalAddr, pLinkKey, ulRsp);
			//ulRsp is a reference 
	
	//Send response to authentication.
	DataRequest(OPCODE_SRESPONSE,bTranId, sizeof(ulRsp), (uChar *)&ulRsp);
	cout << "Send response to authentication" <<endl;
	
	//delete pPacket;
	return;
}

//Receive the authentication response.
void 
LMP::RecvRsp(LMPPacket *pPacket, uChar ucAllLen) {
	//ucChar ucOpCode = pPacket->GetOpCode();
	uChar ucLen = ucAllLen - pPacket->GetHdrLength();
	assert(ucLen == sizeof(uLong));
	
	cout << "Received response to authentication" <<endl;
	uLong ulRsp;
	memcpy(&ulRsp, pPacket->Content, sizeof(uLong));

	uLong ulRspExpect = 0;
	bool bTranId = bTransactionId;

	CalRsp(16, pPacket->Content,RemoteAddr, pLinkKey, ulRspExpect);
		//ulRspExpect is a reference.

	if(ulRsp != ulRspExpect) {
		//end the connection.
		uChar ucReason = AUTHENTICATION_FAILURE;
		if(pBaseBand->GetRole() == MASTER)
			bTranId = 0;
		else
			bTranId = 1;

		SendDetach(bTranId, ucReason); 
		return;
	}
	
	//delete pPacket;
	
	//success. Continue to do!
	//Send Setup Complete message to paged unit.
	DataRequest(OPCODE_SETUP_COMPLETE, bTranId, 0, NULL);
	return;
}

void
LMP::CalRsp(uChar ucLen, uChar *pRandom, BdAddr LocalAddr, 
				uChar *pLinkKey, uLong &ulRsp) {
	//TODO: to fill
	return;
}

////////////////////////////
//
//Clock offset session
//
////////////////////////////
void
LMP::ClockOffsetReq() {
	bool bTranId = bTransactionId;
	DataRequest(OPCODE_CLKOFFSET_REQ, bTranId, 0, NULL);
	return;
}

//receive Clock Offset Request and respond it.
//
void
LMP::RecvClockOffsetReq(LMPPacket *pPacket) {
	bool bTranId =!bTransactionId ; 
	//Send clock offset to this request.
	DataRequest(OPCODE_CLKOFFSET_RSP, 
					bTranId,
					sizeof(usClockOffset),
					(uChar *)&usClockOffset);

	//delete pPacket;
	return;
}

//
void
LMP::RecvClockOffsetRsp(LMPPacket *pPacket) {
	uShort usOffset;
	memcpy(&usOffset, pPacket->Content, sizeof(usOffset));
	//save it, associated with devices.
	
	//delete pPacket;
	return;
}

void
LMP::SendDetach(bool bTranId, uChar ucReason){
	//TODO: stop L2CAP packet transmission first
	
	//Call DataRequest to send the packet.
	DataRequest(OPCODE_DETACH, bTranId, sizeof(ucReason),&ucReason);
	
	//Now start a timer for 6*Tpoll.
	//    wait for baseband level ackownledgement
	//if getting such ack before expiring,
	//    schedule a timer for 3*Tpoll.
	//    and after this timer expires, AM_ADDR can be re-used.
	//else
	//    drop the link.
	//    schedule timer for T link supervision timeout
	//    after it expires, AM_ADDR can be reused.
	return;
}

void 
LMP::ScheduleTimer(Event_t e, LMPEvent *& ev, Time_t t) {
	if(ev) {
		;//do something?
	}else {
		ev = new LMPEvent(e);
		timer.Schedule(ev, t, this);
	}
}

void
LMP::CancelTimer(LMPEvent*& ev, bool delTimer) {
	if(ev!=NULL) {
		timer.Cancel(ev);
		if (delTimer) {
			delete ev;
			ev = nil;
		}
	}
	return;
}

void LMP::Timeout(TimerEvent *ev) {
	LMPEvent *pEvent = (LMPEvent *)ev;
	switch(pEvent->event) {
		case LMPEvent::HELLO_TIMEOUT:
			HelloTimeout = NULL;
			cout <<" detach timeout."<<endl;
			break;
		case LMPEvent::SEND_SNIFF:
			SendSniffTimeout = NULL;
			cout<<"Sending sniff request."<<endl;
			SendSniffReq(usTsniff, usSniffAttemp, usSniffTo);
			break;
		case LMPEvent::SNIFF_TIMEOUT:
			SniffTimeout = NULL;
			CancelTimer(SniffAttempTimeout, true);
			cout <<" Enter sniff."<<endl;
			cout <<" My role is "<<pBaseBand->GetRole()<<endl;
			//start L2CAP traffic
			isSniff = true;
			ScheduleTimer(LMPEvent::SNIFFATTEMP_TIMEOUT, SniffAttempTimeout, usSniffAttemp*SLOTTIME);
			ScheduleTimer(LMPEvent::SNIFF_TIMEOUT, SniffTimeout, usTsniff*SLOTTIME);
			break;
		case LMPEvent::SNIFFATTEMP_TIMEOUT:
			SniffAttempTimeout = NULL;
			cout <<" From sniff to sleep."<<endl;
			cout <<" My role is "<<pBaseBand->GetRole()<<endl;
			//stop L2CAP traffic
			isSniff = false;
			break;
		case LMPEvent::SEND_HOLD:
			cout<<"Sending hold request."<<endl;
			SendHoldReq(usSlotStart, usSlotCount);
			onInterval = onTime->Value()+(usSlotStart+usSlotCount)*SLOTTIME;
			cout<<"onInterval"<< onInterval<<endl;
			delete pEvent;
			pEvent = NULL;
			SendHoldTimeout = NULL;
			ScheduleTimer(LMPEvent::SEND_HOLD, SendHoldTimeout, onInterval);
			break;
		case LMPEvent::HOLD_TIMEOUT:
			cout<<"Enter hold mode."<<endl;
			isHold = true;
			HoldTimeout = NULL;
			ScheduleTimer(LMPEvent::WAKEUP_TIMEOUT, WakeupTimeout, usSlotCount*SLOTTIME);
			break;
		case LMPEvent::WAKEUP_TIMEOUT:
			cout<<"Hold mode wakeup."<<endl;
			isHold = false;
			WakeupTimeout = NULL;
			ulHoldSlot += usSlotCount;
			cout<<"Number of hold slots "<< ulHoldSlot<<endl;
			break;
		case LMPEvent::SEND_PARK:
			SendParkTimeout = NULL;
			cout<<"Sending park request."<<endl;
			SendParkReq(usTb, ucNb, ucDeltaB, ucNbsleep, ucDaccess, ucTaccess, 
				ucNaccslots, ucNpoll, ucMaccess);
			break;
		case LMPEvent::PARK_TIMEOUT:
			ParkTimeout = NULL;
			cout<<"Enter park."<<endl;
			isBeacon = true;
			ucParkCount--;
			if(ucParkCount>0)
				ScheduleTimer(LMPEvent::PARK_BEACON_TIMEOUT, ParkBeaconTimeout, ucDeltaB*SLOTTIME);
			else
				ucParkCount = (ucTaccess/2)*ucMaccess;	//number of beacon slots
			ScheduleTimer(LMPEvent::PARK_ACCESS_TIMEOUT, ParkAccessTimeout, ucDaccess*SLOTTIME);
			if(pBaseBand->GetRole()==MASTER)
				ScheduleTimer(LMPEvent::PARK_TIMEOUT, ParkTimeout, usTb*SLOTTIME);
			else	//Slave
				ScheduleTimer(LMPEvent::PARK_TIMEOUT, ParkTimeout, (ucNbsleep*usTb)*SLOTTIME);
			//TODO: Master sends one broadcast packet to parked slave.
			break;
		case LMPEvent::PARK_BEACON_TIMEOUT:
			ParkBeaconTimeout = NULL;
			cout<<"Park beacon timeout."<<endl;
			isBeacon = true;
			ucParkCount--;
			if(ucParkCount>0)
				ScheduleTimer(LMPEvent::PARK_BEACON_TIMEOUT, ParkBeaconTimeout, ucDeltaB*SLOTTIME);
			else
				ucParkCount = (ucTaccess/2)*ucMaccess;	//number of beacon slots
			break;
		case LMPEvent::PARK_ACCESS_TIMEOUT:
			ParkAccessTimeout = NULL;
			cout<<"Park access timeout."<<endl;
			isBeacon = true;
			isAccess = true;
			ucParkCount--;
			if(ucParkCount>0)
				ScheduleTimer(LMPEvent::PARK_ACCESS_TIMEOUT, ParkAccessTimeout, 2*SLOTTIME);
			else
				ucParkCount = ucNb;
			break;
		default:
			cout <<"Wrong!" <<endl;
			break;
	}
	if(pEvent)
		delete pEvent;
	return;
}
				
void
LMP::RecvDetach(LMPPacket *pPacket) {
	uChar ucReason;
	memcpy(&ucReason, pPacket->Content, sizeof(ucReason));
	cout << "Detach reason = %c" << ucReason;

	if(pBaseBand->GetRole() == MASTER)
		ScheduleTimer(LMPEvent::HELLO_TIMEOUT,HelloTimeout,
						6*usTpoll);
	else
		ScheduleTimer(LMPEvent::HELLO_TIMEOUT,HelloTimeout,
						3*usTpoll);
	//TODO: free all resources allocated for LMP
	//		tear down the link.
	//      Notify upper layer on this.

	//delete pPacket;
	return;
}

//SendQoS helper function.
void
LMP::QoSHelper(bool bTranId, uShort usTpollIn, uChar ucNbcIn, 
				uChar opcode) {
	
	uChar contents[3];
	
	memcpy(&contents, &usTpollIn, sizeof(usTpollIn));
	contents[2] = ucNbcIn;
	DataRequest(opcode, bTranId, (uChar)sizeof(contents)
				   	,(uChar *)&contents);
	return;
}

//this function used by master to notify slave
//slave can not reject.
void
LMP::SendQoS() {
	assert(pBaseBand->GetRole() == MASTER);
	bool bTranId = 0;
	QoSHelper(bTranId, usTpoll, ucNbc, OPCODE_QOS);
	return;
}

//slave got the notification
void
LMP::RecvQoS(LMPPacket *pPacket, uChar ucAllLen) {
	assert(pBaseBand->GetRole() == SLAVE);
	//expect 3 bytes.
	assert((ucAllLen - pPacket->GetHdrLength())==3);

	memcpy(&usTpoll,pPacket->Content, sizeof(usTpoll));
	memcpy(&ucNbc, pPacket->Content + sizeof(usTpoll), 
					sizeof(ucNbc));
	//delete pPacket;
	return;
}

void
LMP::SendQoSReq(uShort usLocalTpoll, uChar ucLocalNbc) {
	//store it, for convenience of acceptance.
	bool bTranId = bTransactionId;
	usTpollReq = usLocalTpoll; 
	ucNbcReq = ucLocalNbc;
	QoSHelper(bTranId, usTpoll, ucNbc, OPCODE_QOS_REQ);
	return;
}

void
LMP::RecvQoSReq(LMPPacket *pPacket, uChar ucAllLen) {
	assert((ucAllLen - pPacket->GetHdrLength())==3);
	
	uShort usLocalTpoll = 0;
	uChar ucLocalNbc = 0;
	bool bTranId = !bTransactionId;
	
	memcpy(&usLocalTpoll, pPacket->Content, sizeof(usLocalTpoll));
	if(pBaseBand->GetRole() == SLAVE)
		memcpy(&ucLocalNbc, pPacket->Content, sizeof(ucLocalNbc));

	//now determine if I should accept the QoS request.
	
	//OK, accept.
	usTpoll = usLocalTpoll;
	if(pBaseBand->GetRole() == SLAVE)
		ucNbc = ucLocalNbc;
	
	SendAccepted(bTranId, OPCODE_QOS_REQ);

	//delete pPacket;
	return;
}

/*
  The PDUs regarding to max slot parameters 
  could be sent at anytime after connection setup is complete.
 */

//Send the available number of slots to remote device
void
LMP::MaxSlot() {
	bool bTranId = bTransactionId;
	DataRequest(OPCODE_MAX_SLOT,bTranId, sizeof(ucMaxSlotAvailable),
					&ucMaxSlotAvailable);
	return;
}

//Receive the MaxSlot report from remote.
void
LMP::RecvMaxSlot(LMPPacket *pPacket, uChar ucAllLen) {
	assert((ucAllLen - pPacket->GetHdrLength())==1);
	memcpy(&ucMaxSlot,pPacket->Content, sizeof(ucMaxSlot));
	//delete pPacket;
	return;
}

//Send the slot request to remote device
//
//Note if I could not get what I requested, then reset it to default
//		value = 1.
void
LMP::SendMaxSlotReq(uChar MaxSlot) {
	//save it, for convenience of acceptance.
	bool bTranId = bTransactionId;
	ucMaxSlot = MaxSlot;
	DataRequest(OPCODE_MAX_SLOTREQ, bTranId, sizeof(MaxSlot),
					&MaxSlot);
	return;
}
	
//Receive the MaxSlot request from remote.
void
LMP::RecvMaxSlotReq(LMPPacket *pPacket, uChar ucAllLen) {
	assert((ucAllLen - pPacket->GetHdrLength())==1);
	bool bTranId = !bTransactionId;
	uChar ucLocalMaxSlot;
	memcpy(&ucLocalMaxSlot,pPacket->Content, sizeof(ucLocalMaxSlot));
	if(ucLocalMaxSlot <=ucMaxSlotAvailable)
		SendAccepted(bTranId, OPCODE_MAX_SLOTREQ);
	else
		SendNotAccepted(bTranId,OPCODE_MAX_SLOTREQ, 
						UNSUPPORTED_PARAMETER_VALUE);
	//delete pPacket;
	return;
}

//Set supervision timeout
void
LMP::SendSupervisionTO(uShort usTimeout) {
	assert(pBaseBand->GetRole() == MASTER);
	bool bTranId = bTransactionId;
	DataRequest(OPCODE_SUPERVISION_TIMEOUT,bTranId, 
					sizeof(usTimeout), (uChar *)&usTimeout);
	return;
}

void
LMP::RecvSupervisionTO(LMPPacket *pPacket) {
	memcpy(&usSupervisionTimeout, pPacket->Content, 
					sizeof(usSupervisionTimeout));
	//delete pPacket;
	return;
}

//When the paging device wishes to create a connection involving
// layers above LM, it sends LMP_Host_connection_req.
//
void
LMP::HostConnectionReq() {
	assert(pBaseBand->GetRole() == MASTER);
	bool bTranId = bTransactionId;
	DataRequest(OPCODE_HOST_CONNECTIONREQ, bTranId, 0, NULL);
	cout << "Send a Connection Request at LMP layer." <<endl;
	return;
}

void
LMP::RecvHostConnectionReq(LMPPacket *pPacket) {
	//I receive a host connection request.
	bool bTranId = !bTransactionId;

	cout << "Received a Connection Request at LMP layer." <<endl;
	//accept it.
	SendAccepted(bTranId, OPCODE_HOST_CONNECTIONREQ);
	//delete pPacket;
	return;
}

void
LMP::RecvSetupComplete(LMPPacket *pPacket) {
	//delete pPacket;
	//Note: although we can modify pPacket a bit
	//and sent back, it is not convenient to do this.
	RoleType role = pBaseBand->GetRole();
	cout << "Received SetupComplete " <<endl;
	if(role == SLAVE) {
	//if(pBaseBand->GetRole() == SLAVE) {
		bool bTranId = 0; //this SetupComplete initiated by master
		//flag Slave recv POLL after LMP
		pBaseBand->bStartPoll = true;	
		DataRequest(OPCODE_SETUP_COMPLETE, bTranId, 0, NULL);
	}

	//the link connection between both units is established.
	//TODO: notify L2CAP.
	cout << "<< LMP layer ready.>> \n" <<endl;
	if(role == MASTER)
		pBaseBand->LMPReadyIndication();
	return;
}

//Above all stuffs that send command will use the following one instead
void
LMP::DataRequest(uChar ucOpCode,  //the Operation code
				bool bTranId,// the transaction id
				uChar ucLen, //the length of pContent
				uChar *pContent) {
	LMPPacket *pPacket = new LMPPacket( bTranId,ucOpCode);

	char *pTemp = new char[ucLen + sizeof(uChar)]; //including a OpCode
	memcpy(pTemp, (void *)pPacket, sizeof(uChar));
	memcpy(pTemp + sizeof(uChar), pContent, ucLen); 
	//call Baseband DataRequest to send it.
	pBaseBand->DataRequest(LM_CH, ucLen + sizeof(uChar),
				   (uChar *)pTemp, RemoteAddr);
	delete pPacket;
	return;
}

void
LMP::DataIndication(uChar ucFlow, uShort usLen, uChar *pData) {

	LMPPacket *pPacket = (LMPPacket *)pData;
	cout << "LMP packet length = "<<usLen <<endl;
	cout << "Received a LMP packet at LMP layer" <<endl;

	switch(pPacket->GetOpCode()) {
		case OPCODE_ACCEPTED:
			RecvAccepted(pPacket);
			break;
		case OPCODE_NOT_ACCEPTED:
			RecvNotAccepted(pPacket);
			break;
		case OPCODE_CLKOFFSET_REQ:
			RecvClockOffsetReq(pPacket);
			//Send response in this function.
			break;
		case OPCODE_CLKOFFSET_RSP:
			RecvClockOffsetRsp(pPacket);
			break;
		case OPCODE_DETACH:
			RecvDetach(pPacket);
			break;
		case OPCODE_AU_RAND:
			RecvAuRand(pPacket, usLen);
			break;
		case OPCODE_SRESPONSE:// authentication response
			RecvRsp(pPacket, usLen);
			break;
		case OPCODE_QOS:
			RecvQoS(pPacket, usLen);
			break;
		case OPCODE_QOS_REQ:
			RecvQoSReq(pPacket, usLen);
			break;
		case OPCODE_MAX_SLOT:
			RecvMaxSlot(pPacket, usLen);
			break;
		case OPCODE_MAX_SLOTREQ:
			RecvMaxSlotReq(pPacket, usLen);
			break;
		case OPCODE_HOST_CONNECTIONREQ:
			RecvHostConnectionReq(pPacket);
			break;
		case OPCODE_SETUP_COMPLETE:
			RecvSetupComplete(pPacket);
			break;
		case OPCODE_SUPERVISION_TIMEOUT:
			RecvSupervisionTO(pPacket);
			break;
		case OPCODE_SNIFF_REQ:
			RecvSniffReq(pPacket);
			break;
		case OPCODE_UNSNIFF_REQ:
			RecvUnsniffReq(pPacket);
			break;
		case OPCODE_HOLD_REQ:
			RecvHoldReq(pPacket);
			break;
		case OPCODE_PARK_REQ:
			RecvParkReq(pPacket);
			break;
		default:
			cout <<"Unsupport command packet types" <<endl;
			break;
	}
}

//Sniff methods
void
LMP::SetSniff(Time_t tSend, uShort usTsniff, 
		uShort usSniffAttemp, uShort usSniffTo) {
	this->usTsniff = usTsniff;
	this->usSniffAttemp = usSniffAttemp;
	this->usSniffTo = usSniffTo;
	ScheduleTimer(LMPEvent::SEND_SNIFF, SendSniffTimeout, tSend);
	return;
}

void
LMP::InitSniffPara(uShort usTsniff) {
	uLong ulClk = pBaseBand->GetClk();
	ulClk = ulClk & 0x0FFFFFFF;
	ucFlag = (ulClk >> 27);
	if(ucFlag) { //clk27 =1; clk27' = 0
		uLong ulClk_26_1 = (ulClk >> 1) & 0x03FFFFFF;
		usDsniff = ulClk_26_1 % usTsniff;
	} else {
		usDsniff = (ulClk >> 1) % usTsniff;
	}
	return;
}

void
LMP::InitParkPara(uShort usTb, uChar ucNbsleep) {
	uLong ulClk = pBaseBand->GetClk();
	ulClk = ulClk & 0x0FFFFFFF;
	ucFlag = (ulClk >> 27);
	if(ucFlag) { //clk27 =1; clk27' = 0
		uLong ulClk_26_1 = (ulClk >> 1) & 0x03FFFFFF;
		usDsniff = ulClk_26_1 % usTb;
		ucDbsleep = ((ulClk_26_1 % (ucNbsleep * usTb)) - usDsniff)/usTb;
	} else {
		usDsniff = (ulClk >> 1) % usTb;
		ucDbsleep = ((ulClk % (ucNbsleep * usTb)) - usDsniff)/usTb;
	}
	return;
}

void
LMP::SendSniffReq(uShort usTsniff, uShort usSniffAttemp, uShort usSniffTo) {
	InitSniffPara(usTsniff);
	SniffReq sReq;
	sReq.ucFlag = ucFlag;
	sReq.usDsniff = usDsniff;
	sReq.usTsniff = usTsniff;
	sReq.usSniffAttemp = usSniffAttemp;
	sReq.usSniffTo = usSniffTo;
	DataRequest(OPCODE_SNIFF_REQ, bTransactionId, sizeof(SniffReq), 
			(uChar *)&sReq);
	cout << "Send a Sniff Request at LMP layer." <<endl;
	return;
}

void
LMP::RecvSniffReq(LMPPacket *pPacket) {
	cout<<"Receive Sniff Request at LMP layer."<<endl;
	bSniffEnabled = true;
	bool bTranId = !bTransactionId;
	SniffReq sReq;
	memcpy(&sReq, pPacket->Content, sizeof(sReq));
	ucFlag = sReq.ucFlag;
	usDsniff = sReq.usDsniff;
	usTsniff = sReq.usTsniff;
	usSniffAttemp = sReq.usSniffAttemp;
	usSniffTo = sReq.usSniffTo;
	
	ulSniffClk = pBaseBand->GetClk() + usDsniff * 2;		
	ScheduleTimer(LMPEvent::SNIFF_TIMEOUT, SniffTimeout, usDsniff*SLOTTIME);
	SendAccepted(bTranId, OPCODE_SNIFF_REQ);
	//TODO: process, get the Sniff parameters 
	// decide whether accept it.
	return;
}

void 
LMP::SendUnsniffReq() {
	bool bTranId = bTransactionId;
	DataRequest(OPCODE_UNSNIFF_REQ, bTranId, 0, NULL);
	return;
}

void 
LMP::RecvUnsniffReq(LMPPacket *pPacket) {
	cout<<"Received a Unsniff Request at LMP layer."<<endl;
	//TODO: do something, to stop sniff
	return;
}

uShort
LMP::GetSniffAttemp() {
	return usSniffAttemp;
}

uShort
LMP::GetSniffTo() {
	return usSniffTo;
}

void
LMP::SetHold(Time_t tSend, uShort usSlotStart, uShort usSlotCount, const Random& ontime) {
	ScheduleTimer(LMPEvent::SEND_HOLD, SendHoldTimeout, tSend);
	this->usSlotStart = usSlotStart;
	this->usSlotCount = usSlotCount;
	onTime = ontime.Copy();
	return;
}

void
LMP::SendHoldReq(uShort usSlotStart, uShort usSlotCount) {
	HoldReq hReq;
	hReq.usSlotStart = usSlotStart;
	hReq.usSlotCount = usSlotCount;
	DataRequest(OPCODE_HOLD_REQ, bTransactionId, sizeof(hReq), (uChar *)&hReq);
	return;
}

void
LMP::RecvHoldReq(LMPPacket *pPacket) {
	cout<<"Received a Hold Request at LMP layer."<<endl;
	bool bTranId = !bTransactionId;
	HoldReq hReq;
	memcpy(&hReq, pPacket->Content, sizeof(hReq));
	usSlotStart = hReq.usSlotStart;
	usSlotCount = hReq.usSlotCount;
	ScheduleTimer(LMPEvent::HOLD_TIMEOUT, HoldTimeout, usSlotStart*SLOTTIME);
	SendAccepted(bTranId, OPCODE_HOLD_REQ);
	return;
}

void
LMP::SetPark(Time_t tSend, uShort usTb, uChar ucNb, uChar ucDeltaB, 
	uChar ucNbsleep, uChar ucDaccess, uChar ucTaccess, 
	uChar ucNaccslots, uChar ucNpoll, uChar ucMaccess) {
	this->usTb = usTb;
	this->ucNb = ucNb;
	this->ucDeltaB = ucDeltaB;
	this->ucNbsleep = ucNbsleep;
	this->ucDaccess = ucDaccess;
	this->ucTaccess = ucTaccess;
	this->ucNaccslots = ucNaccslots;
	this->ucNpoll = ucNpoll;
	this->ucMaccess = ucMaccess;
	ucParkCount = ucNb;
	ScheduleTimer(LMPEvent::SEND_PARK, SendParkTimeout, tSend);
	return;
}

void
LMP::SendParkReq(uShort usTb, uChar ucNb, uChar ucDeltaB, 
	uChar ucNbsleep, uChar ucDaccess, uChar ucTaccess, 
	uChar ucNaccslots, uChar ucNpoll, uChar ucMaccess) {
	InitParkPara(usTb, ucNbsleep);
	AllocPmArAddr();
	ParkReq pReq;
	pReq.ucFlag = ucFlag;
	pReq.usDb = usDsniff;
	pReq.usTb = usTb;
	pReq.ucNb = ucNb;
	pReq.ucDeltaB = ucDeltaB;
	pReq.ucPMAddr = ucPMAddr;
	pReq.ucARAddr = ucARAddr;
	pReq.ucNbsleep = ucNbsleep;
	pReq.ucDbsleep = ucDbsleep;
	pReq.ucDaccess = ucDaccess;
	pReq.ucTaccess = ucTaccess;
	pReq.ucNaccslots = ucNaccslots;
	pReq.ucNpoll = ucNpoll;
	pReq.ucMaccess = ucMaccess;
	DataRequest(OPCODE_PARK_REQ, bTransactionId, sizeof(pReq), (uChar *)&pReq);
	cout << "Send a Park Request at LMP layer." <<endl;
	return;
	
}

void
LMP::RecvParkReq(LMPPacket *pPacket) {
	cout<<"Receive Park Request at LMP layer."<<endl;
	bParkEnabled = true;
	bool bTranId = !bTransactionId;
	ParkReq pReq;
	memcpy(&pReq, pPacket->Content, sizeof(pReq));
	ucFlag = pReq.ucFlag;
	usDsniff = pReq.usDb;
	usTb = pReq.usTb;
	ucNb = pReq.ucNb;
	ucDeltaB = pReq.ucDeltaB;
	ucPMAddr = pReq.ucPMAddr;
	ucARAddr = pReq.ucARAddr;
	ucNbsleep = pReq.ucNbsleep;
	ucDbsleep = pReq.ucDbsleep;
	ucDaccess = pReq.ucDaccess;
	ucTaccess = pReq.ucTaccess;
	ucNaccslots = pReq.ucNaccslots;
	ucNpoll = pReq.ucNpoll;
	ucMaccess = pReq.ucMaccess;
	ucParkCount = ucNb;	
	//slave can wakeup at multiple Tb interval
	ScheduleTimer(LMPEvent::PARK_TIMEOUT, ParkTimeout, (usDsniff+ucDbsleep*usTb)*SLOTTIME);
	SendAccepted(bTranId, OPCODE_PARK_REQ);
	return;
}

void
LMP::AllocPmArAddr() {
	uShort usIndex = 0;
	for (usIndex=0; usIndex<256; usIndex++) {
		if(cPMAddrList[usIndex]==0) {
			cPMAddrList[usIndex] = usIndex + 1;
			ucPMAddr = usIndex + 1;
		}
	}
	for (usIndex=0; usIndex<256; usIndex++) {
		if(cARAddrList[usIndex]==0) {
			cARAddrList[usIndex] = usIndex + 1;
			ucARAddr = usIndex + 1;
		}
	}
	return;
}
