// Georgia Tech Network Simulator - Interface class
// George F. Riley.  Georgia Tech, Spring 2002

// Define a base class for all "network interfaces".
// Inherited by class "Interface" for IP-Based interfaces
// and InterfaceInterconnect for supercomputer interconnect interfaces

#ifndef __interface_basic_h__
#define __interface_basic_h__

#include "common-defs.h"
#include "handler.h"
#include "node.h"

class EventCQ;
class Queue;
class Link;

//Doc:ClassXRef
class InterfaceBasic : public Handler {
  //Doc:Class Class interface is a base class for all interfaces.
  //Doc:Class The various methods for interfaces are documented
  //Doc:Class in the appropriate subclasses.

public:
  InterfaceBasic();
  InterfaceBasic(Node*);
  virtual ~InterfaceBasic();

  //Doc:Method
  Node*            GetNode() const { return pNode;}// Get the associated node
    //Doc:Desc Get the  node pointer to which this interface is associated.
    //Doc:Return A pointer to the node this interface is associated with.

  //Doc:Method
  virtual void     SetNode(Node* n) { pNode = n;}
    //Doc:Desc Set the  node pointer to which this interface is associated.
    //Doc:Arg1 Pointer to the node this interface is associated with.

  virtual Link*    GetLink() const { return pLink;} // Return the link pointer
  virtual void     SetLink(Link* l) { pLink = l;}   // Set the link pointer

  // We use pure virtual here since not all interfaces necessarily
  // have an associated queue.
  virtual Queue*   GetQueue() const = 0;      // Return the queue pointer
  virtual void     SetQueue(const Queue& q) = 0;  // Set a new queue object

  //Doc:Method
  virtual void     Neighbors(NodeWeightVec_t&, bool = false) = 0;
    //Doc:Desc Get a list of directly connected neighbors.
    //Doc:Arg1 Out parameter containing neighbors and link weights
    //Doc:Arg2 Force all if link is multi-drop broadcast.

  // Sched pkt rx in fifo q
  //Doc:Method
  virtual void     ScheduleRx(LinkEvent*, Time_t, bool = true);
    //Doc:Desc Schedule a packet receipt event at a future  time.
    //Doc:Arg1 The link event to add.
    //Doc:Arg2 Simulation time when the receipt will occur.
    //Doc:Arg3 list.  Only false for trace file playbacks.

  //Doc:Method
  EventCQ*   GetEventCQ() { return evList; }
    //Doc:Desc Get a pointer to the pending RX event circular queue
    //Doc:Desc for this interface.
    //Doc:Return Pointer to EventCQ for this interface.

  // Manage up/down
  bool IsDown() { return down; }
  void Down(bool noReInit = false);
  void Up();
  
protected:
  Node*                   pNode;              // Associated Node
  Link*                   pLink;              // Associated link
  EventCQ*                evList;             // Pending fifo events
  bool                    down;               // True if interface is DOWN
};

#endif
