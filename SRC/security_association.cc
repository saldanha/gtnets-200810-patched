#include "security_association.h"
#include "common-defs.h"

SecurityAssociation::SecurityAssociation()
{
	
}

void SecurityAssociation::AddSecurityAssociation(IPAddr_t dst_addr, IPAddr_t remoteTL)
{
	sa.insert(SecurityAssociationMap_t::value_type(dst_addr, remoteTL));
}

IPAddr_t SecurityAssociation::GetSecurityAssociation(IPAddr_t dst_addr)
{
	if(sa.find(dst_addr) != sa.end())
		return sa[dst_addr];
	else
		return nil;
}
