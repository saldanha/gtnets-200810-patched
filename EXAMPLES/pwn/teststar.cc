// Test the star topology with animation.
// George F. Riley, Georgia Tech, Spring 2003

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "scheduler.h"
#include "node.h"
#include "star.h"
#include "tcp-tahoe.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "validation.h"  

#ifdef HAVE_QT
#include <qnamespace.h>
#endif

#define N_LEAF 5

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  Count_t nl = N_LEAF;
  bool noAnim = false;
  
  if (argc > 1) nl = atol(argv[1]);
  if (argc > 2) noAnim = true;

  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);
  
  //Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  //tr->Open("teststar.txt");
  TCP::LogFlagsText(true);       // Log TCP flags in text mode
  Linkp2p lk(Rate("1Mb"), Time("10ms"));
  Star star(nl, lk, IPAddr("192.168.1.1"));
  // Assign a location to the hub to test the "relative" bounding box
  star.GetHub()->SetLocation(Location(10, 10));
  // Specify the bounding box
  if (!noAnim && !Validation::noAnimation)
    {
      star.BoundingBox(Location(0,0), Location(10,10));
    }
  
  
  // Create a server at the hub
  Node* hub = star.GetHub();
  TCPServer* tcpApp =
    (TCPServer*)hub->AddApplication(TCPServer());
  tcpApp->BindAndListen(hub, 80);
  tcpApp->SetTrace(Trace::ENABLED); // Trace TCP actions at server1

  // Random number generator for start times
  Uniform startRng(0, 0.1);
  // Create the clients
  for (Count_t i = 0; i < star.LeafCount(); ++i)
    {
      Node* l = star.GetLeaf(i);
      // Create a tcp source at the leaf
      TCPSend* ts = (TCPSend*)l->AddApplication(
          TCPSend(hub->GetIPAddr(),80,
                  Constant(1000000)));
#ifdef HAVE_QT
      if (i == 0)
        { // Debug..set packets to blue for this flow
          ts->GetL4()->SetColor(Qt::blue);
        }
#endif
      ts->SetTrace(Trace::ENABLED);
      ts->Start(startRng.Value());
    }

  if (!noAnim && !Validation::noAnimation)
    {
      // Specify animation
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Progress(1);
  s.StopAt(100);
  cout << "Starting Simulation" << endl;
  s.Run();
  Stats::Print(); // Print the statistics
  cout << "Memory usage after run " 
       << s.ReportMemoryUsageMB() << "MB" << endl;
  cout << "Total events processed "
       << Scheduler::Instance()->TotalEventsProcessed() << endl;
  cout << "Total pkt-hops " << Stats::pktsTransmitted << endl;
  cout << "Setup time " << s.SetupTime() << endl;
  cout << "Route time " << s.RouteTime() << endl;
  cout << "Run time "   << s.RunTime() << endl;
  cout << "Total time "   << s.TotalTime() << endl;

}
