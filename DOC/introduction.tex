\chapter{Introduction}\label{Chap:Introduction}
The \gtns\ (\GTNS) is a full--featured network simulation environment
that allows researchers in computer networks to study the behavior
of moderate to large scale networks, under a variety of conditions.
The design philosophy of \GTNS\ is to create a simulation environment that
is structured much like actual networks are structured.  For example,
in \GTNS, there is clear and distinct separation of protocol stack layers.
Packets in \GTNS\ consist of a list of {\em prodocol data units}
({\em PDUs}) that are appended and removed from the packet as it
moves down and up the protocol stack.
Simulation objects representing network nodes have one or more
{\em Interfaces}, each of which can have an associated \IPA\ and
an associated link.  Layer 4 protocol objects in \GTNS\ are bound to ports,
in a fashion nearly identical to the binding to ports in real network
protocols.  
Connections between protocol objects at the transport layer are specified
using a {\tt source IP, source port, destination IP, destination port}
tuple just like actual \TCP\ connections.  The interface between applications
and transport protocols uses the familiar {\em connect}, {\em listen},
{\em send}, and {\em sendto} calls much like the ubiquitous {\em sockets}
API in Unix environments.
Applications in \GTNS\ can have one or more associated
protocol objects, and can simulate the flow of data (including
actual data contents) between applications.

A partial list of the features and capabilities of the \GTNS\ simulator
is given below.

\begin{itemize}
\item Connection Oriented Applications.
Supports a large variety of TCP--based applications,
in a client/server environment, including FTP models, and web--browsing
models.
\item Connectionless Applications.  Models for UPD based connectionless
applcations including On--Off data sources and constant bit rate sources.
\item TCP.  Models Tahoe, Reno, NewReno, and SACK.  Each \TCP\ model
supports detailed logging of sequence vs. time plots for both
sequence and acknowledgement numbers.
\item Routing.  Routes can be either calculated statically, on--demand
using the NIxVector approach, or manually by the simulation user.
\item Node Mobility.  Supports node mobility using both random
waypoint and specific waypoint models.
\item Random Number Generators.  Contains models for a variety of
random number generators, including exponential, pareto, uniform, 
normal, empirical, constant, and sequential.
\item Packet Tracing.  Supports very fine--grained control over the
tracing of packets through the simulation.  Tracing can be enabled
or disabled by node, protocols, or specific protocol endpoints.
Furthermore, individual data items in each protocol header can
be selectively enabled or disabled from being logged.
\item Layer 3 Protocols.  Supports \IP\ version 4.
\item Layer 2 Protocols.  Supports both IEEE 802.3 and IEEE 802.11 protocols.
\item Links.  Supports Point--to--Point, Shared Ethernet, Switched Ethernet,
and Wireless links.
\item Queuing.  Supports the drop--tail, Random Early Detection
({\em RED}), and Infinitessimal Pertubation Analysis ({\em IPA})
queuing methods.
\item Statistics gathering.  Supports data collection using histograms
and cumulative distribution functions.
\item Animation. Support graphical viewing of the simulation topology,
with selective enabling and disabling of display for specified nodes and
links.
\item Stock Topology Objects.  Supports a number of {\em stock objects}
for topology generation, including Star, Tree, Dumbbell, and Grid.
\item Distributed Simulations.  Supports distributing a single simulation
topology on either a network of loosely coupled workstations,
a shared--memory symmetric multiprocessing system, or a combination
of both.
\item Simulation Statistics.  Gathers and reports a large number of
statistics regarding the performance of the simulator itself, including
total number of events, total packets generated, total execution
time, just to name a few.
\end{itemize}
