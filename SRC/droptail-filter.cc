// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: droptail-filter.cc 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - DropTail Queue with Spoof Filterclass
// George F. Riley.  Georgia Tech, Spring 2004

#include <iostream>

#ifdef HAVE_QT
#include <qcolor.h>
#endif

#include "debug.h"
#include "droptail-filter.h"
#include "droppdu.h"
#include "ipv4.h"
#include "ratetimeparse.h"
#include "hex.h"
#include "interface.h"

using namespace std;

// DropTailFilter static variables.  All values from UMd code
int DropTailFilter::Size = 3;
int DropTailFilter::Degree = 2;
int DropTailFilter::Subnets = 3;
int DropTailFilter::SizeOfSubnets = 3;

DropTailFilter::DropTailFilter()
    : DropTail(), l4Proto(nil), validIP(IPADDR_NONE), validMask(0),
      filterIP(IPADDR_NONE), appAlert(nil), needAlert(true),
      rateAlgorithm(false), normalColor(nil), Load(LOADSIZE, 0)

{
  ConstructorHelper();
}

DropTailFilter::DropTailFilter(Count_t l) // Constructor with size
    : DropTail(l), l4Proto(nil), validIP(IPADDR_NONE), validMask(0),
      filterIP(IPADDR_NONE), appAlert(nil), needAlert(true),
      rateAlgorithm(false), normalColor(nil), Load(LOADSIZE, 0)
{
  ConstructorHelper();
}

DropTailFilter::DropTailFilter(const DropTailFilter& c) // Copy constructor
    : DropTail(c), l4Proto(nil), validIP(c.validIP), validMask(c.validMask),
      needAlert(true), rateAlgorithm(c.rateAlgorithm), Load(LOADSIZE, 0)
{
  if (c.l4Proto)
    {
      l4Proto = (UDP*)c.l4Proto->Copy();
    }
  DDOS_on = true; // Not needed remove later
}

DropTailFilter::~DropTailFilter()
{
  if (l4Proto) delete l4Proto;
}

Queue* DropTailFilter::Copy() const
{ 
  return new DropTailFilter(*this);
}


bool DropTailFilter::Enque(Packet* p)
{
  bool spoofDrop = CheckSpoofedSource(p);

  if (spoofDrop)
    {
      // This is tricky however, as we want the trace file
      // to indicate a filtering drop, not a queue full drop
      // So we log the drop here, and return true claiming
      // the packet was enqueued.
      DEBUG0((cout << "DropTailFilter dropping spoofed packet" << endl));
      DropPacket(p, "L2-FD");
      return true;
    }
  
  // No filtering or matches ok
  return DropTail::Enque(p);
}

// From timerhandler
void DropTailFilter::Timeout(TimerEvent* ev)
{ // Indicates we need another alert
  DEBUG0((cout << "DTF Re-enabling alert" << endl));
  needAlert = true;
  delete ev;
#ifdef HAVE_QT  
  // Restore normal color
  Node* n = interface->GetNode();
  n->Color(*normalColor);
#endif
}

  
bool DropTailFilter::CheckSpoofedSource(Packet* p)
{ // Return true of packet should be dropped due to spoofed source address
  // Run the rate filtering algorithm if needed
  if (rateAlgorithm) RateAlgorithm(p);
  IPV4Header* ip = (IPV4Header*)p->FindPDU(3); // Get the layer 3 pdu
  if (filterIP != IPADDR_NONE)
    { // Check if forced filtering
      if ((IPAddr_t)ip->src == filterIP) return true;
    }
  
  if (validIP != IPADDR_NONE)
    { // Filtering is turned on
      if (ip)
        {  // Found the header
          IPAddr_t src = ip->src;
          DEBUG0((cout << "DTF::Enque, ip " << ip 
                  << " valid IP " << (string)IPAddr(validIP)
                  << " mask " << Hex8(validMask) 
                  << " src " << (string)IPAddr(src)
                  << endl));
          
          if ((src & validMask) != (validIP & validMask))
            { // Does not match, packet should be dropped
              // If we have an alertApplication attached, send an alert
              // to the server
              if (appAlert)
                {
                  if (needAlert)
                    { // Not sent alert recently
                      appAlert->SendAlertPacket(
                          ALERT_IPSPF, validIP,
                          appAlert->GetServerIP(), AppAlert::AlertPort);
                      needAlert = false;
                      // Schedule a timer to re-enable in 100ms
                      TimerEvent* ev = new TimerEvent();
                      timer.Schedule(ev, Time("100ms"), this);
#ifdef HAVE_QT		      
                      // Turn this node red
                      Node* n = interface->GetNode();
                      *normalColor = n->Color();
                      n->Color(Qt::red);
#endif
                    }
                }
              return true;
            }
        }
    }
  return false; // Not spoofed or not checking
}

void DropTailFilter::SetRateAlgorithm(bool ra)
{
  rateAlgorithm = ra;
}

void DropTailFilter::Connect(IPAddr_t dstIP, PortId_t dstPort)
{
  if (!appAlert)
    { // Need the alert application
      Node* n = interface->GetNode();
      appAlert = (AppAlert*)n->AddApplication(AppAlert());
      appAlert->SetRegister(false);      // No need to register w/ server
      appAlert->Connect(dstIP, dstPort); // Connect to the server
      appAlert->Start(0);                // Sends hello packet to server
    }
  }

void DropTailFilter::ValidIP(IPAddr_t vIp, Mask_t vMask)
{
  validIP = vIp;
  validMask = vMask;
}


void DropTailFilter::ConstructorHelper()
{
#ifdef HAVE_QT
  normalColor = new QColor();
#endif
    
  IPSpoofingChecked = 0;  // 0: no ip spoofing detected
  UDPfloodingIP     = IPADDR_NONE; 
  UDPchecked        = false; // no udp-flooding detected

  //posG  =  Size;       // Need to find out what this is
  DDOS_on      = true;
  LastUpDate   = 0;
  L_index      = 0;
  nonparamstat = 0.0;  // Added Alvaro May 19 2003 (Bug track)
  LL=0;
  YellowThres=10.0;//Values updated in code.
  RedThres=20.0;
  UnderAttack = false;
#ifdef CHECK_SYN_FLOOD
  // Add later
  // Added from Jia-Shiang's code for tcp-flooding
  Firewall_ = -1;
  for (int i=0; i <= 999; i++)
    {
      Monitor_Attacker[i] = -1;  //-1:unknon,  0:normal, 1:attacker
      Monitor_Timer[i] = new SYNTimer(i, this);
    }
#endif
}

void DropTailFilter::RateAlgorithm(Packet* p)
{
  // Alvaro Egress Filtering-----------------

  int PacketFiltered = 0;
  IPV4Header* iph = (IPV4Header*)p->FindPDU(3); // Get the layer 3 pdu
  if (UnderAttack)
    {
      //jjs Engress Filtering -------------------
      DEBUG0((cout << "DTF::RateAlgo, UnderAttack, DDOS_On " << DDOS_on
              << endl));
      int MaxThput            = -1;
      MaxRateSourceIpAddr = IPADDR_NONE;
      int totalPktsAfterAlarm = 20;     
	
      // WARNING, ARBITRARY VALUE THAT SHOULD DEPEND ON BANDWIDTH OF LINK 
      // AND HISTORIC PACKETS

      if (!DDOS_on)
        {
          TotalArriveAfterAlarm=0;
        }
      else
        {
          TotalArriveAfterAlarm++;
          DEBUG0((cout << "TAAA " << TotalArriveAfterAlarm
                  << " TPAA " << totalPktsAfterAlarm << endl));
          
          ThroughputPerSource[iph->src]++;
          if (TotalArriveAfterAlarm>totalPktsAfterAlarm)
            {
              for (ThroughputMap_t::iterator i = ThroughputPerSource.begin();
                   i != ThroughputPerSource.end(); ++i)
                {
                  if (i->second > MaxThput)
                    { // Found new max
                      MaxThput = i->second;
                      MaxRateSourceIpAddr = i->first;
                    }
                }
              DEBUG0((cout << "MaxRateSource is "
                      << (string)IPAddr(MaxRateSourceIpAddr)
                      << endl));
              if (!UDPchecked)
                {
                  UDPchecked = true;
                  if (appAlert)
                    {
                      cout << "Sending UDP Storm Alert" << endl;
                      appAlert->SendAlertPacket(
                          ALERT_UDPFL, MaxRateSourceIpAddr,
                          appAlert->GetServerIP(), AppAlert::AlertPort);
                    }
                }
              filterIP = MaxRateSourceIpAddr; // Filter this out
            }
#ifdef DONT_DO_THIS
          if ( (IPAddr_t)iph->src == MaxRateSourceIpAddr )
            { // Drop this one.
              DropPacket(p, "L2-AD");
              PacketFiltered=1;
            }
#endif
          //jjs Engress Filtering ------------------------------------------
        }
    }

  //added by alvaro
  
  if (!UnderAttack)
    { //reset the counters
      ThroughputPerSource.clear();
    }
  
  // Added by gpapag for udp flooding 

  if (PacketFiltered == 0)
    {
      if ((IPAddr_t)iph->src == UDPfloodingIP)
        {
          DropPacket(p, "L2-SD"); // Storm discard
          PacketFiltered = 1;
        }
    }
  
  // Added by gpapag for IP spoofing
  // Don't need this here, as IP spoofing is checked elsewhere
#ifdef DONT_NEEd
  if (PacketFiltered == 0)
    {
      int      bbrouters = 0;            // dummy
      // if this is an outgoing queue to the backbone network
      bbrouters = Size;

      sprintf(tmpStr, "lsearch $lRouterlist %d", fromNode_);
      tcl.eval(tmpStr);

      if ((strcmp((char *) tcl.result(), "-1") != 0) && (toNode_ < bbrouters))
        {
          int  ipRange = 0;
	
          sprintf(tmpStr, "set Sizeofsubnets");
          tcl.eval(tmpStr);
          ipRange = (int) atoi((char *) tcl.result());
          
          hdr_cmn *cmnh = hdr_cmn::access(p);  // take the common header
          hdr_ip  *iph  = hdr_ip::access(p);   // take the ip header


          // if the source ip address is out of range, then drop the packet

          if ((iph->prio() != 15) &&
              ((iph->saddr() < fromNode_) ||
               (iph->saddr() >= fromNode_ + ipRange)))
            {
              int  spoofedipid  =  iph->saddr();
              
              drop(p);                  // drop the ip spoofed packet
              PacketFiltered = 1;       // don't count it for the statistic
              
              // inform the server about the detection
              // 1: IP spoofing
              
              if (IPSpoofingChecked == 0)
                {
                  IPSpoofingChecked = 1;
                  sprintf(tmpStr, "$alertApp(%d) send-alert 1 %d", fromNode_, spoofedipid);
                  tcl.eval(tmpStr);
                  
                  sprintf(tmpStr, "$ns at %f  \"$ns trace-annotate \\\"At time=%f: IP Spoofing Attack detected at router %d\\\"\"", Scheduler::instance().clock() + 0.000001, Scheduler::instance().clock(), fromNode_);
                  tcl.eval(tmpStr);
                  
                }
            }
        }
    }    // END of Added by gpapag for IP spoofing
#endif
  

#ifdef DONT_NEED
  // Dont really know what this is...
  //jjs Firewall-------------------------------------------------------------

  if (PacketFiltered == 0)
    {

    double now = Scheduler::instance().clock();

    //fprintf(stderr, "inside monitor-firewall %f\n", now);

    hdr_ip* iph = hdr_ip::access(p);     
    hdr_tcp *tcph = hdr_tcp::access(p);

    if (Firewall_ == 1)   {       
      if (tcph->flags()==10)  {   // "flag=10" is a SYN pkt   see tcp-full.cc
        Monitor_Attacker[iph->saddr()]++;              
			
        // Added by gpapag for tcp flooding

        Monitor_Timer[iph->saddr()]->resched(TCP_FLOODING_ATTACK);
        if ( Monitor_Attacker[iph->saddr()] >= 1)   {
          Monitor_Attacker[iph->saddr()] = 1;
        }
 
      }   

      if (tcph->flags() == 24)  {   // "flag=24" is a [PSH,ACK] pkt   
        Monitor_Attacker[iph->saddr()] = -1;
      }  

      if (Monitor_Attacker[iph->saddr()] >= 1)  {
        //printf("-->t=%fDrop Attack src.port:%d.%d dst.port:%d.%d fid=%d\n",now,iph->saddr(), iph->sport(),iph->daddr(), iph->dport(),iph->flowid());
        //q_->remove(p);    
        drop(p);  //drop attack packets
        PacketFiltered = 1;  // don't count it for the statistic
      }
    }
  }
  //jjs Firewall-------------------------------------------------------------
#endif

  //jjs DDOS-------------------------------------------------------
  if (PacketFiltered==0)
    {
      LL = LL+1;   //Count packets not bytes
      r  = 10;     //size of window to compute statistic, r<BOUNDR !!!
      c  = 1;	     //right now we don't deal with more (correlated) links
      gamma = 0;   //again, right now we only have one direction!!
	
      //meany[0]=20.0; //initial arbitrary value
      //covy[0]=10.0;  //another initial arbitrary value for debugging

      double now = Simulator::Now();
      double timerefresh = 0.1; 


      if (now > LastUpDate + timerefresh)
        {
          LastUpDate    = now;
          Load[L_index] = LL;
          LL            = 0;
          int counter   = 0;

          for (int j = r-1; j >= 0; j--)
            {
              //HERE IS WHERE WE BUILD Y...
              // HOW TO GET THE INFORMATION FROM THE OTHER LINKS??
              //WE NEED AN INPUT FROM TCL CODE AT RUNNING TIME TO 
              // BE ABLE TO EXTEND THIS 
              //ALGORITHM
              if ((L_index-counter) >= 0)
                {
                  y[j][gamma] = (double) Load[(L_index-counter)];
                }
              else
                {
                  y[j][gamma] = (double) Load[(L_index-counter)+100];
                }
              counter++;
            }
          counter = 0;	
          for (int j = BOUNDR-1; j >= 0; j--)
            {
              if ((L_index-counter)>=0)
                {
                  historicy[j][gamma] = (double) Load[(L_index-counter)];
                }
              else
                {
                  historicy[j][gamma] = (double) Load[(L_index-counter)+100];
                }
              if (historicy[j][gamma]>1000)
                { //suspicously high number of packets... bug found 
                  cout << "Error ! Load[" << L_index-counter
                       << "]=" << Load[L_index-counter] << endl;
                }
              counter++;
            }
          meany[gamma] = 0.0;
          for (int j = BOUNDR-1-r; j >= 0; j--)
            {
              meany[gamma] = meany[gamma]+historicy[j][gamma];
            }
          meany[gamma]=meany[gamma]/(double)(BOUNDR-r);

          //This is used to prevent the transient at start-up...

          covy[gamma] = 0.0;
          for (int j = BOUNDR-1-r; j>= 0; j--)
            {
              covy[gamma] = covy[gamma]+(historicy[j][gamma] - meany[gamma])*
                  (historicy[j][gamma]-meany[gamma]);
            }
          covy[gamma] = covy[gamma]/(double)(BOUNDR-1-r);	
          double statistic = 0.0;
          statistic        = ComputeStatistic();
            
          //END ALVARO DDOS---------------------------------------------

          //L_index =    (L_index+1 == 100) ? 0 : (L_index+1);
          L_index = (++L_index) % LOADSIZE;
          

        // IF WE HAVE TRAINING TIME FOR SETTING THE THRESHOLDS WE COULD USE THE 
        // FOLLOWING CODE:		
        //if (now > (double) BOUNDR*timerefresh && now < (double)(FALSEALARMDELAY+BOUNDR)*timerefresh )
        if (now < (double)(FALSEALARMDELAY+BOUNDR)*timerefresh )
          {
            double temporalthres=9*9*9*9*9;
            if (now < (double) BOUNDR*timerefresh)
              {//if we are still estimating the historic mean
                //then high values because we don't want false alarms
                RedThres=temporalthres;
                YellowThres=temporalthres;
              }
            else if (RedThres==temporalthres)
              {
                RedThres=1.2*statistic;
              }
            else if(1.2*statistic>RedThres)
              {
                RedThres=1.2*statistic;
                YellowThres=.9*RedThres;
              }
          }
        // Added by gpapag, for Link coloring May 7, 2003

        int    curColor            = 0;   // dummy variable
        int    finalColor          = 0;   // the final color of the link

                
        if (statistic < YellowThres)
          {
            curColor = 2;   // green
          }
        else if (statistic < RedThres)
          {
            curColor = 3;   // yellow
          }
        else
          {            // statistic > RedThres
            curColor = 4;   // red
          }
        
        color_ = curColor;
        finalColor = curColor; // Debug for now
        cout << "Rate algo, node "
             << (string)IPAddr(interface->GetNode()->GetIPAddr())
             << " pkt source "
             << (string)iph->src 
             << " Color " << finalColor
             << endl;
  
        if (finalColor == 2)
          {
            if (UnderAttack)
              {
                //sprintf(tmpStr, "$n(%d) delete-mark qFilterMark", fromNode_);
                //tcl.eval(tmpStr);
              }
            UnderAttack = false;
            filterIP = IPADDR_NONE;
            //strcpy((char *) yatmpStr, "green");
          }
        else if (finalColor == 3)
          {
            if (UnderAttack)
              {
                //sprintf(tmpStr, "$n(%d) delete-mark qFilterMark", fromNode_);
                //tcl.eval(tmpStr);
              }
            UnderAttack = false;
            filterIP = IPADDR_NONE;
#ifdef HAVE_QT	    
            Node* n = interface->GetNode();
            // This "normalColor" stuff needs work..is not right
            *normalColor = n->Color();
            n->Color(Qt::yellow);
#endif
            //strcpy((char *) yatmpStr, "yellow");
          }
        else if (finalColor == 4)
          {
            if (statistic > 1.5*RedThres)
              {   // upper bound to return to normal
                nonparamstat=1.5*RedThres;
              }
	    Node* n = interface->GetNode();
#ifdef HAVE_QT
            *normalColor = n->Color();
            n->Color(Qt::red);
#endif
            UnderAttack = true;
            cout << "UNDER ATTACK, node " << (string)IPAddr(n->GetIPAddr())
                 << endl;
            //strcpy((char *) yatmpStr, "red");

            //int  tmpRouter = 0;   // dummy variable

            //tcl.eval("set Size");
            //tmpRouter = (int) atoi((char *) tcl.result());
#ifdef DONT_NEED
          if (fromNode_ >= tmpRouter)  {  // not a backbone router
            sprintf(tmpStr, "lsearch $lRouterlist %d", fromNode_);
            tcl.eval(tmpStr); 
            if (strcmp((char *) tcl.result(), "-1") == 0)  {
              UnderAttack = false;  
            }
          }
#endif
          if (UnderAttack == true)
            {
              //sprintf(tmpStr, "$n(%d) add-mark qFilterMark red circle",
              //        fromNode_);
              //tcl.eval(tmpStr);
			 
            }
          }

#ifdef DONT_NEED_THiS
        // Filtering will be performed only at routers.
        // Routers are the nodes with number < Size and all the 
        // nodes in the lRouterlist.

        int  tmpRouter = 0;   // dummy variable

        tcl.eval("set Size");
        tmpRouter = (int) atoi((char *) tcl.result());

        if (fromNode_ >= tmpRouter)  {  // not a backbone router
          sprintf(tmpStr, "lsearch $lRouterlist %d", fromNode_);
          tcl.eval(tmpStr); 
          if (strcmp((char *) tcl.result(), "-1") == 0)  {
            UnderAttack = 0;  
          }
        }

        // Check if the scenario wants coloring.
        // Coloring will be done only if the variable ColorSwitch is
        // set in the tcl file:
        //      set  ColorSwitch  1
        // For any other values of ColorSwitch, no coloring will be
        // provided.
 
        tcl.eval("set ColorSwitch");
        if (strcmp((char *) tcl.result(), "1") == 0)  {
          sprintf(tmpStr, "$ns simplex-link-op $n(%d) $n(%d) color %s", fromNode_, toNode_, yatmpStr);
          tcl.eval(tmpStr);
        }   // if ColorSwitch == 1
#endif
        }
      // printf("%f %d\n",now, q_->byteLength() );
    }
  
  
  //jjs DDOS-------------------------------------------------------------
  
}

void DropTailFilter::DropPacket(Packet* p, const char* st)
{
  DropPDU d(st, p); // Note filter discard
  interface->GetNode()->TracePDU(nil, &d);
  Stats::pktsDropped++; // Count dropped packets
  if (p->notification)
    { // Sender has requested notification, so we need to 
      // schedule one.
      // hack for now just to 1ms
      Simulator::instance->AddNotify(p->notification, Time("1ms"), nil);
    }
  delete p;     // Delete the packet
}
//ALVARO DDOS-----------------------------------------------------------
//double DropTail::ComputeStatistic(double &y, int gamma,double &meany, double &covy, int r, int c)
// Computes the statistic as in Basseville & Nikiforov, page 222 
// Equations 7.2.33 and 7.2.34 with uncorrelated components in y and with 
// unit basis vector gamma.
// Inputs:
// y is the observations. It is a matrix where c are the number of links 
// and r the number of observations gamma is the unit vector of size c 
// represented here as an integer, e.g. 0 means direction [1 0 0]'
// and 1 means direction [0 1 0] etc...
// meany is the mean of y for each link, thus it is a vector of size c
// covy is also a vector as we are considering uncorrelated links

double DropTailFilter::ComputeStatistic()
{
  double nonparamtemp=nonparamstat+y[r-1][gamma]-1.1*meany[gamma];
  if (nonparamtemp>0)
    {
      nonparamstat=nonparamtemp;
    }
  else
    {
      nonparamstat=0.0;
    }
  return nonparamstat;
}

//END ALVARO DDOS ----------------------------------------------------------
