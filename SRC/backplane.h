// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: backplane.h 92 2004-09-14 18:18:43Z dheeraj $



// Specification of the Distributed Simulated Backplane
// George F. Riley, Georgia Tech, Summer 2000

#ifndef __BACKPLANE_H__
#define __BACKPLANE_H__
#include <sys/types.h>
// Misc types
typedef unsigned int   isize_t; // Item size type
typedef unsigned int   itemid_t;// Item Identifier type

#ifndef linux
#ifndef u_long
typedef unsigned long  u_long;
#endif
#ifndef u_short
typedef unsigned short u_short;
#endif
#ifndef u_char
typedef unsigned char  u_char;
#endif
#endif

typedef unsigned long  nodeid_t;// Node id type

// Definitions for FLAGS values
#define PROTO_OPTIONAL 0
#define PROTO_REQUIRED 1

#define ITEM_OPTIONAL  0
#define ITEM_REQUIRED  1
#define ITEM_BYTE_SWAP 2

// Query application if a protocol is contained in an internal format message
// DSim can skip the entire data item list if not contained
typedef int (*ExportQueryCb_t)(char*, int, void*);

// Define the prototype for the Data Item export callbacks
// Parameters are :
//  char* Source buffer
//  char* Return buffer
//  int   Return buffer maximum length (bytes!)
//  void* User context
// Return value is number of bytes actually copied to return buffer
typedef int(*ItemExportCb_t)(char*, char*, int, void*);

// Define the prototype for the Data Item import callback
// Parameters are:
// char* Source buffer
// int   Length of source buffer
// char* Return buffer
// void* User context
typedef int(*ItemImportCb_t)(char*, int, char*, void*);


  // Defien the prototype for the Data Item "Default" callback
// Parameters are:
// char* Return buffer
// void* User context
typedef int(*ItemDefaultCb_t)(char*, void*);

// Define the handle to DSim data structures (blind pointers)
typedef void* DSHandle_t; // Pointer to global info structure
typedef void* PRHandle_t; // Pointer to a protocol info structure
typedef void* ITHandle_t; // Pointer to a data item info structure

// Initialize the DSim backplane
DSHandle_t InitializeDSim(nodeid_t myid);

// Register a new protocol
PRHandle_t RegisterProtocol(char*, int, ExportQueryCb_t, void*);

// Regster a new data item
ITHandle_t RegisterItem(PRHandle_t, char*, int, isize_t,
                        ItemExportCb_t, void*,
                        ItemImportCb_t, void*,
                        ItemDefaultCb_t,void*);

// Informs the backplane that all registration has completed, and the
// consenus protocol should be performed.
void RegistrationComplete(nodeid_t myid, unsigned int npeers);

// Find out how big the generic message buffers need to be (bytes)
isize_t InquireMessageSize();

// Export a message to generic format
int ExportMessageWrapper( char*, char*, int, char*, int*);
  // char*       - Pointer to input buffer
  // char*       - Pointer to baggage buffer
  // int         - Length of baggage buffer
  // char*       - Pointer to output buffer
  // int*        - Length of output buffer (in/out)

// Import a message from generic forma
int ImportMessageWrapper( char*, int, char*, char*, int*);
  // char*       - Pointer to input buffer
  // int         - Length of input buffer
  // char*       - Pointer to output buffer
  // char*       - Pointer to baggage buffer
  // int*        - Length of baggage buffer

// Get the current handle
DSHandle_t GetDSimHandle(void);

// Convert size in bits to size in bytes
int SizeInBytes(int);

char* RoundUpBuffer(char*, char*, int, int); // Insure proper byte alignment

int GetPeerSocket (int ); // Get the socket handle for the specified peer

// Put a data item on a tcp socket, 32bit item length followed by data
int PutCountedItem(const int s, const char* b, const int l);

// Get a data item from a tcp socket.  32 bit length followe by the data
int GetCountedItem(int s, char* b, int l);

// The versions using CQueue structures as Import input and Export output
// These versions avoid an extra data copy at each end
#include "cq.h"
int ExportMessageQ( char*, char*, int, CQueue*, char* pBase, size_t, size_t);
  // Same as above, except CQueue instead of Outputbuffer, length
  // Last param is pIn to use (since CQueue may not be updated yet)
int ImportMessageQ( CQueue*, char* pBase, size_t, char*, char*, int*, size_t);
  // Same as above, except CQueue instead of Inputbuffer, length

#endif



