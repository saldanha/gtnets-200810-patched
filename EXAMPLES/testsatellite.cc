// Test the Satellite node
// George F. Riley, Georgia Tech, Fall 2002

#include <iostream>
#include <string>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "node-satellite.h"
#include "application-cbr.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"

#include "ratetimeparse.h"
#include "linkp2p.h"
#include "udp.h"
#include "validation.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

#define N_LEAF 5

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);
  
  bool useTCP = false;
  Count_t  sendCount = 1000000;
  
  if (argc > 1) if (string(argv[1]) == string("TCP")) useTCP = true;
  if (argc > 2) sendCount = atol(argv[2]);
      
  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
    }

  Node* n1= new Node();
  Node* n2= new Node();
  //Node* n3 = new Node();
  
  SatelliteNode* sat = new SatelliteNode();
  n1->CustomShapeFile("SatGroundRight.png");
  n2->CustomShapeFile("SatGroundLeft.png");
  //n3->CustomShapeFile("Ship.png");
  
  cout << "n1 is " << n1 << " id " << n1->Id() << endl;
  cout << "n2 is " << n2 << " id " << n2->Id() << endl;
  cout << "Sat node is " << sat << " id " << sat->Id() << endl;
  
  n1->SetLocation(1.0, 1.0);
  sat->SetLocation(2.0, 2.0);
  n2->SetLocation(3.0, 1.0);
  //n3->SetLocation(1.0, 2.0);
  
  // Make the displayed region slightly larger
  s.NewLocation(0.5, 0.5);
  s.NewLocation(3.5, 2.5);

  Linkp2p lk(Rate("100Kb"), Time("120ms"));
  lk.BitErrorRate(1e-5); // Higher than normal BER
  
  n1->AddDuplexLink(sat, lk, IPAddr("192.168.0.1"));
  n2->AddDuplexLink(sat, lk, IPAddr("192.168.1.1"));

  if (useTCP)
    { // Add the server
      TCPServer* tcp = (TCPServer*)n2->AddApplication(
          TCPServer());
      tcp->BindAndListen(80);
      // Add the sender
      TCPSend* send = (TCPSend*)n1->AddApplication(
          TCPSend(n2->GetIPAddr(), 80, Constant(sendCount)));
      send->Start(0);
    }
  else
    {
      // Add the CBR application
      CBRApplication* cbrApp =
          (CBRApplication*)n1->AddApplication(
              CBRApplication(n2->GetIPAddr(), 12345, NO_PORT, Rate("64Kb")));
      cbrApp->Start(0);
    }
  

  // Specify animation
  if (!Validation::noAnimation)
    {
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Progress(1);
  s.StopAt(100);
  s.Run();
}
