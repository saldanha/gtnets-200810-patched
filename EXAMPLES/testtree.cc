// Test the tree topology with animation.
// George F. Riley, Georgia Tech, Fall 2003

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "tree.h"
#include "tcp-tahoe.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "args.h"
#include "validation.h"  

#ifdef HAVE_QT
#include <qnamespace.h>
#include <qcolor.h>
#endif

#define N_LEVELS 5
#define N_FANOUT 2

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  Count_t nl = N_LEVELS;
  Count_t fo = N_FANOUT;
  Arg("levels", nl, N_LEVELS);
  Arg("fanout", fo, N_FANOUT);
  Arg::ProcessArgs(argc, argv);
  
  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);
  
  //Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  //tr->Open("teststar.txt");
  TCP::LogFlagsText(true);       // Log TCP flags in text mode
  Linkp2p lk(Rate("1Mb"), Time("10ms"));

  // Create the tree
  Tree tree(nl, fo, lk, IPAddr("192.168.1.1"));
  tree.BoundingBox(Location(0,0), Location(10,10));
  
  // Create a servers at each leaf
  for (Count_t l = 0; l < tree.LeafCount(); ++l)
    {
      Node* n = tree.GetLeaf(l);
      TCPServer* tcpApp =
          (TCPServer*)n->AddApplication(TCPServer());
      tcpApp->BindAndListen(n, 80);
      tcpApp->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
    }
  
  // Random number generator for start times
  Uniform startRng(0, 0.1);
  Uniform peerRNG(0, tree.LeafCount()); // For choosing peers
  
  // Create the clients
  for (Count_t i = 0; i < tree.LeafCount(); ++i)
    {
      Node* l = tree.GetLeaf(i);
      // Choose a random leaf peer
      Node* p = tree.GetLeaf(peerRNG.IntValue());
      
      // Create a tcp source at the leaf
      TCPSend* ts = (TCPSend*)l->AddApplication(
          TCPSend(p->GetIPAddr(),80,
                  Constant(1000000)));
#ifdef HAVE_QT
      // Assign different colors to each flow.
      // The Qt predefined colors (at least the interesting ones)
      // are standard color 7 through 18 (12 different ones);
      Count_t colorInd = i % 12;
      switch(colorInd)
        {
          case 0:
            ts->GetL4()->SetColor(Qt::red);
            break;
          case 1:
            ts->GetL4()->SetColor(Qt::green);
            break;
          case 2:
            ts->GetL4()->SetColor(Qt::blue);
            break;
          case 3:
            ts->GetL4()->SetColor(Qt::cyan);
            break;
          case 4:
            ts->GetL4()->SetColor(Qt::magenta);
            break;
          case 5:
            ts->GetL4()->SetColor(Qt::yellow);
            break;
          case 6:
            ts->GetL4()->SetColor(Qt::darkRed);
            break;
          case 7:
            ts->GetL4()->SetColor(Qt::darkGreen);
            break;
          case 8:
            ts->GetL4()->SetColor(Qt::darkBlue);
            break;
          case 9:
            ts->GetL4()->SetColor(Qt::darkCyan);
            break;
          case 10:
            ts->GetL4()->SetColor(Qt::darkMagenta);
            break;
          case 11:
            ts->GetL4()->SetColor(Qt::darkYellow);
            break;
        }
#endif
      ts->SetTrace(Trace::ENABLED);
      ts->Start(startRng.Value());
    }

  // Specify animation
  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }

  s.Progress(1);
  s.StopAt(100);
  s.Run();
}
