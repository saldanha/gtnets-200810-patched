// Test program for Wireless Grid object
// George F. Riley.  Georgia Tech, Fall 2002

#include <iostream>
#include <stdlib.h>

#include "simulator.h"
#include "wireless-grid-rectangular.h"
#include "wireless-grid-polar.h"
#include "ipaddr.h"
#include "rng.h"
#include "node.h"
#include "mobility-random-waypoint.h"
#include "ratetimeparse.h"
#include "validation.h"  

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  Location l(500,500);
  WirelessGridPolar g(l,
                      Constant(500),
                      Constant(250),
                      IPADDR_NONE,
                      Sequential(0, 350.0, Uniform(0,5)));
  g.AddMobility(RandomWaypoint(g, Uniform(1,10), Uniform(10,20)));
  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      s.PauseAnimation(100);
      s.AnimationUpdateInterval(Time("1ms")); // 1ms initial update rate
    }
  s.StopAt(1000);
  s.Run();
}
