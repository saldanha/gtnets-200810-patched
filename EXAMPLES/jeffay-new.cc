// Recreate the Kevin Jeffay web browsing experiment
// George F. Riley.  Georgia Tech, Fall 2002

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <string.h>

#include "simulator.h"
#include "scheduler.h"
#include "dumbbell.h"
#include "linkp2p.h"
#include "ratetimeparse.h"
#include "tcp-tahoe.h"
#include "rtt-estimator.h"
#include "application-tcpserver.h"
#include "application-webbrowser.h"
#include "histogram.h"
#include "globalstats.h"
#include "droptail.h"
#include "red.h"
#include "star.h"
#include "ipv4.h"
#include "average-min-max.h"
#include "validation.h"

using namespace std;

#define SEG_SIZE 1500
#define HDR_SIZE   40

#define N_SERVERS     7
#define N_CLIENT_NETS 7

// Round trip delays per the Tuning Web paper (see Appendix A)
Time_t delayMs[] = {
  81, 105,  64,  64,  67, 147, 114,
 126, 137,  47,  53,  41,  86, 114,
  33,  42,  40, 114, 112, 117, 108,
  35,  45,  95, 100,  31, 100, 116,
 105,  92,  78,  41,  53, 109,  66,
  85, 112,  38,  83,  55,   8,  41,
 124,  87, 101,  87,  95,   7,  61
};

Queue*  bnQueue = nil;      // Bottleneck queue
AverageMinMax rttStats;
Histogram h(2.0, 20);       // 0-2.0 seconds, 20 bins
Histogram toh(20, 20);      // 20 bins, 1 each

// Simple timer class for 1pps debugging info
class MyTimer : public Timer {
public:
  virtual void Timeout(TimerEvent*);  // Called when timer expires
};

void MyTimer::Timeout(TimerEvent* ev)
{
  Time_t now = Simulator::Now();
  cout << "Progress " << now
       << " aBr " << setw(3) << WebBrowser::activeBrowsers
       << " aCn " << setw(3) << WebBrowser::activeConnections
       << " sTp " << setw(3) << WebBrowser::startedThisPeriod
       << " cTp " << setw(3) << WebBrowser::completedThisPeriod
       << " cCn " << WebBrowser::completedConnections
       << " qD "  << bnQueue->DropCount()
       << " qE "  << bnQueue->EnqueueCount()
       << " qL "  << bnQueue->Length()

       << endl;
  WebBrowser::startedThisPeriod = 0;
  WebBrowser::completedThisPeriod = 0;
  int inow = (int)now;
#ifdef LOG_RTT_INFO
  if ((inow % 100) == 0)
    {
      cout << "RttStats ";
      rttStats.Log(cout); // Print the rtt stats
    }
#endif
  if (inow == 1200)
    { // Time to reset all stats
      bnQueue->ResetStats();
      h.Reset();
	  toh.Reset();
      WebBrowser::completedConnections = 0;
      WebBrowser::totRespTime = 0;
      WebBrowser::totRespSize = 0;
      WebBrowser::totObjectsPerPage = 0;
      WebBrowser::totBrowserSessions = 0;
    }
  Schedule(ev, 1.0);
}

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Count_t nBrowsers = 3000;      // Default numberof browsers
  Count_t qLimit = 30;           // Default Queue Limit
  Count_t qMax = 90;             // Max_q if RED
  bool    red = false;           // True if red queue
  bool    unconstrained = false; // True if 100Mbps bottleneck
  Mult_t  constrainMult = 0.1;   // 10% constraint on bottleneck (default)

  // debug
  Time_t totDel = 0;
  for (int i = 0; i < 49; ++i) totDel += delayMs[i];
  cout << "Avg Delay " << totDel / 49.0 << endl;

  if (strstr(argv[0], "uncon") != NULL)
    {
      unconstrained = true;
      constrainMult = 1.0;
      cout << "Using unconstrained network" << endl;
    }
  if (argc > 1) nBrowsers = atol(argv[1]);
  if (argc > 2) 
    { // See if RED specified
      int l = strlen(argv[2]);
      for (int i = 0; i < l; ++i) argv[2][i] = tolower(argv[2][i]);
      string arg(argv[2]);
      red = (arg.find("red") != string::npos);
    }
  if (argc > 3) qLimit = atol(argv[3]);
  if (argc > 4) qMax = atol(argv[4]);

  Simulator s;
  if (red)
    {
      Queue::Default(REDQueue());
    }
  else
    {
      Queue::Default(DropTail());
      Queue::DefaultLength(200000); // Set large limit for all queues
      // execept bottleneck (set below)
      //Queue::DefaultLength(qLimit * (SEG_SIZE + HDR_SIZE));
      //Queue::DefaultLimitPkts(qLimit); // ! debug
    }

  Trace* gs = Trace::Instance();
  gs->IPDotted(true);
  //gs->Open("jeffay.txt");
  TCP::LogFlagsText(true);    // Log flags in text mode
  // Enable all l3 tracing
  //IPV4::Instance()->SetTrace(Trace::ENABLED);

  // Set some TCP defaults per the Tuning Web paper
  TCP::DefaultSegSize(1500);  // Ethernet segment size
  TCP::DefaultAdvWin(0x4000); // 16kb Window
  TCP::DefaultSSThresh(1000000); // 1Mb slow start threshold
  TCP::Default().SetTrace(Trace::ENABLED); // Enable trace on default tcp
  // Set statistics collection for RTT, for debugging
  RTTEstimator::SetStatistics(&rttStats);

  Linkp2p l;
  l.Bandwidth(Rate("100Mb"));
  l.Delay(Time("1ms"));
  Linkp2p l2;
  l2.Bandwidth(Rate("10Mb"));
  l2.Delay(Time("1ms"));

  // See Jeffay Fig 2 for topology
  Dumbbell b(1, 1, constrainMult, l); // Enet sw, campus rtr, isp rtr, Enet sw
  bnQueue = b.RightQueue(); // Bottleneck queue
  bnQueue->SetLimit(qLimit * (SEG_SIZE + HDR_SIZE));
  // Create the seven servers
  Star serverStar(N_SERVERS, b.Right(0), l2, IPAddr("192.168.2.0"));
  // Create the servers
  IPAddrVec_t servers; // IP Addresses of possible servers
  for (Count_t i = 0; i < N_SERVERS; ++i)
    {
      Node* n = serverStar.GetLeaf(i);
      IPAddr serverIP = n->GetIPAddr();
      cout << "Right node " << i 
           << " ipaddr " << (string)serverIP << endl;
      servers.push_back(serverIP);
      // Add a tcp server on right side
      TCPServer* app = (TCPServer*)n->AddApplication(TCPServer());
      app->Bind(HTTP_PORT);
      app->CloseOnEmpty();
      app->DeleteOnComplete();
    }
  // Create the web browser nodes
  Star clientStar(N_CLIENT_NETS, b.Left(0), l2);

  vector <Star*> stars;
  IPAddr_t starIP = IPAddr("192.169.1.1");
  for (Count_t i = 0; i < N_CLIENT_NETS; ++i)
    {
      Node* n = clientStar.GetLeaf(i);
      Star* s = new Star(N_SERVERS, n, l2, starIP);
      stars.push_back(s); // Save the star networks for browser instantiation
      for (Count_t j = 0; j < N_SERVERS; ++j)
        { // Set link delay to get round trip variations, per Jeffay Paper
          // Subtract 5 since we have 5 hops with 1ms delay + this one
          Time_t d = delayMs[i * N_SERVERS + j] - 5;
          s->HubLink(j)->Delay(d/1000.0);
        }
      starIP += 0x0100; // Advance to next set of IP addresses
    }

  // Create the browsers
  Count_t si = 0; // Star index
  Count_t li = 0; // Leaf index
  Uniform startRng(0,600);                // RNG for start times
  vector <WebBrowser*> browsers;
  for (Count_t i = 0; i < nBrowsers; ++i)
    {
      WebBrowser* wb = (WebBrowser*)stars[si]->GetLeaf(li)->AddApplication(
          WebBrowser(servers, Uniform(0, servers.size())));
      wb->ThinkTimeBound(600.0);
      wb->SetStatistics(&h);
      //wb->SetDebugStats(&toh);
      //wb->IdleTime(5400.0);
      wb->IdleTime(100.0);
      wb->Start(startRng.Value());
      browsers.push_back(wb); // Save for later
      // Advance to next browser
      if (++li == N_SERVERS)
        {
          li = 0;
          if (++si == N_SERVERS) si = 0;
        }
    }

  //s.Progress(10);
  MyTimer t;
  t.Schedule(new TimerEvent, 1.0); // Simple debug info
  s.StopAt(6000);
  //s.StopAt(1800);
  s.StopAt(200);
  s.Run();
  char work[100];
  char work1[100];
  sprintf(work, "%d", qLimit);
  sprintf(work1, "%d", nBrowsers);
  string fn = string("testweb-") + string(work1) + string("-");
  if (red)
    fn = fn + string("RED-") + string(work) + string(".cdf");
  else
    fn = fn + string("DropTail-") + string(work) + string(".cdf");

  ofstream cdf(fn.c_str());
  h.CDF(cdf, "# CDF of Web Object Response Time", ' ');
  cdf.close();
  // Debug..dump hung connections
  for (Count_t i = 0; i < browsers.size(); ++i)
    {
      browsers[i]->DBDump();
    }
  // Cleanup for memory leak report
  for(vector<Star*>::size_type i = 0; i < stars.size(); ++i)
    {
      delete stars[i];
    }
  for(vector<WebBrowser*>::size_type i = 0; i < browsers.size(); ++i)
    {
      delete browsers[i];
    }

  rttStats.Log(cout); // Print the rtt stats
#define VERBOSE
#ifdef VERBOSE
  Stats::Print(); // Print the statistics
  rttStats.Log(cout); // Print the rtt stats
  cout << "Memory usage after run " 
       << s.ReportMemoryUsageMB() << "MB" << endl;
  cout << "Total events processed "
       << Scheduler::Instance()->TotalEventsProcessed() << endl;
  cout << "Setup time " << s.SetupTime() << endl;
  cout << "Route time " << s.RouteTime() << endl;
  cout << "Run time "   << s.RunTime() << endl;
  cout << "Average Response Time " << WebBrowser::totRespTime / 
    WebBrowser::completedConnections << endl;
  cout << "Average Response Size " << WebBrowser::totRespSize / 
    WebBrowser::completedConnections << endl;
  cout << "Average Objects/Page " << (double)WebBrowser::totObjectsPerPage / 
    (double)WebBrowser::totBrowserSessions << endl;
  cout << "Tot objects " << WebBrowser::totObjectsPerPage << endl;
  cout << "Tot sessions " << WebBrowser::totBrowserSessions << endl;
  cout << "Max Response Time " << WebBrowser::largestRespTime << endl;
  cout << "Max Response Size " << WebBrowser::largestRespSize << endl;
#endif
}
