/****************************************************************************/
/*  File:            bgp_regex.cc                                           */
/*  Author:          Sunitha Beeram                                         */
/*  Email:           sunithab@ece.gatech.edu                                */
/*  Documentation:   TBD                                                    */
/*                                                                          */
/*  Version:         1.0beta                                                */
/*                                                                          */
/*  Copyright (c) 2005 Georgia Institute of Technology                      */
/*  This program is free software; you can redistribute it and/or           */
/*  modify it under the terms of the GNU General Public License             */
/*  as published by the Free Software Foundation; either version 2          */
/*  of the License, or (at your option) any later version.                  */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful,         */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*  GNU General Public License for more details.                            */
/*                                                                          */
/****************************************************************************/

#include "bgp.h"

/* Character `_' has special mean.  It represents [,{}() ] and the
   beginning of the line(^) and the end of the line ($).  

   (^|[,{}() ]|$) */

regex_t *
BGP::bgp_regcomp (char *regstr)
{
  /* Convert _ character to generic regular expression. */
  int i, j;
  int len;
  int magic = 0;
  char *magic_str;
  char magic_regexp[] = "(^|[,{}() ]|$)";
  int ret;
  regex_t *regex;

  len = strlen (regstr);
  for (i = 0; i < len; i++)
    if (regstr[i] == '_')
      magic++;

  magic_str = (char*)XMALLOC (MTYPE_TMP, len + (14 * magic) + 1);
  
  for (i = 0, j = 0; i < len; i++)
    {
      if (regstr[i] == '_')
	{
	  memcpy (magic_str + j, magic_regexp, strlen (magic_regexp));
	  j += strlen (magic_regexp);
	}
      else
	magic_str[j++] = regstr[i];
    }
  magic_str[j] = '\0';

  regex = (regex_t*)XMALLOC (MTYPE_BGP_REGEXP, sizeof (regex_t));

  ret = regcomp (regex, magic_str, REG_EXTENDED);

  XFREE (MTYPE_TMP, magic_str);

  if (ret != 0)
    {
      XFREE (MTYPE_BGP_REGEXP, regex);
      return NULL;
    }

  return regex;
}

int
BGP::bgp_regexec (regex_t *regex, struct aspath *aspath)
{
  return regexec (regex, aspath->str, 0, NULL, 0);
}

void
BGP::bgp_regex_free (regex_t *regex)
{
  regfree (regex);
  XFREE (MTYPE_BGP_REGEXP, regex);
}
