#ifndef __encryption_h__
#define __encryption_h__

#include "common-defs.h"

typedef enum Encrypt { NOENC, PT, CT };

class Encryption {
	
public:
	Encryption ();
	
	void SetEncryption( Encrypt enc);
	Encrypt GetEncryption();
	
private:
	Encrypt level;
};
#endif

