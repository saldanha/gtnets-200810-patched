// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: node-impl.h 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - Node Implementation class
// George F. Riley.  Georgia Tech, Fall 2002

// Defines an abstract base class for node implementations

#ifndef __nodeimpl_h__
#define __nodeimpl_h__

#include "common-defs.h"
#include "macaddr.h"
#include "node.h"

class Link;
class Node;
class Interface;
class Routing;
class Protocol;
class ProtocolGraph;
class PortDemux;
class Linkp2p;
class Queue;
class Application;
class RoutingAODV;
class AODVRoutingEntry;
class Image;
class WirelessLink;

//Doc:ClassXRef
class NodeImpl {
public:
  NodeImpl(Node*);
  virtual ~NodeImpl();

  virtual NodeImpl* Copy() const = 0;    // All nodeimpl's must be copy'able
  NodeId_t Id() { return pNode->Id(); }

  // IP Address Management
  virtual IPAddr_t GetIPAddr();          // Get the IPAddres of this node
  virtual void     SetIPAddr(IPAddr_t i);// Set the IPAddres of this node
  virtual void     IPAddrs(IPMaskVec_t&);// Get all IP/Masks for this node
  virtual bool     LocalIP(IPAddr_t);    // True if specified ip is local
  virtual bool     IPKnown();            // True if non-default ip assigned

  // Real/Ghost test
  virtual bool     IsReal() = 0;         // True if real node

  // Satellite functions, overridden if needed
  virtual bool     FirstBitRx() const { return false;}
  virtual void     FirstBitRx(bool)   { }
  
  // switching functions
  virtual void IsSwitchNode(bool) = 0;
  virtual bool IsSwitchNode() = 0;

  virtual void PacketRX(Packet* p, Interface* iface) = 0;



  // Interfaces
  virtual Interface* AddInterface(Interface*);
  virtual Interface* AddInterface(const L2Proto&, bool bootstrap=false);
  virtual Interface* AddInterface(const L2Proto&,
                                  IPAddr_t, Mask_t,
                                  MACAddr = MACAddr::Allocate(),
				  bool bootstrap = false);
  virtual Interface* AddInterface(const L2Proto&,const Interface&,
				  IPAddr_t, Mask_t, 
				  MACAddr = MACAddr::Allocate(),
				  bool bootstrap = false);
  virtual Count_t    InterfaceCount();   // Get count of interfaces
  virtual const IFVec_t& Interfaces();   // Get complete list of interfaces
  virtual Interface* GetIfByLink(Link*); // Get iface to the specified link
  virtual Interface* GetIfByNode(Node*); // Get iface to the specified node
  virtual Interface* GetIfByIP(IPAddr_t);// Get iface with specified IP

  // Broadcast packet on all interfaces
  virtual void       Broadcast(Packet*, Proto_t) = 0;

  // Links.  All return local interface
  // Add duplex link to/from remote
  virtual Interface* AddDuplexLink(Node*);
  virtual Interface* AddDuplexLink(Node*, const Linkp2p&);
  // Next two specify new links between existing interfaces
  virtual Interface* AddDuplexLink(Interface*,Interface*);
  virtual Interface* AddDuplexLink(Interface*,Interface*, const Linkp2p&);
  //virtual Interface* AddDuplexLink(Interface*, Interface*, const Linkp2p&, Node*);
  // Next two specify local and remote IPAddress and Masks
  virtual Interface* AddDuplexLink(Node*,
                                   IPAddr_t, Mask_t, IPAddr_t, Mask_t);
  virtual Interface* AddDuplexLink(Node*, const Linkp2p&,
                                   IPAddr_t, Mask_t, IPAddr_t, Mask_t);
  virtual Link*      GetLink(Node*);    // Get link to specified node
  // Next two are for distributed simulations
  virtual Interface* AddRemoteLink(IPAddr_t, Mask_t);
  virtual Interface* AddRemoteLink(IPAddr_t, Mask_t, Rate_t, Time_t);

  // Simplex links, for modeling one-way links
  virtual Interface* AddSimplexLink(Node*);
  virtual Interface* AddSimplexLink(Node*, const Linkp2p&);
  virtual Interface* AddSimplexLink(Node*, const Linkp2p&,
                                    IPAddr_t, Mask_t);

  // Queues
  virtual Queue*     GetQueue() = 0;        // Return queue if only one i/f
  virtual Queue*     GetQueue(Node*) = 0;   // Get the queue to specified node

  // Applications
  virtual Application* AddApplication(const Application& a) = 0;

  // Neighbor management (used by some routing protocols)
  virtual void        Neighbors(NodeWeightVec_t &, bool); // List of neighbors
  virtual Count_t     NeighborCount();       // Count of routing neighbors
  // Get neighbors on given iface
  virtual void        NeighborsByIf(Interface*, IPAddrVec_t&);

  // Routing
  virtual void DefaultRoute(RoutingEntry) = 0;     // Specify default route
  virtual void DefaultRoute(Node*) = 0;            // Specify default route
  virtual void AddRoute( IPAddr_t, Count_t, Interface*, IPAddr_t) = 0;
  virtual RoutingEntry LookupRoute(IPAddr_t) = 0;  // Find a routing entry
  virtual RoutingEntry LookupRouteNix(Count_t) = 0;// Lookup route by NIx
  virtual Routing::RType_t RoutingType() = 0;      // Report type rtg in use
  virtual Interface* LocalRoute(IPAddr_t);         // If dst directly connected
  virtual void       InitializeRoutes() = 0;       // Routing init (if any)
  virtual void       ReInitializeRoutes(bool) = 0; // Routing re-initialization
  virtual Count_t    RoutingFibSize() const = 0;   // Statistics, size of FIB
  virtual Count_t    GetNix(Node*) const;          // Neighbor Index for node
  virtual RoutingNixVector* GetNixRouting() = 0;   // Returns Nix rtg pointer
  virtual Routing*   GetRouting() = 0;             // Returns routing object

  //AODV Routing
  //Specially for AODV routing, it is not compatiable with
  //other route entry.
  //
  virtual AODVRoutingEntry *LookupRouteAODV(IPAddr_t ip) =0;
  virtual void      SetRoutingAODV(void *pRouting)= 0;
  virtual RoutingAODV *GetRoutingAODV() = 0;

  // Protocol Graph Interface
  virtual Protocol* LookupProto(Layer_t, Proto_t) = 0;// Lookup proto by layer
  virtual void      InsertProto(Layer_t, Proto_t, Protocol*) = 0;

  // Port Demux Interface
  // Register port usage
  virtual bool      Bind(Proto_t, PortId_t, Protocol*) = 0;
  virtual bool      Bind(Proto_t,
                         PortId_t, IPAddr_t,
                         PortId_t, IPAddr_t,
                         Protocol*) = 0;           // Register port usage
  virtual PortId_t  Bind(Proto_t, Protocol*) = 0;  // Choose port, register
  virtual bool      Unbind(Proto_t, PortId_t, Protocol*) = 0;// Remove binding
  virtual bool      Unbind(Proto_t,
                           PortId_t, IPAddr_t,
                           PortId_t, IPAddr_t,
                           Protocol*) = 0;         // Remove port binding
  virtual Protocol* LookupByPort(Proto_t, PortId_t) = 0;
  virtual Protocol* LookupByPort(Proto_t,
                                 PortId_t, IPAddr_t,
                                 PortId_t, IPAddr_t) = 0;  // Lookup by s/d port/ip

  // Packet tracing interface
  // Trace header if enabled
  virtual bool      TracePDU(Protocol*, PDU*, Packet*, char*) = 0;
  // Set trace level this node
  virtual void      SetTrace(Trace::TraceStatus) = 0;

  // Node location (x, y, z) interface
  virtual void      SetLocation(Meters_t, Meters_t, Meters_t) = 0;
  virtual void      SetLocation(const Location&) = 0;
  virtual void      SetLocationLongLat(const Location&) = 0;
  virtual bool      HasLocation() = 0;       // True if location assigned
  virtual Meters_t  LocationX() = 0;
  virtual Meters_t  LocationY() = 0;
  virtual Meters_t  LocationZ() = 0;
  virtual Location  GetLocation() = 0;       // Return current location
  virtual Location  UpdateLocation() = 0;    // Get updated location
  virtual Mobility* AddMobility(const Mobility&) = 0;    // Add mobility model
  virtual Mobility* GetMobility() const = 0; // Get the current mobility model
  virtual bool      IsMobile() = 0;          // True if mobility enabled
  virtual bool      IsMoving() = 0;          // True if not pausing
  virtual bool      WirelessTx() = 0;        // True if wireless transmitting
  virtual bool      WirelessRx() = 0;        // True if wireless receiving
  virtual bool      WirelessCx() = 0;        // True if wireless colliding
  virtual bool      WirelessRxMe() = 0;      // True if wireless receiving
  virtual bool      WirelessRxZz() = 0;      // True if wireless receiving
  virtual void      UserInformation(void*) = 0;   // Specify user info
  virtual void*     UserInformation() = 0;        // Return user info
  void UseWormContainment(bool);
  bool UseWormContainment() { return usewormcontainment; }
  virtual void SetWormContainment(WormContainment*) = 0;
  virtual void UseARP(bool) = 0;

  virtual WormContainment* GetWormContainment() = 0;

  // QTWindow information
  virtual void           Show(bool) = 0;          // Turn on/off display
  virtual bool           Show() = 0;              // True if display enabled
  virtual QCanvasItem*   Display(QTWindow*) = 0;
  virtual QCanvasItem*   Display(const QPoint&, QTWindow*) = 0;
  virtual void           WirelessTxColor(const QColor&) = 0;
  virtual const QColor&  WirelessTxColor() = 0;
  virtual bool           PushWirelessTx(QCanvasItem*) = 0;
  virtual QCanvasItem*   PopWirelessTx() = 0;
  virtual void           PixelSize(Count_t) = 0;
  virtual Count_t        PixelSizeX() = 0;
  virtual Count_t        PixelSizeY() = 0;
  virtual void           Shape(Node::Shape_t) = 0;
  virtual Node::Shape_t  Shape() = 0;
  virtual CustomShape_t  CustomShape();
  virtual void           CustomShape(CustomShape_t);
  virtual bool           CustomShapeFile(const char*);
  virtual bool           CustomShapeImage(const Image&);
  virtual void           Color(const QColor&) = 0;
  virtual bool           HasColor() = 0;
  virtual QColor&        Color() = 0;
  virtual NodeAnimation* GetNodeAnimation() const = 0;
  
  // ICMP related
  virtual bool   ICMPEnabled() const;
  virtual void   DisableICMP();

  // Failure management
  virtual void   Down();
  virtual void   Up();
  virtual bool   IsDown();

  // Wireless transmitter information
  virtual Meters_t  Distance(Node*) = 0;
  virtual void      BuildRadioInterfaceList(WirelessLink*) = 0;
  virtual const RadioVec_t& GetRadioInterfaceList() = 0;
  virtual void      SetRadioRange(Meters_t ) =0;
  virtual Meters_t  GetRadioRange(void ) =0;

  virtual void      AddCallback(Layer_t, Proto_t,
                                PacketCallbacks::Type_t,
                                Interface*,
                                PacketCallbacks::Function_t) = 0;
  virtual void      AddCallbackHead(Layer_t, Proto_t,
                                    PacketCallbacks::Type_t,
                                    Interface*,
                                    PacketCallbacks::Function_t) = 0;
  virtual void      DeleteCallback(Layer_t, Proto_t, 
                                   PacketCallbacks::Type_t,
                                   Interface*) = 0;
  virtual bool      CallCallbacks(Layer_t, Proto_t, 
                                  PacketCallbacks::Type_t, Packet*,
                                  Interface*) = 0;

  virtual void      setBattery (Joules_t) = 0;
  virtual Joules_t  getBattery (void) = 0;

  virtual void      setComputePower (double) = 0;
  virtual double    getComputePower (void) = 0;

  virtual void        setRouteTable (RouteTable*) = 0;
  virtual RouteTable* getRouteTable (void) = 0;

  
protected:
  Node*    pNode;         // Points to corresponding node interface object
  bool     usewormcontainment;      // Does this node use worm containment
  // Data items common between real and ghost nodes
  IPAddr_t ipAddr;                  // IP Address for this node
  IFVec_t  interfaces;              // All defined network interfaces (may be ghosts)
  Count_t  neighborCount;           // Total number of neighbors
  bool     down;                    // True if node is down
};

#endif
