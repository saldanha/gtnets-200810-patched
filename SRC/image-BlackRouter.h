// Generated automatically by make-gtimage from BlackRouter.png
// DO NOT EDIT

#include "image.h"

class BlackRouterImage : public Image {
public:
  BlackRouterImage(){}
  const char* Data() const { return data;}
  int Size() const { return size;}
  operator const char*() { return data;}
  static const char* data;
  static int size;
};
