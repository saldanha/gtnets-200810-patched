/****************************************************************************/
/*   Georgia Tech Network Simulator - ITM (Internet Topology Modeler) class */
/*                                    Interface between GTNetS & BRITE      */
/*   Author: Talal M. Jaafar                                                */
/*   Revision: 1.1    12/02/2004                                            */
/*             1.2    12/09/2004                                            */
/*                                                                          */
/****************************************************************************/

/****************************************************************************/
/*                  Copyright 2001, Trustees of Boston University.          */
/*                               All Rights Reserved.                       */
/*                                                                          */
/* Permission to use, copy, or modify this software and its documentation   */
/* for educational and research purposes only and without fee is hereby     */
/* granted, provided that this copyright notice appear on all copies and    */
/* supporting documentation.  For any other uses of this software, in       */
/* original or modified form, including but not limited to distribution in  */
/* whole or in part, specific prior permission must be obtained from Boston */
/* University.  These programs shall not be used, rewritten, or adapted as  */
/* the basis of a commercial software or hardware product without first     */
/* obtaining appropriate licenses from Boston University.  Boston University*/
/* and the author(s) make no representations about the suitability of this  */
/* software for any purpose.  It is provided "as is" without express or     */
/* implied warranty.                                                        */
/*                                                                          */
/****************************************************************************/
/*                                                                          */
/*  Author:     Alberto Medina                                              */
/*              Anukool Lakhina                                             */
/*  Title:     BRITE: Boston university Representative Topology gEnerator   */
/*  Revision:  2.0         4/02/2001                                        */
/****************************************************************************/

#include <vector>
#include <string>
//#include <node.h>

class Node;
class Model;
class Topology;
class AbsNode;

//Doc:ClassXRef
class Itm {
  //Doc:Class Objects of class {\tt Itm} are used in \GTNS\ to represent a
  //Doc:Class random generated topology.  The generated topology is constructed 
  //Doc:Class using BRITE, the Boston university Representative Internet Topology 
  //Doc:Class gEnerator, which supports a wide variety of generation models.
 public:
  static std::vector<int> ASBorders;
  enum NodeColor { WHITE_ = 0, GRAY_ = 1, BLACK_ = 2 };
  
  //Doc:Desc The constructor for the {\tt Itm} object takes two arguments. 
  //Doc:Desc The ChildMaxNum is the number of children per
  //Doc:Desc per leafnode which are uniformly 
  //Doc:Desc assigned to the leafnodes.  The second parameter which is the 
  //Doc:Desc configFile, a simple Key-Value text file. The configFile could 
  //Doc:Desc either define the model to be used when generating the topology, 
  //Doc:Desc or import the topology model (such as GT-ITM model).
  //Doc:Arg1 The number of children per leafnode (uniform distribution)
  //Doc:Arg2 The configuration file to be used for specific topology model)
  Itm(int ChildMaxNum, char* configFile);
  
  //Doc:Method
  int GetASCount();
  //Doc:Desc Returns the number of ASes in the simulated topology
  
  //Doc:Method
  std::vector<Node*> GetASchildren(unsigned int ASnum);
  //Doc:Desc Returns a vector of child nodes per the specified AS

 private:
  Topology* T;
   
  //Doc:Method
  void BFS_IP(std::vector<AbsNode*>& N, int rootIndex, std::vector<int>& color, int numOfBorders, int borderNum);
  //Doc:Desc A BFS search on a given border node to assign IP addresses for all leaf
  //Doc:Desc nodes under that border node
  //Doc:Arg2 rootIndex is the border index in the BRITE node vector (N, passed in as arg1)
  //Doc:Arg4 numOfBorders is the total number of borders in the AS
  //Doc:Arg5 borderNum is the border number in the AS

  //Doc:Method
  void BFS_Size(std::vector<AbsNode*>& N, int rootIndex, std::vector<int>& color);
  //Doc:Desc A BFS search on a given border node to determine the number of subnodes
  //Doc:Desc and leafnodes under that border node
  //Doc:Arg2 rootIndex is the border index in the BRITE node vector (N, passed in as arg1)

  //Doc:Method
  int NumOfBits(int num);
  //Doc:Desc Given an int, this function will return the number
  //Doc:Desc of bits required to represent that int in binary form
  //Doc:Arg1  num is the int to be converted to binary form
  //Doc:Return number of bits to represent the passed int

  //Doc:Method
  const char* IntToBits(int numBits, int num);
  //Doc:Desc This function will convert an int to its binary
  //Doc:Desc representation using the specified number of bits
  //Doc:Arg1  numBits should be big enough to represent the passed int
  //Doc:Arg2  num is the int to be converted to binary form
  //Doc:Return a binary representation of an int

  //Doc:Method
  unsigned long BitsToInt(char* bits, int numBits);
  //Doc:Desc This function will convert a binary number to its
  //Doc:Desc decimal form
  //Doc:Arg1 the binary representation
  //Doc:Arg2 the number of bits
  //Doc:Return decimal value of a binary number
};
