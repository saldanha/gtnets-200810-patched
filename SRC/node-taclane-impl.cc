#include <iostream>

#include "common-defs.h"
#include "debug.h"
#include "node-real.h"
#include "node-taclane-impl.h"
#include "l3prototaclane.h"
#include "node-taclane.h"
#include "interface.h"


//Need to manually setup Security Associations
//Need to manually setup Routing

//Duplexlink.h has been modified so that interfaces cannot be added to a TACLANE node.
//This limits the TACLANE to 1 plain text interface and 1 cypher text interface.

using namespace std;

NodeTACLANEImpl::NodeTACLANEImpl(TACLANENode* n,
                                 IPAddr_t ptipaddr, IPAddr_t ctipaddr) 
    : NodeReal(n)
{
  ptif = AddInterface(L2Proto802_3(), ptipaddr, Mask(32));
  ctif = AddInterface(L2Proto802_3(), ctipaddr, Mask(32));
  DEBUG0((cout << "TLNode " << n->Id()
          << " added ptif " << (string)IPAddr(ptipaddr)
          << " ctif " << (string)IPAddr(ctipaddr) << endl));
}

NodeImpl* NodeTACLANEImpl::Copy() const 
{
  return new NodeTACLANEImpl(*this);
}

// Overrides for the add interface methods
// Insure we only have two

Interface* NodeTACLANEImpl::AddInterface(const L2Proto& l2, bool bootstrap)
{
  if (InterfaceCount() == 2)
    {
      cout << "Can't add more than two interfaces to a Taclane device" << endl;
      return nil;
    }
  return NodeImpl::AddInterface(l2, bootstrap);
}

Interface* NodeTACLANEImpl::AddInterface(const L2Proto& l2,
                                         IPAddr_t i, Mask_t mask,
                                         MACAddr m, bool bootstrap)
{
  if (InterfaceCount() == 2)
    {
      cout << "Can't add more than two interfaces to a Taclane device" << endl;
      return nil;
    }
  return NodeImpl::AddInterface(l2, i, mask, m, bootstrap);
}


Interface* NodeTACLANEImpl::AddInterface(const L2Proto& l2,
                                         const Interface& iface,
                                         IPAddr_t i, Mask_t mask, 
                                         MACAddr m, bool bootstrap)
{
  if (InterfaceCount() == 2)
    {
      cout << "Can't add more than two interfaces to a Taclane device" << endl;
      return nil;
    }
  return NodeImpl::AddInterface(l2, iface, i, mask, m, bootstrap);
}



