#include "encryption.h"

Encryption::Encryption () 
{
	level = NOENC;
}
	
void Encryption::SetEncryption( Encrypt enc )
{ 
	if( enc==NOENC || enc==PT || enc == CT )
		level = enc;
}
		
Encrypt Encryption::GetEncryption()
{
	return level;
}
