// Make the Class to include file cross reference.
// George F. Riley, Georgia Tech, Winter 2002

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <set>

#include "stringtok.h"

using namespace std;

string pound("\\#");
string space("\\ ");
string leftleft("<{}<");
string leftarrow("<");
string bf("{\\bf ");
string endbf("}&");
string backback("\\\\");
string openbracket("\\{");
string closebracket("\\}");
string underscore("\\_");
string percent("\\%");
string andsym("\\&");
string backslash("$\\backslash$");
string dollar("\\$");
string letters("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
string whitespace("\011\012\013\014\015\040");

typedef std::vector<std::string> StringVec_t;

class XRef {
public:
  XRef(const char* f, const string c) : fileName(f), className(c) { }
  string      fileName;  // Name of include file
  string      className; // Name of class
  StringVec_t inherited; // List of sub-classed classes
};

// Define a comparator for the sorted set of XRef's
class XRefLess
{
public:
  XRefLess() { }
  bool operator()(const XRef& l, const XRef& r) const {
    return (l.className < r.className);
  }
};

typedef std::set<XRef, XRefLess> XRefSet_t;

XRefSet_t xref;

void ProcessOneFile(char* pFn)
{
  char* pName = pFn;
  char* pCh;
  // Find the last /
  while ((pCh = strchr(pName, '/')) != NULL) pName = &pCh[1];
  cout << "Working on " << pFn << endl;
  ifstream f(pFn);
  if (!f)
    {
      cout << "HuH? Can't open " << pFn << endl;
      return;
    }
  int lineNum = 0;
  while(f)
    { // Look for the //Doc:ClassXRef flags
      string s;
      getline(f, s);
      lineNum++;
      if (s.length() == 0) continue;  //Empty
      StringVec_t tokens;
      stringtok(tokens, s);
      //cout << "Line " << lineNum << " Tokens.size " << tokens.size() << endl;
      //if (tokens.size() == 1)
      //  cout << "First token lth " << tokens[0].length() << endl;
      if (!tokens.size()) continue; // Empty
      if (tokens[0] == "//Doc:ClassXRef")
        { // Found it
          if (!f) continue; // ? No following line?
          getline(f,s);
          lineNum++;
          tokens.clear();
          stringtok(tokens, s, " ,\t\n");
          if (!tokens.size())
            {
              cout << "HuH? Empty line after XRef flag" << endl;
              cout << "File " << pFn << " line " << lineNum << endl;
              continue;
            }
          if (tokens.size() < 3)
            {
              cout << "HuH? Too few tokens after XRef flag" << endl;
              cout << "File " << pFn << " line " << lineNum << endl;
              continue;
            }
          if (tokens[0] != "class")
            {
              cout << "HuH? Missing class after XRef flag" << endl;
              cout << "Found " << tokens[0] << endl;
              cout << "File " << pFn << " line " << lineNum << endl;
              continue;
            }
          XRef xr(pName, tokens[1]);
          if (tokens[2] == ":")
            { // found subclasses
              for (unsigned int j = 4; j < tokens.size(); ++j)
                {
                  if (tokens[j] == "{") break;
                  if (tokens[j] == "public") continue;
                  xr.inherited.push_back(tokens[j]);
                }
            }
          xref.insert(xr); // Add to the sorted set
        }
    }
}

string NameToLatex(const string& n)
{ // Convert a name to a latex compatible string
  string outln;
  for (string::const_iterator i = n.begin(); i != n.end(); ++i)
    { // process each character
      char ch = *i;
      switch(ch) {
//       case '#' :
//         outln.append(pound);
//         break;
//       case  ' ':
//         outln.append(space);
//         break;
//       case  '{':
//         outln.append(openbracket);
//         break;
//       case  '}':
//         outln.append(closebracket);
//         break;
      case  '_':
        outln.append(underscore);
        break;
      case  '%':
        outln.append(percent);
        break;
      case  '&':
        outln.append(andsym);
        break;
      case  '$':
        outln.append(dollar);
        break;
//       case  '\\':
//         outln.append(backslash);
//         break;
      case '<' :
        {
          string::const_iterator nexti = i + 1;
          char nch = (nexti == n.end() ? ' ' : *nexti);
          if (nch == '<') 
            {
              outln.append(leftleft);
              i++;
            }
          else
            {
              outln.append(leftarrow);
            }
        }
        break;
      default:
        {
          char work[2];
          work[0] = ch;
          work[1] = 0;
          outln.append(work);
        }
        break;
      }
    }
  return outln;
}

#define LINES_PER_PAGE 55
#define LINES_PER_PAGE_FIRST 59
void PrintXRefTable()
{
  ofstream f("classxref.tex");
  if (!f)
    {
      cout << "HuH?  Can't open classxref.tex" << endl;
      return;
    }
  bool first = true;
  int  linesLeft = LINES_PER_PAGE_FIRST;
  for (XRefSet_t::iterator i = xref.begin(); i != xref.end(); ++i)
    {
      if (first)
        { // Need the table header
          f << "\\begin{tabular}{|l|l|l|} \\hline" << endl;
          f << "Class Name & Include File & Hierarchy \\\\ \\hline"
            << endl;
          first = false;
        }
      f << NameToLatex(i->className) << "&" << i->fileName << "&";
      // Print the class hierarchy
      for (StringVec_t::const_iterator j = i->inherited.begin();
           j != i->inherited.end(); ++j)
        {
          f << " " << *j;
        }
      f << "\\\\ \\hline" << endl;
      if (!--linesLeft)
        {
          f << "\\end{tabular}" << endl;
          f << "\\newpage" << endl;
          first = true; // new table header on next one
          linesLeft = LINES_PER_PAGE;
        }
    }
  if (!first)
    {
      f << "\\end{tabular}" << endl;
    }
}

int main( int argc, char** argv)
{
  for (int i = 1; i < argc; ++i)
    {
      ProcessOneFile(argv[i]);
    }
  PrintXRefTable();
#ifdef VERBOSE 
  for (XRefSet_t::iterator i = xref.begin(); i != xref.end(); ++i)
    {
      cout << "File " << i->fileName << " class " << i->className << endl;
      if (i->inherited.size())
        {
          cout << "Hierarchy";
          for (StringVec_t::const_iterator j = i->inherited.begin();
               j != i->inherited.end(); ++j)
            {
              cout << " " << *j;
            }
          cout << endl;
        }
    }
#endif
}

