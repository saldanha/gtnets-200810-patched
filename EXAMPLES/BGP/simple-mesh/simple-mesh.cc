//
// Sample test program to instantiate a BGP router
// Sunitha Beeram, Georgia Tech, Spring 2005
//

#include <iostream>
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "bgp.h"
#include "ratetimeparse.h"
#include "linkp2p.h"

using namespace std;

int main(int argc,char ** argv)
{
    Simulator Sim;
    char command[]="show ip bgp";
    Node *node[4];
    BGP *bgpRouter[4];
    Linkp2p lk(Rate("500Kb"), Time("20ms"));
	int StopTime = 1000;

    Node::DefaultShape(Node::CIRCLE);
    
    Trace* tr= Trace::Instance();
    tr->Open("anycast.txt");
    
    Uniform startRng(0,0.1);
    
    for (int i=0; i < 4; ++i)
    {
        node[i]= new Node();
        bgpRouter[i]= new BGP(0);
        bgpRouter[i]->AttachNode(node[i]);
    }
        
    /*AS1*/
    
    bgpRouter[0]->config_file("./AS1/bgpd1.conf");
    bgpRouter[1]->config_file("./AS1/bgpd2.conf");
    bgpRouter[2]->config_file("./AS1/bgpd3.conf");
    node[2]->AddDuplexLink(node[0],lk);
    node[2]->AddDuplexLink(node[1],lk);
	node[1]->AddDuplexLink(node[0],lk);
   
	/*AS2*/
 
    bgpRouter[3]->config_file("./AS2/bgpd1.conf");
	node[3]->AddDuplexLink(node[2],lk);

    /*schedule an event to check the bgp routing tables*/
    
    for (int i =0; i < 4 ; ++i)
    {
        BGPEvent *ev = new BGPEvent(BGPEvent::COMMAND);
        ev->argv = (char*)malloc(sizeof(char)*strlen(command)+1);
        strcpy(ev->argv,command);
        bgpRouter[i]->timer->Schedule(StopTime-1,ev);
    }

    bgpRouter[0]->StartAt(0.1);
    bgpRouter[1]->StartAt(0.1);
    bgpRouter[2]->StartAt(0.1);
    bgpRouter[3]->StartAt(0.1);
    bgpRouter[0]->StopAt(StopTime);
    bgpRouter[1]->StopAt(StopTime);
    bgpRouter[2]->StopAt(StopTime);
    bgpRouter[3]->StopAt(StopTime);
    
    Sim.StopAt(StopTime);
    cout << "Starting Simulation" << endl;
    Sim.Run();
    cout << "Simulation complete" << endl;
}

