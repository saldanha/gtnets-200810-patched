#include <iostream>
#include <string>

#include "pdu.h"
#include "packet.h"
#include "linkp2p.h"
#include "ratetimeparse.h"

using namespace std;

class MyPDU : public PDU
{
public:
  MyPDU(const string& s1) : s(s1) {}
  Size_t Size() const { return 10;}
  PDU*   Copy() const { return new MyPDU(*this);}
public:
  string s;
};

void PrintPDUS(Packet* p)
{
  for (PDUVec_t::size_type i = 0; i < p->top; ++i)
    {
      MyPDU* pdu = (MyPDU*)p->PDUs[i];
      cout << pdu->s << endl;
    }
}

int main()
{
  Packet* p = new Packet();
  p->PushPDU(new MyPDU("one"));
  p->PushPDU(new MyPDU("two"));
  p->PushPDU(new MyPDU("three"));
  p->PushPDU(new MyPDU("four"));
  p->PushPDU(new MyPDU("five"));
  PrintPDUS(p);
  p->InsertPDU(new MyPDU("above-four"), 1);
  cout << endl;
  PrintPDUS(p);
  p->InsertPDU(new MyPDU("above-five"));
  cout << endl;
  PrintPDUS(p);
  // Now remove a few
  p->PopPDU();
  p->PopPDU();
  p->PopPDU();
  p->PopPDU();
  p->InsertPDU(new MyPDU("above-three"));
  cout << endl;
  PrintPDUS(p);

  Linkp2p l1(Rate("10Mb"), Time("10ms"));
  Linkp2p l2(Rate("100Mb"), Time("1ms"));
  l2 = l1;
}

  
