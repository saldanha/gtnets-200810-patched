// Generated automatically by make-gtimage from ../IMAGES/gb_taclane.png
// DO NOT EDIT

#include "image.h"

class gb_taclaneImage : public Image {
public:
  gb_taclaneImage(){}
  const char* Data() const { return data;}
  int Size() const { return size;}
  operator const char*() { return data;}
  static const char* data;
  static int size;
};
