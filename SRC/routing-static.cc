// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: routing-static.cc 136 2004-10-11 19:40:49Z riley $



// Georgia Tech Network Simulator - Static Routing class
// George F. Riley.  Georgia Tech, Spring 2002

// Statically (one-time) computed and specified routes

#include <iostream>
#include <vector>
#include <map>
#include <string>

// Uncomment below to enable debug level 0
// #define DEBUG_MASK 0x01
//#define DEBUG_MASK 0x4
//#define DEBUG_MASK 0x7
#include "debug.h"
#include "routing.h"
#include "routing-static.h"
#include "mask.h"
#include "ipaddr.h"
#include "node.h"
#include "bfs.h"
#include "hex.h"
#include "interface.h"

using namespace std;

void RoutingStatic::Default(RoutingEntry r)
{
  defaultRoute = r; // Set the default route
#ifndef WIN32
  DEBUG(1,(cout << "RoutingStatic::Default if " << r.interface
          << " ip " << (string)IPAddr(r.nexthop) << endl));
#else
  DEBUG(1,(cout << "RoutingStatic::Default if " << r.interface_
          << " ip " << (string)IPAddr(r.nexthop) << endl));
#endif
}

RoutingEntry RoutingStatic::GetDefault()
{
  return defaultRoute;
}

void RoutingStatic::Add( IPAddr_t a, Mask_t m, Interface* i, IPAddr_t ip)
{
  Count_t mBits = Mask(m).NBits();
  DEBUG(2,(cout << "StaticRouting::Add, target ip " << (string)IPAddr(a)
           << " next hop ip " << (string)IPAddr(ip) 
           << " maskBits " << mBits
           << endl));
  DEBUG(2,(cout << "Befor insert, size of " << mBits 
           << " fib is " << fib[mBits].size() << endl));
  fib[mBits].insert(FIB_t::value_type(a & (Mask_t)Mask(m),
                                  RoutingEntry(i, ip))); // Insert in FIB
  DEBUG(2,(cout << "After insert, size of " << mBits 
           << " fib is " << fib[mBits].size() << endl));
}

RoutingEntry RoutingStatic::Lookup(Node*, IPAddr_t t)
{ // Lookup routing entry for specified target address
  for (FIBMap_t::reverse_iterator ri = fib.rbegin(); ri != fib.rend(); ++ri)
    {
      DEBUG0((cout << "RoutingStatic::Lookup, checking mask lth " 
              << ri->first << endl));
      FIB_t::iterator i = ri->second.find(t & Mask(ri->first));
      if (i != ri->second.end()) return i->second; // Return the routing entry
    }
  return defaultRoute; // Return the default route
}

RoutingEntry RoutingStatic::LookupFromPDU(PDU*)
{// Find from PDU (NixVector)..not used for static
  return RoutingEntry();
}

Routing* RoutingStatic::Clone()
{
  return new RoutingStatic();
}

Routing::RType_t RoutingStatic::Type()
{
  return STATIC;
}

void    RoutingStatic::InitializeRoutes(Node* r)
{ // Initialization, Computing routing table for this node
  // For static routing, use BFS if all link weights default,
  // otherwise use Dijkstra (note...Dijkstra not implemented yet)
  // But first, find out if single neighbor (leaf) node, in which
  // case, no routing computations are necessary
  Count_t nc = r->NeighborCount();
  if (nc <= 1)
    {
      DEBUG0((cout << "Node " << r->Id()
              << " has only 1 neighbor, skipping routes" << endl));
      return;
    }
  DEBUG0((cout << "Node " << r->Id()
          << " has " << nc << " neighbors, calculating routes" << endl));
  NodeIfVec_t nextHop;      // Next hop vector
  NodeVec_t   parent;       // Parent vector
  const NodeVec_t&  nodes = Node::GetNodes();
  IPAddrVec_t       aliases;
  BFS(nodes, r, nextHop, parent, IPADDR_NONE, aliases);
  // For debug, print the nh vector
  DEBUG0((cout << "Node " << r->Id() << " printing nh table" << endl));
  DEBUG0({for (NodeVec_t::size_type i = 0; i < nextHop.size(); ++i)
    {
      cout << "  NextHop for dest " << i << " is ";
      if (nextHop[i].node)
        {
          cout << nextHop[i].node->Id() << endl;
        }
      else
        {
          cout << "nil pointer" << endl;
        }
    }});
  // First count the most common nexthop, and use that for default
  vector<Count_t> c(nodes.size(), 0);
  Count_t maxCount = 0;      // Maximum value found
  NodeIf  maxNodeIf(nil,nil);
 
  for (NodeVec_t::size_type i = 0; i < nextHop.size(); ++i)
    {
      if (nextHop[i].node)
        { // Next hop exists
          if (++c[nextHop[i].node->Id()] > maxCount)
            { // Found new maximum
              maxCount  = c[nextHop[i].node->Id()];
              maxNodeIf = nextHop[i];
            }
        }
    }
  DEBUG0((cout << "Node " << r->Id() << " max count is " << maxCount
          << " to node " << maxNodeIf.node->Id() << endl));
  if (maxNodeIf.iface)
    { // At least one found
      Default(RoutingEntry(maxNodeIf.iface,
                           maxNodeIf.iface->NodePeerIP(maxNodeIf.node)));
    }
  // Now create routing table entries
  for (NodeVec_t::size_type i = 0; i < nextHop.size(); ++i)
    {
      Node*      target = nodes[i];
      Node*      node = nextHop[i].node;
      Interface* iface = nextHop[i].iface;
      if (node) 
        { // Node exists
          if (node  != maxNodeIf.node ||
              iface != maxNodeIf.iface)
            { // Non-default found, add the routes
              DEBUG(2,(cout << "Node " << r->Id()
                       << " Non-default route for target " << i << endl));
              // Get a list of all IP addresses for the target node
              IPMaskVec_t ips;
              target->IPAddrs(ips);
              for (IPMaskVec_t::size_type j = 0; j < ips.size(); ++j)
                {
                  DEBUG(2,(cout << "  IPAddr is " << (string)IPAddr(ips[j].ip)
                           << " mask " << Hex8(ips[j].mask) 
                           << " iface " << iface << endl));
                  // Add to routing table
                  Add(ips[j].ip, ips[j].mask, iface, iface->NodePeerIP(node));
                }
            }
        }
    }  
}

void    RoutingStatic::ReInitializeRoutes(Node* r, bool  u)
{ // Respond to topology changes
  // Naieve approach, just clear the fibs and re-calculate
  for (FIBMap_t::iterator i = fib.begin(); i != fib.end(); ++i)
    {
      i->second.clear();
    }
  InitializeRoutes(r);
}
  
bool    RoutingStatic::NeedInit()
{ // Initialization needed for static
  return true;
}

bool    RoutingStatic::NeedReInit()
{ // Initialization needed for static
  return true;
}

Size_t  RoutingStatic::Size() const
{ // For statistics, return the size (bytes) of the FIB
  Size_t s = 0;
  DEBUG0((cout << "Hello from RoutingStatic::Size() fibsize " << fib.size() 
          << endl));
  for (FIBMap_t::const_iterator i = fib.begin(); i != fib.end(); ++i)
    {
      s += i->second.size() * (sizeof(IPAddr_t) + sizeof(RoutingEntry));
      DEBUG0((cout << "Working on mask length " << i->first
              << " fibsize " << i->second.size()
              << " totsize " << s << endl));
    }
  return s;
}

