// Test the background maps function of GTNetS
// George F. Riley, Georgia Tech, Summer 2003

#include "simulator.h"
#include "ratetimeparse.h"
#include "location.h"
#include "validation.h"  

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  StringVec_t sv;
  sv.push_back("namer-bdy.txt");
  
  if (argc > 1)
    {
      sv.clear();
      for (int i = 1; i < argc; ++i)
        {
          sv.push_back(argv[i]);
        }
    }
  
  Simulator  s;
  // Display the background map of North America
  s.AddBackgroundMap(sv);
  
  //s.AddBackgroundMap(sv,
  //                   RectRegion(Location(-130.0, 20.0),Location(-60.0, 50.1)));
  
  s.Progress(1);
  s.StopAt(100);
  s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
  s.StartAnimation(0, true);
  s.Run();
}

  
