// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-voip.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - VOIP application class
// Young J. Lee.  Georgia Tech, Fall 2004
// AIFF interface added by George F. Riley, Georgia Tech, Spring 2005


// Define a voip application class

// Uncomment below to enable debug level 0
//#define DEBUG_MASK 0x1
#include "debug.h"
#include "application-voip.h"
#include "udp.h"
#include "node.h"
#include "simulator.h"
#include "aiff.h"
#include "ua-law.h"

using namespace std;

Count_t VOIPApplication::activeCalls;   // Debug..number of active calls
// Codec to use
VOIPApplication::Codec_t VOIPApplication::codec = VOIPApplication::CODEC_ULAW;
// Custom codec callback
CodecCallback_t VOIPApplication::codecCb = nil;
  
VOIPApplication::VOIPApplication(IPAddr_t d, PortId_t pid,
                                 const Random& on, const Random& off,
                                 Rate_t r, Size_t s)
    : OnOffApplication(d, pid, on, off, UDP(), r, s),
      callDuration(nil), waitDuration(nil),
      pendingSSEvent(nil), pendingDataEvent(nil),
      callFile(nil), recordFile(nil), sampleRate(8000),
      sampleIndex(0), nextRxIndex(0),
      longBuf(nil), buf(nil), silenceSupression(false), ssThreshold(0),
      verbose(false), verbose1(false)
{
}

VOIPApplication::VOIPApplication(IPAddr_t d, PortId_t pid,
                                 const string& cfn, Size_t s)
    : OnOffApplication(d, pid, Constant(0), Constant(0),
                       UDP(), 0, s),
      callDuration(nil), waitDuration(nil),
      pendingSSEvent(nil), pendingDataEvent(nil),
      callFileName(cfn), callFile(nil), recordFile(nil), 
      sampleRate(8000), sampleIndex(0), nextRxIndex(0),
      longBuf(nil), buf(nil), silenceSupression(false), ssThreshold(0),
      verbose(false), verbose1(false)
{
}

void VOIPApplication::CallDuration(const Random& r)
{ // Duration of calls
  callDuration = r.Copy();
}

void VOIPApplication::WaitDuration(const Random& r)
{ // Wait time between of calls
  waitDuration = r.Copy();
}

bool VOIPApplication::CallFile(const string& cfn)
{
  callFileName = cfn;
  return true;
}

// Event handler
void VOIPApplication::Handle(Event* e, Time_t t)
{
  AppVOIPEvent* ev = (AppVOIPEvent*)e;
  switch (ev->event) {
    case AppVOIPEvent::START_CALL :
      if (callDuration)
        { // Schedule a stop after specified time
          ev->event = AppVOIPEvent::STOP_CALL;
          Random_t callTime = callDuration->Value();
          Scheduler::Schedule(pendingSSEvent, callTime, this);
        }
      OnOffApplication::StartApp(); // Start the data flowing
      activeCalls++;
      return;
    case AppVOIPEvent::STOP_CALL :
      if (waitDuration)
        { // Schedule a start after specified time
          ev->event = AppVOIPEvent::START_CALL;
          Random_t waitTime = waitDuration->Value();
          Scheduler::Schedule(pendingSSEvent, waitTime, this);
        }
      OnOffApplication::StopApp(); // Stop the data
      activeCalls--;
      return;
    case AppVOIPEvent::NEXT_SAMPLE:
      SendNextSample();
      return;
    }
  OnOffApplication::Handle(e, t);  // May be on-off application event
}

// Application Methods
void VOIPApplication::StartApp()    // Called at time specified by Start
{
  // Bind local endpoint if not already
  if (l4Proto->Port() == NO_PORT)
    {
      l4Proto->Bind(peerPort);  // Local and remote ports the same
      l4Proto->Connect(peerIP, peerPort);
    }

  if (pendingSSEvent)
    {
      Scheduler::Cancel(pendingSSEvent);
      delete pendingSSEvent;
      pendingSSEvent = nil;
    }
  if (callDuration)
    { // The call duration RNG exists.  Schedule an event to stop the
      // call after a random time.
      Random_t callTime = callDuration->Value();
      pendingSSEvent = new AppVOIPEvent(AppVOIPEvent::STOP_CALL);
      Scheduler::Schedule(pendingSSEvent, callTime, this);
    }
  if (callFileName.length() != 0)
    { // Use an AIFF file for data, so we don't start the on-off app
      callFile = new AIFF();
      if (!callFile->Open(callFileName))
        {
          cout << "Can't open voice call file " << callFileName << endl;
        }
      else
        { // Opened ok, send the next sample
          SendNextSample();
        }
    }
  else
    {
      OnOffApplication::StartApp();
    }
  if (recFileName.length() != 0)
    {
      recordFile = new AIFF();
      recordFile->Create(recFileName);
      recordFile->SampleRate(sampleRate);
      recordFile->SampleSize(16);
      recordFile->NumChannels(1);
    }
  activeCalls++;
}

void VOIPApplication::StopApp()    // Called at time specified by Stop
{
  if (pendingSSEvent)
    { // Delete any pending events
      Scheduler::Cancel(pendingSSEvent);
      delete pendingSSEvent;
      pendingSSEvent = nil;
    }
  if (pendingDataEvent)
    { // Stop pending send data event
      Scheduler::Cancel(pendingDataEvent);
      delete pendingDataEvent;
      pendingDataEvent = nil;
    }
      
  if (callFile)
    { // Close the aiff file
      callFile->Close();
    }
  else
    { // Not using an AIFF call file, tell on-off to stop
      OnOffApplication::StopApp();
    }
  // Close the recording file
  if (recordFile) recordFile->Close();
  // Delete AIFF objects, so we will re-initialize on next call
  delete callFile;
  delete recordFile;
  callFile = nil;
  recordFile = nil;
  activeCalls--;
}

Application* VOIPApplication::Copy() const
{ // Make a copy of this application
  return new VOIPApplication(*this);
}

void VOIPApplication::Receive(Packet* p, L4Protocol* l4, Seq_t seq)
{
  Data* data = (Data*)p->PopPDU();
  Count_t dataSize = data->Size();
  if (data && dataSize >= sizeof(Count_t))
    { // Ok, got good data
      char* contents = data->Contents();
      if (contents)
        {
          Count_t* cp = (Count_t*)contents;
          Count_t seq = *cp;
          dataSize -= sizeof(Count_t);
          if (dataSize > pktSize)
            {
              DEBUG0((cout << "Got size mis-matched packet in voip app, size "
                      <<  dataSize << endl));
            }
          else
            {
              if (nextRxIndex < seq)
                {
                  DEBUG0((cout << "Missed " << seq - nextRxIndex
                          << " bytes" << endl));
                  if(verbose1)cout << "Missed " << seq - nextRxIndex
                                   << " bytes at seq " << nextRxIndex
                                   << " file "  << recFileName
                                   << endl;
                }
              while(nextRxIndex < seq)
                { // Dropped pkt, write zeros to recording file
                  if (recordFile)
                    {
                      recordFile->WriteSoundData(0, 0);
                    }
                  nextRxIndex++;
                }
              nextRxIndex = seq + dataSize;
              if (!longBuf) longBuf  = new long[pktSize];
              if (!buf)     buf      = new char[pktSize + sizeof(sampleIndex)];
              Count_t bufOffset = sizeof(Count_t);
              // Perform the decoding
              switch (codec) {
              case CODEC_ULAW:
                // Convert from ULaw to 16 bit
                for (Count_t i = 0; i < dataSize; ++i)
                  {
                    longBuf[i] = ULawToShort(contents[i + bufOffset]);
                  }
                break;
              case CODEC_ALAW:
                // Convert from ALaw to 16 bit
                for (Count_t i = 0; i < dataSize; ++i)
                  {
                    longBuf[i] = ALawToShort(contents[i + bufOffset]);
                  }
                break;
              case CODEC_CUSTOM:
                // Use the custom codec
                if (codecCb)
                  {
                    dataSize = codecCb(longBuf, &contents[bufOffset],
                                       dataSize, false);
                  }
                else
                  {
                    cout << "HuH? No VOIP custom codec callback?" << endl;
                  }
                break;
              }
              // Write to sound recording file
              recordFile->WriteSoundDataSingle(longBuf, dataSize);
            }
        }
    }
  delete p;
}

// Specific to VOIPApplication
bool VOIPApplication::RecordingFile(const string& rfn)
{
  recFileName = rfn;
  return true;
}

void VOIPApplication::UseSilenceSupression(double thresh)
{
  silenceSupression = true;
  ssThreshold = thresh;
}

// Private methods

void VOIPApplication::SendNextSample()
{
  if (!longBuf) longBuf  = new long[pktSize];
  if (!buf)     buf      = new char[pktSize + sizeof(sampleIndex)];
  Count_t actual = callFile->ReadSoundDataSingle(longBuf, pktSize);
  // Add the sampleIndex to the buffer
  Count_t* cp = (Count_t*)buf;
  *cp = sampleIndex;
  sampleIndex += actual;
  Count_t bufOffset = sizeof(sampleIndex);
  if (actual)
    { 
      bool supress = false;
      if (silenceSupression)
        {
          double intensity = AIFF::SoundIntensity(longBuf, actual);
          supress = (intensity < ssThreshold); // Supress if too low
          if (supress) DEBUG0((cout << "Supressing pkt" << endl));
          if (verbose)
            {
              if (supress) cout << "supressing pkt " << (sampleIndex - actual)
                                << " intensity " << intensity << endl;
              else         cout << "sending    pkt " << (sampleIndex - actual)
                                << " intensity " << intensity << endl;
            }
        }
      if (!supress)
        { // Encode each sample using the specified coded
          switch(codec) {
            case CODEC_ULAW:
              for (Count_t i = 0; i < actual; ++i)
                {
                  buf[i + bufOffset] = ShortToULaw(longBuf[i]);
                }
              break;
            case CODEC_ALAW:
              for (Count_t i = 0; i < actual; ++i)
                {
                  buf[i + bufOffset] = ShortToALaw(longBuf[i]);
                }
              break;
            case CODEC_CUSTOM:
              if (codecCb)
                {
                  actual = codecCb(longBuf, &buf[bufOffset],
                                   pktSize, true);
                }
              else
                {
                  cout << "HuH? No VOIP custom codec callback?" << endl;
                }
              break;
            }
          l4Proto->Send(buf, actual + bufOffset);
        }
    }
  else
    { // End of call
      callFile->Close();
      // Need to schedule the call again after call duration.
    }
  // Schedule the next packet to send
  if (!pendingDataEvent)
    pendingDataEvent = new AppVOIPEvent(AppVOIPEvent::NEXT_SAMPLE);
  Time_t future = (double)pktSize / (double)sampleRate;
  Scheduler::Schedule(pendingDataEvent, future, this);
}

// Static methods
void VOIPApplication::SetCodec(Codec_t c)
{
  codec = c;
}

void VOIPApplication::SetCustomCodecCallback(CodecCallback_t cb)
{
  codecCb = cb;
  codec = CODEC_CUSTOM;
}

      
