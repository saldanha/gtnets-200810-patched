#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include "simulator.h"
#include "bluetus.h"
#include "node-blue.h"
#include "node-blue-impl.h"
#include "node.h"
#include "node-impl.h"
#include "node-real.h"
#include "ratetimeparse.h"
#include "baseband.h"
#include "l2cap.h"
#include "lmp.h"
#include "bnep.h"
#include "application-blue.h"

int main() {
	Simulator s;
	
	s.HasMobility(true);

	BlueNode *pBlueNode = new BlueNode[2];
	sleep(2);
	
	pBlueNode[0].SetLocation(0.0,0.0);
	pBlueNode[1].SetLocation(3.0,5.0);

	pBlueNode[0].Shape(Node::CIRCLE);

	printf("pBlueNode0.pBaseband = %x\n", static_cast<NodeBlueImpl*>(pBlueNode[0].pImpl)->pBaseband);
	printf("pBlueNode1.pBaseband = %x\n", static_cast<NodeBlueImpl*>(pBlueNode[1].pImpl)->pBaseband);
	
	BdAddr addr0 = pBlueNode[0].GetBdAddr();
	BdAddr addr1 = pBlueNode[1].GetBdAddr();

	printf("Node 0 addr =0x%x%x%x\n",addr0.usAddr[2], addr0.usAddr[1], addr0.usAddr[0]);
	printf("Node 1 addr =0x%x%x%x\n",addr1.usAddr[2], addr1.usAddr[1], addr1.usAddr[0]);

/*
	s.StartAnimation(0, true);
	s.AnimateBasebandTx(true);
	s.AnimationUpdateInterval(Time("10us"));
*/	
	pBlueNode[0].Start(MASTER, 1.0);
	pBlueNode[1].Start(SLAVE, 1.2);

    Time_t duration = Time("1s");
    Mult_t dutyCycle = 0.05;  // On half, off half (on average)
	Random::GlobalSeed(1, 1, 1, 1, 1, 1);
	//pLMP0->SetHold(11, 20, 1580, Exponential(duration * dutyCycle));
	//pLMP0->SetSniff(11, 1600, 16, 2);
	BlueApplication BlueApp(&pBlueNode[0], &pBlueNode[1], 
							Exponential(duration * dutyCycle), // On/Off period
							Exponential(duration * (1 - dutyCycle)), // On/Off period
							static_cast<NodeBlueImpl*>(pBlueNode[0].pImpl)->pBNEP, 100000);
	BlueApp.Start(10.0);
	BlueApp.Stop(110.0);
	s.StopAt(110.5);
	s.Run();
		    	
	delete []pBlueNode;
	return 0;
}



