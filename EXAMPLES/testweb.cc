// Test the Web Browsing Model
// George F. Riley.  Georgia Tech, Fall 2002

#include "simulator.h"
#include "scheduler.h"
#include "dumbbell.h"
#include "linkp2p.h"
#include "ratetimeparse.h"
#include "tcp-tahoe.h"
#include "application-tcpserver.h"
#include "application-webbrowser.h"
#include "histogram.h"
#include "globalstats.h"
#include "droptail.h"
#include "ipv4.h"
#include "validation.h"  

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;

  Queue::Default(DropTail()); // Set default drop tail
  Trace* gs = Trace::Instance();
  gs->IPDotted(true);
  gs->Open("testweb.txt");
  TCP::LogFlagsText(true);    // Log flags in text mode
  TCP::Default().SetTrace(Trace::ENABLED); // Enable trace on default tcp
  // Enable all l3 tracing
  //IPV4::Instance()->SetTrace(Trace::ENABLED);

  Count_t leafCount = 10;
  if (argc > 1) leafCount = atol(argv[1]);
  Count_t brCount = 10;
  if (argc > 2) leafCount = atol(argv[2]);
  Linkp2p l;
  l.Bandwidth(Rate("100Mb"));
  l.Delay(Time("1ms"));
  Dumbbell b(leafCount, leafCount, 0.1,
             IPAddr("192.168.1.0"), IPAddr("192.168.2.0"), l);
  b.LeftLink()->Delay(Time("10ms"));
  b.RightLink()->Delay(Time("10ms"));
  for (Count_t i = 0; i < b.LeftCount(); ++i)
    {
      cout << "Left  node " << i 
           << " ipaddr " << (string)IPAddr(b.Left(i)->GetIPAddr()) << endl;
      // For debugging set small limit
      Queue* q = b.LeftQueue(i);
      q->SetLimitPkts(2);
      
    }
  IPAddrVec_t servers; // IP Addresses of possible servers
  for (Count_t i = 0; i < b.RightCount(); ++i)
    {
      IPAddr serverIP = b.Right(i)->GetIPAddr();
      cout << "Right node " << i 
           << " ipaddr " << (string)serverIP << endl;
      servers.push_back(serverIP);
      // Add a tcp server on right side
      TCPServer* app = new TCPServer(TCPTahoe(b.Right(i)));
      app->Bind(HTTP_PORT);
      app->CloseOnEmpty();
      app->DeleteOnComplete();
      L4Protocol* p = app->GetL4();
      p->SetTrace(Trace::ENABLED);
    }
  Histogram* h = new Histogram(2.0, 20);  // 0-2.0 seconds, 20 bins
  for (Count_t i = 0; i < b.LeftCount(); ++i)
    {
      for (Count_t j = 0; j < brCount; ++j)
        {
          WebBrowser* wb = (WebBrowser*)b.Left(i)->AddApplication(
              WebBrowser(servers, Uniform(0, servers.size())));
          wb->ThinkTimeBound(60.0);
          wb->SetStatistics(h);
          wb->Start(0);
        }
    }
  
  s.Progress(10);
  s.StopAt(5000);
  s.Run();
  ofstream cdf("testweb.cdf");
  h->CDF(cdf, "# CDF of Web Object Response Time");
  cdf.close();
#define VERBOSE
#ifdef VERBOSE
  Stats::Print(); // Print the statistics
  cout << "Memory usage after run " 
       << s.ReportMemoryUsageMB() << "MB" << endl;
  cout << "Total events processed "
       << Scheduler::Instance()->TotalEventsProcessed() << endl;
  cout << "Setup time " << s.SetupTime() << endl;
  cout << "Route time " << s.RouteTime() << endl;
  cout << "Run time "   << s.RunTime() << endl;
#endif
}
