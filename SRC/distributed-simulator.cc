// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: distributed-simulator.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - Distributed Simulator Object
// George F. Riley, Georgia Tech.  Spring 2002

#include <iostream>

#include "debug.h"
#include "distributed-simulator.h"
#include "link-rti.h"
#include "interface-interconnect-remote.h"
#include "scheduler.h"
#include "qtwindow.h"
#include "backplane.h"
#include "tcp.h"
#include "udp.h"
#include "ipv4.h"
#include "routing-nixvector.h"

#ifdef  HAVE_RTI
extern "C" {
#include "rticore.h"
#include "brti.h"
}
#endif

using namespace std;

SystemId_t DistributedSimulator::systemId = 0; // System id
bool       DistributedSimulator::granted = false;  // True if TimeAdvanceGrant
Time_t     DistributedSimulator::grantedTime = 0.0;// Time of last TAG
MGroupVec_t DistributedSimulator::rti_MGroup;      // RTI Multicast groups
BCastStringVec_t DistributedSimulator::rti_BCast;  // For RTI Bcast groups

DistributedSimulator::DistributedSimulator(SystemId_t sysId) : 
    Simulator(),
    usingNER(true)
{
#ifndef HAVE_RTI
  cout << "Distributed Simulator constructor called, but no RTI" << endl;
  cout << "This is is not correct (exiting)" << endl;
  exit(1);
#endif
#ifdef HAVE_RTI
  systemId = sysId;
  char* argv1[2] = {"gtnets", NULL };
  int   argc1 = 1;
  cout << "Initializing RTI" << endl;
  cerr << "Initializing RTI" << endl;
  RTI_Init(argc1, argv1);  // Initilize the RTI kit
  cout << "Done Initializing RTI" << endl;
  cerr << "Done Initializing RTI" << endl;
  if (!silent)
    {
      if (usingNER) cout << "Using NER Request Method" << endl;
      else cout << "Using TAR Request Method" << endl;
    }
#endif
}

DistributedSimulator::~DistributedSimulator()
{
}

void DistributedSimulator::Run()
{
#ifdef HAVE_RTI
  // Perform some RTI related initialization
  if (!silent) cout << "Using lookahead " << LinkRTI::minTxTime << endl;
  if (usingNER)
    RTI_SetLookAhead(LinkRTI::minTxTime);
  else
    RTI_SetLookAhead(0);  // for the TAR version (default)
  RTIKIT_Barrier();  // Wait for others to become ready also
      
  BCastStringVec_t::iterator v;
  for(v=rti_BCast.begin(); v != rti_BCast.end(); v++)
    {
      RTI_CreateClass((char*)((*v).bgroup).c_str());
      // Get the object class handle
      objClass = RTI_GetObjClassHandle((char*)((*v).bgroup).c_str());
      if (objClass == NULL)
        {
          cout << "Can't get objclasshandle for " << ((*v).bgroup).c_str()
               << endl;
          exit(0);
        }
    
      // Publish the class and register our interest
      RTI_PublishObjClass(objClass);
      if (supercomputerInterconnect)
        { // Use the supercomputer link names
          RemoteInterfaceInterconnect* pRic =
              (RemoteInterfaceInterconnect*)v->link;
          pRic->objInstance = RTI_RegisterObjInstance(objClass);
        }
      else
        {
          ((*v).link)->objInstance = RTI_RegisterObjInstance(objClass);
        }
    }
  
  RTIKIT_Barrier();  // Wait for others to become ready also
  
  // Subscribe to the RTI multicast groups
  MGroupVec_t::iterator u;
  for(u=rti_MGroup.begin(); u != rti_MGroup.end(); u++)
    {
      // Get the object class handle
      objClass = RTI_GetObjClassHandle((char*)((*u).group).c_str());
      if (objClass == NULL)
        {
          cout << "Can't get objclasshandle for " << ((*u).group).c_str()
               << endl;
          exit(0);
        }
      
	
      // Check if subscription is initialized, and init/subscribe if not
      if (!RTI_IsClassSubscriptionInitialized(objClass))
        { // Set up a context
          RTI_InitObjClassSubscription (objClass, WhereMessage, (*u).interFace);
          // Subscribe to the class
          RTI_SubscribeObjClassAttributes(objClass);
        }
    }        
  
  RTIKIT_Barrier();  // Wait for others to become ready also
#endif

  CommonRunInit();   // Common code prior to start of event loop
     
#ifdef HAVE_RTI
  while(!halted)
    {
      Event* e;
      e = Scheduler::PeekEvent();
      Time_t     requestTime = INFINITE_TIME;
      if (e) requestTime = e->Time();
      if (requestTime > grantedTime)
        { // Need to ask RTI if ok to advance
          granted = false;
          DEBUG(4,(cout << "Requesting adv to " << requestTime
                   << endl));
          if (usingNER)
            RTI_NextEventRequest(requestTime);
          else
            RTI_TimeAdvanceRequest(grantedTime + LinkRTI::minTxTime);
          DEBUG(4,(cout << "Ticking after TAR" << endl));
          while(!granted) Core_tick(); // Wait for approval
          DEBUG(4,(cout << "Ticked, grantedTime " << grantedTime << endl));
        }
      if (grantedTime == INFINITE_TIME) break;
      DEBUG(4,(cout << "Granted to " << grantedTime << endl));
      e = Scheduler::PeekEvent(); // Get fresh copy in case new message arrived
      if (e && e->Time() < simtime)
        cout << "HuH?  Time backing up in Simulator::run()" << endl;
      if (e && e->Time() <= grantedTime)
        {
          e = Scheduler::DeQueue();  // Ok to process, remove from queue
          if (e->handler)
            {
              simtime = e->Time();
              Scheduler::Instance()->totevp++;
              DEBUG(4,(cout << "Handling ev at time " << e->Time() << endl));
              e->handler->Handle(e, e->Time()); // Process the event
            }
          else
            {
              if (!silent)
                {
                  cout << "Skipping event " << e->uid
                       << "  at time " << e->Time()
                       << ", no handler " << endl;
                }
            }
          if (!usingNER) Core_tick(); // Process message deliveries
        }
#ifdef HAVE_QT
      if (animate) qtWin->ProcessEvents();
#endif
    }
  DEBUG0((cout << "Simulator::Run exit event loop" << endl));
  if (!Scheduler::Instance()->EventListSize() && !halted)
    if(!silent)cout << "Simulator::Run, event list exhausted" << endl;
  if (!silent) cout << " grantedTime " << grantedTime << endl;
  // Now keep trying to advance to INFINITE_TIME, to insure all others have exited
  while(grantedTime < INFINITE_TIME)
    {
      granted = false;
      RTI_NextEventRequest(INFINITE_TIME);
      while(!granted) Core_tick(); // Wait for approval
    }
  // And enter the final barrier
  RTIKIT_FinalBarrier();
  // Hack per Kalyan
  for (int x = 0; x < 100; ++x)
    {
      struct timeval tv;
      tv.tv_sec = 0;
      tv.tv_usec = 10000;
      FM_extract(~0);
      select(0, NULL, NULL, NULL, &tv); // 10ms delay
    }
  // End hack
#ifdef VERBOSE
  if (!silent)
    { 
      cout << "Total RTI updates "  << LinkRTI::nUpdates << endl;
      cout << "Total RTI reflects " << LinkRTI::nReflects << endl;
      cout << "Total RTI ignored " << LinkRTI::ignoredReflects << endl;
      TM_PrintStats(); // Print debug statistics
    }
#endif
#endif
  Stats::finalMemory = ReportMemoryUsage();
}

void DistributedSimulator::UseBackplane()
{
#ifdef HAVE_RTI
  // Initialize the backplane interface
  DSHandle_t dsimHandle = InitializeDSim(systemId);
  // Register the TCP and IP data items
  TCP::RegisterBackplane(dsimHandle);
  IPV4::RegisterBackplane(dsimHandle);
  // Notify the backplane that registration is complete
  RegistrationComplete(systemId, FM_numnodes);
#endif
}


#ifdef HAVE_RTI
// RTIKit requires this exact name
void TimeAdvanceGrant (TM_Time T)
{
 DistributedSimulator::grantedTime = T; // More efficient to use below
 //TM_Recent_LBTS(&Simulator::grantedTime); // Use most recent LBTS value
 DEBUG0((cout << "Granted to " << DistributedSimulator::grantedTime << endl));
 DistributedSimulator::granted = true;
}

// Two dummy functions needed by BRTI but not used here

extern "C" {
void MyHereProc (long MsgSize, struct MsgS *Msg) {}
void RequestRetraction(CoreRetractionHandle) { }
}
#endif

#ifdef HAVE_RTI
// Static "WhereMessage" procedure
char* DistributedSimulator::WhereMessage( // Advise where to store Rx message
      long         MsgSize,        // Size of RX Message
      void*        iface,          // Context info, contains interface pointer
      long         MsgType)        // Type specified by sender
{
  //rxIFace = (Interface*)iface;     // Save for use by ReflectAttributeValues
  DEBUG0((cout << "LinkRTI::WhereMessage, simtime " 
          << Simulator::Now() << endl));
  char* b = (char*)malloc(MsgSize + 32);
  memcpy(b, &iface, sizeof(Interface*));
  return (b+sizeof(Interface*));
}

void ReflectAttributeValues(
                   TM_Time T,
                   struct MsgS* pMsg,
                   long MsgSize,
                   long MsgType)
{
  char* b = (char*)pMsg;
  b -= sizeof(Interface*);
  Interface* iface;
  memcpy(&iface, b, sizeof(Interface*));
  unsigned int* pMyNodeId = (unsigned int*)&pMsg[1];
  if (*pMyNodeId != RTIKIT_nodeid)
    { // Ignore own messages
      LinkRTI::nReflects++;
      unsigned int* pMyDebugSize = (unsigned int*)&pMyNodeId[1];
      char* b = (char*)&pMyDebugSize[1];
#ifdef VERBOSE
      cout << "RAV" << endl;
      // debug, dump the data
      for (int k = 0; k < 16; ++k) cout << hex << pMyDebugSize[k+1] << endl;
#endif
      DEBUG0((cout << "LinkRTI::RAV, iface " << iface
              << " size " << MsgSize 
              << " timestamp " << pMsg->TimeStamp
              << " nodeid " << *pMyNodeId
              << " simtime " << Simulator::Now()
              << endl));

      Size_t sz = 0;
      Size_t bsz = MsgSize - sizeof(struct MsgS) - sizeof(unsigned int);
      char* nb = Serializable::GetSize(b, bsz, sz);
      Packet* p = new Packet(nb, sz);
      DEBUG0((cout << "Packet size is " << p->Size() << endl));
      // DEBUG
#undef VERBOSE_DEBUG1
#ifdef  VERBOSE_DEBUG1
      for (unsigned x = 0; x < p->top; ++x)
        {
          cout << "pdu " << x << " layer " << p->PeekPDU(x)->Layer() 
               << " ptr " << p->PeekPDU(x)
               << endl;
        }
      L2802_3Header* l2 = (L2802_3Header*)p->PeekPDU(0);
      LLCSNAPHeader* snap = (LLCSNAPHeader*)p->PeekPDU(1);
      cout << " snap eth " << Hex4(snap->snap_ethtype) << endl;
      // debug follows
      IPV4Header* iphdr = (IPV4Header*)p->PeekPDU(2); // Peek at ip hdr
      TCPHeader* tcp = (TCPHeader*)p->PeekPDU(3); // Get the TCP header
      cout << "RX " << iface
           << " sip " << (string)IPAddr(iphdr->src)
           << " sp " << tcp->sourcePort
           << " dip " << (string)IPAddr(iphdr->dst)
           << " dp " << tcp->destPort
           << " seq " << tcp->sequenceNumber 
           << " ack " << tcp->ackNumber
           << " lth " << tcp->dataLength
           << endl;
      if (p->Size() > 2000)
        {
          cout << "HuH?  Serialized packet bad size" << endl;
          free(pMsg);
          return;
        }
      if (p->Size() != *pMyDebugSize)
        {
          cout << "HuH?  Expected pkt size " << *pMyDebugSize
               << " got " << p->Size() << endl;
        }
      if (p->top > 10)
        {
          cout << "HuH?  Serialized packet, bad top" << endl;
          free(b);
          return;
        }
#endif
      if (Simulator::instance->supercomputerInterconnect)
        {
          InterfaceInterconnect* i = (InterfaceInterconnect*)iface;
          // Schedule the packet receive event at the destination interface
          LinkEvent* evRx = new LinkEvent(LinkEvent::PACKET_RX, p);
          i->ScheduleRx(evRx,pMsg->TimeStamp - Simulator::Now());
        }
      else
        {
          Interface* i = iface;
          Node* n = i->GetNode();                 // Associated node
          IPV4Header* iphdr = (IPV4Header*)p->PeekPDU(2);
          if (!iphdr)
            {
              cout << "HuH?  Serialized packet w/ no IP Header" << endl;
              free(b);
              return;
            }
      
          if (!p->nixVec && !n->LocalRoute(iphdr->dst))
            { // No nix vector and not local route, see if using NV routing
              DEBUG(3,(cout << "LINKRTI::RAV, node " << n
                       << " id " << n->Id()
                       << " ip " << (string)IPAddr(n->GetIPAddr())
                       << endl));
              RoutingNixVector* pnv = n->GetNixRouting();
              if (pnv)
                { // Using nixvector routing, get (or create) an NV to target
                  // Get the ipv4 header...we could use a better way to do this
                  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU(2);
                  // Add the nv to the packet
                  p->nixVec = pnv->GetNixVector(n, iphdr->dst);
                  DEBUG(3,(cout << "LinkRTI::RAV Adding new Nv to packet, dst "
                           << (string)iphdr->dst 
                           << " nv " << p->nixVec << endl));
                  DEBUG(3,(p->nixVec->Dump()));
                  DEBUG(3,(cout << "nv sz " << p->nixVec->size
                           << " nv used " << p->nixVec->used << endl));
                }
            }
          // Schedule the packet receive event at the destination interface
          LinkEvent* evRx = new LinkEvent(LinkEvent::PACKET_RX, p);
          i->ScheduleRx(evRx,pMsg->TimeStamp - Simulator::Now());
        }
    }
  else
    {
      DEBUG(2,(cout << "ignoring self message" << endl));
      LinkRTI::ignoredReflects++;
    }
  free(b);
}


#endif

