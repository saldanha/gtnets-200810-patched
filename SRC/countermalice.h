/*

* GTNetS provides a portable C++ class library for network simulations
*
* Copyright (C) 2003 George F. Riley
*
* No guarantees or warranties or anything are provided or implied in any way
* whatsoever.  Use this program at your own risk.  Permission to use this
* program for any purpose is given, as long as the copyright is kept intact.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

* Permission is hereby granted for any non-commercial use of this code

*/

// Georgia Tech Network Simulator - CounterMalice class
// Mohamed Abd Elhafez.  Georgia Tech, Summer 2004

// Implements the counter malice algorithm for worm containment

#ifndef __counter_malice_h__
#define __counter_malice_h__

#include <queue>

#include "common-defs.h"
#include "pdu.h"
#include "ipaddr.h"
#include "ipv4.h"
#include "node.h"
#include "tcp.h"
#include "virusthrottle.h"

#define ALLOW true
#define DISALLOW false
#define TCP_PROTOCOL 6
#define UDP_PROTOCOL 17

class Connection{
 public:
  Connection(): blocked(false), icmpcount(0), timeout(0.0){}

  Connection(bool b, Count_t c, Time_t t): blocked(b), icmpcount(c), timeout(t) {}
 public:
  bool       blocked;
  Count_t    icmpcount;
  Time_t     timeout;
};

typedef std::map<IPAddr_t, DelayQueue_t> Queues_t;
typedef std::map<IPAddr_t, WorkingSet_t> WorkingSets_t;
typedef std::pair<bool, Count_t> ConnPair_t;
typedef std::map<IPAddr_t, Connection> Connections_t;
typedef std::vector<Interface*> IFVec_t;


//Doc:ClassXRef
class CounterMalice : public WormContainment, public Handler{
//Doc:Class The class {\tt CounterMalice} implements the counter malice algorithm
//Doc:Class It derives 	from the class {\tt WormContainment}. 
	
public:
  //Doc:Method
  CounterMalice();
  //Doc:Desc Default constructor

  // Handler
  virtual void Handle(Event*, Time_t);

  //Doc:Method
  void ProcessOutPacket(Packet*, IPAddr_t, int, Interface*);
  //Doc:Desc Process the given packet for TCP SYN packets or UDP packets
  
 //Doc:Method
  void ProcessInPacket(Packet*, Interface*);
  //Doc:Desc Process the given packet for TCP SYN packets or UDP packets
  

  bool InWorkingSet(IPV4Header*);
 
  void Delay(IPV4Header*, Packet*, Interface*);

  void SetOutIfs(IFVec_t);
   

  void SetOutIf(Interface*);
  
  bool IsOutIface(Interface*);

  static void SetWorkingSetSize(int);
     
  static void SetDelayQueueLimit(int);
  
  static void SetTimeout(int);
   
  static Count_t  TotalBlocked() {return blocked;}
  static Count_t  FalseDetected() {return falsedetection;}

 private:
  Queues_t delayqueues;
  WorkingSets_t workingsets;
  Connections_t conns;
  bool scheduled;
  IFVec_t    outIfs;

 public:
  static int delayqueuelimit;
  static int workingsetsize;
  static int blocked;
  static int falsedetection;
  static int timeout;

};

#endif

