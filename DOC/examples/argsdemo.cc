// Demonstrate use of the GTNetS argument processing
// George F. Riley, Georgia Tech, Summer 2003

#include <iostream>
#include "args.h"

unsigned long pktSize;
double        rate;

// Two command line parameters are defined in this case,
// "size" and "rate".  The default size is 1000 and the
// default rate is 1e6.  Variable pktSize will default
// to 1000, but can be overridden with a command line
// parameter, as follows:
//   ./myGTNetS size=1500 rate=1e7 static

int main()
{
 Arg("size", pktSize, 1000);
 Arg("rate", rate,    1e6);

  Arg::ProcessArgs();
  if (Arg::Specified("static")) {
    std::cout << "static is specified" << std::endl;
  }
  std::cout << "pktSize is " << pktSize << std::endl;
  std::cout << "rate is " << rate << std::endl;
}

