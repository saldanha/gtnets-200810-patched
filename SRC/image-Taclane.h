// Generated automatically by make-gtimage from IMAGES/Taclane.png
// DO NOT EDIT

#include "image.h"

class TaclaneImage : public Image {
public:
  TaclaneImage(){}
  const char* Data() const { return data;}
  int Size() const { return size;}
  operator const char*() { return data;}
  static const char* data;
  static int size;
};
