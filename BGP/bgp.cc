/****************************************************************************/
/*  File:            bgp.cc                                                 */
/*  Author:          Sunitha Beeram                                         */
/*  Email:           sunithab@ece.gatech.edu                                */
/*  Documentation:   TBD                                                    */
/*                                                                          */
/*  Version:         1.0beta                                                */
/*                                                                          */
/*  Copyright (c) 2005 Georgia Institute of Technology                      */
/*  This program is free software; you can redistribute it and/or           */
/*  modify it under the terms of the GNU General Public License             */
/*  as published by the Free Software Foundation; either version 2          */
/*  of the License, or (at your option) any later version.                  */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful,         */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*  GNU General Public License for more details.                            */
/*                                                                          */
/****************************************************************************/



#include <signal.h>

#include "bgp.h"
#include "memusage.h"
#include "../SRC/tcp-tahoe.h"
#include "../SRC/tcp.h"
#include "../SRC/interface.h"
#include "../SRC/scheduler.h"

char* BGP::progname = "BGP++";
int BGP::cmdvec_init =1 ;
time_t BGP::start_time=0;
double BGP::uniform_max = 0;
double BGP::uniform_min = 0;
int BGP::rnd = 0;

bool BGP::enter_bgp_construct = false;
bool BGP::enter_bgp_main = false;
int BGP::instance_cnt = 0;

bool BGP::dont_reuse = false;
bool BGP::default_randomize = true;

IpBgp_t BGP::IpBgp;

DEFUNST (ip_community_list,
	 ip_community_list_cmd,
	 "ip community-list WORD (deny|permit) .AA:NN",
	 IP_STR
	 "Add a community list entry\n"
	 "Community list name\n"
	 "Specify community to reject\n"
	 "Specify community to accept\n"
	 "Community number in aa:nn format or local-AS|no-advertise|no-export\n")
    
    
DEFUNST (no_ip_community_list,
         no_ip_community_list_cmd,
         "no ip community-list WORD (deny|permit) .AA:NN",
         NO_STR
         IP_STR
         "Add a community list entry\n"
         "Community list name\n"
         "Specify community to reject\n"
         "Specify community to accept\n"
         "Community number in aa:nn format or local-AS|no-advertise|no-export\n")
    

DEFUNST (no_ip_community_list_all,
         no_ip_community_list_all_cmd,
         "no ip community-list WORD",
         NO_STR
         IP_STR
         "Add a community list entry\n"
         "Community list name\n")
    
    
  DEFUNST (debug_bgp_fsm,
	   debug_bgp_fsm_cmd,
	   "debug bgp fsm",
	   DEBUG_STR
	   BGP_STR
	   "BGP Finite State Machine\n")



  DEFUNST (no_debug_bgp_fsm,
	   no_debug_bgp_fsm_cmd,
	   "no debug bgp fsm",
	   NO_STR
	   DEBUG_STR
	   BGP_STR
	   "Finite State Machine\n")


  DEFUNST (debug_bgp_events,
	   debug_bgp_events_cmd,
	   "debug bgp events",
	   DEBUG_STR
	   BGP_STR
	   "BGP events\n")


  DEFUNST (no_debug_bgp_events,
	   no_debug_bgp_events_cmd,
	   "no debug bgp events",
	   NO_STR
	   DEBUG_STR
	   BGP_STR
	   "BGP events\n")


  DEFUNST (debug_bgp_filter,
	   debug_bgp_filter_cmd,
	   "debug bgp filters",
	   DEBUG_STR
	   BGP_STR
	   "BGP filters\n")


  DEFUNST (no_debug_bgp_filter,
	   no_debug_bgp_filter_cmd,
	   "no debug bgp filters",
	   NO_STR
	   DEBUG_STR
	   BGP_STR
	   "BGP filters\n")

  DEFUNST (debug_bgp_keepalive,
	   debug_bgp_keepalive_cmd,
	   "debug bgp keepalives",
	   DEBUG_STR
	   BGP_STR
	   "BGP keepalives\n")


  DEFUNST (no_debug_bgp_keepalive,
	   no_debug_bgp_keepalive_cmd,
	   "no debug bgp keepalives",
	   NO_STR
	   DEBUG_STR
	   BGP_STR
	   "BGP keepalives\n")


  DEFUNST (debug_bgp_update,
	   debug_bgp_update_cmd,
	   "debug bgp updates",
	   DEBUG_STR
	   BGP_STR
	   "BGP updates\n")

  DEFUNST (debug_bgp_update_direct,
	   debug_bgp_update_direct_cmd,
	   "debug bgp updates (in|out)",
	   DEBUG_STR
	   BGP_STR
	   "BGP updates\n"
	   "Inbound updates\n"
	   "Outbound updates\n")


  DEFUNST (no_debug_bgp_update,
	   no_debug_bgp_update_cmd,
	   "no debug bgp updates",
	   NO_STR
	   DEBUG_STR
	   BGP_STR
	   "BGP updates\n")

  DEFUNST (debug_bgp_normal,
	   debug_bgp_normal_cmd,
	   "debug bgp",
	   DEBUG_STR
	   BGP_STR)


  DEFUNST (no_debug_bgp_normal,
	   no_debug_bgp_normal_cmd,
	   "no debug bgp",
	   NO_STR
	   DEBUG_STR
	   BGP_STR)


  DEFUNST (no_debug_bgp_all,
	   no_debug_bgp_all_cmd,
	   "no debug all bgp",
	   NO_STR
	   DEBUG_STR
	   "Enable all debugging\n"
	   BGP_STR)


  DEFUNST (show_debugging_bgp,
	   show_debugging_bgp_cmd,
	   "show debugging bgp",
	   SHOW_STR
	   DEBUG_STR
	   BGP_STR)


  DEFUNST (dump_bgp_all,
	   dump_bgp_all_cmd,
	   "dump bgp all PATH",
	   "Dump packet\n"
	   "BGP packet dump\n"
	   "Dump all BGP packets\n"
	   "Output filename\n")


  DEFUNST (dump_bgp_all_interval,
	   dump_bgp_all_interval_cmd,
	   "dump bgp all PATH INTERVAL",
	   "Dump packet\n"
	   "BGP packet dump\n"
	   "Dump all BGP packets\n"
	   "Output filename\n"
	   "Interval of output\n")


  DEFUNST (no_dump_bgp_all,
	   no_dump_bgp_all_cmd,
	   "no dump bgp all [PATH] [INTERVAL]",
	   NO_STR
	   "Dump packet\n"
	   "BGP packet dump\n"
	   "Dump all BGP packets\n")


  DEFUNST (dump_bgp_updates,
	   dump_bgp_updates_cmd,
	   "dump bgp updates PATH",
	   "Dump packet\n"
	   "BGP packet dump\n"
	   "Dump BGP updates only\n"
	   "Output filename\n")


  DEFUNST (dump_bgp_updates_interval,
	   dump_bgp_updates_interval_cmd,
	   "dump bgp updates PATH INTERVAL",
	   "Dump packet\n"
	   "BGP packet dump\n"
	   "Dump BGP updates only\n"
	   "Output filename\n"
	   "Interval of output\n")


  DEFUNST (no_dump_bgp_updates,
	   no_dump_bgp_updates_cmd,
	   "no dump bgp updates [PATH] [INTERVAL]",
	   NO_STR
	   "Dump packet\n"
	   "BGP packet dump\n"
	   "Dump BGP updates only\n")


  DEFUNST (dump_bgp_routes_interval,
	   dump_bgp_routes_interval_cmd,
	   "dump bgp routes-mrt PATH INTERVAL",
	   "Dump packet\n"
	   "BGP packet dump\n"
	   "Dump whole BGP routing table\n"
	   "Output filename\n"
	   "Interval of output\n")

DEFUNST (dump_bgp_routes,
       dump_bgp_routes_cmd,
       "dump bgp routes-mrt PATH",
       "Dump packet\n"
       "BGP packet dump\n"
       "Dump whole BGP routing table\n"
       "Output filename\n")

  DEFUNST (no_dump_bgp_routes,
	   no_dump_bgp_routes_cmd,
	   "no dump bgp routes-mrt [PATH] [INTERVAL]",
	   NO_STR
	   "Dump packet\n"
	   "BGP packet dump\n"
	   "Dump whole BGP routing table\n")


  DEFUNST (ip_as_path, 
	   ip_as_path_cmd,
	   "ip as-path access-list WORD (deny|permit) .LINE",
	   IP_STR
	   "BGP autonomous system path filter\n"
	   "Specify an access list name\n"
	   "Regular expression access list name\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "A regular-expression to match the BGP AS paths\n")


  DEFUNST (no_ip_as_path,
	   no_ip_as_path_cmd,
	   "no ip as-path access-list WORD (deny|permit) .LINE",
	   NO_STR
	   IP_STR
	   "BGP autonomous system path filter\n"
	   "Specify an access list name\n"
	   "Regular expression access list name\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "A regular-expression to match the BGP AS paths\n")

      
  DEFUNST (no_ip_as_path_all,
	   no_ip_as_path_all_cmd,
	   "no ip as-path access-list WORD",
	   NO_STR
	   IP_STR
	   "BGP autonomous system path filter\n"
	   "Specify an access list name\n"
	   "Regular expression access list name\n")

  DEFUNST (bgp_network,
	   bgp_network_cmd,
	   "network A.B.C.D/M",
	   "Specify a network to announce via BGP\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n")


  DEFUNST (bgp_network_mask,
	   bgp_network_mask_cmd,
	   "network A.B.C.D mask A.B.C.D",
	   "Specify a network to announce via BGP\n"
	   "Network number\n"
	   "Network mask\n"
	   "Network mask\n")


  DEFUNST (no_bgp_network,
	   no_bgp_network_cmd,
	   "no network A.B.C.D/M",
	   NO_STR
	   "Specify a network to announce via BGP\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n")


  DEFUNST (no_bgp_network_mask,
	   no_bgp_network_mask_cmd,
	   "no network A.B.C.D mask A.B.C.D",
	   NO_STR
	   "Specify a network to announce via BGP\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n")



  DEFUNST (aggregate_address,
	   aggregate_address_cmd,
	   "aggregate-address A.B.C.D/M",
	   "Configure BGP aggregate entries\n"
	   "Aggregate prefix\n")

  DEFUNST (aggregate_address_mask,
	   aggregate_address_mask_cmd,
	   "aggregate-address A.B.C.D A.B.C.D",
	   "Configure BGP aggregate entries\n"
	   "Aggregate address\n"
	   "Aggregate mask\n")


  DEFUNST (aggregate_address_summary_only,
	   aggregate_address_summary_only_cmd,
	   "aggregate-address A.B.C.D/M summary-only",
	   "Configure BGP aggregate entries\n"
	   "Aggregate prefix\n"
	   "Filter more specific routes from updates\n")


  DEFUNST (aggregate_address_mask_summary_only,
	   aggregate_address_mask_summary_only_cmd,
	   "aggregate-address A.B.C.D A.B.C.D summary-only",
	   "Configure BGP aggregate entries\n"
	   "Aggregate address\n"
	   "Aggregate mask\n"
	   "Filter more specific routes from updates\n")


  DEFUNST (aggregate_address_as_set,
	   aggregate_address_as_set_cmd,
	   "aggregate-address A.B.C.D/M as-set",
	   "Configure BGP aggregate entries\n"
	   "Aggregate prefix\n"
	   "Generate AS set path information\n")


  DEFUNST (aggregate_address_mask_as_set,
	   aggregate_address_mask_as_set_cmd,
	   "aggregate-address A.B.C.D A.B.C.D as-set",
	   "Configure BGP aggregate entries\n"
	   "Aggregate address\n"
	   "Aggregate mask\n"
	   "Generate AS set path information\n")


  DEFUNST (aggregate_address_as_set_summary,
	   aggregate_address_as_set_summary_cmd,
	   "aggregate-address A.B.C.D/M as-set summary-only",
	   "Configure BGP aggregate entries\n"
	   "Aggregate prefix\n"
	   "Generate AS set path information\n"
	   "Filter more specific routes from updates\n")


  DEFUNST (aggregate_address_mask_as_set_summary,
	   aggregate_address_mask_as_set_summary_cmd,
	   "aggregate-address A.B.C.D A.B.C.D as-set summary-only",
	   "Configure BGP aggregate entries\n"
	   "Aggregate address\n"
	   "Aggregate mask\n"
	   "Generate AS set path information\n"
	   "Filter more specific routes from updates\n")


  DEFUNST (no_aggregate_address,
	   no_aggregate_address_cmd,
	   "no aggregate-address A.B.C.D/M",
	   NO_STR
	   "Configure BGP aggregate entries\n"
	   "Aggregate prefix\n")


  DEFUNST (no_aggregate_address_mask,
	   no_aggregate_address_mask_cmd,
	   "no aggregate-address A.B.C.D A.B.C.D",
	   NO_STR
	   "Configure BGP aggregate entries\n"
	   "Aggregate address\n"
	   "Aggregate mask\n")

  DEFUNST (show_ip_bgp,
	   show_ip_bgp_cmd,
	   "show ip bgp",
	   SHOW_STR
	   IP_STR
	   BGP_STR)


  DEFUNST (show_ip_bgp_route,
	   show_ip_bgp_route_cmd,
	   "show ip bgp A.B.C.D",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Network in the BGP routing table to display\n")


  DEFUNST (show_ip_bgp_prefix,
	   show_ip_bgp_prefix_cmd,
	   "show ip bgp A.B.C.D/M",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n")


  DEFUNST (show_ip_bgp_regexp, 
	   show_ip_bgp_regexp_cmd,
	   "show ip bgp regexp .LINE",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Display routes matching the AS path regular expression\n"
	   "A regular-expression to match the BGP AS paths\n")


  DEFUNST (show_ip_bgp_prefix_list, 
	   show_ip_bgp_prefix_list_cmd,
	   "show ip bgp prefix-list WORD",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Display routes matching the prefix-list\n"
	   "IP prefix-list name\n")


  DEFUNST (show_ip_bgp_filter_list, 
	   show_ip_bgp_filter_list_cmd,
	   "show ip bgp filter-list WORD",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Display routes conforming to the filter-list\n"
	   "Regular expression access list name\n")


  DEFUNST (show_ip_bgp_cidr_only,
	   show_ip_bgp_cidr_only_cmd,
	   "show ip bgp cidr-only",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Display only routes with non-natural netmasks\n")


  DEFUNST (show_ip_bgp_community_all,
	   show_ip_bgp_community_all_cmd,
	   "show ip bgp community",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Display routes matching the communities\n")


  DEFUNST (show_ip_bgp_community,
	   show_ip_bgp_community_cmd,
	   "show ip bgp community (AA:NN|local-AS|no-advertise|no-export)",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Display routes matching the communities\n"
	   "community number\n"
	   "Do not send outside local AS (well-known community)\n"
	   "Do not advertise to any peer (well-known community)\n"
	   "Do not export to next AS (well-known community)\n")


  DEFUNST (show_ip_bgp_community_exact,
	   show_ip_bgp_community_exact_cmd,
	   "show ip bgp community (AA:NN|local-AS|no-advertise|no-export) exact-match",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Display routes matching the communities\n"
	   "community number\n"
	   "Do not send outside local AS (well-known community)\n"
	   "Do not advertise to any peer (well-known community)\n"
	   "Do not export to next AS (well-known community)\n"
	   "Exact match of the communities")


  DEFUNST (show_ip_bgp_community_list,
	   show_ip_bgp_community_list_cmd,
	   "show ip bgp community-list WORD",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Display routes matching the community-list\n"
	   "community-list name\n")

  DEFUNST (show_ip_bgp_community_list_exact,
	   show_ip_bgp_community_list_exact_cmd,
	   "show ip bgp community-list WORD exact-match",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Display routes matching the community-list\n"
	   "community-list name\n"
	   "Exact match of the communities\n")

  DEFUNST (show_ip_bgp_prefix_longer,
	   show_ip_bgp_prefix_longer_cmd,
	   "show ip bgp A.B.C.D/M longer-prefixes",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Display route and more specific routes\n")


  DEFUNST (show_ip_bgp_neighbor_advertised_route,
	   show_ip_bgp_neighbor_advertised_route_cmd,
	   "show ip bgp neighbors (A.B.C.D|X:X::X:X) advertised-routes",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Detailed information on TCP and BGP neighbor connections\n"
	   "Neighbor to display information about\n"
	   "Neighbor to display information about\n"
	   "Display the routes advertised to a BGP neighbor\n")


  DEFUNST (show_ip_bgp_neighbor_received_routes,
	   show_ip_bgp_neighbor_received_routes_cmd,
	   "show ip bgp neighbors (A.B.C.D|X:X::X:X) received-routes",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Detailed information on TCP and BGP neighbor connections\n"
	   "Neighbor to display information about\n"
	   "Neighbor to display information about\n"
	   "Display the received routes from neighbor\n")


  DEFUNST (show_ip_bgp_neighbor_routes,
	   show_ip_bgp_neighbor_routes_cmd,
	   "show ip bgp neighbors (A.B.C.D|X:X::X:X) routes",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Detailed information on TCP and BGP neighbor connections\n"
	   "Neighbor to display information about\n"
	   "Neighbor to display information about\n"
	   "Display routes learned from neighbor\n")



  DEFUNST (bgp_distance,
	   bgp_distance_cmd,
	   "distance bgp <1-255> <1-255> <1-255>",
	   "Define an administrative distance\n"
	   "BGP distance\n"
	   "Distance for routes external to the AS\n"
	   "Distance for routes internal to the AS\n"
	   "Distance for local routes\n")


  DEFUNST (no_bgp_distance,
	   no_bgp_distance_cmd,
	   "no distance bgp <1-255> <1-255> <1-255>",
	   NO_STR
	   "Define an administrative distance\n"
	   "BGP distance\n"
	   "Distance for routes external to the AS\n"
	   "Distance for routes internal to the AS\n"
	   "Distance for local routes\n")



  DEFUNST (bgp_distance_source,
	   bgp_distance_source_cmd,
	   "distance <1-255> A.B.C.D/M",
	   "Define an administrative distance\n"
	   "Administrative distance\n"
	   "IP source prefix\n")



  DEFUNST (no_bgp_distance_source,
	   no_bgp_distance_source_cmd,
	   "no distance <1-255> A.B.C.D/M",
	   NO_STR
	   "Define an administrative distance\n"
	   "Administrative distance\n"
	   "IP source prefix\n")

  DEFUNST (bgp_distance_source_access_list,
	   bgp_distance_source_access_list_cmd,
	   "distance <1-255> A.B.C.D/M WORD",
	   "Define an administrative distance\n"
	   "Administrative distance\n"
	   "IP source prefix\n"
	   "Access list name\n")

  DEFUNST (no_bgp_distance_source_access_list,
	   no_bgp_distance_source_access_list_cmd,
	   "no distance <1-255> A.B.C.D/M WORD",
	   NO_STR
	   "Define an administrative distance\n"
	   "Administrative distance\n"
	   "IP source prefix\n"
	   "Access list name\n")


  DEFUNST (bgp_damp_set,
	   bgp_damp_set_cmd,
	   "bgp dampening <1-45> <1-20000> <1-20000> <1-255>",
	   "BGP Specific commands\n"
	   "Enable route-flap dampening\n"
	   "Half-lifedetime for the penalty\n"
	   "Value to start reusing a route\n"
	   "Value to start suppressing a route\n"
	   "Maximum duration to suppress a stable route\n")


  DEFUNST (bgp_damp_unset,
	   bgp_damp_unset_cmd,
	   "no bgp dampening",
	   NO_STR
	   "BGP Specific commands\n"
	   "Enable route-flap dampening\n")


  DEFUNST (match_ip_address, 
	   match_ip_address_cmd,
	   "match ip address WORD",
	   MATCH_STR
	   IP_STR
	   "Match address of route\n"
	   "IP access-list name\n")


  DEFUNST (no_match_ip_address, 
	   no_match_ip_address_cmd,
	   "no match ip address",
	   NO_STR
	   MATCH_STR
	   IP_STR
	   "Match address of route\n")


  DEFUNST (match_ip_next_hop, 
	   match_ip_next_hop_cmd,
	   "match ip next-hop WORD",
	   MATCH_STR
	   IP_STR
	   "Match next-hop address of route\n"
	   "IP access-list name\n")


  DEFUNST (no_match_ip_next_hop,
	   no_match_ip_next_hop_cmd,
	   "no match ip next-hop",
	   NO_STR
	   MATCH_STR
	   IP_STR
	   "Match next-hop address of route\n")


  DEFUNST (match_ip_address_prefix_list, 
	   match_ip_address_prefix_list_cmd,
	   "match ip address prefix-list WORD",
	   MATCH_STR
	   IP_STR
	   "Match address of route\n"
	   "Match entries of prefix-lists\n"
	   "IP prefix-list name\n")



  DEFUNST (no_match_ip_address_prefix_list,
	   no_match_ip_address_prefix_list_cmd,
	   "no match ip address prefix-list",
	   NO_STR
	   MATCH_STR
	   IP_STR
	   "Match address of route\n"
	   "Match entries of prefix-lists\n")


  DEFUNST (match_ip_next_hop_prefix_list, 
	   match_ip_next_hop_prefix_list_cmd,
	   "match ip next-hop prefix-list WORD",
	   MATCH_STR
	   IP_STR
	   "Match next-hop address of route\n"
	   "Match entries of prefix-lists\n"
	   "IP prefix-list name\n")


  DEFUNST (no_match_ip_next_hop_prefix_list,
	   no_match_ip_next_hop_prefix_list_cmd,
	   "no match ip next-hop prefix-list",
	   NO_STR
	   MATCH_STR
	   IP_STR
	   "Match next-hop address of route\n"
	   "Match entries of prefix-lists\n")


  DEFUNST (match_metric, 
	   match_metric_cmd,
	   "match metric <0-4294967295>",
	   MATCH_STR
	   "Match metric of route\n"
	   "Metric value\n")


  DEFUNST (no_match_metric,
	   no_match_metric_cmd,
	   "no match metric",
	   NO_STR
	   MATCH_STR
	   "Match metric of route\n")


  DEFUNST (match_community, 
	   match_community_cmd,
	   "match community WORD",
	   MATCH_STR
	   "Match BGP community list\n"
	   "Community-list name (not community value itself)\n")

  DEFUNST (no_match_community,
	   no_match_community_cmd,
	   "no match community",
	   NO_STR
	   MATCH_STR
	   "Match BGP community list\n")


  DEFUNST (match_aspath,
	   match_aspath_cmd,
	   "match as-path WORD",
	   MATCH_STR
	   "Match BGP AS path list\n"
	   "AS path access-list name\n")


  DEFUNST (no_match_aspath,
	   no_match_aspath_cmd,
	   "no match as-path",
	   NO_STR
	   MATCH_STR
	   "Match BGP AS path list\n")

  DEFUNST (set_ip_nexthop,
	   set_ip_nexthop_cmd,
	   "set ip next-hop A.B.C.D",
	   SET_STR
	   IP_STR
	   "Next hop address\n"
	   "IP address of next hop\n")


  DEFUNST (no_set_ip_nexthop,
	   no_set_ip_nexthop_cmd,
	   "no set ip next-hop",
	   NO_STR
	   SET_STR
	   IP_STR
	   "Next hop address\n")


  DEFUNST (set_metric,
	   set_metric_cmd,
	   "set metric (<0-4294967295>|<+/-metric>)",
	   SET_STR
	   "Metric value for destination routing protocol\n"
	   "Metric value\n"
	   "Add or subtract metric\n")


  DEFUNST (no_set_metric,
	   no_set_metric_cmd,
	   "no set metric",
	   NO_STR
	   SET_STR
	   "Metric value for destination routing protocol\n")


  DEFUNST (set_local_pref,
	   set_local_pref_cmd,
	   "set local-preference <0-4294967295>",
	   SET_STR
	   "BGP local preference path attribute\n"
	   "Preference value\n")


  DEFUNST (no_set_local_pref,
	   no_set_local_pref_cmd,
	   "no set local-preference",
	   NO_STR
	   SET_STR
	   "BGP local preference path attribute\n")


  DEFUNST (set_weight,
	   set_weight_cmd,
	   "set weight <0-4294967295>",
	   SET_STR
	   "BGP weight for routing table\n"
	   "Weight value\n")


  DEFUNST (no_set_weight,
	   no_set_weight_cmd,
	   "no set weight",
	   NO_STR
	   SET_STR
	   "BGP weight for routing table\n")


  DEFUNST (set_aspath_prepend,
	   set_aspath_prepend_cmd,
	   "set as-path prepend .<1-65535>",
	   SET_STR
	   "Prepend string for a BGP AS-path attribute\n"
	   "Prepend to the as-path\n"
	   "AS number\n")


  DEFUNST (no_set_aspath_prepend,
	   no_set_aspath_prepend_cmd,
	   "no set as-path prepend",
	   NO_STR
	   SET_STR
	   "Prepend string for a BGP AS-path attribute\n"
	   "Prepend to the as-path\n")

  DEFUNST (set_community,
	   set_community_cmd,
	   "set community .AA:NN",
	   SET_STR
	   "BGP community attribute\n"
	   "Community number in aa:nn format or local-AS|no-advertise|no-export\n")


  DEFUNST (no_set_community,
	   no_set_community_cmd,
	   "no set community",
	   NO_STR
	   SET_STR
	   "BGP community attribute\n")

  DEFUNST (set_community_delete,
	   set_community_delete_cmd,
	   "set community-delete WORD",
	   SET_STR
	   "BGP community attribute (Delete from the existing community)\n"
	   "Community list (Permitted communities are deleted)\n")


  DEFUNST (no_set_community_delete,
	   no_set_community_delete_cmd,
	   "no set community-delete",
	   NO_STR
	   SET_STR
	   "BGP community attribute (Delete from existing community)\n")


  DEFUNST (set_community_additive,
       set_community_additive_cmd,
       "set community-additive .AA:NN",
       SET_STR
       "BGP community attribute (Add to the existing community)\n"
       "Community number in aa:nn format or local-AS|no-advertise|no-export\n")


  DEFUNST (no_set_community_additive,
       no_set_community_additive_cmd,
       "no set community-additive",
       NO_STR
       SET_STR
       "BGP community attribute (Add to the existing community)\n")


  DEFUNST (set_ecommunity_rt,
	   set_ecommunity_rt_cmd,
	   "set extcommunity rt .ASN:nn_or_IP-address:nn",
	   SET_STR
	   "BGP extended community attribute\n"
	   "Route Target extened communityt\n"
	   "VPN extended community\n")


  DEFUNST (no_set_ecommunity_rt,
	   no_set_ecommunity_rt_cmd,
	   "no set extcommunity rt",
	   NO_STR
	   SET_STR
	   "BGP extended community attribute\n"
	   "Route Target extened communityt\n")


  DEFUNST (set_ecommunity_soo,
	   set_ecommunity_soo_cmd,
	   "set extcommunity soo .ASN:nn_or_IP-address:nn",
	   SET_STR
	   "BGP extended community attribute\n"
	   "Site-of-Origin extended community\n"
	   "VPN extended community\n")


  DEFUNST (no_set_ecommunity_soo,
	   no_set_ecommunity_soo_cmd,
	   "no set extcommunity soo",
	   NO_STR
	   SET_STR
	   "BGP extended community attribute\n"
	   "Site-of-Origin extended community\n")


  DEFUNST (set_origin,
	   set_origin_cmd,
	   "set origin (egp|igp|incomplete)",
	   SET_STR
	   "BGP origin code\n"
	   "remote EGP\n"
	   "local IGP\n"
	   "unknown heritage\n")


  DEFUNST (no_set_origin,
	   no_set_origin_cmd,
	   "no set origin",
	   NO_STR
	   SET_STR
	   "BGP origin code\n")



  DEFUNST (set_atomic_aggregate,
	   set_atomic_aggregate_cmd,
	   "set atomic-aggregate",
	   SET_STR
	   "BGP atomic aggregate attribute\n" )


  DEFUNST (no_set_atomic_aggregate,
	   no_set_atomic_aggregate_cmd,
	   "no set atomic-aggregate",
	   NO_STR
	   SET_STR
	   "BGP atomic aggregate attribute\n" )


  DEFUNST (set_aggregator_as,
	   set_aggregator_as_cmd,
	   "set aggregator as <1-65535> A.B.C.D",
	   SET_STR
	   "BGP aggregator attribute\n"
	   "AS number of aggregator\n"
	   "AS number\n"
	   "IP address of aggregator\n")


  DEFUNST (no_set_aggregator_as,
	   no_set_aggregator_as_cmd,
	   "no set aggregator as",
	   NO_STR
	   SET_STR
	   "BGP aggregator attribute\n"
	   "AS number of aggregator\n")


  DEFUNST (set_originator_id,
	   set_originator_id_cmd,
	   "set originator-id A.B.C.D",
	   SET_STR
	   "BGP originator ID attribute\n"
	   "IP address of originator\n")


  DEFUNST (no_set_originator_id,
	   no_set_originator_id_cmd,
	   "no set originator-id",
	   NO_STR
	   SET_STR
	   "BGP originator ID attribute\n")

  DEFUNST (bgp_router_id, 
	   bgp_router_id_cmd,
	   "bgp router-id A.B.C.D",
	   "BGP specific commands\n"
	   "Override configured router identifier\n"
	   "Manually configured router identifier\n")



  DEFUNST (no_bgp_router_id, 
	   no_bgp_router_id_cmd,
	   "no bgp router-id",
	   NO_STR
	   "BGP specific commands\n"
	   "Override configured router identifier\n")


  DEFUNST (bgp_timers, 
	   bgp_timers_cmd,
	   "timers bgp <0-65535> <0-65535>",
	   "Adjust routing timers\n"
	   "BGP timers\n"
	   "Keepalive interval\n"
	   "Holdtime\n")


  DEFUNST (no_bgp_timers, 
	   no_bgp_timers_cmd,
	   "no timers bgp",
	   NO_STR
	   "Adjust routing timers\n"
	   "BGP timers\n")


  DEFUNST (bgp_cluster_id, 
	   bgp_cluster_id_cmd,
	   "bgp cluster-id A.B.C.D",
	   "BGP specific commands\n"
	   "Configure Route-Reflector Cluster-id\n"
	   "Route-Reflector Cluster-id in IP address format\n")


  DEFUNST (no_bgp_cluster_id, 
	   no_bgp_cluster_id_cmd,
	   "no bgp cluster-id",
	   NO_STR
	   "BGP specific commands\n"
	   "Configure Route-Reflector Cluster-id\n")


  DEFUNST (bgp_confederation_peers, 
	   bgp_confederation_peers_cmd,
	   "bgp confederation peers .<1-65535>",
	   "BGP specific commands\n"
	   "AS confederation parameters\n"
	   "Peer ASs in BGP confederation\n"
	   AS_STR)


  DEFUNST (bgp_confederation_identifier, 
	   bgp_confederation_identifier_cmd,
	   "bgp confederation identifier <1-65535>",
	   "BGP specific commands\n"
	   "AS confederation parameters\n"
	   "AS number\n"
	   "Set routing domain confederation AS\n")


  DEFUNST (no_bgp_confederation_peers, 
	   no_bgp_confederation_peers_cmd,
	   "no bgp confederation peers .<1-65535>",
	   NO_STR
	   "BGP specific commands\n"
	   "AS confederation parameters\n"
	   "Peer ASs in BGP confederation\n"
	   AS_STR)


  DEFUNST (no_bgp_confederation_identifier, 
	   no_bgp_confederation_identifier_cmd,
	   "no bgp confederation identifier <1-65535>",
	   NO_STR
	   "BGP specific commands\n"
	   "AS confederation parameters\n"
	   "AS number\n"
	   "Set routing domain confederation AS\n")



  DEFUNST (no_bgp_client_to_client_reflection,
	   no_bgp_client_to_client_reflection_cmd,
	   "no bgp client-to-client reflection",
	   NO_STR
	   "BGP specific commands\n"
	   "Configure client to client route reflection\n"
	   "reflection of routes allowed\n")


  DEFUNST (bgp_client_to_client_reflection,
	   bgp_client_to_client_reflection_cmd,
	   "bgp client-to-client reflection",
	   "BGP specific commands\n"
	   "Configure client to client route reflection\n"
	   "reflection of routes allowed\n")


  DEFUNST (bgp_always_compare_med,
	   bgp_always_compare_med_cmd,
	   "bgp always-compare-med",
	   "BGP specific commands\n"
	   "Allow comparing MED from different neighbors\n")



  DEFUNST (no_bgp_always_compare_med,
	   no_bgp_always_compare_med_cmd,
	   "no bgp always-compare-med",
	   NO_STR
	   "BGP specific commands\n"
	   "Allow comparing MED from different neighbors\n")


  DEFUNST (bgp_deterministic_med,
	   bgp_deterministic_med_cmd,
	   "bgp deterministic-med",
	   "BGP specific commands\n"
	   "Pick the best-MED path among paths advertised from the neighboring AS\n")


  DEFUNST (no_bgp_deterministic_med,
	   no_bgp_deterministic_med_cmd,
	   "no bgp deterministic-med",
	   NO_STR
	   "BGP specific commands\n"
	   "Pick the best-MED path among paths advertised from the neighboring AS\n")


  DEFUNST (bgp_enforce_first_as,
	   bgp_enforce_first_as_cmd,
	   "bgp enforce-first-as",
	   BGP_STR
	   "Enforce the first AS for EBGP routes\n")

  DEFUNST (no_bgp_enforce_first_as,
	   no_bgp_enforce_first_as_cmd,
	   "no bgp enforce-first-as",
	   NO_STR
	   BGP_STR
	   "Enforce the first AS for EBGP routes\n")


  DEFUNST (bgp_bestpath_compare_router_id,
	   bgp_bestpath_compare_router_id_cmd,
	   "bgp bestpath compare-routerid",
	   "BGP specific commands\n"
	   "Change the default bestpath selection\n"
	   "Compare router-id for identical EBGP paths\n")


  DEFUNST (no_bgp_bestpath_compare_router_id,
	   no_bgp_bestpath_compare_router_id_cmd,
	   "no bgp bestpath compare-routerid",
	   NO_STR
	   "BGP specific commands\n"
	   "Change the default bestpath selection\n"
	   "Compare router-id for identical EBGP paths\n")


  DEFUNST (bgp_bestpath_aspath_ignore,
	   bgp_bestpath_aspath_ignore_cmd,
	   "bgp bestpath as-path ignore",
	   "BGP specific commands\n"
	   "Change the default bestpath selection\n"
	   "AS-path attribute\n"
	   "Ignore as-path length in selecting a route\n")


  DEFUNST (no_bgp_bestpath_aspath_ignore,
	   no_bgp_bestpath_aspath_ignore_cmd,
	   "no bgp bestpath as-path ignore",
	   NO_STR
	   "BGP specific commands\n"
	   "Change the default bestpath selection\n"
	   "AS-path attribute\n"
	   "Ignore as-path length in selecting a route\n")


  DEFUNST (bgp_bestpath_med,
	   bgp_bestpath_med_cmd,
	   "bgp bestpath med (confed|missing-as-worst)",
	   "BGP specific commands\n"
	   "Change the default bestpath selection\n"
	   "MED attribute\n"
	   "Compare MED among confederation paths\n"
	   "Treat missing MED as the least preferred one\n")


  DEFUNST (bgp_bestpath_med2,
	   bgp_bestpath_med2_cmd,
	   "bgp bestpath med confed missing-as-worst",
	   "BGP specific commands\n"
	   "Change the default bestpath selection\n"
	   "MED attribute\n"
	   "Compare MED among confederation paths\n"
	   "Treat missing MED as the least preferred one\n")

  DEFUNST (no_bgp_bestpath_med,
	   no_bgp_bestpath_med_cmd,
	   "no bgp bestpath med (confed|missing-as-worst)",
	   NO_STR
	   "BGP specific commands\n"
	   "Change the default bestpath selection\n"
	   "MED attribute\n"
	   "Compare MED among confederation paths\n"
	   "Treat missing MED as the least preferred one\n")


  DEFUNST (no_bgp_bestpath_med2,
	   no_bgp_bestpath_med2_cmd,
	   "no bgp bestpath med confed missing-as-worst",
	   NO_STR
	   "BGP specific commands\n"
	   "Change the default bestpath selection\n"
	   "MED attribute\n"
	   "Compare MED among confederation paths\n"
	   "Treat missing MED as the least preferred one\n")


  DEFUNST (bgp_default_local_preference,
	   bgp_default_local_preference_cmd,
	   "bgp default local-preference <0-4294967295>",
	   "BGP specific commands\n"
	   "Configure BGP defaults\n"
	   "local preference (higher=more preferred)\n"
	   "Configure default local preference value\n")


  DEFUNST (no_bgp_default_local_preference,
	   no_bgp_default_local_preference_cmd,
	   "no bgp default local-preference",
	   NO_STR
	   "BGP specific commands\n"
	   "Configure BGP defaults\n"
	   "local preference (higher=more preferred)\n")

  DEFUNST (router_bgp, 
	   router_bgp_cmd, 
	   "router bgp <1-65535>",
	   ROUTER_STR
	   BGP_STR
	   AS_STR)

  DEFUNST (show_startup_config,
	   show_startup_config_cmd,
	   "show startup-config",
	   SHOW_STR
	   "Contentes of startup configuration\n")

  DEFUNST (config_log_file,
	   config_log_file_cmd,
	   "log file FILENAME",
	   "Logging control\n"
	   "Logging to file\n"
	   "Logging filename\n")
  DEFUNST (no_config_log_file,
	   no_config_log_file_cmd,
	   "no log file [FILENAME]",
	   NO_STR
	   "Logging control\n"
	   "Cancel logging to file\n"
	   "Logging file name\n")

  DEFUNST (neighbor_advertise_interval,
	   neighbor_advertise_interval_cmd,
	   NEIGHBOR_CMD "advertisement-interval <0-600>",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Minimum interval between sending BGP routing updates\n"
	   "time in seconds\n")


  DEFUNST (no_neighbor_advertise_interval,
	   no_neighbor_advertise_interval_cmd,
	   NO_NEIGHBOR_CMD "advertisement-interval",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Minimum interval between sending BGP routing updates\n")

#ifdef HAVE_ZEBRA_93b
  DEFUNST (clear_ip_bgp_dampening,
	 clear_ip_bgp_dampening_cmd,
	 "clear ip bgp dampening",
	 CLEAR_STR
	 IP_STR
	 BGP_STR
	 "Clear route flap dampening information\n")

  DEFUNST (clear_ip_bgp_dampening_prefix,
	 clear_ip_bgp_dampening_prefix_cmd,
	 "clear ip bgp dampening A.B.C.D/M",
	 CLEAR_STR
	 IP_STR
	 BGP_STR
	 "Clear route flap dampening information\n"
	 "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n")

  DEFUNST (clear_ip_bgp_dampening_address,
         clear_ip_bgp_dampening_address_cmd,
         "clear ip bgp dampening A.B.C.D",
         CLEAR_STR
         IP_STR
         BGP_STR
         "Clear route flap dampening information\n"
         "Network to clear damping information\n")

  DEFUNST (clear_ip_bgp_dampening_address_mask,
         clear_ip_bgp_dampening_address_mask_cmd,
         "clear ip bgp dampening A.B.C.D A.B.C.D",
         CLEAR_STR
         IP_STR
         BGP_STR
         "Clear route flap dampening information\n"
         "Network to clear damping information\n"
         "Network mask\n")

#endif  /*HAVE_ZEBRA_93b*/


  DEFUNST (show_memory_all,
	   show_memory_all_cmd,
	   "show memory all",
	   "Show running system information\n"
	   "Memory statistics\n"
	   "All memory statistics\n")
  DEFUNST (show_memory_lib,
	   show_memory_lib_cmd,
	   "show memory lib",
	   SHOW_STR
	   "Memory statistics\n"
	   "Library memory\n")

  DEFUNST (show_memory_bgp,
	   show_memory_bgp_cmd,
	   "show memory bgp",
	   SHOW_STR
	   "Memory statistics\n"
	   "BGP memory\n")

  DEFUNST (no_router_bgp,
	  no_router_bgp_cmd,
	  "no router bgp <1-65535>",
	  NO_STR
	  ROUTER_STR
	  BGP_STR
	  AS_STR)

  DEFUNST (neighbor_remote_as,
	   neighbor_remote_as_cmd,
	   NEIGHBOR_CMD "remote-as <1-65535>",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Specify a BGP neighbor\n"
	   AS_STR)

  /*DEFUNST (no_neighbor_remote_as,
	   no_neighbor_remote_as_cmd,
	   NO_NEIGHBOR_CMD "remote-as <1-65535>",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Specify a BGP neighbor\n"
	   AS_STR)
*/
  DEFUNST (neighbor_activate,
	   neighbor_activate_cmd,
	   NEIGHBOR_CMD "activate",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Enable the Address Family for this Neighbor\n")

  DEFUNST (no_neighbor_activate,
	   no_neighbor_activate_cmd,
	   NO_NEIGHBOR_CMD "activate",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Enable the Address Family for this Neighbor\n")

  DEFUNST (no_neighbor,
	   no_neighbor_cmd,
	   NO_NEIGHBOR_CMD,
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR)

  DEFUNST (neighbor_passive,
	   neighbor_passive_cmd,
	   NEIGHBOR_CMD "passive",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Don't send open messages to this neighbor\n")

  DEFUNST (no_neighbor_passive,
	   no_neighbor_passive_cmd,
	   NO_NEIGHBOR_CMD "passive",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Don't send open messages to this neighbor\n")

  DEFUNST (neighbor_shutdown,
	   neighbor_shutdown_cmd,
	   NEIGHBOR_CMD "shutdown",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Administratively shut down this neighbor\n")


  DEFUNST (no_neighbor_shutdown,
	   no_neighbor_shutdown_cmd,
	   NO_NEIGHBOR_CMD "shutdown",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Administratively shut down this neighbor\n") 



  DEFUNST (neighbor_ebgp_multihop,
	   neighbor_ebgp_multihop_cmd,
	   NEIGHBOR_CMD "ebgp-multihop",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Allow EBGP neighbors not on directly connected networks\n")

  DEFUNST (no_neighbor_ebgp_multihop,
	   no_neighbor_ebgp_multihop_cmd,
	   NO_NEIGHBOR_CMD "ebgp-multihop",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Allow EBGP neighbors not on directly connected networks\n")


  DEFUNST (neighbor_ebgp_multihop_ttl,
	   neighbor_ebgp_multihop_ttl_cmd,
	   NEIGHBOR_CMD "ebgp-multihop <1-255>",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Allow EBGP neighbors not on directly connected networks\n"
	   "maximum hop count\n")

  DEFUNST (neighbor_description,
	   neighbor_description_cmd,
	   NEIGHBOR_CMD "description .LINE",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Neighbor specific description\n"
	   "Up to 80 characters describing this neighbor\n")


  DEFUNST (no_neighbor_description,
	   no_neighbor_description_cmd,
	   NO_NEIGHBOR_CMD "description",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Neighbor specific description\n")
  DEFUNST (neighbor_version,
	   neighbor_version_cmd,
	   NEIGHBOR_CMD "version (4|4-)",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Neighbor's BGP version\n"
	   "Border Gateway Protocol 4\n"
	   "Multiprotocol Extensions for BGP-4(Old Draft)\n")


  DEFUNST (no_neighbor_version,
	   no_neighbor_version_cmd,
	   NO_NEIGHBOR_CMD "version",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Neighbor's BGP version\n")


  DEFUNST (neighbor_nexthop_self,
	   neighbor_nexthop_self_cmd,
	   NEIGHBOR_CMD "next-hop-self",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Disable the next hop calculation for this neighbor\n")

  DEFUNST (no_neighbor_nexthop_self,
	   no_neighbor_nexthop_self_cmd,
	   NO_NEIGHBOR_CMD "next-hop-self",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Disable the next hop calculation for this neighbor\n")


  DEFUNST (neighbor_send_community,
	   neighbor_send_community_cmd,
	   NEIGHBOR_CMD "send-community",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Send Community attribute to this neighbor (default enable)\n")


  DEFUNST (no_neighbor_send_community,
	   no_neighbor_send_community_cmd,
	   NO_NEIGHBOR_CMD "send-community",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Send Community attribute to this neighbor (default enable)\n")

  DEFUNST (neighbor_send_community_type,
	   neighbor_send_community_type_cmd,
	   NEIGHBOR_CMD "send-community (both|extended|standard)",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Send Community attribute to this neighbor (default enable)\n"
	   "Send Standard and Extended Community attributes\n"
	   "Send Extended Community attributes\n"
	   "Send Standard Community attributes\n")


  DEFUNST (no_neighbor_send_community_type,
	   no_neighbor_send_community_type_cmd,
	   NO_NEIGHBOR_CMD "send-community (both|extended|standard)",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Send Community attribute to this neighbor (default enable)\n"
	   "Send Standard and Extended Community attributes\n"
	   "Send Extended Community attributes\n"
	   "Send Standard Community attributes\n")

  DEFUNST (neighbor_weight,
	   neighbor_weight_cmd,
	   NEIGHBOR_CMD "weight <0-65535>",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Set default weight for routes from this neighbor\n"
	   "default weight\n")


  DEFUNST (no_neighbor_weight,
	   no_neighbor_weight_cmd,
	   NO_NEIGHBOR_CMD "weight",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Set default weight for routes from this neighbor\n")

  DEFUNST (neighbor_soft_reconfiguration,
	   neighbor_soft_reconfiguration_cmd,
	   NEIGHBOR_CMD "soft-reconfiguration inbound",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Per neighbor soft reconfiguration\n"
	   "Allow inbound soft reconfiguration for this neighbor\n")

  DEFUNST (no_neighbor_soft_reconfiguration,
	   no_neighbor_soft_reconfiguration_cmd,
	   NO_NEIGHBOR_CMD "soft-reconfiguration inbound",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Per neighbor soft reconfiguration\n"
	   "Allow inbound soft reconfiguration for this neighbor\n")

  DEFUNST (neighbor_route_reflector_client,
	   neighbor_route_reflector_client_cmd,
	   NEIGHBOR_CMD "route-reflector-client",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Configure a neighbor as Route Reflector client\n")

  DEFUNST (no_neighbor_route_reflector_client,
	   no_neighbor_route_reflector_client_cmd,
	   NO_NEIGHBOR_CMD "route-reflector-client",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Configure a neighbor as Route Reflector client\n")

  DEFUNST (neighbor_capability_route_refresh,
	   neighbor_capability_route_refresh_cmd,
	   NEIGHBOR_CMD "capability route-refresh",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Outbound capability configuration\n"
	   "Advertise route-refresh capability to this neighbor\n")


  DEFUNST (no_neighbor_capability_route_refresh,
	   no_neighbor_capability_route_refresh_cmd,
	   NO_NEIGHBOR_CMD "capability route-refresh",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Outbound capability configuration\n"
	   "Advertise route-refresh capability to this neighbor\n")

  /* neighbor dont-capability-negotiate */
  DEFUNST (neighbor_dont_capability_negotiate,
	   neighbor_dont_capability_negotiate_cmd,
	   NEIGHBOR_CMD "dont-capability-negotiate",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Do not perform capability negotiation\n")

  DEFUNST (no_neighbor_dont_capability_negotiate,
	   no_neighbor_dont_capability_negotiate_cmd,
	   NO_NEIGHBOR_CMD "dont-capability-negotiate",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Do not perform capability negotiation\n")

  DEFUNST (neighbor_override_capability,
	   neighbor_override_capability_cmd,
	   NEIGHBOR_CMD "override-capability",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Override capability negotiation result\n")

  DEFUNST (no_neighbor_override_capability,
	   no_neighbor_override_capability_cmd,
	   NO_NEIGHBOR_CMD "override-capability",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Override capability negotiation result\n")

  DEFUNST (neighbor_strict_capability,
	   neighbor_strict_capability_cmd,
	   NEIGHBOR_CMD "strict-capability-match",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Strict capability negotiation match\n")

  DEFUNST (no_neighbor_strict_capability,
	   no_neighbor_strict_capability_cmd,
	   NO_NEIGHBOR_CMD "strict-capability-match",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Strict capability negotiation match\n")

  DEFUNST (neighbor_timers,
	   neighbor_timers_cmd,
	   NEIGHBOR_CMD "timers <0-65535> <0-65535>",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "BGP per neighbor timers\n"
	   "Keepalive interval\n"
	   "Holdtime\n")

  DEFUNST (no_neighbor_timers,
	   no_neighbor_timers_cmd,
	   NO_NEIGHBOR_CMD "timers",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "BGP per neighbor timers\n")

  DEFUNST (neighbor_timers_connect,
	   neighbor_timers_connect_cmd,
	   NEIGHBOR_CMD "timers connect <0-65535>",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "BGP per neighbor timers\n"
	   "BGP connect timer\n"
	   "Connect timer\n")

  DEFUNST (no_neighbor_timers_connect,
	   no_neighbor_timers_connect_cmd,
	   NO_NEIGHBOR_CMD "timers connect [TIMER]",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "BGP per neighbor timers\n"
	   "BGP connect timer\n"
	   "Connect timer\n")

  DEFUNST (neighbor_prefix_list,
	   neighbor_prefix_list_cmd,
	   NEIGHBOR_CMD "prefix-list WORD (in|out)",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Filter updates to/from this neighbor\n"
	   "Name of a prefix list\n"
	   "Filter incoming updates\n"
	   "Filter outgoing updates\n")

  DEFUNST (no_neighbor_prefix_list,
	   no_neighbor_prefix_list_cmd,
	   NO_NEIGHBOR_CMD "prefix-list WORD (in|out)",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Filter updates to/from this neighbor\n"
	   "Name of a prefix list\n"
	   "Filter incoming updates\n"
	   "Filter outgoing updates\n")
  DEFUNST (neighbor_filter_list,
	   neighbor_filter_list_cmd,
	   NEIGHBOR_CMD "filter-list WORD (in|out)",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Establish BGP filters\n"
	   "AS path access-list name\n"
	   "Filter incoming routes\n"
	   "Filter outgoing routes\n")


  DEFUNST (no_neighbor_filter_list,
	   no_neighbor_filter_list_cmd,
	   NO_NEIGHBOR_CMD "filter-list WORD (in|out)",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Establish BGP filters\n"
	   "AS path access-list name\n"
	   "Filter incoming routes\n"
	   "Filter outgoing routes\n")

  DEFUNST (neighbor_route_map,
	   neighbor_route_map_cmd,
	   NEIGHBOR_CMD "route-map WORD (in|out)",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Apply route map to neighbor\n"
	   "Name of route map\n"
	   "Apply map to incoming routes\n"
	   "Apply map to outbound routes\n")

  DEFUNST (no_neighbor_route_map,
	   no_neighbor_route_map_cmd,
	   NO_NEIGHBOR_CMD "route-map WORD (in|out)",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Apply route map to neighbor\n"
	   "Name of route map\n"
	   "Apply map to incoming routes\n"
	   "Apply map to outbound routes\n")

  DEFUNST (neighbor_maximum_prefix,
	   neighbor_maximum_prefix_cmd,
	   NEIGHBOR_CMD "maximum-prefix <1-4294967295>",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Maximum number of prefix accept from this peer\n"
	   "maximum no. of prefix limit\n")

  DEFUNST (no_neighbor_maximum_prefix,
	   no_neighbor_maximum_prefix_cmd,
	   NO_NEIGHBOR_CMD "maximum-prefix",
	   NO_STR
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Maximum number of prefix accept from this peer\n")

  DEFUNST (neighbor_transparent_as,
	   neighbor_transparent_as_cmd,
	   NEIGHBOR_CMD "transparent-as",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Do not append my AS number even peer is EBGP peer\n")

  DEFUNST (neighbor_transparent_nexthop,
	   neighbor_transparent_nexthop_cmd,
	   NEIGHBOR_CMD "transparent-nexthop",
	   NEIGHBOR_STR
	   NEIGHBOR_ADDR_STR
	   "Do not change nexthop even peer is EBGP peer\n")

  DEFUNST (show_ip_bgp_summary, 
	   show_ip_bgp_summary_cmd,
	   "show ip bgp summary",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Summary of BGP neighbor status\n")

  DEFUNST (show_ip_bgp_neighbors,
	   show_ip_bgp_neighbors_cmd,
	   "show ip bgp neighbors",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Detailed information on TCP and BGP neighbor connections\n")

  DEFUNST (show_ip_bgp_neighbors_peer,
	   show_ip_bgp_neighbors_peer_cmd,
	   "show ip bgp neighbors (A.B.C.D|X:X::X:X)",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Detailed information on TCP and BGP neighbor connections\n"
	   "Neighbor to display information about\n"
	   "Neighbor to display information about\n")

  DEFUNST (show_ip_bgp_paths, 
	   show_ip_bgp_paths_cmd,
	   "show ip bgp paths",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "Path information\n")

  DEFUNST (show_ip_bgp_community_info, 
	   show_ip_bgp_community_info_cmd,
	   "show ip bgp community-info",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "List all bgp community information\n")

  DEFUNST (show_ip_bgp_attr_info, 
	   show_ip_bgp_attr_info_cmd,
	   "show ip bgp attribute-info",
	   SHOW_STR
	   IP_STR
	   BGP_STR
	   "List all bgp attribute information\n")

  DEFUNST (clear_ip_bgp_all,
	   clear_ip_bgp_all_cmd,
	   "clear ip bgp *",
	   CLEAR_STR
	   IP_STR
	   BGP_STR
	   "Clear all peers\n")

  DEFUNST (clear_ip_bgp_as,
	   clear_ip_bgp_as_cmd,
	   "clear ip bgp <1-65535>",
	   CLEAR_STR
	   IP_STR
	   BGP_STR
	   "Clear peers with the AS number\n")

  DEFUNST (clear_ip_bgp_peer,
	   clear_ip_bgp_peer_cmd, 
	   "clear ip bgp (A.B.C.D|X:X::X:X)",
	   CLEAR_STR
	   IP_STR
	   BGP_STR
	   "BGP neighbor IP address to clear\n"
	   "BGP neighbor IPv6 address to clear\n")

  DEFUNST (clear_ip_bgp_peer_soft_in,
	   clear_ip_bgp_peer_soft_in_cmd,
	   "clear ip bgp A.B.C.D soft in",
	   CLEAR_STR
	   IP_STR
	   BGP_STR
	   "BGP neighbor address to clear\n"
	   "Soft reconfig\n"
	   "Soft reconfig inbound update\n")

  DEFUNST (clear_ip_bgp_as_soft_in,
	   clear_ip_bgp_as_soft_in_cmd,
	   "clear ip bgp <1-65535> soft in",
	   CLEAR_STR
	   IP_STR
	   BGP_STR
	   "Clear peers with the AS number\n"
	   "Soft reconfig\n"
	   "Soft reconfig inbound update\n")


  DEFUNST (clear_ip_bgp_all_soft_in,
	   clear_ip_bgp_all_soft_in_cmd,
	   "clear ip bgp * soft in",
	   CLEAR_STR
	   IP_STR
	   BGP_STR
	   "Clear all peers\n"
	   "Soft reconfig\n"
	   "Soft reconfig inbound update\n")

  DEFUNST (clear_ip_bgp_as_soft_out,
	   clear_ip_bgp_as_soft_out_cmd,
	   "clear ip bgp <1-65535> soft out",
	   CLEAR_STR
	   IP_STR
	   BGP_STR
	   "Clear peers with the AS number\n"
	   "Soft reconfig\n"
	   "Soft reconfig outbound update\n")

  DEFUNST (clear_ip_bgp_all_soft_out,
	   clear_ip_bgp_all_soft_out_cmd,
	   "clear ip bgp * soft out",
	   CLEAR_STR
	   IP_STR
	   BGP_STR
	   "Clear all peers\n"
	   "Soft reconfig\n"
	   "Soft reconfig outbound update\n")

  DEFUNST (clear_ip_bgp_peer_soft,
	   clear_ip_bgp_peer_soft_cmd,
	   "clear ip bgp A.B.C.D soft",
	   CLEAR_STR
	   IP_STR
	   BGP_STR
	   "BGP neighbor address to clear\n"
	   "Soft reconfig\n")

  DEFUNST (clear_ip_bgp_as_soft,
	   clear_ip_bgp_as_soft_cmd,
	   "clear ip bgp <1-65535> soft",
	   CLEAR_STR
	   IP_STR
	   BGP_STR
	   "Clear peers with the AS number\n"
	   "Soft reconfig\n")

  DEFUNST (clear_ip_bgp_all_soft,
	   clear_ip_bgp_all_soft_cmd,
	   "clear ip bgp * soft",
	   CLEAR_STR
	   IP_STR
	   BGP_STR
	   "Clear all peers\n"
	   "Soft reconfig\n")

  DEFUNST (clear_ip_bgp_peer_soft_out,
	   clear_ip_bgp_peer_soft_out_cmd,
	   "clear ip bgp A.B.C.D soft out",
	   CLEAR_STR
	   IP_STR
	   BGP_STR
	   "BGP neighbor address to clear\n"
	   "Soft reconfig\n"
	   "Soft reconfig outbound update\n")

  DEFUNST (route_map,
	   route_map_cmd,
	   "route-map WORD (deny|permit) <1-65535>",
	   "Create route-map or enter route-map command mode\n"
	   "Route map tag\n"
	   "Route map denies set operations\n"
	   "Route map permits set operations\n"
	   "Sequence to insert to/delete from existing route-map entry\n")

  DEFUNST (no_route_map,
	   no_route_map_cmd,
	   "no route-map WORD (deny|permit) <1-65535>",
	   NO_STR
	   "Create route-map or enter route-map command mode\n"
	   "Route map tag\n"
	   "Route map denies set operations\n"
	   "Route map permits set operations\n"
	   "Sequence to insert to/delete from existing route-map entry\n")

  DEFUNST (no_route_map_all,
	   no_route_map_all_cmd,
	   "no route-map WORD",
	   NO_STR
	   "Create route-map or enter route-map command mode\n"
	   "Route map tag\n")

  DEFUNST (rmap_onmatch_next,
	   rmap_onmatch_next_cmd,
	   "on-match next",
	   "Exit policy on matches\n"
	   "Next clause\n")

  DEFUNST (no_rmap_onmatch_next,
	   no_rmap_onmatch_next_cmd,
	   "no on-match next",
	   NO_STR
	   "Exit policy on matches\n"
	   "Next clause\n")

  DEFUNST (rmap_onmatch_goto,
	   rmap_onmatch_goto_cmd,
	   "on-match goto <1-65535>",
	   "Exit policy on matches\n"
	   "Goto Clause number\n"
	   "Number\n")

  DEFUNST (no_rmap_onmatch_goto,
	   no_rmap_onmatch_goto_cmd,
	   "no on-match goto",
	   NO_STR
	   "Exit policy on matches\n"
	   "Next clause\n")

  DEFUNST (access_list, 
	   access_list_cmd,
	   "access-list WORD (deny|permit) A.B.C.D/M",
	   "Add an access list entry\n"
	   "Access-list name\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "Prefix to match. e.g. 10.0.0.0/8\n"
	   "Any prefix to match\n")

  DEFUNST (no_access_list,
	   no_access_list_cmd,
	   "no access-list WORD (deny|permit) (A.B.C.D/M)",
	   NO_STR 
	   "Add an access list entry\n"
	   "Access-list name\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "Prefix to match. e.g. 10.0.0.0/8\n")

  DEFUNST (access_list_remark,
	   access_list_remark_cmd,
	   "access-list WORD remark .LINE",
	   "Add an access list entry\n"
	   "Access-list name\n"
	   "Access list entry comment\n"
	   "Comment up to 100 characters\n")

  DEFUNST (no_access_list_remark,
	   no_access_list_remark_cmd,
	   "no access-list WORD remark",
	   NO_STR
	   "Add an access list entry\n"
	   "Access-list name\n"
	   "Access list entry comment\n")

  DEFUNST (no_access_list_all,
	   no_access_list_all_cmd,
	   "no access-list WORD",
	   NO_STR
	   "Add an access list entry\n"
	   "Access-list name\n")

  DEFUNST (ip_prefix_list,
	   ip_prefix_list_cmd,
	   "ip prefix-list WORD (deny|permit) (A.B.C.D/M|any)",
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Any prefix match. Same as \"0.0.0.0/0 le 32\"\n")

  DEFUNST (no_ip_prefix_list,
	   no_ip_prefix_list_cmd,
	   "no ip prefix-list WORD",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n")

  DEFUNST (show_ip_prefix_list,
	   show_ip_prefix_list_cmd,
	   "show ip prefix-list",
	   SHOW_STR
	   IP_STR
	   PREFIX_LIST_STR)

  DEFUNST (clear_ip_prefix_list,
	   clear_ip_prefix_list_cmd,
	   "clear ip prefix-list",
	   CLEAR_STR
	   IP_STR
	   PREFIX_LIST_STR)

  DEFUNST (ip_prefix_list_ge,
	   ip_prefix_list_ge_cmd,
	   "ip prefix-list WORD (deny|permit) A.B.C.D/M ge <0-32>",
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Minimum prefix length to be matched\n"
	   "Minimum prefix length\n")

  DEFUNST (no_ip_prefix_list_ge,
	   no_ip_prefix_list_ge_cmd,
	   "no ip prefix-list WORD (deny|permit) A.B.C.D/M ge <0-32>",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Minimum prefix length to be matched\n"
	   "Minimum prefix length\n")

  DEFUNST (ip_prefix_list_ge_le,
	   ip_prefix_list_ge_le_cmd,
	   "ip prefix-list WORD (deny|permit) A.B.C.D/M ge <0-32> le <0-32>",
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Minimum prefix length to be matched\n"
	   "Minimum prefix length\n"
	   "Maximum prefix length to be matched\n"
	   "Maximum prefix length\n")

  DEFUNST (ip_prefix_list_le,
	   ip_prefix_list_le_cmd,
	   "ip prefix-list WORD (deny|permit) A.B.C.D/M le <0-32>",
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Maximum prefix length to be matched\n"
	   "Maximum prefix length\n")

  DEFUNST (no_ip_prefix_list_le,
	   no_ip_prefix_list_le_cmd,
	   "no ip prefix-list WORD (deny|permit) A.B.C.D/M le <0-32>",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Maximum prefix length to be matched\n"
	   "Maximum prefix length\n")

  DEFUNST (ip_prefix_list_le_ge,
	   ip_prefix_list_le_ge_cmd,
	   "ip prefix-list WORD (deny|permit) A.B.C.D/M le <0-32> ge <0-32>",
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Maximum prefix length to be matched\n"
	   "Maximum prefix length\n"
	   "Minimum prefix length to be matched\n"
	   "Minimum prefix length\n")

  DEFUNST (no_ip_prefix_list_le_ge,
	   no_ip_prefix_list_le_ge_cmd,
	   "no ip prefix-list WORD (deny|permit) A.B.C.D/M le <0-32> ge <0-32>",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Maximum prefix length to be matched\n"
	   "Maximum prefix length\n"
	   "Minimum prefix length to be matched\n"
	   "Minimum prefix length\n")

  DEFUNST (ip_prefix_list_seq,
	   ip_prefix_list_seq_cmd,
	   "ip prefix-list WORD seq <1-4294967295> (deny|permit) (A.B.C.D/M|any)",
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "sequence number of an entry\n"
	   "Sequence number\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Any prefix match. Same as \"0.0.0.0/0 le 32\"\n")

  DEFUNST (no_ip_prefix_list_seq,
	   no_ip_prefix_list_seq_cmd,
	   "no ip prefix-list WORD seq <1-4294967295> (deny|permit) (A.B.C.D/M|any)",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "sequence number of an entry\n"
	   "Sequence number\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Any prefix match.  Same as \"0.0.0.0/0 le 32\"\n")

  DEFUNST (ip_prefix_list_seq_ge,
	   ip_prefix_list_seq_ge_cmd,
	   "ip prefix-list WORD seq <1-4294967295> (deny|permit) A.B.C.D/M ge <0-32>",
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "sequence number of an entry\n"
	   "Sequence number\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Minimum prefix length to be matched\n"
	   "Minimum prefix length\n")

  DEFUNST (no_ip_prefix_list_seq_ge,
	   no_ip_prefix_list_seq_ge_cmd,
	   "no ip prefix-list WORD seq <1-4294967295> (deny|permit) A.B.C.D/M ge <0-32>",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "sequence number of an entry\n"
	   "Sequence number\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Minimum prefix length to be matched\n"
	   "Minimum prefix length\n")

  DEFUNST (ip_prefix_list_seq_ge_le,
	   ip_prefix_list_seq_ge_le_cmd,
	   "ip prefix-list WORD seq <1-4294967295> (deny|permit) A.B.C.D/M ge <0-32> le <0-32>",
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "sequence number of an entry\n"
	   "Sequence number\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Minimum prefix length to be matched\n"
	   "Minimum prefix length\n"
	   "Maximum prefix length to be matched\n"
	   "Maximum prefix length\n")

  DEFUNST (no_ip_prefix_list_seq_ge_le,
	   no_ip_prefix_list_seq_ge_le_cmd,
	   "no ip prefix-list WORD seq <1-4294967295> (deny|permit) A.B.C.D/M ge <0-32> le <0-32>",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "sequence number of an entry\n"
	   "Sequence number\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Minimum prefix length to be matched\n"
	   "Minimum prefix length\n"
	   "Maximum prefix length to be matched\n"
	   "Maximum prefix length\n")

  DEFUNST (ip_prefix_list_seq_le,
	   ip_prefix_list_seq_le_cmd,
	   "ip prefix-list WORD seq <1-4294967295> (deny|permit) A.B.C.D/M le <0-32>",
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "sequence number of an entry\n"
	   "Sequence number\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Maximum prefix length to be matched\n"
	   "Maximum prefix length\n")

  DEFUNST (no_ip_prefix_list_seq_le,
	   no_ip_prefix_list_seq_le_cmd,
	   "no ip prefix-list WORD seq <1-4294967295> (deny|permit) A.B.C.D/M le <0-32>",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "sequence number of an entry\n"
	   "Sequence number\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Maximum prefix length to be matched\n"
	   "Maximum prefix length\n")

  DEFUNST (ip_prefix_list_seq_le_ge,
	   ip_prefix_list_seq_le_ge_cmd,
	   "ip prefix-list WORD seq <1-4294967295> (deny|permit) A.B.C.D/M le <0-32> ge <0-32>",
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "sequence number of an entry\n"
	   "Sequence number\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Maximum prefix length to be matched\n"
	   "Maximum prefix length\n"
	   "Minimum prefix length to be matched\n"
	   "Minimum prefix length\n")

  DEFUNST (no_ip_prefix_list_seq_le_ge,
	   no_ip_prefix_list_seq_le_ge_cmd,
	   "no ip prefix-list WORD seq <1-4294967295> (deny|permit) A.B.C.D/M le <0-32> ge <0-32>",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "sequence number of an entry\n"
	   "Sequence number\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Maximum prefix length to be matched\n"
	   "Maximum prefix length\n"
	   "Minimum prefix length to be matched\n"
	   "Minimum prefix length\n")

  DEFUNST (ip_prefix_list_description,
	   ip_prefix_list_description_cmd,
	   "ip prefix-list WORD description .LINE",
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "Prefix-list specific description\n"
	   "Up to 80 characters describing this prefix-list\n")

  DEFUNST (no_ip_prefix_list_description,
	   no_ip_prefix_list_description_cmd,
	   "no ip prefix-list WORD description",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "Prefix-list specific description\n")

  DEFUNST (ip_prefix_list_sequence_number,
	   ip_prefix_list_sequence_number_cmd,
	   "ip prefix-list sequence-number",
	   IP_STR
	   PREFIX_LIST_STR
	   "Include/exclude sequence numbers in NVGEN\n")

  DEFUNST (no_ip_prefix_list_sequence_number,
	   no_ip_prefix_list_sequence_number_cmd,
	   "no ip prefix-list sequence-number",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Include/exclude sequence numbers in NVGEN\n")

  DEFUNST (show_ip_prefix_list_name,
	   show_ip_prefix_list_name_cmd,
	   "show ip prefix-list WORD",
	   SHOW_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n")

  DEFUNST (show_ip_prefix_list_name_seq,
	   show_ip_prefix_list_name_seq_cmd,
	   "show ip prefix-list WORD seq <1-4294967295>",
	   SHOW_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "sequence number of an entry\n"
	   "Sequence number\n")

  DEFUNST (show_ip_prefix_list_prefix,
	   show_ip_prefix_list_prefix_cmd,
	   "show ip prefix-list WORD A.B.C.D/M",
	   SHOW_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n")

  DEFUNST (show_ip_prefix_list_prefix_longer,
	   show_ip_prefix_list_prefix_longer_cmd,
	   "show ip prefix-list WORD A.B.C.D/M longer",
	   SHOW_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Lookup longer prefix\n")

  DEFUNST (show_ip_prefix_list_prefix_first_match,
	   show_ip_prefix_list_prefix_first_match_cmd,
	   "show ip prefix-list WORD A.B.C.D/M first-match",
	   SHOW_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "First matched prefix\n")

  DEFUNST (show_ip_prefix_list_summary,
	   show_ip_prefix_list_summary_cmd,
	   "show ip prefix-list summary",
	   SHOW_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Summary of prefix lists\n")

  DEFUNST (show_ip_prefix_list_summary_name,
	   show_ip_prefix_list_summary_name_cmd,
	   "show ip prefix-list summary WORD",
	   SHOW_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Summary of prefix lists\n"
	   "Name of a prefix list\n")

  DEFUNST (show_ip_prefix_list_detail,
	   show_ip_prefix_list_detail_cmd,
	   "show ip prefix-list detail",
	   SHOW_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Detail of prefix lists\n")

  DEFUNST (show_ip_prefix_list_detail_name,
	   show_ip_prefix_list_detail_name_cmd,
	   "show ip prefix-list detail WORD",
	   SHOW_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Detail of prefix lists\n"
	   "Name of a prefix list\n")

  DEFUNST (clear_ip_prefix_list_name,
	   clear_ip_prefix_list_name_cmd,
	   "clear ip prefix-list WORD",
	   CLEAR_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n")

  DEFUNST (clear_ip_prefix_list_name_prefix,
	   clear_ip_prefix_list_name_prefix_cmd,
	   "clear ip prefix-list WORD A.B.C.D/M",
	   CLEAR_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n")

  DEFUNST (no_ip_prefix_list_prefix,
	   no_ip_prefix_list_prefix_cmd,
	   "no ip prefix-list WORD (deny|permit) (A.B.C.D/M|any)",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Any prefix match.  Same as \"0.0.0.0/0 le 32\"\n")

  DEFUNST (no_ip_prefix_list_ge_le,
	   no_ip_prefix_list_ge_le_cmd,
	   "no ip prefix-list WORD (deny|permit) A.B.C.D/M ge <0-32> le <0-32>",
	   NO_STR
	   IP_STR
	   PREFIX_LIST_STR
	   "Name of a prefix list\n"
	   "Specify packets to reject\n"
	   "Specify packets to forward\n"
	   "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
	   "Minimum prefix length to be matched\n"
	   "Minimum prefix length\n"
	   "Maximum prefix length to be matched\n"
	   "Maximum prefix length\n")

DEFUNST (neighbor_interface,
       neighbor_interface_cmd,
       NEIGHBOR_CMD "interface WORD",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR
       "Interface\n"
       "Interface name\n")

DEFUNST (clear_ip_bgp_as_ipv4_soft_out,
       clear_ip_bgp_as_ipv4_soft_out_cmd,
       "clear ip bgp <1-65535> ipv4 (unicast|multicast) soft out",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear peers with the AS number\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig outbound update\n")

DEFUNST (clear_ip_bgp_peer_group_soft_out,
       clear_ip_bgp_peer_group_soft_out_cmd,
       "clear ip bgp peer-group WORD soft out",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all members of peer-group\n"
       "BGP peer-group name\n"
       "Soft reconfig\n"
       "Soft reconfig outbound update\n")

DEFUNST (no_bgp_multiple_instance,
       no_bgp_multiple_instance_cmd,
       "no bgp multiple-instance",
       NO_STR
       BGP_STR
       "BGP multiple instance\n")

DEFUNST (no_neighbor_remove_private_as,
       no_neighbor_remove_private_as_cmd,
       NO_NEIGHBOR_CMD2 "remove-private-AS",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Remove private AS number from outbound updates\n")

DEFUNST (show_ip_bgp_ipv4_summary,
       show_ip_bgp_ipv4_summary_cmd,
       "show ip bgp ipv4 (unicast|multicast) summary",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Summary of BGP neighbor status\n")

DEFUNST (neighbor_set_peer_group,
       neighbor_set_peer_group_cmd,
       NEIGHBOR_CMD "peer-group WORD",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR
       "Member of the peer-group\n"
       "peer-group name\n")

DEFUNST (address_family_vpnv4,
       address_family_vpnv4_cmd,
       "address-family vpnv4",
       "Enter Address Family command mode\n"
       "Address family\n")


DEFUNST (no_neighbor_local_as,
       no_neighbor_local_as_cmd,
       NO_NEIGHBOR_CMD2 "local-as",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Specify a local-as number\n")

DEFUNST (neighbor_attr_unchanged4,
       neighbor_attr_unchanged4_cmd,
       NEIGHBOR_CMD2 "attribute-unchanged med (as-path|next-hop)",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "BGP attribute is propagated unchanged to this neighbor\n"
       "Med attribute\n"
       "As-path attribute\n"
       "Nexthop attribute\n")

DEFUNST (neighbor_update_source,
       neighbor_update_source_cmd,
       NEIGHBOR_CMD2 "update-source WORD",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Source of routing updates\n"
       "Interface name\n")

DEFUNST (no_auto_summary,
       no_auto_summary_cmd,
       "no auto-summary",
       NO_STR
       "Enable automatic network number summarization\n")

DEFUNST (show_ip_extcommunity_list_arg,
       show_ip_extcommunity_list_arg_cmd,
       "show ip extcommunity-list (<1-199>|WORD)",
       SHOW_STR
       IP_STR
       "List extended-community list\n"
       "Extcommunity-list number\n"
       "Extcommunity-list name\n")

DEFUNST (neighbor_remove_private_as,
       neighbor_remove_private_as_cmd,
       NEIGHBOR_CMD2 "remove-private-AS",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Remove private AS number from outbound updates\n")

DEFUNST (clear_ip_bgp_external_soft,
       clear_ip_bgp_external_soft_cmd,
       "clear ip bgp external soft",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all external peers\n"
       "Soft reconfig\n")

DEFUNST (no_neighbor_set_peer_group,
       no_neighbor_set_peer_group_cmd,
       NO_NEIGHBOR_CMD "peer-group WORD",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR
       "Member of the peer-group\n"
       "peer-group name\n")

DEFUNST (clear_ip_bgp_external,
       clear_ip_bgp_external_cmd,
       "clear ip bgp external",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all external peers\n")

DEFUNST (no_ip_extcommunity_list_expanded,
       no_ip_extcommunity_list_expanded_cmd,
       "no ip extcommunity-list <100-199> (deny|permit) .LINE",
       NO_STR
       IP_STR
       EXTCOMMUNITY_LIST_STR
       "Extended Community list number (expanded)\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       "An ordered list as a regular-expression\n")

DEFUNST (clear_ip_bgp_as_ipv4_soft,
       clear_ip_bgp_as_ipv4_soft_cmd,
       "clear ip bgp <1-65535> ipv4 (unicast|multicast) soft",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear peers with the AS number\n"
       "Address family\n"
       "Address Family Modifier\n"
       "Address Family Modifier\n"
       "Soft reconfig\n")


DEFUNST (no_neighbor_interface,
       no_neighbor_interface_cmd,
       NO_NEIGHBOR_CMD "interface WORD",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR
       "Interface\n"
       "Interface name\n")

DEFUNST (no_neighbor_distribute_list,
       no_neighbor_distribute_list_cmd,
       NO_NEIGHBOR_CMD2 "distribute-list (<1-199>|<1300-2699>|WORD) (in|out)",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Filter updates to/from this neighbor\n"
       "IP access-list number\n"
       "IP access-list number (expanded range)\n"
       "IP Access-list name\n"
       "Filter incoming updates\n"
       "Filter outgoing updates\n")

DEFUNST (clear_ip_bgp_peer_group_ipv4_soft_in,
       clear_ip_bgp_peer_group_ipv4_soft_in_cmd,
       "clear ip bgp peer-group WORD ipv4 (unicast|multicast) soft in",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all members of peer-group\n"
       "BGP peer-group name\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig inbound update\n")

DEFUNST (clear_ip_bgp_external_ipv4_in_prefix_filter,
       clear_ip_bgp_external_ipv4_in_prefix_filter_cmd,
       "clear ip bgp external ipv4 (unicast|multicast) in prefix-filter",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all external peers\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig inbound update\n"
       "Push out prefix-list ORF and do inbound soft reconfig\n")

DEFUNST (show_ip_bgp_vpnv4_all_summary,
       show_ip_bgp_vpnv4_all_summary_cmd,
       "show ip bgp vpnv4 all summary",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display VPNv4 NLRI specific information\n"
       "Display information about all VPNv4 NLRIs\n"
       "Summary of BGP neighbor status\n")

DEFUNST (clear_ip_bgp_instance_all_ipv4_in_prefix_filter,
       clear_ip_bgp_instance_all_ipv4_in_prefix_filter_cmd,
       "clear ip bgp view WORD * ipv4 (unicast|multicast) in prefix-filter",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all peers\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig inbound update\n"
       "Push out prefix-list ORF and do inbound soft reconfig\n")

DEFUNST (clear_ip_bgp_all_ipv4_in_prefix_filter,
       clear_ip_bgp_all_ipv4_in_prefix_filter_cmd,
       "clear ip bgp * ipv4 (unicast|multicast) in prefix-filter",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all peers\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig inbound update\n"
       "Push out prefix-list ORF and do inbound soft reconfig\n")

DEFUNST (neighbor_attr_unchanged3,
       neighbor_attr_unchanged3_cmd,
       NEIGHBOR_CMD2 "attribute-unchanged next-hop (as-path|med)",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "BGP attribute is propagated unchanged to this neighbor\n"
       "Nexthop attribute\n"
       "As-path attribute\n"
       "Med attribute\n")

DEFUNST (ip_community_list_standard,
       ip_community_list_standard_cmd,
       "ip community-list <1-99> (deny|permit) .AA:NN",
       IP_STR
       COMMUNITY_LIST_STR
       "Community list number (standard)\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       COMMUNITY_VAL_STR)

DEFUNST (no_ip_extcommunity_list_name_all,
       no_ip_extcommunity_list_name_all_cmd,
       "no ip extcommunity-list (standard|expanded) WORD",
       NO_STR
       IP_STR
       EXTCOMMUNITY_LIST_STR
       "Specify standard extcommunity-list\n"
       "Specify expanded extcommunity-list\n"
       "Extended Community list name\n")

DEFUNST (no_neighbor_attr_unchanged3,
       no_neighbor_attr_unchanged3_cmd,
       NO_NEIGHBOR_CMD2 "attribute-unchanged next-hop (as-path|med)",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "BGP attribute is propagated unchanged to this neighbor\n"
       "Nexthop attribute\n"
       "As-path attribute\n"
       "Med attribute\n")

DEFUNST (clear_ip_bgp_as_ipv4_soft_in,
       clear_ip_bgp_as_ipv4_soft_in_cmd,
       "clear ip bgp <1-65535> ipv4 (unicast|multicast) soft in",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear peers with the AS number\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig inbound update\n")

DEFUNST (exit_address_family,
       exit_address_family_cmd,
       "exit-address-family",
       "Exit from Address Family configuration mode\n")

DEFUNST (clear_ip_bgp_peer_vpnv4_soft_in,
       clear_ip_bgp_peer_vpnv4_soft_in_cmd,
       "clear ip bgp A.B.C.D vpnv4 unicast soft in",
       CLEAR_STR
       IP_STR
       BGP_STR
       "BGP neighbor address to clear\n"
       "Address family\n"
       "Address Family Modifier\n"
       "Soft reconfig\n"
       "Soft reconfig inbound update\n")

DEFUNST (no_bgp_log_neighbor_changes,
       no_bgp_log_neighbor_changes_cmd,
       "no bgp log-neighbor-changes",
       NO_STR
       "BGP specific commands\n"
       "Log neighbor up/down and reset reason\n")

DEFUNST (no_neighbor_allowas_in,
       no_neighbor_allowas_in_cmd,
       NO_NEIGHBOR_CMD2 "allowas-in",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "allow local ASN appears in aspath attribute\n")


DEFUNST (clear_ip_bgp_all_vpnv4_soft_out,
       clear_ip_bgp_all_vpnv4_soft_out_cmd,
       "clear ip bgp * vpnv4 unicast soft out",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all peers\n"
       "Address family\n"
       "Address Family Modifier\n"
       "Soft reconfig\n"
       "Soft reconfig outbound update\n")


DEFUNST (show_ip_bgp_ipv4_paths,
       show_ip_bgp_ipv4_paths_cmd,
       "show ip bgp ipv4 (unicast|multicast) paths",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Path information\n")

DEFUNST (bgp_config_type,
       bgp_config_type_cmd,
       "bgp config-type (cisco|zebra)",
       BGP_STR
       "Configuration type\n"
       "cisco\n"
       "zebra\n")
DEFUNST (no_bgp_config_type,
       no_bgp_config_type_cmd,
       "no bgp config-type",
       NO_STR
       BGP_STR
       "Display configuration type\n")


DEFUNST (neighbor_allowas_in,
       neighbor_allowas_in_cmd,
       NEIGHBOR_CMD2 "allowas-in",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Accept as-path with my AS present in it\n")

DEFUNST (ip_extcommunity_list_expanded,
       ip_extcommunity_list_expanded_cmd,
       "ip extcommunity-list <100-199> (deny|permit) .LINE",
       IP_STR
       EXTCOMMUNITY_LIST_STR
       "Extended Community list number (expanded)\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       "An ordered list as a regular-expression\n")

DEFUNST (bgp_network_import_check,
       bgp_network_import_check_cmd,
       "bgp network import-check",
       "BGP specific commands\n"
       "BGP network command\n"
       "Check BGP network route exists in IGP\n")


DEFUNST (clear_ip_bgp_instance_all_ipv4_soft,
       clear_ip_bgp_instance_all_ipv4_soft_cmd,
       "clear ip bgp view WORD * ipv4 (unicast|multicast) soft",
       CLEAR_STR
       IP_STR
       BGP_STR
       "BGP view\n"
       "view name\n"
       "Clear all peers\n"
       "Address family\n"
       "Address Family Modifier\n"
       "Address Family Modifier\n"
       "Soft reconfig\n")

DEFUNST (bgp_log_neighbor_changes,
       bgp_log_neighbor_changes_cmd,
       "bgp log-neighbor-changes",
       "BGP specific commands\n"
       "Log neighbor up/down and reset reason\n")

DEFUNST (neighbor_unsuppress_map,
       neighbor_unsuppress_map_cmd,
       NEIGHBOR_CMD2 "unsuppress-map WORD",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Route-map to selectively unsuppress suppressed routes\n"
       "Name of route map\n")

DEFUNST (clear_ip_bgp_as_vpnv4_soft_in,
       clear_ip_bgp_as_vpnv4_soft_in_cmd,
       "clear ip bgp <1-65535> vpnv4 unicast soft in",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear peers with the AS number\n"
       "Address family\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig inbound update\n")

 DEFUNST (no_ip_community_list_name_standard,
       no_ip_community_list_name_standard_cmd,
       "no ip community-list standard WORD (deny|permit) .AA:NN",
       NO_STR
       IP_STR
       COMMUNITY_LIST_STR
       "Specify a standard community-list\n"
       "Community list name\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       COMMUNITY_VAL_STR)


DEFUNST (show_ip_extcommunity_list,
       show_ip_extcommunity_list_cmd,
       "show ip extcommunity-list",
       SHOW_STR
       IP_STR
       "List extended-community list\n")

DEFUNST (address_family_ipv4,
       address_family_ipv4_cmd,
       "address-family ipv4",
       "Enter Address Family command mode\n"
       "Address family\n")

DEFUNST (ip_extcommunity_list_name_expanded,
       ip_extcommunity_list_name_expanded_cmd,
       "ip extcommunity-list expanded WORD (deny|permit) .LINE",
       IP_STR
       EXTCOMMUNITY_LIST_STR
       "Specify expanded extcommunity-list\n"
       "Extended Community list name\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       "An ordered list as a regular-expression\n")
	
DEFUNST (no_ip_extcommunity_list_name_expanded,
       no_ip_extcommunity_list_name_expanded_cmd,
       "no ip extcommunity-list expanded WORD (deny|permit) .LINE",
       NO_STR
       IP_STR
       EXTCOMMUNITY_LIST_STR
       "Specify expanded extcommunity-list\n"
       "Community list name\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       "An ordered list as a regular-expression\n")


DEFUNST (clear_ip_bgp_peer_group_soft,
       clear_ip_bgp_peer_group_soft_cmd,
       "clear ip bgp peer-group WORD soft",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all members of peer-group\n"
       "BGP peer-group name\n"
       "Soft reconfig\n")

DEFUNST (clear_ip_bgp_external_ipv4_soft,
       clear_ip_bgp_external_ipv4_soft_cmd,
       "clear ip bgp external ipv4 (unicast|multicast) soft",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all external peers\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n")


DEFUNST (clear_ip_bgp_all_ipv4_soft,
       clear_ip_bgp_all_ipv4_soft_cmd,
       "clear ip bgp * ipv4 (unicast|multicast) soft",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all peers\n"
       "Address family\n"
       "Address Family Modifier\n"
       "Address Family Modifier\n"
       "Soft reconfig\n")

DEFUNST (no_neighbor_attr_unchanged2,
       no_neighbor_attr_unchanged2_cmd,
       NO_NEIGHBOR_CMD2 "attribute-unchanged as-path (next-hop|med)",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "BGP attribute is propagated unchanged to this neighbor\n"
       "As-path attribute\n"
       "Nexthop attribute\n"
       "Med attribute\n")


DEFUNST (ip_extcommunity_list_name_standard,
       ip_extcommunity_list_name_standard_cmd,
       "ip extcommunity-list standard WORD (deny|permit) .AA:NN",
       IP_STR
       EXTCOMMUNITY_LIST_STR
       "Specify standard extcommunity-list\n"
       "Extended Community list name\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       EXTCOMMUNITY_VAL_STR)

DEFUNST (no_neighbor_default_originate,
       no_neighbor_default_originate_cmd,
       NO_NEIGHBOR_CMD2 "default-originate",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Originate default route to this neighbor\n")


DEFUNST (clear_ip_bgp_peer_vpnv4_soft_out,
       clear_ip_bgp_peer_vpnv4_soft_out_cmd,
       "clear ip bgp A.B.C.D vpnv4 unicast soft out",
       CLEAR_STR
       IP_STR
       BGP_STR
       "BGP neighbor address to clear\n"
       "Address family\n"
       "Address Family Modifier\n"
       "Soft reconfig\n"
       "Soft reconfig outbound update\n")

DEFUNST (no_neighbor_port,
       no_neighbor_port_cmd,
       NO_NEIGHBOR_CMD "port",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR
       "Neighbor's BGP port\n")

DEFUNST (clear_ip_bgp_external_ipv4_soft_in,
       clear_ip_bgp_external_ipv4_soft_in_cmd,
       "clear ip bgp external ipv4 (unicast|multicast) soft in",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all external peers\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig inbound update\n")

DEFUNST (no_bgp_network_import_check,
       no_bgp_network_import_check_cmd,
       "no bgp network import-check",
       NO_STR
       "BGP specific commands\n"
       "BGP network command\n"
       "Check BGP network route exists in IGP\n")

DEFUNST (ip_community_list_name_standard,
       ip_community_list_name_standard_cmd,
       "ip community-list standard WORD (deny|permit) .AA:NN",
       IP_STR
       COMMUNITY_LIST_STR
       "Add a standard community-list entry\n"
       "Community list name\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       COMMUNITY_VAL_STR)

DEFUNST (clear_ip_bgp_as_vpnv4_soft,
       clear_ip_bgp_as_vpnv4_soft_cmd,
       "clear ip bgp <1-65535> vpnv4 unicast soft",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear peers with the AS number\n"
       "Address family\n"
       "Address Family Modifier\n"
       "Soft reconfig\n")


DEFUNST (no_neighbor_update_source,
       no_neighbor_update_source_cmd,
       NO_NEIGHBOR_CMD2 "update-source",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Source of routing updates\n"
       "Interface name\n")

DEFUNST (no_neighbor_peer_group_remote_as,
       no_neighbor_peer_group_remote_as_cmd,
       "no neighbor WORD remote-as <1-65535>",
       NO_STR
       NEIGHBOR_STR
       "Neighbor tag\n"
       "Specify a BGP neighbor\n"
       AS_STR)

DEFUNST (neighbor_capability_orf_prefix,
       neighbor_capability_orf_prefix_cmd,
       NEIGHBOR_CMD2 "capability orf prefix-list (both|send|receive)",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Advertise capability to the peer\n"
       "Advertise ORF capability to the peer\n"
       "Advertise prefixlist ORF capability to this neighbor\n"
       "Capability to SEND and RECEIVE the ORF to/from this neighbor\n"
       "Capability to RECEIVE the ORF from this neighbor\n"
       "Capability to SEND the ORF to this neighbor\n")

DEFUNST (address_family_ipv4_safi,
       address_family_ipv4_safi_cmd,
       "address-family ipv4 (unicast|multicast)",
       "Enter Address Family command mode\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n")

DEFUNST (clear_ip_bgp_instance_all_ipv4_soft_out,
       clear_ip_bgp_instance_all_ipv4_soft_out_cmd,
       "clear ip bgp view WORD * ipv4 (unicast|multicast) soft out",
       CLEAR_STR
       IP_STR
       BGP_STR
       "BGP view\n"
       "view name\n"
       "Clear all peers\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig outbound update\n")

DEFUNST (no_neighbor_unsuppress_map,
       no_neighbor_unsuppress_map_cmd,
       NO_NEIGHBOR_CMD2 "unsuppress-map WORD",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Route-map to selectively unsuppress suppressed routes\n"
       "Name of route map\n")


DEFUNST (no_synchronization,
       no_synchronization_cmd,
       "no synchronization",
       NO_STR
       "Perform IGP synchronization\n")

DEFUNST (no_ip_extcommunity_list_all,
       no_ip_extcommunity_list_all_cmd,
       "no ip extcommunity-list (<1-99>|<100-199>)",
       NO_STR
       IP_STR
       EXTCOMMUNITY_LIST_STR
       "Extended Community list number (standard)\n"
       "Extended Community list number (expanded)\n")


DEFUNST (clear_ip_bgp_external_in_prefix_filter,
       clear_ip_bgp_external_in_prefix_filter_cmd,
       "clear ip bgp external in prefix-filter",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all external peers\n"
       "Soft reconfig inbound update\n"
       "Push out prefix-list ORF and do inbound soft reconfig\n")

DEFUNST (clear_ip_bgp_external_soft_out,
       clear_ip_bgp_external_soft_out_cmd,
       "clear ip bgp external soft out",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all external peers\n"
       "Soft reconfig\n"
       "Soft reconfig outbound update\n")

DEFUNST (clear_ip_bgp_peer_ipv4_soft_in,
       clear_ip_bgp_peer_ipv4_soft_in_cmd,
       "clear ip bgp A.B.C.D ipv4 (unicast|multicast) soft in",
       CLEAR_STR
       IP_STR
       BGP_STR
       "BGP neighbor address to clear\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig inbound update\n")


DEFUNST (show_ip_community_list_arg,
       show_ip_community_list_arg_cmd,
       "show ip community-list (<1-199>|WORD)",
       SHOW_STR
       IP_STR
       "List community-list\n"
       "Community-list number\n"
       "Community-list name\n")

DEFUNST (neighbor_local_as,
       neighbor_local_as_cmd,
       NEIGHBOR_CMD2 "local-as <1-65535>",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Specify a local-as number\n"
       "AS number used as local AS\n")

DEFUNST (bgp_default_ipv4_unicast,
       bgp_default_ipv4_unicast_cmd,
       "bgp default ipv4-unicast",
       "BGP specific commands\n"
       "Configure BGP defaults\n"
       "Activate ipv4-unicast for a peer by default\n")

DEFUNST (no_bgp_default_ipv4_unicast,
       no_bgp_default_ipv4_unicast_cmd,
       "no bgp default ipv4-unicast",
       NO_STR
       "BGP specific commands\n"
       "Configure BGP defaults\n"
       "Activate ipv4-unicast for a peer by default\n")

DEFUNST (neighbor_distribute_list,
       neighbor_distribute_list_cmd,
       NEIGHBOR_CMD2 "distribute-list (<1-199>|<1300-2699>|WORD) (in|out)",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Filter updates to/from this neighbor\n"
       "IP access-list number\n"
       "IP access-list number (expanded range)\n"
       "IP Access-list name\n"
       "Filter incoming updates\n"
       "Filter outgoing updates\n")

DEFUNST (neighbor_maximum_prefix_warning,
       neighbor_maximum_prefix_warning_cmd,
       NEIGHBOR_CMD2 "maximum-prefix <1-4294967295> warning-only",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Maximum number of prefix accept from this peer\n"
       "maximum no. of prefix limit\n"
       "Only give warning message when limit is exceeded\n")


DEFUNST (ip_extcommunity_list_standard,
       ip_extcommunity_list_standard_cmd,
       "ip extcommunity-list <1-99> (deny|permit) .AA:NN",
       IP_STR
       EXTCOMMUNITY_LIST_STR
       "Extended Community list number (standard)\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       EXTCOMMUNITY_VAL_STR)

DEFUNST (clear_ip_bgp_peer_group_ipv4_soft_out,
       clear_ip_bgp_peer_group_ipv4_soft_out_cmd,
       "clear ip bgp peer-group WORD ipv4 (unicast|multicast) soft out",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all members of peer-group\n"
       "BGP peer-group name\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig outbound update\n")

DEFUNST (neighbor_route_server_client,
       neighbor_route_server_client_cmd,
       NEIGHBOR_CMD2 "route-server-client",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Configure a neighbor as Route Server client\n")


DEFUNST (no_ip_community_list_name_all,
       no_ip_community_list_name_all_cmd,
       "no ip community-list (standard|expanded) WORD",
       NO_STR
       IP_STR
       COMMUNITY_LIST_STR
       "Add a standard community-list entry\n"
       "Add an expanded community-list entry\n"
       "Community list name\n")

DEFUNST (clear_ip_bgp_peer_group_ipv4_soft,
       clear_ip_bgp_peer_group_ipv4_soft_cmd,
       "clear ip bgp peer-group WORD ipv4 (unicast|multicast) soft",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all members of peer-group\n"
       "BGP peer-group name\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n")

DEFUNST (clear_ip_bgp_as_in_prefix_filter,
       clear_ip_bgp_as_in_prefix_filter_cmd,
       "clear ip bgp <1-65535> in prefix-filter",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear peers with the AS number\n"
       "Soft reconfig inbound update\n"
       "Push out prefix-list ORF and do inbound soft reconfig\n")


DEFUNST (neighbor_attr_unchanged1,
       neighbor_attr_unchanged1_cmd,
       NEIGHBOR_CMD2 "attribute-unchanged (as-path|next-hop|med)",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "BGP attribute is propagated unchanged to this neighbor\n"
       "As-path attribute\n"
       "Nexthop attribute\n"
       "Med attribute\n")

DEFUNST (neighbor_default_originate,
       neighbor_default_originate_cmd,
       NEIGHBOR_CMD2 "default-originate",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Originate default route to this neighbor\n")

DEFUNST (no_neighbor_capability_orf_prefix,
       no_neighbor_capability_orf_prefix_cmd,
       NO_NEIGHBOR_CMD2 "capability orf prefix-list (both|send|receive)",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Advertise capability to the peer\n"
       "Advertise ORF capability to the peer\n"
       "Advertise prefixlist ORF capability to this neighbor\n"
       "Capability to SEND and RECEIVE the ORF to/from this neighbor\n"
       "Capability to RECEIVE the ORF from this neighbor\n"
       "Capability to SEND the ORF to this neighbor\n")

DEFUNST (no_neighbor_enforce_multihop,
       no_neighbor_enforce_multihop_cmd,
       NO_NEIGHBOR_CMD2 "enforce-multihop",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Enforce EBGP neighbors perform multihop\n")

DEFUNST (show_ip_bgp_instance_neighbors,
       show_ip_bgp_instance_neighbors_cmd,
       "show ip bgp view WORD neighbors",
       SHOW_STR
       IP_STR
       BGP_STR
       "BGP view\n"
       "View name\n"
       "Detailed information on TCP and BGP neighbor connections\n")

DEFUNST (clear_ip_bgp_all_ipv4_soft_out,
       clear_ip_bgp_all_ipv4_soft_out_cmd,
       "clear ip bgp * ipv4 (unicast|multicast) soft out",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all peers\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig outbound update\n")

DEFUNST (show_ip_bgp_instance_neighbors_peer,
       show_ip_bgp_instance_neighbors_peer_cmd,
       "show ip bgp view WORD neighbors (A.B.C.D|X:X::X:X)",
       SHOW_STR
       IP_STR
       BGP_STR
       "BGP view\n"
       "View name\n"
       "Detailed information on TCP and BGP neighbor connections\n"
       "Neighbor to display information about\n"
       "Neighbor to display information about\n")


DEFUNST (no_neighbor_attr_unchanged1,
       no_neighbor_attr_unchanged1_cmd,
       NO_NEIGHBOR_CMD2 "attribute-unchanged (as-path|next-hop|med)",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "BGP attribute is propagated unchanged to this neighbor\n"
       "As-path attribute\n"
       "Nexthop attribute\n"
       "Med attribute\n")

DEFUNST (neighbor_local_as_no_prepend,
       neighbor_local_as_no_prepend_cmd,
       NEIGHBOR_CMD2 "local-as <1-65535> no-prepend",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Specify a local-as number\n"
       "AS number used as local AS\n"
       "Do not prepend local-as to updates from ebgp peers\n")

DEFUNST (show_ip_bgp_instance_ipv4_summary,
       show_ip_bgp_instance_ipv4_summary_cmd,
       "show ip bgp view WORD ipv4 (unicast|multicast) summary",
       SHOW_STR
       IP_STR
       BGP_STR
       "BGP view\n"
       "View name\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Summary of BGP neighbor status\n")

DEFUNST (no_ip_extcommunity_list_standard,
       no_ip_extcommunity_list_standard_cmd,
       "no ip extcommunity-list <1-99> (deny|permit) .AA:NN",
       NO_STR
       IP_STR
       EXTCOMMUNITY_LIST_STR
       "Extended Community list number (standard)\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       EXTCOMMUNITY_VAL_STR)

DEFUNST (neighbor_attr_unchanged,
       neighbor_attr_unchanged_cmd,
       NEIGHBOR_CMD2 "attribute-unchanged",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "BGP attribute is propagated unchanged to this neighbor\n")

DEFUNST (no_neighbor_attr_unchanged4,
       no_neighbor_attr_unchanged4_cmd,
       NO_NEIGHBOR_CMD2 "attribute-unchanged med (as-path|next-hop)",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "BGP attribute is propagated unchanged to this neighbor\n"
       "Med attribute\n"
       "As-path attribute\n"
       "Nexthop attribute\n")

DEFUNST (clear_ip_bgp_peer_group,
       clear_ip_bgp_peer_group_cmd,
       "clear ip bgp peer-group WORD",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all members of peer-group\n"
       "BGP peer-group name\n")

DEFUNST (neighbor_port,
       neighbor_port_cmd,
       NEIGHBOR_CMD "port <0-65535>",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR
       "Neighbor's BGP port\n"
       "TCP port number\n")

DEFUNST (clear_ip_bgp_peer_in_prefix_filter,
       clear_ip_bgp_peer_in_prefix_filter_cmd,
       "clear ip bgp A.B.C.D in prefix-filter",
       CLEAR_STR
       IP_STR
       BGP_STR
       "BGP neighbor address to clear\n"
       "Soft reconfig inbound update\n"
       "Push out the existing ORF prefix-list\n")

DEFUNST (bgp_multiple_instance_func,
       bgp_multiple_instance_cmd,
       "bgp multiple-instance",
       BGP_STR
       "Enable bgp multiple instance\n")

DEFUNST (no_bgp_fast_external_failover,
       no_bgp_fast_external_failover_cmd,
       "no bgp fast-external-failover",
       NO_STR
       BGP_STR
       "Immediately reset session if a link to a directly connected external peer goes down\n")

DEFUNST (clear_ip_bgp_all_in_prefix_filter,
       clear_ip_bgp_all_in_prefix_filter_cmd,
       "clear ip bgp * in prefix-filter",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all peers\n"
       "Soft reconfig inbound update\n"
       "Push out prefix-list ORF and do inbound soft reconfig\n")

DEFUNST (no_ip_community_list_expanded,
       no_ip_community_list_expanded_cmd,
       "no ip community-list <100-199> (deny|permit) .LINE",
       NO_STR
       IP_STR
       COMMUNITY_LIST_STR
       "Community list number (expanded)\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       "An ordered list as a regular-expression\n")


DEFUNST (clear_ip_bgp_all_vpnv4_soft,
       clear_ip_bgp_all_vpnv4_soft_cmd,
       "clear ip bgp * vpnv4 unicast soft",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all peers\n"
       "Address family\n"
       "Address Family Modifier\n"
       "Soft reconfig\n")


DEFUNST (no_neighbor_attr_unchanged,
       no_neighbor_attr_unchanged_cmd,
       NO_NEIGHBOR_CMD2 "attribute-unchanged",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "BGP attribute is propagated unchanged to this neighbor\n")

DEFUNST (clear_ip_bgp_peer_group_ipv4_in_prefix_filter,
       clear_ip_bgp_peer_group_ipv4_in_prefix_filter_cmd,
       "clear ip bgp peer-group WORD ipv4 (unicast|multicast) in prefix-filter",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all members of peer-group\n"
       "BGP peer-group name\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig inbound update\n"
       "Push out prefix-list ORF and do inbound soft reconfig\n")

DEFUNST (clear_ip_bgp_all_vpnv4_soft_in,
       clear_ip_bgp_all_vpnv4_soft_in_cmd,
       "clear ip bgp * vpnv4 unicast soft in",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all peers\n"
       "Address family\n"
       "Address Family Modifier\n"
       "Soft reconfig\n"
       "Soft reconfig inbound update\n")

DEFUNST (neighbor_capability_dynamic,
       neighbor_capability_dynamic_cmd,
       NEIGHBOR_CMD2 "capability dynamic",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Advertise capability to the peer\n"
       "Advertise dynamic capability to this neighbor\n")

DEFUNST (ip_community_list_name_expanded,
       ip_community_list_name_expanded_cmd,
       "ip community-list expanded WORD (deny|permit) .LINE",
       IP_STR
       COMMUNITY_LIST_STR
       "Add an expanded community-list entry\n"
       "Community list name\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       "An ordered list as a regular-expression\n")

DEFUNST (clear_ip_bgp_peer_group_soft_in,
       clear_ip_bgp_peer_group_soft_in_cmd,
       "clear ip bgp peer-group WORD soft in",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all members of peer-group\n"
       "BGP peer-group name\n"
       "Soft reconfig\n"
       "Soft reconfig inbound update\n")

DEFUNST (show_ip_community_list,
       show_ip_community_list_cmd,
       "show ip community-list",
       SHOW_STR
       IP_STR
       "List community-list\n")

DEFUNST (clear_ip_bgp_peer_group_in_prefix_filter,
       clear_ip_bgp_peer_group_in_prefix_filter_cmd,
       "clear ip bgp peer-group WORD in prefix-filter",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all members of peer-group\n"
       "BGP peer-group name\n"
       "Soft reconfig inbound update\n"
       "Push out prefix-list ORF and do inbound soft reconfig\n")

DEFUNST (neighbor_attr_unchanged2,
       neighbor_attr_unchanged2_cmd,
       NEIGHBOR_CMD2 "attribute-unchanged as-path (next-hop|med)",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "BGP attribute is propagated unchanged to this neighbor\n"
       "As-path attribute\n"
       "Nexthop attribute\n"
       "Med attribute\n")

DEFUNST (clear_ip_bgp_peer_ipv4_in_prefix_filter,
       clear_ip_bgp_peer_ipv4_in_prefix_filter_cmd,
       "clear ip bgp A.B.C.D ipv4 (unicast|multicast) in prefix-filter",
       CLEAR_STR
       IP_STR
       BGP_STR
       "BGP neighbor address to clear\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig inbound update\n"
       "Push out the existing ORF prefix-list\n")

DEFUNST (neighbor_peer_group,
       neighbor_peer_group_cmd,
       "neighbor WORD peer-group",
       NEIGHBOR_STR
       "Neighbor tag\n"
       "Configure peer-group\n")


DEFUNST (ip_community_list_expanded,
       ip_community_list_expanded_cmd,
       "ip community-list <100-199> (deny|permit) .LINE",
       IP_STR
       COMMUNITY_LIST_STR
       "Community list number (expanded)\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       "An ordered list as a regular-expression\n")

DEFUNST (bgp_fast_external_failover,
       bgp_fast_external_failover_cmd,
       "bgp fast-external-failover",
       BGP_STR
       "Immediately reset session if a link to a directly connected external peer goes down\n")


DEFUNST (neighbor_maximum_prefix_threshold,
       neighbor_maximum_prefix_threshold_cmd,
       NEIGHBOR_CMD2 "maximum-prefix <1-4294967295> <1-100>",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Maximum number of prefix accept from this peer\n"
       "maximum no. of prefix limit\n"
       "Threshold value (%) at which to generate a warning msg\n")

DEFUNST (clear_ip_bgp_all_ipv4_soft_in,
       clear_ip_bgp_all_ipv4_soft_in_cmd,
       "clear ip bgp * ipv4 (unicast|multicast) soft in",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all peers\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig inbound update\n")

DEFUNST (clear_ip_bgp_as_ipv4_in_prefix_filter,
       clear_ip_bgp_as_ipv4_in_prefix_filter_cmd,
       "clear ip bgp <1-65535> ipv4 (unicast|multicast) in prefix-filter",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear peers with the AS number\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig inbound update\n"
       "Push out prefix-list ORF and do inbound soft reconfig\n")

DEFUNST (clear_ip_bgp_instance_all_ipv4_soft_in,
       clear_ip_bgp_instance_all_ipv4_soft_in_cmd,
       "clear ip bgp view WORD * ipv4 (unicast|multicast) soft in",
       CLEAR_STR
       IP_STR
       BGP_STR
       "BGP view\n"
       "view name\n"
       "Clear all peers\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig inbound update\n")

DEFUNST (no_ip_community_list_name_expanded,
       no_ip_community_list_name_expanded_cmd,
       "no ip community-list expanded WORD (deny|permit) .LINE",
       NO_STR
       IP_STR
       COMMUNITY_LIST_STR
       "Specify an expanded community-list\n"
       "Community list name\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       "An ordered list as a regular-expression\n")

DEFUNST (clear_ip_bgp_peer_ipv4_soft,
       clear_ip_bgp_peer_ipv4_soft_cmd,
       "clear ip bgp A.B.C.D ipv4 (unicast|multicast) soft",
       CLEAR_STR
       IP_STR
       BGP_STR
       "BGP neighbor address to clear\n"
       "Address family\n"
       "Address Family Modifier\n"
       "Address Family Modifier\n"
       "Soft reconfig\n")

DEFUNST (clear_ip_bgp_external_ipv4_soft_out,
       clear_ip_bgp_external_ipv4_soft_out_cmd,
       "clear ip bgp external ipv4 (unicast|multicast) soft out",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all external peers\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig outbound update\n")

DEFUNST (no_ip_extcommunity_list_name_standard,
       no_ip_extcommunity_list_name_standard_cmd,
       "no ip extcommunity-list standard WORD (deny|permit) .AA:NN",
       NO_STR
       IP_STR
       EXTCOMMUNITY_LIST_STR
       "Specify standard extcommunity-list\n"
       "Extended Community list name\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       EXTCOMMUNITY_VAL_STR)

DEFUNST (no_neighbor_peer_group,
       no_neighbor_peer_group_cmd,
       "no neighbor WORD peer-group",
       NO_STR
       NEIGHBOR_STR
       "Neighbor tag\n"
       "Configure peer-group\n")

DEFUNST (clear_ip_bgp_peer_ipv4_soft_out,
       clear_ip_bgp_peer_ipv4_soft_out_cmd,
       "clear ip bgp A.B.C.D ipv4 (unicast|multicast) soft out",
       CLEAR_STR
       IP_STR
       BGP_STR
       "BGP neighbor address to clear\n"
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig outbound update\n")

DEFUNST (no_neighbor_capability_dynamic,
       no_neighbor_capability_dynamic_cmd,
       NO_NEIGHBOR_CMD2 "capability dynamic",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Advertise capability to the peer\n"
       "Advertise dynamic capability to this neighbor\n")


DEFUNST (neighbor_default_originate_rmap,
       neighbor_default_originate_rmap_cmd,
       NEIGHBOR_CMD2 "default-originate route-map WORD",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Originate default route to this neighbor\n"
       "Route-map to specify criteria to originate default\n"
       "route-map name\n")

DEFUNST (clear_ip_bgp_external_soft_in,
       clear_ip_bgp_external_soft_in_cmd,
       "clear ip bgp external soft in",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear all external peers\n"
       "Soft reconfig\n"
       "Soft reconfig inbound update\n")

DEFUNST (neighbor_maximum_prefix_threshold_warning,
       neighbor_maximum_prefix_threshold_warning_cmd,
       NEIGHBOR_CMD2 "maximum-prefix <1-4294967295> <1-100> warning-only",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Maximum number of prefix accept from this peer\n"
       "maximum no. of prefix limit\n"
       "Threshold value (%) at which to generate a warning msg\n"
       "Only give warning message when limit is exceeded\n")

DEFUNST (neighbor_enforce_multihop,
       neighbor_enforce_multihop_cmd,
       NEIGHBOR_CMD2 "enforce-multihop",
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Enforce EBGP neighbors perform multihop\n")

DEFUNST (no_neighbor_route_server_client,
       no_neighbor_route_server_client_cmd,
       NO_NEIGHBOR_CMD2 "route-server-client",
       NO_STR
       NEIGHBOR_STR
       NEIGHBOR_ADDR_STR2
       "Configure a neighbor as Route Server client\n")

DEFUNST (clear_ip_bgp_peer_vpnv4_soft,
       clear_ip_bgp_peer_vpnv4_soft_cmd,
       "clear ip bgp A.B.C.D vpnv4 unicast soft",
       CLEAR_STR
       IP_STR
       BGP_STR
       "BGP neighbor address to clear\n"
       "Address family\n"
       "Address Family Modifier\n"
       "Soft reconfig\n")

DEFUNST (no_ip_community_list_standard,
       no_ip_community_list_standard_cmd,
       "no ip community-list <1-99> (deny|permit) .AA:NN",
       NO_STR
       IP_STR
       COMMUNITY_LIST_STR
       "Community list number (standard)\n"
       "Specify community to reject\n"
       "Specify community to accept\n"
       COMMUNITY_VAL_STR)


DEFUNST (clear_ip_bgp_as_vpnv4_soft_out,
       clear_ip_bgp_as_vpnv4_soft_out_cmd,
       "clear ip bgp <1-65535> vpnv4 unicast soft out",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear peers with the AS number\n"
       "Address family\n"
       "Address Family modifier\n"
       "Soft reconfig\n"
       "Soft reconfig outbound update\n")


DEFUNST (show_ip_bgp_instance_summary,
       show_ip_bgp_instance_summary_cmd,
       "show ip bgp view WORD summary",
       SHOW_STR
       IP_STR
       BGP_STR
       "BGP view\n"
       "View name\n"
       "Summary of BGP neighbor status\n")

  
DEFUNST (set_vpnv4_nexthop,
       set_vpnv4_nexthop_cmd,
       "set vpnv4 next-hop A.B.C.D",
       SET_STR
       "VPNv4 information\n"
       "VPNv4 next-hop address\n"
       "IP address of next hop\n")

DEFUNST (access_list_standard_any,
       access_list_standard_any_cmd,
       "access-list (<1-99>|<1300-1999>) (deny|permit) any",
       "Add an access list entry\n"
       "IP standard access list\n"
       "IP standard access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any source host\n")
  
DEFUNST (show_ip_bgp_ipv4_prefix,
       show_ip_bgp_ipv4_prefix_cmd,
       "show ip bgp ipv4 (unicast|multicast) A.B.C.D/M",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n")

DEFUNST (show_ip_bgp_flap_prefix,
       show_ip_bgp_flap_prefix_cmd,
       "show ip bgp flap-statistics A.B.C.D/M",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display flap statistics of routes\n"
       "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n")

DEFUNST (no_access_list_extended_mask_host,
       no_access_list_extended_mask_host_cmd,
       "no access-list (<100-199>|<2000-2699>) (deny|permit) ip A.B.C.D A.B.C.D host A.B.C.D",
       NO_STR
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "Source address\n"
       "Source wildcard bits\n"
       "A single destination host\n"
       "Destination address\n")

DEFUNST (no_access_list_extended_host_any,
       no_access_list_extended_host_any_cmd,
       "no access-list (<100-199>|<2000-2699>) (deny|permit) ip host A.B.C.D any",
       NO_STR
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "A single source host\n"
       "Source address\n"
       "Any destination host\n")

DEFUNST (show_ip_bgp_neighbor_damp,
       show_ip_bgp_neighbor_damp_cmd,
       "show ip bgp neighbors (A.B.C.D|X:X::X:X) dampened-routes",
       SHOW_STR
       IP_STR
       BGP_STR
       "Detailed information on TCP and BGP neighbor connections\n"
       "Neighbor to display information about\n"
       "Neighbor to display information about\n"
       "Display the dampened routes received from neighbor\n")

DEFUNST (clear_ip_bgp_dampening_address,
       clear_ip_bgp_dampening_address_cmd,
       "clear ip bgp dampening A.B.C.D",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear route flap dampening information\n"
       "Network to clear damping information\n")



DEFUNST (no_match_ecommunity,
       no_match_ecommunity_cmd,
       "no match extcommunity",
       NO_STR
       MATCH_STR
       "Match BGP/VPN extended community list\n")

DEFUNST (show_ip_bgp_ipv4_prefix_list,
       show_ip_bgp_ipv4_prefix_list_cmd,
       "show ip bgp ipv4 (unicast|multicast) prefix-list WORD",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Display routes conforming to the prefix-list\n"
       "IP prefix-list name\n")

DEFUNST (bgp_network_mask_route_map,
       bgp_network_mask_route_map_cmd,
       "network A.B.C.D mask A.B.C.D route-map WORD",
       "Specify a network to announce via BGP\n"
       "Network number\n"
       "Network mask\n"
       "Network mask\n"
       "Route-map to modify the attributes\n"
       "Name of the route map\n")

DEFUNST (match_community_exact,
       match_community_exact_cmd,
       "match community (<1-99>|<100-199>|WORD) exact-match",
       MATCH_STR
       "Match BGP community list\n"
       "Community-list number (standard)\n"
       "Community-list number (expanded)\n"
       "Community-list name\n"
       "Do exact matching of communities\n")

DEFUNST (bgp_network_mask_backdoor,
       bgp_network_mask_backdoor_cmd,
       "network A.B.C.D mask A.B.C.D backdoor",
       "Specify a network to announce via BGP\n"
       "Network number\n"
       "Network mask\n"
       "Network mask\n"
       "Specify a BGP backdoor route\n")

DEFUNST (access_list_extended_mask_host,
       access_list_extended_mask_host_cmd,
       "access-list (<100-199>|<2000-2699>) (deny|permit) ip A.B.C.D A.B.C.D host A.B.C.D",
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "Source address\n"
       "Source wildcard bits\n"
       "A single destination host\n"
       "Destination address\n")


DEFUNST (show_ip_bgp_vpnv4_all_route,
       show_ip_bgp_vpnv4_all_route_cmd,
       "show ip bgp vpnv4 all A.B.C.D",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display VPNv4 NLRI specific information\n"
       "Display information about all VPNv4 NLRIs\n"
       "Network in the BGP routing table to display\n")


DEFUNST (show_ip_bgp_ipv4,
       show_ip_bgp_ipv4_cmd,
       "show ip bgp ipv4 (unicast|multicast)",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n")

DEFUNST (no_bgp_network_mask_natural,
       no_bgp_network_mask_natural_cmd,
       "no network A.B.C.D",
       NO_STR
       "Specify a network to announce via BGP\n"
       "Network number\n")

DEFUNST (match_ecommunity,
       match_ecommunity_cmd,
       "match extcommunity (<1-99>|<100-199>|WORD)",
       MATCH_STR
       "Match BGP/VPN extended community list\n"
       "Extended community-list number (standard)\n"
       "Extended community-list number (expanded)\n"
       "Extended community-list name\n")

  DEFUNST (show_ip_bgp_ipv4_regexp,
       show_ip_bgp_ipv4_regexp_cmd,
       "show ip bgp ipv4 (unicast|multicast) regexp .LINE",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Display routes matching the AS path regular expression\n"
       "A regular-expression to match the BGP AS paths\n")

  DEFUNST (show_ip_bgp_ipv4_cidr_only,
       show_ip_bgp_ipv4_cidr_only_cmd,
       "show ip bgp ipv4 (unicast|multicast) cidr-only",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Display only routes with non-natural netmasks\n")


DEFUNST (show_ip_access_list,
       show_ip_access_list_cmd,
       "show ip access-list",
       SHOW_STR
       IP_STR
       "List IP access lists\n")

  
DEFUNST (access_list_extended,
       access_list_extended_cmd,
       "access-list (<100-199>|<2000-2699>) (deny|permit) ip A.B.C.D A.B.C.D A.B.C.D A.B.C.D",
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "Source address\n"
       "Source wildcard bits\n"
       "Destination address\n"
       "Destination Wildcard bits\n")
DEFUNST (show_ip_bgp_ipv4_route_map,
       show_ip_bgp_ipv4_route_map_cmd,
       "show ip bgp ipv4 (unicast|multicast) route-map WORD",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Display routes matching the route-map\n"
       "A route-map to match on\n")


DEFUNST (access_list_extended_any_host,
       access_list_extended_any_host_cmd,
       "access-list (<100-199>|<2000-2699>) (deny|permit) ip any host A.B.C.D",
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "Any source host\n"
       "A single destination host\n"
       "Destination address\n")

DEFUNST (no_access_list_extended_mask_any,
       no_access_list_extended_mask_any_cmd,
       "no access-list (<100-199>|<2000-2699>) (deny|permit) ip A.B.C.D A.B.C.D any",
       NO_STR
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "Source address\n"
       "Source wildcard bits\n"
       "Any destination host\n")

DEFUNST (access_list_any,
       access_list_any_cmd,
       "access-list WORD (deny|permit) any",
       "Add an access list entry\n"
       "IP zebra access-list name\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Prefix to match. e.g. 10.0.0.0/8\n")

  
 DEFUNST (show_ip_bgp_flap_cidr_only,
       show_ip_bgp_flap_cidr_only_cmd,
       "show ip bgp flap-statistics cidr-only",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display flap statistics of routes\n"
       "Display only routes with non-natural netmasks\n")


DEFUNST (show_ip_bgp_dampened_paths,
       show_ip_bgp_dampened_paths_cmd,
       "show ip bgp dampened-paths",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display paths suppressed due to dampening\n")

DEFUNST (show_ip_bgp_flap_prefix_list,
       show_ip_bgp_flap_prefix_list_cmd,
       "show ip bgp flap-statistics prefix-list WORD",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display flap statistics of routes\n"
       "Display routes conforming to the prefix-list\n"
       "IP prefix-list name\n")

DEFUNST (show_ip_bgp_flap_regexp,
       show_ip_bgp_flap_regexp_cmd,
       "show ip bgp flap-statistics regexp .LINE",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display flap statistics of routes\n"
       "Display routes matching the AS path regular expression\n"
       "A regular-expression to match the BGP AS paths\n")

DEFUNST (no_match_origin,
       no_match_origin_cmd,
       "no match origin",
       NO_STR
       MATCH_STR
       "BGP origin code\n")


DEFUNST (access_list_standard,
       access_list_standard_cmd,
       "access-list (<1-99>|<1300-1999>) (deny|permit) A.B.C.D A.B.C.D",
       "Add an access list entry\n"
       "IP standard access list\n"
       "IP standard access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Address to match\n"
       "Wildcard bits\n")

DEFUNST (no_access_list_extended_host_host,
       no_access_list_extended_host_host_cmd,
       "no access-list (<100-199>|<2000-2699>) (deny|permit) ip host A.B.C.D host A.B.C.D",
       NO_STR
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "A single source host\n"
       "Source address\n"
       "A single destination host\n"
       "Destination address\n")

DEFUNST (access_list_extended_mask_any,
       access_list_extended_mask_any_cmd,
       "access-list (<100-199>|<2000-2699>) (deny|permit) ip A.B.C.D A.B.C.D any",
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "Source address\n"
       "Source wildcard bits\n"
       "Any destination host\n")
DEFUNST (show_ip_bgp_flap_address,
       show_ip_bgp_flap_address_cmd,
       "show ip bgp flap-statistics A.B.C.D",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display flap statistics of routes\n"
       "Network in the BGP routing table to display\n")

DEFUNST (show_ip_bgp_neighbor_flap,
       show_ip_bgp_neighbor_flap_cmd,
       "show ip bgp neighbors (A.B.C.D|X:X::X:X) flap-statistics",
       SHOW_STR
       IP_STR
       BGP_STR
       "Detailed information on TCP and BGP neighbor connections\n"
       "Neighbor to display information about\n"
       "Neighbor to display information about\n"
       "Display flap statistics of the routes learned from neighbor\n")

DEFUNST (show_ip_bgp_view_route,
       show_ip_bgp_view_route_cmd,
       "show ip bgp view WORD A.B.C.D",
       SHOW_STR
       IP_STR
       BGP_STR
       "BGP view\n"
       "BGP view name\n"
       "Network in the BGP routing table to display\n")

  
DEFUNST (access_list_extended_host_host,
       access_list_extended_host_host_cmd,
       "access-list (<100-199>|<2000-2699>) (deny|permit) ip host A.B.C.D host A.B.C.D",
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "A single source host\n"
       "Source address\n"
       "A single destination host\n"
       "Destination address\n")

DEFUNST (show_ip_bgp_ipv4_community_list,
       show_ip_bgp_ipv4_community_list_cmd,
       "show ip bgp ipv4 (unicast|multicast) community-list WORD",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Display routes matching the community-list\n"
       "community-list name\n")

DEFUNST (show_ip_bgp_ipv4_neighbor_routes,
       show_ip_bgp_ipv4_neighbor_routes_cmd,
       "show ip bgp ipv4 (unicast|multicast) neighbors (A.B.C.D|X:X::X:X) routes",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Detailed information on TCP and BGP neighbor connections\n"
       "Neighbor to display information about\n"
       "Neighbor to display information about\n"
       "Display routes learned from neighbor\n")

DEFUNST (show_ip_bgp_vpnv4_all_prefix,
       show_ip_bgp_vpnv4_all_prefix_cmd,
       "show ip bgp vpnv4 all A.B.C.D/M",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display VPNv4 NLRI specific information\n"
       "Display information about all VPNv4 NLRIs\n"
       "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n")

DEFUNST (no_access_list_extended,
       no_access_list_extended_cmd,
       "no access-list (<100-199>|<2000-2699>) (deny|permit) ip A.B.C.D A.B.C.D A.B.C.D A.B.C.D",
       NO_STR
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "Source address\n"
       "Source wildcard bits\n"
       "Destination address\n"
       "Destination Wildcard bits\n")


DEFUNST (bgp_network_mask_natural,
       bgp_network_mask_natural_cmd,
       "network A.B.C.D",
       "Specify a network to announce via BGP\n"
       "Network number\n")

DEFUNST (bgp_network_route_map,
       bgp_network_route_map_cmd,
       "network A.B.C.D/M route-map WORD",
       "Specify a network to announce via BGP\n"
       "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
       "Route-map to modify the attributes\n"
       "Name of the route map\n")

DEFUNST (show_ip_bgp_ipv4_community,
       show_ip_bgp_ipv4_community_cmd,
       "show ip bgp ipv4 (unicast|multicast) community (AA:NN|local-AS|no-advertise|no-export)",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Display routes matching the communities\n"
       "community number\n"
       "Do not send outside local AS (well-known community)\n"
       "Do not advertise to any peer (well-known community)\n"
       "Do not export to next AS (well-known community)\n")

DEFUNST (show_ip_bgp_ipv4_neighbor_received_routes,
       show_ip_bgp_ipv4_neighbor_received_routes_cmd,
       "show ip bgp ipv4 (unicast|multicast) neighbors (A.B.C.D|X:X::X:X) received-routes",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Detailed information on TCP and BGP neighbor connections\n"
       "Neighbor to display information about\n"
       "Neighbor to display information about\n"
       "Display the received routes from neighbor\n")

DEFUNST (show_ip_bgp_ipv4_prefix_longer,
       show_ip_bgp_ipv4_prefix_longer_cmd,
       "show ip bgp ipv4 (unicast|multicast) A.B.C.D/M longer-prefixes",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
       "Display route and more specific routes\n")

DEFUNST (access_list_extended_host_mask,
       access_list_extended_host_mask_cmd,
       "access-list (<100-199>|<2000-2699>) (deny|permit) ip host A.B.C.D A.B.C.D A.B.C.D",
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "A single source host\n"
       "Source address\n"
       "Destination address\n"
       "Destination Wildcard bits\n")

DEFUNST (show_ip_as_path_access_list_all,
       show_ip_as_path_access_list_all_cmd,
       "show ip as-path-access-list",
       SHOW_STR
       IP_STR
       "List AS path access lists\n")



DEFUNST (show_ip_bgp_view,
       show_ip_bgp_view_cmd,
       "show ip bgp view WORD",
       SHOW_STR
       IP_STR
       BGP_STR
       "BGP view\n"
       "BGP view name\n")


DEFUNST (show_ip_bgp_flap_route_map,
       show_ip_bgp_flap_route_map_cmd,
       "show ip bgp flap-statistics route-map WORD",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display flap statistics of routes\n"
       "Display routes matching the route-map\n"
       "A route-map to match on\n")

DEFUNST (set_community_none,
       set_community_none_cmd,
       "set community none",
       SET_STR
       "BGP community attribute\n"
       "No community attribute\n")


DEFUNST (show_ip_bgp_ipv4_community_all,
       show_ip_bgp_ipv4_community_all_cmd,
       "show ip bgp ipv4 (unicast|multicast) community",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Display routes matching the communities\n")

DEFUNST (show_ip_bgp_view_prefix,
       show_ip_bgp_view_prefix_cmd,
       "show ip bgp view WORD A.B.C.D/M",
       SHOW_STR
       IP_STR
       BGP_STR
       "BGP view\n"
       "BGP view name\n"
       "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n")

DEFUNST (access_list_extended_any_any,
       access_list_extended_any_any_cmd,
       "access-list (<100-199>|<2000-2699>) (deny|permit) ip any any",
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "Any source host\n"
       "Any destination host\n")

DEFUNST (show_ip_access_list_name,
       show_ip_access_list_name_cmd,
       "show ip access-list (<1-99>|<100-199>|<1300-1999>|<2000-2699>|WORD)",
       SHOW_STR
       IP_STR
       "List IP access lists\n"
       "IP standard access list\n"
       "IP extended access list\n"
       "IP standard access list (expanded range)\n"
       "IP extended access list (expanded range)\n"
       "IP zebra access-list\n")
DEFUNST (show_ip_bgp_flap_prefix_longer,
       show_ip_bgp_flap_prefix_longer_cmd,
       "show ip bgp flap-statistics A.B.C.D/M longer-prefixes",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display flap statistics of routes\n"
       "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
       "Display route and more specific routes\n")

DEFUNST (show_ip_bgp_ipv4_neighbor_advertised_route,
       show_ip_bgp_ipv4_neighbor_advertised_route_cmd,
       "show ip bgp ipv4 (unicast|multicast) neighbors (A.B.C.D|X:X::X:X) advertised-routes",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Detailed information on TCP and BGP neighbor connections\n"
       "Neighbor to display information about\n"
       "Neighbor to display information about\n"
       "Display the routes advertised to a BGP neighbor\n")

DEFUNST (access_list_exact,
       access_list_exact_cmd,
       "access-list WORD (deny|permit) A.B.C.D/M exact-match",
       "Add an access list entry\n"
       "IP zebra access-list name\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Prefix to match. e.g. 10.0.0.0/8\n"
       "Exact match of the prefixes\n")
DEFUNST (show_ip_bgp_flap_statistics,
       show_ip_bgp_flap_statistics_cmd,
       "show ip bgp flap-statistics",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display flap statistics of routes\n")

DEFUNST (match_origin,
       match_origin_cmd,
       "match origin (egp|igp|incomplete)",
       MATCH_STR
       "BGP origin code\n"
       "remote EGP\n"
       "local IGP\n"
       "unknown heritage\n")

DEFUNST (show_ip_bgp_ipv4_route,
       show_ip_bgp_ipv4_route_cmd,
       "show ip bgp ipv4 (unicast|multicast) A.B.C.D",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Network in the BGP routing table to display\n")
DEFUNST (clear_ip_bgp_dampening_address_mask,
       clear_ip_bgp_dampening_address_mask_cmd,
       "clear ip bgp dampening A.B.C.D A.B.C.D",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear route flap dampening information\n"
       "Network to clear damping information\n"
       "Network mask\n")

DEFUNST (access_list_standard_host,
       access_list_standard_host_cmd,
       "access-list (<1-99>|<1300-1999>) (deny|permit) host A.B.C.D",
       "Add an access list entry\n"
       "IP standard access list\n"
       "IP standard access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "A single host address\n"
       "Address to match\n")



DEFUNST (show_ip_bgp_ipv4_filter_list,
       show_ip_bgp_ipv4_filter_list_cmd,
       "show ip bgp ipv4 (unicast|multicast) filter-list WORD",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Display routes conforming to the filter-list\n"
       "Regular expression access list name\n")


DEFUNST (show_ip_bgp_route_map,
       show_ip_bgp_route_map_cmd,
       "show ip bgp route-map WORD",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display routes matching the route-map\n"
       "A route-map to match on\n")


DEFUNST (no_access_list_exact,
       no_access_list_exact_cmd,
       "no access-list WORD (deny|permit) A.B.C.D/M exact-match",
       NO_STR
       "Add an access list entry\n"
       "IP zebra access-list name\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Prefix to match. e.g. 10.0.0.0/8\n"
       "Exact match of the prefixes\n")

  DEFUNST (show_ip_bgp_ipv4_neighbor_received_prefix_filter,
       show_ip_bgp_ipv4_neighbor_received_prefix_filter_cmd,
       "show ip bgp ipv4 (unicast|multicast) neighbors (A.B.C.D|X:X::X:X) received prefix-filter",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Detailed information on TCP and BGP neighbor connections\n"
       "Neighbor to display information about\n"
       "Neighbor to display information about\n"
       "Display information received from a BGP neighbor\n"
       "Display the prefixlist filter\n")

DEFUNST (no_access_list_standard_any,
       no_access_list_standard_any_cmd,
       "no access-list (<1-99>|<1300-1999>) (deny|permit) any",
       NO_STR
       "Add an access list entry\n"
       "IP standard access list\n"
       "IP standard access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any source host\n")

DEFUNST (no_access_list_extended_host_mask,
       no_access_list_extended_host_mask_cmd,
       "no access-list (<100-199>|<2000-2699>) (deny|permit) ip host A.B.C.D A.B.C.D A.B.C.D",
       NO_STR
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "A single source host\n"
       "Source address\n"
       "Destination address\n"
       "Destination Wildcard bits\n")


DEFUNST (no_access_list_extended_any_host,
       no_access_list_extended_any_host_cmd,
       "no access-list (<100-199>|<2000-2699>) (deny|permit) ip any host A.B.C.D",
       NO_STR
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "Any source host\n"
       "A single destination host\n"
       "Destination address\n")

DEFUNST (no_access_list_standard_nomask,
       no_access_list_standard_nomask_cmd,
       "no access-list (<1-99>|<1300-1999>) (deny|permit) A.B.C.D",
       NO_STR
       "Add an access list entry\n"
       "IP standard access list\n"
       "IP standard access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Address to match\n")

  
DEFUNST (no_access_list_standard_host,
       no_access_list_standard_host_cmd,
       "no access-list (<1-99>|<1300-1999>) (deny|permit) host A.B.C.D",
       NO_STR
       "Add an access list entry\n"
       "IP standard access list\n"
       "IP standard access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "A single host address\n"
       "Address to match\n")

DEFUNST (no_access_list_extended_any_any,
       no_access_list_extended_any_any_cmd,
       "no access-list (<100-199>|<2000-2699>) (deny|permit) ip any any",
       NO_STR
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "Any source host\n"
       "Any destination host\n")

  
DEFUNST (show_ip_bgp_flap_filter_list,
       show_ip_bgp_flap_filter_list_cmd,
       "show ip bgp flap-statistics filter-list WORD",
       SHOW_STR
       IP_STR
       BGP_STR
       "Display flap statistics of routes\n"
       "Display routes conforming to the filter-list\n"
       "Regular expression access list name\n")


DEFUNST (bgp_network_mask_natural_backdoor,
       bgp_network_mask_natural_backdoor_cmd,
       "network A.B.C.D backdoor",
       "Specify a network to announce via BGP\n"
       "Network number\n"
       "Specify a BGP backdoor route\n")


DEFUNST (access_list_extended_host_any,
       access_list_extended_host_any_cmd,
       "access-list (<100-199>|<2000-2699>) (deny|permit) ip host A.B.C.D any",
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "A single source host\n"
       "Source address\n"
       "Any destination host\n")

DEFUNST (show_ip_as_path_access_list,
       show_ip_as_path_access_list_cmd,
       "show ip as-path-access-list WORD",
       SHOW_STR
       IP_STR
       "List AS path access lists\n"
       "AS path access list name\n")

DEFUNST (clear_ip_bgp_dampening_prefix,
       clear_ip_bgp_dampening_prefix_cmd,
       "clear ip bgp dampening A.B.C.D/M",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear route flap dampening information\n"
       "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n")


DEFUNST (show_ip_bgp_neighbor_received_prefix_filter,
       show_ip_bgp_neighbor_received_prefix_filter_cmd,
       "show ip bgp neighbors (A.B.C.D|X:X::X:X) received prefix-filter",
       SHOW_STR
       IP_STR
       BGP_STR
       "Detailed information on TCP and BGP neighbor connections\n"
       "Neighbor to display information about\n"
       "Neighbor to display information about\n"
       "Display information received from a BGP neighbor\n"
       "Display the prefixlist filter\n")

DEFUNST (show_ip_bgp_ipv4_community_list_exact,
       show_ip_bgp_ipv4_community_list_exact_cmd,
       "show ip bgp ipv4 (unicast|multicast) community-list WORD exact-match",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Display routes matching the community-list\n"
       "community-list name\n"
       "Exact match of the communities\n")

DEFUNST (bgp_network_mask_natural_route_map,
       bgp_network_mask_natural_route_map_cmd,
       "network A.B.C.D route-map WORD",
       "Specify a network to announce via BGP\n"
       "Network number\n"
       "Route-map to modify the attributes\n"
       "Name of the route map\n")


DEFUNST (no_access_list_standard,
       no_access_list_standard_cmd,
       "no access-list (<1-99>|<1300-1999>) (deny|permit) A.B.C.D A.B.C.D",
       NO_STR
       "Add an access list entry\n"
       "IP standard access list\n"
       "IP standard access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Address to match\n"
       "Wildcard bits\n")

DEFUNST (bgp_network_backdoor,
       bgp_network_backdoor_cmd,
       "network A.B.C.D/M backdoor",
       "Specify a network to announce via BGP\n"
       "IP prefix <network>/<length>, e.g., 35.0.0.0/8\n"
       "Specify a BGP backdoor route\n")

DEFUNST (access_list_standard_nomask,
       access_list_standard_nomask_cmd,
       "access-list (<1-99>|<1300-1999>) (deny|permit) A.B.C.D",
       "Add an access list entry\n"
       "IP standard access list\n"
       "IP standard access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Address to match\n")
DEFUNST (no_access_list_any,
       no_access_list_any_cmd,
       "no access-list WORD (deny|permit) any",
       NO_STR
       "Add an access list entry\n"
       "IP zebra access-list name\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Prefix to match. e.g. 10.0.0.0/8\n")
DEFUNST (access_list_extended_any_mask,
       access_list_extended_any_mask_cmd,
       "access-list (<100-199>|<2000-2699>) (deny|permit) ip any A.B.C.D A.B.C.D",
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "Any source host\n"
       "Destination address\n"
       "Destination Wildcard bits\n")

DEFUNST (no_access_list_extended_any_mask,
       no_access_list_extended_any_mask_cmd,
       "no access-list (<100-199>|<2000-2699>) (deny|permit) ip any A.B.C.D A.B.C.D",
       NO_STR
       "Add an access list entry\n"
       "IP extended access list\n"
       "IP extended access list (expanded range)\n"
       "Specify packets to reject\n"
       "Specify packets to forward\n"
       "Any Internet Protocol\n"
       "Any source host\n"
       "Destination address\n"
       "Destination Wildcard bits\n")
DEFUNST (show_ip_bgp_ipv4_community_exact,
       show_ip_bgp_ipv4_community_exact_cmd,
       "show ip bgp ipv4 (unicast|multicast) community (AA:NN|local-AS|no-advertise|no-export) exact-match",
       SHOW_STR
       IP_STR
       BGP_STR
       "Address family\n"
       "Address Family modifier\n"
       "Address Family modifier\n"
       "Display routes matching the communities\n"
       "community number\n"
       "Do not send outside local AS (well-known community)\n"
       "Do not advertise to any peer (well-known community)\n"
       "Do not export to next AS (well-known community)\n"
       "Exact match of the communities")


DEFUNST (clear_ip_bgp_dampening,
       clear_ip_bgp_dampening_cmd,
       "clear ip bgp dampening",
       CLEAR_STR
       IP_STR
       BGP_STR
       "Clear route flap dampening information\n")

  DEFUNST (no_set_vpnv4_nexthop,
       no_set_vpnv4_nexthop_cmd,
       "no set vpnv4 next-hop",
       NO_STR
       SET_STR
       "VPNv4 information\n"
       "VPNv4 next-hop address\n")


/* helper function to generate the argv vector*/

const char** genargv(char *args)
{
	
	const char **argv= (const char**)malloc(sizeof(char*)*(3)) ;

	argv[0]="command";

	argv[1]=args;
	
	argv[2]=NULL;

	return argv;

}	

void BGPTimer::Schedule(double delay,Event *e)
{
    expire_ = Simulator::instance->Now()+delay;
	
    Scheduler::Schedule(e,delay,this);
}


void BGPTimer::Cancel(Event* e)
{
  Scheduler::Cancel(e);
}

void BGPTimer::expire(Event* e)
{
}

void BGPTimer::Handle(Event* e, Time_t now)
{

    agent->timeout(e);
    
    BGPEvent *ev  =(BGPEvent*)e;
    
    if (ev->argv)
	free( ev->argv);
    
    delete ev;
}  

void BGP::timeout(Event* e)
{
    BGPEvent* ev = (BGPEvent*)e;
    const char **arg;
   
    switch (ev->event)
    {
        
    case BGPEvent::MAIN:
        type = TYPE_MAIN;
        bgp_main();
        break;
        
    case BGPEvent::FETCH1:
        type = TYPE_FETCH1;
        thread_fetch_part1(master);
        break;
        
    case BGPEvent::FETCH2:
        type = TYPE_FETCH2;
        thread_fetch_part2(master,InterruptAgent);
        break;

    case BGPEvent::EXECUTE:
        type = TYPE_EXECUTE;
        thread_call(ev->thread_to_be_fetched);
        break;
        
    case BGPEvent::COMMAND:
        type = TYPE_EXECUTE;
        arg= genargv(ev->argv);
        command(2,arg);
        free(arg);
        break; 
        
    }

}

void BGP::config_file(char* filename)
{
    /*should change this to use strncpy*/
    strcpy(bgp_config_file,filename);
    return;
}

int BGP::command(int argc,const char** argv)
{
  if (argc == 1)
  {
    if (strcmp(argv[0], "show-memory") == 0)
    {	    
      double simulation_time = Simulator::instance->Now();
      printf( "\n At:%lf Total memory usage %ld MB\n",
              simulation_time,ReportMemUsageMB());
      return 0;
    }
    
    if (strcmp(argv[0], "mrai-off") == 0)
    {
      mrai_type = MRAI_DISABLE;
      return 0;
    }
    
    if (strcmp(argv[0], "mrai-per-peer") == 0)
    {
      mrai_type = MRAI_PER_PEER;
      return 0;
    }
    
    if (strcmp(argv[0], "ssld-on") == 0)
    {
      ssld = true;
      return 0;
    }
    
    if (strcmp(argv[0], "ssld-off") == 0)
    {
      ssld = false;
      return 0;
    }
  }
  
  if (argc == 2)
  {
    if (strcmp(argv[0], "finish-time") == 0)
      {
        cout<<" Warning the \"bgp-instance finish-time time\" command is not required any more"<<endl;
        return 0;
      }
    if (strcmp(argv[0], "register") == 0)
    {
      cout<<" Warning the \"bgp-instance register bgp-registry\""
          <<" command is not necessary from  bgp++ 1.05."
          <<" BGP instances are now registered automatically"<<endl;
      
      return 0;
    }
    if (strcmp(argv[0], "config-file") == 0)
    {
      strcpy(bgp_config_file,argv[1]);
      return 0;
    }
    if (strcmp(argv[0], "start-time") == 0)
    {
      double time_to_start = atof(argv[1]);
      
      if (time_to_start < 0 || type != TYPE_MAIN) return 1;
      
      type = TYPE_FETCH1; 
      BGPEvent* fetch1 = new BGPEvent(BGPEvent::FETCH1);
      timer->Schedule(time_to_start,fetch1);
      
      return 0;
    }
    if (strcmp(argv[0], "command") == 0)
    {
      if (type==TYPE_MAIN) return 1;
      
      struct vty *vty;
      struct _vector* vline;
      int ret; 
      
      vty = vty_new ();
      vty->node = CONFIG_NODE;
      vty->index = bgp_get_default(); 
      vline = cmd_make_strvec ((char*)argv[1]);
      
      if (vline == NULL)
        return 0;
	   
      /* Execute configuration command : this is strict match */
	    
      /* Echo the command */
      vty_out (vty, ">%s%s", argv[1], VTY_NEWLINE);
      
      ret = cmd_execute_command_strict (vline, vty, NULL);
      
      cmd_free_strvec (vline);
      
      if ((ret != CMD_SUCCESS) && (ret != CMD_WARNING) )
      {
        switch (ret)
        {
        case CMD_ERR_AMBIGUOUS:
          fprintf (stderr, "Ambiguous command.\n");
          break;
        case CMD_ERR_NO_MATCH:
          fprintf (stderr, "There is no such command.\n");
          break;
        }
        fprintf (stderr, "Error occured during reading below line.\n%s\nfrom configuration file\n", 
                 argv[1]);
        vty_close (vty);
        exit (1);
      }
      
      vty_close (vty);
      
      type = TYPE_FETCH2;
      
      bgp_interrupt(this,NULL,
                    new Data());
      
      return 0;
    }    
    
  }
  
  if (strcmp(argv[0], "cpu-load-model") == 0)
  { 
    if (argc == 2) {
#ifdef HAVE_PERFCTR
      if(strcmp(argv[1], "time-sample") == 0) {
        workload_model =  TIME_SAMPLE_WORKLOAD_MODEL;
        if(!BGP::perfcnt_init) { 
          BGP::info = do_init();
          do_setup(BGP::info);
          do_enable();
          BGP::perfcnt_init = 1;
        }
        return 0;
      }
#endif /* HAVE_PERFCTR */
      
      if(strcmp(argv[1], "no") == 0) {
        workload_model = NO_WORKLOAD_MODEL;
        return 0;
      }
    }
    if (argc == 4) {
      if(strcmp(argv[1], "uniform") == 0) {
        workload_model =  UNIFORM_WORKLOAD_MODEL;
        
        BGP::uniform_max = atof(argv[2]);
        BGP::uniform_min = atof(argv[3]);
        
        if (uniform_max <= uniform_min) {
          fprintf(stderr,"class Bgp:uniform command: bad syntax max %s is not bigger than min %s",argv[2],argv[3]);
          exit(1);
        }	    
        return 0;
      }
    }
  }
  
  return 0;
}

BGP::BGP(double start):mrai_type(MRAI_PER_PREFIX),ssld(false) 
{

    if (!BGP::enter_bgp_construct) { 
      BGP::enter_bgp_construct = true;
    }

    BGP::instance_cnt++;

    CopyOnConnect(false);
	
    community_list_master.num.head=NULL;
    community_list_master.num.tail=NULL;
    community_list_master.str.head=NULL;
    community_list_master.str.tail=NULL;

#ifndef HAVE_ZEBRA_93b
    bgp_reuse_thread = NULL;
    memset(&bgp_damp_cfg,0,sizeof(struct bgp_damp_config));
    prev_bgp_damp_cfg = NULL;
#endif

    /* ip as-path access-list 10 permit AS1. */

    as_list_master.num.head =   NULL;
    as_list_master.num.tail =   NULL;
    as_list_master.str.head =   NULL;
    as_list_master.str.tail =   NULL;
    as_list_master.add_hook =   NULL;
    as_list_master.delete_hook =    NULL;

    zlog_default = NULL;
    sprintf(new_line,"\n");
    
    access_master_ipv4.num.head =  NULL  ;
    access_master_ipv4.num.tail =  NULL  ;
    access_master_ipv4.str.head =  NULL  ;
    access_master_ipv4.str.tail =  NULL  ;
    access_master_ipv4.add_hook =  NULL  ;
    access_master_ipv4.delete_hook =  NULL  ;

/* Static structure of IPv4 prefix_list's master. */

    prefix_master_ipv4.num.head =    NULL;
    prefix_master_ipv4.num.tail =    NULL;
    prefix_master_ipv4.str.head =    NULL;
    prefix_master_ipv4.str.tail =    NULL;
    prefix_master_ipv4.seqnum =    1;
    prefix_master_ipv4.add_hook =    NULL    ;
    prefix_master_ipv4.delete_hook =    NULL    ;

    route_map_master.head      =  NULL;
    route_map_master.tail      =  NULL;
    route_map_master.add_hook  =  NULL;
    route_map_master.delete_hook= NULL;
    route_map_master.event_hook= NULL;

    for (u_int mtype = 0; mtype < MTYPE_MAX ; mtype++) {
#ifdef MEMORY_LOG
      mstat[mtype].name = NULL;
      mstat[mtype].alloc = 0;
      mstat[mtype].t_malloc = 0;
      mstat[mtype].c_malloc = 0;
      mstat[mtype].t_calloc = 0;
      mstat[mtype].c_calloc = 0;
      mstat[mtype].t_realloc = 0;
      mstat[mtype].t_free = 0;
      mstat[mtype].c_strdup = 0;
#else
      mstat[mtype].name = NULL;
      mstat[mtype].alloc = 0;
#endif /* MTPYE_LOG */
    }
    
    time (&BGP::start_time);
    
    if(!BGP::rnd && BGP::default_randomize) {
      FILE *fpr = NULL;
      int seedval;
      struct timeval timeval;
      BGP::rnd=1 ;
      
      // If file "seed_with" exists in cwd seed from this file
      // otherwise seed from clock
      
      fpr = fopen("seed_with","r");
      if(fpr){ 
	char buf[512];
	fgets(buf,512,fpr);
	seedval = atoi(buf);
	fclose(fpr);
	printf ("Seed from file with %d\n",seedval);
      } else {
	gettimeofday (&timeval, NULL);
	seedval =(int)timeval.tv_usec;
      }

      srand(seedval);
      
      // Store the seed we use in the file "seed"
      // for debugging
      
      FILE* fp = fopen("seed","w");
      fprintf(fp,"%d\n",seedval);
      fclose(fp);
    }
    
    memset(&host,0,sizeof(struct host));
    timer = new BGPTimer(this);
    debug_on = 0 ;
    InterruptAgent = NULL;
    thread_to_be_fetched = NULL;
    workload_model = NO_WORKLOAD_MODEL;

#ifdef HAVE_ZEBRA_93b
    damp = &bgp_damp_cfg;
#endif
   
}


void BGP::bgp_main()
{
  
  if (!BGP::enter_bgp_main) { 
    BGP::enter_bgp_main = true;
  }

	instance_cnt--;

  //Makes all necessary initializations and enters the bgp FSM.
  
  // zlog_default is the default logging facility.
  zlog_default = openzlog (BGP::progname, ZLOG_NOLOG, ZLOG_BGP,
			   LOG_CONS|LOG_NDELAY|LOG_PID, LOG_DAEMON);
  
  //bgp_master_init();
  
  /* Make master thread. */
  master = thread_master_create();
  
  /* Initializations. */
  cmd_init (1);
  if(cmdvec_init) memory_init();
  bgp_init();
  sort_node();
  /* static variables flag, only one instance of BGP does the cmd
     associated initialization, the others share the same memory*/
  cmdvec_init = 0;
  
  vtyvec = vector_init(VECTOR_MIN_SIZE);
  
  /* Parse config file. */
  vty_read_config ();
  
  bgp_serv_sock_family();
 
  type = TYPE_FETCH1; 
  BGPEvent* fetch1 = new BGPEvent(BGPEvent::FETCH1);
  timer->Schedule(0,fetch1);
  
}


void
BGP::Receive(Packet *p,L4Protocol *l4, Seq_t)
{
    Data *copy = (Data*)p->PopPDU()->Copy();
    
    bgp_interrupt(this,(TCP*)l4,copy);

    delete p;
}

void
BGP::Closed(L4Protocol *l4)
{

    bgp_interrupt(this,(TCP*)l4,NULL);
}


int BGP::sendMessage(Data d,struct peer* peer)
{
	int len;

    len = peer->listenAgent->Send(d);

    return d.Size();
}

void BGP::AttachNode(Node* n)
{
  if (!n->IsReal()) return; //ghost node
  myNode = n;
}

void BGP::StartAt(Time_t t)
{
  if (!myNode)
    {
      cout << "Attempt to start BGP with no local node" << endl;
      return;
    }

  if (!myNode->IsReal()) return; //No action for ghost nodes
 
  type = TYPE_MAIN;

  BGPEvent* e = new BGPEvent(BGPEvent::MAIN);
  //e->master = master;
  timer->Schedule(t,e);
}

void BGP::StopAt(Time_t t)
{
	finish_time =t ;
}

Application* 
BGP::Copy() const
{
  return new BGP(*this);
}


/* This function is used for communication from GTNetS to Zebra code,
   i.e. packet arrivals or user interrupts 
   (using the ' ns at time "BGP execute \"command\"" ' ) invoke the
   bgp_interrupt function that checks the state of the simulated bgpd and 
   reschedules its timer, if necessary, for the simulated now, i.e. invokes
   the bgpd.
*/
void 
BGP::bgp_interrupt( BGP* BgpToInterrupt,
		    TCP* interruptAgent,Data *intmsg_) 
{
    //shoudnt happen
    if (type == TYPE_MAIN)  return ;

    if (type == TYPE_FETCH2 && (( BgpToInterrupt->timer->expire_ - Simulator::instance->Now())>0) )
    {

    //    If the BgpToInterrupt is waiting for a timer to expire, e.g. the remote peer is inactive,
    //    we go ahead and reschedule the peer's timer to expire now. We also set the remote peer's
    //    InterruptAgent variable to let it know who caused the interrupt.

	BgpToInterrupt->InterruptAgent = (TCP*) interruptAgent ;

    TCPMsgBuf::iterator iter = MsgBuffer.find(interruptAgent);
	MessageBuffer *msgBuffer;

    if (iter == MsgBuffer.end())
    {
		msgBuffer = new MessageBuffer(intmsg_,false,true);
		MsgBuffer[interruptAgent]= msgBuffer;
		BgpToInterrupt->type = TYPE_FETCH2;
		BGPEvent *ev = new BGPEvent(BGPEvent::FETCH2);
		InterruptAgent = interruptAgent;
		BgpToInterrupt->timer->Schedule(0.0,ev);
        return;
    }
    else
    {
        msgBuffer = iter->second;
    }

	msgBuffer->dataAvailable = true;

    if (!msgBuffer->partialRead)
    {
        if (msgBuffer->ReceivedMsg)
        {
            delete msgBuffer->ReceivedMsg;
        }
        
        msgBuffer->ReceivedMsg = intmsg_;
    }
    else if (intmsg_)
    {
      
      if (! msgBuffer->ReceivedMsg)
      {
        msgBuffer->ReceivedMsg = intmsg_;
        msgBuffer->partialRead= false;
      }
      else
      {
        
        //there is partially read data ; we need to append this to the previous one

        u_long size;
        
        size = msgBuffer->ReceivedMsg->Size();

        size += intmsg_->Size();
        
        char *buffer = (char*) malloc(sizeof(char)*size);
        
        memcpy(buffer,msgBuffer->ReceivedMsg->data,msgBuffer->ReceivedMsg->Size());
        memcpy(buffer+msgBuffer->ReceivedMsg->Size(),intmsg_->data,intmsg_->Size());
        
        Data *New = new Data(size,buffer);
        free(buffer);
        free(intmsg_);

        delete msgBuffer->ReceivedMsg;
        
        msgBuffer->ReceivedMsg = New;
        
      }
    }
    
    
    BgpToInterrupt->type = TYPE_FETCH2;
    BGPEvent *ev = new BGPEvent(BGPEvent::FETCH2);
    InterruptAgent = interruptAgent;
    BgpToInterrupt->timer->Schedule(0.0,ev);
    
    }
    else if (  (type != TYPE_FETCH2)||(type == TYPE_FETCH2 && (( BgpToInterrupt->timer->expire_ - Simulator::instance->Now())==0 )))
    {
      // If the remote peer is "busy", we insert an entry in it's InterruptQueue
      
      struct InterruptInfo  *Intr = (struct InterruptInfo*)malloc(sizeof(struct InterruptInfo));
      bzero(Intr,sizeof(struct InterruptInfo));
      
      Intr->IntAgent = (TCP*)interruptAgent ;
      Intr->intmsg = intmsg_;
      BgpToInterrupt->InterruptQueue.push_back(*Intr);
    }
}

/* Make bgpd's server socket. */
void
BGP::bgp_serv_sock_family ()
{
  //    int ret;
  //int bgp_sock;
  //union sockunion su;
    
    /*
      Create server listening Agent
    */
  
    ServAgent  = new TCPTahoe();
    ServAgent->Attach(myNode);
    ServAgent->AttachApplication(this);
    ServAgent->Bind(BGP_PORT_DEFAULT);
    ServAgent->Listen();

}

bool
BGP::ConnectionFromPeer(L4Protocol *l4,IPAddr_t ip, PortId_t p)
{

    /* GTNetS creates a new TCP and leaves the servAgent listening.
       So, we need to add a read thread for this tcp  and then call
       bgp_interrupt
     */


	thread_add_read(master, &BGP::bgp_accept,NULL,(TCP*)l4);

    return true;

}

void 
BGP::ServConnectionComplete(L4Protocol *l4)
{

	bgp_interrupt(this,(TCP*)l4,NULL);
}

void 
BGP::ConnectionComplete(L4Protocol *l4)
{

	bgp_interrupt(this,(TCP*)l4,NULL);

}

BGP* 
BGP::findBgp(IPAddr_t ip)
{

	IpBgp_t::iterator iter;

	for ( iter = IpBgp.begin(); iter != IpBgp.end(); ++iter)
	{
		
		if ( iter->first == ip)
			return iter->second;
	}

	return NULL;
}

/* Accept bgp connection. */
//bool
//BGP::bgp_accept (IPAddr_t *ip,TCP* lagent)
int
BGP::bgp_accept(struct thread* thread)
{

  union sockunion peer_su;
  struct peer *peer;
  struct peer *peer1;
  struct bgp *bgp;
  //bool found = false;

  /*get local agent, that is accepting the connection*/
  TCP *lagent = (TCP *) thread->u.listenAgent;


  IPAddr_t remoteIp = lagent->peerIP;

  bgp = bgp_get_default();


/*  BGP* rPeer = findBgp(remoteIp);

  if ( rPeer)
  {
	peer_su.sin.sin_family = AF_INET;
	peer_su.sin.sin_addr.s_addr= htonl(remoteIp);
	found = true;
  }

    if(!found) {
      zlog_info("Error (from bgp_accept()): Unknown connecting agent-closing connection\n");
	  lagent->Close();
      return -1;
    }*/

    if (BGP_DEBUG (events, EVENTS))
      zlog_info ("[Event] BGP connection from host %s\n", IPAddr::ToDotted(remoteIp));

	bzero(&peer_su,sizeof(peer_su));
	str2sockunion(IPAddr::ToDotted(remoteIp),&peer_su);
	peer1= peer_lookup_by_su(&peer_su);

    if (! peer1 || peer1->status == Idle)
      {
    if (BGP_DEBUG (events, EVENTS))
      {
        if (! peer1)
          {
        if (0) printf("\n Closing connection to remote host, host not found ");
        zlog_info ("[Event] BGP connection IP address %s is not configured\n",
               IPAddr::ToDotted(remoteIp));
          }
        else
          {
        if (0) printf("\n Closing connection to remote host, peer status Idle");
        zlog_info ("[Event] BGP connection IP address %s is Idle state\n",
               IPAddr::ToDotted(remoteIp));
          }
      }

    /* If the connection is not closing, close it */
    if(!(lagent->State() == TCP::LAST_ACK ||
           lagent->State() == TCP::CLOSED   ||
         lagent->State() == TCP::CLOSE_WAIT) )
      {

        zlog_info ("[Event] BGP closing TCP connection to IP address %s \n",
               IPAddr::ToDotted(remoteIp));


        lagent->Close();

      }

    /* We need to delete those agents ??????????*/
		return -1;
      }

    /* Make dummy peer until read Open packet. */
    if (BGP_DEBUG (events, EVENTS))
      zlog_info ("[Event] Make dummy peer structure until read Open packet\n");


      //create dummy peer
      peer = peer_create_accept (bgp);
      SET_FLAG (peer->sflags, PEER_STATUS_ACCEPT_PEER);
      //str2sockunion(IPAddr::ToDotted(remoteIp),&peer->su) ;
      peer->su = peer_su;
      peer->status = Active;
      peer->local_id = peer1->local_id;
      peer->host = strdup(peer1->host);

      // Store Tcp Agent pointer
      peer->listenAgent  = lagent;

      BGP_EVENT_ADD (peer, TCP_connection_open);

      return 0;
}

