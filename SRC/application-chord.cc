/*
 *  $Id: application-chord.cc 456 2006-01-20 17:37:47Z riley $
 *
 *  application-chord.cc by Dr. Sven Krasser
 *
 *  An implementation of the Chord distributed hash table for GTNetS based on
 *  Stoica et al. "Chord: A Scalable Peer-to-peer Lookup Service for Internet
 *  Applications," ACM SIGCOMM 2001, San Deigo, CA, August 2001, pp. 149-160.
 *
 *  For more information see
 *  http://www.ece.gatech.edu/research/labs/nsa/gtnets/chord.shtml
 *
 */
 
// GENERAL PUBLIC LICENSE AGREEMENT
//
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
//
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
//
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
//
// 1.  This License allows you to:
//
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
//
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//
//
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
//
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
//
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
//
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
//
//
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
//
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
//
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
//
//
// 3.  Export Law Assurance.
//
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//
// 4.  Termination.
//
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
//
//
// 5.  Disclaimer of Warranties and Limitation on Liability.
//
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
//
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
//
// GTRC shall have no obligation for support or maintenance of Program.
//
// 6.  Copyright Notice.
//
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
//
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
//
// Said copyright notice shall read as follows:
//
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
 



#include <iostream>
#include <sstream>
#include <string>
//#include <ios>
#include <map>
#include <sys/types.h>

#include "application-chord.h"

using namespace std;
// Chord Identifier
#ifdef WIN32
#include <fstream>
#endif


unsigned char ChordId::m;

ChordId::ChordId() {
	Init();
}

ChordId::ChordId(unsigned u0, unsigned u1, unsigned u2, unsigned u3, unsigned u4) {
	Init();
	id[0] = u0;
	id[1] = u1;
	id[2] = u2;
	id[3] = u3;
	id[4] = u4;
}

ChordId::ChordId(unsigned b) {
	unsigned bl, be;

	Init();
	
	// b-th bit is in element indexed by bl
	bl = b / 32;
	bl = 4 - bl;
	
	// b-th bit is be-th bit in this element
	be = b % 32;
	
	unsigned test = 1 << be;

	id[bl] = test;
}

void ChordId::Init() {
	int i;
	for(i = 0; i < 5; i++)
		id[i] = 0;
	
	// Initialize static bitlength to 160
	if(!m)
		m = 160;	
}

bool ChordId::operator==(ChordId cid) {
	int i;
	for(i = 0; i < 5; i++)
		if(this->id[i] != cid.id[i])
			return false;
	return true;
}

bool ChordId::operator!=(ChordId cid) {
	return !(this->operator==(cid));
}

bool ChordId::operator>(ChordId cid) {
	int i;
	for(i = 0; i < 5; i++)
		if(this->id[i] > cid.id[i])
			return true;			// greater
		else if(this->id[i] < cid.id[i])
			return false;			// less
	return false;					// equal
}

bool ChordId::operator>=(ChordId cid) {
	int i;
	for(i = 0; i < 5; i++)
		if(this->id[i] > cid.id[i])
			return true;			// greater
		else if(this->id[i] < cid.id[i])
			return false;			// less
	return true;					// equal
}

bool ChordId::operator<(ChordId cid) {
	return !(this->operator>=(cid));
}

bool ChordId::operator<=(ChordId cid) {
	return !(this->operator>(cid));
}

ChordId ChordId::operator+(ChordId cid) {
// xxx Fix me: Doesn't work for all values of m yet
	ChordId result;
	u_int64_t sum;
	unsigned carry = 0;
	int i;
	if(m == 160) {
		for(i = 4; i >= 0; i--) {
			sum = (u_int64_t)this->id[i] + (u_int64_t)cid.id[i] + (u_int64_t)carry;
			result.id[i] = sum & 0xffffffff;
			carry = sum >> 32;
		}
	} else if(m <= 32) {
		sum = (u_int64_t)this->id[4] + (u_int64_t)cid.id[4];
		result.id[4] = sum & (((u_int64_t)1 << m) - 1);
		
	} else {
		cout << "ChordId::operator+() not yet implemented for identifier ";
		cout << "bitlengths 33..159." << endl;
	}
	
	return result;
}

bool ChordId::InIntervalOO(ChordId a, ChordId b) {
	if(a < b)
		return *this > a && *this < b;
	else
		return !(*this >= b && *this <= a);
}

bool ChordId::InIntervalOC(ChordId a, ChordId b) {
	if(a < b)
		return *this > a && *this <= b;
	else
		return !(*this > b && *this <= a);
}

void ChordId::SetId(unsigned u0, unsigned u1, unsigned u2, unsigned u3, unsigned u4) {
	id[0] = u0;
	id[1] = u1;
	id[2] = u2;
	id[3] = u3;
	id[4] = u4;
}

ChordId ChordId::MaskBits(unsigned char m) {
	// This method masks out all bits but the m least significant ones
	int bl, i, nm;
	unsigned bitmask;
	
	bl = m / 32; // how many complete elements
	bl = 4 - bl; // start at element element index 4 and erase downwards
	nm = m % 32;
	
	for(i = 0; i < 5; i++) {
		if(i == bl) {
			// this is the element that has some bits considered at some not
			bitmask = 1 << nm;
			bitmask--;
			id[i] &= bitmask;
		} else if(i < bl) {
			// throw away all bits in this element
			id[i] = 0;
		} // all other bits are considered and stay untouched
	}
	return *this;
}

ostream& operator<<(ostream &os, ChordId cid) { // Overload operator in ostream class
	os << "[" << hex;
		
	// This makes it easier to read short identifiers
	if(cid.GetBitLength() <= 8) {
		os.width(2);
		os.fill('0');
		os << cid.id[4];
	} else
	
	for(int i = 0; i < 5; i++) {
		os.width(8);
		os.fill('0');
		os << cid.id[i];
	}
	os << dec << "]";
	return os;
}


// Chord Finger

// not actually a member...
ostream& operator<<(ostream &os, ChordFinger f) {
	if(f.IsSet()) {
		os << f.GetId() << " " << IPAddr::ToDotted(IPAddr(f.GetIP()));
		os << ":" << f.GetPort();
	} else {
		os << "<unset>";
	}
	return os;
}


// Chord Finger Table

ChordFingerTable::ChordFingerTable() : table(0), table_size(160) {
	stats = Chord::GetStats();
	AllocTable();
}

ChordFingerTable::ChordFingerTable(unsigned int size) : table(0) {
	stats = Chord::GetStats();
	table_size = size;
	AllocTable();
}

ChordFingerTable::~ChordFingerTable() {
	if(table)
		delete[] table;
	
	// The following line doesn't compile on some g++ versions due to a compiler bug
	// (http://gcc.gnu.org/ml/gcc-bugs/2003-09/msg01494.html)
	//Object::~Object();
	// Workaround:
	static_cast< Object >( *this ).~Object();
}

void ChordFingerTable::AllocTable() {
	if(table) {
		delete[] table;
	}
	table = new ChordFinger[table_size];
}

void ChordFingerTable::SetSize(unsigned int size) {
	table_size = size;
	AllocTable();
}

void ChordFingerTable::Print() {
	int i;
	ChordFinger f;
	ChordId cid;
	IPAddr_t ip;
	int m = table_size;
	
	cout << "Finger Table on " << cid << ": " << Name() << endl;	
	
	for(i = 1; i <=m; i++) {
		cout << i << "   ";
		f = GetFinger(i);
		if(f.IsUnset())
			cout << "     <empty>" << endl;
		else {
			cid = f.GetId();
			ip = f.GetIP();
			cout << cid << "   "
			     << IPAddr::ToDotted(IPAddr(ip)) << ":"
			     << f.GetPort() << endl;
		}
	}
	//cout << endl;
}


// Code for the Chord Resolver

ChordResolver::ChordResolver() : next_seq_no(1), hi_seq_no(0), busy(false),
	timeout_event(0), timeout_count(0)
{
	stats = Chord::GetStats();
}

void ChordResolver::FindSuccessor(ChordId cid) {
	CDBGPR(<< "FindSuccessor(), cid " << cid << ", rid " << resolver_id);
	if(busy) {
		cout << "Can't issue request to resolver: resolver still busy" << endl;
		return;
	}
	busy = true;
	resolving_cid = cid;
	STATSLOG("findsuccessor invoked " << ch->my_id << " " << cid);
	if(cid.InIntervalOC(ch->my_id, ch->finger_table.GetSuccessor().GetId())) {
		CDBGPR(<< "FindSuccessor() invoked by " << ch->my_id << ": " << cid << " in (my_id, succ]");
		HandleFoundSuccessor(ch->finger_table.GetSuccessor());
	} else {
		ChordFinger prec;
		prec = ch->ClosestPrecedingNode(cid);
		resolving_peer = prec;
		STATSLOG("findsuccessor query_remote " << ch->my_id << " " << cid << " " << prec.GetId());
		CDBGPR(<< "FindSuccessor(): prec is " << prec);
		CDBGPR(<< "rid " << resolver_id << " on " << ch->GetMyFinger());
		timeout_event = new ChordResolverEvent(ChordResolverEvent::TIMEOUT);
		Scheduler::Schedule(timeout_event, ch->rpc_timeout, this);
		ch->SendFindSuccessorRequest(prec.GetIP(), prec.GetPort(), cid, next_seq_no++, resolver_id);
	}
}

void ChordResolver::HandleFindSuccessorResponse(ChordPDU *chpdu) {
	if(chpdu->seq_no > hi_seq_no) {
		CancelTimeout();
		
		hi_seq_no = chpdu->seq_no;
		ChordFinger finger = chpdu->finger;		
		
		timeout_event = new ChordResolverEvent(ChordResolverEvent::TIMEOUT);
		Scheduler::Schedule(timeout_event, ch->rpc_timeout, this);
		ch->SendFindSuccessorRequest(finger.GetIP(), finger.GetPort(), resolving_cid, next_seq_no++, resolver_id);
		STATSLOG("findsuccessor redirect " << ch->my_id << " " << resolving_cid << " " << finger.GetId() << " " << resolving_peer.GetId());
		resolving_peer = finger;
	} else {
		CDBGPR("HandleFindSuccessorResponse() at " << ch->my_id << " received old PDU");
	}
}

void ChordResolver::CancelTimeout() {
	if(timeout_event) {
		Scheduler::Cancel(timeout_event);
		delete timeout_event;
		timeout_event = 0;
	}
}

void ChordResolver::HandleFoundSuccessor(ChordPDU *chpdu) {
	if(chpdu->seq_no > hi_seq_no) {
		CancelTimeout();
		hi_seq_no = chpdu->seq_no;
		busy = false;
		FoundSuccessor(chpdu->finger);
	} else {
		CDBGPR("HandleFoundSuccessor() at " << ch->my_id << " received old PDU");
	}
}

void ChordResolver::HandleFoundSuccessor(ChordFinger finger) {
	busy = false;
	timeout_count = 0;
	CDBGPR("HandleFoundSuccessor() at " << ch->my_id << " successor is " << finger);
	STATSLOG("findsuccessor success " << ch->my_id << " " << resolving_cid << " " << finger.GetId());
	FoundSuccessor(finger);
}

void ChordResolver::HandleFailed() {
	busy = false;
	timeout_count = 0;
	CDBGPR("HandleFailed() at " << ch->my_id << " peer is " << resolving_peer);
	STATSLOG("findsuccessor failed " << ch->my_id << " " << resolving_cid << " " << resolving_peer.GetId());
	Failed(resolving_peer);
}	

void ChordResolver::Failed(ChordFinger failed_peer) {
}

void ChordResolver::FoundSuccessor(ChordFinger result) {
}

void ChordResolver::Handle(Event* e, Time_t t) {
	CDBGPR(<< "ChordResolver::Handle()");
	ChordResolverEvent *ev = (ChordResolverEvent*)e;
	switch(ev->event) {
		case ChordResolverEvent::TIMEOUT:
			delete ev;
			timeout_count++;
			timeout_event = 0;
			STATSLOG("timeout " << timeout_count << " " << ch->my_id << " FindSucecessor " << resolving_peer.GetId());
			if(timeout_count >= CHORD_MAX_XMIT_ATTEMPTS) {
				timeout_count = 0;
				HandleFailed();
			} else {
				timeout_event = new ChordResolverEvent(ChordResolverEvent::TIMEOUT);
				Scheduler::Schedule(timeout_event, ch->rpc_timeout, this);
				ch->SendFindSuccessorRequest(resolving_peer.GetIP(), resolving_peer.GetPort(), resolving_cid, next_seq_no++, resolver_id);
			}
		break;
			
		default:
			cout << "ChordResolver::Handle(): Unknown event" << endl;
			delete ev;
	}
}


// ChordFingerResolver -- Resolver to update the finger table entries

void ChordFingerResolver::FoundSuccessor(ChordFinger finger) {
	CDBGPR(<< "FoundSuccessor() at " << ch->my_id << ": updating finger " << finger_index);
	ch->finger_table.SetFinger(finger_index, finger);
}

void ChordFingerResolver::Failed(ChordFinger failed_peer) {
	unsigned int index;
	ChordFinger prev = ch->my_finger;
	for(index = ch->m; index > 0; index--) {
		if(ch->finger_table.GetFinger(index).GetId() == failed_peer.GetId()) {
			ch->finger_table.SetFinger(index, prev);
		} else {
			prev = ch->finger_table.GetFinger(index);
		}
	}
}


// Main Chord Code

ChordStats* Chord::stats;
bool Chord::use_vis;

ChordStats* Chord::GetStats() {
	if(!stats) {
		if(use_vis)
			stats = new ChordVis();
		else
			stats = new ChordStats();
	}
	return stats;
}

Chord::Chord() {
	Init();
}

Chord::Chord(const Chord& ch) {
	Init();
}

Chord::~Chord() {
	if(join_resolver)
		delete join_resolver;
	if(finger_resolver)
		delete[] finger_resolver;
	if(stats)
		delete stats;
}

void Chord::Init() {
	running = false;
	next_resolver_id = 0;
	udp = 0;
	port = CHORD_STD_PORT;
	join_resolver = 0;
	finger_resolver = 0;
	joined = false;
	next_finger_to_fix = 1;
	next_notify_seq_no = 1;
	next_get_predecessor_seq_no = 1;
	next_check_predecessor_seq_no = 1;
	hi_notify_seq_no = 0;
	hi_get_predecessor_seq_no = 0;
	hi_check_predecessor_seq_no = 0;
	stabilize_event = 0;
	fix_fingers_event = 0;
	stabilize_interval = 1.5;
	fix_fingers_interval = 1.0;
	predecessor_ping_interval = 2.0;
	join_event = create_event = check_predecessor_event = 0;
	check_predecessor_timeout_event = notify_timeout_event = 0;
	get_predecessor_timeout_event = 0;
	die_event = 0;
	rpc_timeout = 1.0;
	predecessor_ping_interval = 5.0;
	check_predecessor_timeout = 0;
	get_predecessor_timeout = 0;
	notify_timeout = 0;
	ignore_messages = false;
	
	stats = GetStats();
	
	m = ChordId::GetBitLength();
	finger_table.SetSize(m);
}

void Chord::RegisterResolver(ChordResolver *cr) {
	ChordResolverId_t crid = next_resolver_id++;
	cr->SetResolverId(crid);
	cr->AttachChord(this);
	resolver_demux[crid] = cr;
}

void Chord::UnregisterResolver(ChordResolver *cr) {
	resolver_demux.erase(cr->GetResolverId());
}

void Chord::Receive(Packet* pkt, L4Protocol*, Seq_t) {
	ChordPDU* chordh = (ChordPDU*)pkt->PeekPDU();
	IPV4Header* iph = (IPV4Header*)pkt->PeekPDU(-2);
	UDPHeader* udph = (UDPHeader*)pkt->PeekPDU(-1);

	CDBGPR(<< "Recv from " << IPAddr::ToDotted(IPAddr(iph->GetSrcIP())) << ":" << udph->sourcePort << " to " << my_finger << ": " << *chordh);
	if(ignore_messages) {
		CDBGPR(<< "IGNORING MESSAGE");
		delete pkt;
		return;
	}
	
	if(chordh->type == REQUEST) {
		switch(chordh->msg) {
			// some node queries for the successor of an Id
			case FIND_SUCCESSOR:
				SendFindSuccessorResponse(iph->GetSrcIP(), udph->sourcePort, chordh->id, chordh->seq_no, chordh->resolver);
			break;
			
			// some node thinks it is our predecessor
			case NOTIFY:
				if(predecessor.IsUnset() || chordh->finger.GetId().InIntervalOO(predecessor.GetId(), my_id)) {
					predecessor = chordh->finger;
					STATSLOG("notify accept " << my_id << " " << chordh->finger.GetId());
				} else {
					STATSLOG("notify reject " << my_id << " " << chordh->finger.GetId());
				}
				SendNotifyResponse(iph->GetSrcIP(), udph->sourcePort, chordh->seq_no);
			break;
			
			// some node that thinks we are its successor wants to get our predecessor
			case GET_PREDECESSOR:
				SendGetPredecessorResponse(iph->GetSrcIP(), udph->sourcePort, chordh->seq_no);
			break;
			
			case CHECK_PREDECESSOR:
				SendCheckPredecessorResponse(iph->GetSrcIP(), udph->sourcePort, chordh->seq_no);
			break;
			
			default:
				cout << "Received unknown Chord request message." << endl;
		}
	} else {
		// type is RESPONSE
		switch(chordh->msg) {
			case FIND_SUCCESSOR:
				// check whether the resolver id is still in the demux map
				if(resolver_demux.find(chordh->resolver) != resolver_demux.end()) {
					if(chordh->redirect) {
						// the node replying knows of a closer node
						resolver_demux[chordh->resolver]->HandleFindSuccessorResponse(chordh);
					} else {
						// the node replying is actually the successor
						resolver_demux[chordh->resolver]->HandleFoundSuccessor(chordh);
					}
				} else {
					cerr << "Received response for unknown resolver" << endl;
				}
			break;
			
			case NOTIFY:
				if(chordh->seq_no > hi_notify_seq_no) {
					hi_notify_seq_no = chordh->seq_no;
					// we received a response -- cancel timeout
					Scheduler::Cancel(notify_timeout_event);
					delete notify_timeout_event;
					notify_timeout_event = 0;
					notify_timeout = 0; // reset timeout counter
				}
			break;
			
			case GET_PREDECESSOR:
				if(chordh->seq_no > hi_get_predecessor_seq_no) {
					hi_get_predecessor_seq_no = chordh->seq_no;
					HandleGetPredecessorResponse(chordh->finger);
				}
			// xxx
			
			/// code to make sure some resend event gets canceled...
			break;
			
			case CHECK_PREDECESSOR:
				if(chordh->seq_no > hi_check_predecessor_seq_no) {
					hi_check_predecessor_seq_no = chordh->seq_no;
					// we received a response -- cancel timeout
					Scheduler::Cancel(check_predecessor_timeout_event);
					delete check_predecessor_timeout_event;
					check_predecessor_timeout_event = 0;
					check_predecessor_timeout = 0; // reset timeout counter
				}
			break;
				
			default:
				cout << "Received unknown Chord response message." << endl;
		}
	}
	delete pkt;
	//cout << "Received Message at " << Simulator::Now() << ": " << chordh->type << "/" << chordh->msg << ", my port is " << port << endl;
}

ChordId Chord::GetMyId() {
	ChordId cid;
	char buffer[6];
#ifndef WIN32
	snprintf(buffer, 6, "%ld", (unsigned long)port);
#else
	_snprintf(buffer, 6, "%ld", (unsigned long)port);
#endif
	sha1.Reset();
	// This will generate the same IDs as the actual Chord implementation.
	// Don't know what the trailing ".0" stands for...
	sha1 << IPAddr::ToDotted(IPAddr(GetMyIP())) << "." << buffer << ".0";
	// cout << IPAddr::ToDotted(IPAddr(GetMyIP())) << "." << buffer << ".0" << endl;
	if(!sha1.Result((unsigned*)cid.id)) {
		cout << "Could not generate Chord ID" << endl;
	}
	cid.MaskBits(m);
	return cid;
}

void Chord::Create() {
	STATSLOG("create " << my_id);
	predecessor.Clear();      // Delete the predecessor
	
	unsigned int i;
	for(i = 1; i <= m; i++)
		finger_table.SetFinger(i, my_finger);
	joined = true;
	finger_resolver = new ChordFingerResolver[m];
	for(i = 1; i <= m; i++) {
		finger_resolver[i-1].SetIndex(i);
		RegisterResolver(&finger_resolver[i-1]);
	}
	CDBG(PrintFingerTable());
	InitEvents();
	
	// Register this node with the statistics object
	if(stats->IdExists(my_id))
		cout << "WARNING: Duplicate Id" << endl;
	stats->AddChord(this);
}

void Chord::Join(IPAddr_t bootstrap_ip, PortId_t bootstrap_port) {
	STATSLOG("join " << my_id << " " << IPAddr::ToDotted(bootstrap_ip) << ":" << bootstrap_port);
	join_resolver = new ChordJoinResolver();
	RegisterResolver(join_resolver);
	// Find out where we belong inside the Chord ring.
	// The result is handed over to the Joined() method.
	join_resolver->FindSuccessor(my_id);
	predecessor.Clear();
		
	// Register this node with the statistics object
	if(stats->IdExists(my_id))
		cout << "WARNING: Duplicate Id" << endl;
	stats->AddChord(this);
}

void Chord::Joined(ChordFinger finger) {
	if(join_resolver) {
		UnregisterResolver(join_resolver);
		delete join_resolver;
		join_resolver = 0;
	}
	
	STATSLOG("joined " << my_id << " " << finger.GetId());
	
	CDBGPR(<< my_id << " joined, successor is " << finger);
	
	finger_table.SetSuccessor(finger);

	unsigned int i;
	ChordId fcid;

	// Bootstrap the finger table:
	// Set all fingers to the successor since this is the only node that
	// we know that has knowledge about the network.
	for(i = 1; i <= m; i++) {
		finger_table.SetFinger(i, finger_table.GetSuccessor());
	}
	
	// Now that the finger table is in a sane state, try to find better fingers
	// using resolvers. This will spawn m resolvers associated with this Chord
	// node. When responses from the resolvers are received, they are
	// demultiplexed, and the corresponding finger table entry is filled in.
	finger_resolver = new ChordFingerResolver[m];
	for(i = 1; i <= m; i++) {
		fcid = my_id + ChordId(i-1);	// my_id + 2^(i-1)
		finger_resolver[i-1].SetIndex(i);
		RegisterResolver(&finger_resolver[i-1]);
		finger_resolver[i-1].FindSuccessor(fcid);
	}
	
	joined = true;
	InitEvents();
}

ChordFinger Chord::ClosestPrecedingNode(ChordId cid) {
	int i;
	ChordFinger finger;
	for(i = m; i >= 1; i--) {
		finger = finger_table.GetFinger(i);
		if(finger.IsSet() && finger.GetId().InIntervalOO(my_id, cid))
			return finger;
	}
	if(joined) {
		CDBGPR(<< my_id << " found no closest preceding node in finger table for " 
		       << cid << ", sending my finger back");
		return my_finger;
	}
	
	CDBGPR(<< my_id << " closest preceding node for " << cid << ": sending join node");
	// Fallback: This node is not in the Chord ring yet, so proxy all request
	// through the join node known to us
	return ChordFinger(ChordId(), join_ip, join_port);
}

void Chord::SendFindSuccessorResponse(IPAddr_t dst_ip, PortId_t dst_port, ChordId cid, unsigned long seq_no, ChordResolverId_t rid) {
	if(ignore_messages)
		return;
	
	ChordPDU *response = new ChordPDU();
	
	response->type = RESPONSE;
	response->msg = FIND_SUCCESSOR;
	response->seq_no = seq_no;	// echo sequence number back to sender
	response->resolver = rid;
	// Are we the node that knows the node our peer is looking for or do we have
	// to redirect it to another peer
	if(cid.InIntervalOC(my_id, finger_table.GetSuccessor().GetId())) {
		// It's us
		CDBGPR(<< "SendFindSuccessorResponse(): " << cid << " in (" << my_id << ", " << finger_table.GetSuccessor().GetId() << "] -- sending succ back");
		response->finger = finger_table.GetSuccessor();
		response->redirect = false;
	} else {
		ChordFinger cpn = ClosestPrecedingNode(cid);
		if(response->finger.GetId() == my_id) {
			CDBGPR(<< "SendFindSuccessorResponse(): We are cpn, sending succ back");
			// The interval is not correct, but there's still no better
			// node than us. This can happen after a Chord ring has been
			// created.
			// Don't redirect to avoid endless loop.
			response->finger = finger_table.GetSuccessor();
			response->redirect = false;
		} else {
			CDBGPR(<< "SendFindSuccessorResponse(): " << cid << " not in (" << my_id << ", " << finger_table.GetSuccessor().GetId() << "] -- sending cpn back");
			// We're not the right node. Redirecting the peer to a closer
			// node based on our finger table knowledge.
			response->finger = cpn;
			response->redirect = true;
		}
	}
	
	CDBGPR(<< *response);
	udp->SendTo(*(Data*)response, dst_ip, dst_port);
}

void Chord::SendFindSuccessorRequest(IPAddr_t dst_ip, PortId_t dst_port, ChordId cid, unsigned long seq_no, ChordResolverId_t rid) {
	if(ignore_messages)
		return;
	
	ChordPDU *request = new ChordPDU();
	
	request->type = REQUEST;
	request->msg = FIND_SUCCESSOR;
	request->seq_no = seq_no;
	request->id = cid;
	request->resolver = rid;
	
	CDBGPR(<< *request);
	udp->SendTo(*(Data*)request, dst_ip, dst_port);
}

void Chord::SendNotifyResponse(IPAddr_t dst_ip, PortId_t dst_port, unsigned long seq_no) {
	if(ignore_messages)
		return;

	ChordPDU *response = new ChordPDU();
	
	response->type = RESPONSE;
	response->msg = NOTIFY;
	response->seq_no = seq_no;	// echo sequence number back to sender

	CDBGPR(<< *response);
	udp->SendTo(*(Data*)response, dst_ip, dst_port);
}

void Chord::SendNotifyRequest(IPAddr_t dst_ip, PortId_t dst_port, unsigned long seq_no) {
	if(ignore_messages)
		return;
	
	if(!notify_timeout_event) {
		notify_timeout_event = new ChordEvent(ChordEvent::TIMEOUT);
		notify_timeout_event->msg = NOTIFY;
		Scheduler::Schedule(notify_timeout_event, rpc_timeout, this);
	} else {
		CDBGPR(<< "Trying to send NOTIFY while message still pending");
		return;
	}
	
	ChordPDU *request = new ChordPDU();
	
	request->type = REQUEST;
	request->msg = NOTIFY;
	request->seq_no = seq_no;
	request->finger = my_finger;
	
	CDBGPR(<< *request);
	udp->SendTo(*(Data*)request, dst_ip, dst_port);
}

void Chord::SendGetPredecessorResponse(IPAddr_t dst_ip, PortId_t dst_port, unsigned long seq_no) {
	if(ignore_messages)
		return;
	
	ChordPDU *response = new ChordPDU();

	response->type = RESPONSE;
	response->msg = GET_PREDECESSOR;
	response->seq_no = seq_no;	// echo sequence number back to sender
	response->finger = predecessor;

	CDBGPR(<< "Send from " << my_finger << " to " << IPAddr::ToDotted(IPAddr(dst_ip)) << ":" << dst_port << ": " << *response);
	udp->SendTo(*(Data*)response, dst_ip, dst_port);
}

void Chord::SendGetPredecessorRequest(IPAddr_t dst_ip, PortId_t dst_port, unsigned long seq_no) {
	if(ignore_messages)
		return;
	
	ChordPDU *request = new ChordPDU();
	
	request->type = REQUEST;
	request->msg = GET_PREDECESSOR;
	request->seq_no = seq_no;
	
	CDBGPR(<< *request);
	udp->SendTo(*(Data*)request, dst_ip, dst_port);
}

void Chord::SendCheckPredecessorRequest(IPAddr_t dst_ip, PortId_t dst_port, unsigned long seq_no) {
	if(ignore_messages)
		return;
	
	ChordPDU *request = new ChordPDU();

	if(!check_predecessor_timeout_event) {
		check_predecessor_timeout_event = new ChordEvent(ChordEvent::TIMEOUT);
		check_predecessor_timeout_event->msg = CHECK_PREDECESSOR;
		Scheduler::Schedule(check_predecessor_timeout_event, rpc_timeout, this);
	} else {
		CDBGPR(<< "Trying to send CHECK_PREDECESSOR while message still pending");
		return;
	}
	
	request->type = REQUEST;
	request->msg = CHECK_PREDECESSOR;
	request->seq_no = seq_no;
	
	CDBGPR(<< *request);
	udp->SendTo(*(Data*)request, dst_ip, dst_port);
}

void Chord::SendCheckPredecessorResponse(IPAddr_t dst_ip, PortId_t dst_port, unsigned long seq_no) {	
	if(ignore_messages)
		return;
	ChordPDU *response = new ChordPDU();
	
	response->type = RESPONSE;
	response->msg = CHECK_PREDECESSOR;
	response->seq_no = seq_no;
	
	CDBGPR(<< *response);
	udp->SendTo(*(Data*)response, dst_ip, dst_port);
}

void Chord::StartApp() {
	if(!localNode) {
		cout << "Chord::StartApp() with no localNode" << endl;
		return;
	}
	
	my_id = GetMyId();
	my_finger = GetMyFinger();
	running = true;
	CDBGPR(<< my_id << " started");


	// give some meaningful name to the finger table object
	stringstream stream;
	string str;
	stream << my_id;
	stream >> str;
	finger_table.Name(str);
	finger_table.SetId(my_id);
}

void Chord::InitEvents() {
	stabilize_event = new ChordEvent(ChordEvent::STABILIZE);
	Scheduler::Schedule(stabilize_event, 0.0, this);
	fix_fingers_event = new ChordEvent(ChordEvent::FIX_FINGERS);
	Scheduler::Schedule(fix_fingers_event, 0.0, this);
	check_predecessor_event = new ChordEvent(ChordEvent::CHECK_PREDECESSOR);
	Scheduler::Schedule(check_predecessor_event, 0.0, this);
}

void Chord::StopApp() {
	running = false;
	
	// Cancel pending events
	if(stabilize_event) {
		Scheduler::Cancel(stabilize_event);
		delete stabilize_event;
		stabilize_event = 0;
	}
	if(fix_fingers_event) {
		Scheduler::Cancel(fix_fingers_event);
		delete fix_fingers_event;
		fix_fingers_event = 0;
	}
	if(check_predecessor_event) {
		Scheduler::Cancel(check_predecessor_event);
		delete check_predecessor_event;
		check_predecessor_event = 0;
	}
	if(check_predecessor_timeout_event) {
		Scheduler::Cancel(check_predecessor_timeout_event);
		delete check_predecessor_timeout_event;
		check_predecessor_timeout_event = 0;
	}	
	if(get_predecessor_timeout_event) {
		Scheduler::Cancel(check_predecessor_timeout_event);
		delete check_predecessor_timeout_event;
		check_predecessor_timeout_event = 0;
	}
	if(notify_timeout_event) {
		Scheduler::Cancel(check_predecessor_timeout_event);
		delete check_predecessor_timeout_event;
		check_predecessor_timeout_event = 0;
	}
	if(join_event) {
		Scheduler::Cancel(join_event);
		delete join_event;
		join_event = 0;
	}
	if(create_event) {
		Scheduler::Cancel(create_event);
		delete create_event;
		create_event = 0;
	}
	
	// Cancel all timeout events in attached resolvers
	map<ChordResolverId_t, ChordResolver*>::iterator resolver_iterator = resolver_demux.begin();
	while(resolver_iterator != resolver_demux.end()) {
		resolver_iterator->second->CancelTimeout();
		resolver_iterator++;
	}
}

void Chord::Die() {
	CDBGPR(<< my_id << " dies");
	STATSLOG("die " << my_id);
	IgnoreMessagesOn();
	StopApp();
	stats->RemoveChord(this);
}

void Chord::Die(Time_t t) {
	die_event = new ChordEvent(ChordEvent::DIE);
	Scheduler::Schedule(die_event, t, this);
}

void Chord::AttachNode(Node *node) {
	localNode = node;
	if(!udp) {
		udp = new UDP(localNode);
		udp->Bind(port);
		udp->AttachApplication(this);
	} else {
		cout << "ChordResolver::AttachNode() invoked twice" << endl;
	}
	my_id = GetMyId();
	my_finger = GetMyFinger();
	
	CDBGPR(<< "Attached to node " << IPAddr::ToDotted(IPAddr(GetMyIP())));
	CDBGPR(<< "my_finger " << my_finger);
}

void Chord::SetPort(PortId_t p) {
	port = p;
}

//Application* Chord::Copy() const {
//	return new Chord(*this);
//}

void Chord::Stabilize() {
	CDBGPR(<< my_id << " Stabilize");
	succ_get_pred = finger_table.GetSuccessor();
	GetPredecessor();
}

void Chord::GetPredecessor() {
	SendGetPredecessorRequest(succ_get_pred.GetIP(), succ_get_pred. GetPort(), next_get_predecessor_seq_no++);
}

void Chord::GetPredecessorTimeout() {
	get_predecessor_timeout++;
	STATSLOG("timeout " << get_predecessor_timeout << " " << my_id << " GetPredecessor " << succ_get_pred.GetId());
	if(get_predecessor_timeout >= CHORD_MAX_XMIT_ATTEMPTS) {
		CDBGPR(<< my_id << " GetPredecessor Timeout. Giving up. Succ " << succ_get_pred.GetId());
		get_predecessor_timeout = 0;
		/// xxx
	} else {
		CDBGPR(<< my_id << " CheckPredecessor Timeout. Retrying. Succ " << succ_get_pred.GetId());
		GetPredecessor();
	}
}

void Chord::HandleGetPredecessorResponse(ChordFinger succpred) {
	CDBGPR(<< "got Stabilize response at " << my_id << ", succ.pred is " << succpred);
	if(succpred.IsSet()) {
		if(succpred.GetId().InIntervalOO(my_id, finger_table.GetSuccessor().GetId())
			|| finger_table.GetSuccessor().GetId() == my_id) {
			finger_table.SetSuccessor(succpred);
		}
	}
	Notify();
}

void Chord::Notify() {
	succ_notify = finger_table.GetSuccessor();
	Notify(succ_notify);
}

void Chord::Notify(ChordFinger succ) {
	CDBGPR(<< my_id << " is notifying " << succ);
	SendNotifyRequest(succ.GetIP(), succ.GetPort(), next_notify_seq_no++);
}

void Chord::NotifyTimeout() {
	notify_timeout++;
	STATSLOG("timeout " << notify_timeout << " " << my_id << " Notify " << succ_notify.GetId());
	if(notify_timeout >= CHORD_MAX_XMIT_ATTEMPTS) {
		CDBGPR(<< my_id << " Notify Timeout. Giving up. Succ " << succ_notify.GetId());
		notify_timeout = 0;
		//xxx Some_code_to_update_finger_table();
	} else {
		CDBGPR(<< my_id << " Notify Timeout. Retrying. Succ " << succ_notify.GetId());
		Notify(succ_notify);
	}
}

void Chord::FixFinger() {
	FixFinger(next_finger_to_fix);
	if(++next_finger_to_fix > m)
		next_finger_to_fix = 1;
}

void Chord::FixFinger(unsigned short index) {
	CDBGPR(<< "Fixing finger " << index << " on " << my_id);
	STATSLOG("fix_finger " << index << " " << my_id);
	if(!finger_resolver[index-1].IsBusy()) {
		ChordId fcid = my_id + ChordId(index-1);  // my_id + 2^(i-1);
		finger_resolver[index-1].FindSuccessor(fcid);
	} else {
		CDBGPR(<< "Finger resolver is busy, doing nothing.");
	}
}

void Chord::CheckPredecessor() {
	if(predecessor.IsSet())
		SendCheckPredecessorRequest(predecessor.GetIP(), predecessor.GetPort(), next_check_predecessor_seq_no++);
}

void Chord::CheckPredecessorTimeout() {
	check_predecessor_timeout++;
	STATSLOG("timeout " << check_predecessor_timeout << " " << my_id << " CheckPredecessor " << predecessor.GetId());
	if(check_predecessor_timeout >= CHORD_MAX_XMIT_ATTEMPTS) {
		CDBGPR(<< my_id << " CheckPredecessor Timeout. Giving up. Pred " << predecessor.GetId());
		check_predecessor_timeout = 0;
		PredecessorDied();
	} else {
		CDBGPR(<< my_id << " CheckPredecessor Timeout. Retrying. Pred " << predecessor.GetId());
		CheckPredecessor();
	}
}

void Chord::PredecessorDied() {
	predecessor.Clear(); // delete the predecessor entry
	STATSLOG("predecessor clear " << my_id);
}

void Chord::IgnoreMessages(Time_t start, Time_t duration) {
	IgnoreMessagesOn(start);
	IgnoreMessagesOff(start + duration);
}

void Chord::IgnoreMessagesOn(Time_t t) {
	ChordEvent *e = new ChordEvent(ChordEvent::IGNORE_MSG_ON);
	Scheduler::Schedule(e, t, this);
}

void Chord::IgnoreMessagesOff(Time_t t) {
	ChordEvent *e = new ChordEvent(ChordEvent::IGNORE_MSG_OFF);
	Scheduler::Schedule(e, t, this);
}

void Chord::Handle(Event* e, Time_t t) {
	ChordEvent *ev = (ChordEvent*)e;
	switch(ev->event) {
		case ChordEvent::STABILIZE:
			//cout << "Stabilize event at " << Simulator::Now() << endl;
			Stabilize();
			stabilize_event = ev;
			Scheduler::Schedule(ev, stabilize_interval, this);
		break;
		
		case ChordEvent::FIX_FINGERS:
			//cout << "Fix_fingers event at " << Simulator::Now() << endl;
			FixFinger();
			fix_fingers_event = ev;
			Scheduler::Schedule(ev, fix_fingers_interval, this);
		break;
		
		case ChordEvent::CREATE:
			CDBGPR(<< "Creating Chord ring at " << my_id);
			Create();
			delete ev;
			ev = 0;
			create_event = 0;
		break;
		
		case ChordEvent::JOIN:
			Join();
			delete ev;
			ev = 0;
			join_event = 0;
		break;
		
		case ChordEvent::DIE:
			Die();
			delete ev;
			ev = 0;
			die_event = 0;
		break;

		case ChordEvent::CHECK_PREDECESSOR:
			CheckPredecessor();
			check_predecessor_event = ev;
			Scheduler::Schedule(ev, predecessor_ping_interval, this);
		break;
		
		case ChordEvent::TIMEOUT:
			switch(ev->msg) {
				case CHECK_PREDECESSOR:
					delete check_predecessor_timeout_event;
					check_predecessor_timeout_event = 0;
					CheckPredecessorTimeout();
				break;
				
				case GET_PREDECESSOR:
					delete get_predecessor_timeout_event;
					get_predecessor_timeout_event = 0;
					GetPredecessorTimeout();
				break;
				
				case NOTIFY:
					delete notify_timeout_event;
					notify_timeout_event = 0;
					NotifyTimeout();
				break;
				
				case FIND_SUCCESSOR: // ChordResolver implements its own timeout
				default:
					CDBGPR(<< "Received unknown timeout even.");
			}
		break;
		
		case ChordEvent::IGNORE_MSG_ON:
			IgnoreMessagesOn();
			delete ev;
			ev = 0;
		break;
		
		case ChordEvent::IGNORE_MSG_OFF:
			IgnoreMessagesOff();
			delete ev;
			ev = 0;
		break;
		
		default:
			Application::Handle(e, t);
	}
}

void Chord::PrintFingerTable() {
	cout << endl
	     << Simulator::Now() << ": Finger Table for " << my_id << " running on "
	     << IPAddr::ToDotted(IPAddr(GetMyIP())) << ":" << port << endl;
	
	finger_table.Print();
	
	cout << "pred " << predecessor << endl << endl;
}

void Chord::SetJoinNode(IPAddr_t ip, PortId_t p) {
	join_ip = ip;
	join_port = p;
}

void Chord::Join() {
	Join(join_ip, join_port);
}

void Chord::Join(Time_t t) {
	join_event = new ChordEvent(ChordEvent::JOIN);
	Scheduler::Schedule(join_event, t, this);
}

void Chord::Join(Time_t t, IPAddr_t ip, PortId_t p) {
	SetJoinNode(ip, p);
	Join(t);
}

void Chord::Create(Time_t t) {
	create_event = new ChordEvent(ChordEvent::CREATE);
	Scheduler::Schedule(create_event, t, this);
}


// Chord Protocol Data Unit

ChordPDU::ChordPDU() : size(300){
}

ChordPDU::ChordPDU(Size_t s) {
	size = s;
}

ostream& operator<<(ostream &os, ChordPDU &cp) { // Overload operator in ostream class
	ChordId cid;
	IPAddr_t ip;
	
	os << "ChordPDU (at " << Simulator::Now() << "):" << endl;
	
	os << "  type:   ";
	if(cp.type == REQUEST)
		os << "REQUEST";
	else if (cp.type == RESPONSE)
		os << "RESPONSE";
	else
		os << "UNKNOWN";
	os << endl;
	
	os << "  msg:    ";
	if(cp.msg == NOTIFY)
		os << "NOTIFY";
	else if (cp.msg == FIND_SUCCESSOR)
		os << "FIND_SUCCESSOR";
	else if (cp.msg == GET_PREDECESSOR)
		os << "GET_PREDECESSOR";
	else if (cp.msg == CHECK_PREDECESSOR)
		os << "CHECK_PREDECESSOR";
	else
		os << "UNKNOWN";
	os << endl;	
	
	os << "  id:     " << cp.id << endl;
	
	cid = cp.finger.GetId();
	ip = cp.finger.GetIP();
	os << "  finger: ";
	os << cid << "  " << IPAddr::ToDotted(IPAddr(ip)) << ":" << cp.finger.GetPort() << endl;
	
	os << "  seq_no: " << cp.seq_no << endl
	   << "  reslvr: " << cp.resolver << endl
	   << "  redir:  " << cp.redirect << endl
	   << "  size:   " << cp.size << endl
	   << endl;
	
	return os;
}
