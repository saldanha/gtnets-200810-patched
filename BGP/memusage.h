// Compute memory usage by examining procfs
// George F. Riley, Georgia Tech, Spring 2000

unsigned long ReportMemUsage();
unsigned long ReportMemUsageMB();

