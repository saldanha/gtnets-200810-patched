// Sample test program to instantiate a BGP router
// Sunitha Beeram, Georgia Tech, Spring 2005

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "bgp.h"


#define ADV 1  //advertise
#define WTH 0  //withdraw

using namespace std;

int main(int argc,char ** argv)
{
  Simulator Sim;
  bool clear=false;

  int StopTime=5000;

  /* If stop time is specified as cli arg*/

  if ( argc > 1 )
  {
	if ( !strcmp(argv[1],"clear"))
		clear=true;
	else
		{
			printf("usage: ./flap-dampening-dbg\n");
			printf("       ./flag-dampening-dbg clear\n");
			exit(1);
		}
  }

  Node::DefaultShape(Node::CIRCLE);
  Linkp2p lk(Rate("1.5Mb"), Time("1ms"));
  Uniform startRng(0,0.1);

  /*First BGP Router*/
  Node* node1 = new Node();
  BGP* bgpRouter1 = new BGP(0);
  bgpRouter1->AttachNode(node1);
  bgpRouter1->config_file("./bgpd1.conf");

  /*Second BGP Router*/
  Node *node2 = new Node();
  BGP* bgpRouter2 = new BGP(0);
  bgpRouter2->AttachNode(node2);
  bgpRouter2->config_file("./bgpd2.conf");
  node1->AddDuplexLink(node2,lk);

  /*Third BGP Router*/
  Node *node3 = new Node();
  BGP* bgpRouter3 = new BGP(0);
  bgpRouter3->AttachNode(node3);
  bgpRouter3->config_file("./bgpd3.conf");
  node2->AddDuplexLink(node3,lk);

  char command1[]="no bgp dampening";
  char command2[]="network 10.0.3.0/24";
  char command3[]="show ip bgp 10.0.3.0/24";
  char command4[]="no network 10.0.3.0/24";
  char command5[]="show ip bgp";	

  if ( clear )
  {

	BGPEvent *ev1 = new BGPEvent(BGPEvent::COMMAND);
	ev1->argv = (char*)malloc(sizeof(char)*(strlen(command1)+1));
    strcpy(ev1->argv,command1);
	bgpRouter2->timer->Schedule(500,ev1);

	BGPEvent *ev2 = new BGPEvent(BGPEvent::COMMAND);
	ev2->argv = (char*)malloc(sizeof(char)*(strlen(command2)+1));
    strcpy(ev2->argv,command2);
	bgpRouter3->timer->Schedule(501,ev2);

	BGPEvent *ev3 = new BGPEvent(BGPEvent::COMMAND);
	ev3->argv = (char*)malloc(sizeof(char)*(strlen(command3)+1));
    strcpy(ev3->argv,command3);
	bgpRouter2->timer->Schedule(530,ev3);
  }


  int state = ADV;

  BGPEvent *ev;

  for ( int i=60 ; i < 430 ; i+=60)
  {
	if ( state == ADV ) 
	{
		ev = new BGPEvent(BGPEvent::COMMAND);
		ev->argv = (char*)malloc(sizeof(char)*(strlen(command4)+1));	
  		strcpy(ev->argv,command4);
		bgpRouter3->timer->Schedule(i,ev);


		ev = new BGPEvent(BGPEvent::COMMAND);
		ev->argv = (char*)malloc(sizeof(char)*(strlen(command3)+1));
    	strcpy(ev->argv,command3);
		bgpRouter1->timer->Schedule(i+59,ev);


		ev = new BGPEvent(BGPEvent::COMMAND);
		ev->argv = (char*)malloc(sizeof(char)*(strlen(command3)+1));
    	strcpy(ev->argv,command3);
		bgpRouter2->timer->Schedule(i+59,ev);

		state = WTH;
	}
	else
	{
		ev = new BGPEvent(BGPEvent::COMMAND);
		ev->argv = (char*)malloc(sizeof(char)*(strlen(command2)+1));	
  		strcpy(ev->argv,command2);
		bgpRouter3->timer->Schedule(i,ev);


		ev = new BGPEvent(BGPEvent::COMMAND);
		ev->argv = (char*)malloc(sizeof(char)*(strlen(command3)+1));
    	strcpy(ev->argv,command3);
		bgpRouter1->timer->Schedule(i+59,ev);


		ev = new BGPEvent(BGPEvent::COMMAND);
		ev->argv = (char*)malloc(sizeof(char)*(strlen(command3)+1));
    	strcpy(ev->argv,command3);
		bgpRouter2->timer->Schedule(i+59,ev);

		state = ADV;
	}
		
  }

  ev = new BGPEvent(BGPEvent::COMMAND);
  ev->argv = (char*)malloc(sizeof(char)*(strlen(command2)+1));	
  strcpy(ev->argv,command2);
  bgpRouter3->timer->Schedule(StopTime-2,ev);


  ev = new BGPEvent(BGPEvent::COMMAND);
  ev->argv = (char*)malloc(sizeof(char)*(strlen(command3)+1));
  strcpy(ev->argv,command3);
  bgpRouter2->timer->Schedule(StopTime-2,ev);


  ev = new BGPEvent(BGPEvent::COMMAND);
  ev->argv = (char*)malloc(sizeof(char)*(strlen(command5)+1));
  strcpy(ev->argv,command5);
  bgpRouter2->timer->Schedule(StopTime-1,ev);
		
  bgpRouter1->StartAt(0.1);
  bgpRouter2->StartAt(0.1);
  bgpRouter3->StartAt(0.1);
  bgpRouter1->StopAt(StopTime);
  bgpRouter2->StopAt(StopTime);
  bgpRouter3->StopAt(StopTime);

  Sim.StopAt(StopTime);
  Sim.Progress(1);
  Sim.Run();
}
 
