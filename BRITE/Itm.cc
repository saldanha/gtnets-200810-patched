/****************************************************************************/
/*   Georgia Tech Network Simulator - ITM (Internet Topology Modeler) class */
/*                                                                          */
/*   Title:  Interface between GTNetS & BRITE                               */
/*   Author: Talal M. Jaafar                                                */
/*   Revision:  1.1    12/02/2004                                           */
/*              1.2    12/09/2004                                            */
/*                                                                          */
/****************************************************************************/

/****************************************************************************/
/*                  Copyright 2001, Trustees of Boston University.          */
/*                               All Rights Reserved.                       */
/*                                                                          */
/* Permission to use, copy, or modify this software and its documentation   */
/* for educational and research purposes only and without fee is hereby     */
/* granted, provided that this copyright notice appear on all copies and    */
/* supporting documentation.  For any other uses of this software, in       */
/* original or modified form, including but not limited to distribution in  */
/* whole or in part, specific prior permission must be obtained from Boston */
/* University.  These programs shall not be used, rewritten, or adapted as  */
/* the basis of a commercial software or hardware product without first     */
/* obtaining appropriate licenses from Boston University.  Boston University*/
/* and the author(s) make no representations about the suitability of this  */
/* software for any purpose.  It is provided "as is" without express or     */
/* implied warranty.                                                        */
/*                                                                          */
/****************************************************************************/
/*                                                                          */
/*  Author:     Alberto Medina                                              */
/*              Anukool Lakhina                                             */
/*  Title:     BRITE: Boston university Representative Topology gEnerator   */
/*  Revision:  2.0         4/02/2001                                        */
/****************************************************************************/

#include <sstream>

#include "Itm.h"
#include "../BRITE/Brite.h"

//#include "node.h"         
#include "linkp2p.h"      
#include "ratetimeparse.h"

using namespace std;

template <typename T>
string ToString(const T & value)
{
    stringstream ss;
    ss << value;
    return ss.str();
}

vector<int> Itm::ASBorders;
int subLeafNum;                      // number of children per Leaf node
vector<int> maxLeafPerStubVec;
vector<int> maxStubPerBorderVec;
int minNetworkBits = 32;
char baseNetAddr[32] = "00001010";   // base network address
int lastASId = 0;                    // used for IP address assignment
vector< vector<Node*> > ASchildren;  // used to store the AS children

Itm::Itm(int ChildMaxNum, char* configFile)
{
#ifdef HAVE_QT
  Node::DefaultShape(Node::CIRCLE);
#endif
  Topology* topology;
  RouterWaxman* rt_wax_model;
  RouterBarabasiAlbert* rt_bar_model;
  ASWaxman* as_wax_model;
  ASBarabasiAlbert* as_bar_model;
  TopDownHierModel* td_model;
  BottomUpHierModel* bu_model;
  ImportedBriteTopologyModel* if_brite_model;
  ImportedGTitmTopologyModel* if_gtitm_model;  
  ImportedNLANRTopologyModel* if_nlanr_model;
  ImportedInetTopologyModel* if_inet_model;

  ModelPar* par;
  
  subLeafNum = ChildMaxNum;

  /* Create Parse object */
  Parse p(configFile);

  /* Parse configuration file */
  par = p.ParseConfigFile();
  assert(par != NULL);

  switch (par->GetModelType()) {
  case RT_WAXMAN:
    rt_wax_model = new RouterWaxman((RouterWaxPar*)par);
    topology = new Topology(rt_wax_model);
    break;

  case RT_BARABASI:
    rt_bar_model = new RouterBarabasiAlbert((RouterBarabasiAlbertPar*)par);
    topology = new Topology(rt_bar_model);
    break;

  case AS_WAXMAN:
    as_wax_model = new ASWaxman((ASWaxPar*)par);
    topology = new Topology(as_wax_model);
    break;

  case AS_BARABASI:
    as_bar_model = new ASBarabasiAlbert((ASBarabasiAlbertPar*)par);
    topology = new Topology(as_bar_model);
    break;
  
  case TD_HIER:
    td_model = new TopDownHierModel((TopDownPar*)par);
    topology = new Topology(td_model);
    break;

  case BU_HIER:
    bu_model = new BottomUpHierModel((BottUpPar*)par);
    topology = new Topology(bu_model);
    break;

  case IF_ROUTER:
  case IF_AS:

    switch (((ImportedFilePar*)par)->GetFormat()) {
    case ImportedFileModel::IF_BRITE:
      if_brite_model = new ImportedBriteTopologyModel((ImportedFilePar*)par);
      topology = new Topology(if_brite_model);
      break;

    case ImportedFileModel::IF_GTITM:
    case ImportedFileModel::IF_GTITM_TS:
      if_gtitm_model = new ImportedGTitmTopologyModel((ImportedFilePar*)par);
      topology = new Topology(if_gtitm_model);
      break;

    case ImportedFileModel::IF_NLANR:
      cout.flush();
      if_nlanr_model = new ImportedNLANRTopologyModel((ImportedFilePar*)par);
      topology = new Topology(if_nlanr_model);
      break;

    case ImportedFileModel::IF_INET:
      if_inet_model = new ImportedInetTopologyModel((ImportedFilePar*)par);
      topology = new Topology(if_inet_model);
      break;

    case ImportedFileModel::IF_SKITTER:
    default:
      cerr << "Itm(): Invalid file format for ImportedFileModel...\n";
      exit(0);
    }
    break;

  default:
    cerr << "Parsing error: invalid parameter structure returned...\n";
    exit(0);

  }

 
  // Run the classification algorithm
  topology->Classify();
  
  T = topology;  
  Graph* g = T->GetGraph();
  Model* mod = T->GetModel();
 
  
  // Check if the specified model type is valid
  switch (mod->GetType()) {
  case RT_WAXMAN:
    break;
  case RT_BARABASI:
    break;
  case AS_WAXMAN:
    break;
  case AS_BARABASI:
    break;
  case TD_HIER:
    break;
  case BU_HIER:
    break;
  case IF_ROUTER:
    break;
  case IF_AS:
    break;
  default:
    cerr << "Topology::Output(): Invalid model type (" << (int)mod->GetType() << ")  passed....\n";
    exit(0);
  }
  

  // Create the BRITE nodes and store their info to use them when creating the GTNetS nodes
  vector<int> nodeAS;          // to store the node AS number
  vector<AbsNode*> nodeVec;
  vector<int> nColor;          // to store the node color

  int maxAS = 0;
  int LeafSum = 0;

  vector<AbsNode*>::iterator endIter;
  for (int i = 0; i < g->GetNumNodes(); i++) {
    endIter = nodeVec.end();
    AbsNode* n = new AbsNode(i);


    switch(g->GetNodePtr(i)->GetNodeInfo()->GetNodeType()) {
    case NodeConf::RT_NODE:
      // store the node AS number
      maxAS = ((RouterNodeConf*)(g->GetNodePtr(i)->GetNodeInfo()))->GetASId();

      if(0)cout << "AS: " << ((RouterNodeConf*)(g->GetNodePtr(i)->GetNodeInfo()))->GetASId() << " | ";
      if(0)cout << "Node " << i << "-->";
      switch (((RouterNodeConf*)(g->GetNodePtr(i)->GetNodeInfo()))->GetRouterType()) {
      case RouterNodeConf::RT_NONE:
	if(0)cout << "RT_NONE " << endl;
	break;
      case RouterNodeConf::RT_LEAF:
	if(0)cout << "RT_LEAF " << endl;
	n->SetClassType(AbsNode::LEAF);
	LeafSum ++;
	break;
      case RouterNodeConf::RT_BORDER:
	if(0)cout << "RT_BORDER" << endl;
	n->SetClassType(AbsNode::BORDER);
	((RouterNodeConf*)n)->SetASId(maxAS);
	nodeAS.push_back(maxAS);  // store only the borders nodes for each AS
	break;
      case RouterNodeConf::RT_STUB:
	if(0)cout << "RT_STUB " << endl;
	n->SetClassType(AbsNode::STUB);
	break;
      case RouterNodeConf::RT_BACKBONE:
	if(0)cout << "RT_BACKBONE " << endl;
	n->SetClassType(AbsNode::BACKBONE);
	break;
      default:
	cerr << "Topology::Output(): Improperly classfied Router node encountered...\n";
	assert(0);
      }
      break;

    case NodeConf::AS_NODE:
      switch (((ASNodeConf*)(g->GetNodePtr(i)->GetNodeInfo()))->GetASType()) {
      case ASNodeConf::AS_NONE:
	if(0)cout << "AS_NONE ";
	break;
      case ASNodeConf::AS_LEAF:
	if(0)cout << "AS_LEAF ";
	break;
      case ASNodeConf::AS_STUB:
	if(0)cout << "AS_STUB ";
	break;
      case ASNodeConf::AS_BORDER:
	if(0)cout << "AS_BORDER ";
	break;
      case ASNodeConf::AS_BACKBONE:
	if(0)cout << "AS_BACKBONE ";
	break;
      default:
	cerr << "Topology::Output(): Improperly classfied AS node encountered...\n";
	assert(0);
      }
      break;
    }
    
    nodeVec.insert(endIter, n);
  }

 


  // Create the edges between the nodes
  list<Edge*>::iterator eList;
  list<Edge*> Elist = g->GetEdgeList();

  vector< vector<int> > Adj(g->GetNumNodes()); 
  for(eList = Elist.begin(); eList != Elist.end(); eList++) {
    // Do not find the adjacencies for the "BackBone" nodes
    if( (*eList)->GetSrc()->GetNodeInfo()->GetNodeType() ==  NodeConf::RT_NODE && 
	(*eList)->GetDst()->GetNodeInfo()->GetNodeType() ==  NodeConf::RT_NODE )
      {    
	// Neither Src nor Dst is a "BackBone" node
	if( ((RouterNodeConf*)(*eList)->GetSrc()->GetNodeInfo())->GetRouterType() !=  RouterNodeConf::RT_BACKBONE && 
	    ((RouterNodeConf*)(*eList)->GetDst()->GetNodeInfo())->GetRouterType() !=  RouterNodeConf::RT_BACKBONE )
	  {
	    Adj[(*eList)->GetSrc()->GetId()].push_back((*eList)->GetDst()->GetId());
	    Adj[(*eList)->GetDst()->GetId()].push_back((*eList)->GetSrc()->GetId());
	  }
      }
  }


  // now set the peers vector for every node
  for(int i=0; i < g->GetNumNodes(); i++)
    {
      if(!Adj[i].empty())
	nodeVec[i]->SetPeers(Adj[i]);
    }


  // determine how many borders each AS has
  for(int i=0; i < (maxAS + 1); i++) {
    ASBorders.push_back(i);
  }
  int as = 0;
  int count = 0;
  for(unsigned int i=0; i < nodeAS.size(); i++) {
    if(nodeAS[i] == as)
      count = count + 1;
    else {
      ASBorders[as] = count;
      as = nodeAS[i];        // the next AS that has borders
      count = 1;
    }
  }

  // initialize the maximum LeafPerStub/StubPerBorder vectors
  for(int i=0; i < (maxAS + 1); i++) {
    maxLeafPerStubVec.push_back(0);
    maxStubPerBorderVec.push_back(0);
  }
  

  if(0) {
    // debug info
    vector<AbsNode*>::iterator nodeIter;
    for(nodeIter = nodeVec.begin(); nodeIter != nodeVec.end(); nodeIter++) {
      vector<int> Peers = (*nodeIter)->GetPeers();
      cout << "Node " << (*nodeIter)->GetId() ;
      cout << " of type " << (*nodeIter)->GetClassType();
      cout << " belongs to AS " << ((RouterNodeConf*)(*nodeIter))->GetASId();
      cout << " with peers: ";
      
      for(unsigned int u=0; u < Peers.size(); u++)
	cout << Peers[u] << " ";
      cout << std::endl;
      
      int numOfLeafPeers = 0;
      if((*nodeIter)->GetClassType() == AbsNode::STUB) {
	for(unsigned int u=0; u < Peers.size(); u++) {
	  if(nodeVec[Peers[u]]->GetClassType() == AbsNode::LEAF)
	    numOfLeafPeers ++;
	}
	cout << "\t\t numOfLeafPeers is: " << numOfLeafPeers << std::endl;
      } 
    }//end of for
  }
  
  
  // initialize all nodes to white color (undiscovered)
  for(int i=0; i < g->GetNumNodes(); i++)
    nColor.push_back(0);

  // determine the size of every border and stub
  for(unsigned int i=0; i < nodeVec.size(); i++) {
    // do a BFS for the border nodes only
    if(nodeVec[i]->GetClassType() == AbsNode::BORDER) {
      BFS_Size(nodeVec, i, nColor);
    }
  }
  
  
  // get the smallest number of network bits (this will be our base network address)
  int maxBorderBits = 0;
  int maxStubBits = 0;
  int maxLeafBits = 1;   /* To Do:  need to double check this with BRITE folks */
  int maxChildBits = 0;
  int newMinNetworkBits = 32;
  for(int i=0; i < (maxAS + 1); i++) {
    maxBorderBits = NumOfBits(ASBorders[i]);
    if(NumOfBits(maxStubPerBorderVec[i]) > maxStubBits)
      maxStubBits = NumOfBits(maxStubPerBorderVec[i]);
    if(NumOfBits(maxLeafPerStubVec[i]) > maxLeafBits)
      maxLeafBits = NumOfBits(maxLeafPerStubVec[i]);
    maxChildBits = NumOfBits(subLeafNum*2);
  
    newMinNetworkBits = 32 - (maxBorderBits + maxStubBits + maxLeafBits + maxChildBits);
    if(newMinNetworkBits < minNetworkBits)
      minNetworkBits = newMinNetworkBits;
  }

  // construct the base network address
  for(int i=0; i < (minNetworkBits - 8); i++) {
    if(i == (minNetworkBits - 9))
      strcat(baseNetAddr,"1");
    else
      strcat(baseNetAddr,"0");
  }

  // initialize again all nodes to white color (undiscovered)
  nColor.clear();
  for(int i=0; i < g->GetNumNodes(); i++)
    nColor.push_back(0);

  // set IP addresses of the leaf nodes back to -1 since they were changed in the BFS_SIZE() call earlier
  for(unsigned int i=0; i < nodeVec.size(); i++) {
    if(nodeVec[i]->GetClassType() == AbsNode::LEAF)
      nodeVec[i]->SetIPAddress(-1);
  }


  int borderNum = 1;
  int currentAS = 0;
  int ASId = 0;
  for(unsigned int i=0; i < nodeVec.size(); i++) {
    // do a BFS for the border nodes only
    if(nodeVec[i]->GetClassType() == AbsNode::BORDER) {
      // encountered a border in the same AS
      ASId = ((RouterNodeConf*)nodeVec[i])->GetASId();
      if(currentAS == ASId) {
	BFS_IP(nodeVec, i, nColor, ASBorders[currentAS], borderNum);
	borderNum = borderNum + 1;
      }
      // encountered a border in a different AS
      else {  
	borderNum = 1;
	currentAS = ASId;
	BFS_IP(nodeVec, i, nColor, ASBorders[currentAS], borderNum);
	borderNum = borderNum + 1;
      }
    }
  }

  
  // Create the GTNetS nodes (no children are created yet)
  vector<Node*> GTnodeVec;
  vector<Node*>::iterator GTendIter;
  for (int i = 0; i < g->GetNumNodes(); i++) {
    GTendIter = GTnodeVec.end();
    Node* n = new Node();
    n->SetLocation(g->GetNodePtr(i)->GetNodeInfo()->GetCoordX(),
		   g->GetNodePtr(i)->GetNodeInfo()->GetCoordY());
    GTnodeVec.insert(GTendIter, n);  
  }

  // Create the edges between the GTNetS nodes
  Random* U = new Uniform(10, 512);  // to be used for assigning BW to GT-ITM links
  for(eList = Elist.begin(); eList != Elist.end(); eList++) {
    Node* vertex1 = GTnodeVec[(*eList)->GetSrc()->GetId()];
    Node* vertex2 = GTnodeVec[(*eList)->GetDst()->GetId()];
    
    // Set the link properties
    Edge* ed = *eList;
  
    double Rval = (ed->GetConf())->GetBW();
    if(Rval == -1) // we're using GT-ITM
      Rval = (double)U->IntValue();
    string rate = ToString(Rval) + "Mb";
 
    double Dval = ((RouterEdgeConf*)ed->GetConf())->GetDelay();
    string delay = ToString(Dval) + "ms";
    
    //Create the link between the vertices
    Linkp2p l(Rate(rate.c_str()), Time(delay.c_str()));
    
    vertex1->AddDuplexLink(vertex2, l);
  }

  delete topology;

  // Create the children nodes, and their links
  vector<Node*> GTchildVec;
  Linkp2p childL(Rate("10Mb"), Time("5ms"));

  // Map the children to their corresponding ASs
  vector<Node*> temp;
  for(int i=0; i < (maxAS+1); i++)
    ASchildren.push_back(temp);
  
  for(unsigned int i=0; i < nodeVec.size(); i++) {
    // for every BRITE leaf node, create subLeafNum of GTNetS nodes
    if(nodeVec[i]->GetClassType() == AbsNode::LEAF && (strcmp(nodeVec[i]->GetIPAddressStr(),"") != 0) ) {
      int AS = ((RouterNodeConf*)nodeVec[i])->GetASId();
      int childIndex = 1;
      // Create a random number of children per leaf node
      Random* rndChildNum = new Uniform(0,subLeafNum);
      for(unsigned int j=0; j < rndChildNum->IntValue(); j++) {
	Node* n = new Node();
	// the child nodes won't appear in the animation unless we specify their locations (which we do not need in our case)
	char* ipAddr = nodeVec[i]->GetIPAddressStr();
	char ip1[32];
	strcpy(ip1,ipAddr);
	strcat(ip1,IntToBits(NumOfBits(subLeafNum*2),childIndex));
	childIndex = childIndex + 1;
	char ip2[32];
	strcpy(ip2,ipAddr);
	strcat(ip2,IntToBits(NumOfBits(subLeafNum*2),childIndex));
	childIndex = childIndex + 1;
	n->AddDuplexLink(GTnodeVec[i], childL, IPAddr(BitsToInt(ip1, 32)), Mask(32), IPAddr(BitsToInt(ip2, 32)));
	GTchildVec.push_back(n);
	ASchildren[AS].push_back(n);
      }
    }
  }

  if(0) {
    int AS = ASchildren.size();
    cout << "number of ASes is: " << AS << std::endl;
    for(unsigned int i = 0; i < GTchildVec.size(); i++)
      cout << "childIP is: " << IPAddr::ToDotted(GTchildVec[i]->GetIPAddr()) << std::endl;
  }
	
}


void Itm::BFS_IP(vector<AbsNode*>& N, int rootIndex, vector<int>& color, int numOfBorders, int borderNum)
{
  if(0)
    cout << "border " << borderNum << " in AS " << ((RouterNodeConf*)N[rootIndex])->GetASId() 
	 << " with " << numOfBorders << " borders" << std::endl;

  std::deque<AbsNode*> Q;
  vector<int> peers;
  vector<int>::iterator peersIter;

  int borderBits = NumOfBits(numOfBorders);
  int stubBits = NumOfBits(N[rootIndex]->GetMaxBorderStubs());
  int leafBits = NumOfBits(N[rootIndex]->GetMaxStubLeaves());
  int subLeafBits = NumOfBits(subLeafNum*2);
  int networkBits = 32 - (borderBits + stubBits + leafBits + subLeafBits);
  
  if(0) {
    cout << "borderBits is: " << borderBits << std::endl;
    cout << "stubBits is: " << stubBits << std::endl;
    cout << "leafBits is: " << leafBits << std::endl;
    cout << "networkBits are: " << networkBits << std::endl;
  }

  int stubIndex = 0;
  int leafIndex = 0;
  char ipAddr[32] = "";
  unsigned long temp = 0;

  // set the root color to gray
  color[N[rootIndex]->GetId()] = 1;

  // add the root to the Q
  Q.push_back(N[rootIndex]);
  
  while(!Q.empty()) {
    // get adjacent peers for the first node in the queue
    peers = (Q.front())->GetPeers();
    if(Q.front()->GetClassType() == AbsNode::STUB)
      // even if the leaf node is attached directly to the border, we consider it as another subnetwork
      stubIndex = stubIndex + 1;
    leafIndex = 1;
    for(peersIter = peers.begin(); peersIter != peers.end(); peersIter++) {
      
      // node is not discovered yet
      if( color[(*peersIter)] == 0 ) {
	// ignore other borders
	if( N[(*peersIter)]->GetClassType() !=  AbsNode::BORDER ) {
	  if( N[(*peersIter)]->GetClassType() ==  AbsNode::LEAF && N[(*peersIter)]->GetIPAddress() == -1) {
	    
	    if((Q.front())->GetClassType() == AbsNode::BORDER)
	      stubIndex = stubIndex + 1;
	    
	    if(((RouterNodeConf*)N[rootIndex])->GetASId() == lastASId)   // keep the same base network
	      strcpy(ipAddr, baseNetAddr);
	    else { // increment it
	      temp = BitsToInt(baseNetAddr, minNetworkBits);
	      temp = temp + 1;
	      strcpy(baseNetAddr, IntToBits(minNetworkBits, temp));
	      strcpy(ipAddr, baseNetAddr);
	      lastASId = ((RouterNodeConf*)N[rootIndex])->GetASId();
	    }
	    strcat(ipAddr, IntToBits(borderBits, borderNum));
	    strcat(ipAddr, IntToBits(stubBits, stubIndex));
      	    strcat(ipAddr, IntToBits (leafBits, leafIndex));
	    // complete IPAddr will be done later on when creating the child nodes
	    if(0)cout << "IPAddr of this Leaf node with Id of " << N[(*peersIter)]->GetId() 
		      << " is: " << IPAddr::ToDotted(BitsToInt(ipAddr, 32)) << std::endl;
	    N[(*peersIter)]->SetIPAddressStr(ipAddr);
	    ((RouterNodeConf*)N[(*peersIter)])->SetASId(((RouterNodeConf*)N[rootIndex])->GetASId());
	  }
	else { // it is a stub node
	  // do not consider other stubs as peers from a stub point
	  if((Q.front())->GetClassType() == AbsNode::BORDER) {
	    color[(*peersIter)] = 1;
	    Q.push_back(N[(*peersIter)]);
	  }
	}//else
	}//if
      }//if
    }//for
    Q.pop_front();
  }//while
  
}


void Itm::BFS_Size(vector<AbsNode*>& N, int rootIndex, vector<int>& color)
{
  std::deque<AbsNode*> Q;
  vector<int> peers;
  vector<int>::iterator peersIter;

  // set the root color to gray
  color[N[rootIndex]->GetId()] = 1;

  // add the root to the Q
  Q.push_back(N[rootIndex]);

  while(!Q.empty()) {
    // get adjacent peers for the first node in the queue
    peers = (Q.front())->GetPeers();
    for(peersIter = peers.begin(); peersIter != peers.end(); peersIter++) {
      // node is not discovered yet
      if( color[(*peersIter)] == 0 ) {
	// ignore other borders
	if( N[(*peersIter)]->GetClassType() !=  AbsNode::BORDER ) {
	  if( N[(*peersIter)]->GetClassType() ==  AbsNode::LEAF && N[(*peersIter)]->GetIPAddress() == -1) {
	    N[(*peersIter)]->SetIPAddress(-2);
	    N[(Q.front())->GetId()]->IncrSize();  // Increment the number of leaf nodes per stub or border
	    if((Q.front())->GetClassType() == AbsNode::STUB)
	      if(  N[(Q.front())->GetId()]->GetSize() > maxLeafPerStubVec[((RouterNodeConf*)N[rootIndex])->GetASId()])
		maxLeafPerStubVec[((RouterNodeConf*)N[rootIndex])->GetASId()] = N[(Q.front())->GetId()]->GetSize();
	  }
	  else { // it is a stub node
	    // do not consider other stubs as peers from a stub point
	    if((Q.front())->GetClassType() == AbsNode::BORDER) {
	      color[(*peersIter)] = 1;
	      N[(Q.front())->GetId()]->IncrSize();  // Increment the number of stub nodes per border node
	      if(  N[(Q.front())->GetId()]->GetSize() > maxStubPerBorderVec[((RouterNodeConf*)N[rootIndex])->GetASId()])
		maxStubPerBorderVec[((RouterNodeConf*)N[rootIndex])->GetASId()] = N[(Q.front())->GetId()]->GetSize();
	      Q.push_back(N[(*peersIter)]);
	    }
	  }//else
	}//if
      }//if
    }//for
    Q.pop_front();
  }//while
  
  N[rootIndex]->SetMaxStubLeaves(maxLeafPerStubVec[((RouterNodeConf*)N[rootIndex])->GetASId()]);
  N[rootIndex]->SetMaxBorderStubs(maxStubPerBorderVec[((RouterNodeConf*)N[rootIndex])->GetASId()]);
}


int Itm::NumOfBits(int num) {
  
  int max = 2;
  int index = 0;
  if(num == 0 || num == 1)
    index = 1;
  else {
    while(num >= max) {
      max = max * 2;
      index ++;
    }
    index = index + 1;
  }
  return index;
}


const char* Itm::IntToBits(int numBits, int num) {
  
  const char* bitsChar;
  string bitStr = ""; 
  for (int i = (numBits - 1); i >= 0; i--) {
    int bit = ((num >> i) & 1);
    bitStr = bitStr + ToString(bit);
  }
  
  bitsChar = bitStr.c_str();
  return bitsChar;
}


unsigned long Itm::BitsToInt(char* bits, int numBits) {
 
  unsigned long sum =0;
  unsigned long bitValue = 0;
  for(int i=0; i<numBits; i++) {
    if(bits[i] == '1') {
      bitValue = 1;
      for(int j=0; j<(numBits - 1 -i); j++)
	bitValue = bitValue * 2;
      sum = sum + bitValue;      
    }
  }
  return sum;
}


int Itm::GetASCount() {

  return ASchildren.size();
}


vector<Node*> Itm::GetASchildren(unsigned int ASnum) {
  
  if(ASnum > (ASchildren.size()-1))
    cerr << "Invalid AS Id" << std::endl;
  return ASchildren[ASnum];
}
