// Sample test program to instantiate a BGP router
// Sunitha Beeram, Georgia Tech, Spring 2005

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "bgp.h"


using namespace std;

int main(int argc,char ** argv)
{
  Simulator Sim;

  int StopTime=100;

  /* If stop time is specified as cli arg*/

  if ( argc > 1)
  {
	StopTime= atoi(argv[1]);
	
	if (StopTime < 100)
		StopTime =100;
  }

  Node::DefaultShape(Node::CIRCLE);
  //Linkp2p lk(Rate("1.5Mb"), Time("1ms"));
  Linkp2p lk(Rate("500Kb"), Time("20ms"));
  Uniform startRng(0,0.1);

  /*First BGP Router*/
  Node* node1 = new Node();
  BGP* bgpRouter1 = new BGP(0);

  bgpRouter1->AttachNode(node1);
  bgpRouter1->config_file("./bgpd1.conf");

  /*Second BGP Router*/
  Node *node2 = new Node();
  BGP* bgpRouter2 = new BGP(0);
  
  bgpRouter2->AttachNode(node2);
  bgpRouter2->config_file("./bgpd2.conf");
  node1->AddDuplexLink(node2,lk);

  /*Third BGP Router*/
  
  Node *node3 = new Node();
  BGP* bgpRouter3 = new BGP(0);
  
  bgpRouter3->AttachNode(node3);
  bgpRouter3->config_file("./bgpd3.conf");
  node2->AddDuplexLink(node3,lk);
  node1->AddDuplexLink(node3,lk);


  /*Fourth BGP Router*/ 
  Node *node4 = new Node();
  BGP* bgpRouter4 = new BGP(0);
  
  bgpRouter4->AttachNode(node4);
  bgpRouter4->config_file("./bgpd4.conf");
  node1->AddDuplexLink(node4,lk);

  /*Fifth BGP Router*/
  Node *node5 = new Node();
  BGP* bgpRouter5 = new BGP(0);
  
  bgpRouter5->AttachNode(node5);
  bgpRouter5->config_file("./bgpd5.conf");
  node4->AddDuplexLink(node5,lk);

  /*Sixth BGP Router*/
  Node *node6 = new Node();
  BGP* bgpRouter6 = new BGP(0);
  
  bgpRouter6->AttachNode(node6);
  bgpRouter6->config_file("./bgpd6.conf");
  node4->AddDuplexLink(node6,lk);

  /*Seventh BGP Router*/

  Node *node7 = new Node();
  BGP* bgpRouter7 = new BGP(0);
  
  bgpRouter7->AttachNode(node7);
  bgpRouter7->config_file("./bgpd7.conf");
  node6->AddDuplexLink(node7,lk);

  /*Eigth BGP Router*/

  Node *node8 = new Node();
  BGP* bgpRouter8 = new BGP(0);
  
  bgpRouter8->AttachNode(node8);
  bgpRouter8->config_file("./bgpd8.conf");
  node6->AddDuplexLink(node8,lk);
  node7->AddDuplexLink(node8,lk);

  /*Ninth*/
  Node *node9 = new Node();
  BGP* bgpRouter9 = new BGP(0);
  
  bgpRouter9->AttachNode(node9);
  bgpRouter9->config_file("./bgpd9.conf");
  node6->AddDuplexLink(node9,lk);
  node7->AddDuplexLink(node9,lk);
  node8->AddDuplexLink(node9,lk);

  /*Tenth*/
  Node *node10 = new Node();
  BGP* bgpRouter10 = new BGP(0);
  
  bgpRouter10->AttachNode(node10);
  bgpRouter10->config_file("./bgpd10.conf");
  node1->AddDuplexLink(node10,lk);

  /*Eleventh*/
  Node *node11 = new Node();
  BGP* bgpRouter11 = new BGP(0);
  
  bgpRouter11->AttachNode(node11);
  bgpRouter11->config_file("./bgpd11.conf");
  node4->AddDuplexLink(node11,lk);

  /*Add events*/

  char command[] ="show ip bgp";

  BGPEvent *ev1 = new BGPEvent(BGPEvent::COMMAND);
  ev1->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev1->argv,command);
  bgpRouter1->timer->Schedule(StopTime-1,ev1);

  BGPEvent *ev2 = new BGPEvent(BGPEvent::COMMAND);
  ev2->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev2->argv,command);
  bgpRouter2->timer->Schedule(StopTime-1,ev2);

  BGPEvent *ev3 = new BGPEvent(BGPEvent::COMMAND);
  ev3->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev3->argv,command);
  bgpRouter3->timer->Schedule(StopTime-1,ev3);

  BGPEvent *ev4 = new BGPEvent(BGPEvent::COMMAND);
  ev4->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev4->argv,command);
  bgpRouter4->timer->Schedule(StopTime-1,ev4);

  BGPEvent *ev5 = new BGPEvent(BGPEvent::COMMAND);
  ev5->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev5->argv,command);
  bgpRouter5->timer->Schedule(StopTime-1,ev5);

  BGPEvent *ev6 = new BGPEvent(BGPEvent::COMMAND);
  ev6->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev6->argv,command);
  bgpRouter6->timer->Schedule(StopTime-1,ev6);

  BGPEvent *ev7 = new BGPEvent(BGPEvent::COMMAND);
  ev7->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev7->argv,command);
  bgpRouter7->timer->Schedule(StopTime-1,ev7);

  BGPEvent *ev8 = new BGPEvent(BGPEvent::COMMAND);
  ev8->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev8->argv,command);
  bgpRouter8->timer->Schedule(StopTime-1,ev8);

  BGPEvent *ev9 = new BGPEvent(BGPEvent::COMMAND);
  ev9->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev9->argv,command);
  bgpRouter9->timer->Schedule(StopTime-1,ev9);

  BGPEvent *ev10 = new BGPEvent(BGPEvent::COMMAND);
  ev10->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev10->argv,command);
  bgpRouter10->timer->Schedule(StopTime-1,ev10);

  BGPEvent *ev11 = new BGPEvent(BGPEvent::COMMAND);
  ev11->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev11->argv,command);
  bgpRouter11->timer->Schedule(StopTime-1,ev11);

  bgpRouter1->StartAt(0.1);
  bgpRouter2->StartAt(0.1);
  bgpRouter3->StartAt(0.1);
  bgpRouter4->StartAt(0.1);
  bgpRouter5->StartAt(0.1);
  bgpRouter6->StartAt(0.1);
  bgpRouter7->StartAt(0.1);
  bgpRouter8->StartAt(0.1);
  bgpRouter9->StartAt(0.1);
  bgpRouter10->StartAt(0.1);
  bgpRouter11->StartAt(0.1);
  bgpRouter1->StopAt(StopTime);
  bgpRouter2->StopAt(StopTime);
  bgpRouter3->StopAt(StopTime);
  bgpRouter4->StopAt(StopTime);
  bgpRouter5->StopAt(StopTime);
  bgpRouter6->StopAt(StopTime);
  bgpRouter7->StopAt(StopTime);
  bgpRouter8->StopAt(StopTime);
  bgpRouter9->StopAt(StopTime);
  bgpRouter10->StopAt(StopTime);
  bgpRouter11->StopAt(StopTime);
  
  Sim.StopAt(StopTime);
  Sim.Run();
}
 
