// Create a latex compatible .tex file with sample cc programs
// Used by the GTSim reference manual.
// George F. Riley, Georgia Tech, Fall 2002

#include <stdio.h>

#include <fstream>
#include <iostream>
#include <string>

using namespace std;

string pound("\\#");
string space("\\ ");
string leftleft("<{}<");
string leftarrow("<");
string rightright(">{}>");
string rightarrow(">");
string bf("{\\bf ");
string endbf("}&");
string backback("\\\\");
string openbracket("\\{");
string closebracket("\\}");
string underscore("\\_");
string percent("\\%");
string andsym("\\&");
string backslash("$\\backslash$");
string dollar("\\$");
string letters("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
string whitespace("\011\012\013\014\015\040");
string tilde("\\symbol{126}");
string minuschar("\\symbol{45}");

const int linesperpage = 56;
string ccfn;
string ccfnbase;
string ccfnNoPath;
string ccfnbaseNoPath;
string texfn;
string openenv;
bool   needClose;

// Helpers
bool StartsWith(const string& s1, const char* s2)
{ // True if string s1 starts with s2
  std::string::const_iterator i = s1.begin();
  while(*i == ' ' && i != s1.end()) ++i; // Skip leading spaces
  while(*s2)
    {
      if (i == s1.end()) return false;   // Nope
      if (*i++ != *s2++) return false;   // Nope
    }
  return true; // Yes
}

string After(const string& s1, const char* s2)
{ // Return remainder of string s1 after s2
  std::string::size_type l = s1.find(s2);
  l += strlen(s2);
  return s1.substr(l, std::string::npos);
}

string LettersOnly(const string& s)
{ // Return a string with only the letters (a-z, A-Z) in the string
  string::size_type i = 0;
  string r;
  
  while(1)
    {
      string::size_type j = s.find_first_not_of(letters, i);
      if (i != j)
        r += s.substr(i, j);
      if (j == string::npos)
        { // Not found
          return r;
        }
      i = j + 1;
      if (i == s.length()) return r;
    }
}

string NoWhitespace(const string& s)
{
  string::size_type i = 0;
  string r;
  
  while(1)
    {
      string::size_type j = s.find_first_of(whitespace, i);
      if (i != j)
        r += s.substr(i, j);
      if (j == string::npos)
        { // Not found
          return r;
        }
      i = j + 1;
      if (i == s.length()) return r;
    }
}

void OutputOneLine(ofstream& tex, int& linenum, string& inln) // This goes 80xx
{
  string outln;
  if (needClose)
    {
      tex << "}" << backback << endl;
      needClose = false;
    }
  
  if (linenum && (linenum % linesperpage == 0))
    { // Need new page
      tex << "\\end{tabular}" << endl;
      tex << "\\end{small}" << endl;
      tex << "\\end{tt}" << endl;
      tex << "}" << endl; // Close the \vbox
      tex << "\\end{" << openenv << "}" << endl;
      
      tex << "\\begin{programcontinued}{" << ccfn << "}" << endl;
      openenv = "programcontinued";
      tex << "\\vbox{" << endl;
      tex << "\\begin{tt}" << endl; 
      tex << "\\begin{small}" << endl;
      tex << "\\begin{tabular}{rl}" << endl;
    }
  char work[255];
  sprintf(work, "%d", ++linenum);
  outln.append(bf + string(work) + endbf + string("{\\progln "));
  needClose = true;
  
  for (string::iterator i = inln.begin(); i != inln.end(); ++i)
    { // process each character
      char ch = *i;
      switch(ch) {
      case '#' :
        outln.append(pound);
        break;
      case  ' ':
        outln.append(space);
        break;
      case  '{':
        outln.append(openbracket);
        break;
      case  '}':
        outln.append(closebracket);
        break;
      case  '_':
        outln.append(underscore);
        break;
      case  '%':
        outln.append(percent);
        break;
      case  '&':
        outln.append(andsym);
        break;
      case  '$':
        outln.append(dollar);
        break;
      case  '~':
        outln.append(tilde);
        break;
      case  '-':
        outln.append(minuschar);
        break;
      case  '\\':
        outln.append(backslash);
        break;
      case '<' :
        {
          string::iterator nexti = i + 1;
          char nch = (nexti == inln.end() ? ' ' : *nexti);
          if (nch == '<') 
            {
              outln.append(leftleft);
              i++;
            }
          else
            {
              outln.append(leftarrow);
            }
        }
        break;
      case '>' :
        {
          string::iterator nexti = i + 1;
          char nch = (nexti == inln.end() ? ' ' : *nexti);
          if (nch == '>') 
            {
              outln.append(rightright);
              i++;
            }
          else
            {
              outln.append(rightarrow);
            }
        }
        break;
      default:
        {
          char work[2];
          work[0] = ch;
          work[1] = 0;
          outln.append(work);
        }
        break;
      }
    }
  //tex << outln << backback << endl;
  tex << outln;
  needClose = true;
}

int main(int argc, char** argv)
{
  if (argc < 2) exit(1);
  for (int which = 1; which < argc; ++which)
    { // Convert each file
      ccfn = argv[which]; // The name of the cc file
      string::size_type lp = ccfn.find_last_of('.');
      if (lp == string::npos)
        { // No periods, just append ".tex"
          ccfnbase = ccfn;
          texfn = ccfn + string(".tex");
        }
      else
        { // Replace everything after the final . with ".tex"
          texfn = ccfn.substr(0, lp) + string(".tex");
          ccfnbase = ccfn.substr(0, lp);
        }

      lp = ccfn.find_last_of('/');
      if (lp == string::npos)
        { // No subdir, just copy
          ccfnNoPath = ccfn;
        }
      else
        { // Just the file name after the path
          ccfnNoPath = ccfn.substr(lp+1, string::npos);
        }
      
      lp = ccfnbase.find_last_of('/');
      if (lp == string::npos)
        { // No subdir, just copy
          ccfnbaseNoPath = ccfnbase;
        }
      else
        { // Just the file name after the path
          ccfnbaseNoPath = ccfnbase.substr(lp+1, string::npos);
        }
      
      if(0)cout << "texfn " << texfn << endl;
      ifstream cc(argv[which]);    // The .cc file
      if (!cc)
        {
          cout << "Can't open input file " << ccfn << endl;
          exit(2);
        }
      ofstream tex(texfn.c_str()); // The .tex file
      if (!tex)
        {
          cout << "Can't open output file " << texfn << endl;
          exit(3);
        }
      // Add some header information
      tex << "% Generated automatically by cc2tex from " << ccfn
          << "  DO NOT EDIT" << endl;
      tex << "\\begin{program}{" << ccfnNoPath << "}" << endl;
      openenv = "program";
      tex << "\\vbox{" << endl;
      tex << "\\nobreak" << endl;
      tex << "\\begin{tt}" << endl;
      tex << "\\begin{small}" << endl;
      tex << "\\begin{tabular}{rl}" << endl;
      needClose = false;
      int linenum = 0;
      int bufferedBlanks = 0;
      while(cc)
        {
          string inln;
          string outln;
          getline(cc, inln); // Get the next line
          if (string::size_type eol = inln.find_last_of('\n') != string::npos)
            { // remove trailing eol
              inln.erase(eol);
            }
          if (inln.size() == 0)
            {
              bufferedBlanks++;
              continue;
            }
          if (StartsWith(inln, "//Doc:"))
            { // Documentation comment, do not include
              if (StartsWith(inln, "//Doc:Label:"))
                { // Define the reference label
                  string labelName = NoWhitespace(After(inln, "//Doc:Label:"));
//                   tex << "\\gdef\\Prog" << ccfnbase << "Line" << labelName
//                       << "{" << linenum << "}" << endl;
                  tex << "\\label{Prog:" << ccfnbaseNoPath 
                      << ":Line:" << labelName << "}";
                }
              continue;
            }
          
          if (StartsWith(inln, "//NewPage"))
            { // Put a new page in the tex file
              linenum = linesperpage;
              // and output the buffered blanks
              string bl;
              OutputOneLine(tex, linenum, bl);
              continue;
            }
          
              
          for (int bb = 0; bb < bufferedBlanks; ++bb)
            { // Output the buffered blanks
              string bl;
              OutputOneLine(tex, linenum, bl);
            }
          bufferedBlanks = 0;
          OutputOneLine(tex, linenum, inln);
        }
      if (needClose)
        {
          tex << "}" << backback << endl;
          needClose = false;
        }
      tex << "\\end{tabular}" << endl;
      tex << "\\end{small}" << endl;
      tex << "\\end{tt}" << endl;
      tex << "}" << endl; // Close the \vbox
      tex << "\\end{" << openenv << "}" << endl;
    }
}
