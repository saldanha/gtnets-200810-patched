// Test the link16 wireless lan and interfaces
// George F. Riley, Georgia Tech, Summer 2005

#include <iostream>

#include "link16.h"
#include "interface-link16.h"
#include "ratetimeparse.h"
#include "application-onoff.h"
#include "rng.h"

using namespace std;

int main(int argc, char** argv)
{
  Simulator s;
  
  Count_t nNodes = 16;
  IPAddr_t baseIP = IPAddr("192.168.1.1");
  Link16 l16(nNodes, baseIP);
  l16.BoundingBox(Location(0,0), Location(100,100));
  l16.SetRadioRange(125.0);
  // Calculate rate for each cbr app
  Rate_t cbrRate = l16.Bandwidth() / 1.5;
  
  // Add an on/off app and slots on each node on each node
  Uniform peerRng(0, l16.NodeCount());
  Uniform startRng(0, 1.0);
  
  for (Count_t i = 0; i < l16.NodeCount(); ++i)
    {
      Node* n = l16.GetNode(i);
      InterfaceLink16* pif = (InterfaceLink16*)n->Interfaces()[0];
      // Add slots
      Count_t thisSlot = i;
      while(thisSlot < l16.nSlots)
        {
          cout << "Adding slot " << thisSlot
               << " to node " << i << endl;
          pif->AddSlot(thisSlot);
          thisSlot += l16.NodeCount();
        }
      
      Count_t peer;
      while(true)
        { // Get a peer other than self
          peer = peerRng.IntValue();
          if (peer != i) break;
        }
      
      OnOffApplication* ooa = (OnOffApplication*)n->AddApplication(
          OnOffApplication(baseIP + peer, 10000, 
                           Exponential(Time("100ms")),
                           UDP(), cbrRate * 2, 512));
      ooa->Start(startRng.Value());
    }
  
  s.StartAnimation(0, true);
  s.AnimationUpdateInterval(Time("100us")); // 100us initial update rate
  s.StopAt(1000);
  s.Run();
}

