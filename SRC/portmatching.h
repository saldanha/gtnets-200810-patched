/*

* GTNetS provides a portable C++ class library for network simulations
*
* Copyright (C) 2003 George F. Riley
*
* No guarantees or warranties or anything are provided or implied in any way
* whatsoever.  Use this program at your own risk.  Permission to use this
* program for any purpose is given, as long as the copyright is kept intact.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

* Permission is hereby granted for any non-commercial use of this code

*/

// Georgia Tech Network Simulator - PortMatching class
// Mohamed Abd Elhafez.  Georgia Tech, Spring 2004

// Implements the port matching algorithm for worm containment

#ifndef __port_matching_h__
#define __port_matching_h__

#include <queue>

#include "common-defs.h"
#include "pdu.h"
#include "ipaddr.h"
#include "ipv4.h"
#include "node.h"
#include "tcp.h"
#include "worm-containment.h"

#define TCP_PROTOCOL 6
#define UDP_PROTOCOL 17

class PortTime{
 public:
  PortTime(PortId_t p, Time_t t):port(p), time(t) {}
 public:
  PortId_t port;
  Time_t   time;
};

typedef std::vector<IPAddr_t> IPVec_t;
typedef std::vector<PortTime> PortVec_t;
typedef std::map<PortId_t, Time_t> PortMap_t;
typedef std::pair<IPVec_t, double> IPNPair_t;
typedef std::map<PortId_t, IPNPair_t> SPortMap_t;
typedef std::vector<Interface*> IFVec_t;

// Define the event subclass for DEWP  events
//Doc:ClassXRef
class DEWPEvent : public Event {
 public:
  typedef enum { PROCESS_QUEUE } DEWPEvent_t;
 public:
  DEWPEvent ()   { }
  DEWPEvent(Event_t ev)
    : Event(ev)
    { }
  virtual ~DEWPEvent() { }
 
};

//Doc:ClassXRef
class PortMatching : public WormContainment, public Handler{
//Doc:Class The class {\tt PortMatching} implements the port matching algorithm
//Doc:Class It derives 	from the class {\tt Handler}. 
	
public:
  //Doc:Method
  PortMatching();
  //Doc:Desc Default constructor

  // Handler
  virtual void Handle(Event*, Time_t);

  //Doc:Method
  //void ProcessInPacket(Packet*, Interface*);
  //Doc:Desc Process the given packet for TCP SYN packets or UDP packets
  
 //Doc:Method
  void ProcessOutPacket(Packet*, IPAddr_t, int, Interface*);
  //Doc:Desc Process the given packet for TCP SYN packets or UDP packets

  //Doc:Method
  void AddIP(SPortMap_t::iterator, IPAddr_t);
  //Doc:Desc Add an IP to an entry in the suspicious list
  //Doc:Arg1 The entry in the suspicious list
  //Doc:Arg2 the IP address to add

  //Doc:Method
  bool IsInfected(PortId_t);
  //Doc:Desc Is this port infected ?
  //Doc:Arg1 The port to check
  //Doc:Return true=infected or false=not infected

  //Doc:Method
  void CheckInfected(SPortMap_t::iterator);
  //Doc:Desc Check if an entry in the suspicious list is infected
  //Doc:Arg1 The entry in the suspicious list

  void SetOutIfs(IFVec_t vec)
    {
      outIfs = vec;
    }

  void SetOutIf(Interface* ifc)
    {
      outIfs.push_back(ifc);
    }

  void SetQTime(Time_t t)
    {
      qtime = t; 
    }

  //Doc:Method
  bool IsOutIface(Interface*);
  //Doc:Desc Check if an interface is the output interface of this network
  //Doc:Arg1 The interface to check
  //Doc:Return wether it is the output interface

  void SetStartTime(Time_t t)
    {
      starttime = t;
    }

private:
  PortMap_t  incoming;        // port map for incoming connections
  PortMap_t  outgoing;        // port map for outgoing connections
  SPortMap_t suspicious;      // port map for suspicious ports
  PortVec_t  infected;        // vector of infected ports
  IFVec_t    outIfs;          // vector of output interfaces
  Time_t     qtime;           // Quarantine time    
  Time_t     starttime;
};

#endif

