/*

* GTNetS provides a portable C++ class library for network simulations
*
* Copyright (C) 2003 George F. Riley
*
* No guarantees or warranties or anything are provided or implied in any way
* whatsoever.  Use this program at your own risk.  Permission to use this
* program for any purpose is given, as long as the copyright is kept intact.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

* Permission is hereby granted for any non-commercial use of this code

*/

// Georgia Tech Network Simulator - EIGRP Routing class
// George F. Riley, Talal M Jaafar.  Georgia Tech, Summer 2004


#ifndef __routingeigrp_h__
#define __routingeigrp_h__

#include "routing.h"

//Doc:ClassXRef
class RoutingEIGRP : public Routing {
//Doc:Class The class {\tt RoutingEIGRP} defines the eigrp routing class.


 public:
  //RoutingEIGRP() : fib(32, FIB_t()){ }
  //Doc:Method	
  RoutingEIGRP() { }
  //Doc:Desc The default constructor
  
  //Doc:Method  
  virtual ~RoutingEIGRP() { }
  //Doc:Desc The destructor
  
  //Doc:Method  
  virtual void Default(RoutingEntry);        // Specify default route
  //Doc:Desc This method adds the default route to a particular node.
  //Doc:Arg1 The default routing entry or the default gateway
  
  //Doc:Method  
  virtual RoutingEntry GetDefault();         // Get default route
  //Doc:Desc This method returns the default route for this node
  //Doc:return The default routing entry or the default gateway

  //Doc:Method  
  virtual void Add( IPAddr_t, Count_t,
                    Interface*, IPAddr_t);   // Add a routing entry
  //Doc:Desc This method is used to add a routing entry into the routing
  //Doc:Desc table.
  //Doc:Arg1 The destination IP Address
  //Doc:Arg2 The subnet Mask of the destination IP Address
  //Doc:Arg3 The interface to use
  //Doc:Arg4 The next hop IP Address
  

  //Doc:Method  
  virtual RoutingEntry Lookup(Node*, IPAddr_t); // Find a routing entry
  //Doc:Desc This method implements the lookup mechanism at a  node
  //Doc:Desc for a given destination IP address
  //Doc:Arg1 Node which is looking up the routing table
  //Doc:Arg2 Destination IP address
  //Doc:Return The corresponding routing entry or nil

  
  //Doc:Method  
  virtual RoutingEntry LookupFromPDU(PDU*);  // Find from PDU (NixVector)
  //Doc:Desc This method uses the PDU to look up the next route. Such a
  //Doc:Desc mechanism is used in routing protocols like nix vector routing
  //Doc:Arg1 The pointer to the PDU
  //Doc:Return The corresponding routing entry or nil
  
  //Doc:Method  
  virtual Routing* Clone();                  // Get a Clone of the object
  //Doc:Desc This method is used to make copies of the routing object.
  //Doc:Desc Such a mechanism is useful for example, in topology creation
  //Doc:Return A pointer to the cloned object

  //Doc:Method  
  virtual RType_t Type();                    // Find out routing type
  //Doc:Desc This method enables to query the routing object of the
  //Doc:Desc routing protocol it implements
  //Doc:Return the routing type
  

  //Doc:Method  
  virtual void    InitializeRoutes(Node*);   // Initialization
  //Doc:Desc This method initializes the routes and the routing table at a
  //Doc:Desc given node
  //Doc:Arg1 The pointer to the Node at which the routes are to be initialized
  

  //Doc:Method
  virtual bool    NeedInit();                // True if init needed
  //Doc:Desc This method determines if initialization is needed and returns
  //Doc:Desc a bool
  //Doc:Return true if initialization is needed
  

  //Doc:Method  
  virtual Size_t  Size() const;              // Total size (bytes) of FIB
  //Doc:Desc This method gives the size of the routing table. or the
  //Doc:Desc Forwarding Information Base
  //Doc:Desc Size of the FIB

  void    Clear();                   // Erase all elements in FIB
  void    displayFIB();
  
public:
  FIBMap_t     fib;                          // The routing database
  RoutingEntry defaultRoute;                 // Default route
};
#endif
