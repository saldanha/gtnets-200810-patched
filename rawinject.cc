// raw inject
$Id: rawinject.cc 4 2004-06-10 19:34:50Z dheeraj $
/*
  To Compile in top-level dir
   g++ -o rawinject rawinject.o -L. -lGTNetS-debug -L/usr/lib/qt3-gcc2.96/lib  -lqt -Xlinker -R -Xlinker .
  or to be more posixy
  g++ -o rawinject rawinject.o -L. -lGTNetS-debug -L/usr/lib/qt3-gcc2.96/lib -lqt -Wl,-rpath,/usr/lib/qt3-gcc2.96/lib -Wl,-rpath,.
*/
// Dheeraj

#include "simulator.h"
#include "node.h"
#include "interface-wireless.h"
#include "wlan.h"
#include "ratetimeparse.h"

#include "packet.h"
#include "ipv4.h"


#define XSIZE		3
#define YSIZE		2
#define NO_NODE		4

using namespace std;

class TriggerEvent : public Event {
public:
  typedef enum {TRIGG_EVENT} Event_t;
public:
  TriggerEvent(Node* na, Node* nb) {
    n1 = na;
    n2 = nb;
  }
public:
  Node* n1;
  Node* n2;
};

class Trigger : public Handler {
public:
  Trigger(Node* n1, Node* n2) {
    trigevent = new TriggerEvent(n1, n2);
  }

  void Handle(Event* ev, Time_t t) {
    TriggerEvent* e = (TriggerEvent*) ev;
    Packet * mypacket = new Packet();
    IPV4Header* l3hdr = new IPV4Header();
    l3hdr->src = e->n1->GetIPAddr();
    l3hdr->dst = e->n2->GetIPAddr();
    mypacket->PushPDU(l3hdr);
    l3hdr->totalLength = mypacket->Size();
    //mypacket->SetSpecial();
    Interface* iface = (e->n1)->GetIfByNode(e->n2);
    iface->Send(mypacket, l3hdr->dst, 0x0800);
    return;
  }
  void Schedule(Time_t time) {
    Simulator::instance->Schedule(trigevent, time, this);
  }
public:
  TriggerEvent* trigevent;
};


int main(int argc, char** argv)
{
  Simulator s;

  WirelessLink wlink(NO_NODE, IPAddr("192.168.0.1"), MASK_ALL);
  s.PrintStats();
  //Uniform urvx(0, XWIDTH), urvy(0, YWIDTH);
  Node* n[4];
  for(int i=0; i<NO_NODE; i++) {
    n[i] = wlink.GetNode(i);
    n[i]->SetRadioRange(100);
  }

  n[0]->SetLocation(0, 0);
  n[1]->SetLocation(70, 0);
  //n[2]->SetLocation(140, 0);
  n[2]->SetLocation(0, 70);
  n[3]->SetLocation(70, 70);
  //n[5]->SetLocation(140, 70);
  InterfaceWireless* iface1 = (InterfaceWireless*) n[0]->GetIfByLink(&wlink) ;
  InterfaceWireless* iface2 = (InterfaceWireless*) n[1]->GetIfByLink(&wlink) ;
  InterfaceWireless* iface3 = (InterfaceWireless*) n[2]->GetIfByLink(&wlink) ;
  InterfaceWireless* iface4 = (InterfaceWireless*) n[3]->GetIfByLink(&wlink) ;

  //s.PrintStats();
  Trigger* mytrig1 = new Trigger(n[0], n[1]);
  Trigger* mytrig2 = new Trigger(n[1], n[0]);
  Trigger* mytrig3 = new Trigger(n[2], n[1]);
  Trigger* mytrig4 = new Trigger(n[1], n[0]);
  Trigger* mytrig5 = new Trigger(n[0], n[1]);
  
  mytrig1->Schedule(1.0);
  mytrig2->Schedule(1.0);
  mytrig3->Schedule(1.0);



  s.PrintStats();
//  s.StopAt(10);
  s.Run();
  
  s.PrintStats();
  cout << "Simulation complete" << endl;
}
