// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: 

// Georgia Tech Network Simulator - Bloom Filter Implementation
// George F. Riley.  Georgia Tech, Spring 2005

// Bloom filters have many uses in networking.

#include <iostream>
#include <set>
#include <cstring>

#include "bloom-filter.h"

using namespace std;

static unsigned char bitInd[8] =
{ 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };

//set<unsigned long> dbHashSet; // debug only

BloomFilter::BloomFilter(Count_t isz, Count_t osz, Count_t nh)
    : iSize(isz), oSize(osz), nHash(nh), o(nil)
{ // Nothing else needed
}

BloomFilter::~BloomFilter()
{
  delete [] o;
#ifdef OLD_MULTIPLE_FILTERS
  if (!o.empty())
    {
      for (vector<unsigned char*>::size_type i = 0; i < o.size(); ++i)
        {
          delete o[i];
        }
    }
#endif
}

void BloomFilter::AddHash(HashGenerator* hf, Count_t l)
{
  if (!o) Initialize();
  if (l >= f.size())
    {
      cout << "HuH?  BloomFilter hash level (" << l << ") out of range" <<endl;
      return;
    }
  f[l] = hf;
}

bool BloomFilter::InFilter(const unsigned char* v)
{ // Check if specified value in filter
  if (!o) Initialize();
  for (Count_t i = 0; i < nHash; ++i)
    {
      if (!f[i])
        {
          cout << "Missing hash function " << i << " in bloom filter" << endl;
          return true;
        }
      unsigned long h = f[i]->Hash(v, iSize); // Compute Hashed value
      h %= oSize; // Lower oSize bits only
      Count_t byte = h / 8;
      Count_t bit  = h % 8;
      if ((o[byte] & bitInd[bit]) == 0) return false; // Not in filter
    }
  // Bit set in each stage, must be here
  return true;
}

bool BloomFilter::AddFilter(const unsigned char* v)
{ // Add specified value to filter
  if (!o) Initialize();
  bool alreadyThere = true;
  for (Count_t i = 0; i < nHash; ++i)
    {
      if (!f[i])
        {
          cout << "Missing hash function " << i << " in bloom filter" << endl;
          return true;
        }
      
      unsigned long h = f[i]->Hash(v, iSize); // Compute Hashed value
      //cout << "Hashing " << hex;
      //Count_t lb = (iSize - 1) / 8 + 1;
      //for (Count_t m = 0; m < lb; m++) cout << (unsigned int)v[m];
      //cout << endl;
      h %= oSize; // Lower oSize bits only
      lastHash[i] = h;
#ifdef TEST_DUPLICATE_HASH
      // debug only
      set<unsigned long>::iterator k = dbHashSet[i].find(h);
      if (k != dbHashSet[i].end())
        {
          hashCollisions[i]++;
          cout << "Duplicate hash value, stage " << i << " h " << hex << h
               << endl;
        }
      else
        {
          //cout << "Unique hash value, stage " << i << " h " << hex << h
          //     << endl;
          dbHashSet[i].insert(h);
        }
      // end debug
#endif
      Count_t byte = h / 8;
      Count_t bit  = h % 8;
      //cout << "Stage " << i << " byte " << byte << " bit " << bit << endl;
      if ((o[byte] & bitInd[bit]) == 0) alreadyThere = false;
      else collisions[i]++;
      // Set the bit
      o[byte] |= bitInd[bit];
    }
  return alreadyThere;
}

void BloomFilter::Clear()
{ //Clear all filter entries.
  if (!o) Initialize();
  memset(o, 0, s);
}

void BloomFilter::DBStats()
{
  // Count the number of 1 bits in the filter
#ifdef SKIP_THIS
  Count_t nOnes = 0;
  for (Count_t j = 0; j < s; ++j)
    {
      unsigned char c = o[i][j];
      if (c & 0x01) nOnes++;
      if (c & 0x02) nOnes++;
      if (c & 0x04) nOnes++;
      if (c & 0x08) nOnes++;
      if (c & 0x10) nOnes++;
      if (c & 0x20) nOnes++;
      if (c & 0x40) nOnes++;
      if (c & 0x80) nOnes++;
    }
#endif
  for (Count_t i = 0; i < nHash; ++i)
    {
      cout << "Stage " << dec << i << " totBits " << s * 8
      //<< " ones " << nOnes << " zeros " << s * 8 - nOnes 
       << " hashCollisions " << hashCollisions[i]
           << " collisions " << collisions[i] << endl;
    }
}

void BloomFilter::DBStatsShort()
{
  for (Count_t i = 0; i < nHash; ++i)
    {
      cout << "Stage " << dec << i
           << " hashCollisions " << hashCollisions[i]
           << " collisions " << collisions[i] 
           << " lastHash " << hex << lastHash[i]
           << endl;
    }
}

          
  
// Private methods
void BloomFilter::Initialize()
{ // Create the output bitmaps and the hash function vector
  s = (oSize - 1) / 8 + 1;
  o = new unsigned char[s];
  for (Count_t i = 0; i < nHash; ++i)
    {
      f.push_back(nil);
      collisions.push_back(0);
      dbHashSet.push_back(set<unsigned long>());
      hashCollisions.push_back(0);
      lastHash.push_back(0);
    }
  Clear();
}


