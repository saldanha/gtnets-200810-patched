#ifndef __l3prototaclane_h__
#define __l3prototaclane_h__

#include <list>

#include "common-defs.h"
#include "l3protocol.h"
#include "ipv4.h"
#include "event.h"
#include "ipaddr.h"
#include "node.h"
#include "handler.h"

class IPV4Header;
class IPV4ReqInfo;
class IPV4Options;
class TACLANENode;
class Interface;
class Packet;

// Define a PDU identical to IPV4 pdu, except it is slightly larger
// to account for the ecryption overhead
class TaclaneHeader : public IPV4Header 
{
 public:
  TaclaneHeader(Size_t s0) : IPV4Header(), s(s0) {}
 public:
  //Doc:Method
  Size_t  Size() const { return IPV4Header::Size() + s;}
  //Doc:Desc This method returns the size of the PDU
    //Doc:Desc   For taclane, it's the size of the IPV4 header plus the padding
  //Doc:Return the size of the PDU
  
  //Doc:Method
  PDU*    Copy() const { return new TaclaneHeader(*this);}
  //Doc:Desc This method creates a copy of this PDU
  //Doc:Return A pointer to the copy of this PDU

  Layer_t SubLayer() { return 1;} // Taclane sublayer
public:
  // A method that constructs a Taclane header from an existing IPV4 header
  // and a specified extra size value
  static TaclaneHeader* CopyFromIP(IPV4Header*, Size_t);

public:
  Size_t s; // Padding size
};

// Define an event to schedule packet tx after the encryption time
class TaclaneEvent : public Event
{
 public:
  TaclaneEvent(const RoutingEntry& r0, Packet* p0)
    : Event(0), r(r0), p(p0)    {}
public:
  RoutingEntry r;
  Packet*      p;
};

  
//Doc:ClassXRef
class L3ProtoTACLANE : public L3Protocol, public Handler {
//Doc:Class The class {\tt L3ProtoTACLAND} implements the layer 3
//Doc:Class functinality for taclane devides.
//Doc:Class It derives 	from the class {\tt L3Protocol}.
	
public:
  typedef enum { DefaultTTL = 64 } DefTTL_t;
  typedef enum { VERSION, HEADERLENGTH, SERVICETYPE,
                 TOTALLENGTH, IDENTIFICATION,
                 FLAGS, FRAGMENTOFFSET,
                 TTL, PROTOCOL, HEADERCHECKSUM,
                 SRC, DST, UID, OPTIONS } IPV4Trace_t; // Tracing options
  //Doc:Method
  L3ProtoTACLANE(TACLANENode*, Interface*, Interface*);
  //Doc:Desc Default constructor

  //Doc:Method
  void DataRequest(Node*, Packet*, void*);
  //Doc:Desc This method is used by layer 4 protocols like udp/tcp to
  //Doc:Desc hand a packet to IP.
  //Doc:Desc For taclane devices , this is ingored.
  //Doc:Arg1 Pointer to the node requesting the data transfer
  //Doc:Arg2 Pointer to the packet to be transmitted
  //Doc:Arg3 Any auxiliary information that layer 4 might want to convey
  
  //Doc:Method
  void DataIndication(Interface*, Packet*);    // From lower layer
  //Doc:Desc This method is used to pass on a packet from any layer 2 protocol
  //Doc:Desc up the stack.
  //Doc:Arg1 A pointer to the interface at which this packet is received
  //Doc:Arg2 A pointer to the packet that was received
  
  Interface* PeekInterface(Node*, void*);  // Find forwarding if

  Proto_t Proto()   { return 0x800;}   // Get the protocol number

  //Doc:Method
  virtual Count_t Version() { return 4;}       // Get IP version number
  //Doc:Desc This method returns the version of Internet Protocol
  //DOc:Return returns the version

  // Handler methods
  void Handle(Event*, Time_t);

  // Private methods
  Time_t Delay(Packet*); // Compute encryption/decryption delay
  Size_t Padding(Packet*);      // Compute extra packet size due to encryption

private:
  Interface*   ptif;  // Plaintext interface
  Interface*   ctif;  // Ciphertext interface
  TACLANENode* n;     // Attached  node
};

#endif

