// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: interface-link16.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - Interface Link16 class
// George F. Riley.  Georgia Tech, Summer 2005


// Define class to model network link16 interfaces

#include <iostream>

#include "link16.h"
#include "interface-link16.h"
#include "queue.h"

using namespace std;

InterfaceLink16::InterfaceLink16(IPAddr_t ip)
    // THis is wrong..use l2protolink16
    : InterfaceWireless(L2Proto802_3(), ip), pendingPkt(nil)
{
  SetOpMode(ONEHOP); // This hacks the IPV4 Data request to send the pkt
}

InterfaceLink16::~InterfaceLink16()
{ // Nothing to do?
}

void InterfaceLink16::Handle(Event* ev, Time_t t)
{
  Link16Event* e = (Link16Event*)ev;
  Link16* l16 = (Link16*)GetLink();
  switch(e->event) {
    case Link16Event::START_NEXT_FRAME:
      if (!bitsRemaining)
        { // No more, see if any pkts enqueued
          pendingPkt = GetQueue()->Deque();
          if (!pendingPkt) return; // No packet
          bitsRemaining = pendingPkt->Size() * 8;
        }
      if (bitsRemaining > l16->slotSize)
        bitsRemaining -= l16->slotSize;
      else
        bitsRemaining = 0;
      cout << "this " << this << " bitsRemaining " << bitsRemaining
           << " slotSize " << l16->slotSize << endl;
      l16->WirelessTxStart(GetNode(), this, e->dst, e->range);
      // Schedule the end event
      e->event = Link16Event::END_FRAME;
      Scheduler::Schedule(e, l16->slotSize / l16->Bandwidth(), this);
      break;
    case Link16Event::END_FRAME:
      l16->WirelessTxEnd(GetNode(), this, e->dst, e->range);
      ScheduleNextFrame();
      break;
    default:
      InterfaceWireless::Handle(ev, t);
  }
}

  
bool InterfaceLink16::Send(Packet* p, IPAddr_t ip, int proto)
{
  if (mySlots.empty())
    {
      cout << "Oops.  Link16 transmit with no slots.  Ignoring" << endl;
      return false;
    }
  
  cout << "Sending l16 pkt to " << (string)IPAddr(ip) << endl;
  // code later
  if (pendingPkt)
    { // Already sending something, enque and send later
      cout << "this " << this << " bitsRemanining " << bitsRemaining
           << endl;
      return EnquePacket(p);
    }
  pendingPkt = p;
  bitsRemaining = p->Size() * 8;
  ScheduleNextFrame();
  return true;
}

void InterfaceLink16::AddSlot(Count_t slotNum)
{
  Link16* l16 = (Link16*)GetLink();
  if (slotNum >= l16->nSlots)
    {
      cout << "Ignoring link16 slot number " << slotNum
           << " out of range 0 .. " << l16->nSlots-1 << endl;
      return;
    }
  mySlots.insert(slotNum);
  nextSlot = mySlots.begin();
}

// Private routines
Time_t InterfaceLink16::NextSlotTime()
{ // Return time of our next available slot time
  // At this point we know we have at least one slot or would not get here
  Link16* l16 = (Link16*)GetLink();
  Count_t nextSlotNumber = *nextSlot++;
  Time_t now = Simulator::Now();
  if (nextSlot == mySlots.end()) nextSlot = mySlots.begin();
  Time_t tSlotsElapsed = Simulator::Now() / l16->slotTime;
  Count_t slotsElapsed = (Count_t)tSlotsElapsed; // Integer
  Time_t  elapsedThisSlot = now - slotsElapsed * l16->slotTime;
  Count_t slotsFuture = 1;
  while(((slotsElapsed + slotsFuture) % l16->nSlots) != nextSlotNumber)
    slotsFuture++;
  return now - elapsedThisSlot + slotsFuture * l16->slotTime;
}
  
  
void InterfaceLink16::ScheduleNextFrame()
{ // Schedule the next frame transmission in the next slot time
  Link16* l16 = (Link16*)GetLink();
  Time_t nextSlot = NextSlotTime();
  if (!txEvent) txEvent = new Link16Event(Link16Event::START_NEXT_FRAME);
  MACAddr m = l16->IPToMac(ip);
  Interface* dif = l16->GetIfByMac(m);
  if (!dif)
    {
      cout << "HuH?  No dest if in Link16::Send" << endl;
      return;
    }
  txEvent->event = Link16Event::START_NEXT_FRAME;
  txEvent->dst = dif->GetNode();
  txEvent->range = txEvent->dst->GetRadioRange();
  
  Time_t now = Simulator::Now();
  Scheduler::Schedule(txEvent, nextSlot - now, this);
}
