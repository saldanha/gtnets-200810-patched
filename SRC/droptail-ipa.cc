// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: droptail-ipa.cc 92 2004-09-14 18:18:43Z dheeraj $



// DropTail Queue class with IPA derivatives calculation
// George F. Riley, Georgia Tech, Fall 2002

#include "droptail-ipa.h"
#include "simulator.h"

using namespace std;

class IPAEvent : public TimerEvent {
public:
  IPAEvent(DropTailIPA* q1, DropTailIPA* q2) 
    : TimerEvent(), queue1(q1), queue2(q2) { };
public:
  DropTailIPA* queue1;
  DropTailIPA* queue2;
};
  
void DropTailIPA::Timeout(TimerEvent* ev)
{
#ifdef CODE_LATER
  Time_t now = Simulator::Now();
  IPAEvent* e = (IPAEvent*)ev;
  NCount_t cl1 = e->queue1->DL1dB1();     // Cumulative dL1dB1
  DCount_t cq1 = e->queue1->DQ1dB1();     // Cumulative dQ1dB1
  NCount_t l1 = cl1 - dL1dB1;             // Loss derivative this period
  DCount_t q1 = cq1 - dQ1dB1;             // Workload derivative this per
  dL1dB1 = cl1;                           // Update cumulative to date
  dQ1dB1 = cq1;      

  // Debug..count the statistics
  if (cl1 && cq1)
    { // Both non-zero
      if (fabs(wL1 * l1) > (wQ1 * q1)) lGreater++;
      if ((wQ1 * q1) > fabs(wL1 * l1)) qGreater++;
      if (l1 == 0 && q1) qNotL++;
    }
  // Stage 2 derivatives
  NCount_t cl2 = e->queue2->DL1dB1();
  DCount_t cq2 = e->queue2->DQ1dB1();     // Cumulative l2/q2 derivatives
  NCount_t l2 = cl2 - dL2dB2;
  DCount_t q2 = cq2 - dQ2dB2;             // This period
  dL2dB2 = cl2;
  dQ2dB2 = cq2;                           // Update cumulative to date

  // Stage 2 derivatives, wrt b1
  NCount_t cl21 = e->queue2->DL2dB1();    // Cumulative l2/b1 derivative
  DCount_t cq21 = e->queue2->DQ2dB1(); 
  NCount_t l21 = cl21 - dL2dB1;           // This period
  DCount_t q21 = cq21 - dQ2dB1;
  dL2dB1 = cl21;                          // Update cumulative
  dQ2dB1 = cq21;
  
  int     adj1;
  int     adj2;
  double  wsum1 = l1 * wL1 + q1 * wQ1 + l21 * wL1 + q21 * wQ1;
  double  wsum2 = l2 * wL2 + q2 * wQ2;
  if (useGradient)
    {
      cout << "l1 " << l1 << " q1 " << q1 << endl;
      //cout << "wL1 " << wL1 << " wQ1 " << wQ1 << endl; 
      if (!single)
        {
          cout << "l21 " << l21 << " q21 " << q21 << endl;
          cout << " l2 " << l2 << " q2 " << q2 << endl;
        }
    }
  //cout << "Adj, wsum1 " << wsum1 << endl;
  //cout << "Adj, l21 " << l21 << " q21 " << q21 << endl;
  // We need to check for a continual lossy-busy-period
  //if (l1 == 0 && q1 == 0 && l21 == 0 && q21 == 0)
  if (l1 == 0 && q1 == 0 && l21 == 0 && q21 == 0)
    { // Either continual LBP or no losses at all
      if (e->queue1->LBP())
        {
          adj1 = -1;
          // Also set wsum1 to +- 1 for gradient adjustments below
          wsum1 = -adjustInterval/mult;
        }
      else
        {
          adj1 = +1;
          wsum1 = adjustInterval/mult;
        }
    }
  else if (wsum1 < 0)
    adj1 = -1;
  else
    adj1 = +1;
  
  // Compute stage 2 adjustments
  if (l2 == 0 && q2 == 0)
    //if (l2 == 0)
    { // Either continual LBP, or no losses at all
      if (e->queue2->LBP())
        {
          adj2 = -1;
          // Also set wsum2 to +- 1 for gradient adjustments below
          wsum2 = -adjustInterval / mult;
        }
      else
        {
          adj2 = +1;
          // Also set wsum2 to max for gradient adjustments below
          wsum2 = adjustInterval / mult;
        }
    }
  else if (wsum2 < 0)
    adj2 = -1;
  else
    adj2 = +1;
  if (useGradient)
    {
      double fadj1 = (wsum1 * mult) / adjustInterval;
      double fadj2 = (wsum2 * mult) / adjustInterval;
      if (fabs(fadj1) > maxAdj) fadj1 = maxAdj * (fadj1)/fabs(fadj1);
      if (fabs(fadj2) > maxAdj) fadj2 = maxAdj * (fadj2)/fabs(fadj2);
      fQlim1 -= fadj1;
      fQlim2 -= fadj2;  // Perform the adjustments
      qLimit1 = (int)fQlim1;
      qLimit2 = (int)fQlim2; // New (integer) queue limits
    }
  else
    { // Use the +- 1 approach
      if(1)cout << "Adj1, l1 " << l1 << " q1 " << q1 
                << " wsum1 " << wsum1 << " adj1 " << adj1 
                << " LBP " << e->queue1->LBP() << endl;
      qLimit1 -= adj1;
      qLimit2 -= adj2;
    }
  qLimit1 = max(minQ, qLimit1);
  qLimit1 = min(maxQ, qLimit1); // Bounded
  e->queue1->SetLimit(qLimit1*(pktSize+hdr)); // Set new limit
  qLimit2 = max(minQ, qLimit2);
  qLimit2 = min(maxQ, qLimit2); // Bounded
  e->queue2->SetLimit(qLimit2*(pktSize+hdr)); // Set new limit
  double q1wl = e->queue1->Losses() * wL1;
  double q1ww = e->queue1->TotalWorkload()/(pktSize+hdr) * wQ1;
  cout << "Adjusted q1 to " << qLimit1;
  if (!single)
    cout << " q2 to " << qLimit2;
  cout << " at time " << now << endl;
  cout << "Weighted loss " << q1wl - priorq1wl;
  cout << ", Weighted delay " << q1ww - priorq1ww;
  cout << ", Sum " <<  q1wl - priorq1wl + q1ww - priorq1ww << endl;
  priorq1wl = q1wl;
  priorq1ww = q1ww;
  Schedule(ev, adjustInterval);
#endif
}

// Constructors

DropTailIPA::DropTailIPA()
  : DropTail(), busyPeriod(false), lossyBusyPeriod(false),
    lbpStart(0), lossyBusyPeriods(0), sumLossy(0), full(false),
    stage1(nil), stage2(nil), totalLosses(0),
    tmp(0), accumulator(0), aspCount(0), dQ2(0),
    ppStart(0), ppMult(0),
    qLim(Queue::DefaultLength()), wL(1), wQ(1),
    lastAdjust(0), adjustPeriod(0), adjustStart(0),
    gain(1), cumdL1(0), cumdQ1(0), cumdL2(0), cumdQ2(0),
    adjustTimer(nil), adjustEvent(nil)
{
}

DropTailIPA::DropTailIPA(Count_t s) // Queue Length
  : DropTail(s), busyPeriod(false), lossyBusyPeriod(false),
    lbpStart(0), lossyBusyPeriods(0), sumLossy(0), full(false),
    stage1(nil), stage2(nil), totalLosses(0),
    tmp(0), accumulator(0), aspCount(0), dQ2(0),
    ppStart(0), ppMult(0),
    qLim(s), wL(1), wQ(1),
    lastAdjust(0), adjustPeriod(0), adjustStart(0),
    gain(1), cumdL1(0), cumdQ1(0), cumdL2(0), cumdQ2(0),
    adjustTimer(nil), adjustEvent(nil)
{
}


DropTailIPA::DropTailIPA(const DropTailIPA& c) // Copy constructor
  : DropTail(c), busyPeriod(false), lossyBusyPeriod(false),
    lbpStart(0), lossyBusyPeriods(0), sumLossy(0), full(false),
    stage1(nil), stage2(nil), totalLosses(0),
    tmp(0), accumulator(0), aspCount(0), dQ2(0),
    ppStart(0), ppMult(0)
{
  DEBUG0((cout << "DT Copy Const, limit " << Limit() << endl));
}


DropTailIPA::~DropTailIPA()
{
}

// Public methods
bool DropTailIPA::Enque(Packet* p)
{
  if (!Length())
    {
      bpStart = Simulator::Now(); // Note start of busy period
      busyPeriod = true;          // Set a busy period
    }
  bool r= DropTail::Enque(p);
  if (r)
    {
      full = false;
      return true;     // Nothing to do if not full
    }
  full = true;
  DEBUG0((cout << "Lost packet at time " << Simulator::Now() << endl));
  totalLosses++;      // Count losses
  if (!lossyBusyPeriod)
    { // Note a new lbp
      lossyBusyPeriod = true;
      lossyBusyPeriods++; // Count lbp's
      lbpStart = Simulator::Now();
      DEBUG0((cout << "Found a new LBP starting at time " 
              << lbpStart << endl));
    }
  if (stage1)
    { // This is stage2 of two-stage (stage1 is pointer to peer stage1)
      accumulator += tmp;
      tmp = 0;
      dQ2 += (Simulator::Now() - ppStart) * ppMult;
      ppMult = 0;         // Reset partial period multiplier
    }
  return false;           // Full queue, did not enque
}

Packet* DropTailIPA::Deque()
{
  Packet* r = DropTail::Deque();
  if (!r) return r; // Nothing deque'd, nothing to do
  if (!Length())
    { // End of busy period
      Time_t now = Simulator::Now();
      if (stage2 && lossyBusyPeriod)
        { // This is stage 1 of two stage tandem, stage2 is pointer to 2nd
          // And stage 1 is in LBP
          if (stage2->full)
            { // Stage 2 is full, increment the accumulator
              stage2->accumulator++;
            }
          else
            { // Stage 2 is not full, advance the tmp counter
              stage2->tmp++;
            }
          // Now manage dQ2/dB1
          if (stage2->Length() && !stage2->full)
            { // Stage 2 is in partial period
              stage2->dQ2 += (now - stage2->ppStart) * stage2->ppMult;
              stage2->ppMult++;      // Adjust multiplier
              stage2->ppStart = now; // Start of new partial perid
            }
        }
      if (stage1)
        { // This is stage 2 of tandem
          tmp = 0;
          dQ2 += (now - ppStart) * ppMult;
          ppMult = 0;
        }
      if (lossyBusyPeriod)
        {
          sumLossy += (Simulator::Now() - lbpStart);
          aspCount++;
          lossyBusyPeriod = false;
        }
      busyPeriod = false;
    }
  full = false;
  return r;
}

void DropTailIPA::SetLimit(Count_t l)
{
  DropTail::SetLimit(l);
  qLim = l;             // And set the floating point version
}

Queue* DropTailIPA::Copy() const
{
  return new DropTailIPA(*this);
}

void   DropTailIPA::ResetStats()
{ // Need to figure out what to do here...
  DropTail::ResetStats();
}

// Methods specific to DropTailIPA Queues
NCount_t DropTailIPA::DL1dB1()
{ // This is just the negative of the count of lossybusyperiods
  return -lossyBusyPeriods;
}

Time_t DropTailIPA::DQ1dB1()
{ // Includes partial time if still in the LBP
  return sumLossy + PartialDQ1();
}

Time_t DropTailIPA::PartialDQ1()
{ // Returns the elapsed time since start of LBP
  if (lossyBusyPeriod)
    {
      return Simulator::Now() - lbpStart;
    }
  return 0; // No parial if not in lbp
}

NCount_t DropTailIPA::DL2dB1()
{
  return accumulator;
}

Time_t DropTailIPA::DQ2dB1()
{ 
  return dQ2;
}

void DropTailIPA::Stage1(DropTailIPA* q)
{
  stage1 = q;
}

void DropTailIPA::Stage2(DropTailIPA* q)
{
  stage2 = q;
}

Count_t DropTailIPA::Losses()
{
  return totalLosses;
}

DCount_t DropTailIPA::Workload()
{
  return Queue::TotalWorkload();
}

bool DropTailIPA::LBP()
{
  return lossyBusyPeriod;
}

void DropTailIPA::LWeight(Mult_t w)
{
  wL = w;
}

void DropTailIPA::QWeight(Mult_t w)
{
  wQ = w;
}

Mult_t DropTailIPA::LWeight()
{
  return wL;
}

Mult_t DropTailIPA::QWeight()
{
  return wQ;
}

void     DropTailIPA::AdjustPeriod(Time_t t)
{ // Set the adjustment period
  adjustPeriod = t;
}

void     DropTailIPA::AdjustStart(Time_t t)
{ // Set the starting time for adjustments
  adjustStart = t;
}

void     DropTailIPA::Gain(Mult_t g)
{ // Set the gain factor for adjustments
  gain = g;
}

