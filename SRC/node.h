// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: node.h 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - Node class
// George F. Riley.  Georgia Tech, Spring 2002

#ifndef __node_h__
#define __node_h__

#include <vector>
#include <deque>
#include <list>

#include "common-defs.h"
#include "l2proto802.3.h"
#include "mask.h"
#include "location.h"
#include "packet-callbacks.h"

class Link;
class Node;
class Interface;
class InterfaceBasic;
class InterfaceWireless;
class WirelessLink;
class Routing;
class Protocol;
class ProtocolGraph;
class PortDemux;
class Linkp2p;
class Queue;
class Application;
class NodeImpl;
class RoutingAODV;
class AODVRoutingEntry;
class WormContainment;
class Image;

class  QTWindow;
class  NodeAnimation;
// Definitions from Qt library.  Unused if no Qt configured.
class  QCanvasPolygonalItem;
class  QCanvasItem;
class  QPoint;
class  QCanvas;
class  QColor;

// Define a callback for custom shapes in animation
typedef QCanvasPolygonalItem*(*CustomShape_t)(QCanvas*, Node*,
                                              const QPoint&,
                                              QCanvasPolygonalItem*);

// For radio interface list
class RadioItem
{
public:
  RadioItem() : inf(nil), dist(0) {}
  RadioItem(InterfaceWireless* i, Meters_t d) : inf(i), dist(d) {}

public:
  InterfaceWireless* inf;
  Meters_t           dist;
};

//typedef std::vector<RadioItem> RadioVec_t;
typedef std::list<RadioItem> RadioVec_t;

//Doc:ClassXRef
class NodeIfWeight { // Used by routing protocols
public:
  NodeIfWeight(Node* n, InterfaceBasic* i, Weight_t w)
    : node(n), iface(i), weight(w) { }
public:
  Node*           node;
  InterfaceBasic* iface;
  Weight_t        weight;
};

//Doc:ClassXRef
class NodeIf { // Node and Interface used to get there, used by routing
public:
  NodeIf(Node* n, Interface* i)
    : node(n), iface(i) { }
public:
  Node*      node;
  Interface* iface;
};

//Doc:ClassXRef
class IpMask { // IP Address and mask
public:
  IpMask(IPAddr_t i, Mask_t m)
    : ip(i), mask(m) { }
public:
  IPAddr_t ip;
  Mask_t   mask;
};

typedef std::vector<Link*>        LinkVec_t;
typedef std::vector<Node*>        NodeVec_t;
typedef std::deque<NodeIf>        NodeDeque_t;
typedef std::vector<IPAddr_t>     IPAddrVec_t;
typedef std::vector<IpMask>       IPMaskVec_t;
typedef std::vector<Interface*>   IFVec_t;
typedef std::vector<NodeIfWeight> NodeWeightVec_t; // Neighbors,Iface w/weights
typedef std::vector<NodeIf>       NodeIfVec_t;     // Nodes and Iface to reach
typedef std::vector<Dist_t>       DistVec_t;

const Dist_t   INF  = 0xffffffff;

#include "routing.h"
#include "trace.h"
#include "macaddr.h"

class RoutingNixVector;
class Mobility;
class Packet;
//Doc:ClassXRef
class Node {
  //class Node {
  //Doc:Class Objects of class {\tt Node} are used in \GTNS\ to represent all
  //Doc:Class nodes in the simulated topology.  Nodes represent end-- systems,
  //Doc:Class wireless devides, hubs, and routers.  When a node is created, it
  //Doc:Class automatically has an associated routing object (the default
  //Doc:Class is \NV\ routing) and a layer 3 protocol object (the default
  //Doc:Class is {\em IPV4}).  After nodes are created, interfaces and links
  //Doc:Class can be added to describe the connectivity of nodes, using the
  //Doc:Class public members described below.

public:
  // Define the node shapes for animation
  typedef enum { NONE, CIRCLE, SQUARE, HEXAGON, OCTAGON,
                 CUSTOM, // Displayed by the custom callback
                 IMAGE   // Displayed with a Qt image file
  } Shape_t;

public:
  //Doc:Method
  Node(SystemId_t = 0);
    //Doc:Desc Creates a node in the simulated topology. The optional parameter
    //Doc:Desc is useful only for distributed simulations, and specifies
    //Doc:Desc which  simulator is responsible for maintaining the state
    //Doc:Desc for this node.  If the id specified matches the system
    //Doc:Desc id specified on the {\tt Simulator} constructor, then
    //Doc:Desc a full--state node is created.  If not, a reduced state
    //Doc:Desc node, with only connectivity information, is created.
    //Doc:Arg1 The system responsible for this node (distributed simulations
    //Doc:Arg1 only.

protected:
  // Used by node subclasses, such as SatelliteNode, to specify
  // type of node implementation needed
  Node(NodeImpl*, SystemId_t = 0);

public:
  virtual ~Node();
  //Doc:Method
  NodeId_t Id() { return id;}
    //Doc:Desc Each node in a \GTNS\ simulation has an associated unique
    //Doc:Desc integer identifier, starting from zero for the first
    //Doc:Desc node created.
    //Doc:Return The node identifier for this node.

  // IP Address Management
  //Doc:Method
  IPAddr_t GetIPAddr();                  // Get the IPAddres of this node
    //Doc:Desc Returns the \IPA\ assigned to this node.  Nodes can have
    //Doc:Desc multiple \IPA{s}, since \IPA{s} are assigned to interfaces and
    //Doc:Desc nodes can have multiple interfaces.  This returns the first
    //Doc:Desc \IPA\ assigned to this node.
    //Doc:Return The first \IPA\ assigned to this node.

  //Doc:Method
  virtual void     SetIPAddr(IPAddr_t);  // Set the IPAddres of this node
    //Doc:Desc Specifies an \IPA\ for this node.
    //Doc:Arg1 The \IPA\ to be assigned to this node.

  // Return vector of all known IPAddr's and associated masks for this node
  //Doc:Method
  void     IPAddrs(IPMaskVec_t&);
    //Doc:Desc Each interface assigned to a node has an associated \IPA\ and
    //Doc:Desc address mask.  This method returns a vector of the \IPA\ and
    //Doc:Desc masks for each interface on this node.
    //Doc:Arg1 An {\em out} parameter that is populated with the
    //Doc:Arg1 \IPA\ and mask information.

  //Doc:Method
  bool     LocalIP(IPAddr_t);            // True if specified ip is local
    //Doc:Desc Determines if the specified \IPA\ is local to this node.
    //Doc:Arg1 The \IPA\ to check for locality.

  //Doc:Method
  bool     IPKnown();                    // True if non-default ip is assigned
    //Doc:Desc Determines if this node has an \IPA\ assigned.

  //Doc:Method
  bool     IsReal();                     // True if node is not a Ghost
    //Doc:Desc   Find out if this node is "real", versus a Ghost
    //Doc:Return True if Real node, false if Ghost.

 //Doc:Method
  bool     IsSwitchNode();                     // True if node is a switch
  //Doc:Desc   Find out if this node is switch.

 //Doc:Method
  void IsSwitchNode(bool);
  //Doc:Desc   Set the node to a switch or not

 //Doc:Method
  void PacketRX(Packet*, Interface*);
  //Doc:Desc   called when a packet is receive on a switch node


  // Interfaces
  //Doc:Method
  Interface* AddInterface(Interface*);
    //Doc:Desc   Add a new interface, using a pre-allocated interface pointer.
    //Doc:Arg1 Interface to add.
    //Doc:Return Added interface.

  //Doc:Method
  Interface* AddInterface(const L2Proto& = L2Proto802_3(),
			  bool bootstrap=false);
    //Doc:Desc Assign a new interface to this node, with no associated
    //Doc:Desc \IPA\ or mask.
    //Doc:Return A pointer to the newly assigned interface.
    //Doc:Arg1 A layer 2 protocol object to be assigned to this interface.
    //Doc:Arg1 The object passed in is {\em copied}, so the same object
    //Doc:Arg1 may be passed to multiple interfaces without problems.

  //Doc:Method
  Interface* AddInterface(const L2Proto&,
                          IPAddr_t, Mask_t, MACAddr m = MACAddr::Allocate(),
			  bool bootstrap=false);
  //Doc:Desc Assign a new interface to this node, with the  specified
  //Doc:Desc \IPA\ and mask.
  //Doc:Return A pointer to the newly assigned interface.
  //Doc:Arg1 A layer 2 protocol object to be assigned to this interface.
  //Doc:Arg1 The object passed in is {\em copied}, so the same object
  //Doc:Arg1 may be passed to multiple interfaces without problems.
  //Doc:Arg2 The \IPA\ assigned to this interface.
  //Doc:Arg3 The address mask assigned to this interface.
  //Doc:Arg4 The {\em MAC} address assigned (normally the default
  //Doc:Arg4 should be used)

  //Doc:Method
  Interface* AddInterface(const L2Proto&,const Interface&,
			  IPAddr_t, Mask_t,
			  MACAddr = MACAddr::Allocate(),
			  bool bootstrap = false);
  //Doc:Desc Assign a new interface to this node, with the  specified
  //Doc:Desc \IPA\ and mask.
  //Doc:Return A pointer to the newly assigned interface.
  //Doc:Arg1 A layer 2 protocol object to be assigned to this interface.
  //Doc:Arg1 The object passed in is {\em copied}, so the same object
  //Doc:Arg1 may be passed to multiple interfaces without problems.
  //Doc:Arg2 Reference to what type of Interface to add
  //Doc:Arg3 The \IPA\ assigned to this interface.
  //Doc:Arg4 The address mask assigned to this interface.
  //Doc:Arg5 The {\em MAC} address assigned (normally the default
  //Doc:Arg5 should be used)



  //Interface* LocalSubnet(IPAddr_t);    // Determine if local subnet
  //Doc:Method
  virtual Count_t    InterfaceCount();   // Get count of interfaces
    //Doc:Desc Returns the number of interfaces assigned to this node.
    //Doc:Return The number of interfaces assigned to this node.

  //Doc:Method
  virtual const IFVec_t& Interfaces();   // Get the complete list of interfaces
    //Doc:Desc Returns a reference to a vector of all interfaces assigned
    //Doc:Desc to this node.
    //Doc:Return A vector of interface pointers for each interfae assigned
    //Doc:Return to this node.

  //Doc:Method
  virtual Interface* GetIfByLink(Link*); // Get interface to the specified link
    //Doc:Desc Returns the interface associated with the specified link.
    //Doc:Arg1 A pointer to the link object to search for.
    //Doc:Return The interface pointer associated with the specified link.
    //Doc:Return Returns {\em nil} if not found.

  //Doc:Method
  virtual Interface* GetIfByNode(Node*); // Get interface to the specified node
    //Doc:Desc Returns the interface used to communicate with the specified
    //Doc:Desc node.
    //Doc:Arg1 A pointer to the node to search for.
    //Doc:Return A pointer to the interface used to communicate with the
    //Doc:Return specified node.  Returns {\em nil} if not found.

  //Doc:Method
  virtual Interface* GetIfByIP(IPAddr_t);// Get interface by IP Address
    //Doc:Desc Returns the interface with the specified \IPA.
    //Doc:Arg1 The \IPA\ to find.
    //Doc:Return A pointer to the interface with the specified \IPA.
    //Doc:Return Returns {\em nil} if not found.

  //Doc:Method
  void       Broadcast(Packet*, Proto_t);// Broadcast packet on all interfaces
    //Doc:Desc Broadcasts a packet on all associated interfaces.
    //Doc:Arg1 The packet to send.  Each interface gets a separate copy of the
    //Doc:Arg1 packet, so the caller is still responsible for the memory
    //Doc:Arg1 for the packet, and should delete this packet when no
    //Doc:Arg1 longer needed.
    //Doc:Arg2 The layer 3 protocol number to insert in the layer 2
    //Doc:Arg2 header for thiis packet.

  // Links.  All return local interface
  // Add duplex link to/from remote
  //Doc:Method
  Interface* AddDuplexLink(Node*);
    //Doc:Desc Adds a new duplex link (and  an associated interface) to
    //Doc:Desc this node, using the default link type.
    //Doc:Arg1 A node pointer to the peer node for this link.
    //Doc:Return A pointer to the  newly added  interface
    //Doc:Return on the local node.

  //Doc:Method
  Interface* AddDuplexLink(Node*, const Linkp2p&);
    //Doc:Desc Adds a new duplex link (and  an associated interface) to
    //Doc:Desc this node, using the specified link type.
    //Doc:Arg1 A node pointer to the peer node for this link.
    //Doc:Arg2 The type of link to add. The passed argument is copied,
    //Doc:Arg2 so an anonymous temporary object can be passed, or
    //Doc:Arg2 the same link object can be passed multiple times.
    //Doc:Return A pointer to the  newly added  interface
    //Doc:Return on the local node.

  // Next two specify new links between existing interfaces
  //Doc:Method
  Interface* AddDuplexLink(Interface*,Interface*);
    //Doc:Desc Add a new duplex link between two existing interfaces,
    //Doc:Desc using the default link type.
    //Doc:Arg1 First existing interface.
    //Doc:Arg2 Second existing interface.
    //Doc:Return A pointer to the first interface specified.

  //Doc:Method
  Interface* AddDuplexLink(Interface*,Interface*, const Linkp2p&);
    //Doc:Desc Add a new duplex link between two existing interfaces,
    //Doc:Desc using the specified link object type.
    //Doc:Arg1 First existing interface.
    //Doc:Arg2 Second existing interface.
    //Doc:Arg3 The link object to use.  This object is copied, so an
    //Doc:Arg3 anonymous temporary object can be used, or the same
    //Doc:Arg3 object can be passed multiple times.
    //Doc:Return A pointer to the first interface specified.

  //Doc:Method
  //Interface* AddDuplexLink(Interface*,Interface*, const Linkp2p&, Node*);
    //Doc:Desc Add a new duplex link between two existing interfaces,
    //Doc:Desc using the specified link object type.
    //Doc:Arg1 First existing interface.
    //Doc:Arg2 Second existing interface.
    //Doc:Arg3 The link object to use.  This object is copied, so an 
    //Doc:Arg3 anonymous temporary object can be used, or the same
    //Doc:Arg3 object can be passed multiple times.
    //Doc:Arg4 Pointer to the remote node
    //Doc:Return A pointer to the first interface specified.
  

  // Next two specify local and remote IPAddress and Masks
  //Doc:Method
  Interface* AddDuplexLink(Node*,
                           IPAddr_t, Mask_t = Mask(32),
                           IPAddr_t = IPADDR_NONE, Mask_t = Mask(32));
    //Doc:Desc Adds a new duplex link (and  an associated interface) to
    //Doc:Desc this node, using the default link type, and specifying
    //Doc:Desc the \IPA\ and mask for both new interfaces.
    //Doc:Arg1 A node pointer to the peer node for this link.
    //Doc:Arg2 The \IPA\ for the local interface.
    //Doc:Arg3 The address mask for the local interface.
    //Doc:Arg4 The \IPA\ for the remote interface.
    //Doc:Arg5 The address mask for the remote interface.
    //Doc:Return A pointer to the  newly added  interface
    //Doc:Return on the local node.

  //Doc:Method
  Interface* AddDuplexLink(Node*, const Linkp2p&,
                           IPAddr_t, Mask_t = Mask(32),
                           IPAddr_t = IPADDR_NONE, Mask_t = Mask(32));
    //Doc:Desc Adds a new duplex link (and  an associated interface) to
    //Doc:Desc this node, using the specified link type, and specifying
    //Doc:Desc the \IPA\ and mask for both new interfaces.
    //Doc:Arg1 A node pointer to the peer node for this link.
    //Doc:Arg2 The type of link to add. The passed argument is copied,
    //Doc:Arg2 so an anonymous temporary object can be passed, or
    //Doc:Arg2 the same link object can be passed multiple times.
    //Doc:Arg3 The \IPA\ for the local interface.
    //Doc:Arg4 The address mask for the local interface.
    //Doc:Arg5 The \IPA\ for the remote interface.
    //Doc:Arg6 The address mask for the remote interface.
    //Doc:Return A pointer to the  newly added  interface
    //Doc:Return on the local node.

  // Next two add "LinkRTI" objects for distributed simulations
  //Doc:Method
  Interface* AddRemoteLink(IPAddr_t, Mask_t);
    //Doc:Desc Used for distributed simulations only.  Defines a link from
    //Doc:Desc this node to another node defined and managed on another
    //Doc:Desc simulator process.  The default link bandwidth and
    //Doc:Desc link delay are used.
    //Doc:Arg1 The \IPA\ for the local interface.
    //Doc:Arg2 The address mask for the local interface.

  //Doc:Method
  Interface* AddRemoteLink(IPAddr_t, Mask_t, Rate_t, Time_t);
    //Doc:Desc Used for distributed simulations only.  Defines a link from
    //Doc:Desc this node to another node defined and managed on another
    //Doc:Desc simulator process.
    //Doc:Arg1 The \IPA\ for the local interface.
    //Doc:Arg2 The address mask for the local interface.
    //Doc:Arg3 The bandwidth for the remote link.
    //Doc:Arg4 The propagation delay for the remote link.

  //Doc:Method
  Link*      GetLink(Node*);                // Get link to specified node
    //Doc:Desc Returns a pointer to the link used to communicate with
    //Doc:Desc the specified node.
    //Doc:Arg1 Node pointer to find a link for.
    //Doc:Return The link pointer for the link used to communicate with
    //Doc:Return the specified node.  Returns {\em nil} is none is found.

  // Simplex links, for modeling one-way links
  //Doc:Method
  Interface* AddSimplexLink(Node*);
    //Doc:Desc Add a simplex link from this node to the specified node, using
    //Doc:Desc the default link object and with no \IPA\ or mask.
    //Doc:Arg1 The node pointer for the remote node.
    //Doc:Return An interface pointer for the local interface added
    //Doc:Return for this link.

  //Doc:Method
  Interface* AddSimplexLink(Node*, const Linkp2p&);
    //Doc:Desc Add a simplex link from this node to the specified node, using
    //Doc:Desc the specified link object and with no \IPA\ or mask.
    //Doc:Arg1 The node pointer for the remote node.
    //Doc:Arg2 The link object to copy for this simplex link.
    //Doc:Return An interface pointer for the local interface added
    //Doc:Return for this link.

  //Doc:Method
  Interface* AddSimplexLink(Node*, const Linkp2p&, IPAddr_t, Mask_t);
    //Doc:Desc Add a simplex link from this node to the specified node, using
    //Doc:Desc the specified link object and with
    //Doc:Desc the specified \IPA\ and mask.
    //Doc:Arg1 The node pointer for the remote node.
    //Doc:Arg2 The link object to copy for this simplex link.
    //Doc:Arg3 The \IPA\ for the local interface.
    //Doc:Arg4 The address mask for the local interface.
    //Doc:Return An interface pointer for the local interface added
    //Doc:Return for this link.

  // Queues
  //Doc:Method
  Queue*     GetQueue();                    // Return queue if only one i/f
    //Doc:Desc Get a pointer to the queue object for this node.  Since nodes
    //Doc:Desc can have multiple interfaces and queues, this is useful
    //Doc:Desc only for nodes with a single interface.
    //Doc:Return A pointer to the single queue for this node.
    //Doc:Return Returns {\em nil}
    //Doc:Return if no interfaces, or more than one interface.

  //Doc:Method
  Queue*     GetQueue(Node*);               // Get the queue to specified node
    //Doc:Desc Get a pointer to the queue object used to buffer packets
    //Doc:Desc destined for the specified node.
    //Doc:Arg1 The target node to locate the queue for.
    //Doc:Return A pointer to the queue object uesd to buffer packets
    //Doc:Return destined for the specified node.  Returns {\em nil} if
    //Doc:Return none is found.

  // Applications
  //Doc:Method
  Application* AddApplication(const Application& a);
    //Doc:Desc Adds an application associated with this node.
    //Doc:Arg1 The application object to use.
    //Doc:Arg1 This application object is copied,
    //Doc:Arg1 so an anonymous temporary object can be specified, or the
    //Doc:Arg1 same application object can be used multiple times.
    //Doc:Return A pointer to the newly added application.

  // Neighbor management (used by some routing protocols)
  //Doc:Method
  virtual void Neighbors(NodeWeightVec_t &, bool = false); // Get all neighbors
    //Doc:Desc Get a list of all neighbors for this node.
    //Doc:Desc An output parameter, returning a vector of nodes and link
    //Doc:Arg1 weights to each directly connected neighbor.
    //Doc:Arg1 True if skip leaf neighbors desired

  void        AddNeighbor(Node*, Weight_t); // Ghost nodes keep neighbor list
  //Doc:Method
  Count_t     NeighborCount();              // Get count of routing neighbors
    //Doc:Desc Get a count of directly connected neighbors.
    //Doc:Desc     //Doc:Arg1 The count of directly connected neighbors.

  // Get neighbors on given iface
  //Doc:Method
  void        NeighborsByIf(Interface*, IPAddrVec_t&);
    //Doc:Desc Get a list of \IPA{s} neighbors connected using the specified
    //Doc:Desc interface.
    //Doc:Arg1 The interface to return the neighbors for.
    //Doc:Arg2 An output parameter giving a vector of \IPA{s} connected
    //Doc:Arg2 on the specified interface.

  // Routing
  //Doc:Method
  void DefaultRoute(RoutingEntry);    // Specify default route
    //Doc:Desc Specifies the default route for packets leaving
    //Doc:Desc this node.
    //Doc:Arg1 The routing entry (interface and \IPA) to use when
    //Doc:Arg1 no other routing information can be found.

  //Doc:Method
  void DefaultRoute(Node*);    // Specify default route
    //Doc:Desc Specifies the default route for packets leaving
    //Doc:Desc this node.
    //Doc:Arg1 The next hop neighbor to use for routing when
    //Doc:Arg1 no other routing information can be found.

  //Doc:Method
  void AddRoute(IPAddr_t, Count_t, Interface*, IPAddr_t);
    //Doc:Desc Add a new routing table entry.
    //Doc:Arg1 The \IPA\ specifying the destination address this
    //Doc:Arg1 routing entry  is for.
    //Doc:Arg2 The mask for the destination \IPA, which allows
    //Doc:Arg2 route aggregation.
    //Doc:Arg3 The interface for the next hop target for the
    //Doc:Arg3 specified destination.
    //Doc:Arg4 The \IPA\ of the next hop target.

  //Doc:Method
  RoutingEntry LookupRoute(IPAddr_t);    // Find a routing entry
    //Doc:Desc Look up the routing table entry for a specified destination
    //Doc:Desc \IPA.
    //Doc:Arg1 The destination \IPA\ to look up.
    //Doc:Return The routing table entry for the specified destination.
    //Doc:Return The interface pointer in this entry will be {\em nil}
    //Doc:Return if no routing information is found for the specified
    //Doc:Return destination.

  //Doc:Method
  RoutingEntry LookupRouteNix(Count_t);  // Lookup route by Neighbor Index
    //Doc:Desc Look up the routing table entry by a neighbor index,
    //Doc:Desc used by the \NV\ routing method.
    //Doc:Arg1 The neighbor index to look up.
    //Doc:Return The routing table entry for the specified neighbor index.
    //Doc:Return The interface pointer in this entry will be {\em nil}
    //Doc:Return if no routing information is found for the specified
    //Doc:Return destination.

  //Doc:Method
  Routing::RType_t RoutingType();        // Report type of routing in use
    //Doc:Desc Return the routing type in use at this node.
    //Doc:Return The routing type in use.  The enumerated type {\tt RType_t}
    //Doc:Return is defined in {\tt routing.h}.

  //Doc:Method
  Interface* LocalRoute(IPAddr_t);       // Find if dst is directly connected
    //Doc:Desc Determine if the specified \IPA\ is on a directly connected
    //Doc:Desc neighbor.
    //Doc:Arg1 The \IPA\ to search for.
    //Doc:Return An pointer to the interface used to communicate  with the
    //Doc:Return specified \IPA.  Returns {\em nil} if the specified
    //Doc:Return  \IPA\ is not a directly connected neighbor.

  //AODV Routing
  //Specially for AODV routing, it is not compatiable with
  //other route entry.
  //
  AODVRoutingEntry *LookupRouteAODV(IPAddr_t);
  void      SetRoutingAODV(void *pRouting);
  RoutingAODV* GetRoutingAODV(); //return the AODV routing object pointer

  //Doc:Method
  void       InitializeRoutes();         // Routing initialization (if any)
    //Doc:Desc Perform any initializations needed by the routing protocol
    //Doc:Desc used by this node.

  //Doc:Method
  void       ReInitializeRoutes(bool);   // Reinit due to topology chanbe
    //Doc:Desc Validates existing routes and re-calculates as necessary
    //Doc:Arg1 True if node or interface UP, false if DOWN

  //Doc:Method
  Count_t    RoutingFibSize() const;     // Statistics, size of FIB
    //Doc:Desc Determine the size of the forwarding information base (FIB)
    //Doc:Desc at this node.
    //Doc:Return The size, in bytes, of the FIB at this node.

  //Doc:Method
  Count_t    GetNix(Node*) const;        // Get Neighbor Index for spec'd node
    //Doc:Desc Get the neighbor index for the specified node.  Used by the
    //Doc:Desc \NV\ routing method.
    //Doc:Arg1 The node to find the neighbor index for.
    //Doc:Return The neighbor index for the specified node.
    //Doc:Return Returns MAX_COUNT if the specified node is not a neighbor.

  //Doc:Method
  RoutingNixVector* GetNixRouting();     // Returns Nix rtg pointer (if Nix)
    //Doc:Desc Get a pointer to the \NV\ routing object for this node.
    //Doc:Return A pointer to the \NV\ routing object for this node.
    //Doc:Return Returns {\em nil} if \NV\ routing is not used at this node.

  //Doc:Method
  Routing*   GetRouting();
    //Doc:Desc Get a pointer to the routing object used by this node.
    //Doc:Return Routing object pointer.

  void       DumpNV();                   // Debug, dump NixVector info

  // Protocol Graph Interface
  //Doc:Method
  Protocol* LookupProto(Layer_t, Proto_t);// Lookup protocol by layer
    //Doc:Desc Find the protocol object at the specified protocol layer,
    //Doc:Desc and protocol number.
    //Doc:Arg1 The protocol layer number to look up.
    //Doc:Arg2 The protocol number to look up.
    //Doc:Return A pointer to the protocol object at the specified layer and
    //Doc:Return protocol number.

  //Doc:Method
  void      InsertProto(Layer_t, Proto_t, Protocol*); // Insert a protocol
    //Doc:Desc Inserts a new protocol in the protocol graph.
    //Doc:Arg1 The protocol layer number to insert the protocol into.
    //Doc:Arg2 The protocol number to insert the protocol intol
    //Doc:Arg3 A protocol to insert.

  // Port Demux Interface
  //Doc:Method
  bool      Bind(Proto_t, PortId_t, Protocol*);    // Register port usage
    //Doc:Desc Bind a protocol to a port number at a particular protocol
    //Doc:Desc number.
    //Doc:Arg1 The protocol number for this port.
    //Doc:Arg2 The port to bind to.
    //Doc:Arg3 The protocol to bind to the port.
    //Doc:Return True if successful.

  //Doc:Method
  bool      Bind(Proto_t,
                 PortId_t, IPAddr_t,
                 PortId_t, IPAddr_t,
                 Protocol*);                       // Register port usage
    //Doc:Desc Binds a protocol to a local \IPA, port, remote \IPA, port,
    //Doc:Desc at a particular protocol number.
    //Doc:Arg1 The protocol number for this binding.
    //Doc:Arg2 The local port number.
    //Doc:Arg3 The local \IPA.
    //Doc:Arg4 The remote port number.
    //Doc:Arg5 The remote \IPA.
    //Doc:Arg6 The protocol to bind.
    //Doc:Return True if successful.

  //Doc:Method
  PortId_t  Bind(Proto_t, Protocol*);              // Choose port and register
    //Doc:Desc Bind a protocol to any available  port.
    //Doc:Arg1 The protocol number to use.
    //Doc:Arg2 The protocol to bind.
    //Doc:Return The port number bound to.

  //Doc:Method
  bool      Unbind(Proto_t, PortId_t, Protocol*);  // Remove port binding
    //Doc:Desc Remove a port binding.
    //Doc:Arg1 The protocol number to use.
    //Doc:Arg2 The currently bound port number.
    //Doc:Arg3 The protocol to unbind.
    //Doc:Return True if successful.

  //Doc:Method
  bool      Unbind(Proto_t,
                 PortId_t, IPAddr_t,
                 PortId_t, IPAddr_t,
                 Protocol*);                       // Remove port binding
    //Doc:Desc Remove a port binding by local and remote port and \IPA.
    //Doc:Arg1 The protocol number to use.
    //Doc:Arg2 Local port number.
    //Doc:Arg3 Local \IPA.
    //Doc:Arg4 Remote port number.
    //Doc:Arg5 Remote \IPA.
    //Doc:Arg6 Protocol to unbind.
    //Doc:Return True if successful.

  //Doc:Method
  Protocol* LookupByPort(Proto_t, PortId_t);       // Lookup by port
    //Doc:Desc Locate the protocol bound to a specified port and protocol
    //Doc:Desc number.
    //Doc:Arg1 The protocol number.
    //Doc:Arg2 The port to look up.
    //Doc:Return A pointer to the protocol bound to the specified port.
    //Doc:Return Returns {\em nil} if no protocol is found at the
    //Doc:Return specified protocol and port.

  //Doc:Method
  Protocol* LookupByPort(Proto_t,
                         PortId_t, IPAddr_t,
                         PortId_t, IPAddr_t);      // Lookup by s/d port/ip
    //Doc:Desc Locate the protocol bound to the specified local and remote
    //Doc:Desc port and \IPA, at the specified protocol number.
    //Doc:Arg1 The protocol number.
    //Doc:Arg2 Local port number.
    //Doc:Arg3 Local \IPA.
    //Doc:Arg4 Remote port number.
    //Doc:Arg5 Remote \IPA.
    //Doc:Return A pointer to the protocol bound as specified.
    //Doc:Return Returns {\em nil} if no protocol is found.

  // Packet tracing interface
  //Doc:Method
  // Trace header if enabled
  bool      TracePDU(Protocol*, PDU*, Packet* = nil, char* = nil);
    //Doc:Desc Log the contents of the specified protocol data unit, from
    //Doc:Desc the specified protocol, to the trace file.
    //Doc:Arg1 The protocol pointer for the protocol object requesting
    //Doc:Arg1 the trace.
    //Doc:Arg2 The protocol data unit pointer.
    //Doc:Arg3 Packet that contains this PDU (optional)
    //Doc:Arg4 Extra text information for trace file (optional)
    //Doc:Return Returns true  if trace file  exists.

  //Doc:Method
  void      SetTrace(Trace::TraceStatus);          // Set trace level this node
    //Doc:Desc Specifies the tracing status of this node.
    //Doc:Desc The trace status desired.  This can be either
    //Doc:Desc {\tt Trace::ENABLED},
    //Doc:Desc which indicates that all packets requesting tracing
    //Doc:Desc at this node will be traced;
    //Doc:Desc {\tt Trace::DISABLED} indicating that no packets will
    //Doc:Desc be traced, or {\tt Trace::DEFAULT}, indicating that the
    //Doc:Desc decision is deferred to the protocols at this node.
    //Doc:Arg1 The trace status desired for this node.

  // Node location (x, y, z) interface
  //Doc:Method
  void      SetLocation(Meters_t, Meters_t, Meters_t=0);  // Set x/y/z location
    //Doc:Desc Set the location of this node in the X--Y--Z coordinate.
    //Doc:Arg1 The X--coordinate (meters).
    //Doc:Arg2 The Y--coordinate (meters).
    //Doc:Arg3 The Z--coordinate (meters).

  //Doc:Method
  void      SetLocation(const Location&);          // Set x/y/z location
    //Doc:Desc Set the location of this node in the X--Y--Z coordinate.
    //Doc:Arg1 A {\tt Location} object specifying
    //Doc:Arg1 the X--Y--Z coordinate (meters).

  //Doc:Method
  void      SetLocationLongLat(const Location&);  // Set x/y location
    //Doc:Desc Set the location of this node in the X--Y plane.
    // Adjusts the lat/long by the cos(lat) factor for  map projections.
    //Doc:Arg1 A {\tt Location} object specifying
    //Doc:Arg1 the X--coordinate (longitued) and Y--coordinate (latitude).

  //Doc:Method
  bool      HasLocation();
    //Doc:Desc Test if the node has a location assigned.
    //Doc:Return True if location assigned for this node.

  //Doc:Method
  Meters_t  LocationX();
    //Doc:Desc Return the X--coordinate of this node.
    //Doc:Return The X--coordinate (meters) of this node.

  //Doc:Method
  Meters_t  LocationY();
    //Doc:Desc Return the Y--coordinate of this node.
    //Doc:Return The Y--coordinate (meters) of this node.

  //Doc:Method
  Meters_t  LocationZ();
  //Doc:Desc Return the Z--coordinate of this node.
  //Doc:Return The Z--coordinate (meters) of this node.

  //Doc:Method
  Location  GetLocation();                         // Return current location
    //Doc:Desc Return the location of this node.
    //Doc:Return A {\tt Location} object specifying the X, Y and Z coordinates
    //Doc:Return of this node (meters).

  //Doc:Method
  Location  UpdateLocation();                      // Get updated location
    //Doc:Desc The location of mobile nodes is not updated continually
    //Doc:Desc during the simulation.  Rather,  it  is updated when
    //Doc:Desc {\tt UpdateLocation()} is called.
    //Doc:Return A {\tt Location} object specifying the X, Y and Z coordinates
    //Doc:Return of this node (meters).

  //Doc:Method
  Mobility* AddMobility(const Mobility&);          // Add a mobility model
    //Doc:Desc Add a mobility model to this node.
    //Doc:Arg1 The {\tt Mobility} object to use.  This is copied to the
    //Doc:Arg1 node, so an anonymous temporary object can be used,
    //Doc:Arg1 or the same {\tt Mobility} object can be passed
    //Doc:Arg1 multiple times.
    //Doc:Return A pointer to the added mobility object.

  //Doc:Method
  Mobility* GetMobility();
    //Doc:Desc   Return existing mobility model.
    //Doc:Return Pointer to this nodes mobility model.  NIL if none.

  //Doc:Method
  bool      IsMobile();                            // True if mobility enabled
    //Doc:Desc Determine if this node has a mobility model.
    //Doc:Return True  if node has a mobility model.

  //Doc:Method
  bool      IsMoving();                            // True if not pausing
    //Doc:Desc Determine if this node is presently moving (rather than
    //Doc:Desc paused or temporarily stationary).
    //Doc:Return True if node is presently moving.

  //Doc:Method
  Meters_t  Distance(Node*);
    //Doc:Desc Calculate distance between this node and specified peer node.
    //Doc:Arg1 Peer node pointer.
    //Doc:Return Distance (meters).

  //Doc:Method
  void      BuildRadioInterfaceList(WirelessLink*); // i/f list in radio range
    //Doc:Desc Build a list of nodes in transmission range.

  //Doc:Method
  const RadioVec_t& GetRadioInterfaceList();
    //Doc:Desc Get a reference to the current radio interface list
    //Doc:Return Reference to the current radio interface list

  //Doc:Method
  void      SetRadioRange(Meters_t range);
    //Doc:Desc Specify the power range of this wireless transmiter (meters).
    //Doc:Arg1 Power range (meters)

  //Doc:Method
  Meters_t  GetRadioRange(void);
    //Doc:Desc Return the power range of this transmitter.
    //Doc:Return Power range (meters).

  // Callback methods
  //Doc:Method
  void      AddCallback(Layer_t, Proto_t,
                        PacketCallbacks::Type_t,
                        Interface*, PacketCallbacks::Function_t);
    //Doc:Desc Add a callback function to this node.  Callbacks allow
    //Doc:Desc a function to examine every packet recieved by this node
    //Doc:Desc at any protocol layer, and either packet receipt or
    //Doc:Desc  packet transmission
    //Doc:Arg1 Layer to insert callback.  Specifying zero indicates
    //Doc:Arg1 all layers will call the callback.
    //Doc:Arg2 Protocol number for callback.  Specifying zero indicates
    //Doc:Arg2 the callback should be called for all protocols at the
    //Doc:Arg2 specified layer.
    //Doc:Arg3 Type of callback. Either PacketCallbacks::RX, indicating
    //Doc:Arg3 a callback on packet receipt, or PacketCallbacks::Tx
    //Doc:Arg3 indicating callback on packet transmission.
    //Doc:Arg4 Interface pointer.  Layer 2 callbacks can specify an additional
    //Doc:Arg4 restriction specifying a particular interface only.
    //Doc:Arg5 Callback function to add.
    //Doc:Arg5 The callback function must return true or false indicating
    //Doc:Arg5 that the packet should (true) or should not (false)
    //Doc:Arg5 continue processing by the caller.  In other words, 
    //Doc:Arg5 if the callback function drops the packet, it sould
    //Doc:Arg5 return false.  The callback function must accept five
    //Doc:Arg5 arguments: Layer_t, PacketCallbacks::Type_t, Packet*
    //Doc:Arg5 Node* and Interface*.
    //Doc:Arg5 The layer is the layer number from which the
    //Doc:Arg5 callback was generated, the Type_t is
    //Doc:Arg5 either RX or TX as described above, the 
    //Doc:Arg5 Packet* is the packet, and the Node*
    //Doc:Arg5 is the node processing the packet.  The Interface*
    //Doc:Arg5 is the interface processing the packet (layer 2 only)

  void      AddCallbackHead(Layer_t, Proto_t,
                        PacketCallbacks::Type_t,
                        Interface*, PacketCallbacks::Function_t);
    //Doc:Desc Add a callback function to this node to the HEAD
    //Doc:Desc   of the callback list, calling this before previous callbacks.
    //Doc:Arg1 Layer to insert callback.  Specifying zero indicates
    //Doc:Arg1 all layers will call the callback.
    //Doc:Arg2 Protocol number for callback.  Specifying zero indicates
    //Doc:Arg2 the callback should be called for all protocols at the
    //Doc:Arg2 specified layer.
    //Doc:Arg3 Type of callback. Either PacketCallbacks::RX, indicating
    //Doc:Arg3 a callback on packet receipt, or PacketCallbacks::Tx
    //Doc:Arg3 indicating callback on packet transmission.
    //Doc:Arg4 Interface pointer.  Layer 2 callbacks can specify an additional
    //Doc:Arg4 restriction specifying a particular interface only.
    //Doc:Arg5 Callback function to add.
    //Doc:Arg5 Arguments are identical to those discussed in
    //Doc:Arg5 {\tt AddCallback} above.

  //Doc:Method
  void      DeleteCallback(Layer_t, Proto_t, PacketCallbacks::Type_t,
                           Interface*);
    //Doc:Desc Remove a callback function from this node.
    //Doc:Arg1 Layer to remove callback from.
    //Doc:Arg2 Protocol number to delete.
    //Doc:Arg3 Type or callback to remove, either TX or RX as above.
    //Doc:Arg4 Interface to remove callback from.

  //Doc:Method
  bool      CallCallbacks(Layer_t, Proto_t,
                          PacketCallbacks::Type_t, Packet*, Interface* = nil);
    //Doc:Desc Call any callback defined on this node for the layer and type
    //Doc:Desc  specified.
    //Doc:Arg1 Layer for callback.
    //Doc:Arg2 Protocol number for callback.
    //Doc:Arg3 Type for callback.
    //Doc:Arg4 Packet being processed.
  //Doc:Method
  void UserInformation(void*);
    //Doc:Desc Nodes have a blind pointer that can contain arbitrary
    //Doc:Desc information useful the the user's simulation.
    //Doc:Arg1 Pointer to any information.  Will be stored in the node
    //Doc:Arg1 object, and returned as needed.

  //Doc:Method
  void* UserInformation();
    //Doc:Desc Return the previously specified user information pointer.
    //Doc:Return User information pointer (nil if none specified)


  //Doc:Method
  void UseWormContainment(bool b);
  //Doc:Desc Specifies if the node is using a wormcontainment algorithm or not
  //Doc:Arg1 True means use containment, false dont use

  //Doc:Method
  bool UseWormContainment();
  //Doc:Desc Is this node using worm containment
  //Doc:Return True means use containment, false dont use

  //Doc:Method
  void SetWormContainment(WormContainment*);
  //Doc:Desc Set the worm containment module for this node
  //Doc:Arg1 Worm containment module


  //Doc:Method
  WormContainment* GetWormContainment();
  //Doc:Desc Gives a reference to the wormcontainment module of this node
  //Doc:Return Worm containment module


  //Doc:Method
  void UseARP(bool);
  //Doc:Desc Specifies if the node is using arp module or not
  //Doc:Arg1 True means use ARP, false dont use

  //Doc:Method
  bool FirstBitRx();
    //Doc:Desc Specified if the node needs a "first bit received" event.
    //Doc:Desc Some nodes, such as satellites and super-computer interconnects
    //Doc:Desc forward packets before the entire packet is received.

  //Doc:Method
  void FirstBitRx(bool);
    //Doc:Desc Specify the node needs first bit received event.
    //Doc:Arg1 True if first bit received event needed.

  //Doc:Method
  bool      WirelessTx();
    //Doc:Desc Check if the node is transmitting on a wireless link.  Used by
    //Doc:Desc the animation widow.
    //Doc:Return True if transmitting.

  //Doc:Method
  bool      WirelessRx();
    //Doc:Desc Check if the node is receiving on a wireless link.  Used by
    //Doc:Desc the animation widow.
    //Doc:Return True if receiving.

  //Doc:Method
  bool      WirelessCx();
    //Doc:Desc Check if the node is experiencing collision on a wireless link.
    //Doc:Desc Used by the animation widow.
    //Doc:Return True if colliding.

  //Doc:Method
  bool      WirelessRxMe();
    //Doc:Desc Check if the node is receiving on a wireless link.  Used by
    //Doc:Desc the animation widow.
    //Doc:Return True if receiving.

  //Doc:Method
  bool      WirelessRxZz();
    //Doc:Desc Check if the node is receiving on a wireless link.  Used by
    //Doc:Desc the animation widow.
    //Doc:Return True if receiving.

  // QTWindow information
  //Doc:Method
  void      Show(bool);                            // Turn on/off display
    //Doc:Desc Enable  or disable the animation window.
    //Doc:Arg1 True if animation window should be enabled, false if not.

  //Doc:Method
  bool      Show();                                // True if display enabled
    //Doc:Desc Determine if the node should be displayed in the anim window
    //Doc:Return True if the node should be displayed in the animation window

  //Doc:Method
  QCanvasItem* Display(QTWindow*);
    //Doc:Desc Each node is responsible for "drawing" itself on the animation
    //Doc:Desc display.  This allows the node to decide shape, size, etc.
    //Doc:Arg1 Qt Canvas to use.  The node is drawn at present Location().
    //Doc:Return Any subclass of QCanvasItem.

  //Doc:Method
  QCanvasItem* Display(const QPoint&, QTWindow*);
    //Doc:Desc Each node is responsible for "drawing" itself on the animation
    //Doc:Desc display.  This allows the node to decide shape, size, etc.
    //Doc:Arg1 Point in Qt canvas to display this node.
    //Doc:Arg2 Qt Canvas to use.
    //Doc:Return Any subclass of QCanvasItem.

  //Doc:Method
  void WirelessTxColor(const QColor&);
    //Doc:Desc Specify color for the wireless transmit animation.
    //Doc:Arg1 Desired color.

  //Doc:Method
  const QColor& WirelessTxColor();
    //Doc:Desc Get the specified  wireless transmit color
    //Doc:Return Color for wireless tx concentric  circles

  //Doc:Method
  bool PushWirelessTx(QCanvasItem*);
    //Doc:Desc Add a canvas itemm to the wireless transmit animation list
    //Doc:Arg1     Item (one of the concentric circles) to  add

  //Doc:Method
  QCanvasItem* PopWirelessTx();
    //Doc:Desc   Pop and return the oldest item in the wireless tx history.
    //Doc:Return Oldest item (nil if none).

  //Doc:Method
  void PixelSize(Count_t);
    //Doc:Desc Set the size of this node (pixels) on the animation display.
    //Doc:Arg1 Size of this node in pixels (both width and height)

  //Doc:Method
  Count_t PixelSizeX();
    //Doc:Desc Query the width of this node (pixels) on the animation display.
    //Doc:Return Size of this node in pixels.

  //Doc:Method
  Count_t PixelSizeY();
    //Doc:Desc Query the height of this node (pixels) on the animation display.
    //Doc:Return Size of this node in pixels.

  //Doc:Method
  void        Shape(Shape_t);
    //Doc:Desc Set the shape for this node on the animation display.
    //Doc:Arg1 Shape for this node.  See node.h for shape types.

  //Doc:Method
  Shape_t Shape();
    //Doc:Desc Query the shape for this node on the animation display.
    //Doc:Return Shape for this node.  See node.h for shape types.

  //Doc:Method
  CustomShape_t CustomShape();
    //Doc:Desc   Get the custom shape callback function.
    //Doc:Return Function pointer to custom shape callback function.

  //Doc:Method
  void CustomShape(CustomShape_t);
    //Doc:Desc Specify the custom shape callback function.
    //Doc:Arg1 Pointer to new custom shape callback function.

  //Doc:Method
  bool CustomShapeFile(const char*);
    //Doc:Desc Specifies a file containing the custom shape image.
    //Doc:Desc Must be in an image format supported by Qt.
    //Doc:Desc NOTE:  This {\bf must} be called {\em  after} the call
    //Doc:Desc to Simulator::StartAnimation, since it needs a QTWindow
    //Doc:Desc object.  If called before StartAnimation, will always
    //Doc:Desc return false.
    //Doc:Arg1 File name for image.
    //Doc:Return True if  successfully loaded

  //Doc:Method
  bool CustomShapeImage(const Image&);
    //Doc:Desc Specifies a pre--defined image for the custom shape.
    //Doc:Desc GTNetS has a number of pre--defined and pre--processed
    //Doc:Desc images that can be used for custom shapes.  See the IMAGES
    //Doc:Desc directory for the raw images.  This method of setting
    //Doc:Desc custom shapes is preferred to the above file name method.
    //Doc:Arg1 One of the pre--defined Images, for example SatelliteImage.
    //Doc:Return True if successfully loaded.

  //Doc:Method
  void Color(const QColor&);
    //Doc:Desc Specifiy a color for this node
    //Doc:Arg1 Desired color (see qcolor.h in qt).

  //Doc:Method
  bool HasColor();
  //Doc:Desc Query if this node has a color assigned
  //Doc:Return True if color has been specified.

  //Doc:Method
  QColor& Color();
    //Doc:Desc Query color for this node.
    //Doc:Return Specified display color for this node.

  //Doc:Method
  NodeAnimation* GetNodeAnimation() const;
    //Doc:Desc   Get a pointer to the nodes NodeAnimation  information block.
    //Doc:Desc       Always nil  for Ghosts, possibly non-nil for real.
    //Doc:Return Associated NodeAnimation poiner.

  //Doc:Method
  bool   ICMPEnabled() const;
    //Doc:Desc Query if ICMP enabled for this node.
    //Doc:Return True if enabled. NOTE. This only means this node will use ICMP
    //Doc:Return if globally enabled (see icmp.h)

  //Doc:Method
  void   DisableICMP();
    //Doc:Desc   Specify this node does NOT use ICMP, even if globally enabled.

  // Node failure management

  //Doc:Method
  void   Down();
    //Doc:Desc Specifies the the node has crashed and should not forward or
    //Doc:Desc generate any packets.

  //Doc:Method
  void   Up();
    //Doc:Desc Specifies the node has recovered from failure.

  //Doc:Method
  bool   IsDown();
    //Doc:Desc Determine if node has failed or not.
    //Doc:Return       TRUE if node is down (failed)

  // Data Members
 private:
  NodeId_t  id;              // Node id for this node
 //protected:
 public:
  NodeImpl* pImpl;           // Node Implementation, ghost or real
  // Static members
public:
  static NodeId_t  nextId;        // Next available node id
  static NodeVec_t nodes;         // Vector of all available nodes
  static double    defaultMaxSpeed;  // Default maximum speed of a node
#ifdef HAVE_QT
  static Count_t   defaultPixelSize; // Size in pixels of animated nodes
  static Shape_t   defaultShape;     // Default shape for all nodes
#endif
public:
  //Doc:Method
  static void SetNextNodeId(NodeId_t i) { nextId = i;} // Set non-default Next
    //Doc:Desc Set a new value for the next node identifier value.
    //Doc:Arg1 The new next node identifier value.

  // Get const ref to Node vec
  //Doc:Method
  static const NodeVec_t& GetNodes() { return nodes;}
    //Doc:Desc Get the vector of all existing nodes.
    //Doc:Return A const reference to the vector of existing nodes.

  //Doc:Method
  static Node* GetNode(NodeId_t);  // Get a node pointer by id
    //Doc:Desc Get the node pointer for the specified node identifier.
    //Doc:Arg1 The node identifier desired.
    //Doc:Return A pointer to the specified node.  Returns {\em nil} if the
    //Doc:Return specified node does not exist.

  //Doc:Method
  static void Clear();
    //Doc:Desc Clear the node vector.
    //Doc:Desc Normally only called after the simulation
    //Doc:Desc   has completed, and we are trying to track memory leaks.

#ifdef HAVE_QT  
  //Doc:Method
  static void DefaultPixelSize(Count_t);
    //Doc:Desc Set the default size (in pixels) of the animated nodes
    //Doc:Arg1 Default size in pixels.

  //Doc:Method
  static Count_t DefaultPixelSize();
    //Doc:Desc Query the default node pixel size
    //Doc:Return The default node pixel size.

  //Doc:Method
  static void DefaultShape(Shape_t);
    //Doc:Desc Set the default shape for nodes on the animation display
    //Doc:Arg1 Default shape.  See node.h for shape types.

  //Doc:Method
  static Shape_t DefaultShape();
    //Doc:Desc Query the default node shape.
    //Doc:Return Default shape.  See node.h for shape types.

#endif
  static void   DefaultMaxSpeed(double speed);
  static double DefaultMaxSpeed();
  
  Joules_t getBattery (void);
  void     setBattery (Joules_t);

  double   getComputePower(void);
  void     setComputePower(double);

  // The following is for proxy routing
 private:
  IPAddr_t    proxyIP;
  Mask        proxyMask;

 public:
  void        SetProxyRoutingConfig(IPAddr_t ip, Mask mask);
  bool        HasProxyRoutingConfig() {return proxyIP!=IPADDR_NONE;};
  bool        CanProxyRouteIP(IPAddr_t ip);
  IPAddr_t    GetProxyIP();
  Mask        GetProxyMask();
  int         GetLongestPrefixLength(IPAddr_t ip);

 public:

  RouteTable* getRouteTable(void);
  void        setRouteTable(RouteTable*);
  
};

//#include "interface.h"

#endif
