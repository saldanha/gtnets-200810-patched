// Simple tester for TCP
// George F. Riley.  Georgia Tech, Spring 2002

#include <iostream>

#include "simulator.h"
#include "application-tcpsend.h"
#include "duplexlink.h"
#include "queue.h"
#include "ratetimeparse.h"
#include "tcp-tahoe.h"
#include "dumbbell.h"
#include "args.h"
#include "validation.h"  

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  // Create the simulator object
  Simulator s;
  Count_t nleaf = 1;
  
  Arg("nleaf", nleaf, 1);
  Arg::ProcessArgs(argc, argv);
  
  // Open the trace file
  Trace* tf = Trace::Instance();
  if (!Arg::Specified("nolog")) tf->Open("testtcp.txt");
  tf->IPDotted(true);

  // Enable l2 tracing
  //L2Proto802_3::Instance()->SetTrace(Trace::ENABLED);
  // Disable l2 tracing
  //L2Proto802_3::Instance()->SetTrace(Trace::DISABLED);

  // Increase detail of L3 trace messages
  IPV4::Instance()->DetailOn(IPV4::TOTALLENGTH);
  // Enable all l3 tracing
  IPV4::Instance()->SetTrace(Trace::ENABLED);


  // Set some defaults
  Queue::Default();
  Queue::DefaultLength(200000);    // 200,000 bytes
  Link::DefaultRate(Rate("10Mb")); // 10 Mega-bits/sec
  Link::DefaultDelay(Time("10ms"));// 10 milliseconds delay

  // Create the topology, using default link
  Dumbbell b(nleaf, nleaf, 0.1, IPAddr("192.168.1.1"), IPAddr("192.168.2.1"));
  b.BoundingBox(Location(0,0), Location(1,1));
  
  // Restrict queue limit on bottleneck
  if (!Arg::Specified("unrestricted")) b.LeftQueue()->SetLimit(10000);

  // If "forcedloss" specified, force 20 losses at time 1.0
  if (Arg::Specified("forcedloss")) b.LeftQueue()->AddForcedLoss(1.0, 20);

  // Set bottleneck link delay a bit longer
  b.LeftLink()->Delay(Time("100ms"));
  b.RightLink()->Delay(Time("100ms"));
  
  // Create the TCP endpoints
  TCP::LogFlagsText(true);    // Log flags in text mode
  for (Count_t k = 0; k < nleaf; ++k)
    {
      TCPTahoe* server = new TCPTahoe(b.Right(k));
      server->Bind(80);
      server->Listen();
      // Enable tracing
      server->SetTrace(Trace::ENABLED);
    }
  
  //client.Name("TestTCPClient");
  //client.EnableTimeSeq(TCP::LOG_SEQ_TX);
  //client.EnableTimeSeq(TCP::LOG_ACK_RX);
  //client.EnableTimeSeq(TCP::LOG_CWIN);

  // Create the TCP sending applications using TCPTahoe
  Uniform startRNG(0, 0.1);
  TCP* firstTCP = nil;
  for (Count_t k = 0; k < nleaf; ++k)
    {
      TCPSend* pApp = (TCPSend*)b.Left(k)->AddApplication(
          TCPSend(b.Right(k)->GetIPAddr(),// Peer IP Address
                  80,                     // Peer port number
                  Constant(1000000)));     // Number of bytes to send
      // Parameterize the tcp endpoint
      TCP* ctcp = pApp->l4Proto;
      ctcp->SetTrace(Trace::ENABLED);
      ctcp->Name("TestTCPClient");
      ctcp->EnableTimeSeq(TCP::LOG_SEQ_TX);
      ctcp->EnableTimeSeq(TCP::LOG_ACK_RX);
      ctcp->EnableTimeSeq(TCP::LOG_CWIN);
      if (Arg::Specified("extradelay"))
        {
          cout << "Adding extra delays" << endl;
          ctcp->AddExtraRxDelay(Time("10ms"));
          ctcp->AddExtraTxDelay(Time("10ms"));
        }
      pApp->Start(startRNG.Value());
      if (!firstTCP) firstTCP = ctcp;
    }

  s.StopAt(100.0);
  if (Arg::Specified("animate") && !Validation::noAnimation)
    { // Enable animiation if specified on command line
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Run();

  // Create the seq/time and cwin/time data files
  string nm = firstTCP->Name() + string("Txseq.txt");
  ofstream f(nm.c_str());
  firstTCP->LogTimeSeq(TCP::LOG_SEQ_TX, f, 512, 0); // Log to file
  f.close();
  string nm1 = firstTCP->Name() + string("Rxack.txt");
  ofstream f1(nm1.c_str());
  firstTCP->LogTimeSeq(TCP::LOG_ACK_RX, f1, 512, 0);// Log to file
  f1.close();
  string nm2 = firstTCP->Name() + string("Cwin.txt");
  ofstream f2(nm2.c_str());
  firstTCP->LogTimeSeq(TCP::LOG_CWIN, f2);  // Log to file
  f2.close();
}

