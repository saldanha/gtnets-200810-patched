// Test the dumbbell topology stock object.
// George F. Riley.  Georgia Tech, Fall 2002

#include "simulator.h"
#include "scheduler.h"
#include "dumbbell.h"
#include "linkp2p.h"
#include "ratetimeparse.h"
#include "tcp-tahoe.h"
#include "application-tcpsend.h"
#include "application-tcpserver.h"
#include "globalstats.h"
#include "droptail.h"
#include "ipv4.h"
#include "validation.h"

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;

  Queue::Default(DropTail()); // Set default to non-detailed drop tail
  Trace* gs = Trace::Instance();
  gs->IPDotted(true);
  gs->Open("testbell.txt");
  TCP::LogFlagsText(true);    // Log flags in text mode
  // Enable all l3 tracing
  IPV4::Instance()->SetTrace(Trace::ENABLED);

  Linkp2p l;
  l.Bandwidth(Rate("100Mb"));
  Dumbbell b(10, 10, 0.1, IPAddr("192.168.1.0"), IPAddr("192.168.2.0"), l);
  // Specify the bounding box
  b.BoundingBox(Location(0,0), Location(10,10));
  for (Count_t i = 0; i < b.LeftCount(); ++i)
    {
      cout << "Left  node " << i 
           << " ipaddr " << (string)IPAddr(b.Left(i)->GetIPAddr()) << endl;
    }
  for (Count_t i = 0; i < b.RightCount(); ++i)
    {
      cout << "Right node " << i 
           << " ipaddr " << (string)IPAddr(b.Right(i)->GetIPAddr()) << endl;
      // Add a tcp server on right side
      TCPServer* server = (TCPServer*)b.Right(i)->AddApplication(
          TCPServer());
      server->BindAndListen(40);
      server->GetL4()->SetTrace(Trace::ENABLED);
    }
  // Add a single tcp sender
  TCPSend* app = (TCPSend*)b.Left(0)->AddApplication(
      TCPSend(b.Right(0)->GetIPAddr(), 40, Constant(1000000)));
  app->l4Proto->SetTrace(Trace::ENABLED);
  app->Start(0);
  // Animate the left side bottleneck queue
  b.LeftQueue()->Animate();
  // Set an arbitrary queue limit
  b.LeftQueue()->SetLimitPkts(20);
  s.StartAnimation(0, true);
  s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
  s.Run();
  Stats::Print(); // Print the statistics
  cout << "Memory usage after run " 
       << s.ReportMemoryUsageMB() << "MB" << endl;
  cout << "Total events processed "
       << Scheduler::Instance()->TotalEventsProcessed() << endl;
  cout << "Setup time " << s.SetupTime() << endl;
  cout << "Route time " << s.RouteTime() << endl;
  cout << "Run time "   << s.RunTime() << endl;
}
