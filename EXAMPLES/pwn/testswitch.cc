// Simple GTNetS example
// $Id: testswitch.cc 114 2004-09-17 16:11:02Z riley $
// George F. Riley, Georgia Tech, Winter 2002

#include <iostream>

#include "simulator.h"      // Definitions for the Simulator Object
#include "node.h"           // Definitions for the Node Object
#include "linkp2p.h"        // Definitions for point-to-point link objects
#include "ratetimeparse.h"  // Definitions for Rate and Time objects
#include "application-tcpserver.h" // Definitions for TCPServer application
#include "application-tcpsend.h"   // Definitions for TCP Sending application
#include "tcp-tahoe.h"      // Definitions for TCP Tahoe
#include "l2proto802.3.h"

#ifdef HAVE_QT
#include <qnamespace.h>
#endif

using namespace std;


int main()
{
  // Create the simulator object    
  Simulator s;

  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);

  // Create and enable IP packet tracing
  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->IPDotted(true);            // Trace IP addresses in dotted notation
  tr->Open("testswitch.txt");    // Create the trace file
  TCP::LogFlagsText(true);       // Log TCP flags in text mode
  IPV4::Instance()->SetTrace(Trace::ENABLED);  // Enable IP tracing all nodes
  //L2Proto802_3::GlobalSetTrace(Trace::ENABLED);

  // Create the nodes
  Node* c1 = new Node();    // Client node 1
  c1->SetLocation(1,0.5);
  Node* c2 = new Node();    // Client node 2
  c2->SetLocation(1,1);
  Node* s1 = new Node();    // Server node 1
  s1->SetLocation(1,1.5);
  Node* s2 = new Node();    // Server node 2
  s2->SetLocation(1,2);

  Node* ns = new Node();
  ns->IsSwitchNode(true);
  cout << "Node " << ns->Id() << "Is Switch = " << ns->IsSwitchNode() << endl;
  ns->SetLocation(0.5, 1.25);

  // For fun, change switch nodes to squares
  ns->Shape(Node::SQUARE);

  // Specify that all intefaces use the ARP protocol
  Interface::DefaultUseARP(true);
  // Create a link object template, 10Mb bandwidth, 5ms delay
  Linkp2p l(Rate("1Mb"), Time("10ms"));
  // Add the links to client and server leaf nodes
  c1->AddDuplexLink(ns, l, IPAddr("192.168.0.1")); // c1 to switch
  c2->AddDuplexLink(ns, l, IPAddr("192.168.0.2")); // c2 to switch
  s1->AddDuplexLink(ns, l, IPAddr("192.168.1.1")); // s1 to switch

  s2->AddDuplexLink(ns, l, IPAddr("192.168.1.2")); // s2 to r2

  // Create a link object template, 10Mb bandwidth, 100ms delay
  //Linkp2p r(Rate("10Mb"), Time("100ms"));
  // Add the router to router link
  //r1->AddDuplexLink(r2, r);

  // Create the TCP Servers
  TCPServer* server1 = (TCPServer*)s1->AddApplication(TCPServer(TCPTahoe()));
  if (server1)
    {
      server1->BindAndListen(80); // Application on s1, port 80
      server1->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
    }
  
   TCPServer* server2 = (TCPServer*)s2->AddApplication(TCPServer(TCPTahoe()));
   if (server2)
     {
       server2->BindAndListen(80); // Application on s2, port 80
       server2->SetTrace(Trace::ENABLED); // Trace TCP actions at server2
     }
  

  Uniform startRv(0.0, 2.0);
  // Create the TCP Sending Applications
  TCPSend* client1 = (TCPSend*)c1->AddApplication(
      TCPSend(s1->GetIPAddr(), 80,
	      Constant(10000),
              TCPTahoe()));
  if (client1)
    {
      // Enable TCP trace for this client
      client1->SetTrace(Trace::ENABLED);
      // Set random starting time
      client1->Start(0.1);
    }
  
   TCPSend* client2 = (TCPSend*)c2->AddApplication(
       TCPSend(s2->GetIPAddr(), 80,
               Constant(1000),
               TCPTahoe()));
   if (client2)
     {
       client2->SetTrace(Trace::ENABLED);
       client2->Start(0.6);
     }
  

 // Specify animation
  s.StartAnimation(0, true);
  s.Progress(1);
  s.StopAt(10);
  s.AnimationUpdateInterval(Time("2us")); // 2us initial update rate
  s.Run();


 
}

