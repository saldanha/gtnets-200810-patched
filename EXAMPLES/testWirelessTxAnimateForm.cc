// testWirelessTxiAnimateForm.cc
// Test wireless transmission animation in the form as Circles and Directed arrows

#if 1
#include "simulator.h"
#include "node.h"
#include "wlan.h"
#include "ratetimeparse.h"
#include "application-cbr.h"
#include "udp.h"
#include "routing-dsr.h"
#include "wireless-grid-rectangular.h"
#include "priqueue.h"
#include "trace.h"


#define ANIMATION_ON

#define XWIDTH		800
#define YWIDTH		800
#define NO_NODE		50

#define SIM_TIME	500

#define ROUTING_DSR	1


using namespace std;

int main(int argc, char** argv)
{
  
  int routingProto = ROUTING_DSR; 
  Seed_t seed = 1;
  Random::GlobalSeed(seed, seed, seed, seed, seed, seed);

  Simulator s;

  // Trace file
  Trace* gs = Trace::Instance();
  gs->Open("testadhoc.txt");
  gs->IPDotted(true);

  // Increase detail of L3 trace messages
  IPV4::Instance()->DetailOn(IPV4::TOTALLENGTH);
  IPV4::Instance()->SetTrace(Trace::ENABLED);

  // Routing protocol
  if (routingProto == ROUTING_DSR) {
    RoutingDSR::defaultSendBufPeriod = 0;
    Routing::SetRouting(new RoutingDSR);  // Set DSR to default routing
  }

  // Default setting
  Queue::Default(PriQueue());
  Queue::DefaultLimitPkts(50);

  WirelessLink wlink(NO_NODE, IPAddr("192.168.0.1"), MASK_ALL);
 

  Uniform urvx(0, XWIDTH), urvy(0, YWIDTH);
  WirelessGridRectangular g(Location(0,0), Constant(NO_NODE), urvx, urvy,
                            IPADDR_NONE, false);
  Node* n[NO_NODE];
  for (int i = 0; i < NO_NODE; i++) {
    n[i] = wlink.GetNode(i);
    n[i]->SetRadioRange(250);
    n[i]->SetLocation(urvx.Value(), urvy.Value());
    n[i]->Shape(Node::SQUARE);
  }

  Rate_t rate = (Rate_t)Rate("4Kb") ;

  // Node 0 is the CBR source and Node 1 is the receiver node
  Application* cbr;
  cbr = n[0]->AddApplication(
      CBRApplication(n[1]->GetIPAddr(), 1001, 1000, rate, 512, UDP()));
  cbr->Start(0);
  cbr->Stop(SIM_TIME);
  n[0]->SetTrace(Trace::ENABLED);
  n[1]->SetTrace(Trace::ENABLED);
  n[0]->Shape(Node::CIRCLE);
  n[1]->Shape(Node::CIRCLE);

  s.StopAt(SIM_TIME);

#ifdef ANIMATION_ON
  s.AnimateWirelessTx(true, DIRECTED_ARR);
  s.PauseOnWirelessTx(false);
  s.StartAnimation(0, true);
  s.AnimationUpdateInterval(Time("1ms"));
#endif
  s.Run();

  cout << "Simulation complete" << endl;
}
#endif
