// Read and process in-line documentation in GTNetS header Files.
// George F. Riley, Georgia Tech, Winter 2002

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "stringparse.h"
#include "stringtok.h"

using namespace std;

string pound("\\#");
string space("\\ ");
string leftleft("<{}<");
string leftarrow("<");
string bf("{\\bf ");
string endbf("}&");
string backback("\\\\");
string openbracket("\\{");
string closebracket("\\}");
string underscore("\\_");
string percent("\\%");
string andsym("\\&");
string backslash("$\\backslash$");
string dollar("\\$");
string letters("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
string whitespace("\011\012\013\014\015\040");

// Define a "string stream" class that allows a peek at next line
//Doc:ClassXRef
class sstream {
  //Doc:Class Class sstream is a stream class of stringsl.  It
  //Doc:Class supports "peeking" at the next available line
  //Doc:Class without removing the line from the buffer.
  //Doc:Class:Para
  //Doc:Class sstream supports the normal typecast to 
  //Doc:Class void* and the ! operator for end--of--file testing.
public:
  //Doc:Method
  sstream(ifstream& f);
  //Doc:Desc This is a test description of sstream constructor
  //Doc:Desc Second Line
  //Doc:Desc:Para
  //Doc:Desc Second para
  //Doc:Desc Second para line 2
  void getline(std::string& st);
  void peekline(std::string& st);
  void nextline();
  operator void*() const { return eof ? (void*)0 : (void*)(-1);}
  int operator!() const { return eof;}
  ifstream&   fs;      // The underlying fstream
  std::string s;       // Next string from the file for peek
  int         eof;     // If end of file
};

// sstream constructor
sstream::sstream(ifstream& f) : fs(f)
{
  if (!fs)
    { // End of file on entry, just set eof
      eof = true;
    }
  else
    { // Prime the peek string and not eof
      eof = false;
      std::getline(fs, s);
    }
}

// sstream get and peek line functions
void sstream::getline(std::string& st)
{
  st = s;  // Return the peeked line
  if (!fs)
    {
      eof = true;
    }
  else
    {
      std::getline(fs, s);
    }
}

void sstream::peekline(std::string& st)
{
  st = s;
}

void sstream::nextline()
{
  if (!fs)
    {
      eof = true;
    }
  else
    {
      std::getline(fs, s);
    }
}

class ParamInfo {
public:
  std::string name;
  std::string latexName;
  std::string paramType;
  std::string defValue;
  std::string doc;  // Documentation for parameter
};
typedef std::vector<ParamInfo>   ParamVec_t;
typedef std::vector<StringSep>   SSVec_t;
typedef std::vector<std::string> StringVec_t;

class MethodInfo {
public:
  MethodInfo() 
    : hasReturn(false), isVirtual(false),isConstructor(false),
      isStatic(false), isPure(false)
      { }
public:
  std::string name;
  std::string latexName;
  std::string returnType;
  std::string returnDoc;
  bool        hasReturn;
  bool        isVirtual;
  bool        isConstructor;
  bool        isStatic;
  bool        isPure;
  ParamVec_t  params;
  std::string doc;  // Overview documentation for method
};
typedef std::vector<MethodInfo> MethodVec_t;

class MemberInfo { // Documentation for member variables
public:
  MemberInfo() : isStatic(false) {};
  std::string  name;
  std::string  type;
  std::string  doc;
  bool         isStatic;
};
typedef std::vector<MemberInfo> MemberVec_t;

  
class ClassInfo {
public:
  ClassInfo() { };
  std::string name;
  std::string latexName;
  std::string doc;
  MethodVec_t constructors;
  MethodVec_t methods;
  MethodVec_t staticMethods;
  MemberVec_t members;
  MemberVec_t staticMembers;
};
typedef std::vector<ClassInfo> ClassVec_t;

// Global variables
ClassInfo currentClass;
  
// Helpers

string NameToLatex(const string& n)
{ // Convert a name to a latex compatible string
  string outln;
  for (string::const_iterator i = n.begin(); i != n.end(); ++i)
    { // process each character
      char ch = *i;
      switch(ch) {
//       case '#' :
//         outln.append(pound);
//         break;
//       case  ' ':
//         outln.append(space);
//         break;
//       case  '{':
//         outln.append(openbracket);
//         break;
//       case  '}':
//         outln.append(closebracket);
//         break;
      case  '_':
        outln.append(underscore);
        break;
      case  '%':
        outln.append(percent);
        break;
      case  '&':
        outln.append(andsym);
        break;
      case  '$':
        outln.append(dollar);
        break;
//       case  '\\':
//         outln.append(backslash);
//         break;
      case '<' :
        {
          string::const_iterator nexti = i + 1;
          char nch = (nexti == n.end() ? ' ' : *nexti);
          if (nch == '<') 
            {
              outln.append(leftleft);
              i++;
            }
          else
            {
              outln.append(leftarrow);
            }
        }
        break;
      default:
        {
          char work[2];
          work[0] = ch;
          work[1] = 0;
          outln.append(work);
        }
        break;
      }
    }
  return outln;
}

string MethodWithParameters(const MethodInfo& m)
{
  string r;
  if (m.returnType != "void")
    {
      r = NameToLatex(m.returnType) + " ";
    }
  
  r += m.latexName + "(";
  for (ParamVec_t::size_type i = 0; i < m.params.size(); ++i)
    {
      const ParamInfo& p = m.params[i];
      if (i) r += ", ";
      r += NameToLatex(p.paramType);
    }
  r += ")";
  return r;
}

void MethodToTex(ofstream& tex, const MethodInfo& m)
{
  tex << "\\begin{Method}{" << MethodWithParameters(m) << "}{"
      <<  NameToLatex(m.doc) << "}" << endl;
  
  if (m.returnDoc.length())
    {
      tex << "\\begin{Return}" << endl;
      tex << "\\item[]\\begin{tabularx}{\\linewidth}{lX}" << endl;
      tex << "\\ArgHdr\\\\" << endl;
      tex << "\\ArgTD{" << NameToLatex(m.returnType) << "}{" 
          << NameToLatex(m.returnDoc) << "}" << endl;
      tex << "\\end{tabularx}"<< endl;
      tex << "\\end{Return}" << endl;
    }
  
  if (m.params.size())
    { // Has parameters
      tex << "\\begin{Args}" << endl;
      tex << "\\item[]\\begin{tabularx}{\\linewidth}{lX}" << endl;
      tex << "\\ArgHdr\\\\" << endl;
      for (ParamVec_t::size_type i = 0; i < m.params.size(); ++i)
        {
          const ParamInfo& p = m.params[i];
          tex << "\\ArgTD{" << NameToLatex(p.paramType) << "}{" 
              << NameToLatex(p.doc);
          if (p.defValue.length())
            { // Default Value exists
              tex << " {\\bf Default Value} is " << NameToLatex(p.defValue);
            }
          tex << "}\\\\" << endl;
        }
      tex << "\\end{tabularx}"<< endl;
      tex << "\\end{Args}" << endl;
    }
  else
    {
      tex << "\\par";
    }
  tex << "\\end{Method}" << endl;
}

void ClassToTex()
{ // Write the .tex file for the current class
  //cout << "C2T" << endl;
  //cout << "ClassName " << currentClass.name << endl;
  //cout << "ClassDoc "  << currentClass.doc << endl;
  //cout << "Const Count " << currentClass.constructors.size() << endl;
  //cout << "Method Count " << currentClass.methods.size() << endl;
  // If no class documentation, then do not write the output file.
  if (currentClass.doc.length() == 0) return;

  std::string fname = currentClass.name + ".tex";
  ofstream tex(fname.c_str());
  if (!tex) return; // ? Can't create?
  tex << "\\begin{ClassHeader}{" << currentClass.latexName << "}{"
      << currentClass.doc << "}" << endl;
  if (currentClass.constructors.size())
    { // At least one constructor
      tex << "\\begin{ClassConstructors}" << endl;
      for (MethodVec_t::size_type i=0; i<currentClass.constructors.size(); ++i)
        {
          MethodInfo& m = currentClass.constructors[i];
          MethodToTex(tex, m);
        }
      tex << "\\end{ClassConstructors}" << endl;
    }
  
  if (currentClass.methods.size())
    { // At least one method
      tex << "\\begin{ClassMethods}" << endl;
      for (MethodVec_t::size_type i=0; i<currentClass.methods.size(); ++i)
        {
          MethodInfo& m = currentClass.methods[i];
          MethodToTex(tex, m);
        }
      tex << "\\end{ClassMethods}" << endl;
   }
  if (currentClass.staticMethods.size())
    { // At least one static method
      tex << "\\begin{ClassStaticMethods}" << endl;
      for (MethodVec_t::size_type i=0; i<currentClass.staticMethods.size(); ++i)
        {
          MethodInfo& m = currentClass.staticMethods[i];
          MethodToTex(tex, m);
        }
      tex << "\\end{ClassStaticMethods}" << endl;
   }
  if (currentClass.members.size())
    { // At least one method
      tex << "\\begin{samepage}" << endl;
      tex << "\\begin{ClassMembers}" << endl;
      tex << "\\item[]\\begin{tabularx}{\\linewidth}{llX}" << endl;
      tex << "\\ArgHdrName\\\\" << endl;
      for (MemberVec_t::size_type i=0; i<currentClass.members.size(); ++i)
        {
          MemberInfo& m = currentClass.members[i];
          tex << "\\ArgTDName{" << NameToLatex(m.type) << "}{" 
              << NameToLatex(m.name) << "}{" 
              << NameToLatex(m.doc) << "}" << "\\\\" << endl;
        }
      tex << "\\end{tabularx}"<< endl;
      tex << "\\end{ClassMembers}" << endl;
      tex << "\\end{samepage}" << endl;
   }
  if (currentClass.staticMembers.size())
    { // At least one static method
      tex << "\\begin{samepage}" << endl;
      tex << "\\begin{ClassStaticMembers}" << endl;
      tex << "\\item[]\\begin{tabularx}{\\linewidth}{llX}" << endl;
      tex << "\\ArgHdrName\\\\" << endl;
      for (MemberVec_t::size_type i=0; i<currentClass.staticMembers.size(); ++i)
        {
          MemberInfo& m = currentClass.staticMembers[i];
          tex << "\\ArgTDName{" << NameToLatex(m.type) << "}{" 
              << NameToLatex(m.name) << "}{" 
              << NameToLatex(m.doc) << "}" << "\\\\" << endl;
        }
      tex << "\\end{tabularx}"<< endl;
      tex << "\\end{ClassStaticMembers}" << endl;
      tex << "\\end{samepage}" << endl;
   }
  tex << "\\end{ClassHeader}" << endl;
  tex.close();
}


bool StartsWith(const string& s1, const char* s2)
{ // True if string s1 starts with s2
  std::string::const_iterator i = s1.begin();
  while(*i == ' ' && i != s1.end()) ++i; // Skip leading spaces
  while(*s2)
    {
      if (i == s1.end()) return false;   // Nope
      if (*i++ != *s2++) return false;   // Nope
    }
  return true; // Yes
}

string After(const string& s1, const char* s2)
{ // Return remainder of string s1 after s2
  std::string::size_type l = s1.find(s2);
  l += strlen(s2);
  return s1.substr(l, std::string::npos);
}

SSVec_t::iterator DefaultValue(SSVec_t v, SSVec_t::iterator i, string& s)
{
  s = i->str;
  if (i->sep == '(')
    {
      ++i;
      int openCount = 1;
      while (i != v.end())
        {
          s += i->str;
          if (i->sep == '(') openCount++;
          if (i->sep == ')') openCount--;
          ++i;
          if (openCount == 0) break;
        }
    }
  return i;
}

SSVec_t::iterator GetParam(SSVec_t v, SSVec_t::iterator i, ParamInfo& p)
{
  if (i->str == string("const"))
    {
      p.paramType = i->str + " ";
      ++i;
    }
  p.paramType += i->str;
  //cout << "Found parameter type " << p.paramType << endl;
  if (i->str == "unsigned" && i != v.end())
    { // check for unsigned long or unsigned int
      SSVec_t::iterator j = i + 1;
      if (j != v.end())
        {
          if (j->str == "long" || j->str == "int" ||
              j->str == "long&" || j->str == "int&")
            {
              p.paramType += " " + j->str;
              ++i;
            }
        }
    }
      
  if (i->sep == ',') return i; // End of parameter
  if (i->sep == ')') return i; // End of parameter
  if (i->sep == '=')
    { // No name, just default value
      return DefaultValue(v, ++i, p.defValue);
    }
  else
    { // Name must be next
      ++i;
      if (i == v.end()) return i; // HuH?
      p.name = i->str;
      p.latexName = NameToLatex(p.name);
      if (i->sep == '=') return DefaultValue(v, ++i, p.defValue);
    }
  return i;
}

bool ProcessMethod(SSVec_t& v, MethodInfo& m)
{
  int t = 0;
  if (v.size() < 3) return false;
  for (SSVec_t::iterator i = v.begin(); i != v.end(); ++i)
    {
      if (i->str == string("virtual"))
        {
          m.isVirtual = true;
          continue;
        }
      if (i->str == string("static"))
        {
          m.isStatic = true;
          continue;
        }
      if (t == 2 && i->str == "" && i->sep == ')') return true; // No params
      switch(t++) {
        case 0 :
          if (i->str == string("const"))
            {
              m.returnType = i->str + " ";
              m.hasReturn = true;
              --t;
              break;
            }
          if (i->str != currentClass.name &&
              i->str != string("~"+currentClass.name))
            { // Not constructor
              m.returnType += i->str;
              m.hasReturn = true;
              break;
            }
          t++; // Constructor or destructor, no type
          if (i->str.find_first_of('~') == string::npos)
            m.isConstructor = true;
          // Fall through if constructor or destructor
        case 1 :
          m.name = i->str;
          m.latexName = NameToLatex(m.name);
          if(0)cout << "Method name is " << m.name << endl;
          if (i->sep != '(')
            {
              cout << "HuH?  Expected ( after method name " << i->str
                   << " found " << i->sep << endl;
              exit(1);
            }
          break;
        default :
          {
            ParamInfo p;
            i = GetParam(v, i, p);
            m.params.push_back(p);
            if (i->sep == ')') return true;
          }
        }
    }
  return true;
}

bool ProcessMember(SSVec_t& v, MemberInfo& m)
{
  bool typeKnown = false;
  if (v.size() < 2) return false;
  for (SSVec_t::iterator i = v.begin(); i != v.end(); ++i)
    {
      if (i->str == string("static"))
        {
          m.isStatic = true;
          continue;
        }
      if (i->str == string("const"))
        {
          continue;
        }
      if (!typeKnown)
        {
          m.type = i->str;
          typeKnown = true;
        }
      else
        {
          m.name = i->str;
        }
    }
  return true;
}

bool MatchedParens(SSVec_t& v)
{
  int opencount = 0;
  for (SSVec_t::size_type i = 0; i < v.size(); ++i)
    {
      if (v[i].sep == '(') ++opencount;
      if (v[i].sep == ')') --opencount;
    }
  return (opencount == 0);
}

bool ListContains(SSVec_t& v, const string& c)
{
  for (SSVec_t::size_type i = 0; i < v.size(); ++i)
    {
      if (v[i].str.find(c) != string::npos) return true;
    }
  return false;
}

bool ParseMember(sstream& is, SSVec_t& v)
{
  is.nextline(); // Skip the Doc line
  char* sep = " =;";
  string s;
  is.getline(s);
  stringparse(v, s, sep);
  return true;
}

bool ParseMethod(sstream& is, SSVec_t& v)
{
  is.nextline(); // Skip the Doc line
  char* sep = " =,()";
  while(is)
    {
      string s;
      is.getline(s);
      stringparse(v, s, sep);
      if (MatchedParens(v))
        { // Got the entire declaration...
          if (ListContains(v, "}")) return true;
          if (ListContains(v, ";")) return true;
        }
    }
  return false; // End of file
}

bool ProcessClass(sstream& f)
{
  // Dump previous is exists
  if (currentClass.name.length())
    {
      ClassToTex();
      currentClass.name.clear();
      currentClass.latexName.clear();
      currentClass.doc.clear();
      currentClass.constructors.clear();
      currentClass.methods.clear();
      currentClass.staticMethods.clear();
      currentClass.members.clear();
      currentClass.staticMembers.clear();
    }
  
  StringVec_t tokens;
  std::string s;
  f.nextline();
  f.getline(s);
  stringtok(tokens, s, " ,\t\n");
  if (!tokens.size())
    {
      cout << "HuH? Empty line after XRef flag" << endl;
      return false;
    }
  if (tokens.size() < 3)
    {
      cout << "HuH? Too few tokens after XRef flag" << endl;
      return false;
    }
  if (tokens[0] != "class")
    {
      cout << "HuH? Missing class after XRef flag" << endl;
      cout << "Found " << tokens[0] << endl;
      return false;
    }
  currentClass.name = tokens[1];
  currentClass.latexName = NameToLatex(currentClass.name);
  
  return true;
}

void ProcessClassDoc(sstream& f)
{
  std::string s;
  while(f)
    {
      f.peekline(s);
      if (!StartsWith(s, "//Doc:Class")) return; // done
      f.nextline();
      if (StartsWith(s, "//Doc:Class "))
        {
          currentClass.doc += After(s, "//Doc:Class");
        }
      else if (StartsWith(s, "//Doc:Class:Para"))
        {
          currentClass.doc += string("\n\\par\n");
        }
      else
        {
          cout << "Unrecognized Doc:Class " << s << endl;
        }
    }
}

void ProcessMethodDoc(sstream& f, MethodInfo& m)
{
  while(f)
    {
      std::string s;
      f.peekline(s);
      if (!StartsWith(s, "//Doc:")) break; // Done with this one
      if (StartsWith(s, "//Doc:Method")) return; // Found next one
      if (StartsWith(s, "//Doc:Member")) return; // Found next one
      f.nextline();
      if (StartsWith(s, "//Doc:Desc "))
        {
          m.doc += After(s, "//Doc:Desc");
          continue;
        }
      else if (StartsWith(s, "//Doc:Return"))
        {
          m.returnDoc += After(s, "//Doc:Return");
        }
      else if (StartsWith(s, "//Doc:Arg"))
        { // Get the argument number
          string n = After(s, "//Doc:Arg");
          StringVec_t::size_type argn = 1;
          std::string::size_type l = n.find(' ');
          if (l != std::string::npos)
            {
              string a = n.substr(0, l);
              n = n.substr(1, string::npos);
              argn = atol(a.c_str());
            }
          if (argn > m.params.size())
            {
              cout << "Arg number " << argn << " out of range "
                   << m.params.size()
                   << " methodName " << m.name << endl;
              return;
            }
          //cout << "Processing arg " << argn << endl;
          //std::string::size_type k = s.find('/');
          m.params[argn-1].doc += n.substr(0, std::string::npos);
        }
      else if (StartsWith(s, "//Doc:Desc:Para"))
        {
          m.doc += string("\n\\par\n");
        }
      else
        {
          cout << "Unknown Doc: subcommand " << s << endl;
        }
    }
}

void ProcessMemberDoc(sstream& f, MemberInfo& m)
{
  while(f)
    {
      std::string s;
      f.peekline(s);
      if (!StartsWith(s, "//Doc:")) break; // Done with this one
      if (StartsWith(s, "//Doc:Member")) return; // Found next one
      if (StartsWith(s, "//Doc:Method")) return; // Found next one
      f.nextline();
      if (StartsWith(s, "//Doc:Desc "))
        {
          m.doc += After(s, "//Doc:Desc");
        }
      else
        {
          cout << "Unknown Doc: subcommand " << s << endl;
        }
    }
}

void ProcessOneFile(char* pFn)
{
  char* pName = pFn;
  char* pCh;
  // Find the last /
  while ((pCh = strchr(pName, '/')) != NULL) pName = &pCh[1];
  //cout << "Working on " << pFn << endl;
  ifstream f(pFn);
  if (!f)
    {
      cout << "HuH? Can't open " << pFn << endl;
      return;
    }
  sstream  sf(f);
  int lineNum = 0;
  while(sf)
    { // Look for the //Doc: flags
      string s;
      sf.peekline(s);
      //cout << "Working on " << s << endl;
      lineNum++;
      if (StartsWith(s, "//Doc:ClassXRef"))
        { // We need the class name to recognize constructors w/o return type
          ProcessClass(sf);
          cout << "Found classname " << currentClass.name << endl;
        }
      else if (StartsWith(s, "//Doc:Class"))
        {
          ProcessClassDoc(sf);
          //cout << "Class doc " << currentClass.doc << endl;
        }
      else if (StartsWith(s, "//Doc:Method"))
        { // Found Method with documentation
          if (!f) continue; // ? No following line?
          SSVec_t v;
          if(ParseMethod(sf, v))
            { // Ok, found the method prototype, process documentation
              //cout << "line is " << s << endl;
              MethodInfo m;
              if (!ProcessMethod(v, m))
                {
                  cout << "HuH?  Failed to process method" << endl;
                  for (SSVec_t::size_type i = 0; i < v.size(); ++i)
                    {
                      cout << "st    " << v[i].str 
                           << " sep " << v[i].sep << endl;
                    }
                  exit(1);
                }
              //cout << "found Doc:Method, name " << m.name
              //     << endl;
              ProcessMethodDoc(sf, m); // Get the documentation following
              if (m.isConstructor)
                {
                  currentClass.constructors.push_back(m);
                }
              else
                {
                  if (m.isStatic)
                    {
                      currentClass.staticMethods.push_back(m);
                    }
                  else
                    {
                      currentClass.methods.push_back(m);
                    }
                }
            }
        }
      else if (StartsWith(s, "//Doc:Member"))
        {
          if (!f) continue; // ? No following line?
          SSVec_t v;
          if(ParseMember(sf, v))
            { // Parsed ok
              MemberInfo m;
              if (!ProcessMember(v, m))
                {
                  cout << "HuH?  Failed to process method" << endl;
                  for (SSVec_t::size_type i = 0; i < v.size(); ++i)
                    {
                      cout << "st    " << v[i].str 
                           << " sep " << v[i].sep << endl;
                    }
                  exit(1);
                }
              ProcessMemberDoc(sf, m); // Get the documentation following
              if (m.isStatic)
                {
                  currentClass.staticMembers.push_back(m);
                }
              else
                {
                  currentClass.members.push_back(m);
                }
            }
        }
      else
        {
          sf.nextline(); // Skip the line
        }
    }
  // Dump previous is exists
  if (currentClass.name.length())
    {
      ClassToTex();
    }
}

int main(int argc, char** argv)
{
  if (argc < 2) exit(1);
  ProcessOneFile(argv[1]);
#ifdef OLD_WAY
  ifstream is(argv[1]);
  if (!is)
    {
      cout << "Can't open " << argv[1] << endl;
      exit(1);
    }
  SSVec_t v;
  while(ParseMethod(is, v))
    {
      MethodInfo m;
      if (ProcessMethod(v, m))
        {
          cout << "Method RType " << m.returnType
               << " name " << m.name 
               << " paramCnt " << m.params.size() << endl;
          cout << "MDoc " << m.doc << endl;
          for (ParamVec_t::size_type i = 0; i < m.params.size(); ++i)
            {
              cout << "Param " << i
                   << " type " << m.params[i].paramType
                   << " name " << m.params[i].name
                   << " defValue " << m.params[i].defValue << endl;
            }
        }
      v.clear();
    }
#endif
}

#ifdef VERBOSE_DEBUG
      cout << "v.size() " << v.size() << endl;
      for (vector<StringSep>::size_type i = 0; i < v.size(); ++i)
        {
          cout << "st " << v[i].str << " sep " << v[i].sep << endl;
        }
#endif
      
