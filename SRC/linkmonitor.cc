// Defines a class that monitors link performance by service class
// George F. Riley, Georgia Tech, Fall 2004

#include <iostream>

#include "packet.h"
#include "linkmonitor.h"

using namespace std;

LinkMonitor::LinkMonitor()
{ // Nothing needed
}

LinkMonitor::~LinkMonitor()
{ // Nothing needed
}

LinkMonitor* LinkMonitor::Copy() const
{
  return new LinkMonitor(*this);
}

void LinkMonitor::Transmit(Packet* p)
{
  // Get the packet priority
  Priority_t pr = p->Priority();
  // Insure large enough
  while(pr >= history.size()) history.push_back(0.0);
  history[pr] += p->Size() * 8.0; // Count the bits in this packet
}

void LinkMonitor::Log(std::ostream& os, Mult_t tot, const char* hdr, char sep)
{
  for (vector<double>::size_type i = 0; i < history.size(); ++i)
    {
      double bits = history[i];
      if (tot != 0.0) bits /= tot;
      os << i << sep << bits << endl;
    }
}


  
  
  


  
