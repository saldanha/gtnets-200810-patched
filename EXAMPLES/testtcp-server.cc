// Simple tester for TCP Server Application
// George F. Riley.  Georgia Tech, Summer 2002

#include <iostream>

#include "simulator.h"
#include "application-tcpserver.h"
#include "application-tcpreceive.h"
#include "duplexlink.h"
#include "queue.h"
#include "ratetimeparse.h"
#include "tcp-tahoe.h"
#include "validation.h"  

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  // Create the simulator object
  Simulator s;

  //Trace* tf = Trace::Instance();
  //tf->Open("testtcp-server.txt");
  //tf->IPDotted(true);         // Use dotted notation for IP Addresses
  //TCP::LogFlagsText(true);    // Log flags in text mode
  //IPV4::Instance()->SetTrace(Trace::ENABLED); // Trace IP layer

  // Set some defaults
  Queue::DefaultLength(200000);    // 200,000 bytes
  Link::DefaultRate(Rate("10Mb")); // 10 Mega-bits/sec

  // Create the nodes
  Node* n1 = new Node(IPAddr("192.168.0.1"));
  Node* n2 = new Node();
  Node* n3 = new Node();
  Node* n4 = new Node(IPAddr("192.168.1.1"));
  // No tracing on interior nodes
  n2->SetTrace(Trace::DISABLED);
  n3->SetTrace(Trace::DISABLED);
  // Create the links
  DuplexLink l12(n1, n2);
  DuplexLink l23(n2, n3);
  DuplexLink l34(n3, n4);

  // Restrict bw from 2 to 3 (bottleneck)
  l23.local->Bandwidth(Rate("1Mb"));
  l23.local->Delay(Time("100ms"));
  l23.remote->Bandwidth(Rate("1Mb"));
  l23.remote->Delay(Time("100ms"));

  // Restrict queue limit on bottleneck
  l23.localif->GetQueue()->SetLimit(10000);

  // Create the TCP server application
  TCPServer* server = (TCPServer*)n2->AddApplication(TCPServer(TCPTahoe()));
  server->Bind(80);
  server->CloseOnEmpty();         // Close tcp connection when all sent
  server->DeleteOnComplete();     // Delete application and tcp when done
  // Enable tracing on the server
  TCP* stcp = (TCP*)server->GetL4();
  stcp->SetTrace(Trace::ENABLED);

  // Create the TCP receiving application using TCPTahoe
  TCPReceive* pApp = (TCPReceive*)n1->AddApplication(
              TCPReceive(n4->GetIPAddr(),       // Peer IP Address
                         80,                    // Peer port number
                         Constant(100),         // Request size
                         Constant(100000)));    // Number of bytes to receive
  // Enable tracing at the client
  TCP* ctcp = (TCP*)pApp->GetL4();
  ctcp->DeleteOnComplete();
  ctcp->SetTrace(Trace::ENABLED);

  pApp->Start(0.1);
  s.StopAt(10.0);
  s.Run();
}

