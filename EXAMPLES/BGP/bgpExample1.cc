// Sample test program to instantiate a BGP router
// Sunitha Beeram, Georgia Tech, Spring 2005

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "bgp.h"

using namespace std;

int main(int argc,char ** argv)
{
  Simulator Sim;

  Node::DefaultShape(Node::CIRCLE);

  Trace* tr= Trace::Instance();
  tr->Open("bgpExample1.txt");

  Uniform startRng(0,0.1);

  Node* node = new Node();
  BGP* bgpRouter = new BGP(0);

  bgpRouter->AttachNode(node);
  bgpRouter->StartAt(0.1);

  bgpRouter->config_file("./bgpd1.conf");

  node->SetLocation(4,0);
  
  Sim.Progress(1);
  Sim.StopAt(400);
  cout << "Starting Simulation" << endl;
  Sim.Run();
  cout << "Simulation complete" << endl;
}
 
