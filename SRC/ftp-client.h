// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: ftp-client.h 185 2004-11-05 16:40:27Z riley $



// Georgia Tech Network Simulator - FTP Client Class
// George F. Riley.  Georgia Tech, Spring 2002

// Define the FTP Client Class

#ifndef __ftp_client_h__
#define __ftp_client_h__

#include "common-defs.h"
#include "rng.h"
#include "application.h"
#include "timer.h"
#include "tcp.h"
#include <vector>

//Doc:ClassXRef
class FTPAction {
public:
  typedef enum { NONE, OPEN, GET, PUT, CLOSE, SLEEP, REPEAT } Actions_t;
  FTPAction();
  FTPAction(Actions_t a);
  FTPAction(const FTPAction&);            // Copy constructor
  FTPAction& operator=(const FTPAction&); // Assignment operator
  ~FTPAction();
  void Reset();
public:
  Actions_t action;    // This ftp action
  IPAddr_t  peerIP;    // IP Address of server
  Count_t   reqSz;     // Size of request (if not random)
  Count_t   fileSz;    // Size of reply (if not random)
  Random*   reqRv;     // Random variable for request size
  Random*   fileRv;    // Random variable for fileonse size
  Time_t    sleepTime; // Sleep time
  Random*   sleepRv;   // Sleep time random variable
  Count_t   nRepeat;   // Number of repeats desired (0 if infinite)
  Count_t   cRepeat;   // Number of repeats so far
  Count_t   stepRepeat;// Step number to repeat
};

typedef std::vector<FTPAction> ActionVec_t;

//Doc:ClassXRef
class FTPClient : public Application, public TimerHandler  {
public:
  FTPClient(const TCP& = TCP::Default()); // Specify which TCP to use
  ~FTPClient();
  FTPClient* Copy() const { return new FTPClient(*this); }
  
  // TimerHandler 
  void Timeout(TimerEvent*);

  // Upcalls from L4 protocol
  void Receive(Packet*,L4Protocol*);    // Data received
  void Sent(Count_t, L4Protocol*);      // Data has been sent
  void Closed(L4Protocol*);             // Connection has closed
  void ConnectionComplete(L4Protocol*); // Connection request succeeded
  void ConnectionFailed(L4Protocol*,bool);// Connection request failed

  // All actions return step number, that can be used as a repeat target
  Count_t Open(IPAddr_t);          // Open session with peer
  Count_t Get(Count_t, Count_t);   // Get, size of req, size of file
  // Get, size of req/file (random)    
  Count_t Get(const Random&, const Random&);
  Count_t Put(Count_t, Count_t);   // Get, size of req, size of file
  // Get, size of req/rile (random)    
  Count_t Put(const Random&, const Random&);
  Count_t Close();                 // Close connection
  Count_t Sleep(Time_t);           // Sleep for specified time
  Count_t Sleep(Random*);          // Sleep for random time
  Count_t Repeat(Count_t,Count_t); // Repeat steps, starting at specified

public:
  virtual void StartApp();       // Called at time specified by Start
  virtual void StopApp();        // Called at time specified by Stop
private:
  typedef enum { CLOSED, CLOSING, OPENING, OPEN, 
                 SENDING_GET, GETTING, SENDING_PUT, PUTTING } State_t;
  typedef enum { SUCCESS, FAILED, FINISHED } ActionStatus_t;
  typedef enum { FTP_PORT = 21 } FTPPort_t;
  State_t     state;
  Count_t     currentStep;
  Count_t     getputSize;        // Size of get or put
  Count_t     getputCurrent;     // Number bytes tx/rx so far
  bool        started;           // True if application is active
  TCP*        l4Proto;           // TCP to use
  ActionVec_t actions;           // Pending actions
  FTPAction*  currentAction;     // Current action
  Timer       timer;             // Sleep timer
  TimerEvent* timeoutEvent;      // Pending timeout event
private:
  ActionStatus_t NextAction();   // Process next action
};

#endif


