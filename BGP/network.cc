/****************************************************************************/
/*  File:            network.cc                                           */
/*  Author:          Sunitha Beeram                                         */
/*  Email:           sunithab@ece.gatech.edu                                */
/*  Documentation:   TBD                                                    */
/*                                                                          */
/*  Version:         1.0beta                                                */
/*                                                                          */
/*  Copyright (c) 2005 Georgia Institute of Technology                      */
/*  This program is free software; you can redistribute it and/or           */
/*  modify it under the terms of the GNU General Public License             */
/*  as published by the Free Software Foundation; either version 2          */
/*  of the License, or (at your option) any later version.                  */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful,         */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*  GNU General Public License for more details.                            */
/*                                                                          */
/****************************************************************************/

#include "bgp.h"

/* Read nbytes from fd and store into ptr. */
int
BGP::readn (int fd, char *ptr, int nbytes)
{

#ifdef __NOT_USED__
  int nleft;
  int nread;

  Data *d;

  if ( !ReceivedMsg)
    return 0;

  nleft = nbytes;

  while (nleft > 0) 
    {
    d = ReceivedMsg->CopyFromOffset(nbytes,0);
    nread = d->Size();

    if (nread < 0) 
	  return (nread);
    else
	  if (nread == 0) 
	    break;

      nleft -= nread;
      ptr += nread;
    }
#endif /*__NOT_USED__*/
  return 0;
}  

/* Write nbytes from ptr to fd. */
int
BGP::writen(int fd, char *ptr, int nbytes)
{
  int nleft;
  int nwritten;

  nleft = nbytes;

  while (nleft > 0) 
    {
      nwritten = write(fd, ptr, nleft);
      
      if (nwritten <= 0) 
	return (nwritten);

      nleft -= nwritten;
      ptr += nwritten;
    }
  return nbytes - nleft;
}
