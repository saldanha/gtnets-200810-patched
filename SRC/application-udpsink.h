// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id:


// Georgia Tech Network Simulator - UDP Sink Application class
// George F. Riley.  Georgia Tech, Summer 2002

// Define a UDP Sink Application Class

#ifndef __udp_sync_h__
#define __udp_sync_h__

#include <iostream>
#include <map>

#include "common-defs.h"
#include "statistics.h"
#include "application.h"
#include "udp.h"

//Doc:ClassXRef
class UDPSink : public Application {
  //Doc:Class Class {\tt UDPSink} creates a simple model of a
  //Doc:Class request/response
  //Doc:Class {\tt TCP} server.  The server binds to a specified port, and
  //Doc:Class listens for connection requests from {\tt TCP} peers.  The 
  //Doc:Class data received from the peers specifies how much data to send
  //Doc:Class or receive.

private:
  typedef enum { LOG_STATS = 100 } AppUDPSinkEvent_t;
  
public:
  // Constructor
  //Doc:Method
  UDPSink(PortId_t = 0);
    //Doc:Desc Constructor for {\tt UDPSink} objects.
    //Doc:Desc Optionally specify a port number to bind to.
    //Doc:Arg1 Port number to bind.

  //Doc:Method
  UDPSink(const UDPSink& c);                   // Copy constructor
    //Doc:Desc Copy constructor.
    //Doc:Arg1 {\tt UDPSink} object to copy.

  //Doc:Method
  virtual ~UDPSink();
    //Doc:Desc Destructor for UDPSink

  // Upcalls from L4 protocol
  virtual void Receive(Packet*,L4Protocol*,Seq_t);   // Data received

  // Inherited from Application
  //Doc:Method
  virtual void StartApp();        // Called at time specified by Start
    //Doc:Desc Called at the specified applcation start time.

  //Doc:Method
  virtual void StopApp();         // Called at time specified by Stop
    //Doc:Desc Called at the specified applcation stop time.

  //Doc:Method
  virtual void AttachNode(Node*); // Note which node attached to
    //Doc:Desc Specify which node to which this application is attached.
    //Doc:Arg1 Node pointer to attached node.

  // Handler functions
  void Handle(Event*, Time_t);

  // Specific to UDPSink app
  //Doc:Method
  virtual void Bind(Node*, PortId_t);      // Bind to node/port
    //Doc:Desc Bind the application to the specified port and start 
    //Doc:Desc listening for connections.
    //Doc:Arg1 Node pointer to the node this application is associated with.
    //Doc:Arg2 Port number to bind to.

  //Doc:Method
  virtual void Bind(PortId_t);             // Bind to node/port
    //Doc:Desc Bind the application to the specified port and start 
    //Doc:Desc listening for connections.  Node is already known.
    //Doc:Arg1 Port number to bind to.

  virtual Application* Copy() const;           // Make a copy of application

  //Doc:Method
  Statistics* SetStatistics(const Statistics&);
    //Doc:Desc Set a statistics object to use for statistics collection.
    //Doc:Desc This application will record the packet loss rate
    //Doc:Desc periodically (see StatsUpdateInterval()) below.
    //Doc:Arg1 Reference to a statistics object.
    //Doc:Return A pointer to the new statistics object.

  //Doc:Method
  void StatsUpdateInterval(Time_t r) { statsUpdateInterval = r; }
    //Doc:Desc Set the statistics update interval.
    //Doc:Arg1     Interval(seconds) for stats updates call

  typedef enum { LOSS, BANDWIDTH } StatsType_t;

  //Doc:Method
  void StatsLogType(StatsType_t t) { statsLogType = t;}
    //Doc:Desc Specify if stats logging is loss rate or bandwidth.
    //Doc:Arg1 Stats logging type.  Either LOSS or BANDWIDTH

  //Doc:Method
  Statistics* DelayStatistics(const Statistics&);
    //Doc:Desc Enable delay statistics.
    //Doc:Arg1 Statistics object to use.
    //Doc:Return Pointer to new statistics object

  //Doc:Method
  Rate_t RxRate();
    //Doc:Desc Return the total bytes/second received since
    //Doc:Desc first packet rx.
    //Doc:Arg1   Bytes per second of data reception, since first packet.

  //Doc:Method
  void TrackInterArrival(Time_t);
    //Doc:Desc Specify that inter-arrival time is to be tracked for this
    //Doc:Desc connection
    //Doc:Arg1 Size of buckets in the histogram for IAT.  For easmple,
    //Doc:Arg1 if jitter is to be tracked in units of 1ms, specify
    //Doc:Arg1 Time("1ms") for this.

  //Doc:Method
  void LogInterArrival(std::ostream&, const char* = nil, char = ' ');
    //Doc:Desc Log the jitter information to a file
    //Doc:Arg1 Output stream to log onto.
    //Doc:Arg2 Optional header line
    //Doc:Arg3 Optional separator character

private:
  void LogStats();
public:
  //Doc:Member
  UDP*           l4UDP;
    //Doc:Desc Count of the total number of bytes received

  //Doc:Member
  Node*          node;
    //Doc:Desc Attached node.

  //Doc:Member
  Event*         logEvent;
    //Doc:Desc Event used to log the statistics periodically

  //Doc:Member
  Statistics*    stats;
    //Doc:Desc Statistics collection object

  //Doc:Member
  Statistics*    delayStats;
    //Doc:Desc Statistics collection object

  //Doc:Member
  Time_t         statsUpdateInterval;
    //Doc:Desc Statistics collection interval

  //Doc:Member
  Time_t         firstPacketRx;
    //Doc:Desc Time first packet is received.  Used to calculate
    //Doc:Desc overall bytes/sec data rx.

  //Doc:Member
  Time_t         lastPacketRx;
    //Doc:Desc Time last packet was received.  Used to calculate
    //Doc:Desc jitter.

  //Doc:Member
  PortId_t       port;
    //Doc:Desc Port number to bind to.

  //Doc:Member
  Count_t        bytesRx;
    //Doc:Desc Count of the total number of bytes received

  //Doc:Member
  Count_t        packetsRx;
    //Doc:Desc Count of the total number of packets received

  //Doc:Member
  Count_t        packetsLost;
    //Doc:Desc Count of the total number of packets lost
  
  //Doc:Member
  Seq_t          nextSeqRx;
    //Doc:Desc Sequence number of next expected packet

  //Doc:Member
  StatsType_t    statsLogType;
    //Doc:Desc Type of stats logging

  Time_t         lastLogTime;
  Count_t        lastPacketsRx;
  Count_t        lastBytesRx;
  Count_t        lastPacketsLost;
  Time_t         iatUnits;
  typedef std::map<Count_t, Count_t> IATHisto_t;
  IATHisto_t*    iatHist;  // Indexed by bin number, contains count of rx'es
  Time_t         lastTransit;
  Mult_t         jitter;
};

#endif

