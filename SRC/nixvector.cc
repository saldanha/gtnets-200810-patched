// GENERAL PUBLIC LICENSE AGREEMENT
//
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
//
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
//
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
//
// 1.  This License allows you to:
//
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
//
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//
//
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
//
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
//
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
//
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
//
//
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
//
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
//
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
//
//
// 3.  Export Law Assurance.
//
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//
// 4.  Termination.
//
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
//
//
// 5.  Disclaimer of Warranties and Limitation on Liability.
//
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
//
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
//
// GTRC shall have no obligation for support or maintenance of Program.
//
// 6.  Copyright Notice.
//
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
//
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
//
// Said copyright notice shall read as follows:
//
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//

#include <stdio.h>
#include <math.h>
#include <cstring>

#include "nixvector.h"
#include "simulator.h"

using namespace std;

// Default setting
Word_t CNCList::defaultSize = NC_LIST_SIZE;
Word_t CReplyList::defaultSize = REPLY_LIST_SIZE;
Word_t CNbrTable::defaultMaxNeighbor = MAX_NEIGHBOR;
Byte_t CNbrTable::defaultRetryThresh = 0;
Word_t CNVFIB::defaultSize = 50;
Time_t CNVFIB::defaultTimeout1 = 10.0;
Time_t CNVFIB::defaultTimeout2 = 10.0;
Count_t CNVFIB::count[2];

// bitcopy global function
int bitcpy(Long_t* dst, int dp, Long_t* src, int sp, int len)
{
  if (!len) return 0;

  int ofs_d, ofs_s, p_d, p_s;
  ofs_d = dp / 32;  p_d = dp % 32;
  ofs_s = sp / 32;  p_s = sp % 32;

  int size = bit2dw(len)+1;
  DWn mask(size), s(size);
  for(int i=0; i<size-1; i++) mask.dw[i] = (Long_t)-1;
  mask.dw[size-2] >>= (32-len%32);

  memcpy(s.dw, src+ofs_s, bit2dw(p_s+len)*sizeof(Long_t));
  s >>= p_s;
  s &= mask;
  mask <<= p_d;
  s <<= p_d;

  for (int i=0; i<bit2dw(p_d+len); i++) {
    dst[i+ofs_d] &= ~mask.dw[i];
    dst[i+ofs_d] |= s.dw[i];
  }
  return len;
}


// DWn

DWn::DWn(int sz) : size(sz)
{
  dw = new Long_t[sz];
  memset(dw, 0, sz*sizeof(Long_t));
}

DWn& DWn::operator = (DWn& r)
{
  size = r.size;
  if (dw) delete[] dw;
  dw = new Long_t[size];
  memcpy(dw, r.dw, size*sizeof(Long_t));
  return *this;
}

DWn& DWn::operator <<= (int n)
{
  if (!n) return *this;
  if (n >= 32) {
    cout << "shift bits error n = " << n << endl;
    return *this;
  }

  Long_t carry = 0, temp;
  for (int i=0; i<size; i++)
    {
      temp = dw[i];
      dw[i] = carry | (temp << n);
      carry = temp >> (32-n);
    }
  return *this;
}

DWn& DWn::operator >>= (int n)
{
  if (!n) return *this;
  if (n >= 32) {
    cout << "shift bits error n = " << n << endl;
    return *this;
  }

  Long_t carry = 0, temp;
  for (int i=size-1; i>=0; i--)
    {
      temp = dw[i];
      dw[i] = carry | (dw[i] >> n);
      carry = temp << (32-n);
    }
  return *this;
}

DWn& DWn::operator |= (DWn& r)
{
  // Assume the same size
  for (int i=0; i<size; i++) dw[i] |= r.dw[i];
  return *this;
}

DWn& DWn::operator &= (DWn& r)
{
  // Assume the same size
  for (int i=0; i<size; i++) dw[i] &= r.dw[i];
  return *this;
}


// CNCList

void CNCList::Add(Byte_t id, IPAddr_t src, IPAddr_t dst, MACAddr_t inf)
{
  Time_t now = Simulator::Now();
  if (Item.size() >= defaultSize)  // full
    Item.pop_front();
  Item.push_back(CNCItem(id, src, dst, now, inf));

  //cout << "NCList size " << Item.size() << endl;
  //cout << "ncid " << id << " src " << (string)IPAddr(src)
  //<< " dst " << (string)IPAddr(dst) << " time " << now << endl;
}

int CNCList::Find(Byte_t id, IPAddr_t src, IPAddr_t dst)
{
  for (NCVec_t::size_type i = 0; i < Item.size(); ++i) {
    //if (Item[i].id==id && Item[i].src==src && Item[i].dst==dst) {
    if (Item[i].id>=id && Item[i].src==src) {
      //cout << "id " << id << " src " << (string)IPAddr(src)
      //<< " dst " << (string)IPAddr(dst)
      //<< " time " << Item[i].time << " now " << Simulator::Now()
      //<< " diff " << Simulator::Now()-Item[i].time << endl;
      return i;
    }
  }
  return -1;
}

// CReplyList

void CReplyList::Add(IPAddr_t s, Byte_t id, Byte_t t, Byte_t m, CMetricVec& mv)
{
  if (Item.size() >= defaultSize) // full
    Item.pop_front();
  Item.push_back(CRepItem(t, id, m, mv, s, Simulator::Now()));
}

CRepItem* CReplyList::Find(IPAddr_t src, Byte_t id, Byte_t t)
{
  for (RepVec_t::iterator i = Item.begin(); i != Item.end(); ++i)
    if (i->type==t && i->id==id && i->src==src) return &(*i);
  return nil;
}

void CReplyList::Update(int i, CMetricVec& mv)
{
  Item[i].mv = mv;
  Item[i].t = Simulator::Now();
}


// CNixVector

CNixVector::CNixVector(const CNixVector& r) :
    nvl(r.nvl), nvu(r.nvu)
{
  if (r.nv) {
    int dwAlloc = bit2dw(r.nvl);
    nv = new Long_t[dwAlloc];
    memcpy(nv, r.nv, dwAlloc*sizeof(Long_t));
  }
  else
    nv = 0;
}

CNixVector::CNixVector(Word_t l, Word_t u, Long_t* v) :
    nvl(l), nvu(u)
{
  if (v) {
    int dwAlloc = bit2dw(l);
    nv = new Long_t[dwAlloc];
    memcpy(nv, v, dwAlloc*sizeof(Long_t));
  }
  else
    nv = 0;
}

CNixVector::~CNixVector()
{
  if (nv) delete[] nv;
}

void CNixVector::ExpandBits(int len)
{
  if (len <= 0) {
    //cout << "CNixVector::ExpandBits() error len = " << len << endl;
    return;
  }
  int free_bits = bit2dw(nvl)*32 - nvl;
  if (free_bits >= len) return;    // enough space

  int dwAlloc = bit2dw(nvl+len);
  Long_t* old = nv;
  nv = new Long_t[dwAlloc];
  memset(nv, 0, dwAlloc*sizeof(Long_t));
  if (old) {
    memcpy(nv, old, bit2dw(nvl)*sizeof(Long_t));
    delete[] old;
  }
}

void CNixVector::Set(MACAddr_t mac)
{
  int len = NO_COLOR_BITS + NO_MAC_BITS;
  ExpandBits(len);

  int dp = nvl + NO_COLOR_BITS;

  Long_t s[2];
  s[0] = mac;
  s[1] = 0;
  bitcpy(nv, dp, s, 0, NO_MAC_BITS);
  nvl += len;
}

void CNixVector::Set(int nix)
{
  nix++;    // nix starts from 1 to account for hidden bit

  /* results in log2(8) = 2, not 3 in Linux
     Long_t b = (Long_t)(log10(nix)/log10(2));
  */
  double d = log10((double)nix)/log10(2.0);
  Long_t b = (Long_t)d;

  int len = NO_COLOR_BITS + b;
  ExpandBits(len);

  Long_t dwNix = nix;
  dwNix = (dwNix << NO_COLOR_BITS) | (0xf & (b+1));
  bitcpy(nv, nvl, &dwNix, 0, len);
  nvl += len;
};

int CNixVector::Get(MACAddr& aix)
{
  Long_t dwNix = 0;
  Byte_t color = PeekColor();

  if (color != WHITE_COLOR) {
    // Other color, contains nix
    color--;
    bitcpy(&dwNix, 0, nv, nvu+NO_COLOR_BITS, color);
    nvu += NO_COLOR_BITS + color;
    Long_t hidden = 1 << color;
    dwNix += hidden;
    aix.macAddr = dwNix-1;
    //aix.Upper(0);

    //cout << "{\n";
    //cout << "color " << color << " memory nix " << dwNix-1 << endl;
    //cout << "}\n";

    return 1;
  }

  // White color, contains MAC address
  Long_t mac[2];
  mac[0] = mac[1] = 0;
  bitcpy(mac, 0, nv, nvu+NO_COLOR_BITS, NO_MAC_BITS);
  nvu += NO_COLOR_BITS + NO_MAC_BITS;
  aix.macAddr = mac[0];
  aix.macAddr = 0;
  return 0;
}

bool CNixVector::Advance(int n)
{
  // This function assumes symmetric routing
  if (nvu) {
    //cout << "HuH? nvu is not zero nvu " << nvu << endl;
    return false;
  }
  while (n > 0) {
    if (nvl < nvu + NO_COLOR_BITS) {
      //cout << "HuH? nvl < nvu + NO_COLOR_BITS"
      //<< " nvl " << nvl << " nvu " << nvu << " n " << n << endl;
      return false;
    }
    Byte_t color = PeekColor();
    if (color == WHITE_COLOR) {
      // This should NOT happen!!!
      //cout << "HuH? color " << color << endl;
      return false;
    }
    nvu += NO_COLOR_BITS + color - 1;
    n--;
  }
  return true;
}

Byte_t CNixVector::PeekColor()
{
  Long_t mask, color;
  int ofs = nvu / 32;
  int bp =  nvu % 32;

  if (bp <= 32-NO_COLOR_BITS) {
    mask = 0xf;
    color = (nv[ofs] >> bp) & mask;
  }
  else {
    long m = 0x80000000;
    mask = (m >> (31-bp));
    color = (nv[ofs] & mask) >> bp;
    mask = (Long_t)-1;
    mask = ~(mask << (bp+NO_COLOR_BITS-32));
    color |= (nv[ofs+1] & mask) << (32-bp);
  }
  return (Byte_t)color;
}

void CNixVector::Dump()
{
  int len;
  int gp1, gp2;
  Long_t color=0;
  int dwAlloc = bit2dw(nvl);

  Long_t* out = new Long_t[dwAlloc];
  memset(out, 0, sizeof(Long_t)*dwAlloc);

  gp2 = sizeof(Long_t)*dwAlloc*8;
  for (gp1 = 0; gp1 < nvl; gp1 += len) {
    len = NO_COLOR_BITS;
    gp2 -= len;
    bitcpy(&color, 0, nv, gp1, len);
    gp1 += len;
    bitcpy(out, gp2, &color, 0, len);
    len = color==WHITE_COLOR ? NO_MAC_BITS : color-1;
    gp2 -= len;
    bitcpy(out, gp2, nv, gp1, len);
  }

  Word_t* wout = (Word_t*)out;
  for (int i=dwAlloc*2-1; i>=0; i--)
    printf("%04x ", wout[i]);
  cout << endl;
  delete[] out;
}

CNixVector& CNixVector::operator = (const CNixVector& r)
{
  if (r.nv) {
    int dwAlloc = bit2dw(r.nvl);
    //cout << "dwAlloc = " << dwAlloc << endl;
    if (dwAlloc != bit2dw(nvl)) {
      if (nv) delete[] nv;
      nv = new Long_t[dwAlloc];
    }
    memcpy(nv, r.nv, dwAlloc*sizeof(Long_t));
  }
  else {
    if (nv) delete[] nv;
    nv = 0;
  }
  nvl = r.nvl;
  nvu = r.nvu;
  return *this;
}


// CNVCache

int CNVCache::Find(const RIVec_t& item, const PathId& pid, bool force)
{
  // search for the matching item, and return the found index or -1 else
  for (IntVec_t::iterator i = cache.begin(); i != cache.end(); ++i) {
    if (!item[*i]) continue;
    if (!force && !item[*i]->Valid()) continue;
    if (item[*i]->pid == pid) return *i;
  }
  return -1;
}

void CNVCache::Add(int i)
{
  if (cache.size() == 20) cache.pop_front();
  cache.push_back(i);
}


// CNVFIB

CNVFIB::CNVFIB()
{
  Item.reserve(10);
}

CNVFIB::~CNVFIB()
{
  for(RIVec_t::iterator i=Item.begin(); i!=Item.end(); ++i)
    if (*i) delete *i;
}

#define SINGLE_PATH

void CNVFIB::Add(const PathId& pid, Byte_t mid, const CMetricVec& mv,
                 const CNixVector& nv, Byte_t attr, bool val)
{
#ifdef SINGLE_PATH
  // Check to see if there is already newer one
  for (RIVec_t::iterator i = Item.begin(); i != Item.end(); ++i) {
    if (!(*i) || !(*i)->Valid()) continue;
    if ((*i)->pid > pid) {
      //cout << "HuH? new " << pid.dn << " old " << (*i)->pid.dn << endl;
      return;
    }
  }

  // Invalidate older NV
  for (RIVec_t::size_type i = 0; i < Item.size(); ++i) {
    if (!Item[i] || !Item[i]->Valid()) continue;
    if (Item[i]->pid < pid) {
      Invalidate(i);
      //break;
    }
  }
#endif

  Time_t now = Simulator::Now();
  CRouteInfo* ri = new CRouteInfo();
  ri->val = val;
  ri->pid = pid;
  ri->mid = mid;
  ri->mv = mv;
  ri->nv = nv;
  ri->attr = attr;
  ri->time = now;

  // Find an available slot
  RIVec_t::size_type j;
  for (j = 0; j < Item.size(); ++j) if (!Item[j]) break;
  if (j < Item.size())
    Item[j] = ri;
  else if (Item.size() < defaultSize)  // expand
    Item.push_back(ri);
  else {
    // Find the oldest one among invalid
    Time_t oldest=now, old2=now;
    RIVec_t::size_type k;
    for (RIVec_t::size_type i = 0; i < Item.size(); ++i)
      if (!Item[i]->Valid()) {
        if (Item[i]->time < oldest) {
          oldest = Item[i]->time;
          j = i;
        }
      }
      else {
        if (Item[i]->time < old2) {
          old2 = Item[i]->time;
          k = i;
        }
      }
    if (j == Item.size()) j = k;
    //cout << "NVFIB size " << Item.size()
    //<< " now " << now << " oldest " << Item[j]->time
    //<< " val " << Item[j]->val << endl;
    timer.Cancel(&Item[j]->ev);
    delete Item[j];
    Item[j] = ri;
  }
  ri->ev.h = j;
  Time_t to = val ? defaultTimeout1 : defaultTimeout2;
  timer.Schedule(&ri->ev, to, this);
}

void CNVFIB::Invalidate(int i)
{
  // invalidate by index
  if (i < 0 || !Item[i]) return;
  //cout << "nv invalidated time " << Item[i]->time
  //<< " now " << Simulator::Now()
  //<< " diff " << Simulator::Now()-Item[i]->time << endl;
  Item[i]->val = false;
  Item[i]->time = Simulator::Now();
  timer.Cancel(&Item[i]->ev);
  timer.Schedule(&Item[i]->ev, defaultTimeout2, this);
}

int CNVFIB::Remove(int i)
{
  // remove by index
  if (i < 0 || !Item[i]) return -1;
  timer.Cancel(&Item[i]->ev);
  delete Item[i];
  Item[i] = nil;
  //nItem--;
  return i;
}

int CNVFIB::Remove(IPAddr_t dst)
{
  // remove all the matching items, and return the # of deleted
  int num_deleted = 0;
  for (RIVec_t::size_type i = 0; i < Item.size(); ++i) {
    if (!Item[i]) continue;
    if (Item[i]->pid.dst == dst) {
      delete Item[i];
      Item[i] = nil;
      //nItem--;
      num_deleted++;
    }
  }
  return num_deleted;
}

int CNVFIB::Remove(Byte_t mid, IPAddr_t dst)
{
  // remove the matching item, and return the deleted index or -1 else
  for (RIVec_t::size_type i = 0; i < Item.size(); ++i) {
    if( !Item[i] ) continue;
    if (Item[i]->mid==mid && Item[i]->pid.dst==dst) {
      delete Item[i];
      Item[i] = nil;
      //nItem--;
      return i;
    }
  }
  return -1;
}

int CNVFIB::Find(IPAddr_t dst, bool force)
{
  // search by dst, and return the found index or -1 else
  Byte_t pnum = 0;
  int j = -1;
  for (RIVec_t::size_type i = 0; i < Item.size(); ++i) {
    if (!Item[i]) continue;
    if (!force && !Item[i]->Valid()) continue;
    if (Item[i]->pid.dst == dst)
      if (Item[i]->pid.dn > pnum) {
        j = i;
        pnum = Item[i]->pid.dn;
      }
  }
  return j;
}

int CNVFIB::Find(const PathId& pid, bool force)
{
  // search for the matching item, and return the found index or -1 else

  // search the small cache first
  int j = nvCache.Find(Item, pid, force);
  if (j >= 0) {
    count[0]++;
    return j;
  }

  // search the NV-FIB
  for (RIVec_t::size_type i = 0; i < Item.size(); ++i) {
    if (!Item[i]) continue;
    if (!force && !Item[i]->Valid()) continue;
    if (Item[i]->pid == pid) {  // found
      nvCache.Add(i);
      count[1]++;
      return i;
    }
  }
  return -1;
}

int CNVFIB::FindByDst(IPAddr_t dst, Word_t dn, bool force)
{
  // search by (dst,pnum), and return the found index or -1 else
  for (RIVec_t::size_type i = 0; i < Item.size(); ++i) {
    if (!Item[i]) continue;
    if (!force && !Item[i]->Valid()) continue;
    if (Item[i]->pid.dst == dst && Item[i]->pid.dn == dn)
      return i;
  }
  return -1;
}

int CNVFIB::Search(Byte_t mid, IPAddr_t dst)
{
  // search for the matching item, and return the found index or -1 else
  for (RIVec_t::size_type i = 0; i < Item.size(); ++i) {
    if (!Item[i] || !Item[i]->Valid()) continue;
    if (Item[i]->mid == mid && Item[i]->pid.dst == dst) return i;
  }
  return -1;
}

void CNVFIB::Update(int i, Byte_t hop, CNixVector& nv)
{
  // update path info
  if (!Item[i]) return;
  Item[i]->val = true;
  Item[i]->mv.hop = hop;
  Item[i]->nv = nv;
  Item[i]->time = Simulator::Now();
}

void CNVFIB::Update(int i, IPAddr_t dst, Byte_t hop, CNixVector& nv)
{
  // update path info
  if (!Item[i]) return;
  Item[i]->val = true;
  Item[i]->pid.dst = dst;
  Item[i]->mv.hop = hop;
  Item[i]->nv = nv;
  Item[i]->time = Simulator::Now();
}

void CNVFIB::Timeout(TimerEvent* ev)
{
  //cout << "timeout: ";
  int i = ((MyEvent*)ev)->h;
  if (Item[i]->Valid())
    Invalidate(i);
  else {
    //cout << "nv deleted time " << Item[i]->time << " now " << Simulator::Now()
    //<< " diff " << Simulator::Now()-Item[i]->time << endl;
    delete Item[i];
    Item[i] = nil;
  }
}

// CNbrTable

CNbrTable::CNbrTable() : nNbr(0)
{
  Nbr.reserve(10);
}

int CNbrTable::Add(MACAddr_t mac)
{
  // This function is called if Find() returns -1

  if (nNbr >= defaultMaxNeighbor) return -1;

  // Find an empty slot
  NbrVec_t::size_type i;
  for (i = 0; i < Nbr.size(); ++i)
    if (Nbr[i].Empty()) break;
  if (i == Nbr.size())  // no empty slot
    Nbr.push_back(CNeighbor(mac, true));  // expand
  else
    Nbr[i] = CNeighbor(mac, true);
  nNbr++;
  return i;
}

bool CNbrTable::InvalidateIndex(int i)
{
  Nbr[i].cnt++;
  if (Nbr[i].cnt > defaultRetryThresh) {
    Nbr[i].val = false;
    Nbr[i].cnt = 0;
    return true;  // invalidated
  }
  return false;
}

bool CNbrTable::Invalidate(MACAddr_t mac)
{
  int i = Find(mac);
  if (i >= 0) return Invalidate(i);
  return false;
}

int CNbrTable::Find(MACAddr_t mac)
{
  for (NbrVec_t::size_type i = 0; i < Nbr.size(); ++i)
    if (Nbr[i].mac == mac) return i;
  return -1;
}

Size_t CNbrTable::Size() const
{
  Size_t sz;
  sz = Nbr.size()*sizeof(CNeighbor);
  return sz;
}

// BlackList
void BlackList::Add(IPAddr_t ip)
{
  if (list.size() >= 5) list.pop_front();
  list.push_back(BLEntry(ip, Simulator::Now()));
}

bool BlackList::Find(IPAddr_t ip)
{
  Time_t now = Simulator::Now();
  for (BlackList_t::iterator i=list.begin(); i!=list.end(); ++i)
    {
      if (i->ip == ip) {
        if (now - i->t <= 10.0) return true;
        else { list.erase(i); return false; }
      }
    }
  return false;
}
