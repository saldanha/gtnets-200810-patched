// GENERAL PUBLIC LICENSE AGREEMENT
//
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
//
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
//
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
//
// 1.  This License allows you to:
//
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
//
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//
//
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
//
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
//
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
//
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
//
//
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
//
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
//
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
//
//
// 3.  Export Law Assurance.
//
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//
// 4.  Termination.
//
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
//
//
// 5.  Disclaimer of Warranties and Limitation on Liability.
//
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
//
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
//
// GTRC shall have no obligation for support or maintenance of Program.
//
// 6.  Copyright Notice.
//
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
//
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
//
// Said copyright notice shall read as follows:
//
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: queue.cc 454 2005-12-16 15:58:15Z riley $



// Georgia Tech Network Simulator - Queue base class
// George F. Riley.  Georgia Tech, Spring 2002

// Virtual Base class for all queue types.
// Provides some common functionality

#include "queue.h"
#include "droptail.h"
#include "simulator.h"
#include "link.h"
#include "interface.h"

#ifdef HAVE_QT
#include "qtwindow.h"
#include <qpoint.h>
#include <qcanvas.h>
#endif

using namespace std;

// Static members
Queue*  Queue::defaultQueue  = nil;
Count_t Queue::defaultLength = DEFAULT_DROPTAIL_LIMIT;
Count_t Queue::defaultLimitPkts = 0;  // Default is no packet limit
Count_t Queue::defaultAnimWidth = 10; // Width (pixels) of animated queues
Count_t Queue::globalQueueDrop = 0;   // Total packets dropped due to q-limit

void Queue::DummyEnque(Packet*)
{ // Over-ridden by subclasses as needed
}

Count_t Queue::DequeAllDstMac(MACAddr)
{ // Over-ridden by subclasses as needed
  return 0;
}

Count_t Queue::DequeAllDstIP(IPAddr_t)
{ // Over-ridden by subclasses as needed
  return 0;
}

Packet* Queue::DequeOneDstMac(MACAddr)
{ // Over-ridden by subclasses as needed
  return nil;
}

Packet* Queue::DequeOneDstIP(IPAddr_t)
{ // Over-ridden by subclasses as needed
  return nil;
}

DCount_t Queue::Average()
{ // Get average queue length
  Time_t now = Simulator::Now();
  if (now == startTime) return 0.0;    // Avoid divide by 0
  return totalByteSeconds / (now - startTime);
}

void     Queue::ResetAverage()
{ // Start a new averaging interval
  startTime = Simulator::Now();
  lastUpdate = startTime;
  totalByteSeconds = 0;
}

void     Queue::UpdateAverage()
{ // Queue is changing, update average
  Time_t now = Simulator::Now();
  totalByteSeconds += (now - lastUpdate) * Length();
  lastUpdate = now;
  // Also log the Time-Size history if exitst
  UpdateTimeSizeLog();
}

bool     Queue::Detailed()
{
  return detailed; // True if detailed packet enquing
}

void     Queue::Detailed(bool d)
{
  detailed = d;
}

Time_t   Queue::QueuingDelay()
{
  if (!interface) return 0;  // No idea if no interface assigned
  Rate_t linkBw = interface->GetLink()->Bandwidth();
  return ((Length() * 8.0) / linkBw); // Amount of time to drain queue
}

DCount_t Queue::TotalWorkload()
{
  return totalByteSeconds;
}

void Queue::SetInterface(Interface* i)
{ // Set the associated interface
  interface = i;
}

Count_t Queue::DropCount()
{
  return dropCount;
}

Count_t Queue::EnqueueCount()
{
  return totEnq;
}

void    Queue::CountEnq(Packet* p)
{ // Called by interface when link is not busy.
  // Packets bypass the queue, but should be counted for statistics
  // Packet pointer is needed by some queuing methods (dualqueue)
  // to determine which sub-queue should be used.
  totEnq++;
}

void    Queue::ResetStats()
{
  dropCount = 0;
  totEnq = 0;
}

void    Queue::AddForcedLoss(Time_t t, Count_t c, Time_t e)
{
  if (!forcedLosses) forcedLosses = new LossList_t();
  LossList_t::iterator i;
  for (i = forcedLosses->begin(); i != forcedLosses->end(); ++i)
    {
      if (t < i->time) break; // Insert before here
    }
  forcedLosses->insert(i, ForcedLoss(t, c, e));
}

bool    Queue::CheckForcedLoss(bool remove)
{
  if (!forcedLosses) return false; // No forced loss list
  if (forcedLosses->empty()) return false;  // list is empty
  LossList_t::iterator i;
  Time_t now = Simulator::Now();
  bool r = false;

  for (i = forcedLosses->begin(); i != forcedLosses->end(); )
    {
      if (now < i->time) return r; // Done checking
      if (now < i->expiration) r = true;
      if (0) cout << "Forced loss at " << now << " count " << i->count << endl;
      if (remove) i->count--;
      if (i->count == 0 || now >= i->expiration)
        { // Done with this one, remove it
          LossList_t::iterator j = i;
          ++i;
          if (remove) forcedLosses->erase(j);
        }
      if (r) return r; // No need to keep checking
    }
  return r; // code later
}

bool   Queue::CheckSpoofedSource(Packet*)
{
  return false; // Not checked in "normal" queues
}

// Time/Size log methods
void   Queue::EnableTimeSizeLog(bool b)
{
  if (b)
    { // Enable
      if (!timeSizeLog) timeSizeLog = new TimeSeq_t();
    }
  else
    { // Disable.  Delete any existing.
      delete timeSizeLog;
      timeSizeLog = nil;
    }
}

void   Queue::LogSizePackets(bool b)
{ // Specify logging in packets, not byets (if b)
  timeSizePackets = b;
}

void   Queue::PrintTimeSizeLog(ostream& os, Count_t div, char sep)
{
  if (!timeSizeLog) return; // no data available
  for (TimeSeq_t::size_type i = 0; i < timeSizeLog->size(); ++i)
    {
      TimeSeq& ts = (*timeSizeLog)[i];
      Count_t v = ts.seq;
      if (div) v /= div;
      os << ts.time << sep << v << endl;
    }
}

void   Queue::DisplayQueue(QTWindow* qtw, Node* s, Node* d)
{
#ifdef HAVE_QT
  if (!animate) return;
  QPoint sp = qtw->NodeLocation(s); // Get location of source
  QPoint dp = qtw->NodeLocation(d); // Get location of destination
  // Compute slope of line connecting src/dst
  double dx = dp.x() - sp.x();
  double dy = dp.y() - sp.y();
  // Compute the angle of a line from src to dst
  double theta = atan2(dy, dx);
  double sinTheta = sin(theta);
  double cosTheta = cos(theta);
  double sinThetaM2 = sin(theta + M_PI_2);
  double cosThetaM2 = cos(theta + M_PI_2);

  // Allocate the qLines vector if not already
  if (!qLines) qLines = new ItemVec_t();

#ifdef OLD_WAY
  Count_t k = 0;

  while(true)
    {
      // Display this one
      Packet* p = GetPacket(k);
      if (!p) break; // No more
      double lx = sp.x() + k * cosThetaM2; // Left x
      double ly = sp.y() + k * sinThetaM2; // Left y
      double rx = sp.x() + Queue::defaultAnimWidth * cosTheta + k * cosThetaM2;
      double ry = sp.y() + Queue::defaultAnimWidth * sinTheta + k * sinThetaM2;
      QCanvasLine* line = nil;
      if (k < qLines->size())
        {
          line = (*qLines)[k];
        }
      else
        {
          line = new QCanvasLine(qtw->Canvas());
          qLines->push_back(line);
        }
      if (p->IsColored())
        {
          line->setPen(QColor(p->R(), p->G(), p->B()));
        }
      else
        {
          line->setPen(Qt::blue);
        }
      line->setPoints((int)lx, (int)ly, (int)rx, (int)ry);
      line->show();
      ++k;
    }
#else
  ColorVec_t colors;
  GetPacketColors(colors);

  ColorVec_t::size_type i = 0;
  ItemVec_t::size_type k = 0;
  for (; i < colors.size(); ++i)
    {
      QCanvasLine* line = nil;
      if (k < qLines->size())
        {
          line = (*qLines)[k];
        }
      else
        {
          line = new QCanvasLine(qtw->Canvas());
          qLines->push_back(line);
          double lx = sp.x() + k * cosThetaM2; // Left x
          double ly = sp.y() + k * sinThetaM2; // Left y
          double rx = sp.x() + Queue::defaultAnimWidth * cosTheta +
              k * cosThetaM2;
          double ry = sp.y() + Queue::defaultAnimWidth * sinTheta +
              k * sinThetaM2;
          line->setPoints((int)lx, (int)ly, (int)rx, (int)ry);
        }
      line->show();
      line->setPen(QColor(colors[i]));
      ++k;
    }
#endif

  while(k < qLines->size())
    { // Hide any excess
      (*qLines)[k]->hide();
      ++k;
    }
#endif
}

// Private method used by subclasses to log the size vs time info
void   Queue::UpdateTimeSizeLog()
{
  // Note that we use the TimeSeq log from TCP for this, which is
  // a slight hack.  The TimeSeq object uses a type Seq_t for
  // the value being logged, but we need a Count_t value.  Luckily
  // these are both just unsigned long's so we are ok.
  if (!timeSizeLog) return;
  Count_t l = Length();
  if (timeSizePackets) l = LengthPkts();
  if (!timeSizeLog->empty())
    { // See if current timestamp is same as prior,
      // if so, overwrite last entry
      TimeSeq& last = timeSizeLog->back();
      if (last.time == Simulator::Now())
        { // Same, just overwrite
          last.seq = l;
          return;
        }
    }
  timeSizeLog->push_back(TimeSeq(Simulator::Now(), l));
}





// Static Methods
void   Queue::Default(const Queue& d)
{
  if (defaultQueue) delete defaultQueue; // Delete any existing
  defaultQueue = d.Copy();
}

Queue& Queue::Default()
{
  if (!defaultQueue) defaultQueue = new DropTail();
  return *defaultQueue;
}

void   Queue::DefaultLength(Count_t l)
{
  defaultLength = l;
  Default().SetLimit(l);
}

void   Queue::DefaultLimitPkts(Count_t l)
{
  defaultLimitPkts = l;
  Default().SetLimitPkts(l);
}


