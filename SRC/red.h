// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: red.h 92 2004-09-14 18:18:43Z dheeraj $



// Georgia Tech Network Simulator - RED Queue class
// Tarik Arici (tariq@ece.gatech.edu), Aug. 2002

#ifndef __red_h__
#define __red_h__

#include <list>
#include "droptail.h"

//Doc:ClassXRef
class REDQueue : public DropTail {
 //Doc:Class This class derives from the class DropTail and implements
  //Doc:Class the Random Early Detection Queuing discipline. More references
  //Doc:Class about RED can be founf at Sally Floyd's webpages
  public:
    //Doc:Method
    REDQueue();
    //Doc:Desc Default constructor
    
    //Doc:Method    
    REDQueue( DCount_t in_w_q, Count_t in_min_th, Count_t in_max_th, 
	     Count_t in_limit, DCount_t in_max_p, Count_t in_mean_pktsize);
    //Doc:Desc This constructor the critical RED parameters and builds a
    //Doc:Desc correspoding RED queue
    //Doc:Arg1 weight of the queue
    //Doc:Arg2 minimum threshold
    //Doc:Arg3 maximum threshold
    //Doc:Arg4 Limit/max size for the queue
    //Doc:Arg5 maximum value for mark/drop probability
    //Doc:Arg6 Average packet size
    
    //Doc:Method
    virtual ~REDQueue() { }
    //Doc:Desc Destructor
    
    //Doc:Method
    bool Enque(Packet*);  // Return true if enqued, false if dropped
    //Doc:Desc This method is used to enque a packet in the queue
    //Doc:Arg1 the pointer to the packet to be added to the queue
    //Doc:Return a boolean to indicate if the enque operation succeeded

    //Doc:Method    
    Packet* Deque();     // Return next element (NULL if none)
    //Doc:Desc This method deques a packet from the queue and returns it
    //Doc:Return Pointer to the packet

    
    //Doc:Method    
    Packet* MarkPacketwProb();
    //Doc:Desc This method marks a packet with its mark/drop probablity. This
    //Doc:Desc method is used when the average queue is still between the min
    //Doc:Desc and max thresholds
    //Doc:Return the marked packet
    
    //Doc:Method
    Packet* MarkPacket();
    //Doc:Desc This method marks a packet and returns a pointer to it.
    //Doc:Desc This is method is called when the current queue size is
    //Doc:Desc larger than the max threshold.
    //Doc:Return Pointer to the packet

    //Doc:Method
    Packet* DropPacket();
    //Doc:Desc This method drops the packet from the queue, it is called
    //Doc:Desc when we need an unconditional drop. like when the queue
    //Doc:Desc overflows
    
    //Doc:Method
    Count_t Length() { return size;} // Length in bytes
    //Doc:Desc This method returns the size of the queue in terms of bytes
    //Doc:Return number of bytes

    //Doc:Method
    Queue*  Copy() const;            // Make a copy
    //Doc:Desc This method makes a copy of the queue and returns a pointer
    //Doc:Desc to the copy
    //Doc:Return pointer to a copy of the queue
    
    //Doc:Method
    void    SetInterface(Interface*);// Associated interface
    //Doc:Desc This method attaches the queue to an interface
    //Doc:Arg1 The interface to which it is to be attached

    //Doc:Method
    Rate_t  Bandwidth();             // Get bandwidth of associated link
    //Doc:Desc This method returns the bandwidth of the link associated with
    //Doc:Desc the interface that this queue is attached to.
    //Doc:Return the bandwidth of the link
  private:
    int count;            // packets since last marked packet
    Count_t mean_pktsize; // mean packet size
    Count_t size;         // current bytes in queue
    DCount_t q_avg;       // average queue size
    Count_t min_th;       // minimum threshold
    Count_t max_th;       // maximum threshold
    DCount_t w_q;         // queue weight for the current queue size
    DCount_t max_p;       // maximum value for mark/drop probability
    Time_t idle_since;    // queue is idle since this time
    PktList_t pkts;       // List of packets in the queue
    Interface* iface;     // Associated interface
    // to be modified later
    Count_t bandwidth;    // bandwidth(bits) (we have to get this from the link)
    DCount_t pktspersec;  // pkts per sec is used to update q_avg when it's idle

};
#endif



    
