// Test program for Grid of nodes object
// George F. Riley.  Georgia Tech, Spring 2002

#include <iostream>

#include "simulator.h"
#include "grid.h"
#include "rng.h"
#include "tcp-tahoe.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"

using namespace std;

int main(int argc, char** argv)
{
  Count_t xs = 5;
  Count_t ys = 5;
  if (argc > 1) xs = atol(argv[1]);
  if (argc > 2) ys = atol(argv[2]);
  // Create the simulator object
  Simulator s;

  // Create the grid
  Grid g(xs, ys, IPAddr("192.0.1.1"), 0, 0, 1, 1);
  for (Count_t x = 0; x < xs; ++x)
    {
      Node* nb = g.GetNode(x,0); // Bottom row
      Node* nt = g.GetNode(x,ys-1); // Top row
      // Create a tcp server to use on top row
      TCPServer* server = (TCPServer*)nt->AddApplication(TCPServer());
      server->BindAndListen(nt, 80);
      // Create a tcp application to connect to the servers
      TCPSend* app = (TCPSend*)nb->AddApplication(
               TCPSend(TCPTahoe(), nt->GetIPAddr(), 80, Constant(100000)));
      app->Start(1.0 * x); // Staggered start
    }
  // Print some debug info for each
  for (Count_t y = 0; y < ys; ++y)
    {
      for (Count_t x = 0; x < xs; ++x)
        {
          Node* n = g.GetNode(x, y);
          cout << "Node " << x << " " << y
               << " location " << n->LocationX() << " " << n->LocationY()
               << " ip " << (string)IPAddr(n->GetIPAddr()) 
               << " number of ifaces " << n->InterfaceCount()
               << endl;
        }
    }
  s.StopAt(10.0);
  s.Progress(1.0);
  s.Run(); // Run the simulation
  cout << "Total events processed " << s.TotalEventsProcessed() << endl;
  cout << "Setup time " << s.SetupTime() << endl;
  cout << "Route time " << s.RouteTime() << endl;
  cout << "Run time "   << s.RunTime() << endl;
}

  



