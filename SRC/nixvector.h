/* -*- mode: c++ -*- */

// GENERAL PUBLIC LICENSE AGREEMENT
//
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
//
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
//
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
//
// 1.  This License allows you to:
//
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
//
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//
//
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
//
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
//
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
//
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
//
//
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
//
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
//
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
//
//
// 3.  Export Law Assurance.
//
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//
// 4.  Termination.
//
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
//
//
// 5.  Disclaimer of Warranties and Limitation on Liability.
//
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
//
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
//
// GTRC shall have no obligation for support or maintenance of Program.
//
// 6.  Copyright Notice.
//
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
//
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
//
// Said copyright notice shall read as follows:
//
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


#ifndef __NIXVECTOR_H
#define __NIXVECTOR_H

#include <vector>
#include <deque>
#include <list>
#include "common-defs.h"
#include "ipaddr.h"
#include "macaddr.h"
#include "ipv4.h"
#include "timer.h"

#define NC_LIST_SIZE		64
#define REPLY_LIST_SIZE		32
#define NVREQ_LIST_SIZE		20
#define MAX_NEIGHBOR		32000
#define SEND_BUF_SIZE		64

#define	NO_COLOR_BITS		4
#define	WHITE_COLOR		0x0
#define NO_MAC_BITS		48

#define	NVR_OPT_SYM		0x1    // symmetric routing
#define	NVR_OPT_ERR		0x2    // containing routing fail info
#define	NVR_OPT_RNV		0x4    // build RNV while routing

#define	NV_ATTR_UNI		0x1    // NV set up partially

#define METRIC_NONE		0
#define METRIC_HOP		1
#define METRIC_LOSS		2

inline int bit2dw(int len) { return len/32 + (len%32 ? 1:0); }
int bitcpy(Long_t* dst, int dp, Long_t* src, int sp, int len);

class Packet;
class CRouteInfo;
class NVREvent;

typedef std::vector<CRouteInfo*> RIVec_t;


// n-tuple double word class
class DWn {
public:
  DWn() : size(0), dw(0) {}
  DWn(int sz);
  virtual ~DWn() { if(dw) delete[] dw; }
  DWn& operator =   (DWn& r);
  DWn& operator <<= (int n);
  DWn& operator >>= (int n);
  DWn& operator &=  (DWn& r);
  DWn& operator |=  (DWn& r);

public:
  int     size;
  Long_t  *dw;
};

// Path ID
class PathId {
public:
  PathId() : dst(0), org(0), sn(0), dn(0) {}
  PathId(IPAddr_t d, IPAddr_t o, Byte_t n1=0, Byte_t n2=0)
      : dst(d), org(o), sn(n1), dn(n2) {}

  bool semi_equal(const PathId& r) { return dst == r.dst && org == r.org; }
  bool operator == (const PathId& r)
  { return semi_equal(r) && sn == r.sn && dn == r.dn; }
  bool operator != (const PathId& r) { return !(*this == r); }
  bool operator < (const PathId& r) { return semi_equal(r) && dn < r.dn; }
  bool operator > (const PathId& r) { return semi_equal(r) && dn > r.dn; }

public:
  IPAddr_t  dst;
  IPAddr_t  org;
  Byte_t    sn;    // NVREQ #
  Byte_t    dn;    // NVREP #
};

// Metric Vector
class CMetricVec {
public:
  CMetricVec() : hop(0), sig(1), pks(0), pkt(0) {}
  CMetricVec(Byte_t h, float s, float p, float p2)
      : hop(h), sig(s), pks(p), pkt(p2) {}

public:
  Byte_t  hop;    // hop count
  float   sig;    // path stability index
  float   pks;    // # packets
  float   pkt;    // # packets
};

// CNCItem
class CNCItem {
public:
  CNCItem() : id(0), src(0), dst(0), t(0) {}
  CNCItem(Byte_t i, IPAddr_t s, IPAddr_t d, Time_t ts, MACAddr_t inf)
      : id(i), src(s), dst(d), t(ts), from_inf(inf) {}

public:
  Byte_t    id;          // nc-id
  IPAddr_t  src;
  IPAddr_t  dst;
  Time_t    t;           // timestamp
  MACAddr_t from_inf;    // debug
};

// CNCList
class CNCList {
public:
  CNCList() {}
  virtual ~CNCList() {}

  void    Add(Byte_t, IPAddr_t, IPAddr_t, MACAddr_t inf=MACAddr::NONE);
  int     Find(Byte_t, IPAddr_t, IPAddr_t);
  CNCItem*  At(int i) { return &Item[i]; }

public:
  typedef std::deque<CNCItem> NCVec_t;
  static Word_t defaultSize;

  NCVec_t  Item;
};

// CRepItem
class CRepItem {
public:
  CRepItem() : type(0), id(0), mid(METRIC_NONE), src(0), t(0), cnt(0) {}
  CRepItem(Byte_t ty, Byte_t i, Byte_t m, CMetricVec& n, IPAddr_t s, Time_t ts)
      : type(ty), id(i), mid(m), mv(n), src(s), t(ts), cnt(0) {}

public:
  Byte_t      type;     // packet type
  Byte_t      id;       // NC id
  Byte_t      mid;      // metric id
  CMetricVec  mv;       // metric vector
  IPAddr_t    src;      // src addr
  Time_t      t;        // timestamp
  Byte_t      cnt;      // reply count, test
};

// CReplyList
class CReplyList {
public:
  CReplyList() {}
  virtual ~CReplyList() {}

  void Add(IPAddr_t s, Byte_t id, Byte_t t, Byte_t mid, CMetricVec& mv);
  void Update(int i, CMetricVec& mv);    // update metric vector
  CRepItem* Find(IPAddr_t src, Byte_t id, Byte_t type);

public:
  typedef std::deque<CRepItem> RepVec_t;
  static Word_t defaultSize;

private:
  RepVec_t  Item;
};

// CNixVector
class CNixVector {
public:
  CNixVector() : nvl(0), nvu(0), nv(0) {}
  CNixVector(const CNixVector& r);
  CNixVector(Word_t nvl, Word_t nvu, Long_t* nv);
  virtual ~CNixVector();

  void   ExpandBits(int bitlen);
  void   Set(MACAddr_t mac);
  void   Set(int nix);
  int    Get(MACAddr& aix);
  bool   Advance(int n);
  Byte_t PeekColor();
  void   Clear()       { if(nv) delete[] nv; nvl=0; nvu=0; nv=0; }
  Size_t Size() const  { return nvl/8 + (nvl%8 ? 1:0); }    // byte
  //Size_t SSize() const { return sizeof(nvl)+sizeof(nvu)+Size(); }
  Size_t SSize() const { return sizeof(nvl)+Size(); }
  bool   IsValid()     { return nvl > 0; }
  bool   IsEnd()       { return nvl < nvu+NO_COLOR_BITS; }
  void   Dump();    // dump to console
  CNixVector& operator = (const CNixVector& r);

public:
  Word_t  nvl;
  Word_t  nvu;
  Long_t* nv;
};

// CNeighbor
class CNeighbor {
public:
  CNeighbor() : val(false), cnt(0) {}
  CNeighbor(MACAddr_t m, bool v) : mac(m), val(v), cnt(0) {}

  bool Valid() { return val; }
  bool Empty() { return mac == MACAddr::NONE; }

public:
  MACAddr_t mac;
  bool      val;    // validity
  Byte_t    cnt;    // retry count
};

typedef std::vector<CNeighbor> NbrVec_t;

// CNbrTable
class CNbrTable {
public:
  CNbrTable();
  virtual ~CNbrTable() {}

  int  Add(MACAddr_t mac);
  bool InvalidateIndex(int i);
  bool Invalidate(MACAddr_t mac);
  int  Find(MACAddr_t mac);
  Size_t Size() const;    // byte

public:
  static Word_t defaultMaxNeighbor;
  static Byte_t defaultRetryThresh;

  int       nNbr;    // # of neighbors
  NbrVec_t  Nbr;
};

class MyEvent : public TimerEvent {
public:
  int  h;
};

// CRouteInfo
class CRouteInfo {
public:
  bool  Valid() { return val; }

public:
  bool        val;      // validity
  Byte_t      mid;      // metric id
  Byte_t      attr;     // NV attribute
  PathId      pid;      // path id
  CMetricVec  mv;       // metric vector
  CNixVector  nv;       // NV
  Time_t      time;     // timestamp
  MyEvent     ev;
};

// CNVCache
class CNVCache {
public:
  int   Find(const RIVec_t&, const PathId&, bool);
  void  Add(int);

private:
  typedef std::deque<int> IntVec_t;
  IntVec_t  cache;
};

// CNVFIB
class CNVFIB : public TimerHandler {
public:
  CNVFIB();
  virtual ~CNVFIB();

  void  Add(const PathId&, Byte_t, const CMetricVec&, const CNixVector&,
            Byte_t attr=0, bool=false);
  int   Remove(IPAddr_t dst);
  int   Remove(Byte_t mid, IPAddr_t dst);
  int   Remove(int i);
  void  Invalidate(int i);
  int   Find(IPAddr_t dst, bool force=false);
  int   Find(const PathId& pid, bool force=false);
  int   FindByDst(IPAddr_t dst, Word_t dn, bool force=false);
  int   Search(Byte_t mid, IPAddr_t dst);
  void  Update(int i, Byte_t hop, CNixVector& nv);
  void  Update(int i, IPAddr_t dst, Byte_t hop, CNixVector& nv);
  bool  IsBetter(int i, Byte_t hop)  { return (hop < Item[i]->mv.hop); }
  bool  IsWorse(int i, Byte_t hop)   { return (hop > Item[i]->mv.hop); }
  bool  IsShorter(int i, Word_t nvl) { return (nvl < Item[i]->nv.nvl); }
  void  Timeout(TimerEvent*);

public:
  static Word_t defaultSize;
  static Time_t defaultTimeout1;    // invalidate
  static Time_t defaultTimeout2;    // remove
  static Count_t count[2];

  Timer    timer;
  RIVec_t  Item;
  CNVCache nvCache;    // small NV cache
};

// NVReqItem
class NVReqItem {
public:
  NVReqItem() : dst(0), id(0), ttl(0), cnt(0), time(0), timer(nil) {}
  NVReqItem(IPAddr_t d, Byte_t i, Byte_t t, Byte_t c, Time_t ts, NVREvent* e)
      : dst(d), id(i), ttl(t), cnt(c), time(ts), timer(e) {}

public:
  IPAddr_t   dst;         // target
  Byte_t     id;          // NVREQ ID
  Byte_t     ttl;         // ttl in NVREQ
  Byte_t     cnt;         // retry count
  Time_t     time;        // timestamp
  //Time_t   lifetime;    // remaining sec till timeout, not used
  NVREvent*  timer;       // timer instead of lifetime
};

// CNCQueItem
class CNCQueItem {
public:
  CNCQueItem() : p(nil), timer(nil) {}
  CNCQueItem(Packet* pkt, NVREvent* t) : p(pkt), timer(t) {}

public:
  Packet*   p;
  NVREvent* timer;
};

// SendBufItem
class SendBufItem {
public:
  SendBufItem(Packet* pkt, IPAddr_t d, Time_t ts) : p(pkt), dst(d), t(ts) {}

public:
  Packet*   p;      // L3 packet
  IPAddr_t  dst;
  Time_t    t;      // timestamp
};

// Black List
class BLEntry {
public:
  BLEntry() : ip(0), t(0) {}
  BLEntry(IPAddr_t i, Time_t ti) : ip(i), t(ti) {}

public:
  IPAddr_t  ip;
  Time_t    t;
};

typedef std::deque<BLEntry> BlackList_t;

class BlackList {
public:
  void  Add(IPAddr_t);
  bool  Find(IPAddr_t);

public:
  BlackList_t  list;
};

#endif
