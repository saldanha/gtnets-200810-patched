#ifndef BLUETUS_H
#define BLUETUS_H

typedef unsigned long uLong;
typedef unsigned short uShort;
typedef unsigned char uChar;
typedef enum {MASTER=0x0001, SLAVE=0x0002}RoleType;

//Error code returned by routines.
#define SUCCESS   0x0000
#define BT_ERROR  0x0001
//bluetooth device address 48 bits.
struct BdAddr{
  uShort usAddr[3];
  public:                                                       
      BdAddr& operator=(const BdAddr& addr) {                   
      usAddr[0] = addr.usAddr[0];                           
      usAddr[1] = addr.usAddr[1];                           
      usAddr[2] = addr.usAddr[2];                           
      return *this;
    }   
  bool operator==(const BdAddr& addr) {
    return ((usAddr[0] == addr.usAddr[0])
        &&(usAddr[1] == addr.usAddr[1])
        &&(usAddr[2] == addr.usAddr[2]));
  }
    
};

#endif
