// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: tree.cc 92 2004-09-14 18:18:43Z dheeraj $



// Georgia Tech Network Simulator - Tree topology
// George F. Riley.  Georgia Tech, Fall 2002

// Define the tree topology using point-to-point links

#include "tree.h"
#include "node.h"
#include "linkp2p.h"
#include "mask.h"

#include <math.h>
#include <stdio.h>

using namespace std;

// Constructors

Tree::Tree(Count_t l, Count_t f, IPAddr_t i, SystemId_t id)
{
  ConstructorHelper(l, f, Linkp2p::Default(), i, id);
}

Tree::Tree(Count_t l, Count_t f, const Linkp2p& link,
           IPAddr_t i, SystemId_t id)
{
  ConstructorHelper(l, f, link, i, id);
}

// Access functions
Node* Tree::GetRoot()
{ // Get the root node
  return GetNode(0,0);
}

Node* Tree::GetNode(Count_t l, Count_t c) // Level, index
{ // Get Specified right side leaf node
  Count_t  nodesThisLevel = 1; // Starting with root
  NodeId_t firstId = first;    // First ID for this level
  for (Count_t thisLevel = 0; thisLevel < l; ++thisLevel)
    {
      firstId += nodesThisLevel;
      nodesThisLevel *= fanout;
    }
  if (c < nodesThisLevel) return Node::GetNode(firstId + c);
  return nil; // Out of range
}

Node* Tree::GetLeaf(Count_t c)
{ // Get specified leaf
  if (c < leafCount) return Node::GetNode(firstLeafId + c);
  return nil; // Out of range
}

Linkp2p* Tree::GetChildLink(Count_t l,  // Level
                            Count_t i,  // Index on level
                            Count_t c)  // Child number
{ 
  if (c >= fanout) return nil;
  Node* n = GetNode(l, i);
  if (!n) return nil;
  Node* cn = GetNode(l + 1, i * fanout + c);
  if (!cn) return nil;
  return (Linkp2p*)n->GetLink(cn);
}

Linkp2p* Tree::GetParentLink(Count_t l, Count_t i)
{ // Get link to parent from specified node
  Node* n = GetNode(l, i);
  if (!n) return nil;
  Node* pn = GetNode(l - 1, i / fanout);
  if (!pn) return nil;
  return (Linkp2p*)n->GetLink(pn);
}

Queue* Tree::GetChildQueue(Count_t l,  // Level
                           Count_t i,  // Index on level
                           Count_t c)  // Child number
{ 
  if (c >= fanout) return nil;
  Node* n = GetNode(l, i);
  if (!n) return nil;
  Node* cn = GetNode(l + 1, i * fanout + c);
  if (!cn) return nil;
  return n->GetQueue(cn);
}

Queue* Tree::GetParentQueue(Count_t l, Count_t i)
{ // Get link to parent from specified node
  Node* n = GetNode(l, i);
  if (!n) return nil;
  Node* pn = GetNode(l - 1, i / fanout);
  if (!pn) return nil;
  return n->GetQueue(pn);
}

void Tree::BoundingBox(const Location& ll, const Location& ur)
{
  vector<Meters_t> myX;
  LocateRow(0, 1, first, ll, ur, myX);
}

void Tree::BoundingBox(const Location& ll, const Location& rr, double angle)
{
  Meters_t xx,yy,x,y,nx,ny;
  Count_t thisLevelCount = 1;

  angle = M_PI/180.0 * angle;  // convert to radians

  BoundingBox(ll, rr);
  xx = (ll.X()+rr.X())/2;
  yy = rr.Y();

  for (Count_t lev = 0; lev < levels; ++lev) {
    DEBUG0((cout<<"Nodes in this level "<<thisLevelCount<<endl));
    for (Count_t count = 0; count < thisLevelCount; ++count) {
      Node *node = GetNode(lev, count);

      DEBUG0((cout<<"In level "<<lev<<" count "<<count<<endl));
      
      x = node->GetLocation().X()-xx;
      y = node->GetLocation().Y()-yy;

      nx = x* cos(angle)-y*sin(angle);
      ny = x* sin(angle)+y*cos(angle);

      nx = nx + xx;
      ny = ny + yy;

      DEBUG0((printf("Location %6.2lf %6.2lf\n", nx,ny)));
      
      node->SetLocation(Location(nx,ny));
      
    }
    thisLevelCount *=fanout;
    DEBUG0((cout<<"Going to the next level"<<endl));
  }

}

// Private methods
void Tree::ConstructorHelper(Count_t l, Count_t f,
                             const Linkp2p& link,
                             IPAddr_t leafIP,
                             SystemId_t id)
{
  first = Node::nextId;
  levels = l;
  fanout = f;
  Node* n = new Node(id);
  Count_t   lastLevelCount = 1;
  NodeId_t  lastLevelFirstId = n->Id();
  IPAddr_t  nextIP = leafIP;

  for (Count_t lev = 1; lev < levels; ++lev)
    { // Create each subsequent level
      bool     lastLevel = (lev == (levels-1));
      Count_t  thisLevelCount = lastLevelCount * fanout;
      NodeId_t thisLevelFirstId = Node::nextId;
      if (lastLevel)
        {
          firstLeafId = Node::nextId;
          leafCount = thisLevelCount;
        }
      for (Count_t c = 0; c < thisLevelCount; ++c)
        { // Create each child
          Node* parent = Node::GetNode(lastLevelFirstId + c / fanout);
          Node* child = new Node(id);
          if (lastLevel)
            { // Create link with IP Address
              if (nextIP == IPADDR_NONE)
                { // No IP specified
                  child->AddDuplexLink(parent, link);
                }
              else
                {
                  child->AddDuplexLink(parent, link,
                                       nextIP++, Mask(32),
                                       IPADDR_NONE, Mask(32));
                }
            }
          else
            { // Create anonymous (no IPAddr) link
              child->AddDuplexLink(parent, link);
            }
          // Add a default route from child to parent
          child->DefaultRoute(parent);
        }
      lastLevelCount = thisLevelCount;
      lastLevelFirstId = thisLevelFirstId;
    }
  last = Node::nextId;
}

void Tree::LocateRow(Count_t level, Count_t leafThisRow, NodeId_t firstId, 
                      const Location& ll, const Location& ur,
                      std::vector<Meters_t>& parentX)
{ // LocateRow recurses through the rows, since each row needs to know
  // the locations of the children to properly position the parent.
  vector<Meters_t> myX;
  if ((level + 1) < levels)
    {
      LocateRow(level+1, leafThisRow * fanout, firstId + leafThisRow,
                 ll, ur, myX);
    }
  else
    { // This is the bottom row.  Calculate X Locations
      Meters_t xSpacing = (ur.X() - ll.X()) / (leafThisRow + 1);
      Meters_t xLoc = xSpacing;
      for (Count_t i = 0; i < leafThisRow; ++i)
        {
          myX.push_back(xLoc);
          xLoc += xSpacing;
        }
    }
  
  const NodeVec_t& nodes = Node::GetNodes();
  Meters_t minX = 0;
  Meters_t ySpacing = (ur.Y() - ll.Y()) / levels;
  Meters_t yLoc = ySpacing / 2.0 + level * ySpacing;
  
  for (Count_t i = 0; i < leafThisRow; ++i)
    {
      Meters_t xLoc = myX[i];
      Node* n = nodes[firstId + i];
      n->SetLocation(ll.X() + xLoc, ur.Y() - yLoc);
      // Maintain stats for higher level x-location
      if ((i % fanout) == 0)
        {
          minX = xLoc;
        }
      if (((i+1) % fanout) == 0)
        {
          Meters_t upX = minX + (xLoc - minX) / 2;
          parentX.push_back(upX);
        }
    }
}

