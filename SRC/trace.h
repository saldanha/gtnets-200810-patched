// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: trace.h 92 2004-09-14 18:18:43Z dheeraj $



// Georgia Tech Network Simulator - Global trace control
// George F. Riley.  Georgia Tech, Spring 2002

#ifndef __trace_h__
#define __trace_h__

#include <fstream>
#include <vector>

#include "common-defs.h"
#include "tfstream.h"

class Node;
class Protocol;
class PDU;

//Doc:ClassXRef
class Trace {
  //Doc:Class Class {\tt Trace} defines the behavior of the packet tracing
  //Doc:Class feature of \GTNS.
public:
  typedef enum { ENABLED, DISABLED, DEFAULT } TraceStatus;
  typedef std::vector<TraceStatus> TSVec_t;
  typedef bool (*Callback_t)(Node*,Protocol*,PDU*,bool);
public:
  //Doc:Method
  Trace();
    //Doc:Desc Default constructor, no arguments.

  ~Trace();
  //Doc:Method
  bool Open(const char*);  // Open the trace file
    //Doc:Desc Create a trace file.
    //Doc:Return     //Doc:Arg1 File name.
    //Doc:Return True if successfully opened.

  //Doc:Method
  void Close();            // Close the trace file
    //Doc:Desc Close the trace file.  Should be called when the simulation completes.

  //Doc:Method
  void On();               // Enable all traces
    //Doc:Desc Enable tracing for all protocol layers.

  //Doc:Method
  void On(Layer_t);        // Enable for specific layer
    //Doc:Desc Enable tracing for a specified protocol layer.
    //Doc:Arg1 Layer to enable (2, 3, or 4).

  //Doc:Method
  void Off();              // Disable all traces
    //Doc:Desc Enable tracing for all protocol layers.

  //Doc:Method
  void Off(Layer_t);       // Disable for specific layer
    //Doc:Desc Disable tracing for a specified protocol layer.
    //Doc:Arg1 Layer to disable (2, 3, or 4).

  //Doc:Method
  bool IsOn(Layer_t);      // Enquire trace status (on/off) by layer
    //Doc:Desc Test whether tracing is on or off by layer.
    //Doc:Arg1 Layer to test.
    //Doc:Return True if tracing enabled for this layer.

  //Doc:Method
  bool IsEnabled();        // Enquire trace file opened
    //Doc:Desc Check if trace file globally enabled.
    //Doc:Return True if enabled.

  Tfstream& GetTS() { return os;} // The trace file object
  void AppendEOL();        // Add the end of line if needed
  void NewNode(Node*);     // Log a new node for trace line
  void SetNode(Node*);     // Set the node for subsequent traces

  //Doc:Method
  void IPDotted(bool d = true) { os.IPDotted(d);}
    //Doc:Desc Specify that all \IPA{s} are to be logged in dotted notation.
    //Doc:Desc If not, trace in 32 bit hex.
    //Doc:Arg1 True if dotted desired.

  //Doc:Method
  void TimePrecision(Count_t d) { os.TimePrecision(d);}
    //Doc:Desc Specify the digits of precision in the time field for
    //Doc:Desc the trace file.
    //Doc:Arg1 Number of digits of precision.

  //Doc:Method
  static Trace* Instance();// Get the global trace object
    //Doc:Desc Get a pointer to the single, global trace object.
    //Doc:Return Pointer to trace object.

  //Doc:Method
  static bool   Enabled() { return enabled;} // True if tracing enabled
    //Doc:Desc Test if tracing is enabled.
    //Doc:Return True if tracing enabled.

  //Doc:Method
  static void AddCallback(Callback_t cb) { callback = cb;}
    //Doc:Desc Add a callback to be called on every trace decision.
    //Doc:Desc The callback is called AFTER the normal decisioning process,
    //Doc:Desc and the result of that is passsed to the callback.  Also
    //Doc:Desc passed are the node pointer, protocol pointer, and pdu pointer.
    //Doc:Desc The callback must be a funtion returning a bool (which is the
    //Doc:Desc final decision on whether the PDU is traced or not) and
    //Doc:Desc with four arguments, Node*, Protocol*, PDU*, bool.
    //Doc:Desc See definition of Callback_t in trace.h.
    //Doc:Arg1 Callback function to add.

public:
  static Callback_t callback;

private:
  static Trace* instance;  // The single global trace object
  static bool   enabled;   // Global enabled flag
  TSVec_t  trace;          // On/Off status by layer
  Tfstream os;             // File for logging
  Node*    node;           // Current node
};

#endif

