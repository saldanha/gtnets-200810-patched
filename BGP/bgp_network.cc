/****************************************************************************/
/*  File:            bgp_network.cc                                         */
/*  Author:          Sunitha Beeram                                         */
/*  Email:           sunithab@ece.gatech.edu                                */
/*  Documentation:   TBD                                                    */
/*                                                                          */
/*  Version:         1.0beta                                                */
/*                                                                          */
/*  Copyright (c) 2005 Georgia Institute of Technology                      */
/*  This program is free software; you can redistribute it and/or           */
/*  modify it under the terms of the GNU General Public License             */
/*  as published by the Free Software Foundation; either version 2          */
/*  of the License, or (at your option) any later version.                  */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful,         */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*  GNU General Public License for more details.                            */
/*                                                                          */
/****************************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "bgp.h"
#include "../SRC/ipaddr.h"
#include "../SRC/tcp.h"
#include "../SRC/tcp-tahoe.h"
#include "../SRC/common-defs.h"

/* BGP try to connect to the peer.  */
int
BGP::bgp_connect (struct peer *peer)
{
  
    BGP* rPeer;

    /* in ns2 bgp++, a new serv agent is created and bgp_accept read thread is added.
       In GTNetS BGP, Application is instantiated with CopyOnConnect and hence a new
       server thread will be created when a new connection request is seen. In 
       ConnectionFromPeer we add the code to add the bgp_accept read thread
    */
    
    if (BGP_DEBUG (events, EVENTS))
        plog_info (peer->log, "%s [Event] Connect start to %s\n",
                   peer->host, peer->host);


    /*rPeer = findBgp(IPAddr(inet_ntoa(peer->su.sin.sin_addr)).ip);
    
    if (!rPeer)
    {
        cout << "Remote peer " << inet_ntoa(peer->su.sin.sin_addr) << "not present"<< endl;
        return connect_error;
    }*/

    /* Create new agents for this connection */
  
    peer->listenAgent = new TCPTahoe();
    if (peer->listenAgent == NULL)
    {
        /*this cant happen but just in case*/
        printf(" Fatal Error connect and listen agents for peer are not initiated ... connection aborted\n");
        return connect_error;
    }
    
    /*bind to available port*/
    peer->listenAgent->Bind();
    peer->listenAgent->Attach(myNode);
    peer->listenAgent->AttachApplication(this);
    
    // Initiate the three way hand-shake
    
    peer->listenAgent->Connect(IPAddr(inet_ntoa(peer->su.sin.sin_addr)).ip,BGP_PORT_DEFAULT);
    
  return connect_in_progress;
}



/* After TCP connection is established.  Get local address and port. */
void
BGP::bgp_getsockname (struct peer *peer)
{
    if (peer->su_local)
    {
        XFREE (MTYPE_TMP, peer->su_local);
        peer->su_local = NULL;
    }
    
    if (peer->su_remote)
    {
        XFREE (MTYPE_TMP, peer->su_remote);
        peer->su_remote = NULL;
    }
    struct bgp *bgp = bgp_get_default();
    
    peer->su_local  = ( union sockunion *) XMALLOC (MTYPE_TMP, sizeof (union sockunion));
    memcpy ( peer->su_local, &bgp->su, sizeof (struct sockaddr_in));
    
    peer->su_remote = ( union sockunion *) XMALLOC (MTYPE_TMP, sizeof (union sockunion));
    memcpy (peer->su_remote, &peer->su, sizeof (struct sockaddr_in));
    
    if (peer->su_local && peer->su_remote && (peer->su_local->sa.sa_family == AF_INET))
    {
        peer->nexthop.v4 = peer->su_local->sin.sin_addr;
    }
    peer->nexthop.ifp = NULL;
    
}
