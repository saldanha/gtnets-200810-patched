// Test animation of random tree
// Monirul I Sharif, Georgia Tech, Spring 2004

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "tree.h"
#include "randomtree.h"
#include "ratetimeparse.h"
#include "udp.h"
#include "linkp2p.h"
#include "validation.h"  

#ifdef HAVE_QT
#include <qnamespace.h>
#endif

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;

  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);
  
  //Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  //tr->Open("testanim.txt");

  Linkp2p lk(Rate("10Mb"), Time("20ms"));

  RandomTree r(4, 8, lk, IPAddr("192.168.0.0"), 1.0);
  //  RandomTree r(4, 4, lk, 2, 2, lk, IPAddr("192.168.0.0"), 1.0);
  //  Tree r(4,4,lk, IPAddr("192.168.0.0"));
  //  r.BoundingBox(Location(0,0),Location(10,10));

  cout<<"Tree has "<<r.LeafCount()<<" leaves"<<endl;

  UDP* u = new UDP(r.GetLeaf(0));
  u->Bind();

  //IPAddr_t a = IPAddr("192.168.0.16");

  /*  for (int i=0; i<64; i++) {
    string s;
    s = "192.168.0.";
    s+=i;
    u->AddExtraTxDelay(Time("10ms"));
    u->SendTo(1000, IPAddr(s), 10);
    }*/

    //  u->SendTo(1000, IPAddr("192.168.1.5"), 10);
    //   u->SendTo(1000, IPAddr("192.168.0.8"), 10);
    //u->SendTo(1000, IPAddr("192.168.0.19"), 10);
//  for (int i=0; i<64; i++) {
    //u->SendTo(1000, IPAddr("192.168.1.16"), 10);

  // Specify animation
  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Progress(1);
  s.StopAt(100);
  s.Run();
}

