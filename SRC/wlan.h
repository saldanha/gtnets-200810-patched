// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: wlan.h 465 2006-02-15 15:33:22Z riley $




#ifndef __wlan_h__
#define __wlan_h__

#include "common-defs.h"
#include "link-real.h"
#include "ratetimeparse.h"
//#include "l2proto802.11.h"

typedef enum {RESERVED, IDLE} LinkState_t;

class Propagation;
class InterfaceWireless;
class L4Protocol;
class Node;

// Define the QT Update events
//Doc:ClassXRef
class WLanEvent : public Event {
public:
  typedef enum { WIRELESS_TX_START = 100, WIRELESS_TX_END, 
                 BASEBAND_TX_START, BASEBAND_TX_END } WLanEvent_t;
  WLanEvent(WLanEvent_t ev) : Event(ev), src(nil), dst(nil), iFace(nil),
      range(0), iter(0) { };
public:
  Node*    src;     // Source node  for wireless transmit animation
  Node*    dst;     // Destination node  for wireless transmit animation
  InterfaceWireless* iFace; // Interface for wireless  animation
  Meters_t range;   // Range of wireless transmission
  Count_t  iter;    // Iteration for wireless tx
};

//Doc:ClassXRef
class WirelessLink : public LinkReal {
  //Doc:Class   Class {\em WirelessLink} is a subclass derived from class
  //Doc:Class   {\tt Link} and it describes a typical wireless link.
  //Doc:Class   It currently makes some assumptions which are valid only
  //Doc:Class   in the case of a wireless LAN but may be extended to
  //Doc:Class   any wireless physical link.
public:
  typedef enum { NONE, PARTIAL, FULL } Detail_t;
  // Constructors
  // This one specifies how many nodes
  //Doc:Method
  WirelessLink(Count_t,             // Node count
	       IPAddr_t, Mask_t,    // IP addr start, mask
	       L4Protocol* l4 = nil,// Layer 4 protocol to bind to each node 
	       Rate_t r = Rate("11Mb"), 
	       Time_t dly = Time("1us"),
	       Detail_t d = PARTIAL,
	       bool bootstrap = false);
  //Doc:Desc This is the default constructor for this link. It constructs
  //Doc:Desc a default WirelessLink of a given number of nodes.
  //Doc:Arg1 Number of nodes in the LAN
  //Doc:Arg2 The IP Address of the first node in a given mask
  //Doc:Arg3 The subnet mask
  //Doc:Arg4 The layer 4 protocol to bind to each node. Currently just nil
  //Doc:Arg5 The Rate of the link.
  //Doc:Arg6 The delay in the link.
  //Doc:Arg7 The level of detail we want to model. Currently there is no
  //Doc:Arg7 difference in the model based on this argument.
  
  ~WirelessLink();      // Destructor
  // Link methods

  //Doc:Method
  virtual    Link*  Copy() const;          // All subclasses must have Copy
  //Doc:Desc This method is used to make rigid copies of the object
  //Doc:Return A pointer to the newly created copy


  //Doc:Method
  void     Handle(Event*, Time_t);
    //Doc:Desc Event handler.  Used to animate wireless transmissinos.
    //Doc:Arg1 Event to handle.
    //Doc:Arg2 Time of event.

  //Doc:Method
  virtual    bool   Transmit(Packet*, Interface*, Node*) { return false; }
  //Doc:Desc This method is used to transmit the Packet from a give node
  //Doc:Arg1 The packet to be transmitted
  //Doc:Arg2 The interface from which the packet is transmitted
  //Doc:Arg3 The node from which the packet is transmitted

  //Doc:Method
  virtual  bool Transmit(Packet*, Interface*, Node*, Time_t );
    //Doc:Desc Transmit a packet.
    //Doc:Arg1 The packet to transmit.
    //Doc:Arg2 A pointer to the local (transmitting) interface
    //Doc:Arg3 A pointer to the local (transmitting) node.
    //Doc:Arg4 The time required for transmission as determined by 802.11 

  //Doc:Method
  Time_t     SpeedOfLight(Node*, Node*); //Speed of light delay, node1 to node2
  //Doc:Desc This method returns the speed of light delay between the two
  //Doc:Desc nodes.
  //Doc:Arg1 The first node
  //DOc:Arg2 The second node

  //Doc:Method
  InterfaceWireless* AddNode(Node*, IPAddr_t = IPADDR_NONE, Mask_t = Mask(32));
    //Doc:Desc Add an existing node to this LAN.
    //Doc:Desc This is less efficient than letting the wlan object
    //Doc:Desc create the nodes during initialization, due to
    //Doc:Desc non-consecutive MAC addresses for interfaces.
    //Doc:Desc However, this is useful to add a wireless base station
    //Doc:Desc that already exists as part of a wired infrastructure.
    //Doc:Desc This method adds a wireless interface and adds that
    //Doc:Desc interface to the wlan interface vector.
    //Doc:Arg1 Node to add.
    //Doc:Return Wireless Interface added.

  //Doc:Method
  Count_t    PeerCount();                      // Return number of peers
  //Doc:Desc Returns the number of peers on this link
  //Doc:Return number of peers

  //Doc:Method
  Count_t    NodeCount();
    //Doc:Desc Returns the total number of nodes on this link
    //Doc:Return Node Count;

  //Doc:Method
  IPAddr_t   PeerIP (long unsigned int);
  //Doc:Desc Since each node on this link is also associated with a count
  //Doc:Desc this returns the IP address of the n'th  peer
  //Doc:Arg1 the count index n
  //Doc:Return the corresopnding IPAddress

  //Doc:Method
  IPAddr_t   NodePeerIP(Node*);
  //Doc:Desc Return the IP address of the node if it is on this link and
  //Doc:Desc is found
  //Doc:Arg1 The pointer to the node object
  //Doc:Return The IP Address
  
  //Doc:Method
  IPAddr_t   DefaultPeer(Interface*) { return firstIP; }
  //Doc:Desc Find the default peer's IP Address, this will find the default
  //Doc:Desc gateway's IP Address in the same subnet
  //Doc:Desc This method is valid only if we have a staic wireless LAN
  //Doc:Arg1 The interface whose default peerIP is to be known
  //Doc:Return The default peer's IP

  //Doc:Method
  void       AllNeighbors(NodeWeightVec_t & nw);
  //Doc:Desc AllNeighbors is used by broadcasts even if they are
  //Doc:Desc not routing peers. Use this to find all the peers
  //Doc:Arg1 The NodeWeightVec element, with respect to which,
  //Doc:Arg1 we need all the peers in this link

  //Doc:Method
  void       Neighbors (NodeWeightVec_t &, Interface *, bool);
  //Doc:Desc AllNeighbors is used by broadcasts even if they are
  //Doc:Desc not routing peers. Use this to find all the peers
  //Doc:Arg1 The NodeWeightVec element, with respect to which,
  //Doc:Arg1 we need all the peers in this link

  //Doc:Method
  bool       NodeIsPeer(Node*); // Find out if node is a peer
  //Doc:Desc Find out if node is a peer
  //Doc:Arg1 The Node
  //Doc:Return boolean result of the test

  
  //Doc:Method
  Count_t    NodePeerIndex(Node*);             // Get peer index for node
  //Doc:Desc Returns the index of the node in this link
  //Doc:Arg1 The pointer to the node
  //Doc:Return The index of the peer

  
  //Doc:Method  
  Count_t    NeighborCount(Node*);             // Number of routing neighbors
  //Doc:Desc Returns the number of neighbors on this link which are routers
  //Doc:Desc technically, count of routing peers
  //Doc:Arg1 Node querying neighbors (significant for Ethernet links)
  //Doc:Return The number of routing peers.

  
  //Doc:Method  
  Interface* GetIfByMac(MACAddr );
  //Doc:Desc Get a pointer to the interface by its MAC address
  //Doc:Arg1 the MAC address
  //Doc:Return pointer to the interface

  
  //Doc:Method  
  MACAddr    IPToMac(IPAddr_t);                // Convert peer IP to peer MAC
  //Doc:Desc Convert peer IP to peer MAC
  //Doc:Arg1 The IP Address whose MAC is to be determined
  //Doc:Return The corresponding MAC address
  
  
  //Doc:Method  
  void       ReserveLink(void) { state = RESERVED; }
  //Doc:Desc This method reserves the link for a given transmission interval
  
  //Doc:Method  
  void       FreeLink(void) { state = IDLE;}
  //Doc:Desc This method frees the link into IDLE state after a transmission

  //Doc:Method  
  InterfaceBasic* GetPeer(Packet*);
  //Doc:Desc This method returns the destination interface by looking
  //Doc:Desc at the destination MAC address of the packet.
  //Doc:Arg1 The packet to be transmitted.
  //Doc:Return The destination interface.

  // Get peer interface for peer index
  //Doc:Method
  virtual InterfaceBasic* GetPeer(Count_t);
    //Doc:Desc Get the interface pointer for the remote endpoint of
    //Doc:Desc this link for the specified peer index
    //Doc:Arg1 Which peer desired (0 <- i < NeighborCount())
    //Doc:Return A pointer to the receiving interface for the specified peer

  virtual void  SetPeer(InterfaceBasic*) { };

  //Doc:Method  
  bool       IsIdle();
  //Doc:Desc This method returns a bool to indicate whether a link is idle/busy
  //Doc:Return true if the link is idle, false otherwise

  //Doc:Method  
  IPAddr_t   GetIP(Count_t);
  //Doc:Desc Get IP of the node by the index of the node
  //Doc:Arg1 int index of the node
  //Doc:Return IP address if the index is within range, else IPADDR_NONE

  
  //Doc:Method  
  Node*      GetNode(Count_t);
  //Doc:Desc  Get the node by its index
  //Doc:Arg1  The index to the list of nodes
  //Doc:Return Pointer to the node
  
  
  //Doc:Method  
  Interface* GetIf(Count_t);
  //Doc:Desc Get the interface of the node connected to this link by its index
  //Doc:Arg1 the index of the node
  //Doc:Return the interface of the node that attaches to this link

  //Doc:Method
  virtual bool ScheduleFailure(Time_t t) { return false; }
  //Doc:Desc This method scedules the failure of the current link in future
  //Doc:Arg1 Time when we want to schedule it
  //Doc:Return bool, true on success and false on failure (if schedule fails)

  //Doc:Method
  virtual bool ScheduleRestore(Time_t t) {return false; }
  //Doc:Desc This method scedules the link being restored from current failure
  //Doc:Desc state. If it is not currently failed, it has no effect
  //Doc:Arg1 Time when we want to schedule the event
  //Doc:Return bool, true on success and false on failure (if schedule fails)

  // Animation routines
  //Doc:Method
  void    WirelessTxStart(Node*, InterfaceWireless*, Node*, Meters_t);
    //Doc:Desc Start the animation of a wireless transmission.
    //Doc:Arg1 Node pointer to transmitter.
    //Doc:Arg2 Wireless Interface performing the transmit
    //Doc:Arg3 Node pointer to designated receiver, nil if none.
    //Doc:Arg4 Radio range of transmitter

  //Doc:Method
  void    WirelessTxEnd(Node*, InterfaceWireless*, Node*, Meters_t);
    //Doc:Desc End the animation of a wireless transmission.
    //Doc:Arg1 Node pointer to transmitter.
    //Doc:Arg2 Wireless Interface performing the transmit
    //Doc:Arg3 Node pointer to designated receiver, nil if none.
    //Doc:Arg4 Radio range of transmitter

  //Doc:Method
  void AddInterface(Interface*);
    //Doc:Desc Add an interface to the list of transmit/receive interfaces
    //Doc:Desc for this lan.
    //Doc:Arg1 Interface to add.

public:
  //NodeId_t   first;  // Since there is a global node list, we don't need one 
  //NodeId_t   last;   // Just log first/last+1 entries
  Rate_t     rate;     // Bandwidth
  Rate_t     basicRate;// Rate for sending control packets
  Rate_t     dataRate; // Rate for sending data packets
  Time_t     delay;    // Propogation delay (1us if detail NONE or PARTIAL)
  IPAddr_t   firstIP;  // First IP Address
  MACAddr    firstMac; // First MAC Address
  MACAddr    lastMac;  // Last MAC address in consecutive list.
  Detail_t   detail;
  Propagation*  propagation;   // radio propagation model
  double     csThreshold;      // carrier sense threshold, mW
  double     rxThreshold;      // rx threshold, mW
  double     snrThreshold;     // SNR or SINR threshold, dB
  // The interfaces in the lan are stored here.  The first
  // (lastMac-firstMac) entries are consecutive MAC addresses, so
  // we can look up by just indexing.  Ones after that are
  // not in order, and must be searched.
  IFVec_t    ifaceVec; // For ease of MAC->IFace mapping

private:
  void ConstructorHelper(
                         Count_t,          // Node count
                         IPAddr_t, Mask_t, // IP addr start, mask
                         L4Protocol* l4,   // Layer 4 protocol to bind to each 
                         Rate_t r, Time_t dly,
			 Detail_t d, bool bootstrap=false );
protected:
  // Animation helpers
  void    WirelessTxStart(Node*, InterfaceWireless*, Node*, Meters_t, Count_t);
  void    WirelessTxEnd(Node*, InterfaceWireless*, Node*, Meters_t, Count_t);

  LinkState_t state;
};

#endif
