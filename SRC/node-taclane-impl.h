#ifndef __node_taclane_impl_h__
#define __node_taclane_impl_h__

#include "node-real.h"
#include "interface.h"
#include "macaddr.h"

//Need to manually setup Security Associations
//Need to manually setup Routing


class TACLANENode;
class L2Proto;
class Interface;

class NodeTACLANEImpl : public NodeReal
{
public:
  NodeTACLANEImpl(TACLANENode*, IPAddr_t, IPAddr_t);
  NodeImpl* Copy() const;
  // Override the add interface methods to be sure we don't get more than two
  virtual Interface* AddInterface(const L2Proto&, bool bootstrap=false);
  virtual Interface* AddInterface(const L2Proto&,
                                  IPAddr_t, Mask_t,
                                  MACAddr = MACAddr::Allocate(),
				  bool bootstrap = false);
  virtual Interface* AddInterface(const L2Proto&,const Interface&,
				  IPAddr_t, Mask_t, 
				  MACAddr = MACAddr::Allocate(),
				  bool bootstrap = false);
private:
  Interface* ptif;
  Interface* ctif;
};

#endif
