// Georgia Tech Network Simulator - String Tokenizer
// George F. Riley.  Georgia Tech, Spring 2002
// Found on WWW, author unknown

#ifndef __stringtok_h__
#define __stringtok_h__

#include <string>
#include <cstring>    // for strchr
#include <list>
#include <map>

class StringSep {
public:
  StringSep(const std::string& st, char sp) : str(st), sep(sp) { }
  std::string str;
  char        sep;
};

// Code for tokenizing the lines
namespace stparse {
    inline bool
    isws (char c, char const * const wstr)
    {
        return (strchr(wstr,c) != NULL);
    }
}


template <typename Container>
void
stringparse (Container &l, std::string const &s, char const * const sep)
{
  const char* ws = " \t\n";

    const std::string::size_type  S = s.size();
          std::string::size_type  i = 0;

    while (i < S) {
        // eat leading whitespace
        while ((i < S) && (stparse::isws(s[i],ws)))  ++i;
        if (i == S)  return;  // nothing left but WS

        // See if comment.  If so ignore rest of line
        if (s[i] == '/' && (i+1)<S &&s[i+1] == '/') return;

        // find end of word
        std::string::size_type  j = i;
        while ((j < S) && (!stparse::isws(s[j],sep)))  ++j;
        
        char thissep = 0;
        if  (j < S) thissep = s[j];
        std::string::size_type nexti = j + 1;
        // if the trailing sep is a space, look ahead for non-space separator
        if  (thissep == ' ')
          {
            std::string::size_type k = j;
            while((k < S) && s[k] == ' ') ++k;
            if (k < S)
              {
                if (stparse::isws(s[k], sep))
                  {
                    thissep = s[k];
                    nexti = k + 1;
                  }
              }
          }
        // add word
        l.push_back(StringSep(s.substr(i,j-i),thissep));

        // set up for next loop
        i = nexti;
    }
}
#endif

