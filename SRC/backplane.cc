// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: backplane.cc 456 2006-01-20 17:37:47Z riley $



// Implementation of the Distributed Simulated Backplane
// George F. Riley, Georgia Tech, Summer 2000

#include <iostream>
#include <map>
#include <vector>
#include <algorithm>

#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "common-defs.h"
#include "backplane.h"
#include "cqptr.h"
#include "hex.h"

#define DEBUG_MASK 0x00
#include "debug.h"

using namespace std;

#if defined(Linux) 
extern "C" int usleep(unsigned int useconds);
extern "C" int gethostname(char*, size_t len);
#define NETCAST socklen_t*
#endif

#if defined(CYGWIN_NT_5_1)
extern "C" unsigned int usleep(unsigned int useconds);
extern "C" int gethostname(char*, size_t len);
#define NETCAST socklen_t*
#endif

#if defined(SunOS)
extern "C" int usleep(useconds_t useconds);

#if !defined(_XPG4_2)
extern int gethostname(char *, int);
#endif

#define NETCAST socklen_t*
#endif

#if defined(Darwin)
extern "C" int usleep(unsigned int useconds);
//extern "C" int gethostname(char*, int);
#define NETCAST socklen_t*
#endif

class DSimInfo;
class ProtoInfo;
class ItemInfo;


#define BYTES_PER_WORD 4

// Define maps for string->proto and string->item

typedef vector<ProtoInfo*>       ProtoVec_t;
typedef ProtoVec_t::iterator     ProtoVec_it;

typedef vector<ItemInfo*>        ItemVec_t;
typedef ItemVec_t::iterator      ItemVec_it;

// Define a vector of DSimInfo instances, for consolidation
typedef vector<DSimInfo*>        DSimVec_t;
typedef DSimVec_t::iterator      DSimVec_it;

typedef vector<nodeid_t>         IDVec_t;
typedef IDVec_t::iterator        IDVec_it;

// Define a base class for Item lists, which has the Name() member
class BaseInfo {
public :
  BaseInfo(const char* n);
  virtual ~BaseInfo();
  const char* Name();
  virtual isize_t Size() { return m_size;}
  virtual void    Size(isize_t s) { m_size = s;}
private :
  char*        m_name; // Name of item
  isize_t      m_size; // Size of item (bits)
public :
  IDVec_t      m_required; // Vector of nodes that "require" this feature
  IDVec_t      m_supported;// Vector of nodes that "support" this feature
  int          m_flags;    // Flags (Required, optional, etc.)
};

// Define the class for info items
class ItemInfo : public BaseInfo {
  public :
    ItemInfo(char*);
    ItemInfo(char*, int);
    virtual ~ItemInfo();
  public :
    ItemExportCb_t    m_ExportCb;      // Export Callback function
    void*             m_ExportCbData;  // Blind pointer to pass back to caller
    ItemImportCb_t    m_ImportCb;      // Import callback function
    void*             m_ImportCbData;  // Blind pointer to pass back to caller
    ItemDefaultCb_t   m_DefaultCb;     // Import callback function
    void*             m_DefaultCbData; // Blind pointer to pass back to caller
};


class ProtoInfo : public BaseInfo {
  public :
    ProtoInfo(char*);
    ProtoInfo(ProtoInfo&);
    virtual ~ProtoInfo();
    ItemInfo* AddItem(char*, int, int,
                      ItemExportCb_t,  void*,
                      ItemImportCb_t,  void*,
                      ItemDefaultCb_t, void*);
    ItemInfo* AddItem(ItemInfo*);
    ItemInfo* ExistingItem(const char*);
    ItemInfo* ExistingItem(itemid_t);
    virtual isize_t Size();
  public :
    ExportQueryCb_t   m_ExportQueryCb; // Export Query Callback function
    void*             m_ExportCbData;  // Blind pointer to pass back to caller
    ItemVec_t         m_Items;         // List of items for this proto
};


class DSimInfo {
public :
  DSimInfo(nodeid_t myid);
  ~DSimInfo();
  ProtoInfo* AddProto(char*, int, ExportQueryCb_t, void*);// Add a new protocol
  ProtoInfo* AddProto(ProtoInfo*);  // Add a new protocol
  ProtoInfo* ExistingProto(const char*);  // Find existing protocol
  ProtoInfo* ExistingProto(itemid_t);   // Find existing protocol
public :
  ProtoVec_t m_Protos; // List of known protocols
public :
  nodeid_t         m_myid;
  static DSimInfo* instance;
};

// BaseInfo Methods
BaseInfo::BaseInfo(const char* n) : m_size(0), m_flags(0)
{
  m_name = strdup(n);
}

BaseInfo::~BaseInfo()
{
}

const char* BaseInfo::Name()
{
  return m_name;
}


// DSimInfo data and methods
DSimInfo* DSimInfo::instance; // One and only instance of DSIM

DSimInfo::DSimInfo(nodeid_t myid ) : m_myid(myid)
{
}

DSimInfo::~DSimInfo()
{
}


ProtoInfo* DSimInfo::AddProto(char* p, int f, ExportQueryCb_t c, void* pd)
{
  //ProtoVec_it i;
ProtoInfo* pInfo;

  pInfo = ExistingProto(p);
  if (pInfo)
    {
      cout << "Ignoring duplicate protocol " << p << endl;
      return pInfo;
    }
  DEBUG(0,(cout << "Adding protocol " << p));
  pInfo = new ProtoInfo(p);
  pInfo->m_ExportQueryCb = c;
  pInfo->m_ExportCbData = pd;
  pInfo->m_flags = f;
  m_Protos.push_back(pInfo);
  return(pInfo);
}

ProtoInfo* DSimInfo::AddProto(ProtoInfo* pProto)
{
  m_Protos.push_back(pProto);
  return pProto;
}

ProtoInfo* DSimInfo::ExistingProto(const char* p)
{
ProtoVec_it i;

  for (i = m_Protos.begin(); i != m_Protos.end(); i++)
    {
      if (!strcmp((*i)->Name(), p)) return *i; // Found existing
    }
  return NULL;
}

ProtoInfo* DSimInfo::ExistingProto(itemid_t i)
{
  if (i >= m_Protos.size()) return(NULL);
  if (i < 0) return(NULL);
  return(m_Protos[i]);
}

// ProtoInfo methods
ProtoInfo::ProtoInfo(char* n) : BaseInfo(n),
  m_ExportQueryCb(0), m_ExportCbData(0)
{
}

ProtoInfo::ProtoInfo(ProtoInfo& p)
  : BaseInfo(p.Name()),
    m_ExportQueryCb(p.m_ExportQueryCb),
    m_ExportCbData(p.m_ExportCbData)
{

}

ProtoInfo::~ProtoInfo()
{
}

ItemInfo* ProtoInfo::AddItem(char* p, int f, int s,
                             ItemExportCb_t ecb,  void* peData,
                             ItemImportCb_t icb,  void* piData,
                             ItemDefaultCb_t dcb, void* pdData)
{
ItemInfo* pInfo;

  pInfo = ExistingItem(p);
  if (pInfo)
    {
      cout << "Protocol " << Name() << " ignoring duplicate item "<< p << endl;
      if ((unsigned int)s > pInfo->Size()) // Reset size in case different
        {
          pInfo->Size(s);
        }
      return pInfo;
    }
  DEBUG0((cout << "Adding item " << p << endl));
  pInfo = new ItemInfo(p);
  pInfo->Size(s);
  pInfo->m_flags = f;
  pInfo->m_ExportCbData = peData;
  pInfo->m_ExportCb = ecb;
  pInfo->m_ImportCbData = piData;
  pInfo->m_ImportCb = icb;
  pInfo->m_DefaultCbData = pdData;
  pInfo->m_DefaultCb = dcb;
  m_Items.push_back(pInfo);
  return(pInfo);
}

ItemInfo* ProtoInfo::AddItem(ItemInfo* pItem)
{
  m_Items.push_back(pItem);
  return(pItem);
}

ItemInfo* ProtoInfo::ExistingItem(const char* p)
{
ItemVec_it i;

  for (i = m_Items.begin(); i != m_Items.end(); i++)
    {
      if (!strcmp((*i)->Name(), p)) return *i; // Found existing
    }
  return NULL;
}

ItemInfo* ProtoInfo::ExistingItem(itemid_t i)
{
  if (i >= m_Items.size()) return(NULL);
  if (i < 0) return(NULL);
  return(m_Items[i]);
}

isize_t ProtoInfo::Size()
{
ItemVec_it  iItem;
isize_t s = 0;

 for (iItem = m_Items.begin(); iItem != m_Items.end(); iItem++)
   {
     s += (sizeof(itemid_t)+sizeof(unsigned int))*8; // ItemId, Lth
     isize_t b = (*iItem)->Size();
     isize_t w = (b+31)/32; // Round up
     s += (w * 32);
   }
 return s;
}

// ItemInfo methods
ItemInfo::ItemInfo(char* n) : BaseInfo(n),
  m_ExportCb(0), m_ExportCbData(0),
  m_ImportCb(0),  m_ImportCbData(0),
  m_DefaultCb(0), m_DefaultCbData(0)
{
}

ItemInfo::ItemInfo(char* n, int f) : BaseInfo(n),
  m_ExportCb(0), m_ExportCbData(0),
  m_ImportCb(0),  m_ImportCbData(0),
  m_DefaultCb(0), m_DefaultCbData(0)
{
}

ItemInfo::~ItemInfo()
{
}


// Helper routines for the global consensus portion
// Mostly socket stuff
void SetNewInstance(DSimInfo* i);

typedef struct sockaddr_in PeerInfo_t;

typedef struct {
  nodeid_t    which; // Who am I
  PeerInfo_t  info;  // IP Address and port
} PeerReply_t;

typedef struct {
  u_long totallth;
  u_long numberpeers;
} PeerInfoHdr_t;

int           MasterSock;// Master socket for initialization
int           PeerSock;  // Peer socket for accepting connections from others
PeerInfo_t*   pPeerInfo; // Info for all peers (including the sockets)
int*          pPeers;    // Sockets for communicating w/ peers
PeerInfoHdr_t hdrinfo;   // Header of reply from master
char          LinkName[255];

#define LINKNAME "./testdsim.sockaddr.port"

int PutCountedItem(const int s, const char* b, const int l)
{ // Put a data item on a tcp socket, 32 bit item length followed by data
int a;
int il;

  il = htonl(l);
  a = write(s, (char*)&il, sizeof(il));
  if (a != sizeof(il))
    {
      cout << "Socket error on write, socket " << s << endl;
      exit(2);
    }
  if (l != 0)
    {
      a = write(s, b, l);
      if (a != l)
        {
          cout << "Socket error on write, socket " << s << endl;
          exit(2);
        }
    }
  return l;
}

int GetCountedItem(int s, char* b, int l)
{ // Get a data item from a tcp socket.  32 bit length followe by the data
  // s is the socket, b is the bufffer, l is the length of the buffer
char* pCh;
unsigned long il; // Item length
unsigned int  t;  // Total length so far
int           a;  // Actual on this read

  // First get the 32 bit count
  pCh = (char*)&il;
  t = 0;
  while(t < sizeof(il))
    {
      a = read(s, pCh, sizeof(il) - t);
      if (a <= 0)
        {
          cout << "Socket read error on socket " << s << endl;
          exit(1);
        }
      t += a;
      pCh += a;
    }
  il = ntohl(il); // To local byte order
  if (il == 0) return(0); // Got 0 length item
  if (il > (unsigned long)l)
    {
      cout << "Insufficient buffer size " << l
           << " for item length " << il << endl;
      exit(1);
    }
  pCh = b;
  t = 0;
  while(t < il)
    {
      a = read(s, pCh, il - t);
      if (a <= 0)
        {
          cout << "Socket read error on socket " << s << endl;
          exit(1);
        }
      t += a;
      pCh += a;
    }
  return(il);
}

void MakeLinkName()
{
char* pHome;
 pHome = getenv("HOME");
 if (pHome == NULL)
   {
     pHome = getcwd(NULL, 255);
     cout << "Home not defined, using " << pHome << endl;
   }
 // Create the linkfile name
 strcpy(LinkName, pHome);
 if (LinkName[strlen(LinkName)-1] != '/')
   { // insure trailing /
     strcat(LinkName, "/");
   }
 strcat(LinkName, LINKNAME);
}

void CreatePeerSock( nodeid_t myid, PeerReply_t& reply)
{
struct hostent* pHE;
sockaddr_in     addr;
int             addrlen;
char            work[255];

  gethostname(work, sizeof(work)); // Get my host
  pHE = gethostbyname(work);       // And get my IP address
  reply.info.sin_family = AF_INET;
  memcpy(&reply.info.sin_addr.s_addr, pHE->h_addr, pHE->h_length);

  addr.sin_port = 0;
  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_family = AF_INET;
  PeerSock = socket(AF_INET, SOCK_STREAM, 0);
  if (bind(PeerSock, (struct sockaddr*)&addr, sizeof(addr)) < 0)
    {
      cout << "CreatePeerSock - bind failed" << endl;
      exit(2);
    }
  addrlen = sizeof(addr);
  getsockname(PeerSock, (struct sockaddr*)&addr, (NETCAST)&addrlen);
  DEBUG0((cout << "Peer " << myid << " using port " << addr.sin_port << endl));
  reply.info.sin_port = addr.sin_port;
  reply.which = myid;
  listen(PeerSock, 5); // Will accept connections from others on this one
}

void CreatePeerConnections(nodeid_t myid, unsigned int npeers)
{
unsigned int i;

  for (i = 0; i < myid; i++)
    { // Connect to lower numbers
      pPeers[i] = socket(AF_INET, SOCK_STREAM, 0);
      DEBUG0((cout << "Host " << myid << " connecting to " <<  i << endl));
      if (connect(pPeers[i],
              (struct sockaddr*)&pPeerInfo[i], sizeof(pPeerInfo[i])))
        {
          cout << "Host " << myid << " connection to " << i
               << " failed" << endl;
          exit(1);
        }
      DEBUG0((cout << "Host " <<  myid << " connected to " << i << endl));
      write(pPeers[i], &myid, sizeof(myid)); // Tell it who we are
    }
  for (i = myid + 1; i < npeers; i++)
    { // Accept from higher ones
      struct sockaddr_in addr;
      int                addrlen = sizeof(addr);
      int                peerid;
      int                s;

      DEBUG0(( cout << "Host " << myid << " accepting" << endl));
      s = accept(PeerSock, (struct sockaddr*)&addr, (NETCAST)&addrlen);
      read(s, &peerid, sizeof(peerid));
      DEBUG0((cout << "Host " << myid
              << " got connection from " <<  peerid << endl));
      pPeers[peerid] = s;
    }
}

ItemInfo* ConsolidateItem(
  ProtoInfo*  pOldInfo,  // Original Protocol
  ProtoInfo*  pNewInfo,  // New Protocol
  ItemInfo*   pItem)     // Item to check
{
  ItemInfo* pMyItem = NULL;
  DEBUG(1,(cout << "Consolidating Item " << pItem->Name() << endl));
  if (pOldInfo)
    pMyItem = pOldInfo->ExistingItem(pItem->Name());
  if (pMyItem)
    { // Already defined on this system
      //delete pItem;
      DEBUG(1,(cout << "Already defined" << endl));
      return pNewInfo->AddItem(pMyItem);
    }
   // We don't have it, use the one passed in
  DEBUG(1,(cout << "Not on this system" << endl));
  return pNewInfo->AddItem(pItem);
}

ProtoInfo* ConsolidateProtocol(
  DSimInfo*  pOldInfo,  // Original Proto/Item list
  DSimInfo*  pNewInfo,  // New Proto/Item List
  ProtoInfo* pProto)    // Proto to check
{
  ProtoInfo* pMyProto;

  pMyProto = pOldInfo->ExistingProto(pProto->Name());
  if (pMyProto)
    { // Already defined on this system
      ProtoInfo* pNewProto = new ProtoInfo(*pMyProto);
      return pNewInfo->AddProto(pNewProto);
    }
   // We don't have it, use the one passed in
  return pNewInfo->AddProto(pProto);
}



int  ValidateBase(BaseInfo* pInfo, char* pPrompt, unsigned int npeers)
{
int      errors = 0;
int      *supported = new int[npeers];
unsigned int k;

  if (pInfo->m_required.size() > 0)
    {
      if (pInfo->m_supported.size() != npeers)
        { // OOps!  Missing support
          errors++;
          cout << pPrompt << " is required by: " << pInfo->Name();
          for (k = 0; k < pInfo->m_required.size(); k++)
            {
              cout << pInfo->m_required[k] << " ";
            }
          cout << endl << "but not supported by: ";
          for (k = 0; k < npeers; k++) supported[k] = 0;
          for (k = 0; k < pInfo->m_supported.size(); k++)
            {
              supported[pInfo->m_supported[k]]++;
            }
          for (k = 0; k < npeers; k++)
            {
              if (!supported[k])
                cout << k << " ";
            }
          cout << endl;
        }
    }
  return errors;
}

void ValidateProtocols( unsigned int npeers)
{ // Insure all required protocols are supported
unsigned int i;
unsigned int j;

int      errors = 0;

 for (i = 0; i < DSimInfo::instance->m_Protos.size(); i++)
   {
     ProtoInfo* pProto = DSimInfo::instance->m_Protos[i];
     // Check for all required supported
     errors += ValidateBase(pProto, "Protocol", npeers);

     for (j = 0; j < pProto->m_Items.size(); j++)
       {
         ItemInfo* pItem = pProto->m_Items[j];
         errors += ValidateBase(pItem, "Item", npeers);
       }
   }
}

void ReceiveConsolidatedProtocols(nodeid_t id, int s)
{ // Used by slaves, receives consolidated proto info from master
char b[4096];
int  flags;

  DSimInfo* pNew = new DSimInfo(0);
  DSimInfo* pOld = DSimInfo::instance;
  SetNewInstance(pNew);
  while(1)
    {
      int l = GetCountedItem(s, b, sizeof(b));
      if (!l) break; // Done, no more protocols
      b[l] = 0; // Terminate string
      int k = GetCountedItem(s, (char*)&flags, sizeof(flags)); // flags
      if (k != sizeof(flags))
        {
          cout << "ReceiveConsolidatedProto error, flags length " << k << endl;
          exit(1);
        }
      flags = ntohl(flags); // To local byte order
      ProtoInfo* pNewProto = new ProtoInfo(b);
      pNewProto->m_flags = flags;
      ProtoInfo* pOldProto = pOld->ExistingProto(b);
      ProtoInfo* pProto = ConsolidateProtocol(pOld, pNew, pNewProto);
      while(1)
        {
          k = GetCountedItem(s, b, sizeof(b)); // Get Item name
          if (!k) break; // No more items
          b[k] = 0; // Terminate string
          int k = GetCountedItem(s, (char*)&flags, sizeof(flags)); // flags
          if (k != sizeof(flags))
            {
              cout << "ReceiveConsolidatedProto error, flags length "
                   << k << endl;
              exit(1);
            }
          flags = ntohl(flags); // To local byte order
          ItemInfo* pNewItem = new ItemInfo(b);
          pNewItem->m_flags = flags;
          ConsolidateItem(pOldProto, pProto, pNewItem);
        }
    }
}

void ReceiveProtocols(nodeid_t id, int s)
{ // Used by master, receives proto info from slaves
char b[4096];
int  t = 0;
int  flags;

  while(1)
    {
      int l = GetCountedItem(s, b, sizeof(b));
      if (!l) break;
      b[l] = 0; // Terminate string
      int k = GetCountedItem(s, (char*)&flags, sizeof(flags)); // flags
      if (k != sizeof(flags))
        {
          cout << "ReceiveProto error, flags length " << k << endl;
          exit(1);
        }
      flags = ntohl(flags); // To local byte order
      ProtoInfo* pProto = DSimInfo::instance->ExistingProto(b);
      if (!pProto)
        { // Add new one
          pProto = (ProtoInfo*)RegisterProtocol(b, flags, NULL, NULL);
          DEBUG(1,(cout << "Master added protocol " << b << endl));
        }
      pProto->m_supported.push_back(id);
      if (flags & PROTO_REQUIRED)
        {
          pProto->m_required.push_back(id); // And note required
        }
      int  i = 0;
      while(1)
        {
          k = GetCountedItem(s, b, sizeof(b)); // Get Item name
          if (!k) break;
          b[k] = 0; // Terminate string
          int k = GetCountedItem(s, (char*)&flags, sizeof(flags)); // flags
          if (k != sizeof(flags))
            {
              cout << "ReceiveProto error, flags length " << k << endl;
              exit(1);
            }
          flags = ntohl(flags); // To local byte order
          ItemInfo* pItem = pProto->ExistingItem(b);
          if (!pItem)
            { // Add new one
              pItem = (ItemInfo*)RegisterItem(pProto, b, flags, 0,
                           NULL, NULL, NULL, NULL, NULL, NULL);
              DEBUG0((cout << "Master added item " << b << endl));
            }
          pItem->m_supported.push_back(id);
          if (flags & ITEM_REQUIRED)
            {
              pItem->m_required.push_back(id);
            }
          if (flags & ITEM_BYTE_SWAP)
            {
              pItem->m_flags |= ITEM_BYTE_SWAP; // Any BS means all BS
            }
          i++;
        }
      DEBUG0((cout << "Master Skipped " << i << " items " << endl));
      t++;
    }
  DEBUG0((cout << "Master skipped " << t << " protos" << endl));
}

void DumpProtocols(int s)
{
unsigned int i;
unsigned int j;

 for (i = 0; i < DSimInfo::instance->m_Protos.size(); i++)
   {
     ProtoInfo* pProto = DSimInfo::instance->m_Protos[i];
     PutCountedItem(s, pProto->Name(), strlen(pProto->Name())+1);
     PutCountedItem(s, (char*)&pProto->m_flags, sizeof(pProto->m_flags));
     for (j = 0; j < pProto->m_Items.size(); j++)
       {
         ItemInfo* pItem = pProto->m_Items[j];
         PutCountedItem(s, pItem->Name(), strlen(pItem->Name())+1);
         PutCountedItem(s, (char*)&pItem->m_flags, sizeof(pItem->m_flags));
       }
     PutCountedItem(s, NULL, 0);
   }
 PutCountedItem(s, NULL, 0);
}

void PrintProtocols(void)
{ // Debug...
unsigned int i;
unsigned int j;

 for (i = 0; i < DSimInfo::instance->m_Protos.size(); i++)
   {
     ProtoInfo* pProto = DSimInfo::instance->m_Protos[i];
     cout << "Protocol " << pProto->Name() << endl;
     for (j = 0; j < pProto->m_Items.size(); j++)
       {
         ItemInfo* pItem = pProto->m_Items[j];
         cout << "  Item " << pItem->Name()
              << " lth " << pItem->Size() << endl;
       }
   }
}

int Master(unsigned int npeers)
{
int         s;
sockaddr_in addr;
int         addrlen;
char        work[255];
char        hn[255];
int         ndflag = 1;
PeerReply_t reply;
//DSimVec_t   DSV; // Keep a list of the protos/items from each peer

unsigned int i;
unsigned int j;

  cout << "Hello from Master" << endl;
  // Start by noting "required" and "supported" for each Proto/Item
  for (i = 0; i < DSimInfo::instance->m_Protos.size(); i++)
    {
      ProtoInfo* pProto = DSimInfo::instance->m_Protos[i];
      pProto->m_supported.push_back(0);
      if (pProto->m_flags & PROTO_REQUIRED)
        {
          pProto->m_required.push_back(0);
        }
      for (j = 0; j < pProto->m_Items.size(); j++)
        {
          ItemInfo* pItem = pProto->m_Items[j];
          pItem->m_supported.push_back(0);
          if (pItem->m_flags & ITEM_REQUIRED)
            {
              pItem->m_required.push_back(0);
            }
        }
    }

  hdrinfo.numberpeers = npeers;
  CreatePeerSock(0, reply);

  gethostname(hn, sizeof(hn));
  DEBUG0((cout << "Master, hostname is " << hn << endl));

  MakeLinkName();
  unlink(LinkName); // Just to be sure no previous one
  addr.sin_port = 0;
  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_family = AF_INET;
  MasterSock = socket(AF_INET, SOCK_STREAM, 0);
  if (bind(MasterSock, (struct sockaddr*)&addr, sizeof(addr)) < 0)
    {
      cout << "Master - bind failed" << endl;
      exit(2);
    }
  addrlen = sizeof(addr);
  getsockname(MasterSock, (struct sockaddr*)&addr, (NETCAST)&addrlen);
  DEBUG0((cout << "Master using port " << ntohs(addr.sin_port) << endl));
  sprintf(work, "%s:%d", hn, ntohs(addr.sin_port));
  if (symlink(work, LinkName) != 0)
    {
      cout << "SymLink failed" << endl;
      exit(4);
    }
  pPeerInfo = new PeerInfo_t[npeers]; // Get peers array
  pPeers = new int[npeers];           // An array to save sockets for replies

  pPeerInfo[0].sin_family = AF_INET;
  pPeerInfo[0].sin_addr.s_addr = reply.info.sin_addr.s_addr;
  pPeerInfo[0].sin_port = reply.info.sin_port;

  listen(MasterSock, 5);
  for (i = 1; i < npeers; i++)
    {
      addrlen = sizeof(addr);
      s = accept(MasterSock, (struct sockaddr*)&addr, (NETCAST)&addrlen);
      if(0)setsockopt(s, IPPROTO_TCP, TCP_NODELAY,
                      (char*)&ndflag, sizeof(ndflag));
      DEBUG0((cout << "Got connection from peer" << endl));
      if(GetCountedItem(s, (char*)&reply, sizeof(reply)) != sizeof(reply))
        {
          cout << "Master failed reading peer reply" << endl;
          exit(1);
        }
      if (reply.which >= npeers)
        {
          cout << "Peerid " << reply.which << " out of range" << endl;
          exit(2);
        }
      DEBUG0((cout << "Master heard from peer " << reply.which
              << " addr " << inet_ntoa(reply.info.sin_addr)
              << " port " << reply.info.sin_port << endl));

      pPeers[reply.which] = s; // Save socket
      memcpy(&pPeerInfo[reply.which], &reply.info, sizeof(reply.info));
      // Now get the protocol info from this peer
      // Make a DSimInfo pointer to log the protos/items in
      //      DSimInfo* pDS = new DSimInfo(reply.which);
      //DSV.push_back(pDS);
      //SetNewInstance(pDS);
      ReceiveProtocols(reply.which, s);
    }
  unlink(LinkName); // Remove for next time
  close(MasterSock);

  // Now validate the protocols/items
  ValidateProtocols(npeers);

  // Master now has a consolidated picture of the entire simulation
  // Time to send it back to peers

  // Inform peers of the consolidated Proto/Item set
  for (i = 1; i < npeers; i++)
    {
      DEBUG(1,(cout << "Master informing peer " << i
               << " of protocols" << endl));
      DumpProtocols(pPeers[i]);
    }

  // Now inform all peers of their peers
  hdrinfo.numberpeers = npeers;
  hdrinfo.totallth = sizeof(PeerInfoHdr_t) + sizeof(PeerInfo_t)*npeers;
  for (i = 1; i < npeers; i++)
    {
      DEBUG0((cout << "Master informing peer " << i << endl));
      PutCountedItem(pPeers[i], (char*)&hdrinfo, sizeof(hdrinfo));
      PutCountedItem(pPeers[i], (char*)pPeerInfo, sizeof(PeerInfo_t)*npeers);
      close(pPeers[i]);
    }
  // Debug, print them out
  DEBUG(2, PrintProtocols());

  DEBUG0((cout << "Master creating peer connections" << endl));
#ifdef NEED_N_N_CONNECTIONS
  CreatePeerConnections(0, npeers);
#endif
  return(1);
}


int Slave(nodeid_t myid)
{
sockaddr_in addr;
char        work[255];
struct hostent* pHE;
int         lth = 0;
char*       pCh;
int         ndflag = 1;
PeerReply_t reply;

  CreatePeerSock(myid, reply);

  MakeLinkName();
  while(lth <= 0)
    { // See if the port link file is available
      lth = readlink(LinkName, work, sizeof(work));
      if (lth > 0)
        { // Got it
          work[lth] = 0;
        }
      else
        {
          usleep(100000);
        }
    }
  // Now decode the link values
  pCh = strtok(work, ":");
  pHE = gethostbyname(pCh);
  if (pHE == NULL)
    {
      cout << "Slave can find host " << pCh << endl;
      exit(1);
    }
  pCh = strtok(NULL, ":"); // points to the port number

  MasterSock= socket( AF_INET, SOCK_STREAM, 0);
  addr.sin_family = AF_INET;
  addr.sin_port = htons(atoi(pCh));
  memcpy(&addr.sin_addr, pHE->h_addr, pHE->h_length);
  cout << "Connecting to master on addr " << addr.sin_addr.s_addr
       << " , port " << atoi(pCh) << endl;

  int tries = 0;
  int success = 0;
  while(tries < 10 && !success)
    {
      if (connect(MasterSock, (struct sockaddr*)&addr, sizeof(addr)) < 0)
        {
          tries++;
          cout << "Trying again, try " << tries << endl;
          sleep(1);
        }
      else success = 1;
    }
  if (!success)
    {
      cout << "Slave can't connect to " << work << endl;
      exit(3);
    }
  if(0)setsockopt(MasterSock, IPPROTO_TCP, TCP_NODELAY,
             (char*)&ndflag, sizeof(ndflag));
  DEBUG0((cout << "Slave connected to host " << work << endl));
  // Tell the master who we are
  PutCountedItem(MasterSock, (char*)&reply, sizeof(reply));
  // Tell the master about all of our protocols and data items
  DumpProtocols(MasterSock);

  // Wait for the consolidated protocols/items
  ReceiveConsolidatedProtocols(myid, MasterSock);

  // Debug, print them out
  DEBUG(2, PrintProtocols());

  // Master will reply when all peers heard from
  GetCountedItem(MasterSock, (char*)&hdrinfo, sizeof(hdrinfo));
  pPeerInfo = new PeerInfo_t[hdrinfo.numberpeers];
  GetCountedItem(MasterSock, (char*)pPeerInfo, hdrinfo.totallth - sizeof(PeerInfoHdr_t));
  for (u_long i = 0; i < hdrinfo.numberpeers; i++)
    {
      DEBUG0((cout << "Peer " <<  inet_ntoa(pPeerInfo[i].sin_addr)
              << " port " << pPeerInfo[i].sin_port));
    }
  pPeers = new int[hdrinfo.numberpeers]; // make the peers sockets
#ifdef NEED_N_N_CONNECTIONS
  CreatePeerConnections(myid, hdrinfo.numberpeers);
#endif
  return(1);
}

void SetNewInstance(DSimInfo* i)
{ // Used by master to consolidate all the various participants
  DSimInfo::instance = i;
}

// C Callable subroutines

// Initialize the DSim backplane
DSHandle_t InitializeDSim(nodeid_t myid)
{
  if (DSimInfo::instance)
    {
      cout << "Ignoring second call to InitializeDSim" << endl;
    }
  else
    {
      DSimInfo::instance = new DSimInfo(myid);
    }
  return((void*)DSimInfo::instance);
}

// Register a new protocol
PRHandle_t RegisterProtocol(char* pProto, int flags,
                            ExportQueryCb_t ecb, void* p)
{
  if (!DSimInfo::instance)
    {
      cout << "Missing call to InitializeDSim" << endl;
      exit(0);
    }
  cout << "Registering proto " << pProto << endl;
  return DSimInfo::instance->AddProto(pProto, flags, ecb, p);
}


// Regster a new data item
ITHandle_t RegisterItem(PRHandle_t h, char* pItem,
                        int flags, isize_t s,
                        ItemExportCb_t ecb, void* ep,
                        ItemImportCb_t icb, void* ip,
                        ItemDefaultCb_t dcb, void* dp)
{
ProtoInfo* pInfo = (ProtoInfo*)h;

  return pInfo->AddItem(pItem, flags, s, ecb, ep, icb, ip, dcb, dp);
}


void RegistrationComplete(nodeid_t myid, unsigned int npeers)
{ // Time to exchange info with peers
  if (myid)
    Slave(myid);
  else
    Master(npeers);
}

isize_t InquireMessageSize()
{
ProtoVec_it iProto;
//ItemVec_it  iItem;
DSimInfo*   dsi = (DSimInfo*)GetDSimHandle();
isize_t s = 0;

 for (iProto = dsi->m_Protos.begin(); iProto != dsi->m_Protos.end(); iProto++)
   {
     ProtoInfo* pri = *iProto;
     s += sizeof(itemid_t)*8; // Proto identifier
     s += pri->Size();
   }
 s += 32;       // Slight fudge since export reports overflow when full
 return s / 8; // Reported sizes are in bits!
}

static void DefaultSingleItem(char* pB, ProtoInfo* pProto, itemid_t ind)
{ // Call the default CB if it is defined
  if (pProto->m_Items[ind]->m_DefaultCb)
    pProto->m_Items[ind]->m_DefaultCb(
        pB,
        pProto->m_Items[ind]->m_DefaultCbData);
}

static void DefaultAllItems(char* pB, ProtoInfo* pProto)
{
  for (itemid_t i = 0; i < pProto->m_Items.size(); i++)
    {
      DefaultSingleItem(pB, pProto, i);
    }
}


void ExportBaggageProto( // Export an entire protocol of baggage
   itemid_t proto,
   CQInPtr<char>& pC,
   //   char**   ppC,  char* pE,       // Start/End of export buffer
   char**   ppCB, char* pEB)      // Start/End of baggage buffer
{
  //char*     pC = *ppC;
char*     pCB = *ppCB;
CQInPtr<itemid_t> pTargetId(pC.CQ(), pC.Base());
itemid_t* pBaggageId = (itemid_t*)pCB;

  pTargetId.Current(pC.Current());
  *pTargetId++ = proto | 0x80000000; // Set the protocol in the export buffer
  pC += sizeof(itemid_t);
  pCB += sizeof(itemid_t); // Skip proto in baggage buffer
  while(1)
    {
      if (pCB >= pEB) break; // End of baggage buffer
      if (*pBaggageId & 0x80000000) break; // Found new proto, time to exit
      // Must be new item
      DEBUG0((cout << "Found baggage item " << Hex8(*pBaggageId) << endl));
      *pTargetId++ = *pBaggageId++;
      pC += sizeof(itemid_t);
      pCB += sizeof(itemid_t);
      // Get length
      isize_t lth = *pBaggageId++;
      *pTargetId++ = lth;
      pC += sizeof(itemid_t);
      pCB += sizeof(itemid_t);
      // Copy item data
      if (pC.Full())
        { // Export buffer overflow
          cout << "Buffer overflow on ExportBaggageProto" << endl;
          //printf("pC %p pE %p lth %d\n", pC, pE, lth);
          exit(1);
        }
      pC.Memcpy(pCB, lth);
      DEBUG0((cout << "Copying baggage item length " << lth << endl));
      pCB += lth;
      pTargetId.Current(pC.Current());
      pBaggageId = (itemid_t*)pCB;
    }
  //*ppC = pC; // Return advance pointers
  *ppCB = pCB;
}



void ExportBaggageItem( // Export a single item of baggage
   itemid_t proto,
   //char**   ppC,  char* pE,       // Start/End of export buffer
   CQInPtr<char>& pC,
   char**   ppCB, char* pEB)      // Start/End of baggage buffer
{
char*     pCB = *ppCB;
CQInPtr<itemid_t> pTargetId(pC.CQ(), pC.Base());
itemid_t* pBaggageId = (itemid_t*)pCB;
itemid_t  item;
isize_t   lth;

  pTargetId.Current(pC.Current());
  if (*pBaggageId == (0x80000000 | proto))
    { // Need to skip proto id in baggage buffer
      pBaggageId++;
      pCB += sizeof(itemid_t);
    }
  // Copy item
  item = *pBaggageId++;
  lth  = *pBaggageId++;
  DEBUG0((cout << "Exp baggage item " << item << " lth " << endl));
  pCB += sizeof(itemid_t) + sizeof(isize_t);;
  if (pC.Full(sizeof(itemid_t) + sizeof(isize_t) + lth))
    { // Export buffer overflow
      int x = pC.Full(sizeof(itemid_t) + sizeof(isize_t) + lth, 1);
      if(x)
        { // ??HuH...why does this fail?
          cout << "Buffer overflow on ExportBaggageItem, x " << x << endl;
          exit(1);
        }
    }
  *pTargetId = item;
  pTargetId++;
  *pTargetId = lth;
  pTargetId++;
  pC += sizeof(itemid_t) + sizeof(isize_t);
  // Copy item data
  pC.Memcpy(pCB, lth);
  pCB += lth;
  pTargetId.Current(pC.Current());
  pBaggageId = (itemid_t*)pCB;
  DEBUG(2,(cout << "ExportBaggageItem, item " << item
           << " lth " << lth << endl));
  pC.Round(0x03);
  pCB = RoundUpBuffer(*ppCB, pCB, (pEB - *ppCB), 0x03);
  *ppCB = pCB;
}


//CQPtr<char>& operator=(CQPtr<char>& lhs, CQPtr<itemid_t>& rhs)
//{
//  lhs.Current(rhs.Current());
//  return lhs;
//}

int ExportMessage(
                  char* pI,             // Input buffer
                  char* pB, int  LthB,  // Baggage buffer, length
                  CQInPtr<char>& pO)     // Output buffer (CQueue)
{
itemid_t  iProto;
itemid_t  iItem;
DSimInfo*     dsi = (DSimInfo*)GetDSimHandle();
ItemInfo*     pItem;
int           ItemLth;
int           ItemLthBytes;
CQInPtr<char> pC(pO);          // Current
char*         pCB = pB;        // Current baggage pointer
char*         pEB = pB + LthB; // End of baggage buffer
CQInPtr<itemid_t> pId(pO.CQ(), pO.Base());
CQInPtr<unsigned int>pLth(pO.CQ(), pO.Base());
itemid_t* pId1;
itemid_t  NextBaggageProto;
itemid_t  NextBaggageItem;

 if (pCB && (pCB < pEB))
   { // Baggage buffer exists
     pId1 = (itemid_t*)pCB;
     NextBaggageProto = *pId1++;
     NextBaggageProto &= 0x7fffffff;
     NextBaggageItem = *pId1;
     DEBUG0((cout << "NextBaggageProto is " << NextBaggageProto << endl));
   }
 else
   {
     NextBaggageProto = 0xffffffff; // None
     NextBaggageItem  = 0xffffffff; // None
   }
 for (iProto = 0; iProto < dsi->m_Protos.size(); iProto++)
   {
     while(NextBaggageProto < iProto)
       {
         DEBUG(4,(cout << "ExportingBaggageProto, NBP is " << NextBaggageProto
                  << " IProto "  <<  iProto << endl));
         ExportBaggageProto(NextBaggageProto, pC, &pCB, pEB);
         if (pCB < pEB)
           {
             pId1 = (itemid_t*)pCB;
             NextBaggageProto = *pId1++;
             NextBaggageProto &= 0x7fffffff;
             NextBaggageItem = *pId1;
           }
         else
           {
             NextBaggageProto = 0xffffffff; // No more
             NextBaggageItem  = 0xffffffff; // No more
           }
       }
     ProtoInfo* pri = dsi->m_Protos[iProto];
     if( !pri->m_ExportQueryCb(pI,0,0) ) continue;
     DEBUG0((cout << "Working on protocol " << pri->Name() << endl));
     pId.Current(pC.Current());
     *pId = (iProto | 0x80000000); // Protos have high bit set
     pC += sizeof(*pId);
     if(pC.Full()) break; // End of buffer
     for (iItem = 0; iItem < pri->m_Items.size(); iItem++)
       {
         pItem = pri->m_Items[iItem];
         if (pItem->m_ExportCb != NULL)
           { // Callback exists, process this item
             ItemLthBytes = SizeInBytes(pItem->Size());
             pId.Current(pC.Current());
             pC += sizeof(*pId);
             pLth.Current(pC.Current());
             pC += sizeof(unsigned int);
             int ItemLthBytesRounded = (ItemLthBytes + 3) & ~0x03;
             DEBUG(4,(cout << "Exporting item " << iItem
                      << " (" << pItem->Name() << " size " << pItem->Size()
                      << " lthBytes " << ItemLthBytes
                      << endl));

             if (pC.Full(ItemLthBytesRounded))
               {
                 int x = pC.Full(ItemLthBytesRounded, 1); // call again verbose
                 if(x)
                   {
                     cout << "Buffer overflow on ExportMessage2, x "
                          << x << endl;
                     cout << "ItemLthBytesRounded " << ItemLthBytesRounded
                          << endl;
                     pC.DBDump();
                     exit(1);
                   }
               }
             if (ItemLthBytes <= 4)
               { // Callback, put data directly into CQ buffer
                 ItemLth = pItem->m_ExportCb(
                     pI, pC, ItemLthBytes, pItem->m_ExportCbData);
                 pC += ItemLth;
               }
             else
               { // Need a scratch buffer, since callback doesn't know
                 // about CQueues
                 char *scr = new char[ItemLthBytes];
                 ItemLth = pItem->m_ExportCb(
                     pI, scr, ItemLthBytes, pItem->m_ExportCbData);
                 pC.Memcpy(scr, ItemLth);
               }
             *pId = iItem;
             *pLth = (unsigned int)ItemLth;
             //pC = RoundUpBuffer(pO, pC, OLth, 0x3);
             pC.Round(0x3);
             DEBUG(4,(cout << "PC After Round " << pC.Current() << endl));
           }
         else
           { // See if we have a baggage item to export
             if (NextBaggageProto == iProto &&
                 NextBaggageItem == iItem)
               {
                 DEBUG(4,(cout << "PC Before Baggage Export "
                          << pC.Current() << endl));
                 ExportBaggageItem(iProto, pC, &pCB, pEB);
                 DEBUG(4,(cout << "PC After Baggage Export "
                          << pC.Current() << endl));
                 if (pCB < pEB)
                   {
                     pId1 = (itemid_t*)pCB;
                     if ((*pId1) & 0x80000000)
                       { // New baggage proto
                         NextBaggageProto = *pId1++;
                         NextBaggageProto &= 0x7fffffff;
                         NextBaggageItem = *pId1;
                       }
                     else
                       { // new baggage item
                         NextBaggageItem = *pId1;
                       }
                   }
                 else
                   {
                     NextBaggageProto = 0xffffffff; // No more
                     NextBaggageItem  = 0xffffffff; // No more
                   }
               }
           }
       }
   }
 pO.Current(pC.Current()); // Update the "in" ptr of original param
 return 0;
}


int ImportMessage(
                  CQOutPtr<char>& pI,  // Input buffer (CQueue)
                  size_t pIn,          // pIn to use for Empty check
                  char* pO,            // Output buffer,
                  char* pB, int* LthB) // Baggage buffer, length(in/out)
{
CQOutPtr<char> pC(pI);  // Current
char*       pCB = pB;   // Current Baggage
char*       pEB = NULL; // End of baggate
DSimInfo*   pS = (DSimInfo*)GetDSimHandle();
ProtoInfo*  pProto = NULL;
ProtoInfo*  pBaggageProto = NULL;
itemid_t    CurrentProtoId = 0;
ItemInfo*   pItem;
int         ItemLth;
itemid_t    ItemId;
CQOutPtr<itemid_t> pId(pI.CQ(), pI.Base());
CQOutPtr<int> pLth(pI.CQ(), pI.Base());
itemid_t    NextProtoIndex = 0;
itemid_t    NextItemIndex = 0;


  if (pB != NULL)
    { // Baggage buffer exists
      if (!LthB)
        {
          cout << "Missing baggage buffer length" << endl;
          exit(1);
        }
      pEB = pB + (*LthB);
    }
  while(!pC.Empty(pIn))
    {
      if(0)cout << "top of import loop, dbdump, pIn %d " << pIn << endl;
      if(0)pC.DBDump();
      pId.Current(pC.Current());
      while((*pId & 0x80000000) && !pC.Empty(pIn))
        { // New proto
          CurrentProtoId = ((*pId) & 0x7fffffff);
          // Find out if we need defaults for skipped protos
          while ((NextProtoIndex < CurrentProtoId) &&
                 NextProtoIndex < pS->m_Protos.size())
            {
              DefaultAllItems(pO, pS->m_Protos[NextProtoIndex]);
              NextProtoIndex++;
            }
          NextProtoIndex = CurrentProtoId + 1;
          pProto = pS->ExistingProto((*pId) & 0x7fffffff);
          if (pProto == NULL)
            { // Can't find it??
              cout << "Can't find protocol " << ((*pId) & 0xfffffff)
                   << " on Import" << endl;
              exit(1);
            }
          pId++;
          pC.Current(pId.Current());
          if (pC.Empty(pIn)) break; // Ok, just empty protocol
          NextItemIndex = 0;
        }
      if (pC.Empty(pIn)) break; // Ok, just empty protocol
      if (!pProto)
        { // Error! items w/ no proto
          cout << "Missing protocol description on Import, pC "
               << pC.Current() << endl;
          exit(1);
        }
      ItemId = *pId;
      DEBUG0((cout << "Itemid " << ItemId << " checking skipped" << endl));
      // See if any skipped items need defaults
      while((NextItemIndex < ItemId) &&
            NextItemIndex < pProto->m_Items.size())
        {
          DEBUG0((cout << "Defaulting item " << NextItemIndex << endl));
          DefaultSingleItem(pO, pProto, NextItemIndex);
          NextItemIndex++;
        }
      NextItemIndex = ItemId + 1;
      pItem = pProto->ExistingItem(ItemId);
      if (!pItem)
        {
          cout << "Can't find item " << *pId << "(" << Hex8(*pId)
               << " in proto " << pProto->Name() << endl;
          exit(1);
        }
      pId++;
      pLth.Current(pId.Current());
      ItemLth = *pLth;
      pLth++;
      DEBUG0((cout << "ItemID " << ItemId
              << " ItemLth is " << ItemLth << endl));
      pC.Current(pLth.Current());
      if (pC.Empty(pIn))
        {
          cout << "Input buffer overflow in ImportMessage3" << endl;
          //printf("pC %p pI %p LthI %d\n", pC, pI, LthI);
          exit(1);
        }
      DEBUG(2,(cout << "Import processing item " << pItem->Name()
               << " id " << ItemId << endl));

      if (pItem->m_ImportCb)
        { // Import callback exists
          //pItem->m_ImportCb(pC, ItemLth, pO, pItem->m_ImportCbData);
          if (ItemLth <= 4)
            {
              pItem->m_ImportCb(pC, ItemLth, pO, pItem->m_ImportCbData);
              pC += ItemLth;
            }
          else
            { // Need a scratch buffer, since Import does not know CQueues
              char *work = new char[ItemLth];
              pC.Memcpy(work, ItemLth);
              pItem->m_ImportCb(work, ItemLth, pO, pItem->m_ImportCbData);
            }
        }
      else
        { // Item is baggage, copy it to baggage buffer
          DEBUG(2,(cout << "Baggage item id " << ItemId << endl));
          if (pB != NULL)
            { // Baggage buffer exists
              if (pProto != pBaggageProto)
                { // New baggage protocol
                  itemid_t cpid = CurrentProtoId | 0x80000000;
                  if ((pCB + sizeof(cpid)) > pEB)
                    {
                      cout << "Baggage buffer overflow in ImportMessage3"
                           << endl;
                      exit(1);
                    }
                  memcpy(pCB, &cpid, sizeof(cpid));
                  pCB += sizeof(cpid);
                  pBaggageProto = pProto;
                }
              if ((pCB + ItemLth + sizeof(ItemLth) + sizeof(*pId)) > pEB)
                {
                  cout << "Baggage buffer overflow in ImportMessage3"
                       << endl;
                  exit(1);
                }
              // Copy the protocol to the baggage buffer
              DEBUG(0,(cout << "Baggage on import (" << pItem->Name()
                       << "), pid " << ItemId
                       << " lth " << ItemLth << endl));
              memcpy(pCB, &ItemId, sizeof(ItemId));
              pCB += sizeof(ItemId);
              memcpy(pCB, &ItemLth, sizeof(ItemLth));
              pCB += sizeof(ItemLth);
              memcpy(pCB, pC, ItemLth);
              pCB += ItemLth;
              pCB = RoundUpBuffer(pB, pCB, *LthB, 0x03);
              pC += ItemLth;
            }
        }
      pC.Round(0x3);
    }
  // See if any items need defaulting from last specified proto
  if (pProto)
    {
      while(NextItemIndex < pProto->m_Items.size())
        {
          DefaultSingleItem(pO, pProto, NextItemIndex);
          NextItemIndex++;
        }
      // See if any unspecified protos (which need defaulting)
      while(NextProtoIndex < pS->m_Protos.size())
        {
          DefaultAllItems(pO, pS->m_Protos[NextProtoIndex]);
          NextProtoIndex++;
        }
    }
  if (LthB)
    {
      *LthB = (pCB - pB); // Length of baggage buffer
    }
  pI.Current(pC.Current());
  return 0;
}

// Get the current handle
DSHandle_t GetDSimHandle(void)
{
  return DSimInfo::instance; // One and only instance of DSIM
}

int SizeInBytes(int SizeInBits)
{
  return (SizeInBits + 7 ) / 8;
}

char* RoundUpBuffer(char* s, // Original buffer pointer
                    char* c, // Current buffer pointer
                    int ml,  // Max length
                    int m)   // Mask
{ //Insure proper byte alignment
  // Returns advanced buffer pointer
  if (!m) return(c); // No alighment needed
  while ((c-s) & m)
    {
      if (((c-s) + 1) > ml)
        {
          cout << "Buffer overflow on alignment" << endl;
          exit(1);
        }
      *c++ = 0;
    }
  return c;
}

int GetPeerSocket (int peer )
{
  return (pPeers[peer]);
}


// These are the original versions, taking char* for DynMsg buffer
int ExportMessageWrapper(
                  char* pI,             // Input buffer
                  char* pB, int  LthB,  // Baggage buffer, length
                  char* pO, int* LthO)  // Output buffer, length
{
CQueue q;

 q.pFirst = q.pIn = q.pOut = 0;
 q.pLimit  = *LthO;
 CQInPtr<char> p(q, pO); // Create an in pointer
 ExportMessage(pI, pB, LthB, p); // And call the CQPtr version
 *LthO = p.Current(); // Amount actually used
 return 0;
}

int ImportMessageWrapper(
                  char* pI, int LthI,  // Input buffer, length
                  char* pO,            // Output buffer,
                  char* pB, int* LthB) // Baggage buffer, length(in/out)
{
CQueue q;

 q.pFirst = q.pOut = 0;
 q.pIn = LthI;
 q.pLimit  = LthI + 1;
 CQOutPtr<char> p(q, pI); // Create an out pointer
 ImportMessage(p, q.pIn, pO, pB, LthB); // And call the CQPtr version
 return 0;
}

// The versions using CQueue structures as Import input and Export output
// These versions avoid an extra data copy at each end
// These are callable from "C"

int ExportMessageQ( char* pI, char* pB, int LthB,
                    CQueue* pQ, char* pBase, size_t pIn, size_t pLth)
{
CQInPtr<char> p(*pQ, pBase);  // Create the input pointer

  p.Current(pIn);
  ExportMessage(pI, pB, LthB, p); // And call the CQPtr version
  // Calculate actual amount exported and put in msg length
  size_t o; // Original length in Msg
  QGetN(pQ, pBase, (char*)&o, sizeof(o), pLth);
  size_t s = QDiff(pQ, pIn, p.Current());
  if(0)cout << "ExpMsg, orig lth %" << o << " QDiff " << s << endl;
  s += o;
  QPutN(pQ, pBase, (char*)&s, sizeof(s), pLth);
  pQ->pIn = p.Current(); // And update the Queue in pointer
  return 0;
}

int ImportMessageQ( CQueue* pQ, char* pBase, size_t pIn,
                    char * pO, char* pB, int* LthB,
                    size_t pOut)
{
CQOutPtr<char> p(*pQ, pBase);  // Create the output pointer

 p.Current(pOut);
 ImportMessage(p, pIn, pO, pB, LthB); // And call the CQPtr version
 size_t s = QDiff(pQ, pOut, p.Current());
 if(0) cout << "ImportMsgQ imported " << s << " bytes, original pIn "
            << pIn << " pOut " << pOut << endl;
 pQ->pOut = p.Current(); // Update the Queue out pointer
 if(0)cout << "ImportMsgQ updated pout to " << pQ->pOut << endl;
 if(0)QDumpPtrs(pQ);
 return 0;
}

