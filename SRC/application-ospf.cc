
#define DEBUG_MASK		0x0000

#include "debug.h"
#include "simulator.h"
#include "application-ospf.h"

using namespace std;

// OSPFApplication methods
OSPFApplication::OSPFApplication (
  OSPFRouter_t rid
  )
  : node (nil), ospf (new OSPF (rid))
{
  DEBUG (0, (cerr << "OSPFApplication::OSPFApplication" << endl));
  DEBUG (1, (cerr << "\tInstance " << this << " created with Router ID " << (string)IPAddr (rid) << endl));
}

OSPFApplication::~OSPFApplication (void)
{
  DEBUG (0, (cerr << "OSPFApplication::~OSPFApplication" << endl));
  DEBUG (1, (cerr << "\tInstance " << this << " destroyed" << endl));

  if (ospf != nil) {
    delete ospf;
  }
}

void OSPFApplication::StartApp (void)
{
  DEBUG (0, (cerr << "OSPFApplication::StartApp" << endl));
  DEBUG (1, (cerr << "\tAt " << Simulator::Now () << endl));

  if ((node == nil) || (ospf == nil)) {
    cerr << "HuH? no node or no ospf" << endl;
  }
  else {
    Event* event = new Event (OSPF::START);
    Scheduler::Schedule (event, 0, ospf);
  }
}

void OSPFApplication::StopApp (void)     
{
  DEBUG (0, (cerr << "OSPFApplication::StoptApp" << endl));
  DEBUG (1, (cerr << "\tAt " << Simulator::Now () << endl));

  if ((node == nil) || (ospf == nil)) {
    cerr << "HuH? no node or no ospf" << endl;
  }
  else {
    Event* event = new Event (OSPF::STOP);
    Scheduler::Schedule (event, 0, ospf);
  }
}

void OSPFApplication::AttachNode (
  Node* node
  ) 
{
  DEBUG (0, (cerr << "OSPFApplication::AttachNode" << endl));
  DEBUG (1, (cerr << "\tNode " << node->Id () << " attached" << endl));

  this->node = node;
  ospf->AttachNode (node);
}

Application* OSPFApplication::Copy (void) const
{
  DEBUG (0, (cerr << "OSPFApplication::Copy" << endl));
  DEBUG (1, (cerr << "\tFrom instance " << this << endl));

  OSPFApplication* appl = new OSPFApplication (ospf->GetRouterID ());
  if (appl != nil) {
    appl->node = node;
  }
  return (appl);
}

OSPFArea* OSPFApplication::AddArea (
  OSPFArea_t aid
  )
{
  DEBUG (0, (cerr << "OSPFApplication::AddArea" << endl));
  DEBUG (1, (cerr << "\tArea ID " << (string)IPAddr (aid) << endl));

  return (ospf->AddArea (aid));
}

OSPFInterface* OSPFApplication::AddInterface (
  OSPFArea* area,
  OSPFNetwork_t type,
  IPAddr_t addr,
  Mask_t mask,
  Byte_t priority,
  OSPFCost_t cost
  )
{
  DEBUG (0, (cerr << "OSPFApplication::AddInterface" << endl));
  DEBUG (1, (cerr << "\tType " << type << "; IP " << (string)IPAddr (addr) << "; Mask " << (string)IPAddr (mask) << endl));

  if ((area != nil) && OSPFInterface::IsInterface (node, addr)) {
    return (area->AddInterface (type, addr, mask, priority, cost));
  }
  else {
    return (nil);
  }
}

