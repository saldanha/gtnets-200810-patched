// GENERAL PUBLIC LICENSE AGREEMENT
//
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
//
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
//
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
//
// 1.  This License allows you to:
//
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
//
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//
//
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
//
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
//
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
//
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
//
//
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
//
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
//
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
//
//
// 3.  Export Law Assurance.
//
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//
// 4.  Termination.
//
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
//
//
// 5.  Disclaimer of Warranties and Limitation on Liability.
//
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
//
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
//
// GTRC shall have no obligation for support or maintenance of Program.
//
// 6.  Copyright Notice.
//
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
//
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
//
// Said copyright notice shall read as follows:
//
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: wlan.cc 498 2006-05-10 16:58:08Z riley $



// the wireless lan layer

#include <iostream>

//#define DEBUG_MASK 0x01
#include "debug.h"
#include "wlan.h"
#include "node.h"
#include "queue.h"
#include "propagation.h"
#include "qtwindow.h"
#include "l2proto802.11.h"
#include "interface-wireless.h"

// Animation related
#include "qtwindow.h"
#ifdef HAVE_QT
#include <qcanvas.h>
#endif

using namespace std;

WirelessLink::WirelessLink(Count_t n,   // Node count
			   IPAddr_t i, Mask_t m,    // IP addr start, mask
			   L4Protocol* l4,          // L4 protocol to bind to
			   Rate_t r, Time_t dly,
			   Detail_t d, bool bootstrap)
    : LinkReal(), firstIP(IPADDR_NONE), firstMac(0), lastMac(0)
{
  ConstructorHelper(n, i, m, l4, r, dly, d, bootstrap);
  propagation = new PropaTwoRay();
  csThreshold = pow(10, -7.8);    // -78 dBm
  rxThreshold = pow(10, -6.4);    // -64 dBm
  snrThreshold = 10;              // 10 dB
}

WirelessLink::~WirelessLink()
{ // Destructor, nothing to do
  if (propagation) delete propagation;
}

InterfaceBasic* WirelessLink::GetPeer(Packet* p)
{
  L2Header802_11* l2pdu = (L2Header802_11*)p->PeekPDU(); // Get a ptr to l2 hdr

  return GetIfByMac(l2pdu->ra);
}

InterfaceBasic* WirelessLink::GetPeer(Count_t i)
{ // Get the i'th peer's interface
  return GetIf(i);
}

// Link methods
Link* WirelessLink::Copy() const
{
  return new WirelessLink(*this);
}

void WirelessLink::Handle(Event* e, Time_t t)
{
  WLanEvent* ev = (WLanEvent*)e;
  switch (ev->event) {
    case WLanEvent::WIRELESS_TX_START :
#ifdef VERBOSE
      cout << "Wireless TX Start "
           << " iter " << ev->iter
           << " time " << Simulator::Now() << endl;
#endif
      WirelessTxStart(ev->src, ev->iFace, ev->dst, ev->range, ++(ev->iter));
      delete ev; // New event allocated in private subroutines
      break;
    case WLanEvent::WIRELESS_TX_END :
#ifdef VERBOSE
      cout << "Wireless TX End "
           << " iter " << ev->iter
           << " time " << Simulator::Now() << endl;
#endif
      WirelessTxEnd(ev->src, ev->iFace, ev->dst, ev->range, ++(ev->iter));
      delete ev; // New event allocated in private subroutines
      break;
    default:
      LinkReal::Handle(e, t);
    }
}

bool WirelessLink::Transmit(Packet* p, Interface* myIface, Node* node,
			    Time_t txTime)
{
  InterfaceWireless* self_if = (InterfaceWireless*)myIface;
  if (self_if->GetLinkState()) {
    // Link is busy
    // This should NOT happen!!!
    cout << "HuH? linkBusy while xmitting linkState "
	 << self_if->GetLinkState() << endl;
    //return false;
  }

  // cout << "WLan::Transmit Transmitting at rate " << r << endl;
  node->BuildRadioInterfaceList(this);

  LinkEvent* evTx = new LinkEvent(LinkEvent::PACKET_TX);
  evTx->numberBytes = p->Size();
  /* API change  
  Time_t txTime = ((Rate_t)p->Size() * 8) / r;
  */
  Scheduler::Schedule(evTx, txTime, this);
  nextFreeTime = Simulator::Now() + txTime;
  Time_t* ptime = new Time_t(nextFreeTime);
  if (self_if->GetLinkFreeTime() < nextFreeTime)
    self_if->SetLinkFreeTime(nextFreeTime);
  else {
    // This should NOT happen!!!
    cout << "HuH? linkFreeTime out of order linkState "
         << self_if->GetLinkState() << endl
         << " self linkFreeTime " << self_if->GetLinkFreeTime()
         << " nextFreeTime " << nextFreeTime
         << " now " << Simulator::Now() << endl;
  }
  self_if->SetLinkState(InterfaceWireless::TX);
  // yj, this is a PHY-TXSTART.indication
  InterfaceWireless::PhyInd_t phyInd = InterfaceWireless::PHY_TXSTART;
  self_if->GetL2Proto()->Notify(&phyInd);
  AddNotify(self_if, ptime);
  // also time to cut the node's energy reserve by appropriate values
  double delta_time = Simulator::Now() - self_if->last_time ;

  L2Header802_11* l2hdr = (L2Header802_11*)p->PeekPDU();
  double ftxPow=0.0;
  switch (l2hdr->type) {
  case RTS:
           ftxPow *= self_if->getRTSTxPower();
           break;
  case CTS:
           ftxPow *= self_if->getCTSTxPower();
           break;
  case ACK:
           ftxPow *= self_if->getACKTxPower();
           break;
  default:
           ftxPow = self_if->getTxPower();
           /* Nothing to do here */
  }
  node->setBattery(node->getBattery() - (txTime * ftxPow +
                                  (delta_time * node->getComputePower())));
  self_if->last_time = Simulator::Now();

  if (node->getBattery() < 0) {
    cout << "OUT OF BATTERY POWER SIMULATION FROM HERE ON IS"
         << "TRUELY WHAT IT IS :: A SIMULATION :) "<< endl;
  }
  const RadioVec_t& rIFList = node->GetRadioInterfaceList();
  RadioVec_t::const_iterator i = rIFList.begin();
  for (; i != rIFList.end(); ++i)
    // TBD:Check for BERs because of Radio Propagation Models
    {
      // schedule recv at all i/f except self
      InterfaceWireless* inf = i->inf;
      if (inf == self_if) continue;
      if (self_if->channel != inf->channel) continue; // Different channels
      double pr = propagation->Pr(i->dist);    // rx power, mW
      if (pr < csThreshold)  // ignore if rx power < CST
        continue;

      bool error = false;
      if (pr < rxThreshold)  // mark as erroneous if rx power < RXT
        error = true;

      Time_t rxTime
	  = txTime + SpeedOfLight(node, inf->GetNode()) + jitter->Value();
      Time_t linkFreeTime = Simulator::Now() + rxTime;

      LinkEvent* evRx = nil;
      if (linkFreeTime > inf->GetLinkFreeTime()) {
        // Schedule pkt receive event for this case only
        evRx = new LinkEvent(LinkEvent::PACKET_RX, p->Copy());
        inf->ScheduleRx(evRx, rxTime);
        inf->SetLinkFreeTime(linkFreeTime);
      }
      if (!inf->GetLinkState()) { // link is idle
        // 1st scheduled pkt
        evRx->hasBitError = error;
        inf->SetRxPacketEvent(evRx);
        inf->SetRxPower(pr);
        inf->SetLinkState(InterfaceWireless::RX);
        // yj, this is a PHY-RXSTART.indication
        phyInd = InterfaceWireless::PHY_RXSTART;
        inf->GetL2Proto()->Notify(&phyInd);
#ifdef HAVE_QT
        /* Differentiate RX, RX_ME, and RX_ZZ
           RX_ME means the pkt is for me
           RX_ZZ means the pkt is interference
           These states are mutually exclusive
        */
        if (error)
          inf->SetLinkState(InterfaceWireless::RX_ZZ);
        else {
          L2Header802_11* l2hdr = (L2Header802_11*)p->PeekPDU();
          if (l2hdr->ra == inf->GetMACAddr() || l2hdr->ra.IsBroadcast())
            inf->SetLinkState(InterfaceWireless::RX_ME);
        }
#endif
      }
      else
        { // link is busy
	  // compare with rx power of incoming pkt
          if (inf->GetRxPacketEvent() && !inf->GetRxPacketEvent()->hasBitError)
            {
              if (inf->GetRxPower()/pr < snrThreshold)
                { // collision
                  inf->GetRxPacketEvent()->hasBitError = true;
                  inf->SetLinkState(InterfaceWireless::CX);
                }
            }
          else if (inf->GetLinkState() != InterfaceWireless::TX) {
            /* Actually, this checking may be unnecessary because
               if a neighbor is xmitting, this node cannot xmit.
               Thus, this node's xmitting assumes no neighbors
               are transmitting.
               Mobility may make this happen?
               interference + interference = collision?
            */
            inf->SetLinkState(InterfaceWireless::CX);
	  }
	  if (evRx) evRx->hasBitError = true;  // this pkt is an interference
	}
    } //for

  //delete p;
  Stats::pktsTransmitted++;
#ifdef HAVE_QT
  // Animate if requested
  if (Simulator::instance->AnimateWirelessTx())
    {
      L2Header802_11* l2hdr = (L2Header802_11*)p->PeekPDU();
      QColor c;
      switch (l2hdr->type)
        {
          case RTS:
            c = Qt::cyan;   break;
          case CTS:
            c = Qt::yellow; break;
          case DATA:
            c = l2hdr->ra.IsBroadcast() ? Qt::black : Qt::blue;  break;
          case ACK:
            c = Qt::green;  break;
          default:
            c = Qt::blue;   break;
        }
      node->WirelessTxColor(c);
      WirelessTxStart(node, self_if, nil, node->GetRadioRange());
    }
#endif
  return true;
}

Time_t WirelessLink::SpeedOfLight(Node* n1, Node* n2)
{ // Calculate speed of light delay between nodes
  Meters_t dist = n1->Distance(n2);
  return dist / SPEED_LIGHT;
}

InterfaceWireless* WirelessLink::AddNode(Node* n, IPAddr_t ip, Mask_t m)
{
  InterfaceWireless*  wif =
      (InterfaceWireless*)n->AddInterface(L2Proto802_11(), ip, m);
  AddInterface(wif);
  return wif;
}

Interface* WirelessLink::GetIfByMac(MACAddr m)
{ // Get the interface corresponding to the specified MAC address
  if (m >= firstMac && m < lastMac)
    { // Is consecutive, just index the interface vector
      return ifaceVec[m - firstMac];
    }
  // Otherwise, search
  for (IFVec_t::size_type i = lastMac - firstMac; i < ifaceVec.size(); ++i)
    {
      if (ifaceVec[i]->GetMACAddr() == m) return ifaceVec[i]; // Found it
    }
  return nil;                         // Not found
}

Count_t WirelessLink::PeerCount()
{ // Return number of peers
  return ifaceVec.size() - 1; // Subtract one since not self-peer
}

Count_t WirelessLink::NodeCount()
{ // Return number of nodes
  return ifaceVec.size();
}

IPAddr_t WirelessLink::PeerIP(Count_t npeer)
{ // Get peer IP Address (if known)
  Node* n = GetNode(npeer);  // Get the node
  if (n) return n->GetIPAddr(); // get the node's ip address
  return IPADDR_NONE;        // Problem, return none
}

Count_t WirelessLink::NodePeerIndex(Node* n)
{ // Return the peer index.  Assumed to be in range
  cout << "WirelessLink::NodePeerIndex called, exiting for debug" << endl;
  exit(1);
  //NodeId_t id = n->Id();
  //return id - first;
  return 0;
}

IPAddr_t WirelessLink::NodePeerIP(Node* n)
{ // Get peer IP Address for the specified node
  cout << "WirelessLink::NodePeerIP called, exiting for debug" << endl;
  exit(1);
  //for (Count_t i = 0; i < PeerCount(); ++i)
  //  {
  //    Node* peerNode = GetNode(i);
  //    if (peerNode == n) return GetIP(i); // Found it, return IP Addresss
  //  }
  return IPADDR_NONE;        // Not found, return none
}

Count_t WirelessLink::NeighborCount(Node*)
{ // Number of routing neighbors
  return PeerCount(); // Otherwise return number of peers
}

void WirelessLink::Neighbors(NodeWeightVec_t& nwv, Interface* i, bool all)
{ // Add to neighbor list
  // For wireless LAN's, return an empty list.
}


void WirelessLink::AllNeighbors(NodeWeightVec_t& nwv)
{ // Make a list of all neighbors.  Note the the "Interface" pointer
  // in the NodeIfWeight vector is the TARGET interface, not the
  // source as in Neighbors above.  This function is used by L2
  // broadcast to determine where to schedule receive events.
  // (For wireless, can be ignored)
  //for (NodeId_t ni = first; ni < last; ++ni)
  //  {
  //    Interface* iface = GetIf(ni);
  //    if (!iface) cout << "HuH?  Ethernet::AllNeighbors nill iface for "
  //                     << ni << endl;
  //    nwv.push_back(NodeIfWeight(Node::GetNode(ni), GetIf(ni), Weight()));
  //  }
}

MACAddr WirelessLink::IPToMac(IPAddr_t ip)
{ // Convert peer IP to peer MAC
  if (ip >= firstIP && ip < (firstIP + (lastMac - firstMac)))
    { // Consecutive
      return firstMac + (ip - firstIP);
    }
  // Have to search the non-consecutive ones
  for (IFVec_t::size_type i = lastMac - firstMac; i < ifaceVec.size(); ++i)
    {
      if ((IPAddr_t)(ifaceVec[i]->GetIPAddr()) == ip)
        return ifaceVec[i]->GetMACAddr(); // Found it
    }
  return nil;                         // Not found
}

IPAddr_t WirelessLink::GetIP(Count_t i)
{ // Get the i'th ip address
  if (i > ifaceVec.size()) return IPADDR_NONE; // Out of range
  return ifaceVec[i]->GetIPAddr();
}

Node* WirelessLink::GetNode(Count_t i)
{ // Get the i'th node
  if (i > ifaceVec.size()) return nil; // Out of range
  return ifaceVec[i]->GetNode();
}

Interface* WirelessLink::GetIf(Count_t i)
{ // Get the interface for the i'th node
  if (i > ifaceVec.size()) return nil; // Out of range
  return ifaceVec[i];
}

bool WirelessLink::NodeIsPeer(Node* n)
{ // Return true of node is a peer
  // Always false should  work !
  return false;
  //NodeId_t id = n->Id();
  //return (id >= first && id < last); // True if in range
}

// Animation
void   WirelessLink::WirelessTxStart(Node* n, InterfaceWireless* iFace,
                                     Node* d, Meters_t range)
{
  if (!n->IsReal()) return;
  DEBUG(1,(cout << "Anim Wireless Tx Start, range " << range
        << " time " << Simulator::Now() << endl));
  WirelessTxStart(n, iFace, d, range, 0);
}

void   WirelessLink::WirelessTxEnd(Node* n, InterfaceWireless* iFace,
                                   Node* d, Meters_t range)
{
  if (!n->IsReal()) return;
  DEBUG(1, (cout << "Anim Wireless Tx End, range " << range
          << " time " << Simulator::Now() << endl));
  WirelessTxEnd(n, iFace, d, range, 0);
}


// Private methods
void WirelessLink::ConstructorHelper(
    Count_t n,      // Node count
    IPAddr_t i,
    Mask_t mask,    // IP addr start, mask
    L4Protocol* l4, // L4 protocol to bind to each
    Rate_t r, Time_t dly,
    Detail_t d, bool bootstrap)
{

  detail = d;                       // Detail level
  bw = r;                           // Bandwidth
  basicRate = r;
  dataRate = r;
  delay = dly;                      // Delay
  for (Count_t j = 0; j < n; ++j)
    { // Create the nodes
      Node* node = new Node();
      AddNode(node, i, mask);
      ++i;                          // Advance IP Address
    }
}

// Private for animating the wireless tx
void   WirelessLink::WirelessTxStart(Node* n, InterfaceWireless* iFace,
				     Node* d, Meters_t range, Count_t it)
{
#ifdef HAVE_QT
  // Get the qt window pointer
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  if (it == 1)
    { // First iteration, see if "pauseOnTx" set
      if (Simulator::instance->PauseOnWirelessTx())
        {
          qtw->Pause();
        }
    }

  if (it)
    {
      // Add the circle to the canvas items list (if not zeroth iteration)
      // First find the radius of the circle in pixels
      Location l = n->GetLocation();
      QPoint   lp = qtw->LocationToPixels(l);
      n->Display(lp, qtw);
      Meters_t r = range / 10.0 * it;
      Location rl = l;
      rl.X(rl.X() + r);
      QPoint  rp = qtw->LocationToPixels(rl);
      //QCanvasCircle* c = new QCanvasCircle(rp.x() - lp.x(), qtw->Canvas());
      // Set the color, with reduced intensity farther  away
      double radius = rp.x() - lp.x();
     
       if(Simulator::instance->AnimateWirelessTxForm()== CIRCLES)
       {
         QCanvasCircle *c = new QCanvasCircle(radius,qtw->Canvas());
	 // Set the color, with reduced intensity farther  away
	
         const QColor& col = n->WirelessTxColor();
         int red = col.red();
         int green = col.green();
         int blue = col.blue();
         int nred   = red   + (int)((double)(255-red)   * (it-1)/10.0);
         int ngreen = green + (int)((double)(255-green) * (it-1)/10.0);
         int nblue  = blue  + (int)((double)(255-blue)  * (it-1)/10.0);
         c->setPen(QColor(nred, ngreen, nblue));
         c->move(lp.x(), lp.y());
         c->show();
         //c->update();
         n->PushWirelessTx(c);
      }
      // Now re-display any nodes in the power range just processed
      Meters_t prevr = range / 10.0 * (it - 1);
      const RadioVec_t& rv = n->GetRadioInterfaceList();
      for (RadioVec_t::const_iterator i = rv.begin(); i != rv.end(); ++i)
        {
          InterfaceWireless* iFace1 = i->inf;
          Meters_t dist1 = i->dist;
          if (dist1 > prevr && dist1 <= r)
            { // Need to update this one
              Node* n1 = iFace1->GetNode();
              Location l1 = n1->GetLocation();
              QPoint   lp1 = qtw->LocationToPixels(l1);
	      if(Simulator::instance->AnimateWirelessTxForm()== DIRECTED_ARR)
	      {
	        if(n1->WirelessRxMe())
		{
		  QCanvasLine *c = new QCanvasLine(qtw->Canvas());
		  c->setPoints(lp.x(),lp.y(),lp1.x(),lp1.y());
			  
		  // Set the color, with reduced intensity farther  away
		  const QColor& col = n->WirelessTxColor();
		  int red = col.red();
		  int green = col.green();
		  int blue = col.blue();
		  int nred   = red   + (int)((double)(255-red)   * (it-1)/10.0);
		  int ngreen = green + (int)((double)(255-green) * (it-1)/10.0);
		  int nblue  = blue  + (int)((double)(255-blue)  * (it-1)/10.0);
		  QColor myColor = QColor(nred, ngreen, nblue);
		  c->setPen(QPen(myColor));
		  // Draw an arrow manually
		  QPoint pd, pa, pb;
		  QCanvasPolygon * arrowHeadPoly = new QCanvasPolygon(qtw->Canvas());
		  double tangent;
		  double sze = 10;
		  pd.setX(lp.x() - lp1.x());
		  pd.setY(lp.y() - lp1.y());
		  if (!(pd.x() == 0 && pd.y() == 0))
		  {
		    tangent = atan2 ((double) pd.y(), (double) pd.x());
		    pa.setX(sze * cos (tangent + M_PI / 7) + lp1.x());
		    pa.setY(sze * sin (tangent + M_PI / 7) + lp1.y());
		    pb.setX(sze * cos (tangent - M_PI / 7) + lp1.x());
		    pb.setY(sze * sin (tangent - M_PI / 7) + lp1.y());
			
		    QPointArray pArr(1);
		    int px1 = lp1.x(), py1= lp1.y();
		    pArr[0]= QPoint(px1,py1);
		    int px2 = pa.x(), py2= pa.y();
		    int px3 = pb.x(), py3= pb.y();
		    pArr.putPoints(1,2,px2,py2,px3,py3);  
		    //arrowHeadPoly << QPoint(lp1.x(),lp1.y()) <<  QPoint(pa.x(),pa.y()) << QPoint(pb.x(),pb.y());
	            arrowHeadPoly->setPoints(pArr);
		    //QCanvasPolygonalItem * arrowHead;
		    //arrowHead = new QCanvasPolygonalItem(qtw->Canvas());
		    //arrowHead->setPen(QPen(myColor));
		    arrowHeadPoly->setBrush(QBrush(myColor));
		  }
			  
		  c->show();
		  //c->update();
		  n->PushWirelessTx(c);
				
		  arrowHeadPoly->show();
		  //arrowHead->update();
		  n->PushWirelessTx(arrowHeadPoly);
	        }
	      }
              n1->Display(lp1, qtw);
            }
        }
          
      // Update the simtime
      qtw->UpdateSimTime();
      qtw->Canvas()->update(); // Show the changes
      qtw->ProcessEvents();
      qtw->WaitWhilePaused();
    }
  
  if (it < 10)
    {
      WLanEvent* ev = new WLanEvent(WLanEvent::WIRELESS_TX_START);
      ev->src = n;
      ev->dst = d;
      ev->iFace = iFace;
      ev->range = range;
      ev->iter = it;
      Time_t perIteration = ev->range / SPEED_LIGHT;
      // Schedule next update event
      Scheduler::Schedule(ev, perIteration, this);
    }
#endif
}

void   WirelessLink::WirelessTxEnd(Node* n, InterfaceWireless* iFace, Node* d,
                               Meters_t range, Count_t it)
{
#ifdef HAVE_QT
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  if (it)
    { // Remove the circles one-by-one to animate end of transmission
      // Must be real node here
      Location l = n->GetLocation();
      QPoint   lp = qtw->LocationToPixels(l);
      n->Display(lp, qtw);
      if(Simulator::instance->AnimateWirelessTxForm()== CIRCLES)
      {
        QCanvasItem* c = n->PopWirelessTx();
        if(c)delete c;
      }
      // Now re-display any nodes in the power range just processed
      Meters_t r = range / 10.0 * it;
      Meters_t prevr = range / 10.0 * (it - 1);
      const RadioVec_t& rv = n->GetRadioInterfaceList();
      for (RadioVec_t::const_iterator i = rv.begin(); i != rv.end(); ++i)
        {
          InterfaceWireless* iFace1 = i->inf;
          Meters_t dist1 = i->dist;
          if (dist1 > prevr && dist1 <= r)
            { // Need to update this one
              Node* n1 = iFace1->GetNode();
              Location l1 = n1->GetLocation();
              QPoint   lp1 = qtw->LocationToPixels(l1);
	      if(Simulator::instance->AnimateWirelessTxForm()== DIRECTED_ARR)
	      {
	        QCanvasItem* c = n->PopWirelessTx();  
		if(c)delete c;
		QCanvasItem* arrow = n->PopWirelessTx();  
		if(arrow)delete arrow;
	      }
              n1->Display(lp1, qtw);
            }
        }
          
      // Update the simtime
      qtw->UpdateSimTime();
      qtw->Canvas()->update(); // Show the changes
      qtw->ProcessEvents();
      qtw->WaitWhilePaused();
    }
  
  if (it < 10)
    {
      WLanEvent* ev = new WLanEvent(WLanEvent::WIRELESS_TX_END);
      ev->src = n;
      ev->dst = d;
      ev->iFace = iFace;
      ev->range = range;
      ev->iter = it;
      Time_t perIteration = ev->range / SPEED_LIGHT;
      // Schedule next update event
      Scheduler::Schedule(ev, perIteration, this);
    }
#endif
}

void   WirelessLink::AddInterface(Interface* i)
{
  if (ifaceVec.empty())
    { // First one
      firstMac = i->GetMACAddr();
      lastMac  = firstMac;
      firstIP  = i->GetIPAddr();
    }
  ifaceVec.push_back(i);
  // And set the link object to this lan
  i->SetLink(this);
  if (i->GetMACAddr() == lastMac)
    { // Mac address is consecutive
      lastMac++;
    }
}

      
