// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: datapdu.cc 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - Data PDU Headers
// George F. Riley.  Georgia Tech, Spring 2002

#include <iostream>
#include <algorithm>

#include <string.h>

#include "debug.h"
#include "datapdu.h"
#include "packet.h"
#include "hex.h"

using namespace std;

// NullData Methods

Data::Data() : size(0), data(nil),
               msgSize(0), responseSize(0), checksum(0)
{
  DEBUG0((cout << "DataPDU() Constructor" << this << endl));
  DBG((Stats::dataPdusAllocated++));
}

Data::Data(Size_t s, char* d, Count_t msg, Count_t resp)
  : size(s), data(nil), msgSize(msg), responseSize(resp)
{ // Make a copy of the data
  if (d)
    {
      data = new char[s];
      memcpy(data, d, s);
    }
  DEBUG0((cout << "DataPDU(s,d) Constructor" << this << endl));
  DBG((Stats::dataPdusAllocated++));
}

Data::Data(const string& s) 
  : size(s.length() + 1), data(strdup(s.c_str())),
    msgSize(0), responseSize(0), checksum(0)
{
  DEBUG0((cout << "DataPDU(string) Constructor" << this << endl));
}

Data::Data(const Data& c)
  : size(c.Size()), data(0),
    msgSize(c.msgSize), responseSize(c.responseSize),
    checksum(c.checksum)
{ // Copy constructor
  DEBUG0((cout << "DataPDU() Copy Constructor" << this << endl));
  DBG((Stats::dataPdusAllocated++));
  if (c.Contents())
    { // Has data
      data = new char[Size()];
      memcpy(data, c.Contents(), Size());
    }
}

Data::Data(char* b, Size_t& sz, Packet* p)
  : data(0)
{ // Construct from serialized buffer
  DEBUG0((cout << "DataPDU() Construct from buffer" << this << endl));
    Size_t s = 0;
  b = Serializable::GetSize(b, sz, s);
  DEBUG0((cout << "data size is " << s << " (" << Hex8(s) << ") " << endl));
  sz -= s;
  b = Construct(b, s);
  p->PushPDUBottom(this);  // Add to packet
}

Data::~Data()
{ // destructor
  DEBUG0((cout << "DataPDU() Destructor " << this << endl));
  DBG((Stats::dataPdusDeleted++));
  if (data) delete [] data;
}

// Serialization
Size_t Data::SSize()
{ // Size needed for serialization
  Size_t r = sizeof(size) + sizeof(msgSize) + sizeof(responseSize);
  if (data) r += size; // if associated data
  return r;
}

char*  Data::Serialize(char* b, Size_t& sz)
{ // Serialize to a buffer
  b = SerializeToBuffer(b, sz, size);
  b = SerializeToBuffer(b, sz, msgSize);
  b = SerializeToBuffer(b, sz, responseSize);
  b = SerializeToBuffer(b, sz, checksum);
  DEBUG0((cout << "Serializing msgSize " << msgSize 
          << " responseSize " << responseSize << endl));
  if (data)
    { // Associated data
      DEBUG0((cout << "Serializing data, size " << size 
              << " sz " << sz << endl));
      memcpy(b, data, size);
      b += size;
      sz -= size;
    }
  return b;
}

char*  Data::Construct(char* b, Size_t& sz)
{ // Construct from buffer
  b = ConstructFromBuffer(b, sz, size);
  b = ConstructFromBuffer(b, sz, msgSize);
  b = ConstructFromBuffer(b, sz, responseSize);
  b = ConstructFromBuffer(b, sz, checksum);
  DEBUG0((cout << "Constructed msgSize " << msgSize 
          << " responseSize " << responseSize << endl));
  if (sz && sz  >= size)
    { // Still have remaining size, must be associated data
      data = new char[size];
      memcpy(data, b, size);
      b += size;
      sz -= size;
    }
  if (sz)
    {
      DEBUG0((cout << "HuH?  remaining size " << sz 
              << " after data construct" << endl));
      sz = 0; // ! Major hack for now till I get this debgged
    }
  return b;
}

PDU* Data::Copy() const
{
  return new Data(*this);
};

PDU* Data::CopyS(Size_t s)
{ // Copy, but with new size (assumes no associated data);
  return new Data(s, nil, msgSize, responseSize);
}

PDU* Data::CopySD(Size_t s, char* d)
{ // Copy, but with new size (assumes no associated data);
  return new Data(s, d, msgSize, responseSize);
}

void Data::Clear()
{ // Remove all pending data
  if (data) delete [] data; // Free memory if used
  data = nil;
  size = 0;
}

void Data::Add(Size_t s, const char* d)
{
  if (data)
    { // Data exists, realloc and copy
      char* n = new char[Size() + s];
      memcpy(n, data, Size());
      if (d)
        { // New data specified
          memcpy(n + Size(), d, s); // Append the new data
        }
      else
        {
          memset(n + Size(), 0, s); // Apend zeros
        }
      delete [] data;           // Delete the old data
      data = n;                 // Points to new one
    }
  else
    { // No existing data, see if new data
      if (d)
        {
          data = new char[s];
          memcpy(data, d, s);
        }
    }
  size += s;
}

void Data::Remove(Size_t s)
{
  Size_t r = s > Size() ? Size() : s;

  size -= r;          // Reduce size from current
  if (data)
    { // data actually exists, realloc and copy
      if (size)
        {
          char* d = new char[Size()];
          memcpy(d, data, Size());
          delete [] data;
          data = d;
        }
      else
        { // Zero size, so don't need the data anymore
          delete [] data;
          data = nil;
        }
    }
}

Size_t Data::SizeFromSeq(const Seq& f, const Seq& o)
{
  Size_t o1 = OffsetFromSeq(f,o); // Offset to start of unused data
  return SizeFromOffset(o1);      // Amount of data after offset
}

Size_t Data::SizeFromOffset(Size_t o)
{ // Find out how much data is available from offset
  if (o > size) return 0;     // No data at requested offset
  return size - o;            // Available data after offset
}

Size_t Data::OffsetFromSeq(const Seq& f, const Seq& o)
{ // f is the first sequence number in this data, o is offset sequence
  if (o < f) return 0; // HuH?  Shouldn't happen
  return o - f;
}

Data* Data::CopyFromOffset(Size_t s, Size_t o)
{ // Make a copy of data from starting position "o" for "s" bytes
  // Return nil if results in zero length data
  Size_t s1 = min(s, SizeFromOffset(o)); // Insure not beyond end of data
  if (s1 == 0)    return nil;   // No data requested
  if (data)
    { // Actual data exists, make copy and return it
      char* d1 = new char[s1];  // Allocate memory for the copy
      memcpy(d1, &data[o], s1); // Copy the data
      Data* d = new Data(s1, d1, msgSize, responseSize);  // Return copy
      return d;
    }
  else
    { // No actual data, just return non-data pdu of correct size
      return new Data(s1, nil, msgSize, responseSize);
    }
}

Data* Data::CopyFromSeq(Size_t s, const Seq& f, const Seq& o)
{
  Data* d = CopyFromOffset(s, OffsetFromSeq(f,o));
  return d;
}

// Checksum management
void Data::Checksum(Word_t ck)
{ // Set a checksum for data pdus with no actual data
  checksum = ck;
}

Word_t Data::Checksum()
{
  if (!data) return checksum; // Use specified value if no data
  // Calculate the checksum
  Word_t* pData = (Word_t*)data;
  // Calculate the checksum, using 16 bit xor
  Word_t  sum = 0;
  Size_t  s2 = size / 2;
  for (Size_t i = 0; i < s2; ++i)
    {
      sum ^= pData[i];
    }
  if (s2 * 2 < size)
    { // Odd size, xor in last word
      sum ^= (data[size-1] << 8);
    }
  return sum;
}

      
  
  
  



