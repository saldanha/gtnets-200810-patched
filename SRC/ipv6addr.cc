
#include "ipv6addr.h"
#include <iostream>
#include <sstream>

using namespace std;

IPV6Addr::IPV6Addr():Addr(Addr::AF_INET6)
{
  mask = 0;
  addrl[0]=addrl[1]=addrl[2]=addrl[3] = 0;
}

IPV6Addr::IPV6Addr(const IPV6Addr& addr_):Addr(Addr::AF_INET6)
{
  addrl[0] = addr_.addrl[0];
  addrl[1] = addr_.addrl[1];
  addrl[2] = addr_.addrl[2];
  addrl[3] = addr_.addrl[3];
}

IPV6Addr::~IPV6Addr()
{}

IPV6Addr::IPV6Addr(const char* addr_) : Addr(Addr::AF_INET6)
{
  int pos1=-1, pos2;
  int cnt = 0;

  std::string saddr(addr_);

  for(;;) {
    pos2 = saddr.find(":", ++pos1);
    std::istringstream iss(saddr.substr(pos1, pos2 - pos1));
    iss >> hex >> addrs[cnt];
    cnt++;
    pos1 = pos2;
    if(cnt > 7)
      break;
  }

  cout << hex
       << addrs[0] << ":" << addrs[1] << ":" << addrs[2] << ":"
       << addrs[3] << ":" << addrs[4] << ":" << addrs[5] << ":"
       << addrs[6] << ":" << addrs[7]
       << endl;

}

bool IPV6Addr::operator==(const Addr& taddr_) const
{
  IPV6Addr& addr_ = (IPV6Addr&)taddr_;
  return ( (addrl[0]==addr_.addrl[0]) && (addrl[1]==addr_.addrl[1]) &&
           (addrl[2]==addr_.addrl[2]) && (addrl[3]==addr_.addrl[3]) );
}

ostream& operator <<(ostream& os, const IPV6Addr& v6a)
{

  os << v6a.addrs[0] << ":" << v6a.addrs[1] << ":" << v6a.addrs[2] << ":"
     << v6a.addrs[3] << ":" << v6a.addrs[4] << ":" << v6a.addrs[5] << ":"
     << v6a.addrs[6] << ":" << v6a.addrs[7];

  return os;
}
