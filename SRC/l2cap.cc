// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//     
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


// Georgia Tech Network Simulator - Bluetooth L2CAP class
// George F. Riley, Georgia Tech, Spring 2004


//Implementation note:
//RTX timer on a L2CAP connection request should not start until the 
//physical link is established.

#include "lmp.h"
#include "l2cap.h"
#include "simulator.h"

using namespace std;

L2cap::L2cap() {
  pSignalChannel = new L2capSignalChannel(this);
  assembleBuf.usTotalLen = 0;
  assembleBuf.usOccupied = 0;
  assembleBuf.usChannelID = 0;
  assembleBuf.pBuf = NULL;

  pLMP = NULL;
  pLowerProtocol = NULL;
  pUpperProtocol = NULL;
}

L2cap::~L2cap() {
  delete pSignalChannel;
}

uShort 
L2cap::DataRequest(uShort usLen, uShort usLocalCID,
      void *pData) {
    
  L2capConnChannel *pChannel;
  uShort  usRemoteCID;
  
  pChannel = ChannelLookupByLocalCID(usLocalCID);
  assert(pChannel!=NULL);
  usRemoteCID = pChannel->GetRemoteChannelID();
  return SendL2CAPPacket(usLen, usRemoteCID, pData, pChannel);
  //return SendL2CAPPacket(usLen, ucLocalCID, pData, NULL);
}

void
L2cap::Segmentation(uShort usLen, uChar *pData) {
  //get the slot allowed through inquiry LMP
  //GetPacketSize();
  //segment it.
  //return the left.
  ;
}

void 
L2cap::Reassembly() {
  ;
}

//Note: The parameter ucLogicChannel is L2CAP_CH or L2CAP_CON_CH
//      , not the concept that other places show.
uShort
L2cap::DataIndication(uChar ucLogicChannel, uChar ucFlow, 
        uShort usLen, uChar *pData/*L2CAPPacket *pPacket*/) {
  
  //for sniff mode
  /*
  if(pLMP->IsSniff()) {
    uLong ulSniffAttempRem = pLowerProtocol->GetClk() - 
        pLMP->ulSniffClk;
    uLong ulSniffRem = (ulSniffAttempRem > pLMP->GetSniffTo()) ? 
        ulSniffAttempRem: pLMP->GetSniffTo();
    pLMP->CancelTimer(pLMP->SniffAttempTimeout, true);
    pLMP->ScheduleTimer(LMPEvent::SNIFFATTEMP_TIMEOUT, 
        pLMP->SniffAttempTimeout, ulSniffRem);
  }
  */

  int ErrCode = SUCCESS;
  L2CAPPacket *pPacket = (L2CAPPacket *)pData;
  uShort usChannelID = pPacket->usChannelID;
  if(ucLogicChannel == L2CAP_CH) {
    if(usLen!=pPacket->usLength + pPacket->HdrSize()) {
      //followed by some segments.
      //save usLength and save the data
      assembleBuf.usTotalLen = pPacket->usLength 
                  + pPacket->HdrSize();
      assembleBuf.usChannelID = pPacket->usChannelID;
      //if it is not empty, delete it to avoid memory leak
      if(assembleBuf.pBuf!=NULL)
        delete []assembleBuf.pBuf;
      assembleBuf.pBuf = new uChar[assembleBuf.usTotalLen];
      
      memcpy(assembleBuf.pBuf, pData, usLen);
      assembleBuf.usOccupied = usLen;
      return ErrCode;
    }
  }else {//L2CAP_CON_CH
    //pending to buffer
    assert(assembleBuf.usTotalLen!=0);
    if((assembleBuf.usOccupied + usLen) > 
      (assembleBuf.usTotalLen)) {
      cout<<"ERROR, discard"<<endl;
      delete []assembleBuf.pBuf;
      assembleBuf.pBuf = NULL;
      assembleBuf.usTotalLen = 0;
      assembleBuf.usOccupied = 0;
      return ErrCode;
    }
    memcpy(assembleBuf.pBuf + assembleBuf.usOccupied,
      pData, usLen);
    assembleBuf.usOccupied += usLen;
    if(assembleBuf.usOccupied != assembleBuf.usTotalLen)
      return ErrCode;
    else {//Caution: possible memory leak!
      usChannelID = assembleBuf.usChannelID;
      pPacket = (L2CAPPacket *)(assembleBuf.pBuf);
    }
  }
  //end of the assemble  
  
  if(usChannelID == SIGNAL_CHANNEL)
    //NOTE: currently not care the last parameter!!!
    pSignalChannel->DataIndication(pPacket, NULL);
  else { //connection channel or connection-less channel
    L2capConnChannel *pChannel = ChannelLookupByLocalCID(usChannelID);
    if (pChannel!=NULL)
      pChannel->DataIndication(pPacket);
    else{
      cout <<"Channel does not exist.\n";
      ErrCode = BT_ERROR;
    }
  }
  
  return ErrCode;
}

/*
//Request a callback when the selected event indication event occurs.
uShort
L2cap::EventIndication(Event_t event, (void *functionCallback)(BdAddr BD_Addr, 
            uLong CID, uLong PSM, uLong Identifier,uLong OutMTU, 
            void *pInFlow, uLong inFlushTO)){ //type to be cfmed.
        //some callback functions does not use some of parameters.
  switch (event) {
    case L2CA_CONNECTIND:
      (*functionCallback)(BD_Addr, CID, PSM, Identifier);// L2CA_ConnectInd;
      break;
    case L2CA_CONFIGIND:
      (*functionCallback)(CID, OutMTU, InFlow, InFlushTO);// L2CA_ConfigInd;
      break;
    case L2CA_DISCONNECTIND:
      (*functionCallback)(CID);// L2CA_DisconnectInd;
      break;
    case L2CA_QOSVIOLATIONIND:
      (*functionCallback)(BD_ADDR);// L2CA_QosViolation;
      break;
    default:
      printf("Reserved or Reserved for future use.\n");
      break;
  }
  
  return 0x0000;//Event successfully registered. 0x0001 on event registration failed.
}
*/

//Build the L2CAP packet and send it
uShort
L2cap::SendL2CAPPacket(uShort length, uShort channelID, 
        void *payload, L2capConnChannel *pChannel) {
  assert(payload != NULL);
  L2CAPPacket *pL2capPacket = NULL;
  uShort size = length + pL2capPacket->HdrSize();
  uChar *pTemp = new uChar[size];
  pL2capPacket = (L2CAPPacket *)pTemp;

  //build L2CAP packet.
  pL2capPacket->usLength = length;
  pL2capPacket->usChannelID = channelID;
  memcpy(pL2capPacket->ucPayload, payload, length);
  
  if(pChannel == NULL)
    pChannel = ChannelLookupByRemoteCID(channelID);
  assert(pChannel!=NULL);
  BdAddr *pAddr = pChannel->GetRemoteAddr();
  //segmentation here.
  uChar ucAllowedSlot = pLMP->GetMaxSlot();
  uShort usStandardSize = 0;
  //note:also depends on the fec coding scheme.
  switch(ucAllowedSlot) {
    case 1:
      usStandardSize = 17; //DM1 assume there is FEC
      break;
    case 3:
      usStandardSize = 121; //DM3 assume there is FEC
      break;
    case 5:
      usStandardSize = 224; //DM5 assume there is FEC
      break;
    default:
      cout<<"Wrong in slot"<<endl;
      break;
  }
  cout <<"usStandardSize = "<<usStandardSize <<endl;  
  if(size > usStandardSize ) {//need segmentation
    cout<<"== Segementation starts. =="<<endl;
    //pointer to the location of data copied so far
    uChar *pWhere = pTemp;
    uChar *pSegment = new uChar[usStandardSize];
    memcpy(pSegment, pWhere, usStandardSize);
    //send the first packet.
    pLowerProtocol->DataRequest((uChar)L2CAP_CH, usStandardSize, 
          pSegment, *pAddr);
          //(uChar *)pL2capPacket, *pAddr);
    size = size - usStandardSize;
    pWhere = pWhere + usStandardSize;
    while(size>0) {
      if(size > usStandardSize){
        pSegment = new uChar[usStandardSize];
        memcpy(pSegment, pWhere, usStandardSize);
        pLowerProtocol->DataRequest((uChar)L2CAP_CON_CH, usStandardSize, 
          pSegment, *pAddr);
          //(uChar *)pWhere, *pAddr);
        size =size - usStandardSize;
        pWhere = pWhere + usStandardSize;

      }else { //last segment
        pSegment = new uChar[size];
        memcpy(pSegment, pWhere, size);
        pLowerProtocol->DataRequest((uChar)L2CAP_CON_CH, size, 
          pSegment, *pAddr);
          //(uChar *)pWhere, *pAddr);
        break; //break the while loop
      }
        
    }
    
    delete pTemp;
  
  }else
    pLowerProtocol->DataRequest((uChar)L2CAP_CH, size, 
          (uChar *)pL2capPacket, *pAddr);
  size = length + pL2capPacket->HdrSize();
  return size; //the overall packet size.
}

//Lookup L2capChannel via ChannelID;
L2capConnChannel * 
L2cap::ChannelLookupByLocalCID(uShort usChannelID) {
  L2capChannelList::iterator  i = ChannelList.begin();    
  for(; i!=ChannelList.end(); i++) {
  //for(ChannelList::iterator i= ChannelList.begin(); i!=ChannelList.end(); i++) {
    if(usChannelID == (*i)->GetLocalChannelID())
      return (L2capConnChannel *)(*i);

  }
  return NULL;
}

//Lookup L2capChannel via remote ChannelID;
L2capConnChannel * 
L2cap::ChannelLookupByRemoteCID(uShort usChannelID) {
  L2capChannelList::iterator  i = ChannelList.begin();    
  for(; i!=ChannelList.end(); i++) {
  //for(ChannelList::iterator i= ChannelList.begin(); i!=ChannelList.end(); i++) {
    if(usChannelID == (*i)->GetRemoteChannelID())
      return (L2capConnChannel *)(*i);

  }
  return NULL;
}

//Get available local channel ID;
uShort 
L2cap::AllocateChannelID (void){
    
  uShort usChannelID = 0x003F; //starts the local channel ID from 0x0040;
  
  do {
    usChannelID++;
  }while(ChannelLookupByLocalCID(usChannelID)!=NULL);

  return usChannelID;
}

//Put the new channel into channel list for management.
bool 
L2cap::ChannelInsert(L2capConnChannel *pChannel) {
  assert(pChannel!=NULL);
  ChannelList.push_back(pChannel);
  return true;
}

      
//Shutdown a channel, free related channel ID ,etc.
bool
L2cap::ChannelClose(uShort usLocalCID){
    
  L2capConnChannel *pChannel = NULL;
  L2capChannelList::iterator i = ChannelList.begin();
    for(; i!=ChannelList.end(); i++) {
    if(usLocalCID == (*i)->GetLocalChannelID()){
      pChannel = (*i);
      
      //erase it fom ChannelList
      ChannelList.erase(i);
      
      //Free the channel resources.
      delete pChannel;
      cout<<"Channel "<<usLocalCID<<" closed now"<<endl;
      return true; 
    }
   }
    return false;  
}

uShort 
L2cap::L2CA_ConnectReq(uShort PSM, BdAddr BD_Addr, 
    uShort *pLCID, uShort *pStatus){
    
  //check whether  lower protocol exists.
  if(pLowerProtocol==NULL){
    cout<<"the lower protocol not installed."<<endl;
    exit(1);
  }

  //create a premature channel 
  uShort usChannelID;
  
  L2capConnChannel *pChannel = new L2capConnChannel(PSM);
  assert(pChannel!=NULL);
  pChannel->pL2CAP = this; //also point to the L2CAP entity.

  usChannelID = AllocateChannelID();
  memcpy(pLCID, &usChannelID, sizeof(usChannelID));
  pChannel->SetRemoteAddr(BD_Addr);
  pChannel->SetLocalAddr(pLowerProtocol->GetMyAddr());
  
  pChannel->pSignalChannel =  pSignalChannel;

  pChannel->SetLocalChannelID(*pLCID);
  
  //put the premature channel into channel list
  ChannelInsert(pChannel);
  
  return pSignalChannel->L2CA_ConnectReq(PSM, BD_Addr,pLCID, 
          (uChar)ILLEGAL_COMMAND_ID, // first time send
          pStatus, pChannel);
  
}

//Issues a response to a connection request event indication.
uShort
L2cap::L2CA_ConnectRsp(BdAddr BD_Addr,uChar ucIdentifier,
        uShort LCID, uShort response, uShort status) {

  //Check the command match in pSignalChannel->L2CA_ConnectRsp.
  return pSignalChannel->L2CA_ConnectRsp(BD_Addr, ucIdentifier, LCID, 
          response, status);
}

//Initiates the sending of an L2CAP_ConfigReq msg and blocks until a corresponding
//     L2CA_ConfigCfm(Neg) or L2CA_TimeOutInd events received.
uShort
L2cap::L2CA_ConfigReq(uShort CID, uShort InMTU, void *pOutFlow, 
        uShort OutFlushTO, uShort LinkTO, uShort *pInMTU_O,
           void *pOutFlow_O, uShort *pOutFlushTO_O){
  //output parameters: those variables with extention "_O" + result
  L2capConnChannel *pChannel = ChannelLookupByLocalCID(CID);  

  assert(pChannel!=NULL);
  
  switch(pChannel->usState) {
    case CLOSED:
       pUpperProtocol->L2CA_ConfigCfmNeg(CID); //or pass the message.
      break;
    case CONFIG: 
      cout<<"CONFIG state, send ConfigReq"<<endl;
      pSignalChannel->L2CA_ConfigReq(CID, InMTU, pOutFlow, OutFlushTO,
          LinkTO, pInMTU_O, pOutFlow_O, pOutFlushTO_O,
          (uChar)ILLEGAL_COMMAND_ID,
             pChannel);
      pSignalChannel->SaveCfgPara(CID, InMTU, pOutFlow, OutFlushTO, LinkTO,
              pInMTU_O, pOutFlow_O, pOutFlushTO_O);
      //save these parameters into a structure so that we could redo
      //when timeout.
      break;
    case OPEN:
      //TODO:suspend the data transmission.
      pSignalChannel->L2CA_ConfigReq(CID, InMTU, pOutFlow, OutFlushTO,
          LinkTO, pInMTU_O, pOutFlow_O, pOutFlushTO_O, 
          (uChar)ILLEGAL_COMMAND_ID,
          pChannel);
      break;
    default:
      cout <<"State = " << pChannel->usState <<"Do nothing? "<<endl;
      break;
  }

  return 0x0000;
}

//issues a response to a configuration request event indication.
uShort
L2cap::L2CA_ConfigRsp(uShort LCID, uShort OutMTU, uChar ucIdentifier, 
      void *pInFlow, uShort result) {
    
  L2capConnChannel *pChannel = ChannelLookupByLocalCID(LCID);

  assert(pChannel!=NULL);
  assert(pChannel->usState == CONFIG);
  //if all requests are positive responded, 
  //move into OPEN state
  if(result == SUCCESS)
    pChannel->usState = OPEN;

  return pSignalChannel->L2CA_ConfigRsp(LCID, OutMTU, 
          //(uChar)0, //Command Id, Note: I put zero temporarily, may cause trouble.
          ucIdentifier,
          result, pInFlow, pChannel);
}

//requests the disconnection of the channel.
uShort
L2cap::L2CA_DisconnectReq(uShort CID) {
    
  L2capConnChannel *pChannel = ChannelLookupByLocalCID(CID);
  assert(pChannel!=NULL);
  
  //Have Signal Channel do the job now.
  pSignalChannel->L2CA_DisconnectReq(CID, (uChar)ILLEGAL_COMMAND_ID);
  pChannel->usState = W4_L2CAP_DISCONNECT_RSP;
  
  return 0x0000;
}

uShort
L2cap::L2CA_DisconnectRsp(uShort usLocalCID, uChar ucIdentifier) {
  
  L2capConnChannel *pChannel = ChannelLookupByLocalCID(usLocalCID);
  assert(pChannel!=NULL);
  
  pSignalChannel->L2CA_DisconnectRsp(usLocalCID, ucIdentifier);
  assert(pChannel->usState == W4_L2CA_DISCONNECT_RSP);
  pChannel->usState = CLOSED;

  //free the channel now.
  if(ChannelClose(usLocalCID) == false)
    cout <<"Error, no such channel exists.\n";
  
  return 0x0000;
}

//Requests the transfer of data across the channel
void
L2cap::L2CA_DataWriteReq(uShort usCID, uShort usLength, 
        void *OutBuffer) {
  L2capConnChannel *pChannel = ChannelLookupByLocalCID(usCID);
  if(pChannel==NULL) {
    cout << "Error, channel ID does not exist.\n"<<endl;
    return;
  }
  if(pChannel->usState!=OPEN)
    cout<<"Channel is not ready! "<<endl;
  else{
    uShort usRemoteCID = pChannel->GetRemoteChannelID();
    SendL2CAPPacket(usLength, usRemoteCID, OutBuffer, pChannel);
  }
  return;

}

uShort
L2cap::L2CA_DataWriteCfm(uLong *size ){
  return 0x0000;//successful write
//  return 0x0001;//error flush timeout expires
//  return 0x0002;//error link termination
}

//The use of this primitive requests for the reception of data. This request returns
// when data is available or the link is terminated.
uShort
L2cap::L2CA_DataRead(uLong CID, uLong length, void *pInBuffer, uLong *pN) {
    //length: the size of the pInBuffer to store message.
    //pInBuffer: address of the buffer used to store the message
    //N: number of bytes transferd to pInBuffer.
  return 0x0000;//success
//  return 0x0001;//unsuccessful.
}

//Requests to create a CID to represent a logical connection to multi-devices
uShort
L2cap::L2CA_GroupCreate(uLong PSM) {
//  return 0xXXXX;//CID
  return 0x0000;
}

//Closes down a group
uShort
L2cap::L2CA_GroupClose(uLong CID) {
  return 0x0000;//successful closure of the channel
//  return 0x0001;//invalid CID
}

//Requests the addition of a member to a group
uShort
L2cap::L2CA_GroupAddMember(uLong CID, BdAddr BD_Addr) {
  return 0x0000;// success
//  return 0x0001;// failure to establish connection to remote device
  //other reserved
}

// requests the removal of a member from a group.
uShort
L2cap::L2CA_GroupRemoveMember(uLong CID, BdAddr BD_Addr) {
  return 0x0000;//success
//  return 0x0001;//failure device not a member of the group.
}

// request a report of the member of a group
uShort
L2cap::L2CA_GroupMembership(uLong CID, uLong *pN, 
        void *BD_Addr_List) {
  return 0x0000;// success
//  return 0x0001;// failure group does not exist
  //other reserved
}

//Initiates an L2CA_EchoReq message and receive the corresponding L2CAP_EchoRsp message
uShort
L2cap::L2CA_Ping(BdAddr BD_ADDR, uLong length,void *pEchoData, 
        uLong size) {
  return 0x0000;//success
//  return 0x0001;//timeout occured.
}

uShort
L2cap::L2CA_DisableCLT(uLong PSM) {
  return 0x0000;//success
//  return 0x0001;// not supported.

}

uShort L2cap::L2CA_EnableCLT(uLong PSM) {
  return 0x0000;//success
//  return 0x0001;//failed not support
}

//
// Signalling channel class method
// 
L2capSignalChannel::L2capSignalChannel(L2cap *pL2CAP) {
    
  SetRemoteChannelID(SIGNAL_CHANNEL);
  SetLocalChannelID(SIGNAL_CHANNEL);
  this->pL2CAP = pL2CAP;

  usMTU = 341;
  pTxQueue = NULL;
  
}

L2capSignalChannel::~L2capSignalChannel() {
  if(pTxQueue)
    delete pTxQueue;
}

//Build the command
//NOTE: maybe data consist of more than 1 command!.
uShort
L2capSignalChannel::BuildCmd(uChar code, 
    uChar identifier, uShort length, 
          void *data, void *commandBuf) {
  
  assert(commandBuf!=NULL);

  uShort size = 0;
  CommandPacket *pCommandPacket = (CommandPacket *)commandBuf;
  
  pCommandPacket->ucCode = code;
  pCommandPacket->ucID = identifier;
  pCommandPacket->usLength = length;
  if(data!=NULL)
    memcpy(pCommandPacket->ucData,data, length); 
  
  size = length + pCommandPacket->HdrSize();
  return size;//the valid length data in commandBuf;
}

uShort
L2capSignalChannel::DataIndication(L2CAPPacket *pPacket,
        L2capConnChannel *pChannel) {
    
  assert(pPacket->usChannelID == SIGNAL_CHANNEL);
  CommandPacket *pCommandPacket = 
      (CommandPacket *)&(pPacket->ucPayload);
  uShort nLength = pPacket->usLength;
  uShort nCurLen = pCommandPacket->usLength; //Command size
  uShort ErrCode = SUCCESS;
  
  if((pPacket->usLength > this->usMTU)) {
    if (pCommandPacket->ucCode % 2){ //kinds of response
      CmdReject Reject;
      uShort usMTU = GetMTU();
      char data[4];
      char CommandPacketBuf[48];
      uShort nLength;
      
      Reject.usReason = REASON_MTUEXCEED;
      memcpy(&data, &Reject, sizeof(Reject));
      memcpy(&data[2], &usMTU, sizeof(usMTU));
      
      nLength = BuildCmd(COMMAND_REJECT,pCommandPacket->ucID,sizeof(data), 
            data, CommandPacketBuf);
      pL2CAP->SendL2CAPPacket(nLength, SIGNAL_CHANNEL, 
              CommandPacketBuf,pChannel);
      
    }
    else {
      //void *pTemp = pPacket->ucPayload;
      //delete pTemp;//discard the packet silently
      //delete pPacket;
      return ErrCode;
    }
    return ErrCode;
  }
  if(pCommandPacket->ucCode > COMMAND_INFO_RESP){
    CmdReject Reject;
    char CommandPacketBuf[48];
    uShort nLength;
    
    Reject.usReason = REASON_NOTUNDERSTAND;
    nLength = BuildCmd(COMMAND_REJECT,pCommandPacket->ucID, sizeof(Reject),
            &Reject, CommandPacketBuf);
    pL2CAP->SendL2CAPPacket(nLength, SIGNAL_CHANNEL, 
            CommandPacketBuf, pChannel);
    return ErrCode;

  }
  do {
    //process the command;
    if(Dispatch(pCommandPacket, pChannel)!=SUCCESS) {
      cout << "Error. \n";
      //ErrCode; 
      break;
    }
    nLength = nLength - (nCurLen + pCommandPacket->HdrSize());
    pCommandPacket = (CommandPacket *)((uChar *)pCommandPacket + nCurLen + 
        pCommandPacket->HdrSize());
    nCurLen = pCommandPacket->usLength;
  }while(nLength!=0);
  
  //discard the packet after process
  //WRONG!!
  //void *pTemp = pPacket->ucPayload;
  //delete pTemp;
  
  //delete pPacket;

  return ErrCode;
  
}

uShort
L2capSignalChannel::Dispatch(CommandPacket *pCommand,
        L2capConnChannel *pChannel) {
    
  uShort ErrCode = SUCCESS;
  
  cout<<"Received a command w/ code = "<<int(pCommand->ucCode)<<endl;
  switch (pCommand->ucCode) {
    case COMMAND_REJECT:
      cout<<"Oh, rejected? "<<endl;
      RecvReject(pCommand);
      break;
    case COMMAND_CONN_REQ:
      RecvConnReq(pCommand);
      break;
    case COMMAND_CONN_RESP:
      {
      //not good,because lookup twice, here and RecvConnResp
      CmdConnResp *pConnResp = (CmdConnResp *)&(pCommand->ucData);
      //Get the channel
      pChannel = pL2CAP->ChannelLookupByLocalCID(
          pConnResp->usSrcCID);
      //check against the command list.
      if(CmdIdLookup(pChannel,pCommand->ucID)!=true){
        cout <<"invalid identifier. Discard";
        break;
      }
      RecvConnResp(pCommand);
      }
      break;
    case COMMAND_CFG_REQ:
      RecvCfgReq(pCommand);
      //process
      break;
    case COMMAND_CFG_RESP:
      {
      //not good,because lookup twice, here and RecvConnResp
      CmdCfgResp *pCfgResp = (CmdCfgResp *)&(pCommand->ucData);
      //Get the channel
      pChannel = pL2CAP->ChannelLookupByLocalCID(
          pCfgResp->usSrcCID);
      //check against the command list
      if(CmdIdLookup(pChannel, pCommand->ucID)!=true){
        cout <<"invalid identifier. Discard";
        break;
      }
      RecvCfgResp(pCommand, pChannel);
      }
      break;
    case COMMAND_DISCONN_REQ:
      RecvDisconnReq(pCommand);
      break;
    case COMMAND_DISCONN_RESP:
      {
      //not good,because lookup twice, here and RecvConnResp
      CmdDisconnResp *pDisconnResp = (CmdDisconnResp *)&(pCommand->ucData);
      //Get the channel
      pChannel = pL2CAP->ChannelLookupByLocalCID(
          pDisconnResp->usSrcCID);
      //check against the command list
      if(CmdIdLookup(pChannel,pCommand->ucID)!=true){
        cout <<"invalid identifier. Discard";
        break;
      }
      RecvDisconnResp(pCommand);
      }
      break;
    case COMMAND_ECHO_REQ:
      //process
      break;
    case COMMAND_ECHO_RESP:
      //process
      break;
    case COMMAND_INFO_REQ:
      //process
      break;
    case COMMAND_INFO_RESP:
      //process
      break;
    case COMMAND_RESERVED:
    default:
      cout << "Unsupported Command or Reserved. \n";
      break;
  }
  return ErrCode; //success.
}


//Initiates the sending of an L2CAP_ConnectReq msg and blocks until 
//a corresponding  L2CA_ConnectCfm(Neg) or L2CA_TimeOutInd events 
//received.
uShort 
L2capSignalChannel::L2CA_ConnectReq(uShort PSM, BdAddr BD_Addr, 
        uShort *pLCID, uChar ucId,  //command id
        uShort *pStatus, L2capConnChannel *pChannel){
            /* BdAddr type is 6 octs */
  //Input parameters:PSM, BD_Addr; Output parameters: pLCID, pStatus, and Result  
  uShort size = 0;
  CmdConnReq request;
  char commandPacket[16];
  
  L2capCmdId CommandID;
     if(ucId == ILLEGAL_COMMAND_ID)
    CommandID = CmdIdAlloc(pChannel); //Get a Command ID
  else
    CommandID.ucIdentifier = ucId; //passed in, used when resend the request!
  
  request.usPSM = PSM;
  request.usSrcCID = *pLCID; //local channel id.
  
  size = BuildCmd(COMMAND_CONN_REQ,//connection request
       CommandID.ucIdentifier,//identifier.
       0x0004,//payload size;
       &request,//data
       &commandPacket); //command including hdr;
  pL2CAP->SendL2CAPPacket(size, SIGNAL_CHANNEL, &commandPacket, 
          pChannel);
      
  pChannel->usState = W4_L2CAP_CONNECT_RSP;
  //In this state, L2CAP_ConnReq message has been sent referencing this CID
  //now waiting for the corresponding L2CAP_ConnRsp message.
  
  //start timer.
  pChannel->ScheduleTimer(L2CAPTimerEvent::CONNREQ_TIMEOUT, 
          CommandID.ucIdentifier, pChannel->pConnReqTimeout,
             TIMEOUT_VALUE); 

  return 0x0000;//result 0x0001 0x0002, 0x0003, 0xEEEE
}
        
//Issues a response to a connection request event indication.
uShort 
L2capSignalChannel::L2CA_ConnectRsp(BdAddr BD_Addr,uChar ucIdentifier,
        uShort LCID, uShort usRspValue, uShort status) {

  uShort size = 0;
  CmdConnResp response;
  char commandPacket[16];

  L2capConnChannel *pChannel = pL2CAP->ChannelLookupByLocalCID(LCID);
  assert(pChannel!=NULL);
  
  response.usDstCID = LCID;
  response.usSrcCID = pChannel->GetRemoteChannelID() ;
  response.usResult = usRspValue;
  response.usStatus = status;

  //check if any matched id.
  if(pChannel->ucPendingCmdId!=ucIdentifier) {
    cout << "No match command(request) id \n" <<endl;  
    return 0x0001;
  }
  
  size = BuildCmd(COMMAND_CONN_RESP,
          ucIdentifier,
          sizeof(response),
          &response,
          &commandPacket);
  pL2CAP->SendL2CAPPacket(size, SIGNAL_CHANNEL, &commandPacket, pChannel);
  
  //reset to ILLEGAL command ID after sending response.
  pChannel->ucPendingCmdId = (uChar)ILLEGAL_COMMAND_ID;
      
  if(usRspValue == 0x0000){ //successful
    assert(pChannel->usState == W4_L2CA_CONNECT_RSP);  
    pChannel->usState = CONFIG;
  }
  else {
    pChannel->usState = CLOSED;
    
    //Close the connection channel and release related resources
    pL2CAP->ChannelClose(LCID);
  }

  return 0x0000;
}

//Initiates the sending of an L2CAP_ConfigReq msg and blocks until a corresponding
//     L2CA_ConfigCfm(Neg) or L2CA_TimeOutInd events received.
uShort
L2capSignalChannel::L2CA_ConfigReq(uShort CID, uShort InMTU, 
      void *pOutFlow, uShort OutFlushTO, uShort LinkTO, 
      uShort *pInMTU_O, void *pOutFlow_O, 
      uShort *pOutFlushTO_O, 
      uChar ucId, //command ID
      L2capConnChannel *pChannel){
  cout<<"Send Config Request"<<endl;  
  uShort size = 0; //command packet size
  uShort usLength = 0; //the whole command payload size.
  CfgParamOpt optMTU;
  CfgParamOpt optFlushTimeout;
  uChar commandPacket[32];
  uChar CommandPayload[16];
  uChar *pTemp;

  pTemp = (uChar *)&CommandPayload;

  //check the state of the channel.
  assert(pChannel!=NULL);
/*  if(pChannel->usState!=OPEN) {
    cout<<"Attemp to configure in non-open state."<<endl;
    cout<<"State is"<<pChannel->usState<<endl;
    return 0x0000;
  }
*/
  CmdCfgReq cfgReq;
  cfgReq.usDstCID = pChannel->GetRemoteChannelID();
  cfgReq.usFlags = 0x00; //TODO: review
  memcpy(pTemp, &cfgReq, sizeof(cfgReq));
  pTemp = pTemp + sizeof(cfgReq);  
  
  usLength = usLength + sizeof(cfgReq);
  
  L2capCmdId CommandID;

     if(ucId == ILLEGAL_COMMAND_ID)
    CommandID = CmdIdAlloc(pChannel); //Get a Command ID
  else
    CommandID.ucIdentifier= ucId; //passed in, used when resend the request!
  
  uShort usMTU = pL2CAP->GetMTU(CID); //Get MTU of this channel
  optMTU.ucType = TYPE_MTU;
  optMTU.ucLength = sizeof(usMTU); //MTU is 2 bytes.
  
  memcpy(pTemp, &optMTU, sizeof(optMTU));
  pTemp = pTemp + sizeof(optMTU);
  memcpy(pTemp, &usMTU, sizeof(usMTU));
  pTemp = pTemp + sizeof(usMTU);
      
  usLength = usLength + sizeof(optMTU) +sizeof(usMTU);
  
  uShort usTimeO = pL2CAP->GetFlushTimeout(CID);
  optFlushTimeout.ucType = TYPE_FLUSHTIMEO;
  optFlushTimeout.ucLength = sizeof(usTimeO);
  
  memcpy(pTemp, &optFlushTimeout, sizeof(optFlushTimeout));
  pTemp = pTemp + sizeof(optFlushTimeout);
  memcpy(pTemp, &usTimeO, sizeof(usTimeO));

  usLength = usLength + sizeof(optFlushTimeout) + sizeof(usTimeO);
  
  size = BuildCmd(COMMAND_CFG_REQ,
          CommandID.ucIdentifier,//identifier.
          usLength,
          &CommandPayload,
          &commandPacket);
  pL2CAP->SendL2CAPPacket(size, SIGNAL_CHANNEL, &commandPacket,pChannel);
  //TODO: support QOS negotiated.
  
  //start RTX timer.
  pChannel->ScheduleTimer(L2CAPTimerEvent::CFGREQ_TIMEOUT,
          CommandID.ucIdentifier, 
          pChannel->pCfgReqTimeout, TIMEOUT_VALUE); 
  //output parameters: those variables with extention "_O" + result
  return 0x0000; //configuration is successful.
           //0x0001 failure invalid CID, 0x0002 unacceptable parameters.
          //0x0003 signalling MTU exceeded, 0x0004 unknown options
           //0xEEEE configurgation timeout occured.
}

//issues a response to a configuration request event indication.
uShort 
L2capSignalChannel::L2CA_ConfigRsp(uShort LCID, uShort OutMTU,
        uChar ucIdentifier, uShort Result, void *pInFlow,
        L2capConnChannel *pChannelIn) {//not used.
  
  CmdCfgResp ConfigResp;
  int size =0;
  char commandPacket[32];
  
  L2capConnChannel *pChannel = pL2CAP->ChannelLookupByLocalCID(LCID);
  assert(pChannel!=NULL);
  
  //check if any matched id.
  if(pChannel->ucPendingCmdId!=ucIdentifier) {
    cout << "No match command(request) id \n";  
    return BT_ERROR;
  }

  ConfigResp.usSrcCID = pChannel->GetRemoteChannelID();
  ConfigResp.usFlags = 0x00; //Not continue.
  ConfigResp.usResult = Result; //the result is a parameter input
  ConfigResp.usConfig = 0x00;

  //reset to ILLEGAL command ID after sending response.
  pChannel->ucPendingCmdId = (uChar)ILLEGAL_COMMAND_ID;

  size = BuildCmd(COMMAND_CFG_RESP,
          ucIdentifier, //a command id responding to request
          sizeof(ConfigResp),
          &ConfigResp,
          commandPacket);
  pL2CAP->SendL2CAPPacket(size, SIGNAL_CHANNEL, &commandPacket, pChannel);
    
  return 0x0000;
}

//requests the disconnection of the channel.
uShort 
L2capSignalChannel::L2CA_DisconnectReq(uShort usLocalCID,
        uChar ucId) {
  
  CmdDisconnReq DisconnReq;
  L2capConnChannel *pChannel = pL2CAP->ChannelLookupByLocalCID(usLocalCID);
  int size = 0;
  char CommandPacket[16];

  L2capCmdId CommandID; 
     if(ucId == ILLEGAL_COMMAND_ID)
    CommandID = CmdIdAlloc(pChannel); //Get a Command ID
  else
    CommandID.ucIdentifier = ucId; //passed in, used when resend the request!
  
  DisconnReq.usSrcCID = usLocalCID;
  DisconnReq.usDstCID = pChannel->GetRemoteChannelID();
      //Get it from associated from channel
  size = BuildCmd(COMMAND_DISCONN_REQ,
          CommandID.ucIdentifier,
          sizeof(DisconnReq),
          &DisconnReq,
          CommandPacket);
  pL2CAP->SendL2CAPPacket(size, SIGNAL_CHANNEL, &CommandPacket, pChannel);
  
  //start RTX timer.
  pChannel->ScheduleTimer(L2CAPTimerEvent::DISCONNREQ_TIMEOUT,
          CommandID.ucIdentifier, 
          pChannel->pDisconnReqTimeout, TIMEOUT_VALUE); 
  
  return 0x0000;// disconnection successful : indicating receiving DisconnectRsp.
//  return 0xEEEE;// disconnection timeout
}

//Issue a disconnection response to matched request 
uShort 
L2capSignalChannel::L2CA_DisconnectRsp(uShort usLocalCID, 
      uChar ucIdentifier) {
  
  int size = 0;
  uShort ErrCode = SUCCESS;
  CmdDisconnResp DisconnResp;
  char CommandPacket[16];
  L2capConnChannel *pChannel = pL2CAP->ChannelLookupByLocalCID(usLocalCID);

  //check if any matched id.
  if(pChannel->ucPendingCmdId!=ucIdentifier) {
    cout << "No match command(request) id \n";  
    return ErrCode;
  }
  DisconnResp.usDstCID = usLocalCID;
  DisconnResp.usSrcCID = pChannel->GetRemoteChannelID();
  
  size = BuildCmd(COMMAND_DISCONN_RESP,
      ucIdentifier,//Get from matching disconn request
      sizeof(DisconnResp),
      &DisconnResp,
      CommandPacket);

  //reset it to ILLEGAL one
  pChannel->ucPendingCmdId = (uChar)ILLEGAL_COMMAND_ID;
  
  pL2CAP->SendL2CAPPacket(size, SIGNAL_CHANNEL, &CommandPacket, pChannel);
  
  return ErrCode;
  
}

//Command ID management function
L2capCmdId 
L2capSignalChannel::CmdIdAlloc(L2capConnChannel *pChannel){
  
  uChar ucIdentifier = 0x00; //start from 0x01;
  L2capCmdId CmdId;
  Time_t now = Simulator::Now();
  
  //erase old command id (360 seconds older.)
  CmdIdPurge(pChannel);

  do {
    ucIdentifier++;
  }while(CmdIdLookup(pChannel,ucIdentifier));
  
  CmdId.ucIdentifier = ucIdentifier;
  CmdId.dExpire = now + 360000; 

  //Insert into the CmdList now.  
  pChannel->CmdIdList.push_back(CmdId);
  return  CmdId;

}

//erase command with elapse time 360s, which means it can be
//reallocated soon.
void 
L2capSignalChannel::CmdIdPurge(L2capConnChannel *pChannel) {
  assert(pChannel!=NULL);
  double now = Simulator::Now();

  L2capCmdIdList::iterator i = (pChannel->CmdIdList).begin();
  for(/*CmdIdList::iterator i= pChannel->CmdIdList.begin()*/; 
          i!=pChannel->CmdIdList.end(); i++) {
    if(i->dExpire >= now) {
      pChannel->CmdIdList.erase(i);
      i--;
    }
  }
  return ;
}

bool 
L2capSignalChannel::CmdIdLookup(L2capConnChannel *pChannel,
        uChar ucId) {
    
  L2capCmdIdList::iterator i = (pChannel->CmdIdList).begin();
  for(/*CmdIdList::iterator i= pChannel->CmdIdList.begin()*/; 
          i!=pChannel->CmdIdList.end(); i++) {
    if(i->ucIdentifier == ucId)
      return true;

  }
  return false;
}

bool 
L2capSignalChannel::CmdIdRemove(L2capConnChannel *pChannel,
        uChar ucId) {
  
  L2capCmdIdList::iterator i = (pChannel->CmdIdList).begin();
  for(/*CmdIdList::iterator i= pChannel->CmdIdList.begin()*/; 
          i!=pChannel->CmdIdList.end(); i++) {
    if(i->ucIdentifier == ucId) {
      pChannel->CmdIdList.erase(i);
      i--;
      return true;
    }
  }
  cout <<"Error: no such Command ID";
  return false;
}

//receiving a reject signalling command.
uShort
L2capSignalChannel::RecvReject(CommandPacket *pCommand) {

  CmdReject *pReject = (CmdReject *)&(pCommand->ucData);
  switch(pReject->usReason) {
    case 0x0000:
      cout<<"Command Not Understood."<<endl;
      break;
    case 0x0001:
      {
      cout<<"Signalling MTU Exceeded."<<endl;
      uShort usAcceptMTU = *((uShort *)((char *)&(pCommand->ucData)
          + sizeof(CmdReject)));
      cout<<"Accepted MTU = "<<usAcceptMTU<<endl;
      //TODO: Rejected..what should I do next?
      }
      break;
    case 0x0002:
      {
      cout<<"Invalid CID in request."<<endl;
      uShort usExpectedDstCID = *((uShort *)((char *)&(pCommand->ucData)
          + sizeof(CmdReject)));
      cout<<"Expected Dst CID = "<<usExpectedDstCID<<endl;
      //comments: still 2 bytes data followed.
      }
      break;
    default:
      break;
  }

  return 0x0000;
}

uShort 
L2capSignalChannel::RecvConnReq(CommandPacket *pCommand) {
    
  CmdConnReq *pConnReq = (CmdConnReq *)&(pCommand->ucData);
  
  //Create a primitive connection-orientied channel
  L2capConnChannel *pChannel = 
      new L2capConnChannel(pConnReq->usPSM);
  pChannel->pL2CAP = pL2CAP; //also point to the L2CAP entity.
  
  pChannel->SetLocalChannelID(pL2CAP->AllocateChannelID());
  pChannel->SetRemoteChannelID(pConnReq->usSrcCID);
  
  pChannel->pSignalChannel = this;
  pChannel->SetRemoteAddr(pL2CAP->pLowerProtocol->GetPeerAddr());
  pChannel->SetLocalAddr(pL2CAP->pLowerProtocol->GetMyAddr());
  
  assert(pChannel->usState == CLOSED);
  pChannel->usState = W4_L2CA_CONNECT_RSP;
  // In this state, remote end-point exists and L2CAP_ConnectReq
  // has been received. L2CA_ConnectInd has sent to upper layer
  // And L2CAP waits for the corresponding response.

  //put the premature channel into channel list
  pL2CAP->ChannelInsert(pChannel);

  //Set the pending request.
  pChannel->ucPendingCmdId = pCommand->ucID;
  //Pass command ID, local Channel ID, PSM ,etc up.
  pL2CAP->pUpperProtocol->L2CA_ConnectInd(pConnReq->usPSM, 
          pCommand->ucID, pChannel->GetLocalChannelID()); 
  
  //TODO: optionally send peer L2CAP_ConnectRspPnd msg
  
  return 0x0000;

}

//Note: response packet is defined very wierd in SPEC1.1
//DstChannelID is the  channel end-point who send this response
//SrcChannelID is the channel end-point who receives this resp
uShort 
L2capSignalChannel::RecvConnResp(CommandPacket *pCommand){
    
  CmdConnResp *pConnResp = (CmdConnResp *)&(pCommand->ucData);

  //Get the channel
  L2capConnChannel *pChannel = pL2CAP->ChannelLookupByLocalCID(
          pConnResp->usSrcCID);
  switch (pConnResp->usResult) {
    case 0x0000: //success
      //send upper layer "connection request" confirmation infomation.
      pChannel->SetRemoteChannelID(pConnResp->usDstCID);
      assert(pChannel->usState == W4_L2CAP_CONNECT_RSP);
      pChannel->usState = CONFIG;

      pL2CAP->pUpperProtocol->L2CA_ConnectCfm(pConnResp->usSrcCID);
      //disable RTX timer. reset the usTry of the conn channel
      pChannel->CancelTimer(pChannel->pConnReqTimeout, true);
      pChannel->ucMaxRetryReq = 0;
      break;
    case 0x0001: //pending
      pL2CAP->pUpperProtocol->L2CA_ConnectPnd(pConnResp->usSrcCID);
      pChannel->ucMaxRetryReq = 0;
      //TODO:disable RTX timer and start ERTX timer
      //pChannel->CancelTimer(pChannel->pConnReqTimeout, true);
      //pChannel->ScheduleTimer(....)
      break;
    case 0x0002:
      cout << "Connection refused -- PSM not supported\n"<<endl;
      //break;
    case 0x0003:
      cout << "Connection refused -- Security block\n" <<endl;
      //break;
    case 0x0004:
      cout << "Connection refused -- no resouces available.\n" <<endl;
      //tell upper layer "I am rejected."
      //disable RTX timer. reset the usTry of the conn channel
      pChannel->CancelTimer(pChannel->pConnReqTimeout, true);
      pL2CAP->pUpperProtocol->L2CA_ConnectCfmNeg(pConnResp->usSrcCID);
      pChannel->usState = CLOSED;
      pChannel->ucMaxRetryReq = 0;

      //shutdown the channel and free its resource.
      pL2CAP->ChannelClose(pChannel->GetLocalChannelID());
      break;
    default:
      cout <<"unsupported result. \n"<<endl;
      break;
      
  }
  
  return SUCCESS;
}

uShort 
L2capSignalChannel::RecvCfgReq(CommandPacket *pCommand) {
    
  CmdCfgReq *pCfgReq = (CmdCfgReq *)&(pCommand->ucData);

  cout<<"Command ID = "<<int(pCommand->ucID) <<endl;
  cout<<"Command length = "<<pCommand->usLength<<endl;

  //Get the channel
  L2capConnChannel *pChannel = pL2CAP->ChannelLookupByLocalCID(
          pCfgReq->usDstCID);
  
  if(pChannel == NULL) {
    cout<<"Channel is NULL?"<<__LINE__<<endl;
    //Send response directly. channel does not exist.
    CmdReject Reject;
    char CommandPacketBuf[48];
    int nLength;
    char data[6]; //reject + requested channel id.

    Reject.usReason = REASON_INVALIDCID;
    memcpy(data, &Reject, sizeof(Reject));
    memcpy(data + sizeof(Reject), &(pCfgReq->usDstCID), sizeof(uShort));
    data[4] = 0x00;
    data[5] = 0x00;
    
    nLength = BuildCmd(COMMAND_REJECT,pCommand->ucID, sizeof(data),
            &data, CommandPacketBuf);
    pL2CAP->SendL2CAPPacket(nLength, SIGNAL_CHANNEL, 
            CommandPacketBuf, pChannel);
    
    return SUCCESS;
  }

  switch(pChannel->usState) {
    case CLOSED:
      cout <<" Received Configure Request at CLOSED state.\n";
      //Send peer L2CAP_Reject message.
      break;
    case CONFIG:
      pChannel->ucPendingCmdId = pCommand->ucID;
      pL2CAP->pUpperProtocol->L2CA_ConfigInd(pCfgReq->usDstCID, 
              pCommand->ucID);
      break;
    case OPEN:
      //TODO: suspend data transmission at pChannel
      pChannel->ucPendingCmdId = pCommand->ucID;
      pL2CAP->pUpperProtocol->L2CA_ConfigInd(pCfgReq->usDstCID,
              pCommand->ucID);
      pChannel->usState = CONFIG;
      break;
    default:
      //discard it silently
      cout <<" Got config req at invalid state.\n";
      break;
  }
  
  return SUCCESS;
  
}

uShort 
L2capSignalChannel::RecvCfgResp(CommandPacket *pCommand,
      L2capConnChannel *pChannelOut) {
  
  CmdCfgResp *pCfgResp = (CmdCfgResp *)&(pCommand->ucData);

  //obtain the pChannel from pCfgResp->usSrcCID
  L2capConnChannel *pChannel = pL2CAP->ChannelLookupByLocalCID(
          pCfgResp->usSrcCID);
#define CONFIGED  0x88
  uChar another_direction = CONFIGED;
  pChannelOut = pChannel;
  switch(pCfgResp->usResult) {
    case 0x0000: //success
      if(another_direction == CONFIGED)
        pChannel->usState = OPEN;
      //TODO: update related options in pChannel;
      pL2CAP->pUpperProtocol->L2CA_ConfigCfm(pCfgResp->usSrcCID);
      //disable RTX timer. reset the usRetry of the channel
      pChannel->CancelTimer(pChannel->pCfgReqTimeout, true);
      pChannel->ucMaxRetryReq = 0;
      break;
    case 0x0001: //unacceptable parameters
      cout << "Failure -- unacceptable parameters \n";
      //TODO: extract the parameters to be accepted in configure options.
    case 0x0002:
      cout << "Failure -- rejected \n";
    case 0x0003:
      cout << "Failure -- unknown options. \n";
    default:
      pL2CAP->pUpperProtocol->L2CA_ConfigCfmNeg(pCfgResp->usSrcCID);

      //disable RTX timer. reset the usRetry of the channel
      pChannel->CancelTimer(pChannel->pCfgReqTimeout, true);
      pChannel->ucMaxRetryReq = 0;
      break;
  }
  return SUCCESS;
      
}

uShort 
L2capSignalChannel::RecvDisconnReq(CommandPacket *pCommand) {
    
  CmdDisconnReq *pDisconnReq = (CmdDisconnReq *)&(pCommand->ucData);
  
  L2capConnChannel *pChannel = pL2CAP->ChannelLookupByLocalCID(
          pDisconnReq->usDstCID);
  if(pChannel == NULL) {
    //Send response directly. channel does not exist.
    cout<<"The channel does not exist"<<endl;
    CmdReject Reject;
    char CommandPacketBuf[48];
    int nLength;
    char data[6]; //reject + requested channel id.

    Reject.usReason = REASON_INVALIDCID;
    memcpy(data, &Reject, sizeof(Reject));
    memcpy(data + sizeof(Reject), &(pDisconnReq->usDstCID), sizeof(uShort));
    data[4] = 0x00;
    data[5] = 0x00;
    
    nLength = BuildCmd(COMMAND_REJECT,pCommand->ucID, sizeof(data),
            &data, CommandPacketBuf);
    pL2CAP->SendL2CAPPacket(nLength, SIGNAL_CHANNEL, 
            CommandPacketBuf, pChannel);

    return SUCCESS;
  }
  //the requested channel must be fully matched.
  assert(pDisconnReq->usSrcCID == pChannel->GetRemoteChannelID());
  
  cout<<"Received disconnect request"<<endl;
  if(pChannel->usState == CLOSED) {
    pChannel->ucPendingCmdId = pCommand->ucID; //must set, otherwise next fails
    L2CA_DisconnectRsp(pDisconnReq->usDstCID, pCommand->ucID);
  } else {
    pChannel->usState = W4_L2CA_DISCONNECT_RSP;
    pChannel->ucPendingCmdId = pCommand->ucID;

    pL2CAP->pUpperProtocol->L2CA_DisconnectInd(pDisconnReq->usDstCID, 
            pCommand->ucID);
  }
  //free the command packet
  //delete pCommand;
  
  return SUCCESS;
}

uShort 
L2capSignalChannel::RecvDisconnResp(CommandPacket *pCommand) {
    
  CmdDisconnResp *pDisconnResp = (CmdDisconnResp *)&(pCommand->ucData);

  //Get pChannel from pDisconnResp->usSrcCID
  L2capConnChannel *pChannel = pL2CAP->ChannelLookupByLocalCID(
          pDisconnResp->usSrcCID);
  assert(pChannel!=NULL);
  assert(pDisconnResp->usDstCID == pChannel->GetRemoteChannelID());

  cout<<"Received Disconnect Response."<<endl;
  pChannel->usState = CLOSED;
  pL2CAP->pUpperProtocol->L2CA_DisconnectCfm();
  
  //disable RTX timer. reset ucMaxRetryReq of the channel
  pChannel->CancelTimer(pChannel->pDisconnReqTimeout, true);
  pChannel->ucMaxRetryReq = 0;
  
  //free the Command packet
  //delete pCommand;
  
  //Shutdown the channel and free its resources.
  pL2CAP->ChannelClose(pChannel->GetLocalChannelID());
  return 0x0000;
}

//
//class L2capConnChannel
//

L2capConnChannel::L2capConnChannel(uShort usPSM, uShort usOutMTU, 
  uShort usInFlushTO, uShort usInMTU, uShort usOutFlushTO) {
    
  usState = CLOSED;
  
  this->usOutMTU = usOutMTU;
  this->usInFlushTO = usInFlushTO;
  this->usInMTU = usInMTU;
  this->usOutFlushTO = usOutFlushTO;

  pL2CAP = NULL;
  pTxQueue = NULL;

  pConnReqTimeout = NULL;
  pCfgReqTimeout = NULL;
  pDisconnReqTimeout = NULL;

  this->usPSM = usPSM;
  ucMaxRetryReq = 0;

  //must reset to ILLEGAL_COMMAND_ID whenever "I" respond to
  //pending request.
  ucPendingCmdId = (uChar)ILLEGAL_COMMAND_ID; //illegal

  //QOS options.
}
  
L2capConnChannel::~L2capConnChannel() {
  if(pTxQueue)
    delete pTxQueue;
  if(pConnReqTimeout)
    delete pConnReqTimeout;
  if(pCfgReqTimeout)
    delete pCfgReqTimeout;
  if(pDisconnReqTimeout)
    delete pDisconnReqTimeout;
}

//Schedule timer for request(conn, cfg, and disconn)
void 
L2capConnChannel::ScheduleTimer(Event_t event,  
        uChar ucIdentifier, 
        L2CAPTimerEvent*& ev, Time_t t) {
  assert(ev==NULL);
  ev = new L2CAPTimerEvent(event, ucIdentifier);
  cout << "scheduleing a timer for ucIdentifier locally.\n";
  timer.Schedule(ev, t, this);
}

//Cancel the timer because of getting response.
void 
L2capConnChannel::CancelTimer(L2CAPTimerEvent*& event, 
        bool delTimer) {
  if(event!=NULL) {
    timer.Cancel(event);
    if (delTimer) { // Delete it
      delete event;
      event = nil;
    }
  }
  return;
}

//Called when timer expires.
//////////////////////////////////////////////////////////////
//Comments: when resend request, the request command id should
//          be the same with before. 
//It is specified in L2CAP specification
//////////////////////////////////////////////////////////////
void 
L2capConnChannel::Timeout(TimerEvent *pEvent) {
  uShort usLocalCID = this->GetLocalChannelID();
  BdAddr *pReAddr = this->GetRemoteAddr();
  L2CAPTimerEvent *pTimerEvent = (L2CAPTimerEvent *)pEvent;
  switch(pTimerEvent->event) {
    case L2CAPTimerEvent::CONNREQ_TIMEOUT:
      ucMaxRetryReq++;
      pConnReqTimeout = NULL;
      if(ucMaxRetryReq < MAX_TRY) {
        pSignalChannel->L2CA_ConnectReq(this->usPSM,
                *pReAddr,
                &usLocalCID, 
                pTimerEvent->ucRequestId, //must same with before
                NULL,
                this);
        if(pConnReqTimeout!=NULL)
          CancelTimer(pConnReqTimeout, true);
          
        ScheduleTimer(L2CAPTimerEvent::CONNREQ_TIMEOUT,
              pTimerEvent->ucRequestId,
              pConnReqTimeout,
              (ucMaxRetryReq + 1) * TIMEOUT_VALUE);
      }
      else
        //close the channel
        pL2CAP->ChannelClose(usLocalCID);

      break;
    case L2CAPTimerEvent::CFGREQ_TIMEOUT:
      ucMaxRetryReq++;
      pCfgReqTimeout = NULL;
      if(ucMaxRetryReq < MAX_TRY) {
        ConfigPara Cfg = pSignalChannel->GetCfgPara();
          pSignalChannel->L2CA_ConfigReq(Cfg.usCID,
                Cfg.usInMTU,
                Cfg.pOutFlow,
                Cfg.usOutFlushTO,
                Cfg.usLinkTO,
                Cfg.pInMTU_O,
                Cfg.pOutFlow_O,
                Cfg.pOutFlushTO_O,
                pTimerEvent->ucRequestId,
                this);
        if(pCfgReqTimeout!=NULL)
          CancelTimer(pCfgReqTimeout, true);
        ScheduleTimer(L2CAPTimerEvent::CFGREQ_TIMEOUT,
              pTimerEvent->ucRequestId,
              pCfgReqTimeout,
               (ucMaxRetryReq + 1) * TIMEOUT_VALUE);
      }
      else
        //close the channel
        pL2CAP->ChannelClose(usLocalCID);
        //pL2CAP->ChannelClose(this->usLocalChannelID);
      break;
    case L2CAPTimerEvent::DISCONNREQ_TIMEOUT:
      ucMaxRetryReq++;
      pDisconnReqTimeout = NULL;
      if(ucMaxRetryReq < MAX_TRY) {
          pSignalChannel->L2CA_DisconnectReq(
                usLocalCID,
                pTimerEvent->ucRequestId);
        if(pDisconnReqTimeout!=NULL)
          CancelTimer(pDisconnReqTimeout, true);
        ScheduleTimer(L2CAPTimerEvent::DISCONNREQ_TIMEOUT,
                pTimerEvent->ucRequestId,
                pDisconnReqTimeout,
                (ucMaxRetryReq + 1) * TIMEOUT_VALUE);
      }
      else
        //close the channel
        pL2CAP->ChannelClose(usLocalCID);
      break;
    default:
      cout << "unknown event type. \n";
      break;
  }
  //consume the event
  delete pEvent;
  return;
}

uShort 
L2capConnChannel::DataIndication(L2CAPPacket *pPacket) {
  uChar *pPayload = NULL;

  if(pPacket->usLength!=0) {
    pPayload = new uChar[pPacket->usLength];
    memcpy(pPayload, &(pPacket->ucPayload), pPacket->usLength);
  }
  //delete pPacket; //consumed the payload already.
  
  if(usState != OPEN){ //not support reconfiguration yet.
    cout <<"Discard packet because of wrong state. \n";
    return SUCCESS;
  }
  switch(usPSM) {
    case SDP:
      break;
    case RFCOMM:
      break;
    case TCS_BIN:
      break;
    case TCS_BIN_CORDLESS:
      break;
    case BNEP:
      {
      uShort usTmp = GetLocalChannelID();
      //pBNEP->DataIndication(pPacket, usLocalChannelID); 
      assert(pL2CAP!=NULL);
      assert(pL2CAP->pUpperProtocol!=NULL);
      pL2CAP->pUpperProtocol->DataIndication((void *)pPayload, 
              usTmp); 
          //this->GetLocalChannelID()):
        //usLocalChannelID uniquely determines the entity
      }
      break;
    case HID_CONTROL:
    case HID_INTERRUPT:
    case UPNP:
    case AVCTP:
    case AVDTP:
    case UDI_C_PLANE:
    default:
      cout <<"unsupported protocol" <<endl;
      break;
  }
  delete pPayload;

  return SUCCESS;
}          

uShort 
L2capConnChannel::DataRequest(uShort usLength, void *pData) {
  uShort usRemoteCID = this->GetRemoteChannelID();
  if(usState!=OPEN) { 
    cout <<" Error.Trying to send data in non-open state.\n" 
        <<endl;
    return BT_ERROR;
  }
  else
    pL2CAP->SendL2CAPPacket(usLength, usRemoteCID, pData, NULL);
  
  return SUCCESS;
}
