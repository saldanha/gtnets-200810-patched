// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: interface.h 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - Interface class
// George F. Riley.  Georgia Tech, Spring 2002

// Define class to model network link interfaces
// Implements the Layer 2 protocols

#ifndef __interface_h__
#define __interface_h__

#include "common-defs.h"
#include "interface-basic.h"
#include "handler.h"
#include "notifier.h"
#include "mask.h"
#include "node.h"
#include "macaddr.h"

class VirusThrottle;
class PortMatching;
class L2Proto;
class Node;
class Queue;
class LinkEvent;
class Link;
class EventCQ;

//Doc:ClassXRef
class Interface : public InterfaceBasic, public NotifyHandler {
  //Doc:Class Class interface is a base class for all interfaces.
  //Doc:Class The various methods for interfaces are documented
  //Doc:Class in the appropriate subclasses.

public:
  Interface(); 
  Interface(const L2Proto& l2 = L2Proto802_3(),
            IPAddr_t i   = IPADDR_NONE,
            Mask_t   m   = MASK_ALL,
            MACAddr  mac = MACAddr::NONE,
	    bool  bootstrap = false);
  Interface(const Interface&);
  virtual ~Interface();

  virtual bool Send(Packet*, IPAddr_t, int) = 0; // Enque and transmit
  virtual bool Send(Packet*, const MACAddr&, int) = 0; // Enque and transmit
  virtual bool Send(Packet*, Word_t) = 0; // Enque and transmit
  virtual bool Xmit(Packet*, IPAddr_t, int){ return false;} // Enque and transmit

  // Handler
  virtual void Handle(Event*, Time_t);
  // Notifier
  virtual void Notify(void*);
  

 //Doc:Method
  virtual void AddARPEntry(IPAddr_t, MACAddr) {}
  //Doc:Desc Adds a new arp entry to the interface arp cache
  //Doc:Arg1 The Ip address of the neighbour
  //Doc:Arg2 The corresponding Mac address


  //Doc:Method
  void         SetIPAddr(IPAddr_t i) { ip = i;}   // Set interface IPAddr
    //Doc:Desc Specify an \IPA\ associated with this interface.
    //Doc:Arg1 The \IPA\ to assign this interface.

  //Doc:Method
  IPAddr       GetIPAddr() const { return ip;}    // Get the associated ip addr
    //Doc:Desc Return the \IPA\ associated with this interface.
    //Doc:Return The \IPA\ associated with this interface.
    //Doc:Return Returns {\tt IPADDR_NONE}
    //Doc:Return if no \IPA\ is assigned.

  //Doc:Method
  Mask_t       GetIPMask() const { return mask;} // Get the ip addr mask
    //Doc:Desc Return the \IPA\ mask associated with this interface.
    //Doc:Return The \IPA\ mask associated with this interface.

  //Doc:Method
  void         SetMACAddr(const MACAddr& m);      // Set MAC addresses
    //Doc:Desc Set the {\em MAC} address associated with this interface.
    //Doc:Arg1 The {\em MAC} address for this interface.
  //Doc:Method
  MACAddr      GetMACAddr() const { return macAddr;} // Return local MAC addr
    //Doc:Desc Get the {\em MAC} address associated with this interface.
    //Doc:Return The {\em MAC} address for this interface.

  virtual Count_t  PeerCount() const = 0;         // Number of peers
  virtual IPAddr   PeerIP(int) const = 0;         // Get peer IP address
  virtual IPAddr   NodePeerIP(Node*) const = 0;   // Get peer IP address
  virtual bool     NodeIsPeer(Node*) const = 0;   // Find out if node is a peer
  virtual Count_t  NodePeerIndex(Node*) const = 0;// Get peer index for node

  //Doc:Method
  Node*            GetNode() const { return pNode;}// Get the associated node
    //Doc:Desc Get the  node pointer to which this interface is associated.
    //Doc:Return A pointer to the node this interface is associated with.

#ifdef MOVED_TO_BASE
  //Doc:Method
  virtual void     SetNode(Node* n) = 0;
    //Doc:Desc Set the  node pointer to which this interface is associated.
    //Doc:Arg1 Pointer to the node this interface is associated with.

  virtual Link*    GetLink() const = 0;       // Return the link pointer
  virtual void     SetLink(Link*) = 0;        // Set the link pointer
#endif
  virtual Queue*   GetQueue() const = 0;      // Return the queue pointer
  virtual void     SetQueue(const Queue& q) = 0;  // Set a new queue object
  virtual bool     QueueDetailed() const = 0;     // True if queue is detailed
  virtual L2Proto* GetL2Proto() const = 0;        // Layer 2 proto object
  virtual void     SetL2Proto(const L2Proto&) = 0;// Set the layer 2 protocol
  virtual void     AddNotify(NotifyHandler*,void*) = 0;// Add a notify client
  virtual void     CancelNotify(NotifyHandler*) = 0;   // Cancel pending notif.
  virtual MACAddr  IPToMac(IPAddr_t) const = 0;        // Get mac addr for ip
  virtual Count_t  NeighborCount(Node*) const = 0; // Number of rtg neighbors
  // Get a list of directly connected neighbors on this interface.
  virtual void     Neighbors(NodeWeightVec_t&, bool = false) = 0;
  virtual IPAddr_t DefaultPeer() = 0;         // Get default peer from link
  virtual bool     IsLocalIP(IPAddr_t) const = 0; // True if IP is on this i/f
  virtual Mult_t   TrafficIntensity() const = 0;  // TotBytesSent/LineSpeed
  // Add to list of remote IP
  virtual void     AddRemoteIP(IPAddr_t, Mask_t, Count_t = 0) = 0;
  virtual void     HandleLLCSNAP(Packet*, bool) = 0;// Process the LLCSnap hdr
  /////////// modified by Mohamed //////////
  virtual bool     IsWireless() const = 0; // True if wireless interface
  virtual bool     IsEthernet() const { return false; } // True if ethernet i/f
  virtual bool     IsReal() const = 0;            // True if not Ghost i/f
  virtual void     RegisterRxEvent(LinkEvent*){return;} ; // Register the last rx event issued by this interface 
  virtual void     AddRxEvent(LinkEvent*){return;} ; // add to the list of rx events , used in case of broadcast


#ifdef MOVED_TO_BASIC
  //Doc:Method
  virtual void     ScheduleRx(LinkEvent*, Time_t, bool = true);// Sched pkt rx in fifo q
    //Doc:Desc Schedule a packet receipt event at a future  time.
    //Doc:Arg1 The link event to add.
    //Doc:Arg2 Simulation time when the receipt will occur.
    //Doc:Arg3 list.  Only false for trace file playbacks.

  //Doc:Method
  EventCQ*   GetEventCQ() { return evList; }
    //Doc:Desc Get a pointer to the pending RX event circular queue
    //Doc:Desc for this interface.
    //Doc:Return Pointer to EventCQ for this interface.
#endif

  //Doc:Method
  virtual void UseWormContainment(bool b){ usewormcontainment = b;}
  //Doc:Desc Specifies if the  is using a wormcontainment algorithm or not
  //Doc:Arg1 True means use containment, false dont use

  //Doc:Method
  virtual bool UseWormContainment(){ return usewormcontainment;}
  //Doc:Desc Is this node using worm containment
  //Doc:Return True means use containment, false dont use

  //Doc:Method
  virtual void SetWormContainment(WormContainment*) = 0;
  //Doc:Desc Set the worm containment module for this node
  //Doc:Arg1 Worm containment module


  //Doc:Method
  virtual WormContainment* GetWormContainment() = 0;
  //Doc:Desc Gives a reference to the wormcontainment module of this node
  //Doc:Return Worm containment module

  //Doc:Method
  virtual void UseARP(bool) = 0;
  //Doc:Desc Specifies if the node is using arp module or not
  //Doc:Arg1 True means use ARP, false dont use

#ifdef MOVED_TO_BASIC
  bool IsDown() { return down; }
  void Down(bool noReInit = false);
  void Up();
#endif

  bool FirstBitRx(){return firstBitRx;}
  void FirstBitRx(bool is){firstBitRx = is;}
  
  // Static functions
  //Doc:Method
  static void DefaultUseARP(bool b = true);
    //Doc:Desc Specify that all subsequently created interfaces should
    //Doc:Desc     use the ARP protocol (or not).  Default is NOT.
    //Doc:Arg1 True if use ARP desired on all new interfaces.

  static void Add(Interface*, MACAddr_t);
  static Interface* GetByMac(MACAddr_t);

protected:
  IPAddr                  ip;                 // Associated IP Address
  Mask_t                  mask;               // Network mask
  MACAddr                 macAddr;            // Associated MAC address
  // Next two moved to INterfaceBasic
  //Node*                   pNode;              // Associated Node
  //EventCQ*                evList;             // Pending fifo events
  bool                    down;               // True if interface is DOWN
  bool                    usewormcontainment; // use wormcontainment module
  bool                    firstBitRx;        // True if statllite interface

public:
  // True if a global list of MAC address to Interface pointers is needed
  // This is used by the playback from trace file feature
  bool                    bootstrap;  
  static bool recordMacAddresses;
  static std::vector<Interface*> macIfVec;
  static bool defaultUseARP;
};

#endif
