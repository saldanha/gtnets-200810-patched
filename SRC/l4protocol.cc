// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: l4protocol.cc 475 2006-03-13 17:03:29Z sunithab $



// Georgia Tech Network Simulator - Layer 4 Protocol base class
// George F. Riley.  Georgia Tech, Spring 2002

// Define the layer 4 protocol interface

#ifdef HAVE_QT
#include <qcolor.h>
#endif

#include "l4protocol.h"
#include "l3protocol.h"
#include "portdemux.h"
#include "queue.h"
#include "application.h"
#include "event.h"
#include "interface.h"

using namespace std;

L4Protocol::L4Protocol()
    : localNode(nil), peerIP(IPADDR_NONE),
      localPort(NO_PORT), peerPort(NO_PORT),
      localApplication(nil), notification(nil),
      l3Proto(IPV4::Instance()), ttl(IPV4::DefaultTTL),
      fid(0), tos(0), iFace(nil), 
      extraRxDelay(0), extraTxDelay(0),
			deleteAppOnComplete(true)
#ifdef HAVE_QT
      ,color(nil)
#endif
{
}

L4Protocol::L4Protocol(Node* n)
    : localNode(n), peerIP(IPADDR_NONE),
      localPort(NO_PORT), peerPort(NO_PORT),
      localApplication(nil), notification(nil),
      l3Proto(IPV4::Instance()), ttl(IPV4::DefaultTTL),
      fid(0), tos(0), iFace(nil), 
      extraRxDelay(0), extraTxDelay(0),
			deleteAppOnComplete(true)
#ifdef HAVE_QT
      , color(nil)
#endif
{
}

// Copy constructor
L4Protocol::L4Protocol(const L4Protocol& c)
    : Protocol(c), 
      localNode(c.localNode), peerIP(c.peerIP),
      localPort(c.localPort), peerPort(c.peerPort),
      localApplication(nil), notification(c.notification),
      l3Proto(c.l3Proto), ttl(c.ttl),
      fid(c.fid), tos(c.tos), iFace(c.iFace), 
      extraRxDelay(c.extraRxDelay), extraTxDelay(c.extraTxDelay),
			deleteAppOnComplete(true)
#ifdef HAVE_QT
      , color(c.color)
#endif
{
}

 
L4Protocol::~L4Protocol()
{
#ifdef HAVE_QT
  if (color) delete color;
#endif
  if (localApplication)
    {
			if (deleteAppOnComplete)
	      delete localApplication;
    }
}

// Event Handler
void L4Protocol::Handle(Event* e, Time_t)
{
  switch(e->event)
    {
      case DELAYED_RX:
        {
          L4DelayedRxEvent* rx = (L4DelayedRxEvent*)e;
          rx->l4Proto->DataIndication(rx->node, rx->packet, 
                                      rx->ipAddr, rx->fromIF);
          DEBUG0((cout << "Processing delayed tx" << endl));
        }
        break;
      case DELAYED_TX:
        {
          L4DelayedTxEvent* tx = (L4DelayedTxEvent*)e;
          tx->l3Proto->DataRequest(tx->node, tx->packet, &tx->reqInfo);
          if (tx->notifySent && localApplication)
            {
              localApplication->Sent(tx->msgLength, this);
            }
          DEBUG0((cout << "Processing delayed tx" << endl));
        }
        break;
    }
  delete e;  // Done with this event
}

bool L4Protocol::Bind(PortId_t p)
{ // Bind to specific port
  if (!localNode) return false; // Can't bind if no node
  if (p == NO_PORT)
    { // If binding to zero
      Bind();
      return localPort != NO_PORT;
    }
  if (localNode->Bind(ProtocolNumber(), p, this))
    {
      localPort = p;
      return true;
    }
  return false;
}

bool L4Protocol::Bind()
{ // Bind to available port
  if (!localNode) return NO_PORT; // Can't bind if no node
  localPort = localNode->Bind(ProtocolNumber(), this);
  return localPort != NO_PORT;
}

bool L4Protocol::Unbind(Proto_t proto, PortId_t port) // Remove port binding
{
  if (localNode) return localNode->Unbind(proto, port, this);
  return true; 
}

void L4Protocol::SetInterface()
{ // Find the output interface for packets from this flow
  // Query the l4 protocol for the output i/f for the peer ip
  iFace = l3Proto->PeekInterface(localNode, &peerIP);
}

#ifdef HAVE_QT
void L4Protocol::SetColor(const QColor& c)
{
  if (!color)
    { // No existing color, create it
      color = new QColor(c);
    }
  else
    {
      *color = c;
    }
}
#endif

void    L4Protocol::AddNotification(NotifyHandler* n)
{
  DEBUG0((cout << "L4Proto " << this << " adding notif " << n << endl));
  notification = n;
}

Packet* L4Protocol::NewPacket()
{
  Packet* p = nil;
  
#ifdef HAVE_QT
  if (IsColored()) p = new ColoredPacket(*color);
#endif
  if (!p) p = new Packet();
  p->notification = notification;
  DEBUG0((cout << "l4proto " << this
          << " newPacket, notif " << notification << endl));
  return p;
}

void L4Protocol::AddExtraRxDelay(Time_t d)
{
  extraRxDelay = d;
}

  
void L4Protocol::AddExtraTxDelay(Time_t d)
{
  extraTxDelay = d;
}

  
void L4Protocol::RequestNotification(NotifyHandler* h, void* v)
{
  if (!iFace) SetInterface();  // Get the interface pointer
  if (!iFace) return;          // Packets cannot be routed
  iFace->AddNotify(h, v);
}

void L4Protocol::CancelNotification(NotifyHandler* h)
{
  if (!iFace) return;          // Packets cannot be routed
  DEBUG0((cout << "iFace " << iFace
          << " cancel notif " << h << endl));
  iFace->CancelNotify(h);
}

bool L4Protocol::BufferAvailable(Size_t s, Packet* p)
{ // True if output link buffer avail.
  if (!iFace) SetInterface();
  if (!iFace) return true; // Cannot be found

  Queue* q = iFace->GetQueue();
  if (!q) return true;     // Should not happen
  DEBUG0((cout << "L4proto, BA, limit " << q->GetLimit() 
          << " length " << q->Length() 
          << " avail " << q->GetLimit() - q->Length()
          << endl));
  
  return q->Check(s,p);    // True if sufficient buffer space available
}




