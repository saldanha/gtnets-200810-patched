// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: interface-real.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - Interface class
// George F. Riley.  Georgia Tech, Spring 2002

// Define class to model network link interfaces
// Implements the Layer 2 protocols

#include <iostream>

//#define DEBUG_MASK 0x01
#include "debug.h"
#include "hex.h"
#include "interface-real.h"
#include "link.h"
#include "queue.h"
#include "droppdu.h"
#include "node.h"
#include "trace.h"
#include "globalstats.h"
#include "ipv4.h"
#include "llcsnap.h"
#include "ratetimeparse.h"
#include "l3protocol.h"
#include "routing-nixvector.h"

#define DEFAULT_ARP_TIMEOUT 1200  // 20 min

using namespace std;

Count_t InterfaceReal::count = 0;

InterfaceReal::InterfaceReal(const L2Proto& l2, IPAddr_t i,
                             Mask_t m, MACAddr mac, bool bootstrap)  
    : Interface(l2, i, m, mac),
      pQueue(Queue::Default().Copy()),
      notifEvent(nil),
      totBytesSent(0), pL2Proto(l2.Copy()), defaultPeer(IPADDR_NONE),
      usearp(Interface::defaultUseARP), arpmodule(nil),
      arptimeout(DEFAULT_ARP_TIMEOUT), wormcontainment(nil)
{
  pQueue->SetInterface(this); // Some queues need to know attached i/f
  pL2Proto->SetInterface(this); // this l2proto obj is attached to an i/f
  count++;
  if (usearp)
    {
      arpmodule = new ARP(pNode);
      arpmodule->SetARPTimeout(arptimeout);
      arpmodule->SetTrace(Trace::ENABLED);
    }
}

InterfaceReal::~InterfaceReal()
{
  delete arpmodule;
  delete pQueue;
  delete pL2Proto;
}

void InterfaceReal::SetWormContainment(WormContainment* cont)
{
  usewormcontainment = true;
  wormcontainment = cont;
}

void InterfaceReal::UseARP(bool b)
{
  usearp = b;
  if (usearp)
    {
      arpmodule = new ARP(pNode);
      arpmodule->SetARPTimeout(arptimeout);
      arpmodule->SetTrace(Trace::ENABLED);
    }
}

bool InterfaceReal::Send(Packet* p, IPAddr_t i, int type)
{
   if(usewormcontainment)
    {
      wormcontainment->ProcessOutPacket(p, i, type, this);
      return true;
    }
   return Xmit(p, i, type);
}

bool InterfaceReal::Xmit(Packet* p, IPAddr_t i, int type)
{
  // Make arp requests for the respective MAC addresses
  if(usearp)
    {
      arpmodule->ARPRequest(this, p, type);
      return true;
    }
  MACAddr dst = IPToMac(i);
  return Send(p, dst, type);
}


// Send without ARP
bool InterfaceReal::Send(Packet* p, const MACAddr& dst, int type)
{
  if (IsDown())
    { // Interface is down, don't send.  Just drop packet
      DropPDU d("L2-ID", p);
      pNode->TracePDU(nil, &d);
      Stats::pktsDropped++; // Count dropped packets
      delete p;
      return false;
    }
      
  LLCSNAPHeader* llc = new LLCSNAPHeader(type);
  p->PushPDU(llc);

  pL2Proto->BuildDataPDU(macAddr, dst, p);
  /*
  PDU* l2pdu = p->PeekPDU();
  if (Trace::Enabled() && !IsWireless())
    { // Wireless proto traces the various handshaking, so we don't
      // do it here.
      GetNode()->TracePDU(pL2Proto, l2pdu, p, "-");
    }
  */
  /* If link is idle (i.e, CSMA) else queue */
  if (!pL2Proto->Busy() && !pQueue->CheckForcedLoss(false))
    { // Link is free, and no forced losses presently for the queue
      // However, we need to check for spoofed loss
      bool spoofLoss = pQueue->CheckSpoofedSource(p);
      if (spoofLoss)
        {
          DEBUG0((cout << "Interface dropping spoofed packet" << endl));
          DropPDU d("L2-FD", p); // Filter drop
          pNode->TracePDU(nil, &d);
          Stats::pktsDropped++; // Count dropped packets
          DEBUG0((cout << "Interface " << this
                  << " dropping pkt due to spoofing detected" << endl));
          if (p->notification)
            { // Sender has requested notification, so we need to
              // schedule one.
              // hack for now just to 1ms...need to calculate queue drain time
              Simulator::instance->AddNotify(
                  p->notification, Time("1ms"), nil);
            }
          delete p;     // Delete the packet
          return false; // Dropped
        }
      // Count as enque/deque to get proper queue statistics
      Stats::pktsEnqued++;
      Stats::pktsDequed++;
      // IPV4Header*      iphdr = (IPV4Header*)p->PeekPDU(2);
//       cout << "Send:: Packet from " << IPAddr::ToDotted(iphdr->src);
//       cout << " To " <<  IPAddr::ToDotted(iphdr->dst) ;
//       cout << " my IP is " <<IPAddr::ToDotted( GetIPAddr()) << endl;

      //if (notify) pLink->AddNotify(this, NULL); // ?? Is this right??
      /* the queue can check if it is full and drop the packet */
      pL2Proto->DataRequest(p);
      return true;
    }
  
  // Add a notification if none pending
  //if(pQueue->LengthPkts() == 0)	pLink->AddNotify(this, NULL);
  //pLink->AddNotify(this, NULL);
  // Can't transmit now, enque for later
  return EnquePacket(p);
}

// this is a broadcast packet just send it to all peers
bool InterfaceReal::Send(Packet* p, Word_t type)
{
  return Send(p, MACAddr::Broadcast(), type);
}

bool InterfaceReal::EnquePacket(Packet* p)
{
  if (!pQueue->Enque(p))
    {
      DropPDU d("L2-QD", p);
      pNode->TracePDU(nil, &d);
      Stats::pktsDropped++; // Count dropped packets
      DEBUG0((cout << "Interface " << this << " dropping pkt" << endl));
      if (p->notification)
        { // Sender has requested notification, so we need to
          // schedule one.
          // hack for now just to 1ms...need to calculate queue drain time
          DEBUG(1,(cout << "Adding notif for " << p->notification << endl));
          Simulator::instance->AddNotify(p->notification, Time("1ms"), nil);
        }
      delete p;     // Delete the packet
      return false; // Dropped
    }
  return true;
}

// Handler methods
void InterfaceReal::Handle(Event* e, Time_t t)
{
  DEBUG0((cout << "InterfaceReal::Handle, time " << t << endl));
  LinkEvent* le = (LinkEvent*)e; // Convert to right event type

  Size_t sz = 0;
  switch (le->event) {
    case  LinkEvent::PACKET_RX:
      if (le->fifoEvent)
        { // Should be head of fifo event queue
#undef  EXTRA_CHECKS
#ifdef  EXTRA_CHECKS
          if (evList->empty())
            {
              cout << "HuH?  Interface got fifo event on empty evList" << endl;
              exit(1);
            }
          EventPair* ep = evList->front();
          if (ep->event != le)
            { 
              cout << "HuH?  Interface got fifo event mismatch" << endl;
              cout << "eventcq " << evList << endl;
              cout << "le " << le << " ep.second " << ep->event << endl;
              exit(1);
            }
#endif
          evList->pop_front(); // Remove from fifo event list
          if (!evList->empty())
            { // Need to schedule the next simulator event
              EventPair* ep1 = evList->front();
              Time_t when = ep1->time - Simulator::Now();
              DEBUG0((cout << "Scheduling evlist front " << ep1->event
                      << " ev " << ep1->event->event << endl));
              Scheduler::ScheduleEarly(ep1->event, when, this);
            }
        }
      if (GetNode()->FirstBitRx())
        { // This event existed simply to get the animation working.
          // The packet was forwarded using the FirstBit event.
          // At this point we can just do nothing
          delete le;
          return;
        }

      Trace::Instance()->AppendEOL(); // End of log line
      DEBUG0((cout << "Interface " << this 
              << " Handle node " << pNode->Id() << endl));

      if (IsDown())
        { // Link is down, just drop packet and return
          DropPDU d("L2-ID", le->p);
          pNode->TracePDU(nil, &d);
          Stats::pktsDropped++;
          delete le->p;
          le->p = nil;
          delete le;
          return;
        }

      if(pNode->IsSwitchNode())  // if node is switch
        {
          // Pass the packet to the switch
          pNode->PacketRX(le->p, this);
          delete le;
          return;
        }

      PacketRxStart(le);
      sz = le->p->Size();
      if (le->hasBitError)
        { // just drop the packet due to bit error
          DropPDU d("L2-BER", le->p);
          pNode->TracePDU(nil, &d);
          DEBUG(0,(cout << "Packet Dropped due to BER" << endl));
          Stats::pktsDropped++;
          delete le->p;
          delete le;
        }
      else
        {
          //	cout << "port matching = " << useportmatching << endl;
          if(usewormcontainment)
            {
              wormcontainment->ProcessInPacket(le->p, this);
              delete le;
              break;
            }
          GetL2Proto()->DataIndication(le->p, le);
        }
    
      PacketRxEnd(le, sz);
      break;
    case  LinkEvent::PACKET_FIRSTBIT_RX:
      {
        Node* n = GetNode();
        if (!n->FirstBitRx())
          {
            cout << "HuH? First bit rx on non-fbrx node" << endl;
            break;
          }
#ifdef OLD_WAY
        const IFVec_t& iflist = n->Interfaces();
        // This is a real hack...need to think through a good solution.
        // For now, find the first interface that is not this one
        // and forward the packet
        bool foundIt = false;
        for (IFVec_t::size_type k = 0; k < iflist.size(); ++k)
          {
            if (iflist[k] != this)
              { // Found it, just send it on.
                Link* lk = iflist[k]->GetLink();
                Time_t rxTime = lk->Delay() +
                    (le->p->Size() * 8) / lk->Bandwidth();
                // We need to transmit using same rxEvent to use same
                // packet ber flags
                Interface* peerIf = lk->GetPeer(le->p);
                le->event = LinkEvent::PACKET_RX; // Convert to normal rx
                lk->Transmit(le, peerIf, rxTime);
                //lk->Transmit(le->p, iflist[k], n);  //Xold way
                foundIt = true;
                if (le->p->nixVec)
                  { // has a nixvector, extract the bits even though not used
                    le->p->nixVec->Extract(
                        le->p->nixVec->BitCount(n->NeighborCount()));
                  }
                le = nil; // Don't delete
                break;
              }
          }
        if (!foundIt)
          {
            cout << "HuH?  Satellite can't forward packet?" << endl;
            delete le->p;
          }
#endif
        GetL2Proto()->DataIndication(le->p, le); // Pass fb-rx event to l2proto
      }
    break;
    case LinkEvent::NOTIFY :
      DEBUG(0,(cout << "Got notify event" << endl));
      // Inform the link object that  link is free
      CallNotification(true);
      break;
    case LinkEvent::LINK_FAIL:
      DEBUG(0,(cout << "Got link fail event" << endl));
      down=true;
      delete le;
      break;
    case LinkEvent::LINK_RESTORE:
      DEBUG(0,(cout << "Got link restore event" << endl));
      down=false;
      delete le;
      break;
    default:
      DEBUG(0, (cout << "Huh!! unknown link event"<< endl));
    }
  //if (le) delete le; // Link event is responsibility of l2proto
}

void InterfaceReal::HandleLLCSNAP(Packet* p, bool fbrx)
{
  /* Time for the LLC/SNAP*/
  LLCSNAPHeader* llcsnap = (LLCSNAPHeader*)p->PopPDU(); 
  DEBUG(2,(cout << "LLCSnap ind Layer " << llcsnap->Layer()
           << " version " << llcsnap->Version()
           << endl));
  
  L3Protocol* l3 = (L3Protocol*)pNode->LookupProto(3, llcsnap->snap_ethtype);
  if (l3)
    {
      DEBUG0((cout << "Found l3 " << llcsnap->snap_ethtype
              << " in proto graph" << endl));
      l3->DataIndication(this, p);
    }
  else
    {
      cout << "Oops! Can't find l3 proto " << llcsnap->snap_ethtype
           << " in InterfaceReal::HandleLLCSNAP"
           << " at node " << pNode->Id()
           << endl;
      delete p;
    }
}

// Notifier
void InterfaceReal::Notify(void* v)
{ // Link has finished previous transmit
  if (v) delete (Time_t*)v;
  if (IsDown())
    { // This interface has failed.  Drain the queue and drop
      // all packets
      while(true)
        {
          Packet* p = pQueue->Deque();
          if (!p) break; // no more
          DropPDU d("L2-ID", p);
          pNode->TracePDU(nil, &d);
          Stats::pktsDropped++; // Count dropped packets
          delete p;
        }
    }
      
  // if the layer 2 mac trasmits multiple packets for a single transmission ??
  if (pL2Proto->Busy()) return;
  Packet* p = pQueue->Deque();
  if (p)
    { // Packet exists
      pL2Proto->DataRequest(p);
    }
  CallNotification(false); // Send possible queue space available notification
}

Count_t  InterfaceReal::PeerCount() const
{ // Number of peers
  return pLink->PeerCount();
}

IPAddr   InterfaceReal::PeerIP(int npeer) const
{ // Get peer IP addr (if known)
  return pLink->PeerIP(npeer);
}

IPAddr   InterfaceReal::NodePeerIP(Node* n) const
{ // Get peer IP addr (if known)
  return pLink->NodePeerIP(n);
}

bool     InterfaceReal::NodeIsPeer(Node* n) const
{ // Find out if specified node is a peer
  if (!pLink) return false;
  return pLink->NodeIsPeer(n);
}

Count_t  InterfaceReal::NodePeerIndex(Node* n) const
{ // Find out peer index (for NixVector Routing)
  return pLink->NodePeerIndex(n);
}

void  InterfaceReal::SetNode(Node* n)
{
  pNode = n;
  // and add the arp protocol in the graph if it exists
  if (arpmodule) pNode->InsertProto(3, arpmodule->Proto(), arpmodule);
}

#ifdef MOVED_TO_BASIC
Link* InterfaceReal::GetLink() const
{
  return pLink;
}

void InterfaceReal::SetLink(Link* pl)
{
  pLink = pl;
}
#endif

Queue* InterfaceReal::GetQueue() const
{
  return pQueue;
}
  
void InterfaceReal::SetQueue(const Queue& q)
{
  if (pQueue) delete pQueue; // Delete existing
  pQueue = q.Copy();         // Make a copy to use
  pQueue->SetInterface(this);
}

bool InterfaceReal::QueueDetailed() const
{
  if (pQueue) return pQueue->Detailed();
  return false;
}

void InterfaceReal::SetL2Proto(const L2Proto& l2)
{
  pL2Proto = l2.Copy();
  pL2Proto->SetInterface(this);
}

void InterfaceReal::AddNotify(NotifyHandler* n,void* v)
{ // Add a notify client
  DEBUG(1,(cout << "IFReal " << this << " AddNotify handler " << n << endl));
  notifications.push_back(Notification(n,v));
  if (!notifEvent && !pQueue->Detailed())
    { // No pending notification event, need to schedule one
      Time_t qd = pQueue->QueuingDelay(); // Time to drain queue
      notifEvent = new LinkEvent(LinkEvent::NOTIFY);
      Scheduler::Schedule(notifEvent, qd, this);
    }
}

void InterfaceReal::CancelNotify(NotifyHandler* n)
{ // Cancel a notification
  DEBUG(1,(cout << "iface " << this 
           << " Node " << pNode
           << " CancelNotify handler " << n << endl));
  for (NList_t::iterator i = notifications.begin();
       i != notifications.end(); ++i)
    {
      if (i->handler == n)
        { // Found it
          DEBUG(1,(cout << "iface " << this << " found notif " << n << endl));
          i->handler = nil;
          // return; // Keep looking in case more than one
        }
    }
}

MACAddr InterfaceReal::IPToMac(IPAddr_t i) const
{ // Given a PEER Ip Address, find corresponding MAC Address
  // Indirects to the Link to find this, since different link
  // types do this differently
  if (pLink) return pLink->IPToMac(i);
  return MACAddr::NONE; // If no link
}

Count_t InterfaceReal::NeighborCount(Node* n) const
{ // Get number of routing neighbor by asking the link
  // The links do this in different ways, so we ask
  if(pLink) return pLink->NeighborCount(n);
  return 0;
}

void  InterfaceReal::Neighbors(NodeWeightVec_t& nwv, bool forceAll)
{ // Add to neighbor list by asking the link
  if (pLink) pLink->Neighbors(nwv, this, forceAll);
}

IPAddr_t InterfaceReal::DefaultPeer()
{
  if (defaultPeer != IPADDR_NONE)
    {
      return defaultPeer; // Cached from prior call
    }
  if (pLink) defaultPeer = pLink->DefaultPeer(this);
  return defaultPeer;
}

bool InterfaceReal::IsLocalIP(IPAddr_t i) const
{ // Determine if specified is on this subnetwork
  DEBUG0((cout << "Checking local ip " << Hex8(i)
          << " ip " << Hex8(ip)
          << " mask " << Hex8(mask) << endl));
  if ((ip & mask) == (i & mask)) return true;
  // Check remote IP's (used only for distributed simulations)
  for (IPMaskVec_t::size_type j = 0; j < remoteIPList.size(); ++j)
    {
      const IpMaskCount& ipm = remoteIPList[j];
      if ((ipm.ip & ipm.mask) == (i & ipm.mask)) return true;
    }
  return false;
}

Mult_t InterfaceReal::TrafficIntensity() const
{ // Computed offered load on this link, as fraction of total capacity
  DCount_t maxBytes = Simulator::Now() * pLink->Bandwidth() / 8;
  return totBytesSent / maxBytes;
}

// Distributed Simulation Routines
void InterfaceReal::AddRemoteIP(IPAddr_t i, Mask_t m, Count_t c)
{ // List of remote IP's reached via this interface
  remoteIPList.push_back(IpMaskCount(i, m, c));
}

// Protected methods
void InterfaceReal::PacketRxStart(LinkEvent*)
{
}

void InterfaceReal::PacketRxEnd(bool, Size_t)
{
}

// Private  methods
void InterfaceReal::CallNotification(bool resched)
{ // Notify the next non-nil entry in the notification list
  while(1)
    {
      if (notifications.empty())
        {
          if (notifEvent && resched)
            { // Delete pending event
              delete notifEvent;
              notifEvent = nil;
            }
          return;       // No pending
        }
      Notification n = notifications.front();  // Get next notifier
      if (n.handler)
        { // Handler exists
          notifications.pop_front();           // And remove it
          DEBUG(1,(cout << "IFace " << this
                   << " Node " << pNode
                   << " Notifying " << n.handler << endl));
          n.handler->Notify(n.userData);       // Call the notification handler
          break;
        }
      else
        {
          notifications.pop_front();           // Remove null handler, continue
        }
    }
  // If any notifications remaining, need to reschedule notification timer
  if (resched)
    { // Need to schedule another
      Time_t qd = pQueue->QueuingDelay(); // Time to drain queue
      if (!notifEvent)
        notifEvent = new LinkEvent(LinkEvent::NOTIFY);
      Scheduler::Schedule(notifEvent, qd, this);
    }
}



