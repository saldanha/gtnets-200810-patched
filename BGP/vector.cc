/****************************************************************************/
/*  File:            vector.cc                                              */
/*  Author:          Sunitha Beeram                                         */
/*  Email:           sunithab@ece.gatech.edu                                */
/*  Documentation:   TBD                                                    */
/*                                                                          */
/*  Version:         1.0beta                                                */
/*                                                                          */
/*  Copyright (c) 2005 Georgia Institute of Technology                      */
/*  This program is free software; you can redistribute it and/or           */
/*  modify it under the terms of the GNU General Public License             */
/*  as published by the Free Software Foundation; either version 2          */
/*  of the License, or (at your option) any later version.                  */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful,         */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*  GNU General Public License for more details.                            */
/*                                                                          */
/****************************************************************************/

#include "bgp.h"

/* Initialize vector : allocate memory and return vector. */
struct _vector*
BGP::vector_init (unsigned int size)
{
  struct _vector* v = (struct _vector*)XCALLOC (MTYPE_VECTOR, sizeof (struct _vector));

  /* allocate at least one slot */
  if (size == 0)
    size = 1;

  v->alloced = size;
  v->max = 0;
  v->index =(void**) XCALLOC (MTYPE_VECTOR_INDEX, sizeof (void *) * size);
  return v;
}

void
BGP::vector_only_wrapper_free (struct _vector* v)
{
  XFREE (MTYPE_VECTOR, v);
}

void
BGP::vector_only_index_free (void *index)
{
  XFREE (MTYPE_VECTOR_INDEX, index);
}

void
BGP::vector_free (struct _vector* v)
{
  XFREE (MTYPE_VECTOR_INDEX, v->index);
  XFREE (MTYPE_VECTOR, v);
}

struct _vector*
BGP::vector_copy (struct _vector* v)
{
  unsigned int size;
  struct _vector* New = (struct _vector*)XCALLOC (MTYPE_VECTOR, sizeof (struct _vector));

  New->max = v->max;
  New->alloced = v->alloced;

  size = sizeof (void *) * (v->alloced);
  New->index = (void**)XCALLOC (MTYPE_VECTOR_INDEX, size);
  memcpy (New->index, v->index, size);

  return New;
}

/* Check assigned index, and if it runs short double index pointer */
void
BGP::vector_ensure (struct _vector* v, unsigned int num)
{
  if (v->alloced > num)
    return;

  v->index = (void**)XREALLOC (MTYPE_VECTOR_INDEX, 
		       v->index, sizeof (void *) * (v->alloced * 2));
  memset (&v->index[v->alloced], 0, sizeof (void *) * v->alloced);
  v->alloced *= 2;
  
  if (v->alloced <= num)
    vector_ensure (v, num);
}

/* This function only returns next empty slot index.  It dose not mean
   the slot's index memory is assigned, please call vector_ensure()
   after calling this function. */
int
BGP::vector_empty_slot (struct _vector *v)
{
  unsigned int i;

  if (v->max == 0)
    return 0;

  for (i = 0; i < v->max; i++)
    if (v->index[i] == 0)
      return i;

  return i;
}

/* Set value to the smallest empty slot. */
int
BGP::vector_set (struct _vector *v, void *val)
{
  unsigned int i;

  i = vector_empty_slot (v);
  vector_ensure (v, i);

  v->index[i] = val;

  if (v->max <= i)
    v->max = i + 1;

  return i;
}

/* Set value to specified index slot. */
int
BGP::vector_set_index (struct _vector* v, unsigned int i, void *val)
{
  vector_ensure (v, i);

  v->index[i] = val;

  if (v->max <= i)
    v->max = i + 1;

  return i;
}

/* Look up vector.  */
void *
BGP::vector_lookup (struct _vector* v, unsigned int i)
{
  if (i >= v->max)
    return NULL;
  return v->index[i];
}

/* Lookup vector, ensure it. */
void *
BGP::vector_lookup_ensure (struct _vector* v, unsigned int i)
{
  vector_ensure (v, i);
  return v->index[i];
}

/* Unset value at specified index slot. */
void
BGP::vector_unset (struct _vector* v, unsigned int i)
{
  if (i >= v->alloced)
    return;

  v->index[i] = NULL;

  if (i + 1 == v->max) 
    {
      v->max--;
      while (i && v->index[--i] == NULL && v->max--) 
	;				/* Is this ugly ? */
    }
}

/* Count the number of not emplty slot. */
unsigned int
BGP::vector_count (struct _vector* v)
{
  unsigned int i;
  unsigned count = 0;

  for (i = 0; i < v->max; i++) 
    if (v->index[i] != NULL)
      count++;

  return count;
}
