/****************************************************************************/
/*  File:            vty.cc                                           */
/*  Author:          Sunitha Beeram                                         */
/*  Email:           sunithab@ece.gatech.edu                                */
/*  Documentation:   TBD                                                    */
/*                                                                          */
/*  Version:         1.0beta                                                */
/*                                                                          */
/*  Copyright (c) 2005 Georgia Institute of Technology                      */
/*  This program is free software; you can redistribute it and/or           */
/*  modify it under the terms of the GNU General Public License             */
/*  as published by the Free Software Foundation; either version 2          */
/*  of the License, or (at your option) any later version.                  */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful,         */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*  GNU General Public License for more details.                            */
/*                                                                          */
/****************************************************************************/


#include "bgp.h"
#include <stdlib.h>
#include <stdarg.h>

struct _vector* BGP::vtyvec=NULL;

/* VTY standard output function. */
int
BGP::vty_out (struct vty *vty, const char *format, ...)
{
  va_list args;
  int len = 0;
  int size = 1024;
  char buf[1024];
  char *p = NULL;

  va_start (args, format);

  if (vty_shell (vty))
    vprintf (format, args);
  else
    {
      /* Try to write to initial buffer.  */
      len = vsnprintf (buf, sizeof buf, format, args);

      /* Initial buffer is not enough.  */
      if (len < 0 || len >= size)
    {
      while (1)
        {
          if (len > -1)
        size = len + 1;
          else
        size = size * 2;

          p = (char*)XREALLOC (MTYPE_VTY_OUT_BUF, p, size);
          if (! p)
        return -1;

          len = vsnprintf (p, size, format, args);

          if (len > -1 && len < size)
        break;
        }
    }

      /* When initial buffer is enough to store all output.  */
      if (! p)
    p = buf;

      /* Pointer p must point out buffer. */
      if (vty_shell_serv (vty))
    write (vty->fd, (u_char *) p, len);
      else
    buffer_write (vty->obuf, (u_char *) p, len);

      /* If p is not different with buf, it is allocated buffer.  */
      if (p != buf)
    XFREE (MTYPE_VTY_OUT_BUF, p);
    }

  va_end (args);

  return len;
}

/* Small utility function which output log to the VTY. */
void
BGP::vty_log (const char *proto_str, const char *format, va_list va)
{
  unsigned int i;
  struct vty *vty;

  for (i = 0; i < vector_max (vtyvec); i++)
    if ((vty = (struct vty*)vector_slot (vtyvec, i)) != NULL)
      if (vty->monitor)
    vty_log_out (vty, proto_str, format, va);
}

int
BGP::vty_shell (struct vty *vty)
{
  return vty->type == VTY_SHELL ? 1 : 0;
}

int
BGP::vty_shell_serv (struct vty *vty)
{
  return vty->type == VTY_SHELL_SERV ? 1 : 0;
}

int
BGP::vty_log_out (struct vty *vty, const char *proto_str, const char *format,
         va_list va)
{
  int len;
  int ret;
  char buf[1024];
  char time_buf[TIME_BUF];

  ret = time_str (time_buf);

  if (ret != 0)
    {
      snprintf (buf, sizeof buf, "%s ", time_buf);
      write (vty->fd, buf, strlen (time_buf) + 1);
    }

  snprintf (buf, sizeof buf, "%s: ", proto_str);
  write (vty->fd, buf, strlen (proto_str) + 2);

  len = vsnprintf (buf, sizeof buf, format, va);
  if (len < 0)
    return -1;
  write (vty->fd, (u_char *)buf, len);

  snprintf (buf, sizeof buf, "\r\n");
  write (vty->fd, buf, 2);

  return len;
}

int
BGP::time_str (char *buf)
{
  time_t clock;
  struct tm *tm;
  int ret;

  //time (&clock);

  clock = (int)Simulator::instance->Now() + start_time;

  tm = localtime (&clock);

  ret = strftime (buf, TIME_BUF, "%Y/%m/%d %H:%M:%S", tm);

  return ret;
}

/* Allocate new vty struct. */
struct vty *
BGP::vty_new ()
{
  struct vty *New = (struct vty*)XCALLOC (MTYPE_VTY, sizeof (struct vty));

  New->obuf = (struct buffer *) buffer_new (100);
  New->buf =(char*) XCALLOC (MTYPE_VTY, VTY_BUFSIZ);
  New->max = VTY_BUFSIZ;
  New->sb_buffer = NULL;

  return New;
}

/* Close vty interface. */
void
BGP::vty_close (struct vty *vty)
{
  int i;

  /* Cancel threads.*/
  if (vty->t_read)
    thread_cancel (vty->t_read);
  if (vty->t_write)
    thread_cancel (vty->t_write);
  if (vty->t_timeout)
    thread_cancel (vty->t_timeout);
  if (vty->t_output)
    thread_cancel (vty->t_output);

  /* Flush buffer. */
  if (! buffer_empty (vty->obuf))
    buffer_flush_all (vty->obuf, vty->fd);

  /* Free input buffer. */
  buffer_free (vty->obuf);

  /* Free SB buffer. */
  if (vty->sb_buffer)
    buffer_free (vty->sb_buffer);

  /* Free command history. */
  for (i = 0; i < VTY_MAXHIST; i++)
    if (vty->hist[i])
      XFREE (MTYPE_VTY_HIST, vty->hist[i]);

  /* Unset vector. */
  vector_unset (vtyvec, vty->fd);

  /* Close socket. */
  if (vty->fd > 0)
    close (vty->fd);

  if (vty->address)
    XFREE (0, vty->address);
  if (vty->buf)
    XFREE (MTYPE_VTY, vty->buf);

  /* OK free vty. */
  XFREE (MTYPE_VTY, vty);
}

/* Read up configuration file from file_name. */
void
BGP::vty_read_config ()
{

  FILE *confp = NULL;
  char *fullpath=  bgp_config_file;
  
  confp = fopen (fullpath, "r");
  
  if (confp == NULL)
  {
    printf("Fatal Error: No such configuration file or directory %s",bgp_config_file);
    exit(1);
  }
  
  vty_read_file (confp);
  
  fclose (confp);
  
  host_config_set (fullpath);
  
}



#ifdef BGP_MRAI 

/* inserts supressed advertisement info in peer's
   supressed advertisement list. If there is another
   entry for supressed prefix it overwites the entry
   and returns 1; otherwise it returns 0.
*/
int
BGP::bgp_advertise_insert(struct peer *peer,struct bgp_advertise *adv)
{
  struct bgp_advertise *current;
  int ret = 0 ;


  if(!peer->top_adv)
    peer->top_adv = adv;
  else {
    for ( current = peer->top_adv; current; current = current->next )
      if (prefix_same(current->p,adv->p)) {

    /*
      cancel previous supressed advertisement for this prefix
    */
    bgp_advertise_remove(peer,current);
    ret = 1;
    break;
      }

    if(!peer->top_adv) {
      peer->top_adv = adv;
      return ret;
    }

    for ( current = peer->top_adv; current; current = current->next )
      if( current->next == NULL) {
    current->next = adv;
    adv->prev = current;
    break;
      }
  }
  return ret;
}

void
BGP::bgp_advertise_remove( struct peer *peer,struct bgp_advertise *rmv)
{


  if (!peer->top_adv) return ;

  if (rmv->next)
    rmv->next->prev = rmv->prev;

  if (rmv->prev)
    rmv->prev->next = rmv->next;
  else
    peer->top_adv = rmv->next;

  XFREE(MTYPE_BGP_ADVERTISE,rmv);
}

void
BGP::bgp_advertise_remove_by_prefix (struct peer *peer,struct prefix *p)
{
  struct bgp_advertise *tmp;

  for(tmp = peer->top_adv; tmp;tmp = tmp->next)
    if(prefix_same(p,tmp->p)) bgp_advertise_remove(peer,tmp);

}
void
BGP::routeadv_list_add(struct peer *peer, struct thread *t) {
  struct bgp_routeadv_list  *New;

  New = (struct bgp_routeadv_list*) XMALLOC(MTYPE_BGP_ROUTEADV_LIST,
                        sizeof(struct bgp_routeadv_list));
  New->t = t;
  New->prev = NULL;
  New->next = NULL;

  if(!peer->t_routeadv_list) peer->t_routeadv_list = New;
  else {
    peer->t_routeadv_list->prev = New ;
    New->next = peer->t_routeadv_list;
    peer->t_routeadv_list = New;
  }
}

void
BGP::routeadv_list_remove(struct peer *peer, struct thread *t) {
  struct bgp_routeadv_list *tmp;
  for (tmp = peer->t_routeadv_list;tmp;tmp = tmp->next)
    if(memcmp(tmp->t,t,sizeof(struct thread))==0) {
      if(tmp->next)
    tmp->next->prev = tmp->prev;
      if(tmp->prev)
    tmp->prev->next = tmp->next;
      if(tmp == peer->t_routeadv_list) peer->t_routeadv_list = NULL;
      XFREE(MTYPE_BGP_ROUTEADV_LIST,tmp);
      break;
    }
}

bool
BGP::routeadv_list_search(struct peer *peer, struct prefix *p) {
  struct thread *thread;
  struct bgp_mrai_info *bmi;
  struct bgp_routeadv_list *tmp=NULL;

  char buf[BUFSIZ];

  // scann across pending timers
  for (tmp = peer->t_routeadv_list;tmp;tmp = tmp->next) {
    thread = tmp->t;
    bmi = (struct bgp_mrai_info*) THREAD_ARG(thread);
    if(0)zlog (peer->log, LOG_INFO,
      "%s TIMER SEARCH prefix %s/%d, UT: %s/%d\n",
      peer->host,
      BGP::inet_ntop (p->family, &p->u.prefix, buf, SU_ADDRSTRLEN),
      p->prefixlen,
      BGP::inet_ntop (bmi->p->family, &bmi->p->u.prefix, buf, SU_ADDRSTRLEN),
      bmi->p->prefixlen);

    if (prefix_same(bmi->p,p)) {
      if(0)zlog (peer->log, LOG_INFO,
        "%s TIMER SEARCH prefix %s/%d timer exists\n",
        peer->host,
        BGP::inet_ntop (p->family, &p->u.prefix, buf, SU_ADDRSTRLEN),
        p->prefixlen);
      return 1;
    }
  }
  if(0)zlog (peer->log, LOG_INFO,
    "%s TIMER SEARCH prefix %s/%d, timer dsnt exists\n",
    peer->host,
    BGP::inet_ntop (p->family, &p->u.prefix, buf, SU_ADDRSTRLEN),
    p->prefixlen);
  return 0;
}

struct bgp_advertise*
BGP::bgp_advertise_new()
{
  struct bgp_advertise* New;
  New =(struct bgp_advertise*)  XMALLOC(MTYPE_BGP_ADVERTISE, sizeof (struct bgp_advertise));
  New->next = NULL;
  New->prev = NULL;
  memset(&New->attribute,0,sizeof(struct attr));
  return New;
}

/* checks if there is a running MRAI timer and depending on the
   case (per prefix or per peer) it decides to supress the update or not
*/

int
BGP::bgp_update_send_check (struct bgp_info *ri, struct peer_conf *conf, struct peer *peer,
                struct prefix *p, struct attr *attr, afi_t afi, safi_t safi,
                struct peer *from, struct prefix_rd *prd, u_char *tag)
{
  struct bgp_advertise *adv;
  double now;
  struct thread *t;
  struct bgp_mrai_info *bmi;
  Prefix2Timestamp_t::iterator iter;
  char buf[SU_ADDRSTRLEN];

  if (mrai_type==MRAI_DISABLE) return 1;
  if (mrai_type==MRAI_PER_PREFIX) {
    /*
       Check when was the last update for this prefix send
    */
    now = Simulator::instance->Now();

    for( iter = peer->update_stamps.begin(); iter != peer->update_stamps.end();++iter )
      if( prefix_same(&iter->first,p))
    if((now - iter->second) < peer->v_routeadv)
      goto supress;

    return 1;

  supress:
    zlog (peer->log, LOG_INFO,
      "%s UPDATE  %s/%d -- SUPRESS due to: MRAI per prefix timer\n",
      peer->host,
      BGP::inet_ntop (p->family, &p->u.prefix, buf, SU_ADDRSTRLEN),
      p->prefixlen);

    /*
      Update supressed, create new advertise info
    */
     adv = bgp_advertise_new();
     adv->conf = conf;
     adv->p = p;

     adv->attribute = *attr;
     adv->attribute.aspath = aspath_dup(attr->aspath);
     adv->attribute.aspath->str = aspath_make_str_count (attr->aspath);
     if(attr->cluster) adv->attribute.cluster = cluster_dup(attr->cluster);
     if(attr->community)  adv->attribute.community = community_dup(attr->community);
     if(attr->ecommunity) adv->attribute.ecommunity = ecommunity_dup(attr->ecommunity);

     adv->afi = afi;
     adv->safi = safi;
     adv->from = from;
     adv->prd = prd;
     adv->tag = tag;

     bgp_advertise_insert(peer,adv);

     /*
       if there is no pending timer for this prefix, we add one
     */
     if (!routeadv_list_search(peer,p)) {

       bmi = (struct bgp_mrai_info*) XMALLOC(MTYPE_BGP_MRAI_INFO,sizeof(struct bgp_mrai_info));
       bmi->peer = peer;
       bmi->p = p;
       bmi->bi = ri;

       t = thread_add_timer(master,&BGP::bgp_routeadv_timer,bmi,(long)(peer->v_routeadv - (now - iter->second)));

       /* Add timer to peer's list of pending timers*/
       routeadv_list_add(peer,t);
     }

     return 0;
  }
  else {

    /* If MRAI timer is on supress advertisement */
    if(peer->t_routeadv) {

      zlog (peer->log, LOG_INFO,
        "%s UPDATE  %s/%d -- SUPRESS due to: MRAI per peer timer\n",
        peer->host,
        BGP::inet_ntop (p->family, &p->u.prefix, buf, SU_ADDRSTRLEN),
        p->prefixlen);

      adv = bgp_advertise_new();
      adv->conf = conf;
      adv->p = p;

      adv->attribute = *attr;
      adv->attribute.aspath = aspath_dup(attr->aspath);
      adv->attribute.aspath->str = aspath_make_str_count (attr->aspath);
      if(attr->cluster) adv->attribute.cluster = cluster_dup(attr->cluster);
      if(attr->community)  adv->attribute.community = community_dup(attr->community);
      if(attr->ecommunity) adv->attribute.ecommunity = ecommunity_dup(attr->ecommunity);

      adv->afi = afi;
      adv->safi = safi;
      adv->from = from;
      adv->prd = prd;
      adv->tag = tag;

      bgp_advertise_insert(peer,adv);

      return 0;
    }

    BGP_TIMER_ON (peer->t_routeadv, &BGP::bgp_routeadv_timer,
           peer->v_routeadv);
    return 1;
  }
}

int
BGP::bgp_withdraw_send_check(struct peer* peer,struct prefix *p)
{
  struct bgp_advertise *adv;

  /*
    Check if there is a supressed update
    for the withdrawn prefix, rm it if so.
  */

  if(peer->top_adv)
    for(adv = peer->top_adv; adv; adv = adv->next) {
      if(prefix_same(adv->p,p)){
    bgp_advertise_remove(peer,adv);
    if(mrai_type==MRAI_PER_PREFIX) bgp_cancel_timer_by_prefix(peer,p);
    break;
      }
    }

  return 1;
}


/* function called when a MRAI timer expires */
int
BGP::bgp_routeadv_timer (struct thread *thread){
  struct peer *peer;
  struct bgp_advertise *top;
  struct bgp_mrai_info *bmi;
  struct bgp_advertise *tmp;
  struct prefix *p;
  struct bgp_info *bi;

  if(mrai_type == MRAI_PER_PEER) {

    /* If supressed advertisements exist and we are doing per peer mrai */
    peer = (struct peer* ) THREAD_ARG(thread);
    peer->t_routeadv = NULL;            /* reset the timer pointer */
    top = peer->top_adv;

    if (BGP_DEBUG (fsm, FSM))
      zlog (peer->log, LOG_DEBUG,
        "%s [FSM] Timer (routeadv timer expire)\n",
        peer->host);

    if(top) {

      bgp_update_send(peer,top->p,&top->attribute,top->afi,
              top->safi,top->from);


      /* Delete allocated aspath */
      aspath_delete(top->attribute.aspath);

      /*Delete allocated cluster */
      if(top->attribute.cluster)
    cluster_free(top->attribute.cluster);

      /*Delete allocated community */
      if(top->attribute.community)
    community_free(top->attribute.community);

      /*Delete allocated ecommunity */
      if(top->attribute.ecommunity)
    ecommunity_free(top->attribute.ecommunity);

      bgp_advertise_remove(peer,top);

    }
  } else if (mrai_type == MRAI_PER_PREFIX) {

      /*  If we are doing per prefix MRAI */
      bmi = (struct bgp_mrai_info*) THREAD_ARG(thread);

    /* prefix for the which the timer was */
    p = bmi->p;
    peer = bmi->peer;
    bi = bmi->bi;

    if (BGP_DEBUG (fsm, FSM))
      zlog (peer->log, LOG_DEBUG,
        "%s [FSM] Timer (routeadv timer expire)\n",
        peer->host);

    XFREE(MTYPE_BGP_MRAI_INFO,bmi);

    top = peer->top_adv;

    /* rm timer from  MRAI timers list */
    routeadv_list_remove(peer,thread);

    /*Scan the supressed advertisements list to find if there was
      any advertisement for this prefix */

    for ( tmp = top; tmp; tmp = tmp->next )
      if (prefix_same(tmp->p,p)) {

    /* if found, send the update now*/
    bgp_update_send(peer,p,&tmp->attribute,tmp->afi,
            tmp->safi,tmp->from);

    /* Delete allocated aspath */
    aspath_delete(tmp->attribute.aspath);

    /*Delete allocated cluster */
    if(tmp->attribute.cluster)
      cluster_free(tmp->attribute.cluster);

    /*Delete allocated community */
    if(tmp->attribute.community)
      community_free(tmp->attribute.community);

    /*Delete allocated ecommunity */
    if(tmp->attribute.ecommunity)
      ecommunity_free(tmp->attribute.ecommunity);

    /* add timestap for this prefix */
    bgp_update_add_timestamp(peer,tmp->p);

    /* rm entry from supressed updates list*/
    bgp_advertise_remove(peer,tmp);

    /*there cannot be more than one supressed advertisement
      for each prefix, since a second supressed advertisement
      would overwrite the entry for the first supressed advertisement.
      thus we break.
    */
    break;
      }
  }
  return 0;
}

#endif

/* Read up configuration file from file_name. */
void
BGP::vty_read_file (FILE *confp)
{
  int ret;
  struct vty *vty;
  
  vty = vty_new ();
  vty->fd = 0;			/* stdout */
  vty->type = VTY_TERM;
  vty->node = CONFIG_NODE;
  
  /* Execute configuration file */
  ret = config_from_file (vty, confp);
  
  if (ret != CMD_SUCCESS) 
  {
    switch (ret)
    {
    case CMD_ERR_AMBIGUOUS:
      fprintf (stderr, "Ambiguous command.\n");
      break;
    case CMD_ERR_NO_MATCH:
      fprintf (stderr, "There is no such command.\n");
      break;
    }
    fprintf (stderr, "Error occured during reading below line.\n%s %s\n", 
             vty->buf,bgp_config_file);
    vty_close (vty);
    exit (1);
  }
  
  vty_close (vty);
}

