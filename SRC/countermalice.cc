/*

* GTNetS provides a portable C++ class library for network simulations
*
* Copyright (C) 2003 George F. Riley
*
* No guarantees or warranties or anything are provided or implied in any way
* whatsoever.  Use this program at your own risk.  Permission to use this
* program for any purpose is given, as long as the copyright is kept intact.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

* Permission is hereby granted for any non-commercial use of this code

*/

// Georgia Tech Network Simulator - CounterMalice class
// Mohamed Abd Elhafez.  Georgia Tech, Summer 2004

// Implements the counter malice algorithm for worm containment

#ifdef HAVE_QT
#include "qnamespace.h"
#include <qcolor.h>
#endif


#include <iostream>

#include "debug.h"
#include "countermalice.h"
#include "l2proto802.3.h"
#include "node.h"
#include "droppdu.h"
#include "hex.h"
#include "icmp.h"

using namespace std;

#define DELAY_QUEUE_FULL 50
#define WORKING_QUEUE_FULL 5
#define SENSITIVITY 1
#define ICMP_PROTOCOL 1
#define ICMP_MAX_COUNT 50
#define TIMEOUT 500

int CounterMalice::delayqueuelimit = DELAY_QUEUE_FULL;
int CounterMalice::workingsetsize = WORKING_QUEUE_FULL;
int CounterMalice::blocked = 0;
int CounterMalice::falsedetection = 0;
int CounterMalice::timeout = TIMEOUT;

CounterMalice::CounterMalice():scheduled(false)
{
  cout << "creating counter malice module " << endl;
}

bool CounterMalice::IsOutIface(Interface* iface)
{
  bool found = false;

  for (unsigned i = 0; i < outIfs.size(); ++i)
    {
      if (outIfs[i] == iface)
	found = true;
    }
  return found;
}

void CounterMalice::ProcessOutPacket( Packet* p, IPAddr_t i,  int type, Interface* iface)
 {
   IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
   if (IsOutIface(iface))
     {
       iface->Xmit(p, i, type);
       return;
     }
   
   if( iphdr->protocol == ICMP_PROTOCOL)
     {
       // Do we have an entry for the dest IP ?
       Connections_t::iterator j = conns.find(iphdr->dst);
       if(j == conns.end())
	 {
	   cout << "Huh? ICMP packet for an unknown entry!" << endl;
	  
	  //  cout << "Connections contents" << endl;
// 	   for (Connections_t::iterator l = conns.begin(); l != conns.end(); l++)
// 	     {
// 	       cout << "Entry " << IPAddr::ToDotted((*l).first) << endl;
// 	     }
	   
	 }      

       // Icmp packet, check if it is destination unreachable
       ICMPHeader* icmp = (ICMPHeader*)p->PeekPDU(1);
       
       if(icmp->type == ICMP::DESTINATION_UNREACHABLE)
	 {
	   //cout << "ICMP host unreachable msg" << endl;
	   conns[iphdr->dst].icmpcount++;
	   // cout << "ICMP count for dst " << IPAddr::ToDotted(iphdr->dst) << " count = " << conns[iphdr->dst].icmpcount << endl;
	   if (conns[iphdr->dst].icmpcount == ICMP_MAX_COUNT)
	     {
	       cout << "CounterMalice:: Blocking  Ip " << IPAddr::ToDotted(iphdr->dst) << " dQueue size = " <<delayqueues[iphdr->src].size()  
		    << " ICMP max count reached" << endl;
	       
	       conns[iphdr->dst].blocked = true;
	       conns[iphdr->dst].timeout = Simulator::Now() + timeout;
	       //  workingsets.erase(iphdr->dst);
	       //delayqueues.erase(iphdr->dst);
	       blocked++;
	     }
	 }
    }
   iface->Xmit(p, i, type);
 }




void CounterMalice::ProcessInPacket( Packet* p, Interface* iface)
{
  // check here to discard if blocked with no further processing
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU(2);

  if (IsOutIface(iface))
    {
      iface->GetL2Proto()->DataIndication(p);
      return;
    }
 
  
  // Do we have an entry for the source IP ?
  Connections_t::iterator j = conns.find(iphdr->src);
  if(j == conns.end())
    {
      //  cout << "Addin new entry for src" << IPAddr::ToDotted(iphdr->src) << endl; 
      // conns.insert(Connections_t::value_type(iphdr->src, true));
      conns[iphdr->src] = Connection(false, 0, 0.0);
      delayqueues[iphdr->src] = DelayQueue_t();
      workingsets[iphdr->src] = WorkingSet_t();
    }      
  else if(conns[iphdr->src].blocked == true) 
    {
      if ( Simulator::Now() >=  conns[iphdr->src].timeout )
	{
	  conns[iphdr->src].blocked = false;
	  conns[iphdr->src].timeout = 0.0;
	  conns[iphdr->src].icmpcount = 0;
	  
	  delayqueues[iphdr->src] = DelayQueue_t();
	  workingsets[iphdr->src] = WorkingSet_t();
	}
      else
	{
	  //cout << "CounterMalice:: connection is blocked" << endl;
	  // Dropping packet
	  //cout << "dropping packet " << p->uid << endl;
	  DropPDU d("L3-CM", p);
	  iface->GetNode()->TracePDU(nil, &d);
	  DEBUG(0,(cout << "Packet Dropped " << endl));
	  Stats::pktsDropped++;
	  delete p;
	  return;
	}
    }
  
  if(iphdr->protocol == TCP_PROTOCOL)
    {
      TCPHeader* tcphdr = (TCPHeader*)p->PeekPDU(3);
      if(tcphdr->flags != TCP::SYN)
	{
	  // cout << "CounterMalice:: not SYN packet" << endl;
	  iface->GetL2Proto()->DataIndication(p);
	  return;
	}
      Delay(iphdr, p, iface);
    }
  else if(iphdr->protocol == UDP_PROTOCOL)
    {
      //cout << "CounterMalice:: udp protocol is used " << endl;
      Delay(iphdr, p, iface);
    }
  else
    {
      iface->GetL2Proto()->DataIndication(p);
    }
}


void CounterMalice::Delay(IPV4Header* iphdr, Packet* p, Interface* iface)
{
  if( InWorkingSet(iphdr) )
    {
      iface->GetL2Proto()->DataIndication(p);
      return;
    }
  if(workingsets[iphdr->src].size() < (unsigned)workingsetsize)
    {
      workingsets[iphdr->src].push_back(iphdr->dst);
     
      iface->GetL2Proto()->DataIndication(p);
      return;
    }
  
  //   cout << "CounterMalice:: working set Contents ------------" << endl;
  //       for (int j = 0; j < workingsets[iphdr->src].size(); j++)
  // 	{
  // 	  cout << IPAddr::ToDotted(workingsets[iphdr->src][j]) << endl;
  
  // 	}
  //	cout << "CounterMalice:: Queuing packet " << p->uid << endl;
  PacketInfo pinfo = PacketInfo();
  pinfo.type = 0;
  pinfo.nexthop = 0;
  delayqueues[iphdr->src].push(PacketPairInfo_t(p, pinfo));
  
  //      cout << "CounterMalice:: DelayQuee for Ip " << IPAddr::ToDotted(iphdr->src) << " size = " <<delayqueues[iphdr->src].size()  << endl;
  
  if((delayqueues[iphdr->src].size() +  conns[iphdr->src].icmpcount) >=
     (unsigned)delayqueuelimit)
    {
      //NodeId_t id = iface->GetNode()->Id(); ??

      cout << "CounterMalice:: On Node " << iface->GetNode()->Id() << " Blocking  Ip " << IPAddr::ToDotted(iphdr->src) << " dQueue size = " <<delayqueues[iphdr->src].size()  << endl;
      cout << "Number of working sets = " << workingsets.size() << endl;
      cout << "Number of dQueues = " << delayqueues.size() << endl;
      cout << "Number of connections = " << conns.size() << endl;

      conns[iphdr->src].blocked = true;
      conns[iphdr->src].timeout = Simulator::Now() + timeout;
      blocked++;
      // workingsets.erase(iphdr->src);
      //  delayqueues.erase(iphdr->src);
      
      // // this is just a hack to find out if the node is really infected or not
      // 	    QColor col = iface->GetNode()->Color();
      // 	    if( col == Qt::red)
      // 	      {
      // 		cout << "CounterMalice:: Worm is really infected" << endl;
      // 	      }
      // 	    else
      // 	      {
      // 		cout << "CounterMalice:: False detection" << endl;
      // 		falsedetection++;
      // 	      }
      
      // 	    // try to turn the blocked node yellow
      // 	    iface->GetNode()->Color(Qt::yellow);
      delete p;
      return;
    }
  // Schedule the next event
  if(!scheduled)
    {
      scheduled = true;
      //cout << "CounterMalice:: Scheduling process queue event" << endl;
      //cout << "CounterMalice queue size =  " << delayqueue.size() << endl;
      //Scheduler::Schedule(new ThrottleEvent(ThrottleEvent::PROCESS_QUEUE, i, iface), pow((double)2,(int)delayqueue.size()) * SENSITIVITY, this);
      Scheduler::Schedule(new ThrottleEvent(ThrottleEvent::PROCESS_QUEUE, iface), SENSITIVITY, this);
    }      
}



void CounterMalice::Handle(Event* e, Time_t t)
{
  ThrottleEvent* te = (ThrottleEvent*)e;
  IPV4Header* iphdr ;

  // check here for release while blocked
  PacketPairInfo_t pp;
  switch(te->event) {
  case ThrottleEvent::PROCESS_QUEUE :
    // loop through all the queues
    for(WorkingSets_t::iterator i = workingsets.begin(); i != workingsets.end(); ++i)
      {
	if((*i).second.size() == (unsigned)workingsetsize)
	  {
	    //cout << "CounterMalice working set queue size =  " << workingset.size() << endl;
	    (*i).second.erase((*i).second.begin());
	  }
	if(!delayqueues[(*i).first].empty() && !conns[(*i).first].blocked)
	  {
	    pp = delayqueues[(*i).first].front();
	    iphdr = (IPV4Header*)pp.first->PeekPDU(2);
	    te->iface->GetL2Proto()->DataIndication(pp.first);

	    //	    te->iface->Xmit(pp.first, pp.second.nexthop, pp.second.type);
	    if(!InWorkingSet(iphdr))
	       (*i).second.push_back(iphdr->dst);
	    delayqueues[(*i).first].pop();
   	  }
      }
    //Scheduler::Schedule(new ThrottleEvent(ThrottleEvent::PROCESS_QUEUE, te->dst, te->iface), pow((double)2, (int)delayqueue.size())*SENSITIVITY, this);
    Scheduler::Schedule(new ThrottleEvent(ThrottleEvent::PROCESS_QUEUE, te->iface), SENSITIVITY, this);
	    
    break;
  default:
    break;
  }
  delete te;
}



bool CounterMalice::InWorkingSet(IPV4Header* iphdr)
{
  // cout << "Inws:   Number of working sets = " << workingsets.size() << endl;
  bool found = false;
  int size = workingsets[iphdr->src].size();
  // cout << " Contents of working set " << endl;

  for(int i = 0; i < size; i++)
    {
      //  cout << "Entry " << i << " " << IPAddr::ToDotted( workingsets[iphdr->src][i]) << endl;
      if( workingsets[iphdr->src][i] == iphdr->dst )
	{
	  found = true;
	  break;
	}
    }
  return found;
}

void CounterMalice::SetOutIfs(IFVec_t vec)
{
  outIfs = vec;
}

void CounterMalice::SetOutIf(Interface* ifc)
{
  outIfs.push_back(ifc);
}

void CounterMalice::SetWorkingSetSize(int size)
{
  workingsetsize = size;
}

void CounterMalice::SetDelayQueueLimit(int size)
{
  delayqueuelimit = size;
}

void CounterMalice::SetTimeout(int t)
{
  timeout = t;
}
