// GENERAL PUBLIC LICENSE AGREEMENT
//
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
//
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
//
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
//
// 1.  This License allows you to:
//
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
//
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//
//
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
//
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
//
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
//
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
//
//
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
//
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
//
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
//
//
// 3.  Export Law Assurance.
//
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//
// 4.  Termination.
//
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
//
//
// 5.  Disclaimer of Warranties and Limitation on Liability.
//
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
//
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
//
// GTRC shall have no obligation for support or maintenance of Program.
//
// 6.  Copyright Notice.
//
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
//
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
//
// Said copyright notice shall read as follows:
//
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: droptail.h 498 2006-05-10 16:58:08Z riley $



// Georgia Tech Network Simulator - DropTail Queue class
// George F. Riley.  Georgia Tech, Spring 2002

#ifndef __droptail_h__
#define __droptail_h__

#include "queue.h"

class Interface;

#define DEFAULT_DROPTAIL_LIMIT 50000

//Doc:ClassXRef
class DropTail : public Queue {
//Doc:Class This method implements the simple droptail or FIFO queue.
public:

  //Doc:Method
  DropTail()
      : Queue(true), limit(Queue::DefaultLength()),
        pktLimit(Queue::DefaultLimitPkts()),
        size(0), pktCount(0), lastTime(0) { }
  //Doc:Desc the default constructor

  //Doc:Method
  DropTail(Count_t l)
    : Queue(true), limit(l), pktLimit(0),
      size(0), pktCount(0), lastTime(0) { }
  //Doc:Desc This constructor creates a queue of specified size
  //Doc:Arg1 the size of the queue in bytes

  //Doc:Method
  DropTail(const DropTail& c)
    : Queue(c), limit(c.limit), pktLimit(c.pktLimit),
      size(c.size), pktCount(c.pktCount), lastTime(c.lastTime) { }
  //Doc:Desc This constructor creates a queue identical to another queue
  //Doc:Arg1 Reference to an existing queue

  virtual ~DropTail() { }

  //Doc:Method
  bool Enque(Packet*); // Return true if enqued, false if dropped
  //Doc:Desc This method is used to enque a packet in the queue
  //Doc:Arg1 the pointer to the packet to be added to the queue
  //Doc:Return a boolean to indicate if the enque operation succeeded or
  //Doc:Return if the packet was dropped.

  //Doc:Method
  Packet* Deque();     // Return next element (NULL if none)
  //Doc:Desc This method deques a packet from the queue and returns it
  //Doc:Return Pointer to the packet

  //Doc:Method
  Packet* PeekDeque();
    //Doc:Desc Return a pointer to the next packet in the queue, without
    //Doc:Desc removing it.  This is used by some subclasses of
    //Doc:Desc   queue (DiffServQueue for example) to find out if the packet
    //Doc:Desc has enough tokens to be transmitted.
    //Doc:Return Pointer to the packet, or nil if none available.

  //Doc:Method
  virtual Count_t DequeAllDstMac(MACAddr);
    //Doc:Desc   Remove all packets in the queue with the specified
    //Doc:Desc destination MAC address.
    //Doc:Arg1 MAC Address to remove
    //Doc:Return Count of removed packets

  //Doc:Method
  virtual Count_t DequeAllDstIP(IPAddr_t);
    //Doc:Desc   Remove all packets in the queue with the specified
    //Doc:Desc destination IP address.
    //Doc:Arg1 IP Address to remove
    //Doc:Return Count of removed packets

  //Doc:Method
  virtual Packet* DequeOneDstMac(MACAddr);
  //Doc:Desc   Remove a packet in the queue with the specified
  //Doc:Desc destination MAC address.
  //Doc:Arg1 MAC Address to remove
  //Doc:Return Pointer to the removed packet

  //Doc:Method
  virtual Packet* DequeOneDstIP(IPAddr_t);
  //Doc:Desc   Remove a packet in the queue with the specified
  //Doc:Desc destination IP address.
  //Doc:Arg1 IP Address to remove
  //Doc:Return Pointer to the removed packet

  //Doc:Method
  Count_t Length();    // Length in bytes
  //Doc:Desc This method returns the size of the queue in terms of bytes
  //Doc:Return number of bytes

  //Doc:Method
  Count_t LengthPkts();// Length in packets
  //Doc:Desc This method returns the size of the queue in terms of the
  //Doc:Desc number of packets
  //Doc:Return number of packets enqued

  //Doc:Method
  void    SetInterface(Interface*);// Associated interface
  //Doc:Desc This method attaches the queue to an interface
  //Doc:Arg1 The interface to which it is to be attached


  //Doc:Method
  Queue*  Copy() const;       // Make a copy
  //Doc:Desc This method makes a copy of the queue and returns a pointer
  //Doc:Desc to the copy
  //Doc:Return pointer to a copy of the queue

  //Doc:Method
  virtual void SetLimit(Count_t l);
  //Doc:Desc This method sets a limit to the size of the queue. this limit
  //Doc:Desc is in terms of bytes
  //Doc:Arg1 size of the queue in bytes


  //Doc:Method
  Count_t GetLimit() const { return limit;}
  //Doc:Desc This method returns the size of the queue in terms of the bytes
  //Doc:Return Max limit of the queue size in bytes

  //Doc:Method
  virtual void SetLimitPkts(Count_t l) { pktLimit = l;} // Set packets limit
  //Doc:Desc This method sets a limit to the size of the queue. this limit
  //Doc:Desc is in terms of number of packets that can be enqued
  //Doc:Arg1 size of the queue in packets

  //Doc:Method
  virtual Count_t GetLimitPkts() const { return pktLimit;} // Get pkt limit
  //Doc:Desc This method returns the size of the queue in terms of the packets
  //Doc:Return Max limit of the queue size in number of packets


  //Doc:Method
  virtual Time_t  QueuingDelay();    // Calculate queuing delay at current lth
  //Doc:Desc This method calculates the current queuing delay
  //Doc:Return the queuing delay


  //Doc:Method
  bool    Check(Size_t, Packet* = nil); // Test if buffer space available
  //Doc:Desc This method checks if buffer space (of a given size) is available
  //Doc:Arg1 The size to be checked
  //Doc:Arg2 The pointer to a packet to be queued.
  //Doc:Return boolean to indicate true or false

  //Doc:Method
  Count_t Limit() { return limit;}
  //Doc:Desc This method returns the maximum limit of the queue size.
  //Doc:Return the limit (in bytes)

  //Doc:Method
  Packet* GetPacket(Count_t k);
    //Doc:Desc Used by the animation.  Gets the k'th packet from the head.
    //Doc:Arg1 Index of desired packet.
    //Doc:Return Packet pointer to k'th packet.  Nil if does not exist.

  //Doc:Method
  void     GetPacketColors(ColorVec_t&);
    //Doc:Desc Fill in the vector of colors for each pending packet.
    //Doc:Desc This is more efficient than the GetPacket method above,
    //Doc:Desc and the above is deprecated.
    //Doc:Arg1 Vector of colors to fill in.
    //Doc:Arg1 zero'th entry is next packet to deque.

private:
  void    UpdateSize();       // Adjust size since last update
public:
  Count_t    limit;           // Limit of bytes in queue
  Count_t    pktLimit;        // Limit on number of pkts (if non-zero)
  Count_t    size;            // Current bytes in queue
  Count_t    pktCount;        // Current packets in queue
  Time_t     lastTime;        // Time size was calculated
public:
  PktList_t  pkts;            // List of packets in the queue
};

#endif
