// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: dumbbell.cc 92 2004-09-14 18:18:43Z dheeraj $



// Georgia Tech Network Simulator - Dumbbell topology
// George F. Riley.  Georgia Tech, Fall 2002

// Define the dumbbell topology using point-to-point links

#include <math.h>

#include "dumbbell.h"
#include "node.h"
#include "linkp2p.h"
#include "mask.h"

using namespace std;

// Constructors

Dumbbell::Dumbbell(Count_t l, Count_t r, Mult_t m, SystemId_t id)
{
  ConstructorHelper(l, r, m, IPADDR_NONE, IPADDR_NONE, Linkp2p::Default(), id);
}

Dumbbell::Dumbbell(Count_t l, Count_t r, Mult_t m,
                   const Linkp2p& link, SystemId_t id)
{
  ConstructorHelper(l, r, m, IPADDR_NONE, IPADDR_NONE, link, id);
}

Dumbbell::Dumbbell(Count_t l, Count_t r, Mult_t m,
                   IPAddr_t lip, IPAddr_t rip, SystemId_t id)
{
  ConstructorHelper(l, r, m, lip, rip, Linkp2p::Default(), id);
}

Dumbbell::Dumbbell(Count_t l, Count_t r, Mult_t m,
                   IPAddr_t lip, IPAddr_t rip,
                   const Linkp2p& link, SystemId_t id)
{
  ConstructorHelper(l, r, m, lip, rip, link, id);
}

// Access functions
Node* Dumbbell::Left(Count_t c)
{ // Get specified left side leaf node
  if (c >= leftCount) return nil;
  return Node::GetNode(first + c + 2);
}

Node* Dumbbell::Right(Count_t c)
{ // Get Specified right side leaf node
  if (c >= rightCount) return nil;
  return Node::GetNode(first + leftCount + c + 2); // Allow for the two routers
}

Node* Dumbbell::Left()
{ // Get left side router
  return Node::GetNode(first);    // Left router is first
}

Node* Dumbbell::Right()
{ // Get right side router
  return Node::GetNode(first + 1); // Right router is second
}

Linkp2p* Dumbbell::LeftLink(Count_t c)
{ // Get link from left leaf to left rtr
  Node* n = Left(c);
  if (!n) return nil;
  return (Linkp2p*)n->GetLink(Left());
}

Linkp2p* Dumbbell::RightLink(Count_t c)
{ // Get link from right leaf to right rtr
  Node* n = Right(c);
  if (!n) return nil;
  return (Linkp2p*)n->GetLink(Right());
}

Linkp2p* Dumbbell::LeftLink()
{ // Get link from left rtr to right rtr
  return (Linkp2p*)Left()->GetLink(Right());
}

Linkp2p* Dumbbell::RightLink()
{// Get link from right rtr to left rtr
  return (Linkp2p*)Right()->GetLink(Left());

}

Queue*   Dumbbell::LeftQueue(Count_t c)
{ // Get queue from left leaf to left rtr
  Node* n = Left(c);
  if (!n) return nil;
  return n->GetQueue(Left());
}

Queue*   Dumbbell::RightQueue(Count_t c)
{ // Get queue from right leaf to right rtr
  Node* n = Right(c);
  if (!n) return nil;
  return n->GetQueue(Right());
}

Queue*   Dumbbell::LeftQueue()
{ // Get queue from left rtr to right rtr
  return Left()->GetQueue(Right());
}

Queue*   Dumbbell::RightQueue()
{ // Get queue from left rtr to right rtr
  return Right()->GetQueue(Left());
}

void     Dumbbell::BoundingBox(const Location& ll, const Location& ur)
{
  Meters_t xDist = ur.X() - ll.X();
  Meters_t yDist = ur.Y() - ll.Y();
  Meters_t xAdder = xDist / 3.0;
  //Meters_t yAdderL = yDist / (LeftCount() + 1.0);
  //Meters_t yAdderR = yDist / (RightCount() + 1.0);
  Angle_t  thetaL = M_PI / (LeftCount() + 1.0);
  Angle_t  thetaR = M_PI / (RightCount() + 1.0);

  // Notify simulator of bounds, for max x/y computations
  Simulator::instance->NewLocation(ll.X(), ll.Y());
  Simulator::instance->NewLocation(ur.X(), ur.Y());

  // Place the left router
  Location lrl(ll.X() + xAdder, ll.Y() + yDist/2.0);
  Node* ln = Left();
  ln->SetLocation(lrl);
  
  // Place the right router
  Location rrl(ll.X() + xAdder * 2.0, ll.Y() + yDist/2.0);
  Node* rn = Right();
  rn->SetLocation(rrl);

  // Place the left nodes
  Angle_t theta = -M_PI_2 + thetaL;
  for (Count_t l = 0; l < LeftCount(); ++l)
    {
      // Make them in a circular pattern to make all line lengths the same
      // Special case when theta = 0, to be sure we get a straight line
      if ((LeftCount() % 2) == 1)
        { // Count is odd, see if we are in middle
          if (l == (LeftCount() / 2))
            {
              theta = 0.0;
            }
        }
      Node* n = Left(l);
      Location lnl(lrl.X() - cos(theta) * xAdder,
                   lrl.Y() + sin(theta) * xAdder);
      // Insure did not exceed bounding box
      if (lnl.Y() < ll.Y()) lnl.Y(ll.Y());
      if (lnl.Y() > ur.Y()) lnl.Y(ur.Y());
      n->SetLocation(lnl);
      theta += thetaL;
    }
  // Place the right nodes
  theta = -M_PI_2 + thetaR;
  for (Count_t r = 0; r < RightCount(); ++r)
    {
      // Special case when theta = 0, to be sure we get a straight line
      if ((RightCount() % 2) == 1)
        { // Count is odd, see if we are in middle
          if (r == (RightCount() / 2))
            {
              theta = 0.0;
            }
        }
      Node* n = Right(r);
      Location rnl(rrl.X() + cos(theta) * xAdder,
                   rrl.Y() + sin(theta) * xAdder);
      // Insure did not exceed bounding box
      if (rnl.Y() < ll.Y()) rnl.Y(ll.Y());
      if (rnl.Y() > ur.Y()) rnl.Y(ur.Y());
      n->SetLocation(rnl);
      theta += thetaR;
    }
}

// Private methods
void Dumbbell::ConstructorHelper(Count_t left, Count_t right,
                                 Mult_t m, 
                                 IPAddr_t leftIP, IPAddr_t rightIP,
                                 const Linkp2p& link, SystemId_t id)
{
  leftCount = left;
  rightCount = right;
  first = Node::nextId;
  // First left and right routers, then left side, then right side
  Node* lr = new Node(id);
  Node* rr = new Node(id);
  DEBUG0((cout << "DB LR id " << lr->Id() << endl));
  DEBUG0((cout << "DB RR id " << rr->Id() << endl));

  // Connect the routers, with reduced bandwidth
  DEBUG0((cout << "Constructing Dumbbell bottleneck, link bw "
          << link.Bandwidth() << endl));
  Linkp2p routerLink(link);
  routerLink.Bandwidth(link.Bandwidth() * m);
  DEBUG0((cout << "Router Link bw " << routerLink.Bandwidth() << endl));
  lr->AddDuplexLink(rr, routerLink);
  
  // Set default routes
  lr->DefaultRoute(rr);
  rr->DefaultRoute(lr);
  
  for (Count_t i = 0; i < left; ++i)
    {
      Node* ln = new Node(id);
      DEBUG0((cout << "DB LN id " << ln->Id() << endl));
      IPAddr_t ip = IPADDR_NONE;
      if  (leftIP != IPADDR_NONE)
        { // Assign specified ip addresses
          ip = leftIP;
        }
      ln->AddDuplexLink(lr, link, ip);
      // Add a default route
      ln->DefaultRoute(lr);
      if (leftIP != IPADDR_NONE) leftIP ++; // Advance to next ip
    }
  for (Count_t i = 0; i < right; ++i)
    {
      Node* rn = new Node(id);
      DEBUG0((cout << "DB RN id " << rn->Id() << endl));
      IPAddr_t ip = IPADDR_NONE;
      if  (rightIP != IPADDR_NONE)
        { // Assign specified ip addresses
          ip = rightIP;
        }
      rn->AddDuplexLink(rr, link, ip);
      // Add a default route
      rn->DefaultRoute(rr);
      if (rightIP != IPADDR_NONE) rightIP ++; // Advance to next ip
    }
  last = Node::nextId;
}

