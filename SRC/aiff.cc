// Subroutines for reading/writing for AIFF format
// George F. Riley, Georgia Tech, Spring 2005

// For net to host and host to net
#ifdef Linux
#include <netinet/in.h>
#endif

#ifdef WIN32
#include "winsock.h"
#endif

#include <cstring>
#include <iostream>
#include <fstream>
#include <iomanip>

#define __USE_ISOC99 1
#include "math.h"

#include "aiff.h"

using namespace std;

// Class AIFF methods
AIFF::AIFF() 
    : sampleRate(0), numChannels(0), sampleSize(0),
      formBlockSize(0), ssndBlockSize(0), numSampleFrames(0),
      formSizePos(ios::beg), ssndSizePos(ios::beg), numSamplePos(ios::beg),
      remainingChunkBytes(0), COMMHdrWritten(false), SSNDHdrWritten(false),
      oddChunkSize(false)
{ // NOthing else needed
}

AIFF::~AIFF()
{
  if (i.is_open())i.close();
  if (o.is_open())o.close();
}

bool AIFF::Open(const std::string& n)   // Open specified AIFF File for reading
{
  if (i.is_open())
    {
      cout << "Oops, AIFF file already open for reading" << endl;
      return false;
    }
  i.open(n.c_str());
  if (!i)
    {
      cout << "Oops, can't open " << n << " for reading" << endl;
      return false;
    }
  char hdr[5];
  bool ok = true;
  
  ok = ReadChunkHeader(hdr);
  if (ok) if (string(hdr) != string("FORM")) ok = false;
  if (ok) 
    {
      ok = Read(hdr, 4);
      hdr[4] = '\0';
      if (string(hdr) != string("AIFF")) ok = false;
    }
  // Read the COMM header
  if (ok)
    {
      ok = ReadChunkHeader(hdr);
      if (string(hdr) != string("COMM")) ok = false;
    }
  if (ok)
    {
      ok = ReadCommonHdr();
      if (ok)
        {
          cout << "Chunk size is " << remainingChunkBytes << endl;
          cout << "Num channels is " << numChannels << endl;
          cout << "numSampleFrames is " << numSampleFrames << endl;
          cout << "sampleSize is " << sampleSize << endl;
          cout << "sampleRate is " << sampleRate << endl;
        }
    }
  
  if (!ok)
    {
      cout << "Input file " << n << " is not in AIFF format" << endl;
      return false;
    }
  remainingChunkBytes = 0;
  oddChunkSize = 0;
  return true;
}

  
// Open specified AIFF file for writing 
bool AIFF::Create(const std::string& n)
{
  if (o.is_open())
    {
      cout << "Oops, AIFF file already open for writing" << endl;
      return false;
    }
  o.open(n.c_str());
  if (!o)
    {
      cout << "Oops, can't open " << n << " for writing" << endl;
      return false;
    }
  // Write the FORM header
  string form("FORM");
  string aiff("AIFF");
  long zero = 0;
  if (!Write(form.c_str(), 4)) return false;
  formSizePos = o.tellp(); // Note the position of the form size field
  if (!Write(zero)) return false;
  formBlockSize = 0;       // Form block starts here
  if (!Write(aiff.c_str(), 4)) return false;
  return true;
}

bool AIFF::Close()
{
  if (o.is_open())
    { // Need to update the form block size, ssnd block size, and numSamples
      long ffbs = htonl(formBlockSize);
      long ssbs = htonl(ssndBlockSize);
      long nsf = htonl(numSampleFrames);
      o.seekp(formSizePos);
      Write(ffbs);
      o.seekp(ssndSizePos);
      Write(ssbs);
      o.seekp(numSamplePos);
      Write(nsf);
    }
  return true;
}


// Reading the AIFF
bool AIFF::Read(char* b, size_t count)
{
	i.read(b, std::streamsize(count));
  int actual = i.gcount();
  return (actual == (int)count);
}

bool AIFF::Read(short& s)        // Read a short to output file
{
  return Read((char*)&s, sizeof(s));
}

bool AIFF::Read(long& l)         // Read a long to output file
{
  return Read((char*)&l, sizeof(l));
}

bool AIFF::Read(unsigned long& u)// Read a unsigned long to output file
{
  return Read((char*)&u, sizeof(u));
}
  
bool AIFF::ReadSoundData(long& l, long& r)
{
  while(!remainingChunkBytes)
    { // Need to find the next SSND header
      // But first skip any (odd byte) remaining
      if (!SkipRemainingChunk()) return false;
      char hdr[5];
      if (!ReadChunkHeader(hdr)) return false;
      if (string(hdr) == string("SSND"))
        { // Found it, need to read the rest of the header
          long offset = 0;
          long blockSize = 0;
          // These are not used
          if (!Read(offset)) return false;
          if (!Read(blockSize)) return false;
          break;
        }
      // Not ssnd header, skip it
      if (!SkipRemainingChunk()) return false;
    }
  // First compute the rounded up sample size
  int rss = 4; // bytes
  if (sampleSize <= 16) rss = 2;
  if (sampleSize <= 8)  rss = 1;
  int nBytes = rss * numChannels; // bytes to read
  int shiftCount = rss * 8 - sampleSize;
  int seShiftCount = 32 - sampleSize; // Used to sign extend values
#ifndef WIN32
  unsigned char d[nBytes];
#else
  unsigned char *d = (unsigned char *) malloc(nBytes);
#endif
  if (!Read((char*)d, nBytes)) return false;
#ifdef VERBOSE_DEBUG
  // debug
  for (int j = 0; j < nBytes; ++j)
    {
      cout << hex << (int)d[j] << " ";
    }
  cout << endl;
#endif
  l = 0;
  int i = 0;
  // Compute the l value
  for (; i < rss; ++i)
    {
      l = l * 0x100 + d[i];
    }
  l >>= shiftCount;
  // Sign extend
  l = (l << seShiftCount) >> seShiftCount;
  
  // Compute the r value
  r = 0;
  if (numChannels > 1)
    {
      for (; i < rss*numChannels; ++i)
        {
          r = r * 0x100 + d[i];
        }
    }
  r >>= shiftCount;
  // Sign extend
  r = (r << seShiftCount) >> seShiftCount;
  if(0)cout << "l " << hex << l << " r " << hex << r << endl;
  return true;
}

size_t AIFF::ReadSoundData(long* l, long* r, size_t count)
{
  size_t c = 0;
  for (size_t j = 0; j < count; ++j)
    {
      if (!ReadSoundData(l[j], r[j])) break;
      ++c;
    }
  return c;
}

// Single channel read
size_t AIFF::ReadSoundDataSingle(long* l, size_t count)
{
  size_t c = 0;
  long notUsed;
  for (size_t j = 0; j < count; ++j)
    {
      if (!ReadSoundData(l[j], notUsed)) break;
      ++c;
    }
  return c;
}

bool AIFF::ReadCommonHdr()
{
  if (!Read(numChannels)) return false;
  numChannels = ntohs(numChannels);
  if (!Read(numSampleFrames)) return false;
  numSampleFrames = ntohl(numSampleFrames);
  if (!Read(sampleSize))
  sampleSize = ntohs(sampleSize) ;
  if (!Read(sampleRateCh, 10)) return false;
  sampleRate = ConvertFromExtended(sampleRateCh);
  return true;
}

bool AIFF::SkipRemainingChunk()
{
  char buf[100000];
  if (oddChunkSize) remainingChunkBytes++;
  while(remainingChunkBytes)
    { // read all remaining data
      int rsize = sizeof(buf);
      if (remainingChunkBytes < rsize) rsize = remainingChunkBytes;
      if (!Read(buf, rsize)) return false;
      remainingChunkBytes -= rsize;
    }
  oddChunkSize = false;
  return true;
}


// Writing the AIFF
bool AIFF::Write(const char* d, long l)
{ // Write data to the file
  if (!o.is_open())
    {
      cout << "Oops!  Writing to unopened output file" << endl;
      return false;
    }
  o.write(d, std::streamsize(l));
  if (o.fail())
    {
      cout << "Oops!  Write of " << l << " bytes failed" << endl;
      return false;
    }
  formBlockSize += l;
  ssndBlockSize += l;
  return true;
}

bool AIFF::Write(short s)
{
  return Write((char*)&s, sizeof(s));
}

bool AIFF::Write(long l)
{
  return Write((char*)&l, sizeof(l));
}

bool AIFF::Write(unsigned long u)
{
  return Write((char*)&u, sizeof(u));
}

bool AIFF::WriteSoundData(long l, long r)
{
  if (!o.is_open())
    {
      cout << "Oops, not open for writing" << endl;
      return false;
    }
  
  if (!COMMHdrWritten)
    {
      string comm("COMM");
      WriteChunkHeader(comm.c_str(), 18);
      short nc = htons(numChannels);
      if (!Write(nc)) return false;
      // Note location of numSampleFrames for later overwrite
      numSamplePos = o.tellp();
      // Set to zero as we re-calculate as things are written
      long nsf = htonl(0);
      if (!Write(nsf)) return false;
      short ss = htons(sampleSize);
      if (!Write(ss)) return false;
      if (!Write(sampleRateCh, 10)) return false;
      COMMHdrWritten = true;
    }
  if (!SSNDHdrWritten)
    {
      string ssnd("SSND");
      if (!Write(ssnd.c_str(), 4)) return false;
      // Note position of SSND length for later update
      ssndSizePos = o.tellp();
      long zero = 0;
      if (!Write(zero)) return false;  // Changed later
      ssndBlockSize = 0;
      if (!Write(zero)) return false;  // offset (not used)
      if (!Write(zero)) return false;  // blocksize (not used)
      SSNDHdrWritten = true;
    }
  // First compute the rounded up sample size
  int rss = 4; // bytes
  if (sampleSize <= 16) rss = 2;
  if (sampleSize <= 8)  rss = 1;
  int nBytes = rss * numChannels; // bytes to read

#ifndef WIN32
  unsigned char d[nBytes];
#else
  unsigned char *d = (unsigned char *) malloc(nBytes);
#endif
  int shiftCount = rss * 8 - sampleSize;
  l <<= shiftCount;
  r <<= shiftCount;
  // Process left and right samples
  if (rss == 4)
    {
      d[0] = (unsigned char)((l >> 24) & 0xff);
      d[1] = (unsigned char)((l >> 16) & 0xff);
      d[2] = (unsigned char)((l >>  8) & 0xff);
      d[3] = (unsigned char)((l >>  0) & 0xff);
      
      if (numChannels > 1)
        {
          d[4] = (unsigned char)((r >> 24) & 0xff);
          d[5] = (unsigned char)((r >> 16) & 0xff);
          d[6] = (unsigned char)((r >>  8) & 0xff);
          d[7] = (unsigned char)((r >>  0) & 0xff);
        }
    }
  else if (rss == 2)
    {
      d[0] = (unsigned char)((l >>  8) & 0xff);
      d[1] = (unsigned char)((l >>  0) & 0xff);
      if (numChannels > 1)
        {
          d[2] = (unsigned char)((r >>  8) & 0xff);
          d[3] = (unsigned char)((r >>  0) & 0xff);
        }
    }
  else
    {
      d[0] = (unsigned char)((l & 0xff));
      d[1] = (unsigned char)((r & 0xff));
    }
  if (!Write((char*)d, nBytes)) return false;
  numSampleFrames++;
  return true;
}

size_t AIFF::WriteSoundData(long* l, long* r, size_t count)
{
  int c = 0;
  for (size_t j = 0; j < count; ++j)
    {
      if (!WriteSoundData(l[j], r[j])) return c;
      ++c;
    }
  return c;
}

size_t AIFF::WriteSoundDataSingle(long* l, size_t count)
{
  int c = 0;
  for (size_t j = 0; j < count; ++j)
    {
      if (!WriteSoundData(l[j], 0)) return c;
      ++c;
    }
  return c;
}

short AIFF::NumChannels()
{
  return numChannels;
}

short AIFF::SampleSize()
{
  return sampleSize;
}

double AIFF::SampleRate()
{
  return sampleRate;
}

bool AIFF::NumChannels(short nc)  // Specify number of channels
{
  numChannels = nc;
  return true;
}

bool AIFF::SampleSize(short ss)   // Specify sample size
{
  sampleSize = ss;
  return true;
}

bool AIFF::SampleRate(double sr)  // Specify sample rate
{
  sampleRate = sr;
  ConvertToExtended(sampleRate, sampleRateCh);
  return true;
}

bool AIFF::ReadChunkHeader(char* id)
{ // Note the id must have 5 bytes allocated, since we tack on a '\0' byte
  id[4] = 0;
  if (!Read(id, 4)) return false;
  if (!Read(remainingChunkBytes)) return false;
  remainingChunkBytes = ntohl(remainingChunkBytes);
  oddChunkSize = ((remainingChunkBytes & 0x01) == 0x01);
  return true;
}

bool AIFF::WriteChunkHeader(const char* id, long chunkSize)
{
  if (!Write(id, 4)) return false;
  chunkSize = htonl(chunkSize);
  if (!Write(chunkSize)) return false;
  return true;
}

// Static methods
double AIFF::ConvertFromExtended(char* d)
{
  volatile char expc[2];
  volatile char manc[8];
  memcpy((void*)expc, d, sizeof(expc));
  memcpy((void*)manc, &d[2], sizeof(manc));
  
  volatile short* exp = (short*)expc;
  volatile long long* man = (long long*)manc;

  *exp = ntohs(*exp);
  // No ntohll, so we have to cheat
  if (htons(1) != 1)
    { // This is little endian, need to swap
      long* pl = (long*)man;
      long  save;
      save = ntohl(pl[1]);
      pl[1] = ntohl(pl[0]);
      pl[0] = save;
    }
  
  if (*man < 0)
    { // Normalized
      double de = (double)(*exp - 16383);
      double two = 2.0;
      double dman = (double)(*man & 0x7fffffffffffffffLL);
      double r = pow(two,de);
      r = r * (1.0 + dman / pow(2.0, 63.0));
      return r;
    }
  else return 0; // code later
}

void AIFF::ConvertToExtended(double d, char* e)
{
  if (d < 0) d = -d; // Need to deal with sign bits
#ifndef WIN32
  double exp = log2(d);
#else
double exp = log(d)/log((double)2);
#endif
  int iexp = (int)exp;
  double man = d / pow(2.0, (double)iexp);
  unsigned long iman = htonl((unsigned long)(man*(double)0x80000000));
  
  iexp += 16383;
  short sexp = htons(iexp);
  
  memcpy(e,     &sexp, 2);
  memcpy(&e[2], &iman, 4);
  memset(&e[6], 0, 4); // Clear lower 4 bytes
}

double AIFF::SoundIntensity(long* d, size_t l)
{
  double intensity = 0;
  for (size_t j = 0; j < l; ++j)
    {
      intensity += (double)d[j] * (double)d[j];
    }
  return intensity;
}


