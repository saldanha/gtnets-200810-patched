// Test program for Grid of nodes object
// George F. Riley.  Georgia Tech, Spring 2002

#include <iostream>
#include <stdlib.h>

#include "simulator.h"
#include "scheduler.h"
#include "grid.h"
#include "ipaddr.h"
#include "node.h"
#include "routing-static.h"
#include "rng.h"
#include "tcp-tahoe.h"
#include "application-tcpsend.h"
#include "ipv4.h"
#include "routing-nixvector.h"
#include "hex.h"
#include "ratetimeparse.h"
#include "args.h"
#include "validation.h"

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
#ifdef TEST_NIXVEC
  NixVectorOption nv;
  nv.Add(0xf, 4);     nv.Dump();
  nv.Add(0x3, 2);     nv.Dump();
  nv.Add(0x1, 2);     nv.Dump();
  nv.Add(0x00ff, 16); nv.Dump();
  nv.Add(0xabc, 12);  nv.Dump();

  nv.Reset();
  NixBits_t nb = nv.Extract(4);
  cout << "nb " << Hex8(nb) << endl;
  nb = nv.Extract(20);
  cout << "nb " << Hex8(nb) << endl;
  nb = nv.Extract(12);
  cout << "nb " << Hex8(nb) << endl;
  exit(0);
#endif

  Count_t xs = 5;
  Count_t ys = 5;
  Arg("xs", xs, 5);
  Arg("ys", ys, 5);
  Arg::ProcessArgs(argc, argv);

  bool useNix = Arg::Specified("usenix");
  bool useTrace = Arg::Specified("usetrace");
  
  Trace* gs = Trace::Instance();
  gs->IPDotted(true);
  if (useNix)
    {
      // Set to NixVector routing
      Routing::SetRouting(new RoutingNixVector());
      if (useTrace) gs->Open("gridtrace-nv.txt");
    }
  else
    {
      // Set to static routing
      Routing::SetRouting(new RoutingStatic());
      // Open the trace file
      if (useTrace) gs->Open("gridtrace.txt");
    }
  // Create the simulator object
  Simulator s;

  // Enable all l3 tracing
  IPV4::Instance()->SetTrace(Trace::ENABLED);

  // Point to Point link object to use
  Linkp2p lk(Rate("10Mb"), Time("10ms"));
  
  // Create the grid
  Grid g(xs, ys, IPAddr("192.0.1.1"), lk);
  
  // Create with no internal IP addresses..we assign them to edges below
  //Grid g(xs, ys, IPADDR_NONE, lk);
  // Assign ip addresses to edges
  //IPAddr_t nextip = IPAddr("10.0.0.1");

  // Set the bounding box of the grid for the animation
  g.BoundingBox(Location(0,0), Location(1,1));
  
  // Create a TCP endpoint to use for the tcp applications below
  TCP::LogFlagsText(true);    // Log flags in text mode
  TCPTahoe client;
  client.SetTrace(Trace::ENABLED);
  // Create servers on the top row
  for (Count_t x = 0; x < xs; ++x)
    {
      Node* nt = g.GetNode(x,ys-1); // Top row
      TCPTahoe* server = new TCPTahoe(nt);
      server->SetTrace(Trace::ENABLED);
      server->Bind(80);
      server->Listen();
    }

  Uniform selectServer(0, xs); // Rng for server selection
  Uniform startTime(0, 0.1);   // Rng for randomize start times
  for (Count_t x = 0; x < xs; ++x)
    {
      Node* nb = g.GetNode(x,0); // Bottom row
      Node* sn = g.GetNode(selectServer.IntValue(), ys-1);
      
      TCPSend* app = (TCPSend*)nb->AddApplication(
          TCPSend(sn->GetIPAddr(), 80,
                  Constant(100000)));
      app->Start(startTime.Value());
    }
  // Print some debug info for each
  for (Count_t y = 0; y < ys; ++y)
    {
      for (Count_t x = 0; x < xs; ++x)
        {
          Node* n = g.GetNode(x, y);
          cout << "Node " << x << " " << y
               << " location " << n->LocationX() << " " << n->LocationY()
               << " ip " << (string)IPAddr(n->GetIPAddr()) 
               << " numer of ifaces " << n->InterfaceCount()
               << endl;
        }
    }
  s.StopAt(10.0);
  s.Progress(1.0);
  if (Arg::Specified("animate") && !Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  
  s.Run(); // Run the simulation
  Count_t fibSize = 0;
  const NodeVec_t& nodes = Node::GetNodes();
  for (NodeVec_t::const_iterator i = nodes.begin(); i != nodes.end(); ++i)
    {
      fibSize += (*i)->RoutingFibSize();
    }
  cout << "Size of Routing FIB is " << fibSize<< endl;
  cout << "Total events processed "
       << Scheduler::Instance()->TotalEventsProcessed() << endl;
  cout << "Setup time " << s.SetupTime() << endl;
  cout << "Route time " << s.RouteTime() << endl;
  cout << "Run time "   << s.RunTime() << endl;
}

  


