
/*
  
  This is the base class for all 'routing policies'
  they use the add and delete methods of the RouteTable to
  modify it.
  
 */

#ifndef __application_routing_h
#define __application_routing_h

#include "application.h"
#include "routing.h"


class ApplicationRouting : public Application {

 public:
	ApplicationRouting();
	ApplicationRouting(L4Protocol* l4proto_);
	
	~ApplicationRouting();
	
	virtual void StartApp()=0;
	virtual void StopApp()=0;
	virtual void DefaultRoute(rt_value_t* )=0; /* the key will be
					       000000/0 in this case */
	virtual void InitializeRoutes()=0;
	virtual void ReInitializeRoutes()=0;
 public:
	L4Protocol* l4proto;
};

#endif
