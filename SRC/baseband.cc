// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//     
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


// Georgia Tech Network Simulator - Bluetooth Baseband class
// George F. Riley, Georgia Tech, Spring 2004

#include "packet.h"
#include "timer.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <list>
#include "qtwindow.h"
#include "rng.h"
#include "timerbuckets.h"

#include "lmp.h"

#include "l2cap.h"
#include "baseband.h"


using namespace std;

const char *PacketType[] = {
  "NULL_PACKET", "POLL_PACKET", "FHS_PACKET", "DM1_PACKET",
  "DH1_PACKET", "HV1_PACKET", "HV2_PACKET", "HV3_PACKET",
  "DV_PACKET", "AUX1_PACKET", "DM3_PACKET", "DH3_PACKET",
  "", "",
  "DM5_PACKET", "DH5_PACKET"};

BaseBand::BaseBand() {
  pNode = NULL;
  State = STANDBY;
  pRXBuf = NULL;
  ulClkN = ulClkF = ulClkE = ulClk = 0;
  ucFlow = GOFLOW;
  ucArqn =NAK;
     ucSeqn = 1; // the first CRC data packet on m/s sides set this to 1.
  ucAMAddr = 0;
  ucSeqnOld = 0;
  ucAcked = 1;
  ucRecvFreq = 0;
  
  usN = 0;
  ucIdNo = 0;
  usTrainSent = 0;
  ucTrainType = 'A'; 
  ucKoffsetStar = 0;
  
  CLOCKTICKTimeout = NULL;
  NewConnectionTimeout = NULL;
  PageTimeout = NULL;
  PageRspTimeout = NULL;
  InquiryTimeout = NULL;
  
  TxPower = 0;
  RxPower = 0;
  RxAcErrPower = 0;
  ActiveNoPower = 0;
  HoldPower = 0;

  memset(&SlaveAddr, 0, sizeof(SlaveAddr));
  memset(&MasterAddr, 0, sizeof(MasterAddr));
  memset(cActiveList, 0, sizeof(cActiveList));

  memset(&ActiveAddr, 0, sizeof(ActiveAddr));
  useTimerBuckets = false;

  memset(cLMPConnected, 1, sizeof(cLMPConnected));
  bPiconetSetupDone = false;
  pLMP = NULL;
  pL2CAP = NULL;

  ucRRQueue = 0;
  bMyTurn = false;
  bStartPoll = false;

  dTotalDelay = 0;  //stat for hold mode packet delay

  ulPktCount = 0;  //stat for number of L2CAP packet
}

void      
BaseBand::Start(RoleType myRole, double t) {
  role = myRole;
  // 
    Uniform randnum(0, 0x0FFFFFFF); //
#ifdef TEST  
  TestFreq();
#endif
  
  ulClkN = 1 + (uLong)randnum.Value(); //(int) (0x0FFFFFFF*rand()/(RAND_MAX+1.0));
  ulClkN = ulClkN & 0x0FFFFFFF;
  ulClkN = ulClkN & 0x0FFFFFFC;
  
  cout <<" At Start, native clock tick value = " << hex << ulClkN << endl;
  BdAddr tmpAddr = {{0x0000, 0x0000, 0x0000}};
  memcpy(&tmpAddr, &LAP_GIAC, sizeof(LAP_GIAC));
  if(role == MASTER) {
    MasterAddr = pNode->GetBdAddr();
    State = INQUIRY;

    Inquiry(); //do inquiry job here.
    
    InquiryTimeout = NULL;
    ScheduleTimer(BaseBandEvent::INQUIRY_TO, InquiryTimeout , INQUIRYTIMEOUT);  
    
  }else {

    SlaveAddr = pNode->GetBdAddr();
    State = INQUIRY_SCAN;
    PreviousState = INQUIRY_SCAN;
    
    ucRecvFreq = HopSelection(0 /* no use */,INQUIRY_SCAN_HOPPING, tmpAddr);
    cout << "SLAVE recv freq = " << (int) ucRecvFreq << endl;
  }
  //run the native clock from now on.
  CLOCKTICKTimeout = NULL;
  ScheduleTimer(BaseBandEvent::CLOCK_TICK, CLOCKTICKTimeout, 
          CLOCKTICK);
    
  return;
}

void 
BaseBand::Stop(double t)
{ // Schedule the Stop Event
//  BaseBandEvent* stopEvent = new BaseBandEvent(BaseBandEvent::STOP_RUN);
//  Scheduler::Schedule(stopEvent, t, this);
}

//called upon lost connection
void
BaseBand::Reset() {
  State = STANDBY;
  //reset all other parameters.
  //Initialize();
  return;
}

void
BaseBand::InstallLMP(LMP *pLMPStack) {
  assert(pLMPStack!=NULL);
  pLMP = pLMPStack;
  pLMP->pBaseBand = this;
  if(pL2CAP!=NULL)
    pL2CAP->AttachLMP(pLMP);
  return;
}

void 
BaseBand::InstallL2CAP(L2cap *pL2CAPStack) {
  assert(pL2CAPStack!=NULL);
  pL2CAP = pL2CAPStack;
  pL2CAP->pLowerProtocol = this;
  if(pLMP!=NULL)
    pL2CAP->AttachLMP(pLMP);
  return;
}

void
BaseBand::Initialize(StateType currentState, RoleType myRole) {
  State = currentState;
  pRXBuf = NULL;
  //ulClkN =ulClkE = ulClk =  //don't initialize native clk and clke.
  ulClkF = 0;
  ucFlow = GOFLOW;
  ucArqn =NAK;
     ucSeqn = 1; // the first CRC data packet on m/s sides set this to 1.
  ucAMAddr = 0;
  ucSeqnOld = 0;
  ucAcked = 1;
  ucRecvFreq = 0;
  
  usN = 0;
  ucIdNo = 0;
  usTrainSent = 0;
  ucTrainType = 'A'; 
  ucKoffsetStar = 0;
  
  //CLOCKTICKTimeout = NULL;//don't initialize again.
  NewConnectionTimeout = NULL;
  PageTimeout = NULL;
  PageRspTimeout = NULL;
  InquiryTimeout = NULL;
  
  role = myRole;
  
  if(role == MASTER){
    MasterAddr = pNode->GetBdAddr();
    memset(&SlaveAddr, 0, sizeof(SlaveAddr));
  }
  else if(role == SLAVE){
    SlaveAddr = pNode->GetBdAddr();
    memset(&MasterAddr, 0, sizeof(MasterAddr));
    bMyTurn = false;
    bStartPoll = false;

  }
}

void
BaseBand::DataRequest(uChar ucLogicChannel, uShort usLen,
        uChar *ucPayload, BdAddr DstAddr) {
  assert(ucLogicChannel <0x04);
  assert(ucLogicChannel != 0x00);
  SendPacket(ucLogicChannel, usLen, ucPayload, DstAddr);
  return;
}

void
BaseBand::SendPacket(uChar ucL_CH, uShort usLen, uChar *ucPayload,
        BdAddr DstAddr, uChar ucSlot) {
  uChar ucSynWord[8];
  SynWordConstruct(MasterAddr, (uChar *)&ucSynWord);
  AccessCode72 *pAccessCode72 = new AccessCode72((uChar *)&ucSynWord);
  DetermineFlowFlag();
  
  uChar ucHec = 0x00; //for debug only
  uChar ucTmpAddr = 0xFF;
  if(role==SLAVE)
    ucTmpAddr = ucAMAddr;
  else {
    //for park sending broadcast pkt
    if(pLMP->isBeacon)
      ucTmpAddr = 0;
    else {
      for(uChar i= 0; i<sizeof(cActiveList); i++){
        if(ActiveAddr[i] == DstAddr)
          ucTmpAddr = cActiveList[i]; 
      }
      if(ucTmpAddr == 0xFF) {
        cout<<"Invalid destination??"<<endl;
        return;
      }
      //check whether current context is
      //for this Dst, if not, switch context.
      if(ucAMAddr!=ucTmpAddr) {
        SwitchContext(ucAMAddr, ucTmpAddr);
      }
    } 
  }

  Header *pPktHdr = new Header;
  pPktHdr->ucAMAddr = ucTmpAddr;
  //for park broadcast pkt
  if(usLen == 0)
    pPktHdr->ucType = NULL_PKT;
  else if(usLen<32)
    pPktHdr->ucType = DM1_PKT;
  else {
    pPktHdr->ucType = DM3_PKT;
    ucSlot = 3;
  }
  pPktHdr->ucFlow = ucFlow;
  pPktHdr->ucArqn = ucArqn;
  pPktHdr->ucSeqn = ucSeqn;
  pPktHdr->ucHec = ucHec;

  PayloadHdr *pPayloadHdr = NULL;
  PayloadHdrMultiSlot *pPayloadHdrMulSlot = NULL;
  //for park broadcast pkt
  uChar ucFreq;
  if(pLMP->isBeacon)
    ucFreq = HopSelection(ulClkF /*no use */, CHANNEL_HOPPING,
        MasterAddr);
  else {
    ulClk = ulClk + 2;
    ucFreq = HopSelection(ulClkF /*no use */, CHANNEL_HOPPING,
          MasterAddr);
    ulClk = ulClk - 2;
  }
  
  //determine the flow flag to control the flow at L2CAP level
  uChar ucFlag;
  if(ucL_CH == LM_CH) //LMP msg
    ucFlag = GOFLOW;
  else
    //ucFlag = (pRXBuf->pNext == NULL) ? GOFLOW : STOPFLOW;
    ucFlag = GOFLOW; //MUST DO
  
  //for park broadcast pkt
  if(usLen == 0)
    Send(pAccessCode72, pPktHdr, NULL, 0.0, ucFreq); //SLOTTIME =625 us
  else if(ucSlot==1) {
    pPayloadHdr =  new PayloadHdr(ucL_CH, ucFlag, (uChar) usLen,
            ucPayload);
    Send(pAccessCode72,pPktHdr, (uChar *)pPayloadHdr, SLOTTIME, ucFreq);
  }else {
    pPayloadHdrMulSlot = new PayloadHdrMultiSlot(ucL_CH, ucFlag, usLen,
            ucPayload);
    Send(pAccessCode72, pPktHdr, (uChar *)pPayloadHdrMulSlot, SLOTTIME, ucFreq);
  }
  
  return;
}

void
BaseBand::SynWordConstruct(uLong ulLAP, uChar *pSynWord) {
  
  assert(pSynWord!=NULL);
  ulLAP = ulLAP & 0x00FFFFFF;
  memset(pSynWord, 0, 8); 
  memcpy(pSynWord, &ulLAP, 3); //copy 3 bytes.
  return;
}

void
BaseBand::SynWordConstruct(BdAddr addr, uChar *pSynWord) {

  uLong ulLAP = addr.usAddr[0] + ((addr.usAddr[1] & 0x00FF)<<16);
  SynWordConstruct(ulLAP, pSynWord);
  return;
}

//This routine can be used to send normal packets, 
//NULL, POLL, FHS packets as well.
//Note: BaseBandPacket allocated will be released after Transmit
//      through "delete (Packet *)pPacket;". The same rule applys
//      to IDPacket.
void
BaseBand::Send(AccessCode72 *pAccessCode72, Header *pPktHdr, 
        uChar *pPayload, double dTime, uChar ucFreq) {
  //note: pPayload can be NULL.
    
  uShort usLen = 0;
  PayloadHdr *pPayloadHdr = NULL;
  PayloadHdrMultiSlot *pPayloadHdrMulSlot = NULL;
  uChar *pPayloadLocal = NULL; 
  
  cout <<"XXXXXXX Sending "<< PacketType[pPktHdr->ucType]
      <<" at Freq = "  << (int)ucFreq 
      <<" Time = "<< Simulator::Now() + dTime <<" XXXXXXX"<< endl;
  
  if(pPayload == NULL) {
    BaseBandPacket *pPkt = NULL; 
    switch(pPktHdr->ucType) {
      case NULL_PKT:
        pPkt = new BaseBandPacket(ucFreq,
            sizeof(AccessCode72) + sizeof(Header),
            *pAccessCode72, *pPktHdr, NULL);
        break;
      case POLL_PKT: //requires comfirmation.
        pPkt = new BaseBandPacket(ucFreq,
            sizeof(AccessCode72) + sizeof(Header),
            *pAccessCode72, *pPktHdr, NULL);
        break;
      default:
        cout <<" Wrong!" << endl;
        break;
    }

    Transmit(pPkt, dTime);
    delete pAccessCode72;
    delete pPktHdr;
    
    return;
  }

  
  //FHS packet.
  if(pPktHdr->ucType == FHS_PKT) {
    BaseBandPacket *pBasebandPkt = new BaseBandPacket(
            ucFreq,
            sizeof(AccessCode72) + sizeof(Header) + sizeof(FHSPayload),//30,
            *pAccessCode72,
            *pPktHdr, //30 bytes: 240 bits, including FEC, CRC
            pPayload);

    Transmit(pBasebandPkt, dTime);
    delete pAccessCode72;
    delete pPktHdr;

    return;
  }
      
  if(pPktHdr->ucType < DM3_PKT) {//single slot packet
    pPayloadHdr = (PayloadHdr *)pPayload;
    usLen = pPayloadHdr->ucLength + 1; 
              //pData length + 1 byte payload header
    pPayloadLocal = new uChar[usLen];
    memcpy(pPayloadLocal, pPayloadHdr, 1);
    memcpy(pPayloadLocal + 1, pPayloadHdr->pData, pPayloadHdr->ucLength);
    delete pPayloadHdr->pData;
    //delete pPayloadHdr;
    
  }else {
    pPayloadHdrMulSlot = (PayloadHdrMultiSlot *)pPayload;
    
    usLen = (pPayloadHdrMulSlot->ucLengthH<<5) + 
        pPayloadHdrMulSlot->ucLengthL + 2; 
              // include 2 bytes payload header.
    pPayloadLocal = new uChar[usLen];
    memcpy(pPayloadLocal, (void *)pPayloadHdrMulSlot, 2);
    memcpy(pPayloadLocal + 2, pPayloadHdrMulSlot->pData, usLen - 2); 
        //exclude 2 bytes header
    delete pPayloadHdrMulSlot->pData;
    //delete pPayloadHdrMulSlot;

  }
  
  BaseBandPacket *pBaseBandPkt = new BaseBandPacket(
          ucFreq,
          sizeof(AccessCode72) + sizeof(Header) + usLen,
          *pAccessCode72,
        *pPktHdr, pPayloadLocal); 
    //in this constructor, no data will copied.
#ifdef TEST  
  cout <<"ucSeqn = " <<(int) ucSeqn << endl;
  cout <<"Old ucSeqnOld = " <<(int) ucSeqnOld << endl;
#endif  
  if(RetransmitFilter(pPktHdr->ucType)) {
    //Transmit LMP packet of connection establish in normal timing.
    if(pPktHdr->ucType < DM3_PKT) {//single slot packet
      if((pPayloadHdr->ucLogicChannel == LM_CH)&&(!bStartPoll)) {
        pBaseBandPkt->Hdr.ucSeqn = ucSeqn;
        ucSeqn = !ucSeqn;
        Transmit(pBaseBandPkt, dTime);
      }
      else {
        //Packet Queue for LMP and L2CAP
        PktStruct tmpPkt;
        tmpPkt.TimeStamp = Simulator::Now();
        tmpPkt.pPacket = pBaseBandPkt;
        int index = pBaseBandPkt->Hdr.ucAMAddr - 1;
        if(pPayloadHdr->ucLogicChannel == LM_CH) {
          cout <<"Enqueue LMP single slot packet."<< (uShort)ucAMAddr <<endl;
          if(role == SLAVE)
            LmpPktSlaveQueue.push_back(tmpPkt);
          else
            LmpPktMasterQueue[index].push_back(tmpPkt);
        }
        else if((pPayloadHdr->ucLogicChannel == L2CAP_CH)
            ||(pPayloadHdr->ucLogicChannel == L2CAP_CON_CH)) {
          cout <<"Enqueue L2CAP single slot packet."<< (uShort)ucAMAddr <<endl;
          if(role == SLAVE)
            L2capPktSlaveQueue.push_back(tmpPkt);
          else
            L2capPktMasterQueue[index].push_back(tmpPkt);
        }
        else
          cout << "Undefined logic channel."<<endl;
      }
      delete pPayloadHdr;
    }
    if(pPktHdr->ucType >= DM3_PKT) {//multi slot packet
      if((pPayloadHdrMulSlot->ucLogicChannel == LM_CH)&&!(pL2CAP->pUpperProtocol->IsConn())) {
        pBaseBandPkt->Hdr.ucSeqn = ucSeqn;
        ucSeqn = !ucSeqn;
        Transmit(pBaseBandPkt, dTime);
      }
      else {
        //Packet Queue for LMP and L2CAP
        PktStruct tmpPkt;
        tmpPkt.TimeStamp = Simulator::Now();
        tmpPkt.pPacket = pBaseBandPkt;
        int index = pBaseBandPkt->Hdr.ucAMAddr - 1;
        if(pPayloadHdrMulSlot->ucLogicChannel == LM_CH) {
          cout <<"Enqueue LMP multi slot packet."<<endl;
          if(role == SLAVE)
            LmpPktSlaveQueue.push_back(tmpPkt);
          else
            LmpPktMasterQueue[index].push_back(tmpPkt);
        }
        else if((pPayloadHdrMulSlot->ucLogicChannel == L2CAP_CH)
            ||(pPayloadHdrMulSlot->ucLogicChannel == L2CAP_CON_CH)) {
          cout <<"Enqueue L2CAP multi slot packet."<<endl;
          if(role == SLAVE)
            L2capPktSlaveQueue.push_back(tmpPkt);
          else
            L2capPktMasterQueue[index].push_back(tmpPkt);
        }
        else
          cout << "Undefined logic channel."<<endl;
      }
      delete pPayloadHdrMulSlot;
    }
  }
  else {
    ;//TODO: Send old packet.
    //      enqueue current packet.
  }
  delete pAccessCode72;
  delete pPktHdr;
  return;
}


//Used to send ID packet only.
void
BaseBand::SendIDPacket(uLong ulLAP, uChar ucFreq, double dTime) {
  uChar ucSynWord[8];
  ulLAP = ulLAP & 0x00FFFFFF;
  SynWordConstruct(ulLAP, (uChar *)&ucSynWord);
  AccessCode68 *pAccessCode68 = new AccessCode68((uChar *)&ucSynWord);

  IDPacket *pPacket = new IDPacket;
  pPacket->usLen = sizeof(AccessCode68);
  pPacket->ucFreq = ucFreq;
  pPacket->accessCode68 = *pAccessCode68;
  delete pAccessCode68;

  Transmit(pPacket, dTime);
#ifdef TEST
  printf("@@@ID Packet freq = %d\n", ucFreq);
#endif
  if((State == INQUIRY) || (State == PAGE))
    UpdateInqPageParameter();

  return;
}

void
BaseBand::ScheduleTimer(Event_t e, BaseBandEvent*& ev, Time_t t) {
  if(ev) {
      ;//TODO:
  }else {
     ev = new BaseBandEvent(e);
    DEBUG0(cout << "BaseBand " << this << " scheduling new timer "
         << ev->event << " time " << Simulator::Now()
         << " for " << t << " secs in future" << endl);
    if(useTimerBuckets)
      TimerBucket::Instance()->Schedule(ev, t, this);
    else
      timer.Schedule(ev, t, this);
   }
  return;
}

void 
BaseBand::CancelTimer(BaseBandEvent*& ev, bool delTimer)
{
  if(ev) {
    if (useTimerBuckets) {
      TimerBucket::Instance()->Cancel(ev);
      if(ev->evList) 
              // For bucketed timers, canceled events are deleted later
              // So just nil the pointer at this point
        ev = NULL;
      else {
              if(delTimer) { // Delete it
                delete ev;
          ev = NULL;
        }
      }
    } else {
      timer.Cancel(ev);
      if (delTimer) { // Delete it
        delete ev;
        ev = nil;
      }
    }
  }
  return;
}


void
BaseBand::Timeout(TimerEvent *pEvent) {
  BaseBandEvent *pBasebandEvent = (BaseBandEvent *)pEvent;
  BdAddr tmpAddr = {{0x0000, 0x0000, 0x0000}};
  uChar ucFreq = 0;

  DEBUG(9,(cout << "BaseBand::Timeout this " << this << " type "
           << pBasebandEvent->event
           << " time " << Simulator::Now() << endl));
  switch(pBasebandEvent->event) {
    case BaseBandEvent::CLOCK_TICK:
      ulClkN++;
      ulClk++; 
      ulClkE++; // update all these clocks
  
      if((role == MASTER) && bStartPoll) {
        for(uChar ucIndex=0;ucIndex<7;ucIndex++) {
          if(cActiveList[ucIndex]>0) {
            if(usTpollCount[ucIndex]>0)
              usTpollCount[ucIndex]--;
            if((usTpollCount[ucIndex]==0)&&(LmpPktMasterQueue[ucIndex].size()==0)
              &&(L2capPktMasterQueue[ucIndex].size()==0)) {
              usTpollCount[ucIndex]=usTpoll[ucIndex];
              if(ucAMAddr!=(ucIndex+1))
                SwitchContext(ucAMAddr, ucIndex+1);
              SendPollPacket(ucAMAddr);
              //usTpollCount[ucIndex] = usTpoll[ucIndex];
            }//if
          }//if
        }//for
      }
        
      if((!pLMP->IsHold()) && ((!pLMP->SniffEnabled()) || (pLMP->SniffEnabled() && pLMP->IsSniff())))
        ActiveNoPower += ACTIVENOPOWER/2;  //power measurement
      else
        HoldPower += HOLDPOWER/2;
          
      if((State == MASTER_RSP) && (GetBits((uShort)ulClkE,1 , 2)==0))
        usN++;
      if((State == SLAVE_RSP) && (GetBits((uShort)ulClkN, 1, 2) == 0))
        usN++;
      if(role == MASTER)
        UpdateNeighborClockE();
      if(role == SLAVE && State == STANDBY) {
        InquiryBackoff--;
        if(InquiryBackoff==0) {
          PreviousState = State; //should be STANDBY
          State = INQUIRY_SCAN;
          cout <<" Returning to INQUIRY_SCAN state" <<endl;
        }
      }
      
      //master TX slot
      if((State == INQUIRY) && (ulClkN%4 < 2) &&(InquiryTimeout!=NULL)){
        memcpy(&tmpAddr, &LAP_GIAC, sizeof(LAP_GIAC));
        ucFreq = HopSelection(/*ulClkN,*/ ulClkF, INQUIRY_HOPPING,
            tmpAddr);
        SendIDPacket(LAP_GIAC, ucFreq/*, SLOTTIME*/); //General inquiry
      }
      if((role == MASTER) && ((ulClkN%4)>=2))
        UpdateRecvFreq();

      //Page the neighbor;
      //After completing Inquiry, we try to page all found devices
      if((role == MASTER)&&(!bPiconetSetupDone)&&
        (InquiryTimeout == NULL)&&((ulClkN%4) < 2)) {
        if(SlaveAddr == tmpAddr){
          PageDevices();
        } else if(State==PAGE){
          //determine the frequence of sending
          ucFreq = HopSelection(/*ulClkN,*/ ulClkF, PAGE_HOPPING,
              SlaveAddr);
          SendIDPacket( (((SlaveAddr.usAddr[1] & 0x00FF) <<16) + (SlaveAddr.usAddr[0])),
              ucFreq);
        }
      }

      //Dequeue LMP or L2CAP packet after application starts up.
      //if(pL2CAP->pUpperProtocol->IsConn()) {
      if(!pLMP->IsHold()) {
      if((!pLMP->SniffEnabled()) || (pLMP->SniffEnabled()&&pLMP->IsSniff())) {
        if(bStartPoll&&(role == MASTER)&&(ulClk%4==0)) {
          //for park
          if(pLMP->isBeacon) {
            BdAddr tmpAddr;
            memset(&tmpAddr, 0, sizeof(tmpAddr));
            SendPacket(L2CAP_CH, 0, NULL, tmpAddr);
            pLMP->isBeacon = false;
          }      
          else if(PollNullPktQueue.size() > 0) {
            //cout << "Dequeue Master POLL packet " <<endl;
            PktStruct& tmpPkt = PollNullPktQueue.front();
            uChar ucIndex = tmpPkt.pPacket->Hdr.ucAMAddr;
            cout << "Dequeue Master POLL packet " << (uShort)ucIndex <<endl;
            if(ucAMAddr!=ucIndex)
              SwitchContext(ucAMAddr, ucIndex);
            uChar ucFreq = HopSelection(ulClkF, CHANNEL_HOPPING, MasterAddr);
            tmpPkt.pPacket->ucFreq = ucFreq;
            tmpPkt.pPacket->Hdr.ucSeqn = ucSeqn;
            //ucSeqn = !ucSeqn;
            Transmit(tmpPkt.pPacket, 0.0);
            PollNullPktQueue.pop_front();
            usTpollCount[ucIndex-1] = usTpoll[ucIndex-1];
            if(pLMP->SniffEnabled() && (State == CONNECTION)){
              uLong ulSniffAttempRem = pLMP->GetSniffAttemp() - (ulClk - pLMP->ulSniffClk)/2;
              uLong ulSniffRem = (ulSniffAttempRem > pLMP->GetSniffTo()) ? 
                    ulSniffAttempRem: pLMP->GetSniffTo();
              pLMP->CancelTimer(pLMP->SniffAttempTimeout, true);
              pLMP->ScheduleTimer(LMPEvent::SNIFFATTEMP_TIMEOUT, 
                    pLMP->SniffAttempTimeout, ulSniffRem*SLOTTIME);
            }
            //ucRRQueue = (ucRRQueue%7) - 1;
          }//POLL_PKT
          else {
            while(cActiveList[ucRRQueue]==0)
              ucRRQueue=(ucRRQueue+1)%7; 
            if(LmpPktMasterQueue[ucRRQueue].size() > 0) {
              cout << "Dequeue Master LMP packet " << (uShort)(ucRRQueue+1) <<endl;
              if(ucAMAddr!=(ucRRQueue+1))
                SwitchContext(ucAMAddr, ucRRQueue+1);
              PktStruct& tmpPkt = LmpPktMasterQueue[ucRRQueue].front();
              assert(ucAMAddr==tmpPkt.pPacket->Hdr.ucAMAddr);
              uChar ucFreq = HopSelection(ulClkF, CHANNEL_HOPPING, MasterAddr);
              tmpPkt.pPacket->ucFreq = ucFreq;
              tmpPkt.pPacket->Hdr.ucSeqn = ucSeqn;
              ucSeqn = !ucSeqn;
              Transmit(tmpPkt.pPacket, 0.0);
              LmpPktMasterQueue[ucRRQueue].pop_front();
              usTpollCount[ucRRQueue] = usTpoll[ucRRQueue];
            }//LMP
            else if(L2capPktMasterQueue[ucRRQueue].size() > 0) {
              cout << "Dequeue Master L2CAP packet " << (uShort)(ucRRQueue+1) <<endl;
              if(ucAMAddr!=(ucRRQueue+1))
                SwitchContext(ucAMAddr, ucRRQueue+1);
              PktStruct& tmpPkt = L2capPktMasterQueue[ucRRQueue].front();
              assert(ucAMAddr==tmpPkt.pPacket->Hdr.ucAMAddr);
              ulPktCount++;
              dTotalDelay += Simulator::Now() - tmpPkt.TimeStamp;
              cout << "Total number of L2CAP packet " << ulPktCount<<endl;
              cout << "Total delay of L2CAP packet " << dTotalDelay<<endl;
              uChar ucFreq = HopSelection(ulClkF, CHANNEL_HOPPING, MasterAddr);
              tmpPkt.pPacket->ucFreq = ucFreq;
              tmpPkt.pPacket->Hdr.ucSeqn = ucSeqn;
              ucSeqn = !ucSeqn;
              Transmit(tmpPkt.pPacket, 0.0);
              L2capPktMasterQueue[ucRRQueue].pop_front();
              //for sniff
              //if(role == MASTER) {
              if(pLMP->SniffEnabled() && (State == CONNECTION)){
                uLong ulSniffAttempRem = pLMP->GetSniffAttemp() - (ulClk - pLMP->ulSniffClk)/2;
                uLong ulSniffRem = (ulSniffAttempRem > pLMP->GetSniffTo()) ? 
                      ulSniffAttempRem: pLMP->GetSniffTo();
                pLMP->CancelTimer(pLMP->SniffAttempTimeout, true);
                pLMP->ScheduleTimer(LMPEvent::SNIFFATTEMP_TIMEOUT, 
                      pLMP->SniffAttempTimeout, ulSniffRem*SLOTTIME);
              }
              //}//MASTER
              usTpollCount[ucRRQueue] = usTpoll[ucRRQueue];
            }//L2CAP
          ucRRQueue = (ucRRQueue+1)%7;
          }
        }
        else if((role == SLAVE)&&(ulClk%4==2)&&(bMyTurn)) {
          if(PollNullPktQueue.size() > 0) {
            cout << "Dequeue Slave NULL packet " << (uShort)ucAMAddr <<endl;
            PktStruct& tmpPkt = PollNullPktQueue.front();
            uChar ucFreq = HopSelection(ulClkF, CHANNEL_HOPPING, MasterAddr);
            tmpPkt.pPacket->ucFreq = ucFreq;
            tmpPkt.pPacket->Hdr.ucSeqn = ucSeqn;
            //ucSeqn = !ucSeqn;
            Transmit(tmpPkt.pPacket, 0.0);
            PollNullPktQueue.pop_front();
          }//NULL_PKT
          else if(LmpPktSlaveQueue.size() > 0) {
            cout << "Dequeue Slave LMP packet " << (uShort)ucAMAddr <<endl;
            PktStruct& tmpPkt = LmpPktSlaveQueue.front();
            uChar ucFreq = HopSelection(ulClkF, CHANNEL_HOPPING, MasterAddr);
            tmpPkt.pPacket->ucFreq = ucFreq;
            tmpPkt.pPacket->Hdr.ucSeqn = ucSeqn;
            ucSeqn = !ucSeqn;
            Transmit(tmpPkt.pPacket, 0.0);
            LmpPktSlaveQueue.pop_front();
          }//LMP
          else if(L2capPktSlaveQueue.size() > 0) {
            cout << "Dequeue Slave L2CAP packet " << (uShort)ucAMAddr <<endl;
            PktStruct& tmpPkt = L2capPktSlaveQueue.front();
            ulPktCount++;
            dTotalDelay += Simulator::Now() - tmpPkt.TimeStamp;
            cout << "Total number of L2CAP packet " << ulPktCount<<endl;
            cout << "Total delay of L2CAP packet " << dTotalDelay<<endl;
            uChar ucFreq = HopSelection(ulClkF, CHANNEL_HOPPING, MasterAddr);
            tmpPkt.pPacket->ucFreq = ucFreq;
            tmpPkt.pPacket->Hdr.ucSeqn = ucSeqn;
            ucSeqn = !ucSeqn;
            Transmit(tmpPkt.pPacket, 0.0);
            L2capPktSlaveQueue.pop_front();
          }//L2CAP
          bMyTurn = false;
        }
      }//sniff
      }//hold
      //}
      CLOCKTICKTimeout = NULL;
      ScheduleTimer(BaseBandEvent::CLOCK_TICK, CLOCKTICKTimeout, 
          CLOCKTICK);
      break;
    case BaseBandEvent::NEW_CONNECTION_TO:
      cout << "New Connection Timeout "<<endl;
      if(role == MASTER) {
        State = PAGE; //repage or do something else?
        cout <<" reenter to  PAGE state" <<endl;
        InitInqPageParameter();
      }
      else {
        State = PAGE_SCAN;
        cout <<" reenter to PAGE_SCAN state" <<endl;
      }

      break;
    case BaseBandEvent::PAGE_TO:
      cout << "Page Timeout at "<<Simulator::Now() << endl;
      break;
    case BaseBandEvent::PAGE_RSP_TO:
      cout <<" Page Response Timeout "<<endl;
      if(role == MASTER) {
        State = PAGE;
        cout <<" reenter to PAGE state" <<endl;
        //Send a msg to LMP indicating error.
      }else {
        State = PAGE_SCAN; //for a scan period.
        cout <<" reenter to PAGE_SCAN state" <<endl;
      }
      break;
    case BaseBandEvent::INQUIRY_TO:
      cout << "!! End of inquiry at Time = " <<Simulator::Now()<< endl;
      InquiryTimeout = NULL;
      break;
    default:
      cout << "unknown events happening.." << endl;
      break;
  }
  
  delete pEvent;
  pEvent = NULL;

  return;
}

//receiving here
void
BaseBand::Handle(Event* e, Time_t t) {
  LinkEvent *pLinkEvent = (LinkEvent *)e;
  switch(pLinkEvent->event) {
    case LinkEvent::PACKET_RX:
      Trigger(pLinkEvent->p); //the associated Packet
      break;
    case LinkEvent::PACKET_TX:
      ;//
      break;
    default:
      cout <<"...??" <<endl;
      break;
  }

  delete pLinkEvent;//consumed.
  return;
}

//1st: determine the frequence of the packet is the same with
//    receiving frequence.
//Doing other 2 jobs in DetermineARQN()
//2nd: determine the AM_addr is the same with the dst am_addr
//3rd: call DataIndication();
void 
BaseBand::Trigger(Packet *pPacketIn) {
  //for sniff
  if((role == SLAVE) && pLMP->SniffEnabled() && (!pLMP->IsSniff())) {
    delete pPacketIn;
    return;
  }
  
  //updte ucRecvFreq according to its state.
  UpdateRecvFreq();
  if(State==STANDBY) {
    delete pPacketIn; 
    return;
  }

  CommonHdr *pCommonHdr = (CommonHdr *)pPacketIn->PopPDU();

#ifdef TEST
  if(State == CONNECTION) {
    cout<<"Slave ulClk = "<<ulClk<<endl;
    printf("Slave see Master Addr = 0x%x%x%x \n", MasterAddr.usAddr[2],
            MasterAddr.usAddr[1], MasterAddr.usAddr[0]);
  }
#endif

  //for park
  if((role == SLAVE) && (pLMP->isBeacon))
    pLMP->isBeacon = false;
  else if(pCommonHdr->ucFreq != ucRecvFreq) {
    delete pPacketIn;
    return;
  }
  
  //compare the access code in DataIndication
#ifdef TEST
  cout << " xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx Receiving Time = "<< hex 
      << Simulator::Now() << endl;
  cout << "ulClkN%4 = "<< ulClkN%4 <<endl;
#endif
    
  if(pCommonHdr->usLen <= sizeof(AccessCode68)) { 
    IDPacket *pPacket = (IDPacket *)pCommonHdr; 
    DataIndication((IDPacket *)pPacket);
  }
  else  {
    BaseBandPacket *pPacket = (BaseBandPacket *)pCommonHdr;
    DataIndication((BaseBandPacket *)pPacket);
  }
  delete pPacketIn;
  return;
}

void
BaseBand::UpdateRecvFreq() {
  HoppingSeqType hoppingType = CHANNEL_HOPPING;
  BdAddr addr = {{0x0000, 0x0000, 0x0000}};
  switch(State) {
    case PAGE:
      hoppingType = PAGE_HOPPING;
      addr = SlaveAddr; //the paged unit addr.
      break;
    case PAGE_SCAN:
      hoppingType = PAGE_SCAN_HOPPING;
      addr = pNode->GetBdAddr();
      break;
    case INQUIRY:
      hoppingType = INQUIRY_HOPPING;
      memcpy(&addr, &LAP_GIAC, sizeof(LAP_GIAC));
      break;
    case INQUIRY_SCAN:
      hoppingType = INQUIRY_SCAN_HOPPING;
      memcpy(&addr, &LAP_GIAC, sizeof(LAP_GIAC));
      break;
    case MASTER_RSP:
      hoppingType = MASTER_PAGE_RSP_HOPPING;
      addr = SlaveAddr; //the paged unit addr.
      break;
    case SLAVE_RSP:
      hoppingType = SLAVE_PAGE_RSP_HOPPING;
      addr = pNode->GetBdAddr();
      break;
    case INQUIRY_RSP:
      hoppingType = INQUIRY_RSP_HOPPING;
      memcpy(&addr, &LAP_GIAC, sizeof(LAP_GIAC));
      break;
    case CONNECTION:
      hoppingType = CHANNEL_HOPPING;
      if(role == SLAVE)
        addr = MasterAddr;
      else
        addr = pNode->GetBdAddr();
      break;
    case STANDBY:
      //do nothing.
      break;
    default:
      cout<<"unSupported state." <<endl;
      break;
  }
  
  ucRecvFreq = HopSelection(ulClkF , hoppingType, addr);
#ifdef TEST
  cout << role << " Updated recv freq = " << (int) ucRecvFreq << endl;
#endif
  return;
}

void
BaseBand::Transmit(IDPacket *pIDPacket, double dTime) {
  
  Packet *pPacket = new Packet;

  pPacket->PushPDU(pIDPacket);
  //send this real packet.
  pNode->UpdateNeighbor(); 

  if(State!=CONNECTION) {
    BlueNodeVec_t::iterator i = pNode->Bluenodes.begin();
    for(;i!=pNode->Bluenodes.end(); i++) {
      if((BlueNode *)(*i) == pNode)
        continue;
      if(!(this->isWithinRange((BlueNode *)(*i))) )
        continue;
      LinkEvent* pEventRx = nil;
      pEventRx = new LinkEvent(LinkEvent::PACKET_RX, pPacket->Copy());
      static_cast<NodeBlueImpl*>(((BlueNode *)(*i))->pImpl)->pBaseband->ScheduleRx(pEventRx, 
                         PropDelay + dTime);
    }
  }else { //using Neighbor list 
    BlueNeighborVec_t::iterator i = static_cast<NodeBlueImpl*>(pNode->pImpl)->blueNb.begin();
    for(;i!=static_cast<NodeBlueImpl*>(pNode->pImpl)->blueNb.end(); i++) {
      LinkEvent* pEventRx = nil;
      pEventRx = new LinkEvent(LinkEvent::PACKET_RX, pPacket->Copy());
      static_cast<NodeBlueImpl*>((i->pBlueNode)->pImpl)->pBaseband->ScheduleRx(pEventRx, 
                         PropDelay + dTime);
    }
  }

#ifdef TEST
  cout<<" ID Packet in transmission" <<endl;
#endif
  TxPower += IDPOWER;  //power measurement
  ActiveNoPower -= ACTIVENOPOWER;
  delete pPacket;
#ifdef HAVE_QT
  // Animate if requested
  if (Simulator::instance->BasebandTxStart())
    {
      QTWindow* qtw = Simulator::instance->GetQTWindow();
      qtw->BasebandTxStart(&(*pNode), (double)BT_MAXRANGE);
    }
#endif
  return;
}

void
BaseBand::Transmit(BaseBandPacket *pBaseBandPacket, double dTime) {

  //power measurement
  switch(pBaseBandPacket->Hdr.ucType) {
    case NULL_PKT:
    case POLL_PKT:
      TxPower += POLLNULLPOWER;
      break;
    case FHS_PKT:
    case DM1_PKT:
    case DH1_PKT:
      TxPower += TXRXPOWER;
      break;
    case DM3_PKT:
    case DH3_PKT:
      TxPower += 46.0;  //upgrade later
      break;
    case DM5_PKT:
    case DH5_PKT:
      TxPower += 47.6;
      break;
    default:
      TxPower += TXRXPOWER;
      break;
  }
  ActiveNoPower -= ACTIVENOPOWER;
  
  Packet *pPacket = new Packet;
  pPacket->PushPDU(pBaseBandPacket);

  pNode->UpdateNeighbor(); 

  if((State!=CONNECTION)||(role == SLAVE)) {
    BlueNodeVec_t::iterator i = pNode->Bluenodes.begin();
    for(;i!=pNode->Bluenodes.end(); i++) {
      if((BlueNode *)(*i) == pNode)
        continue;
      if(!(this->isWithinRange((BlueNode *)(*i))) )
        continue;
      LinkEvent* pEventRx = nil;
      pEventRx = new LinkEvent(LinkEvent::PACKET_RX, pPacket->Copy());
      static_cast<NodeBlueImpl*>(((BlueNode *)(*i))->pImpl)->pBaseband->ScheduleRx(pEventRx, 
                         PropDelay + dTime);
    }
  }else { //using Neighbor list 
    BlueNeighborVec_t::iterator i = static_cast<NodeBlueImpl*>(pNode->pImpl)->blueNb.begin();
    for(;i!=static_cast<NodeBlueImpl*>(pNode->pImpl)->blueNb.end(); i++) {
      LinkEvent* pEventRx = nil;
      pEventRx = new LinkEvent(LinkEvent::PACKET_RX, pPacket->Copy());
      static_cast<NodeBlueImpl*>((i->pBlueNode)->pImpl)->pBaseband->ScheduleRx(pEventRx, 
                         PropDelay + dTime);
    }
  }
#ifdef TEST
  cout<<" Packet in transmission" <<endl;
#endif  
  delete pPacket; //already make copy for each dst.
#ifdef HAVE_QT
  // Animate if requested
  if (Simulator::instance->BasebandTxStart())
    {
      QTWindow* qtw = Simulator::instance->GetQTWindow();
      qtw->BasebandTxStart(&(*pNode), (double)BT_MAXRANGE);
    }
#endif
  return;
  
}

void 
BaseBand::ScheduleRx(LinkEvent *e, Time_t t) {
  Scheduler::Schedule(e, t, this);
  return;
}

void 
BaseBand::ScheduleTx(LinkEvent *e, Time_t t) {
  Scheduler::Schedule(e, t, this);
  return;
}

void
BaseBand::Inquiry() {
  //Initialization
  InitInqPageParameter();
  BdAddr tmpAddr = {{0x0000, 0x0000, 0x0000}};
  memcpy(&tmpAddr, &LAP_GIAC, sizeof(LAP_GIAC));
  //determine the frequence of sending it.
  uChar ucFreq = HopSelection(ulClkF, INQUIRY_HOPPING,
      tmpAddr); //
  SendIDPacket(LAP_GIAC, ucFreq); //General inquiry
  State = INQUIRY;
  return;
}

void 
BaseBand::PageDevice(BdAddr slave) {
  printf("\n");
  printf(" ~~~ Paging Device 0x%x%x%x ~~~\n",slave.usAddr[2], slave.usAddr[1],slave.usAddr[0]);  
  assert(role == MASTER);
  State = PAGE;
  
  memcpy(&SlaveAddr, &slave, sizeof(BdAddr));
  InitInqPageParameter();
  //determine the frequence of sending it.
  uChar ucFreq = HopSelection(ulClkF, PAGE_HOPPING,
      slave);
  SendIDPacket( (((slave.usAddr[1] & 0x00FF) <<16) + (slave.usAddr[0])),
          ucFreq);
  assert(PageTimeout == NULL);
  ScheduleTimer(BaseBandEvent::PAGE_TO, PageTimeout,PAGETIMEOUT);
  return;
}  

void 
BaseBand::PageDevices() {
  if(bPiconetSetupDone)
    return; //already enough members.stop paging
   
  BlueNeighborVec_t::iterator i=static_cast<NodeBlueImpl*>(pNode->pImpl)->blueNb.begin();
  for(;i!=static_cast<NodeBlueImpl*>(pNode->pImpl)->blueNb.end(); i++) {
    BlueNeighbor *pNb= (BlueNeighbor *)(&(*i));
    if((pNb->pBlueNode) == (this->pNode))
      continue;
    //do something job of purging neighbor not in range
    if(!(this->isWithinRange(pNb->pBlueNode))) {
      static_cast<NodeBlueImpl*>(pNode->pImpl)->blueNb.erase(i);
      i--;
    }
    else {
      
      BdAddr tmpAddr = pNb->pBlueNode->GetBdAddr();
      if(pNb->IsPaged == false) {
        Initialize(PAGE , MASTER);
        ulClkE = pNb->ulClkE;
        PageDevice(tmpAddr);
        pNb->IsPaged = true;
        return;
      }
    }
  }
  //in case of less than 7 slaves, flag set here.
  bPiconetSetupDone = true;

  return;
}
//MEMO: in order to support multiple slave, the following should be done.
//      1. each clocktick, update all slave's special parameters: ulClkE, etc.
//      2. in Tpoll period, send a POLL packet in order to keep the connnection alive
//      3. save context before switch context.
//      4. reinitialize parameters before start a new page precedure 

//determine the flow flag to other side.
void
BaseBand::DetermineFlowFlag() {
  return;
  //ucFlow = ;
}

uChar 
BaseBand::DetermineARQN(BaseBandPacket *pBaseBandPkt) {
  Header Hdr = pBaseBandPkt->Hdr;
  uChar ucType = Hdr.ucType;
  uChar expected = 0x00;

  if(Hdr.ucHec != expected) {
    ucArqn = NAK;
    cout << "Reject payload at BaseBand::DetermineARQN because of HEC" <<endl;
    return 1; //reject payload
  }
  
  if((ucType != DM1_PKT) && (ucType != DM3_PKT) && (ucType != DM5_PKT)
    &&(ucType != DH1_PKT) && (ucType != DH3_PKT) &&(ucType != DH5_PKT)
    &&(ucType != DV_PKT)) {
#ifdef TEST
    cout << "Accept payload at BaseBand::DetermineARQN but not change anything" <<endl;
#endif
    return 0; //accept payload, but does not change anything.
  }
  if(State !=CONNECTION) {
    cout <<"Receive a data packet in non-connection state.Dicard. \n";
    return 1;//discard payload
  }
  
  if(Hdr.ucAMAddr != ucAMAddr) {
    cout <<"Discard packet in DetermineARQN."<<endl;
    return 1;
  }
  if(Hdr.ucSeqn == ucSeqnOld) {
    ucArqn = ACK;
#ifdef TEST    
    cout << "?? Hdr.ucSeqn = " << (int)(Hdr.ucSeqn) <<endl;
    cout << "?? ucSeqnOld = " <<(int)ucSeqnOld <<endl;
#endif    
    cout << "Reject payload at BaseBand::DetermineARQN because of the same ucSeqn" <<endl;
    return 1; //ignore payload
  }
  ucSeqnOld = Hdr.ucSeqn;
#ifdef TEST  
  cout <<"Now ucSeqnOld = " <<(int) ucSeqnOld << endl;
#endif  
  ucArqn = ACK;
  return 0; //accept payload
}

uChar
BaseBand::RetransmitFilter(uChar ucType) {
  if((ucType != DM1_PKT) && (ucType != DM3_PKT) && (ucType != DM5_PKT)
    &&(ucType != DH1_PKT) && (ucType != DH3_PKT) &&(ucType != DH5_PKT)
    &&(ucType != DV_PKT))
    return 1; //send new payload;
  if(!ucAcked) {
      cout <<"==Just send new payload== " <<endl;
      return 1;//send new payload
  }
  //ucSeqn = !ucSeqn;  //move to Dequeue!!!Check it.
  return 1; //send new payload
}
  

// Receive packets.
//this does not cover ID packet receipt.
void
BaseBand::DataIndication(BaseBandPacket *pBaseBandPkt) {
  uChar ucPktType = pBaseBandPkt->Hdr.ucType;

  ActiveNoPower -= ACTIVENOPOWER;  //power measurement
  
  //In case of NON-FHS packet,Check access code.
  if(ucPktType!=FHS_PKT){
    if(CheckAccessCode( (pBaseBandPkt->accessCode72).GetSynWord() , 
      (((MasterAddr.usAddr[1] & 0x00FF) <<16) + 
       (MasterAddr.usAddr[0]))) == false) {
#ifdef TEST
      cout<<"Discard incoming packet because of AccessCode"<<endl;
#endif  
      RxAcErrPower += RXACERRPOWER;  //power measurement
      return;
    }
    if((ucAMAddr!=0)&&(ucAMAddr!=pBaseBandPkt->Hdr.ucAMAddr)) {
#ifdef TEST        
      cout <<"Discard incoming packet." <<endl;
#endif      
      return;
    }
  }
   
  if((ucPktType == POLL_PKT)||(ucPktType==NULL_PKT)){
    if((ucAMAddr!=0)&&(ucAMAddr!=pBaseBandPkt->Hdr.ucAMAddr)) {
#ifdef TEST        
      cout <<"Discard incoming packet." <<endl;
#endif      
      //delete pBaseBandPkt;
      return;
    }
  }

  cout <<"Received a " << PacketType[ucPktType] 
      << " at "<<Simulator::Now()<< endl;

  //power measurement
  switch(ucPktType) {
    case NULL_PKT:
    case POLL_PKT:
      RxPower += POLLNULLPOWER;
      break;
    case FHS_PKT:
    case DM1_PKT:
    case DH1_PKT:
      RxPower += TXRXPOWER;
      break;
    case DM3_PKT:
    case DH3_PKT:
      RxPower += 46.0;  //upgrade later
      break;
    case DM5_PKT:
    case DH5_PKT:
      RxPower += 47.6;
      break;
    default:
      RxPower += TXRXPOWER;
      break;
  }

  if((ucPktType != POLL_PKT) &&(ucPktType!=NULL_PKT)) {
    if(DetermineARQN(pBaseBandPkt)) {
      cout << " Reject/Ignore incoming packet. " <<endl;
      //delete pBaseBandPkt;
      return;
    }

    //we only check this flag at CONNECTION state
    if(State == CONNECTION) {
      ucAcked = pBaseBandPkt->Hdr.ucArqn;//acknowledge the last transmission
      ucFlowRevert = pBaseBandPkt->Hdr.ucFlow;
    } 
  }

  FHSPayload *pFHSPayload = NULL;
  switch(ucPktType) {
    case FHS_PKT: 
      pFHSPayload = (FHSPayload *)pBaseBandPkt->pPayload;

      if(State == INQUIRY) { //master
        //estimate the slave clock.
        if(ulClkN%4 == 2)
          ulClkE = ((pFHSPayload->ulClk27_2)<<2) + 0x02;
        else
          ulClkE = ((pFHSPayload->ulClk27_2)<<2) + 0x03;
        //save slave device addr.
        BdAddr temp;
        temp.usAddr[0] = (pFHSPayload->ulLAP) & 0x00FFFF;
        temp.usAddr[1] = ((pFHSPayload->ucUAP) <<8) + ((pFHSPayload->ulLAP)>>16);
        temp.usAddr[2] = pFHSPayload->usNAP;
        BlueNode *pSNode = pNode->LookupNode(temp);
        if(pSNode != NULL) {
             if(!(pNode->LookupNeighbor(pSNode))) {
            BlueNeighbor nb;
            nb.pBlueNode = pSNode;
            nb.ulClkE = ulClkE;
            nb.IsPaged = false;
            static_cast<NodeBlueImpl*>(pNode->pImpl)->blueNb.push_back(nb);
            cout <<"Found a new neighbor \n" << endl;
          }
        } else
          cout <<"Huh? where are you from ?" <<endl;
      }
      else if(State == SLAVE_RSP) {//slave
        //getting response, cancel the page response timer.
        CancelTimer(PageRspTimeout, true);
        
        //store the master's clk and my AM_address.
        ulClk = (pFHSPayload->ulClk27_2)<<2;
        ucAMAddr = pFHSPayload->ucAMAddr;

#ifdef TEST    //should be zero here
        cout << "my AM_addr = " << (int) ucAMAddr << endl;;
#endif        
        MasterAddr.usAddr[0] = (pFHSPayload->ulLAP) & 0x00FFFF;
        MasterAddr.usAddr[1] = ((pFHSPayload->ucUAP) <<8) + ((pFHSPayload->ulLAP)>>16);
        MasterAddr.usAddr[2] = pFHSPayload->usNAP;

        BdAddr tmpAddr = pNode->GetBdAddr();
        ulClkN = ulClkN + 2;
        uChar ucFreq = HopSelection(this->ulClkF, SLAVE_PAGE_RSP_HOPPING,
          tmpAddr);
        ulClkN = ulClkN -2;
        SendIDPacket(((tmpAddr.usAddr[1] & 0x00FF) <<16)
          + (tmpAddr.usAddr[0]),ucFreq, SLOTTIME);
        
        State = CONNECTION;
        //schedule to wait for a POLL pkt.
        //if not received in framed time, return to PAGE_SCAN state
        NewConnectionTimeout = NULL;
        ScheduleTimer(BaseBandEvent::NEW_CONNECTION_TO, NewConnectionTimeout,
              NEWCONNECTIONTIMEOUT);
      }
      break;
    case POLL_PKT:
      //if(State == SLAVE_RSP) {
      if((State == CONNECTION) && (role == SLAVE)) {
        uChar ucSynWord[8];
        uChar ucHec = 0x00; //for debug only.

        if(!bStartPoll) {
          //cancel this timer now since I get what I expect.
          CancelTimer(NewConnectionTimeout, true);
          if(ucAMAddr!=0) //means it is not 1st POLL
            return;
          ucAMAddr = pBaseBandPkt->Hdr.ucAMAddr; //Wah! Got my am addr!
          cout <<"pBaseband = " << this;
          cout << " my AM_addr is " << (int)ucAMAddr<<endl;
        }
        
        //Send NULL pkt to response POLL
        ucArqn = ACK;
        //ucArqn = NAK;
        SynWordConstruct(MasterAddr, (uChar *)&ucSynWord);
        AccessCode72 *pAccessCode72 = new AccessCode72((uChar *)&ucSynWord);

        Header *pPktHdr = new Header;
        pPktHdr->ucAMAddr = ucAMAddr;
        pPktHdr->ucType = NULL_PKT;
        pPktHdr->ucFlow = ucFlow;
        pPktHdr->ucArqn = ucArqn;
        pPktHdr->ucSeqn = ucSeqn;
        pPktHdr->ucHec = ucHec;

        ulClk = ulClk + 2;
        uChar ucFreq = HopSelection(this->ulClkF, CHANNEL_HOPPING,
             MasterAddr);
        ulClk = ulClk - 2;

        if(!bStartPoll) {  //page stage
          Send(pAccessCode72, pPktHdr, NULL, SLOTTIME, ucFreq); //SLOTTIME =625 us
          cout <<"BaseBand layer is ready. " << endl;
        }
        else if(bStartPoll && (LmpPktSlaveQueue.size()==0) 
            && (L2capPktSlaveQueue.size()==0)){  //POLL after LMP establish
          SendNullPacket(pAccessCode72, pPktHdr, ucFreq);
        } //Otherwise I have LMP/L2CAP packet to send.
          //since bMyTurn is on, it will send pkt in its tx slot
      }
      
      break;
    case NULL_PKT:
      {
      if(bStartPoll)
        cout<<"Recv NULL after bStartPoll."<<endl;
      else {
      //Header hdr = pBaseBandPkt->Hdr;
      if(NewConnectionTimeout!=NULL)
        CancelTimer(NewConnectionTimeout, true);
      if((State == CONNECTION) && (role == MASTER)) {
        cout<<"Master got final response for page."<<endl;
        cout<<"Master : Baseband layer is ready!\n"<<endl;
        
        //Now save the context and page the next device.
        SaveContext(ucAMAddr);
        if(cActiveList[6]==7){
          bPiconetSetupDone = true;
          cout << "7 slaves active already."<<endl;
        }
        
        CheckPiconetSetup();
        //reset this so that master could page next devices.
        if(!bPiconetSetupDone)
          memset(&SlaveAddr,0, sizeof(SlaveAddr));
//#if 0        
        else {
        //create LMP object, establish connection above baseband.
          cout<<endl<<"*******************"<<endl;
          cout<<"~Create LMP entity or send ConnReq" <<endl;
          if(pLMP == NULL) {
            pLMP = new LMP(this);
            pLMP->SetPeerAddr(SlaveAddr);
            pLMP->HostConnectionReq();
          } else {//do nothing.
            pLMP->SetPeerAddr(SlaveAddr);
            pLMP->HostConnectionReq();
          }
        }//bPiconetSetupDone
        
//#endif        
      }//if
      }//else
      }
      break;
    case DM1_PKT:
    case DH1_PKT:
    case AUX1_PKT:
      assert(State==CONNECTION);
      {
        PayloadHdr *pHdr = (PayloadHdr *)(pBaseBandPkt->pPayload);
        uChar ucChannel = pHdr->ucLogicChannel;
#ifdef TEST        
        cout << "++ ucSeqn = " << (int)(ucSeqn) <<endl;
        cout << "++ ucSeqnOld = " <<(int)ucSeqnOld <<endl;
#endif
        if(ucChannel == LM_CH) {
          if(pLMP == NULL) {
            pLMP = new LMP(this);
            if(role==SLAVE)
              pLMP->SetPeerAddr(MasterAddr);
            else
              cout<<"Huh? who made me create LMP?" <<endl;
          }
          pLMP->DataIndication(pHdr->ucFlow, pHdr->ucLength,
                  ((uChar *)pHdr)+1); //modify interface in LMP
        }
        else if(ucChannel == L2CAP_CH || ucChannel == L2CAP_CON_CH)
          pL2CAP->DataIndication(ucChannel, pHdr->ucFlow,
                  pHdr->ucLength, 
                  //pHdr->pData);
                  ((uChar *)pHdr+1)); //1 byte header
        else
          cout << "@@undefined logic channel. " <<endl;
      }
      break;
    case DM3_PKT:
    case DH3_PKT:
    case DM5_PKT:
    case DH5_PKT:
      assert(State==CONNECTION);
      {
        PayloadHdrMultiSlot *pHdr = (PayloadHdrMultiSlot *)
            (pBaseBandPkt->pPayload);
        uChar ucChannel = pHdr->ucLogicChannel;
        uShort usLen = pHdr->ucLengthL + ((pHdr->ucLengthH)<<5);
        if(ucChannel == LM_CH) {
          cout << "Huh? why use multiple slot for LMP? " <<endl;
#if 0
          if(pLMP == NULL) {
            pLMP = new LMP(this);
          }
          pLMP->DataIndication(pHdr->ucFlow, usLen,
                  ((uChar *)pHdr)+1); //modify interface in LMP
#endif
        }
        else if(ucChannel == L2CAP_CH || ucChannel == L2CAP_CON_CH)
          pL2CAP->DataIndication(ucChannel, pHdr->ucFlow,
                  usLen, 
                  //pHdr->pData);
                  ((uChar *)pHdr+2)); //2 bytes header
                  //modify interface in L2CAP
        else
          cout << "@@undefined logic channel. \n" <<endl;
      }
      break;

    default:
      cout <<"@@Unsupported packet type. " <<endl;
      break;
  }

  //for sniff
  if((role == SLAVE) && pLMP->SniffEnabled() && (State == CONNECTION) && (ucPktType != FHS_PKT)){
    uLong ulSniffAttempRem = pLMP->GetSniffAttemp() - (ulClk - pLMP->ulSniffClk)/2;
    uLong ulSniffRem = (ulSniffAttempRem > pLMP->GetSniffTo()) ? 
        ulSniffAttempRem: pLMP->GetSniffTo();
    pLMP->CancelTimer(pLMP->SniffAttempTimeout, true);
    pLMP->ScheduleTimer(LMPEvent::SNIFFATTEMP_TIMEOUT, 
        pLMP->SniffAttempTimeout, ulSniffRem*SLOTTIME);
  }

  if((role == SLAVE)&&(State == CONNECTION)&&(ucPktType != FHS_PKT))
    bMyTurn = true;
    
  //delete pBaseBandPkt;
  return;
}

bool BaseBand::CheckAccessCode(uChar *DstCode, uLong ulLAP){
  uChar ucSynWord[8];
  SynWordConstruct(ulLAP, (uChar *)&ucSynWord);
  if(memcmp(DstCode, &ucSynWord, sizeof(ucSynWord)))
    return false;
  else
    return true;
}  

//Receive ID packet 
void
BaseBand::DataIndication(IDPacket *pIDPacket) {

  ActiveNoPower -= ACTIVENOPOWER;  //power measurement
  
  uChar ucSynWord[8];
  
  BdAddr tmpAddr = pNode->GetBdAddr();

  DetermineFlowFlag();
  
  if(State==INQUIRY_SCAN) { //slave
    //check the access code.
    if(CheckAccessCode( (pIDPacket->accessCode68).GetSynWord() , 
                LAP_GIAC) == false) {
      //delete pIDPacket;
      RxAcErrPower += RXACERRPOWER;  //power measurement
      return;
    }
      
    cout <<"@@@@ Received a ID packet @@@@" ;
    cout <<" in INQUIRY_SCAN state at "<<Simulator::Now() << endl;
    
    RxPower += IDPOWER;  //power measurement
      
    //Inquiry backoff to avoid contention.
    if(PreviousState!=STANDBY) {
        Uniform randnum(0, 0x7FF); //0--2047 timeticks = 0--1023 slot
      InquiryBackoff = (short)randnum.Value();
      if(InquiryBackoff!=0) {
        //schedule a timer with sRand slot or else;
        State = STANDBY; //change state to STANDBY.
        cout <<"Inquiry Backoff = " << (InquiryBackoff * CLOCKTICK) <<endl;
        cout <<"Enter STANDBY state." <<endl;
        //delete pIDPacket;
        return;
      }//else respond
    }
    //
    //
    
    State = INQUIRY_RSP;
    //schedule a FHS packet to send back.
    SynWordConstruct(LAP_GIAC, (uChar *)&ucSynWord);

    AccessCode72 *pAccessCode72 = new AccessCode72((uChar *)&ucSynWord);

    uChar ucHec = 0x00; //for debug only.
    //Packet header
    Header *pPktHdr = new Header;
    pPktHdr->ucAMAddr = 0x00;
    pPktHdr->ucType = FHS_PKT;
    pPktHdr->ucFlow = ucFlow;
    pPktHdr->ucArqn = ucArqn; //not meaningful
    pPktHdr->ucSeqn = ucSeqn;
    pPktHdr->ucHec = ucHec;
    
    uChar *pTemp =  new uChar[sizeof(FHSPayload)]; 
    FHSPayload *pPayload  = (FHSPayload *)pTemp;
    //FHSPayload *pPayload  = new FHSPayload; //pTemp;
      //Note:this does not include FEC, etc. stuff.
    pPayload->ulParityBits = 0x00000000;
    pPayload->ucCon2Bits = 0;
    
    pPayload->ulLAP = ((tmpAddr.usAddr[1] & 0x00FF) <<16)
        + (tmpAddr.usAddr[0]);
    pPayload->ucUndef = 0;
    pPayload->ucUAP = (tmpAddr.usAddr[1] & 0xFF00) >> 8;
    pPayload->usNAP = tmpAddr.usAddr[2];
    pPayload->ucAMAddr = 0x00;
    pPayload->ulClk27_2 = (ulClkN & (0x0FFFFFFF))>> 2;
    pPayload->ucPageScanMode = 0; //mandatory scan mode
  
    memset(&tmpAddr, 0, sizeof(tmpAddr)); //reuse tmpAddr
    memcpy(&tmpAddr, &LAP_GIAC, sizeof(LAP_GIAC));

    ulClkN = ulClkN + 2;
    uChar ucFreq = HopSelection(this->ulClkF, 
            INQUIRY_RSP_HOPPING, tmpAddr);
    ulClkN = ulClkN - 2;
    printf("++++FHS packet sending at %d \n", ucFreq);

    //respond after 625 us.
    //Send(pAccessCode72, pPktHdr,(uChar *)pPayload /*pTemp*/, 
    Send(pAccessCode72, pPktHdr,pTemp, 
            SLOTTIME, ucFreq); //SLOTTIME =625 us
    
    //increase usN after each FHS pkt has been transmitted in response
    //to the inquiry.
    usN++;
    
    //Prepare for the paging scan
    State = PAGE_SCAN;
  }
  else if(State==PAGE_SCAN) { //slave

    //check the access code.
    if(CheckAccessCode( (pIDPacket->accessCode68).GetSynWord(), 
        (((tmpAddr.usAddr[1] & 0x00FF) <<16) + (tmpAddr.usAddr[0])))
            == false) {
#ifdef TEST
      cout << "=== Access Code check failure.===" << endl;
#endif
      RxAcErrPower += RXACERRPOWER;  //power measurement
      //delete pIDPacket;
      return;
    }
    State = SLAVE_RSP;
    //Cancel page response timer after getting response.
    if(PageRspTimeout!= NULL)
      CancelTimer(PageRspTimeout, true);
    
    cout <<"@@@@ Received a ID packet @@@@" ;
    cout <<" in PAGE_SCAN state at "<<Simulator::Now() <<endl;  
    
    RxPower += IDPOWER;  //power measurement
    
    //Frozen the clock value of my own.
    this->ulClkF = ulClkN;
#ifdef TEST    
    cout<<"ulClkN = "<<ulClkN<<endl;
    cout<<"ulClkF = "<<ulClkF<<endl;
    printf("Slave Addr = 0x%x%x%x\n",tmpAddr.usAddr[2],
             tmpAddr.usAddr[1],tmpAddr.usAddr[0]/*pNode->GetBdAddr()*/);
#endif    
    
    //set usN to zero in the slot where slave acknowledge the page
    //or master send the response
    usN = 0;
    
    //Send slave ID back.  
    ulClkN = ulClkN + 2;
    uChar ucFreq = HopSelection(this->ulClkF, SLAVE_PAGE_RSP_HOPPING,
        pNode->GetBdAddr());
    ulClkN = ulClkN - 2;

    //always respond after 625 us.
    SendIDPacket(((tmpAddr.usAddr[1] & 0x00FF) <<16)
            + (tmpAddr.usAddr[0]), ucFreq, SLOTTIME);

    cout <<" Reply a ID packet at SLAVE_PAGE_RESPONSE state" <<endl;
    //schedule timer waiting for FHS page response.
    PageRspTimeout = NULL;
    ScheduleTimer(BaseBandEvent::PAGE_RSP_TO, PageRspTimeout,
            PAGERSPTIMEOUT);
  } 
  else if(State == PAGE) { //master
    if(CheckAccessCode( (pIDPacket->accessCode68).GetSynWord() , 
      (((SlaveAddr.usAddr[1] & 0x00FF) <<16) + 
       (SlaveAddr.usAddr[0]))) == false) {
      //delete pIDPacket;
      RxAcErrPower += RXACERRPOWER;  //power measurement
      return;
    }
    //Frozen the clock value of my own.
    //this->ulClkF = ulClkN;
    this->ulClkF = ulClkE;


    State = MASTER_RSP;

    //Cancel existing Page response timer if there exist one.
    if(PageRspTimeout!=NULL)
      CancelTimer(PageRspTimeout, true);
    if(PageTimeout!=NULL)
      CancelTimer(PageTimeout, true);

    cout <<"@@@@ Received a ID packet @@@@" ;
    cout <<" in PAGE state at "<<Simulator::Now() <<endl;
    
    RxPower += IDPOWER;  //power measurement
    
    //usN++; //the first increment be done before sending the FHS 
           //to the paged unit.
#ifdef TEST
    cout<< "usN = "<<usN<<endl;
#endif
    //schedule a FHS packet to send back.
    SynWordConstruct(SlaveAddr, (uChar *)&ucSynWord);

    AccessCode72 *pAccessCode72 = new AccessCode72((uChar *)&ucSynWord);
    uChar ucHec = 0x00; //for debug only
    //Packet header
    Header *pPktHdr = new Header;
    pPktHdr->ucAMAddr = 0x00;
    pPktHdr->ucType = FHS_PKT;
    pPktHdr->ucFlow = ucFlow;
    pPktHdr->ucArqn = ucArqn;
    pPktHdr->ucSeqn = ucSeqn;
    pPktHdr->ucHec = ucHec;
    
    uChar *pTemp =  new uChar[sizeof(FHSPayload)]; 
    FHSPayload *pPayload  = (FHSPayload *)pTemp;
    //FHSPayload *pPayload  = new FHSPayload;//pTemp;
      //Note:this does not include FEC, etc. stuff.
    pPayload->ulParityBits = 0x00000000;
    pPayload->ucCon2Bits = 0;
    pPayload->ulLAP = ((tmpAddr.usAddr[1] & 0x00FF) <<16)
        + (tmpAddr.usAddr[0]);
    pPayload->ucUndef = 0;
    pPayload->ucUAP = ((tmpAddr.usAddr[1] & 0xFF00) >> 8);
    pPayload->usNAP = tmpAddr.usAddr[2];
    pPayload->ucAMAddr = 0x00;
    pPayload->ulClk27_2 = ((ulClkN + 2)>> 2);
    pPayload->ucPageScanMode = 0; //mandatory scan mode
  
#ifdef TEST
    cout <<" ulClkN%4 = " << ulClkN%4 << endl;
#endif
    if(ulClkN%4==3) {//received at the 2nd half slot
      ulClkE = ulClkE + 1;
      uChar ucFreq = HopSelection(this->ulClkF,MASTER_PAGE_RSP_HOPPING,//PAGE_HOPPING,
          SlaveAddr);
      ulClkE = ulClkE - 1;
      
      //Send(pAccessCode72, pPktHdr, (uChar *)pPayload/*pTemp*/, 
      Send(pAccessCode72, pPktHdr, pTemp, 
              CLOCKTICK, ucFreq); 
    }
    else if(ulClkN%4==2) {//received at 1st half slot
      ulClkE = ulClkE + 2;
      uChar ucFreq = HopSelection(this->ulClkF,MASTER_PAGE_RSP_HOPPING,//PAGE_HOPPING,
          SlaveAddr);
      ulClkE = ulClkE - 2;

      //Send(pAccessCode72, pPktHdr, (uChar *)pPayload/*pTemp*/, 
      Send(pAccessCode72, pPktHdr, pTemp, 
              SLOTTIME, ucFreq); 
    }
    else {
      cout <<" Timing schedule is WRONG?" <<endl;
      delete []pTemp;
      //delete pIDPacket;
      return;
    }
    usN--;
#ifdef TEST    
    cout<<"ulClkF = "<<ulClkF<<endl;
    cout<<"ulClkE = "<<ulClkE<<endl;
    cout<<"ulClkN = "<<ulClkN<<endl;
    printf("Slave Addr in Master = 0x%x%x%x\n", SlaveAddr.usAddr[2],
            SlaveAddr.usAddr[1], SlaveAddr.usAddr[0]);
    cout<<"usN = "<<usN<<endl;
#endif
    //SPECIAL NOTE: we did not implement retransmit FHS here.
    
    //schedule timer waiting for acknowledgment page response.
    PageRspTimeout = NULL;
    ScheduleTimer(BaseBandEvent::PAGE_RSP_TO, PageRspTimeout,
            PAGERSPTIMEOUT);
  }
  else if(State == MASTER_RSP) { //master
    //check the access code.
    if(CheckAccessCode( (pIDPacket->accessCode68).GetSynWord(), 
        (((SlaveAddr.usAddr[1] & 0x00FF) <<16) + (SlaveAddr.usAddr[0])))
            == false) {
      //delete pIDPacket;
      RxAcErrPower += RXACERRPOWER;  //power measurement
      cout << "discard ID packet." <<endl;
      return;
    } 
    cout <<"@@@@ Received a ID packet @@@@" ;
    cout <<" in MASTER_RSP state at "<<Simulator::Now()<<endl;

    RxPower += IDPOWER;  //power measurement
    
    //Cancel existing Page response timer
    if(PageRspTimeout!=NULL)
      CancelTimer(PageRspTimeout, true);

    State = CONNECTION;
    ucArqn = ACK; 
    //ucArqn = NAK;
    //Send the 1st packet , POLL

    SynWordConstruct(tmpAddr, (uChar *)&ucSynWord);
    //SynWordConstruct(pNode->GetBdAddr(), (uChar *)&ucSynWord);

    AccessCode72 *pAccessCode72 = new AccessCode72((uChar *)&ucSynWord);
    char cNewAmAddr = AllocAmAddr();
    if(cNewAmAddr == 0) {
      cout << " already 7 slaves existing. " <<endl;
      return ;
    }
    //save this as current context so that we could know who I am
    //talking to
    ucAMAddr = cNewAmAddr; 
    ActiveAddr[cNewAmAddr-1] = SlaveAddr;

    uChar ucHec = 0x00;
    /*
    Header *pPktHdr = new Header(cNewAmAddr,
          POLL_PKT, //packet type.
          ucFlow, //
          ucArqn,
          ucSeqn
          ucHec); */
    Header *pPktHdr = new Header;
    pPktHdr->ucAMAddr = cNewAmAddr;
    pPktHdr->ucType = POLL_PKT;
    pPktHdr->ucFlow = ucFlow;
    pPktHdr->ucArqn = ucArqn;
    pPktHdr->ucSeqn = ucSeqn;
    pPktHdr->ucHec = ucHec;
  
    ulClk = ulClkN;
    ulClk = ulClk + 2;
#ifdef TEST
    cout<<"Master ulClk = "<<ulClk<<endl;
    printf("Master Addr = 0x%x%x%x \n", tmpAddr.usAddr[2],
             tmpAddr.usAddr[1],tmpAddr.usAddr[0]);
#endif
    uChar ucFreq = HopSelection(this->ulClkF, CHANNEL_HOPPING,
       pNode->GetBdAddr());
    ulClk = ulClk - 2;

    Send(pAccessCode72, pPktHdr, NULL, SLOTTIME, ucFreq); //SLOTTIME =625 us
    
    PageRspTimeout = NULL;
    //schedule timer waiting for acknowledgment POLL .
    NewConnectionTimeout = NULL;
    ScheduleTimer(BaseBandEvent::NEW_CONNECTION_TO, NewConnectionTimeout,
            NEWCONNECTIONTIMEOUT);
  }
  else {
    //check the access code.
    if((role == SLAVE) && CheckAccessCode( (pIDPacket->accessCode68).GetSynWord(), 
        (((tmpAddr.usAddr[1] & 0x00FF) <<16) + (tmpAddr.usAddr[0])))
            == false) {
#ifdef TEST
      cout << "=== Access Code check failure.===" << endl;
#endif
      //delete pIDPacket;
      RxAcErrPower += RXACERRPOWER;  //power measurement
      return;
    }
    cout <<" Receive ID packet in inappropriate state: "<<State << endl;;
    //delete pIDPacket;
    RxPower += IDPOWER;  //power measurement
    return;
  }
    
  //delete pIDPacket;
  //fixed by xzhang on 05/13/04
  return;  
}

void
BaseBand::SendPollPacket(uChar ucAMAddr) {
    uChar ucSynWord[8];
    SynWordConstruct(MasterAddr, (uChar *)&ucSynWord);
    AccessCode72 *pAccessCode72 = new AccessCode72((uChar *)&ucSynWord);
    uChar ucHec = 0x00;
    Header *pPktHdr = new Header;
    pPktHdr->ucAMAddr = ucAMAddr;
    pPktHdr->ucType = POLL_PKT;
    pPktHdr->ucFlow = ucFlow;
    pPktHdr->ucArqn = ucArqn;
    pPktHdr->ucSeqn = ucSeqn;
    pPktHdr->ucHec = ucHec;
  
    uChar ucFreq = 0;  //temp ucFreq compute at dequeue
    
    BaseBandPacket *pPkt = NULL;
    pPkt = new BaseBandPacket(ucFreq,
          sizeof(AccessCode72) + sizeof(Header),
          *pAccessCode72, *pPktHdr, NULL);
    
    PktStruct tmpPkt;
    tmpPkt.TimeStamp = Simulator::Now();
    tmpPkt.pPacket = pPkt;
    PollNullPktQueue.push_back(tmpPkt);
    
    delete pAccessCode72;
    delete pPktHdr;
    
    return;
}

void
BaseBand::SendNullPacket(AccessCode72 *pAccessCode72, 
      Header *pPktHdr, uChar ucFreq) {
    BaseBandPacket *pPkt = NULL;
    pPkt = new BaseBandPacket(ucFreq,
          sizeof(AccessCode72) + sizeof(Header),
          *pAccessCode72, *pPktHdr, NULL);
    
    PktStruct tmpPkt;
    tmpPkt.TimeStamp = Simulator::Now();
    tmpPkt.pPacket = pPkt;
    PollNullPktQueue.push_back(tmpPkt);
    
    delete pAccessCode72;
    delete pPktHdr;

    return;
}

//
//Note: 1.channel control part
//      2.data structure's maitainance.
//      3.timing part. 
//      4.train type issue, @@DONE@@
//
void
BaseBand::UpdateInqPageParameter() {
  uShort usNumber;
  usNumber = (State == INQUIRY)? Ninq : Npage;
  if(ucIdNo == TRAINSIZE) {
    ucIdNo = 0;
    usTrainSent++;
    if (usTrainSent == usNumber){
      usTrainSent = 0;
      ucTrainType = (ucTrainType == 'A') ? 'B' : 'A';  
    }
  }
  ucIdNo++;
  return;
}

//do the initialization work before inquiry and page.
void
BaseBand::InitInqPageParameter(){
  ucIdNo = 0;
  ucTrainType = 'A';
  usTrainSent = 0;

  //for Master, the N starts from 1. P136
  usN = 1;
  //if(State == PAGE)
  //memset(cActiveList, 0, sizeof(cActiveList));
  return;
}

char 
BaseBand::AllocAmAddr() {
  unsigned char cIndex = 0;
  for (cIndex=0; cIndex<7; cIndex++) {
    if(cActiveList[cIndex]==0) {
      cActiveList[cIndex] = cIndex +1; //1; //now the am addr is used.
      return (cIndex + 1); //1--7
    }
  }
  return 0; //invalid Active member address.
}

//after inquiry/page, the parameters such as usN, ucIdNo are of no use.
void 
BaseBand::SaveContext(uChar ucActiveAddr) {
  //search the Vector list first. if not found, then create.
  //otherwise, update it only
  ContextVec_t::iterator i=SlaveContexts.begin();
  for(; i!=SlaveContexts.end(); i++){
    if(i->ucAMAddr == ucActiveAddr) {//update it
      i->ucAMAddr = ucAMAddr;
      assert(i->SlaveAddr == SlaveAddr); //if not the same, unexpected!
      i->ucFlow = ucFlow;
      i->ucArqn = ucArqn;
      i->ucSeqn = ucSeqn;
      i->ucSeqnOld = ucSeqnOld;
      i->ucAcked = ucAcked;
      i->ucFlowRevert = ucFlowRevert;
      return;
    }
  }
  Context curContext;
  curContext.ucAMAddr = ucAMAddr;
  curContext.SlaveAddr = SlaveAddr;
  curContext.ucFlow = ucFlow;
  curContext.ucArqn = ucArqn;
  curContext.ucSeqn = ucSeqn;
  curContext.ucSeqnOld = ucSeqnOld;
  curContext.ucAcked = ucAcked;
  curContext.ucFlowRevert = ucFlowRevert;
  
  SlaveContexts.push_back(curContext);

  cout<<"Up to now, there are "<<SlaveContexts.size() <<" contexts" <<endl;
  return;
}

bool
BaseBand::SwitchContext(uChar ucAddrToSave, uChar ucAddrToActive){
  SaveContext(ucAddrToSave);
  //bring the other into be foreground.
  ContextVec_t::iterator i=SlaveContexts.begin();
  for(; i!=SlaveContexts.end(); i++){
    if(i->ucAMAddr == ucAddrToActive) {//pour all parameters to current
      ucAMAddr = i->ucAMAddr ;
      SlaveAddr = i->SlaveAddr;
      ucFlow = i->ucFlow;
      ucArqn = i->ucArqn;
      ucSeqn = i->ucSeqn;
      ucSeqnOld = i->ucSeqnOld;
      ucAcked = i->ucAcked;
      ucFlowRevert = i->ucFlowRevert;
      return true;
    }
  }

  cout <<"Error, no such active member."<<endl;
  return false;
}

bool
BaseBand::RemoveContext(uChar ucAddr) {
  assert(role==MASTER);
  ContextVec_t::iterator i=SlaveContexts.begin();
  for(; i!=SlaveContexts.end(); i++){
    if(i->ucAMAddr == ucAddr) {//this one to remove
      assert(i->SlaveAddr == ActiveAddr[ucAddr]); //the same
      SlaveContexts.erase(i);
      i--;
      return true;
    }//if
  }//for
  return false; //not found
}

//this method is used to update ClockE of potential slaves
void
BaseBand::UpdateNeighborClockE() {
  assert(role==MASTER);
  BlueNeighborVec_t::iterator i=static_cast<NodeBlueImpl*>(pNode->pImpl)->blueNb.begin();
  
  for(;i!=static_cast<NodeBlueImpl*>(pNode->pImpl)->blueNb.end(); i++) {
    BlueNeighbor *pNb= (BlueNeighbor *)(&(*i));
    if((pNb->pBlueNode) == (this->pNode))
      continue;
    //do something job of purging neighbor not in range
    if(!(this->isWithinRange(pNb->pBlueNode))) {
      static_cast<NodeBlueImpl*>(pNode->pImpl)->blueNb.erase(i);
      i--;
    }
    else 
      pNb->ulClkE++;
  }
  return;
}  

//check the number of active member 
//and the number of neighbor
void BaseBand::CheckPiconetSetup() {
  uChar ucCount = 0;
  //ucCount = 0;
  for(uChar i=0; i<sizeof(cActiveList);i++) {
    if(cActiveList[i]!=0)
      ucCount++;
  }
  if(ucCount==(static_cast<NodeBlueImpl*>(pNode->pImpl))->blueNb.size()) 
    bPiconetSetupDone = true;

  return;
}

void BaseBand::LMPReadyIndication(void) {
  //need to know which SlaveAddr is used right now
  //determine the next one
  uChar ucIndex;
  uChar ucAMAddrToActive = 0xFF;
  // 6  5  4  3  2  1  0
  //[0][0][5][4][3][2][1]     aActiveList
  // 1  1  1  1  1  1  1      cLMPConnected
  //== once a LMP connected, zero that byte.
  cLMPConnected[ucAMAddr-1] = 0;
  for(ucIndex= 0; ucIndex<sizeof(cActiveList); ucIndex++){
    if(cActiveList[ucIndex]&& cLMPConnected[ucIndex]) {
      ucAMAddrToActive = cActiveList[ucIndex];
      break; //get the index of not connected.
    }
       
  }

  if(ucAMAddrToActive!=0xFF){
    //if not completed, save context. do next
    cout<<"Active member address to be active = "
      <<(int)ucAMAddrToActive<<endl;
    usTpoll[ucAMAddr-1] = pLMP->GetTpoll();
    usTpollCount[ucAMAddr-1] = usTpollCount[ucAMAddr-1];
    
    SwitchContext(ucAMAddr,ucAMAddrToActive);
    pLMP->SetPeerAddr(SlaveAddr);
    pLMP->HostConnectionReq();
  }else {
    cout<<"LMP layer for every slave is ready"<<endl;
    usTpoll[ucAMAddr-1] = pLMP->GetTpoll();
    usTpollCount[ucAMAddr-1] = usTpollCount[ucAMAddr-1];
    bStartPoll = true;
  }
  return;
}

//Hop Selection Module 
uChar
BaseBand::HopSelection(uLong ulClkFrozen, 
      HoppingSeqType hopSeqType, BdAddr addr) {

  //refine addr;
  addr.usAddr[2] = 0x0000;
  addr.usAddr[1] = addr.usAddr[1] & 0x0FFF;
  //declare those inputs  
  uChar ucF = 0; 
  uChar ucE = GetBits(addr.usAddr[0], 13, 1, 6)  + //A13
    GetBits(addr.usAddr[0],11, 1,5) + //A11
    GetBits(addr.usAddr[0],9, 1, 4)  + //A9
    GetBits(addr.usAddr[0], 7, 1, 3)  + //A7
    GetBits(addr.usAddr[0], 5, 1, 2)  + //A5
    GetBits(addr.usAddr[0], 3, 1,1 ) + //A3
    GetBits(addr.usAddr[0], 1, 1); //A1
  uShort usD = GetBits(addr.usAddr[0], 15, 6) + //A15--10
    GetBits(addr.usAddr[1], 2, 3, 6);//A18--16
  uChar ucC = GetBits(addr.usAddr[0], 8, 1, 4) + //A8
    GetBits(addr.usAddr[0], 6, 1, 3) + //A6
    GetBits(addr.usAddr[0], 4, 1, 2) + //A4
    GetBits(addr.usAddr[0], 2, 1, 1) + //A2
    GetBits(addr.usAddr[0], 0, 1);//A0
  uChar ucB = GetBits(addr.usAddr[1], 6, 4); //A22--19
  //uChar ucA = GetBits(addr.usAddr[1], 27, 5); //A27--23
  uChar ucA = GetBits(addr.usAddr[1], 11, 5); //A27--23
  uChar ucY2 = 0;
  bool bY1 = 0;
  uChar ucX = 0;
  
  ucF = ucF & 0x7F;
  ucE = ucE & 0x7F;
  usD = usD & 0x01FF;
  ucC = ucC & 0x1F;
  ucB = ucB & 0x0F;
  ucA = ucA & 0x1F;
  ucY2 = ucY2 & 0x3F;
  ucX = ucX & 0x1F;
  
  //for convenience of bit operations.
  ulClkE = ulClkE & 0x0FFFFFFF;
  ulClkN = ulClkN & 0x0FFFFFFF;
  ulClk = ulClk & 0x0FFFFFFF;
  ulClkFrozen = ulClkFrozen & 0x0FFFFFFF;

  //uShort *pClkE = (uShort *) &ulClkE ;
  //uShort *pClkN = (uShort *) &ulClkN;
  uShort usClkE_h = (ulClkE&0xFFFF0000)>>16;
  uShort usClkE_l = ulClkE&0x0000FFFF;
  uShort usClkN_h = (ulClkN&0xFFFF0000)>>16;
  uShort usClkN_l = ulClkN&0x0000FFFF;

  uChar ucKoffset = (ucTrainType == 'A') ? 24 : 8; //
  ucKoffsetStar = ucKoffset;

  switch(hopSeqType) {
      case PAGE_HOPPING:
      {
        //determine X
        uChar ucClkE_16_12 = GetBits(usClkE_l, 15 , 4) + 
            GetBits(usClkE_h, 0, 1, 4);
        uChar ucClkE_4_2_0 = GetBits(usClkE_l, 4, 3, 1) +
            GetBits(usClkE_l, 0, 1);
        uChar ucXp = ucClkE_16_12 + ucKoffset + 
            (ucClkE_4_2_0 - ucClkE_16_12) % 16;
        ucX = ucXp % 32;

        //determine Y1
        bY1 = GetBits((uShort)ulClkE, 1, 1);
        //determine Y2
        ucY2 = (GetBits(usClkE_l, 1 , 1) == 0)? 0 : 0x20;
      }
      break;
      case INQUIRY_HOPPING:
      {
        //determine X
        uChar ucClkN_16_12 = GetBits(usClkN_l, 15 , 4) + 
            GetBits(usClkN_h, 0, 1, 4);
        uChar ucClkN_4_2_0 = GetBits(usClkN_l, 4, 3, 1) +
            GetBits(usClkN_l, 0, 1);
        uChar ucXi = ucClkN_16_12 + ucKoffset + 
            (ucClkN_4_2_0 - ucClkN_16_12) % 16;
        ucX = ucXi % 32;
        
        //determine Y1
        bY1 = GetBits((uShort)ulClkN, 1, 1);
        //determine Y2
        ucY2 = (GetBits(usClkN_l, 1 , 1) == 0)? 0 : 0x20;
      }
      break;
      
    case PAGE_SCAN_HOPPING:
      //determine X
      ucX = GetBits(usClkN_l, 15 , 4) + 
        GetBits(usClkN_h, 0, 1, 4);
      break;
    case INQUIRY_SCAN_HOPPING:
      {
        uChar ucClkN_16_12 = GetBits(usClkN_l, 15 , 4) + //15--12
            GetBits(usClkN_h, 0, 1, 4); //16
        //this is actually X already.
        //determine the "N" somewhere
        ucX = (ucClkN_16_12 + usN) % 32;
      }
      break;
    case SLAVE_PAGE_RSP_HOPPING:
      {
        //uShort *pClkNStar = (uShort *) &ulClkFrozen;
        uShort usClkNStar_h = (ulClkFrozen&0xFFFF0000)>>16;
        uShort usClkNStar_l = ulClkFrozen&0x0000FFFF;

        uChar ucClkNStar_16_12 = GetBits(usClkNStar_l, 15 , 4) + 
            GetBits(usClkNStar_h, 0, 1, 4);
        // determine N somewhere else
        ucX = (ucClkNStar_16_12 + usN) % 32;
        
        //determine Y1
        bY1 = GetBits((uShort)ulClkN, 1, 1);
        //determine Y2
        ucY2 = (GetBits((uShort)ulClkN, 1 , 1) == 0)? 0 : 0x20;
        //Note: refer to Spec 1.2, you will find it should be ulClkN,
        //      not ulClkE
      }
      break;
    case MASTER_PAGE_RSP_HOPPING:
      {
        //uShort *pClkEStar = (uShort *) &ulClkFrozen;
        uShort usClkEStar_h = (ulClkFrozen&0xFFFF0000)>>16;
        uShort usClkEStar_l = ulClkFrozen&0x0000FFFF;

        uChar ucClkEStar_16_12 = GetBits(usClkEStar_l, 15 , 4) + 
            GetBits(usClkEStar_h, 0, 1, 4);

        uChar ucClkEStar_4_2_0 = GetBits(usClkEStar_l, 4, 3, 1) +
            GetBits(usClkEStar_l, 0, 1);
        uChar ucXp = ucClkEStar_16_12 + ucKoffsetStar + 
            (ucClkEStar_4_2_0 - ucClkEStar_16_12)%16 + usN;
        ucX = ucXp % 32;
        //determine Y1
        bY1 = GetBits((uShort)ulClkE, 1, 1);
        //determine Y2
        ucY2 = (GetBits((uShort)ulClkE, 1 , 1) == 0)? 0 : 0x20;
      }
      break;
    case INQUIRY_RSP_HOPPING:
      {
        uChar ucClkN_16_12 = GetBits(usClkN_l, 15 , 4) + //15--12
            GetBits(usClkN_h, 0, 1, 4); //16
        //this is actually X already.
        //determine the "N" somewhere
        ucX = (ucClkN_16_12 + usN) % 32;
        
        bY1 = 1;
        ucY2 = 0x20;
      }
      break;
    case CHANNEL_HOPPING:
      {
        //uShort *pClk = (uShort *) &ulClk;
        uShort usClk_l = ulClk&0x0000FFFF;
        uShort usClk_h = (ulClk&0xFFFF0000)>>16;

        //determine X
        ucX = GetBits(usClk_l, 6, 5); //6--2
        //determine Y1
        bY1 = GetBits(usClk_l, 1, 1); //1
        //determine Y2
        ucY2 = (GetBits(usClk_l,1, 1)==0)? 0 : 0x20;
        //determine A
        ucA = ucA ^ GetBits(usClk_h, 9, 5); 
        //A27--23 XOR Clk25--21
        //
        //determine C
        ucC = ucC ^ GetBits(usClk_h, 4, 5);
        //   XOR Clk20--16
        //
        //determine D
        usD = usD ^ GetBits(usClk_l, 15,9);
        //A18--10 ^ Clk15--7
        //
        //determine F
        uLong ulTemp = GetBits(usClk_l, 15, 9) + //clk15--7
            GetBits(usClk_h, 11, 12, 9); //clk27--16
        ucF = (ulTemp << 4) % 79; //16*
        assert(ucF<79);
        
      }
      break;
    default:
      cout <<"Should never arrive here!" <<endl;
  }
  uChar ucResult = 0x00;
  ucResult = (ucX + ucA) % 32; //ADD
  ucResult = ucResult ^ ucB;   //XOR
  
  //Ci ^ Y1
  uChar ucResultCY1 = (bY1==1) ? (ucC ^ 0x1F) : (ucC ^ 0x00);
  ucResultCY1 = ucResultCY1 & 0x1F;

  //Perm5
  ucResult = Perm5(ucResult, ucResultCY1, usD);
  ucResult = ucResult & 0x1F;
  
  uShort usTemp = (ucE + ucF + ucY2 + ucResult) % 79;
  ucResult = usTemp & 0x7F;

  //map to the hop frequences.
  if(ucResult <40)
    return ucResult * 2;
  else
    return ((ucResult - 39) * 2) - 1;

}

//ucVar1 and ucVar2 is actually a bit each.
void
BaseBand::ButterFly(uChar ucCtrl, uChar& ucVar1, uChar& ucVar2){
  if(ucCtrl) {
    uChar ucTemp = ucVar1;
    ucVar1 = ucVar2;
    ucVar2 = ucTemp;
  }
  return;
}

//Permutation operation.
uChar
BaseBand::Perm5(uChar ucZ, uChar ucCY1, uShort usD) {
  uChar ucZi[5];
  uChar ucPi[14];

  //Get Z(i)
  for(int i=0; i<5; i++)
    ucZi[i] = (ucZ >> i) & 0x01;
  //Get P(i)
  for(int i=0; i<9; i++) 
    ucPi[i] = (uChar)((usD >> i) & 0x0001);
  for(int i=0; i<5; i++) 
    ucPi[i+9] = (ucCY1 >> i) & 0x01;
  
  //Stage 1
  ButterFly(ucPi[13],ucZi[1], ucZi[2]);
  ButterFly(ucPi[12],ucZi[0], ucZi[3]);
  //Stage 2
  ButterFly(ucPi[11],ucZi[1], ucZi[3]);
  ButterFly(ucPi[10],ucZi[2], ucZi[4]);
  //Stage 3
  ButterFly(ucPi[9],ucZi[0], ucZi[3]);
  ButterFly(ucPi[8],ucZi[1], ucZi[4]);
  //Stage 4
  ButterFly(ucPi[7],ucZi[3], ucZi[4]);
  ButterFly(ucPi[6],ucZi[0], ucZi[2]);
  //Stage 5
  ButterFly(ucPi[5],ucZi[1], ucZi[3]);
  ButterFly(ucPi[4],ucZi[0], ucZi[4]);
  //Stage 6
  ButterFly(ucPi[3],ucZi[3], ucZi[4]);
  ButterFly(ucPi[2],ucZi[1], ucZi[2]);
  //Stage 7
  ButterFly(ucPi[1],ucZi[2], ucZi[3]);
  ButterFly(ucPi[0],ucZi[0], ucZi[1]);

  return (ucZi[0] + (ucZi[1] <<1) + (ucZi[2] <<2) + (ucZi[3] <<3) +
          (ucZi[4] << 4));
}


uShort 
BaseBand::GetBits(uShort usVar, uChar ucStartBit, 
        uChar ucHowmany, uChar ucLeftShift){
  //NOTE: ucStartBit is the highest bit we want.
  //assert(ucStartBit <= 32);
  //assert(ucHowmany <= ucStartBit + 1);
  uShort usTemp =( ((1<< (ucStartBit+1)) - 1) & usVar ) 
      >> (ucStartBit - ucHowmany + 1);
  usTemp = usTemp <<ucLeftShift;
  return usTemp;
}  


//unsigned short crc = 0xffff;
//
//g(d)=d^16 + d^12 + d^5 + 1;
//
//http://ams.cern.ch/AMS/Dataformats/node26.html
void
BaseBand::CRC16(uShort& usCrc, uChar *ucData, uShort usLen) {
  uChar ucIndex;
  uShort usTemp;
  
  for(uChar i=0; i<usLen; i++) {
    usTemp = *ucData++ <<8;
    for(ucIndex = 0; ucIndex <=7; ucIndex++) {
      if((usCrc ^ usTemp) & 0x8000)
        usCrc = (usCrc << 1) ^ 0x1021;
      else
        usCrc = (usCrc << 1);
      usTemp <<=1;
    }
  }
  cout <<"CRC = %x\n" << usCrc <<endl;
  return;
}

//Note: since we operate on byte boundary, we have to zero 
//     higher(???) or lower(???)     
//      bits if non-byte-able vars is used.
uChar
BaseBand::Hec(uChar ucCrc, uChar *ucData, uChar ucLen) {
  uChar ucIndex;
  uShort usTemp;
  
  for(uChar i=0; i<ucLen; i++) {
    usTemp = *ucData++ <<8;
    for(ucIndex = 0; ucIndex <=7; ucIndex++) {
      if((ucCrc ^ usTemp) & 0x80) //to be confirmed.
        ucCrc = (ucCrc << 1) ^ 0xA7;
      else
        ucCrc = (ucCrc << 1);
      usTemp <<=1;
    }
  }
  cout <<"CRC = %x\n" << ucCrc <<endl;
  return ucCrc;
}

void 
BaseBand::Fec23(uChar *ucData, uShort usLen) {
  //TODO: to fill it later.
  return;
}

void 
BaseBand::Fec13(uChar *ucData, uShort usLen) {
  //just repeat a bit three timers.
  //TODO: to fill it later.
  return;
}

bool
BaseBand::isWithinRange(BlueNode *pNode) {
  double dX1, dY1;
  double dX2, dY2;
  double dDistance;
  dX1 = this->pNode->LocationX();
  dY1 = this->pNode->LocationY();
  dX2 = pNode->LocationX();
  dY2 = pNode->LocationY();
  dDistance = sqrt((dX1 - dX2) *(dX1 - dX2) + 
          (dY1 -dY2) * (dY1 - dY2));
  return dDistance<=BT_MAXRANGE;
}

//QT color for display
uChar
BaseBand::Freq() {
  UpdateRecvFreq();
  return ucRecvFreq;
}

void BaseBand::TestFreq() {
  BdAddr tmpAddr = {{0xef25, 0x2a96, 0x0000}};
  BdAddr testAddr = {{0x0000, 0x0000, 0x0000}}; 
  testAddr = tmpAddr;
  //BdAddr tmpAddr = {{0x0000, 0x0000, 0x0000}};
  //test begins
  printf("============Start Test==========\n");
  printf(" /////////Inquiry\\\\\\\\\\\n");
  ulClkN = (uLong) 0;  
  for(uLong i = 0; i<64; i++ ) {
    uChar testFreq = HopSelection(0,INQUIRY_HOPPING, tmpAddr);
    printf("  freq = %x  %d ||", (int)ulClkN, testFreq) ;
    ulClkN = ulClkN + 1;
    if(!(i%4))
      printf("\n");
  }
  printf("\n");
//starts from 0x0001000
  printf("starts from 0x0001000\n");
  ulClkN = 0x0001000;
  for(uLong i = 0; i<64; i++ ) {
    uChar testFreq = HopSelection(0,INQUIRY_HOPPING, tmpAddr);
    printf("  freq = %x  %d ||", (int)ulClkN, testFreq) ;
    ulClkN = ulClkN + 1;
    if(!(i%4))
      printf("\n");
  }
  printf("\n");
//starts from 0x0002000
  printf("starts from 0x0002000\n");
  ulClkN = 0x0002000;
  for(uLong i = 0; i<64; i++ ) {
    uChar testFreq = HopSelection(0,INQUIRY_HOPPING, tmpAddr);
    printf("  freq = %x  %d ||", (int)ulClkN, testFreq) ;
    ulClkN = ulClkN + 1;
    if(!(i%4))
      printf("\n");
  }
  printf("\n");
//starts from 0x0003000
  printf("starts from 0x0003000\n");
  ulClkN = 0x0003000;
  for(uLong i = 0; i<64; i++ ) {
    uChar testFreq = HopSelection(0,INQUIRY_HOPPING, tmpAddr);
    printf("  freq = %x  %d ||", (int)ulClkN, testFreq) ;
    ulClkN = ulClkN + 1;
    if(!(i%4))
      printf("\n");
  }
  
  printf("\n");

//test Inquiry scan hop frequence.
  ulClkN = (uLong) 0;
  printf(" /////////Inquiry Scan\\\\\\\\\\n");
  for(uLong i = 0; i<64; i++ ) {
    uChar testFreq = HopSelection(0,INQUIRY_SCAN_HOPPING, tmpAddr);
    printf("  freq = %x  %d||", (int)ulClkN, testFreq) ;
    if(!(i%4))
      printf("\n");
    ulClkN = ulClkN + 0x1000; 
  }
  printf("\n");
  
//test page scan hop frequence.
  ulClkN = (uLong) 0;
  printf(" /////////Page Scan\\\\\\\\\\n");
  for(uLong i = 0; i<64; i++ ) {
    uChar testFreq = HopSelection(0,PAGE_SCAN_HOPPING, tmpAddr);
    printf("  freq = %x  %d||", (int)ulClkN, testFreq) ;
    if(!(i%4))
      printf("\n");
    ulClkN = ulClkN + 0x1000; 
  }
  printf("\n");

//Test Page hop frequence.  
  printf(" /////////PAGE\\\\\\\\\\\n");
  ulClkE = (uLong) 0;
  ulClkN = (uLong) 0;

  for(uLong i = 0; i<64; i++ ) {
    uChar testFreq = HopSelection(0,PAGE_HOPPING, tmpAddr);
    printf("  freq = %x  %d ||", (int)ulClkE, testFreq) ;
    ulClkE = ulClkE + 1;
    if(!(i%4))
      printf("\n");
  }
  printf("\n");
//starts from 0x0001000
  printf("starts from 0x0001000\n");
  ulClkE = 0x0001000;
  for(uLong i = 0; i<64; i++ ) {
    uChar testFreq = HopSelection(0,PAGE_HOPPING, tmpAddr);
    printf("  freq = %x  %d ||", (int)ulClkE, testFreq) ;
    ulClkE = ulClkE + 1;
    if(!(i%4))
      printf("\n");
  }
  printf("\n");
//starts from 0x0002000
  printf("starts from 0x0002000\n");
  ulClkE = 0x0002000;
  for(uLong i = 0; i<64; i++ ) {
    uChar testFreq = HopSelection(0,PAGE_HOPPING, tmpAddr);
    printf("  freq = %x  %d ||", (int)ulClkE, testFreq) ;
    ulClkE = ulClkE + 1;
    if(!(i%4))
      printf("\n");
  }
  printf("\n");
//starts from 0x0003000
  printf("starts from 0x0003000\n");
  ulClkE = 0x0003000;
  for(uLong i = 0; i<64; i++ ) {
    uChar testFreq = HopSelection(0,PAGE_HOPPING, tmpAddr);
    printf("  freq = %x  %d ||", (int)ulClkE, testFreq) ;
    ulClkE = ulClkE + 1;
    if(!(i%4))
      printf("\n");
  }
  
//test master page resp hop frequence.
  printf("\n============^^^^^^===========\n");
  ulClkE = (uLong) 0x0000014;
  ulClkF = (uLong) 0x0000012;
  ucTrainType = 'A';
  usN = 1;
  printf("/////////Master Page Resp\\\\\\\\\\n");
  printf("\n");
  for(uLong i = 0; i<64; i++ ) {
    uChar testFreq = HopSelection(ulClkF,MASTER_PAGE_RSP_HOPPING, tmpAddr);
    printf("  freq = %x  %d||", (int)ulClkE, testFreq) ;
    if(!((i+1)%4))
      printf("\n");
    ulClkE = ulClkE + 2;
     if((i%2)==1)
      usN++;         
  }

  printf("\n");
  return;    
}
