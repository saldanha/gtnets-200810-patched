# $Id: qtcheck.mk 458 2006-01-27 00:14:10Z dheeraj $

ifndef _QT_CHECK_
_QT_CHECK_	:= DONE



ifndef QTDIR
# some brain-dead distros may have a potpourri
ifeq ($(findstring qt.h, $(wildcard /usr/include/*)), qt.h)
ifeq ($(findstring libqt.so, $(wildcard /usr/lib/*)), libqt.so)
       MOC        =    /usr/bin/moc
       CFLAGS    +=   -DHAVE_QT
       QT_INCL    =   -I/usr/include
       HAVE_QT    =    1
       QT_LIBS    =   -L/usr/lib -lqt $(GLDFLAGS)/usr/lib/qt3
endif
ifeq ($(findstring libqt-mt.so, $(wildcard /usr/lib/*)), libqt.so)
       MOC        =    /usr/bin/moc
       CFLAGS    +=   -DHAVE_QT
       QT_INCL    =   -I/usr/include
       HAVE_QT    =    1
       QT_LIBS    =   -L/usr/lib -lqt-mt $(GLDFLAGS)/usr/lib/qt3
endif

endif

# debian has them here
ifeq ($(findstring qt.h, $(wildcard /usr/include/qt3/*)), qt.h)
ifeq ($(findstring libqt.so, $(wildcard /usr/lib/qt3/*)), libqt.so)
       HAVE_QT    = 1
       MOC        =    /usr/bin/moc
       CFLAGS    +=   -DHAVE_QT
       QT_INCL    =   -I/usr/include/qt3
       QT_LIBS    =   -L/usr/lib/qt3 -lqt $(GLDFLAGS)/usr/lib/qt3
endif
ifeq ($(findstring libqt-mt.so, $(wildcard /usr/lib/qt3/*)), libqt.so)
       HAVE_QT    = 1
       MOC        =    /usr/bin/moc
       CFLAGS    +=   -DHAVE_QT
       QT_INCL    =   -I/usr/include/qt3
       QT_LIBS    =   -L/usr/lib/qt3 -lqt-mt $(GLDFLAGS)/usr/lib/qt3
endif
ifeq ($(findstring libqt.so, $(wildcard /usr/lib/*)), libqt.so)
       HAVE_QT    = 1
       MOC        =    /usr/bin/moc
       CFLAGS    +=   -DHAVE_QT
       QT_INCL    =   -I/usr/include/qt3
       QT_LIBS    =   -L/usr/lib -lqt $(GLDFLAGS)/usr/lib
endif
ifeq ($(findstring libqt-mt.so, $(wildcard /usr/lib/*)), libqt-mt.so)
       HAVE_QT    = 1
       MOC        =    /usr/bin/moc
       CFLAGS    +=   -DHAVE_QT
       QT_INCL    =   -I/usr/include/qt3
       QT_LIBS    =   -L/usr/lib -lqt-mt $(GLDFLAGS)/usr/lib
endif
endif

# freebsd has them here
ifeq ($(findstring qt.h, $(wildcard /usr/local/include/*)), qt.h)
ifeq ($(findstring libqt.so, $(wildcard /usr/local/lib/*)), libqt.so)
       HAVE_QT    = 1
       MOC        =   /usr/local/bin/moc
       CFLAGS    +=   -DHAVE_QT
       QT_INCL    =   -I/usr/local/include
       QT_LIBS    =   -L/usr/local/lib -lqt $(GLDFLAGS)/usr/local/lib
endif
endif

# QT from troll tech installs in a separate directory
ifeq ($(findstring qt.h, $(wildcard /usr/local/qt3/include*)), qt.h)
ifeq ($(findstring libqt.so, $(wildcard /usr/local/qt3/lib/*)), libqt.so)
       HAVE_QT   = 1
       MOC       =  /usr/local/bin/moc
       CFLAGS   +=  -DHAVE_QT
       QT_INCL   =  -I/usr/local/include/qt3
       QT_LIBS   =  -L/usr/local/qt3/lib -lqt $(GLDFLAGS)/usr/local/qt3/lib
endif
ifeq ($(findstring libqt-mt.so, $(wildcard /usr/local/qt3/lib/*)), libqt.so)
       HAVE_QT   = 1
       MOC       =  /usr/local/bin/moc
       CFLAGS   +=  -DHAVE_QT
       QT_INCL   =  -I/usr/local/include/qt3
       QT_LIBS   =  -L/usr/local/qt3/lib -lqt-mt $(GLDFLAGS)/usr/local/qt3/lib
endif
endif

# redhat has QTDIR defined but just in case its not ??
ifeq ($(findstring qt.h, $(wildcard /usr/lib/qt3/include/*)), qt.h)
ifeq ($(findstring libqt.so, $(wildcard /usr/lib/qt3/lib/*)), libqt.so)
       HAVE_QT   = 1
       MOC       = /usr/lib/qt3/bin/moc
       CFLAGS   += -DHAVE_QT
       QT_INCL   = -I/usr/lib/qt3/include
       QT_LIBS   = -L/usr/lib/qt3/lib -lqt $(GLDFLAGS)/usr/lib/qt3/lib
endif
ifeq ($(findstring libqt-mt.so, $(wildcard /usr/lib/qt3/lib/*)), libqt.so)
       HAVE_QT   = 1
       MOC       = /usr/lib/qt3/bin/moc
       CFLAGS   += -DHAVE_QT
       QT_INCL   = -I/usr/lib/qt3/include
       QT_LIBS   = -L/usr/lib/qt3/lib -lqt-mt $(GLDFLAGS)/usr/lib/qt3/lib
endif
endif

# Check for redhad qt-3.3
ifeq ($(findstring qt.h, $(wildcard /usr/lib/qt-3.3/include/*)), qt.h)
ifeq ($(findstring libqt.so, $(wildcard /usr/lib/qt-3.3/lib/*)), libqt.so)
       HAVE_QT   = 1
       MOC       = /usr/lib/qt-3.3/bin/moc
       CFLAGS   += -DHAVE_QT
       QT_INCL   = -I/usr/lib/qt-3.3/include
       QT_LIBS   = -L/usr/lib/qt-3.3/lib -lqt $(GLDFLAGS)/usr/lib/qt-3.3/lib
endif
ifeq ($(findstring libqt-mt.so, $(wildcard /usr/lib/qt-3.3/lib/*)), libqt-mt.so)
       HAVE_QT   = 1
       MOC       = /usr/lib/qt-3.3/bin/moc
       CFLAGS   += -DHAVE_QT
       QT_INCL   = -I/usr/lib/qt-3.3/include
       QT_LIBS   = -L/usr/lib/qt-3.3/lib -lqt-mt $(GLDFLAGS)/usr/lib/qt-3.3/lib
endif
endif

# fedora nuances, moved qt to yet another place QTDIR is not defined
ifeq ($(findstring qt.h, $(wildcard /usr/lib/qt-3.1/include/*)), qt.h)
ifeq ($(findstring libqt.so, $(wildcard /usr/lib/qt-3.1/lib/*)), libqt.so)
	HAVE_QT   = 1
	MOC       = /usr/lib/qt-3.1/bin/moc
	CFLAGS    += -DHAVE_QT
	QT_INCL   = -I/usr/lib/qt-3.1/include
	QT_LIBS   = -L/usr/lib/qt-3.1/lib -lqt $(GLDFLAGS)/usr/lib/qt-3.1/lib
endif
ifeq ($(findstring libqt-mt.so, $(wildcard /usr/lib/qt-3.1/lib/*)), libqt.so)
	HAVE_QT   = 1
	MOC       = /usr/lib/qt-3.1/bin/moc
	CFLAGS    += -DHAVE_QT
	QT_INCL   = -I/usr/lib/qt-3.1/include
	QT_LIBS   = -L/usr/lib/qt-3.1/lib -lqt-mt $(GLDFLAGS)/usr/lib/qt-3.1/lib
endif
endif

# MacOS X doesn;t have QTDIR defined if qt3 is installed from fink
# Note:: this is not the native macos X it is the one built on x11
ifeq ($(findstring qt.h, $(wildcard /sw/include/qt/*)), qt.h)
ifeq ($(findstring libqt-mt.dylib, $(wildcard /sw/lib/*)), libqt-mt.dylib)
       HAVE_QT   = 1
       MOC       = /sw/bin/moc
       CFLAGS   += -DHAVE_QT
       QT_INCL   = -I/sw/include/qt
       QT_LIBS   = -L/sw/lib -lqt-mt
endif
ifeq ($(findstring libqt.dylib, $(wildcard /sw/lib/*)), libqt.dylib)
       HAVE_QT   = 1
       MOC       = /sw/bin/moc
       CFLAGS   += -DHAVE_QT
       QT_INCL   = -I/sw/include/qt
       QT_LIBS   = -L/sw/lib -lqt
endif
endif

# ## MacOS X native qt is installed in /Developer/qt
# ## This is added later than the X11 version so that it is preferred, if found
# ifeq ($(findstring qt.h, $(wildcard /Developer/qt/include/*)), qt.h)
# ifeq ($(findstring libqt-mt.dylib, $(wildcard /Developer/qt/lib/*)), libqt-mt.dylib)
#        HAVE_QT   = 1
#        MOC       = /Developer/qt/bin/moc
#        CFLAGS   += -DHAVE_QT
#        QT_INCL   = -I/Developer/qt/include
#        QT_LIBS   = -L/Developer/qt/lib -lqt-mt
# endif
# ifeq ($(findstring libqt.dylib, $(wildcard /Developer/qt/lib/*)), libqt.dylib)
#        HAVE_QT   = 1
#        MOC       = /Developer/qt/bin/moc
#        CFLAGS   += -DHAVE_QT
#        QT_INCL   = -I/Developer/qt/include
#        QT_LIBS   = -L/Developer/qt/lib -lqt
# endif
# endif

else
       MOC       = $(QTDIR)/bin/moc
       CFLAGS   += -DHAVE_QT
       QT_INCL   = -I$(QTDIR)/include
ifeq ($(findstring libqt.so, $(wildcard $(QTDIR)/lib/*)), libqt.so)
       HAVE_QT   = 1
       QT_LIBS   = -L$(QTDIR)/lib -lqt $(GLDFLAGS)$(QTDIR)/lib
endif
ifeq ($(findstring libqt-mt.so, $(wildcard $(QTDIR)/lib/*)), libqt-mt.so)
       HAVE_QT   = 1
       QT_LIBS   = -L$(QTDIR)/lib -lqt-mt $(GLDFLAGS)$(QTDIR)/lib
endif
ifeq ($(findstring libqt.dylib, $(wildcard $(QTDIR)/lib/*)), libqt.dylib)
       HAVE_QT   = 1
       QT_LIBS   = -L$(QTDIR)/lib -lqt
endif
ifeq ($(findstring libqt-mt.dylib, $(wildcard $(QTDIR)/lib/*)), libqt-mt.dylib)
       HAVE_QT   = 1
       QT_LIBS   = -L$(QTDIR)/lib -lqt-mt
endif
endif

# this target is for debugging
checkqt:
	echo $(MOC) ; \
	echo $(CFLAGS) ; \
	echo $(QT_INCL) ; \
	echo $(QT_LIBS) ;


endif
