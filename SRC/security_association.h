#ifndef __security_association_h__
#define __security_association_h__

#include <map>
#include "common-defs.h"

typedef std::map<IPAddr_t, IPAddr_t> SecurityAssociationMap_t;

class SecurityAssociation {
	
public:
	SecurityAssociation();
	
	void AddSecurityAssociation(IPAddr_t, IPAddr_t);
	IPAddr_t GetSecurityAssociation(IPAddr_t);
	
private:
	SecurityAssociationMap_t sa;
};
#endif

