// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: pdu.h 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - Protocol Data Unit base class
// George F. Riley.  Georgia Tech, Spring 2002

// Class PDU is the base class for all packet headers

#ifndef __pdh_h__
#define __pdh_h__

#include <vector>

#include "debug.h"
#include "common-defs.h"
#include "tfstream.h"
#include "globalstats.h"
#include "serializable.h"
#include "memreuse.h"

//Doc:ClassXRef
class PDU : public Serializable, public ReuseBase {
//Doc:Class Class {\tt PDU} serves as the base class for all the 
//Doc:Class protocol data units. Protocol headers and data chunks that
//Doc:Class that form the packets are derived from this class.
	
public:
  //Doc:Method
  PDU() { DBG((Stats::pdusAllocated++));}
  //Doc:Desc This method is the default constructor for this class. It
  //Doc:Desc updates a static variable to reflect the number of PDUs
  //Doc:Desc allocated
  
  //Doc:Method
  PDU(const PDU&) { DBG((Stats::pdusAllocated++));}
  //Doc:Desc This constructs a PDU from the reference to an already existing
  //Doc:Desc PDU.
  //Doc:Arg1 The const reference to the existing PDU
  
  //Doc:Method
  virtual ~PDU() { DBG((Stats::pdusDeleted++));}
  
public:
  //Doc:Method
  virtual Size_t Size() const = 0;    // PDU's must have a size
  //Doc:Desc This method returns the size of the PDU
  //Doc:Return the size of the PDU
  
  //Doc:Method
  virtual PDU* Copy() const = 0;            // Create a copy of this header
  //Doc:Desc This method creates a copy of this PDU
  //Doc:Return A pointer to the copy of this PDU
  
  //Doc:Method  
  virtual Layer_t Layer() { return 0;}// Some PDU's specify their layer
  //Doc:Desc Each PDU must also return a layer numer as to which protocol layer
  //Doc:Desc it belongs to according to the ISO model
  //Doc:Return the layer number
  
  //Doc:Method  
  virtual Layer_t SubLayer() { return 0;}// Some layers have sublayers
  //Doc:Desc Each PDU may be part of a sub-layer.  For example, LLCSNAP
  //Doc:Desc is layer "2a", sitting between the real layer 2 and l3 PDUs.
  //Doc:Return The sub-layer number
  
  //Doc:Method  
  virtual Count_t Version() { return 0;} // Some PDU's report version number
  //Doc:Desc If we are using multiple versions of the same PDU we may want
  //Doc:Desc to return the version number of the PDU we are using. Also useful
  //Doc:Desc when tracing
  //Doc:Return the version number
  
  //Doc:Method  
  virtual Proto_t Proto() { return 0;}   // Some PDU's report protocol number
  //Doc:Desc Most of the protocols are identified by the numeric values as
  //Doc:Desc listed by IANA. thie method returns that protocol number
  //Doc:Return the protocol number

  //Doc:Method
  virtual Priority_t Priority() { return 0;} // Some PDU's have priority
    //Doc:Desc For queues that support priorities, packets need some way to
    //Doc:Desc claim what the priority is.  This is done by querying the
    //Doc:Desc PDU list.  When a non-zero priority is found, that
    //Doc:Desc is the claimed priority.
    //Doc:Return Priority associated with this PDU (zero if none.
  
  // Trace the contents of this pdu
  //Doc:Method  
  virtual void Trace(Tfstream&, Bitmap_t,
                     Packet* = nil, const char* = nil) { }
  //Doc:Desc This method traces the contents of the PDU
  //Doc:Arg1 File output stream descriptor
  //Doc:Arg2 Bitmap that specifies the level of detail of the trace
  //Doc:Arg3 Packet that contains this PDU (optional)
  //Doc:Arg4 Extra text information for trace file (optional)
  
  //Doc:Method  
  unsigned long Detail(Bitmap_t d, Bitmap_t b) { return ((1L << d) & b);}
  // Include the serializable interface here as a temporary patch
  // until all PDU subclasses implement this
  //Doc:Method  
  virtual Size_t SSize() { return 0;}      // Size needed for serialization
  //Doc:Desc The actual size of all the PDU contents in terms of bytes for
  //Doc:Desc serialization of the contents.
  //Doc:Return the size
  
  //Doc:Method  
  virtual char*  Serialize(char* b, Size_t&) { return b;}// Serialize to buffer
  //Doc:Desc This method is used to serialize the contents of the PDU
  //Doc:Desc into a serialized character buffer b . the size of this buffer is
  //Doc:Desc in the size arguement
  //Doc:Arg1 This is the pointer to the character buffer
  //Doc:Arg2 a reference to the size variable
  //Doc:Return the pointer to the updated character buffer
  
  //Doc:Method  
  virtual char*  Construct(char* b, Size_t&) { return b;}// Const. from buffer
  //Doc:Desc This method is the reverse of the Serialize . It constructs the
  //Doc:Desc PDU object from the character buffer that has been received over
  //Doc:Desc the network
  //Doc:Arg1 This is the pointer to the character buffer
  //Doc:Arg2 a reference to the size variable
  //Doc:Return the pointer to the character buffer of the remaining data
};

typedef std::vector<PDU*> PDUVec_t;     // Vector of packet headers

#endif


