// Test the GhostNode distributed simulation
// George F. Riley, Georgia Tech, Fall 2003

#include <iostream>
#include <vector>
#include <sstream>

#include "distributed-simulator.h"
#include "node.h"
#include "linkp2p.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "rng.h"
#include "ratetimeparse.h"
#include "star.h"
#include "validation.h"

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  if (argc < 3)
    {
      cout << "Usage: testghost myid number-lps (number-leaf-per-star)" << endl;
      exit(1);
    }
  // Make deterministic for testing
  Random::GlobalSeed(31731,44543,425345,19367,48201,72333);
  bool distributed = false;
  SystemId_t  myId = atol(argv[1]);
  // use a temporary int since we use -nProcs to indicate not distributed
  int intProcs = atol(argv[2]);
  Simulator* s;
  if (intProcs > 0)
    {
      s = new DistributedSimulator(myId);
      distributed = true;
    }
  else
    { // Not distributed, just for testing
      s = new Simulator();
      intProcs = -intProcs;
    }
  SystemId_t  nProcs = (SystemId_t)intProcs;;
  
  Count_t     nLeaf = 100; // Leaf nodes per star
  if (argc > 3) nLeaf = atol(argv[3]);
  
  IPAddr_t    baseIP = IPAddr("192.168.1.1");
  IPAddr_t    adderIP = IPAddr("0.0.1.0");
  
  Linkp2p   myLink(Rate("10Mb"), Time("10ms"));

  Trace* tf = Trace::Instance();
  TCP::LogFlagsText(true);    // Log flags in text mode
  
  ostringstream osstr;
  osstr << "testghost" << myId << ".txt";
  tf->Open(osstr.str().c_str());
  tf->IPDotted(true);
  // Enable all l3 tracing
  IPV4::Instance()->SetTrace(Trace::ENABLED);

  // Make nProcs star toplogies.  Connect center of
  // each star to make a clique
  vector<Star*> stars;
  IPAddr_t nextIP = baseIP;
  // Random number generator for start times
  Uniform startRng(0, 0.1);
  
  for (SystemId_t i = 0; i < nProcs; ++i)
    {
      SystemId_t thisId = i;
      if (!distributed) thisId = 0; // Use zero for all if not distributed
      cout << "Creating star " << i << " id " << thisId << endl;
      Star* st = new Star(nLeaf, nextIP, thisId);
      stars.push_back(st);
      nextIP += adderIP;
      
      // Make the clique
      for (SystemId_t j = 0; j < i; ++j)
        {
          Star* st1 = stars[j];
          Node* n1 = st->GetHub();
          Node* n2 = st1->GetHub();
          n1->AddDuplexLink(n2);
        }
      // Add a tcp server on every leaf (if my id)
      if ((i == myId) || !distributed)
        { // Add a tcp server at each leaf
          for (Count_t k = 0; k < st->LeafCount(); ++k)
            {
              Node* n = st->GetLeaf(k);
              TCPServer* tcpApp =
                  (TCPServer*)n->AddApplication(TCPServer());
              tcpApp->BindAndListen(80);
              tcpApp->Start(0);
            }
        }
    }
  // Now add the tcp sending apps
  SystemId_t nextPeerSystem = (myId + 1) % nProcs;
  Count_t    nextPeerNode = 0;
  for (SystemId_t i = 0; i < nProcs; ++i)
    {
      if ((i == myId) || !distributed)
        { // Add a tcp sender at each leaf
          Star* st = stars[i];
          for (Count_t k = 0; k < st->LeafCount(); ++k)
            {
              Node* n = st->GetLeaf(k);
              if (nextPeerSystem == i && nextPeerNode == k)
                { // Don't connect to self
                  nextPeerNode = (++nextPeerNode) % nLeaf;
                }
              Node* pn = stars[nextPeerSystem]->GetLeaf(nextPeerNode);
              TCPSend* ts = (TCPSend*)n->AddApplication(
                  TCPSend(pn->GetIPAddr(),80,
                          Constant(150000)));
              ts->Start(startRng.Value());
              nextPeerNode = (++nextPeerNode) % nLeaf;
              nextPeerSystem = (++nextPeerSystem) % nProcs;
            }
        }
    }
  s->Progress(1);
  s->StopAt(100);
  s->Run();
  cout << "Simulation complete, lp " << myId << endl;
  s->PrintStats();
}

  
