#include <iostream>

#include "debug.h"
#include "l3prototaclane.h"
#include "ratetimeparse.h"
#include "node-taclane.h"
#include "droppdu.h"
#include "routing-nixvector.h"

using namespace std;

// TaclaneHeader functions
TaclaneHeader* TaclaneHeader::CopyFromIP(IPV4Header* ipHdr, Size_t padding)
{
  TaclaneHeader* tlHdr = new TaclaneHeader(padding);
  tlHdr->version = ipHdr->version;
  tlHdr->serviceType = ipHdr->serviceType;
  tlHdr->totalLength = ipHdr->totalLength;
  tlHdr->identification = ipHdr->identification;
  tlHdr->flags = ipHdr->flags;
  tlHdr->fragmentOffset = ipHdr->fragmentOffset;
  tlHdr->ttl = ipHdr->ttl;
  tlHdr->protocol = ipHdr->protocol;
  tlHdr->headerChecksum = ipHdr->headerChecksum;
  tlHdr->src = ipHdr->src;
  tlHdr->dst = ipHdr->dst;
  return tlHdr;
  
}


L3ProtoTACLANE::L3ProtoTACLANE( TACLANENode* n0, Interface* pt, Interface* ct )
  : ptif(pt), ctif(ct), n(n0)
{
  n->InsertProto(3, Proto(), this);
  DetailOff(HEADERLENGTH);
  DetailOff(SERVICETYPE);
  DetailOff(TOTALLENGTH);
  DetailOff(IDENTIFICATION);
  DetailOff(FLAGS);
  DetailOff(FRAGMENTOFFSET);
  DetailOff(HEADERCHECKSUM);
  DetailOff(OPTIONS);	

}

void L3ProtoTACLANE::DataRequest(Node*, Packet* p, void*)
{ // Should never happen for taclane devices
  cout << "HuH?  DataRequest on Taclane device..ignoring" << endl;
  delete p;
}

void L3ProtoTACLANE::DataIndication(Interface* i,Packet* p)
{
  //Get the encryption level of the interface and the packet
  // Encrypt packet_enc = p->GetEncryption(); ?? Do we need this?
  Time_t enc_delay = 0.0; // Encryption/decryption delay
  
  // Add to trace file
  if (Trace::Enabled()) n->TracePDU(this, p->PeekPDU(), p, "+");

  // Call the callbacks
  if (!n->CallCallbacks(Layer(), Proto(), PacketCallbacks::RX, p)) return;
  // If the callback returns false, the packet has been deleted.

  // The node should always be a TACLANE ndoe here.
  TACLANENode* tln = (TACLANENode*)n;
  
  RoutingEntry re;
  if (i == ctif)
    { // Got a packet on the ciphertext inteface.
      // First check enabled/disabled..ignore all this if disabled
      if (tln->enabled)
        {
          
          // Remove the taclaneheader
          TaclaneHeader* tlHdr = (TaclaneHeader*)p->PopPDU();
          enc_delay = Delay(p); //decryption delay
      
          // The destination address should be this i/f.  discard if not
          if (tlHdr->dst != ctif->GetIPAddr())
            {
              if (Trace::Enabled())
                {
                  DropPDU d("L3T-ND", p);
                  n->TracePDU(this, &d);
                }
              DEBUG(0,(cout << "Taclane dropping ct packet dst mismatch" << endl));
              Stats::pktsDropped++;
              delete p;
              return;
            }
          // Set packet to plaintext
          p->SetEncryption(PT);
        }
#ifndef WIN32
      re.interface = ptif; // Route to plaintext interface
#else
	  re.interface_ = ptif; // Route to plaintext interface
#endif
      re.nexthop = ptif->PeerIP(0);
    }
  else if (i == ptif)
    { // Got a packet on the plaintext interface
      // First check enabled/disabled
      if (tln->enabled)
        {
          // Get ip hdr (don't remove)
          IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
          Size_t padding = Padding(p);
          DEBUG0((cout << "Taclane pkt size " << p->Size()
                  << " padding " << padding 
                  << endl));
          TaclaneHeader* tlHdr = TaclaneHeader::CopyFromIP(iphdr, padding);
          tlHdr->totalLength += tlHdr->Size(); // Account for new size
          enc_delay = Delay(p); // Encryption/decryption delay

          // We need to replace the src/dst fields to be this interface (src)
          // and the security association for the dst
          IPAddr_t sa_addr = n->GetSecurityAssociation(iphdr->dst);
          if (sa_addr == IPADDR_NONE)
            { // Oops! No SA
              if (Trace::Enabled())
                {
                  DropPDU d("L3T-NSA", p);
                  n->TracePDU(this, &d);
                }
              DEBUG(0,(cout << "Taclane dropping pt packet, no security assoc." 
                       << endl));
              Stats::pktsDropped++;
              delete tlHdr;
              delete p;
              return;
            }
          tlHdr->src = ctif->GetIPAddr();
          tlHdr->dst = sa_addr;
          p->PushPDU(tlHdr);
          // Set packet to ciphertext
          p->SetEncryption(CT);
        }
#ifndef WIN32
      re.interface = ctif; // Route to ciphertext interface
#else
      re.interface_ = ctif; // Route to ciphertext interface
#endif
      re.nexthop = ctif->PeerIP(0);
    }
  else
    {
      cout << "HuH?  Got taclane pkt on unknown interface" << endl;
      // Should never happen
      delete p;
      return;
    }
  // If the packet has a nixvector, it is no longer valid, as the
  // src/dst may be changed
  if (p->nixVec)
    {
      delete p->nixVec;
      p->nixVec = nil;
    }
  
  if (enc_delay != 0.0)
    {
      // Now schedule an event to transmit the packet after the enc/dec delay
      TaclaneEvent* e = new TaclaneEvent(re, p);
      DEBUG0((cout << "Scheduling taclane delay of " << enc_delay
              << " now " << Simulator::Now()
              << endl));
      Scheduler::Schedule(e, enc_delay, this);
    }
  else
    { // No delay, just process now.
      if (Trace::Enabled()) n->TracePDU(this, p->PeekPDU(), p, "-");
#ifndef WIN32
      re.interface->Send(p, re.nexthop, Proto());
#else
      re.interface_->Send(p, re.nexthop, Proto());
#endif
    }
  
}

Interface* L3ProtoTACLANE::PeekInterface(Node*, void*)  // Find forwarding if
{ // We can safely return nil here, since this is only used by L4 protocols
  return nil;
}

// Handler methods
void   L3ProtoTACLANE::Handle(Event* e, Time_t)
{
  TaclaneEvent* te = (TaclaneEvent*)e;
  // The only event is to transmit the packet, so just transmit it
  // First trace it if tracing enabled
  DEBUG0((cout << "Taclane::Handle, now " << Simulator::Now() << endl));
  if (Trace::Enabled())
    {
      // Need to look into this in more detail..something is not quite right
      //Trace::Instance()->AppendEOL(); // Force new line (this is a hack!
      n->TracePDU(this, te->p->PeekPDU(), te->p, "-");
    }
#ifndef WIN32
  te->r.interface->Send(te->p, te->r.nexthop, Proto());
#else
  te->r.interface_->Send(te->p, te->r.nexthop, Proto());
#endif
  // Delete the event
  delete e;
}


  
Time_t L3ProtoTACLANE::Delay(Packet* p)
{
  Size_t s = p->Size(); // Size in bytes of packet
  if (s < 46) return 0;
  if (s <= 110) return Time("1.875ms");
  if (s <= 238) return Time("2.25ms");
  if (s <= 494) return Time("2.3ms");
  if (s <= 1006) return Time("2.4ms");
  return Time("3.1ms");
}

Size_t L3ProtoTACLANE::Padding(Packet* p)
{ // Compute the added size for the packet based on current size
  Size_t s = p->Size(); // Size in bytes of packet
  if (s <= 80) return 136 - 20- s;
  // Should check max size and drop pkt here if too big
  return 184 + 48 * (int)((s - 81)/48) - 20 - s;
}

  
