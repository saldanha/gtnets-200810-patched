/// Test the butterfly interconnect network
// George F. Riley, Georgia Tech, Spring 2005

#include "simulator.h"
#include "scheduler.h"
#include "butterfly.h"
#include "udp.h"
#include "validation.h"
#include "ratetimeparse.h"
#include "common-defs.h"
#include "args.h"
#include "node-firstbit.h"
#include "application-udpsink.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

using namespace std;

// Define a UDP application that sends variable size packets
// to variable destinations

class SendPktEvent : public Event {
public:
  typedef enum { SEND_PKT = 100 } AppCBREvent_t;
  SendPktEvent() { }
  SendPktEvent(Event_t ev) : Event(ev) { }
};

class RandomUDPApplication : public Application
{
public:
  RandomUDPApplication(
      PortId_t      lPort,
      PortId_t      cPort,
      const Random& iPTime,
      const Random& pSize,
      const Random& dIP);
  RandomUDPApplication(const RandomUDPApplication&);
  ~RandomUDPApplication();
  // Handler functions
  void Handle(Event*, Time_t);
  // Applicaition functions
  virtual void StartApp();    // Called at time specified by Start
  virtual void StopApp();     // Called at time specified by Stop
  virtual void AttachNode(Node*); // Note which node attached to
  virtual Application* Copy() const;// Make a copy of the application
  virtual L4Protocol*  GetL4() const { return l4Proto;}
private:
  void SendNextPacket();
  void ScheduleNextSend();
  void CancelPendingSend();
public:
  PortId_t      localPort;
  PortId_t      destPort;
  Random*       interPacketTime;
  Random*       pktSize;
  Random*       destIP;
  Node*         pNode;
  L4Protocol*   l4Proto;
  SendPktEvent* spEvent;
  bool          scheduled;
};

RandomUDPApplication::RandomUDPApplication(
      PortId_t      lPort,
      PortId_t      dPort, 
      const Random& iPTime,
      const Random& pSize,
      const Random& dIP)
    : localPort(lPort), destPort(dPort),
      interPacketTime(iPTime.Copy()),
      pktSize(pSize.Copy()), destIP(dIP.Copy()), pNode(nil),
      l4Proto(nil), spEvent(nil), scheduled(false)
{
}

RandomUDPApplication::RandomUDPApplication(const RandomUDPApplication& c)
    : localPort(c.localPort), destPort(c.destPort),
      interPacketTime(c.interPacketTime->Copy()),
      pktSize(c.pktSize->Copy()), destIP(c.destIP->Copy()), pNode(c.pNode),
      l4Proto(nil), spEvent(nil), scheduled(false)
{
}


RandomUDPApplication::~RandomUDPApplication()
{
  delete interPacketTime;
  delete pktSize;
  delete destIP;
}

void RandomUDPApplication::Handle(Event* e, Time_t t)
{
  if (e->event == SendPktEvent::SEND_PKT)
    {
      SendNextPacket();
      scheduled = false;
      ScheduleNextSend();
    }
  else
    {
      Application::Handle(e, t);
    }
}

void RandomUDPApplication::StartApp()
{
  if (!pNode)
    {
      cout << "Oops!  RandomUDPApp without attached node" << endl;
      return;
    }
  CancelPendingSend();
  ScheduleNextSend();
}

void RandomUDPApplication::StopApp()
{
  CancelPendingSend();
}

void RandomUDPApplication::AttachNode(Node* n)
{
  pNode = n;
  l4Proto = new UDP();
  l4Proto->Attach(n);
  n->Bind(l4Proto->Proto(), localPort, l4Proto);
}

Application* RandomUDPApplication::Copy() const
{
  return new RandomUDPApplication(*this);
}

// Private methods
void RandomUDPApplication::SendNextPacket()
{
  // Choose random size and destination
  Size_t s = pktSize->IntValue();
  IPAddr_t d = destIP->IntValue();
  l4Proto->SendTo(s, d, destPort);
}

void RandomUDPApplication::ScheduleNextSend()
{
  if (!spEvent) spEvent = new SendPktEvent(SendPktEvent::SEND_PKT);
  Time_t t = interPacketTime->Value();
  Scheduler::Schedule(spEvent, t, this);
  scheduled = true;
}

void RandomUDPApplication::CancelPendingSend()
{
  if (spEvent && scheduled)
    {
      Scheduler::Cancel(spEvent);
    }
  scheduled = false;
}

int main(int argc, char** argv)
{
  Simulator s;
  IPAddr_t firstLeft = IPAddr("192.168.1.1");
  IPAddr_t firstRight = IPAddr("192.169.1.1");
  Count_t  nLeaf = 64;
  
  Butterfly b(nLeaf, 4, 3, firstLeft, firstRight);
#ifdef OLD_WAY
  typedef std::vector<UDP*>UDPVec_t;
  UDPVec_t iUDP;
  UDPVec_t oUDP;
  Uniform rand(0,64);
  Uniform startTime(0,100);
  StartUDPTimer udpTimer;
  for(int i=0;i<64;i++)
    {
      UDP* ui=new UDP(b.iNodes[i]);
      ui->Bind();
      iUDP.push_back(ui);
      UDP* uo=new UDP(b.oNodes[i]);
      uo->Bind(20000);
      oUDP.push_back(uo);
    }
  for(int i=0;i<10000;i++)
    {
      udpTimer.Schedule(new StartUDPEvent(iUDP[rand.Value()], 
					  IPAddr(33554433+rand.Value())), 
					  startTime.Value());
    }
#endif

  // Set the colors
  static QColor stdcol[12];
  stdcol[ 0].setRgb( 255,   0,   0 );   // index 249   red
  stdcol[ 1].setRgb(   0, 255,   0 );   // index 250   green
  stdcol[ 2].setRgb(   0,   0, 255 );   // index 252   blue
  stdcol[ 3].setRgb(   0, 255, 255 );   // index 254   cyan
  stdcol[ 4].setRgb( 255,   0, 255 );   // index 253   magenta
  stdcol[ 5].setRgb( 255, 255,   0 );   // index 251   yellow
  stdcol[ 6].setRgb( 128,   0,   0 );   // index 1     dark red
  stdcol[ 7].setRgb(   0, 128,   0 );   // index 2     dark green
  stdcol[ 8].setRgb(   0,   0, 128 );   // index 4     dark blue
  stdcol[ 9].setRgb(   0, 128, 128 );   // index 6     dark cyan
  stdcol[10].setRgb( 128,   0, 128 );   // index 5     dark magenta
  stdcol[11].setRgb( 128, 128,   0 );   // index 3     dark yellow

  Exponential  ipTime(Time("100ms"));
  Uniform      pktSize(100, 1400);
  Uniform      peerIP(firstRight, firstRight + nLeaf);
  Uniform      startRng(0, 0.100);
  PortId_t lPort = 10000;
  PortId_t rPort = 20000;
  UDP::DefaultPacketSize(1500);
  for(Count_t i = 0; i < nLeaf; i++)
    { // Add the random udp app at each left node and udp sink at right
      RandomUDPApplication* rudp = (RandomUDPApplication*)b.iNodes[i]->
          AddApplication(RandomUDPApplication(lPort, rPort,
                                              ipTime, pktSize, peerIP));
      rudp->GetL4()->SetColor(stdcol[i % 12]);
      //rudp->GetL4()->SetColor(QColor(3 * i, 3 * i, 3 * i).rgb());
      rudp->Start(startRng.Value());
      UDPSink* udps = (UDPSink*)b.oNodes[i]->
          AddApplication(UDPSink(rPort));
      udps->Start(0);
    }
  

  s.AmimationRegion(Location(0,0), Location(10, 10));
  b.BoundingBox(Location(1,0), Location(9, 10));
  s.StartAnimation(0, true, true);
  if(!Validation::noAnimation)
    {
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Progress(1);
  s.StopAt(100);
  s.Run();
}

