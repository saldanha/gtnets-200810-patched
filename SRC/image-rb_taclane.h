// Generated automatically by make-gtimage from ../IMAGES/rb_taclane.png
// DO NOT EDIT

#include "image.h"

class rb_taclaneImage : public Image {
public:
  rb_taclaneImage(){}
  const char* Data() const { return data;}
  int Size() const { return size;}
  operator const char*() { return data;}
  static const char* data;
  static int size;
};
