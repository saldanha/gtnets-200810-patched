// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: interface-wireless.h 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - Interface class
// George F. Riley.  Georgia Tech, Spring 2002

// Define class to model wireless network link interfaces

#ifndef __interface_wireless_h__
#define __interface_wireless_h__

#include "interface-real.h"
#include <string>

using namespace std;

//Doc:ClassXRef
class InterfaceWireless : public InterfaceReal {
  //Doc:Class Class InterfaceWireless extends the functionality of the 
  //Doc:Class basic wired interface to include information  required
  //Doc:Class by the layer  2 wireless protocols.

public:
  typedef enum { IDLE = 0, TX, RX, CX, RX_ME, RX_ZZ } LinkState_t;
  typedef enum {
	NONE = 0, PHY_TXSTART, PHY_TXEND, PHY_RXSTART, PHY_RXEND, PHY_RXEND_ERR
  } PhyInd_t;

  typedef enum {ADHOC, BSS, HOSTAP, ONEHOP} Opmode_t;
  // OneHop mode is used for Link16 non-routed links
public:
  //Doc:Method
  InterfaceWireless(const L2Proto& l2 = L2Proto802_3(),
		    IPAddr_t i        = IPADDR_NONE,
		    Mask_t   m        = MASK_ALL,
		    MACAddr  mac      = MACAddr::NONE,
		    bool bootstrap    = false
		    );
  //Doc:Desc Constructor for InterfaceWireless.
  //Doc:Arg1 Layer 2 protocol for this interface.
  //Doc:Arg2 \IPA\ for this interface.
  //Doc:Arg3 \IPA\ mask for this interface.
  //Doc:Arg4 MAC address
  
  // Notifier
  virtual void Notify(void*);

  // Inherited from interface.h
  // We need to override PeerCount, since for BSS mode this is
  // done differently
  virtual Count_t  PeerCount() const;         // Number of peers

  // Also neighborcount and IPToMac are different for BSS mode and HOSTAP mode
  MACAddr      IPToMac(IPAddr_t) const;           // Return mac addr for ip
  Count_t      NeighborCount(Node*) const;        // Number of rtg neighbors

  //Doc:Method
  LinkState_t GetLinkState() const { return linkState;}
    //Doc:Desc   Get the link state for the wireless link.
    //Doc:Return Link state.

  //Doc:Method
  void        SetLinkState(LinkState_t ls) { linkState = ls;}
    //Doc:Desc Set the link state for the wireless link
    //Doc:Arg1 Link state to set.

  //Doc:Method
  Time_t      GetLinkFreeTime() const {return linkFreeTime;}
    //Doc:Desc Get the link free time for the associated link
    //Doc:Return Time this link will be free

  //Doc:Method
  void        SetLinkFreeTime(Time_t t) {linkFreeTime = t;}
    //Doc:Desc Set a new link free time for the associated link.
    //Doc:Arg1   New time for link free

  LinkEvent*  GetRxPacketEvent() const {return evRx;}
    //Doc:Desc Get the rx event for earliest pending packet receipt event
    //Doc:Return Rx Packet event

  //Doc:Method
  void        SetRxPacketEvent(LinkEvent* p) {evRx = p;}
    //Doc:Desc Set a new rx event for earliest pending packet receipt event
    //Doc:Arg1   New packet rx event

  double      GetRxPower() const {return pr;}
    //Doc:Desc Get the rx power for earliest pending packet receipt
    //Doc:Return Rx Power

  //Doc:Method
  void        SetRxPower(double p) {pr = p;}
    //Doc:Desc Set new rx power for earliest pending packet receipt
    //Doc:Arg1   New packet rx power

  // Override the PacketRx{Start|End} function from InterfaceReal
  virtual void        PacketRxStart(LinkEvent*);
  virtual void        PacketRxEnd(bool, Size_t);
  
  //Doc:Method
  virtual bool     IsWireless() const { return true;}
    //Doc:Desc Test if this interface is wireless 
    //Doc:Return Always true for InterfaceWireless nodes

  void        setTxPower(double p) {txPower = p;}
  double      getTxPower(void) {return txPower;}
  void        setRxPower(double p) {rxPower = p;}
  double      getRxPower(void) {return rxPower;}

  string      getSSID(void) {return ssid;}
  void        setSSID(string ssid_) {ssid = ssid_;}

  void        setRTSTxPower(double p) {rtsTxPow = p;}
  void        setCTSTxPower(double p) {ctsTxPow = p;}
  void        setACKTxPower(double p) {ackTxPow = p;}
  double      getRTSTxPower(void) {return rtsTxPow;}
  double      getCTSTxPower(void) {return ctsTxPow;}
  double      getACKTxPower(void) {return ackTxPow;}

  //Doc:Method
  static void SetDefaultRadioRange(Meters_t);
    //Doc:Desc Specify the default radio range for all wireless interfaces
    //Doc:Arg1 Default range desired

  //Doc:Method
  void        SetOpMode(Opmode_t opmode_);
    //Doc:Desc Set the operational mode for this interface.  Can
    //Doc:Desc be either ADHOC (default),
    //Doc:Desc HOSTAP for host adapter (base station) mode, or
    //Doc:Desc BSS for mobile device seeking base station association.
    //Doc:Arg1 Mode to set.

  //Doc:Method
  Opmode_t    GetOpMode(void) const { return opmode;}
    //Doc:Desc Return the operational mode for this device.
    //Doc:Desc See the possible values above.
    //Doc:Return Operational mode for this device.

  //Doc:Method
  void        SetChannel(Count_t c) { channel = c;}
    //Doc:Desc The channel number allows different wireless transmitters to use
    //Doc:Desc different channels.  The packet transmission broadcast is 
    //Doc:Desc received
    //Doc:Desc only at interfaces with a matching channel number.
    //Doc:Arg1 Channel number to set.

  //Doc:Method
  Count_t     GetChannel() { return channel;}
    //Doc:Desc Returns the channel number for this interface
    //Doc:Return Channel number
  
public:
  Time_t       last_time;     // To keep track of when we
                              // updated energy calculations last  
  bool         bootstrap;     // Do we need bootstrapping functionality
  Opmode_t     opmode;        // Are we an AP or STA or Ad-Hoc
  string       ssid;
  Count_t      channel;       // Channel number for this tx/rx 
  
private:
  Time_t       linkFreeTime;  // Time when link will get free
  LinkEvent*   evRx;          // Receive event for the 1st scheduled pkt
  double       pr;            // rx power of the 1st scheduled pkt
  LinkState_t  linkState;     // Wireless link state
  double       txPower;       // the tx power of this iface
  double       rxPower;       // the rx power level

  double       rtsTxPow;
  double       ctsTxPow;
  double       ackTxPow;

  Meters_t     radioRange;    // Transmit range (meters)
 public:
  static Meters_t defaultRadioRange;
};

#endif

