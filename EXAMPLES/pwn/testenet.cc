// Test program for Ethernet LAN object
// George F. Riley.  Georgia Tech, Spring 2002

#include "simulator.h"
#include "ethernet.h"
#include "tcp-tahoe.h"
#include "duplexlink.h"
#include "ipaddr.h"
#include "mask.h"
#include "application-tcpsend.h"
#include "routing-static.h"
#include "validation.h"

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Count_t enetSize = 10;
  if (argc > 1) enetSize = atol(argv[1]);

  // Set to static routing
  Routing::SetRouting(new RoutingStatic());

  // Create the Simulator object       
  Simulator s; 

  IPV4::Instance()->SetTrace(Trace::ENABLED);        // Enable all l3 tracing
  //L2Proto802_3::Instance()->SetTrace(Trace::ENABLED);// Enable all l2 tracing
  TCP::LogFlagsText(true);    // Log flags in text mode
  TCPTahoe  t;       // TCP endpoint
  t.localPort = 80;  // A HTTP server endpoint for each LAN node
  t.SetTrace(Trace::ENABLED); // Trace information to/from this protocol
  t.Listen();        // Set endpoint in listen state
  // Open the trace file
  Trace::Instance()->Open("testenet.txt");
  // Create the two ethernets with specified number of nodes each
  cout << "Creating subnets of " << enetSize << " nodes each" << endl;
  Node* r1 = new Node();
  Node* r2 = new Node();
  Ethernet  e1(enetSize, r1, IPAddr("208.201.0.1"), Mask(16), t);
  Ethernet  e2(enetSize, r2, IPAddr("208.202.0.1"), Mask(16), t);
  //cout << "Next MAC " << (string)MACAddr::next << endl;
  // Connect the two Ethernets via the gateways
  DuplexLink d(e1.GetNode(0), IPAddr("208.203.0.1"), Mask(24),
               e2.GetNode(0), IPAddr("208.203.0.2"), Mask(24));
  // Set the default routes (needed only for manual routing)
  //e1.GetNode(0)->DefaultRoute(RoutingEntry(d.localif,  e2.GetIP(0)));
  //e2.GetNode(0)->DefaultRoute(RoutingEntry(d.remoteif, e1.GetIP(0)));
  TCPTahoe t1(e1.GetNode(1));  // New TCP endpoint on ethernet 1, node 1
  t1.SetTrace(Trace::ENABLED); // Trace information to/from this protocol
  TCPSend app(e2.GetIP(1), 80,     // Ethernet 2, node 1, port 80
              Constant(10000),         // Send 10000 bytes each time
              t1,                      // TCP object to use
              Constant(5),             // Sleep for 5 seconds
              5);                      // Loop 5 times
  app.Start(1.0);
  s.Progress(1.0);
  s.StopAt(30.0);                      // Stop at time = 10.0 seconds
  s.Run();                             // Run the simulation
  // For debug, find out total size of routing FIB
  Count_t fibSize = 0;
  const NodeVec_t& nodes = Node::GetNodes();
  cout << "Total node count " << nodes.size() << endl;

  for (NodeVec_t::const_iterator i = nodes.begin(); i != nodes.end(); ++i)
    {
      fibSize += (*i)->RoutingFibSize();
    }
  cout << "Size of Routing FIB is " << fibSize<< endl;
}
