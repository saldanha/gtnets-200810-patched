// Test the DiffServ queues
// George F. Riley, Georgia Tech, Fall 2004
 
#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "dumbbell.h"
#include "star.h"
#include "tcp-tahoe.h"
#include "application-cbr.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "udp.h"
#include "validation.h"
#include "queue.h"
#include "diffserv-queue.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

#define N_LEAF 5

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  Count_t nl = N_LEAF; // Number leaf nodes for dumbbell
  Count_t ns = N_LEAF; // Number leaf nodes for each star
  Rate_t  cbrRate = Rate("400Kb");

  if (argc > 1) nl = atol(argv[1]);
  if (argc > 2) ns = atol(argv[2]);
  if (argc > 3) cbrRate = Rate(argv[3]);

  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);

  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->Open("testdiffserve.txt");
  IPV4::Instance()->SetTrace(Trace::ENABLED);
  TCP::LogFlagsText(true);       // Log TCP flags in text mode
  Linkp2p lk(Rate("10Mb"), Time("10ms"));
  Dumbbell b(nl, nl, 0.1,
             IPAddr("192.168.0.1"), IPAddr("192.169.0.1"), lk);
  // Specify the bounding box
  b.BoundingBox(Location(0,0), Location(10,10));

  // Animate the queue on the bottleneck link
  Queue* q = b.LeftQueue();
  Interface* iface = q->interface;
  // Set three priorities with equal sharing of the 1Mb bottleneck link
  iface->SetQueue(DiffServQueue(3, Rate("1Mb")));
  
  // Get the diffserv queue pointer
  DiffServQueue* dq = (DiffServQueue*)b.LeftQueue();
  dq->Animate(true);
  // Set 50% for codepoint 2, 25 percent for 0 and 1
  dq->RateAllocation(0, Rate("1Mb") * 0.25);
  dq->RateAllocation(1, Rate("1Mb") * 0.25);
  dq->RateAllocation(2, Rate("1Mb") * 0.50);
  
  Uniform startRng(0, 0.1); // Random number generator for start times
  // Create the Star networks and assign clients/servers
  Angle_t adderL = -M_PI / (b.LeftCount() + 1.0);
  Angle_t adderR = M_PI / (b.RightCount() + 1.0);
  Angle_t thetaL = -M_PI_2 + adderL;
  Angle_t thetaR = -M_PI_2 + adderR;

#ifdef HAVE_QT
  b.Left()->Color(Qt::blue);
  b.Right()->Color(Qt::blue);
#endif
  for (Count_t i = 0; i < b.LeftCount(); ++i)
    {
      Node* l = b.Left(i);
      Node* r = b.Right(i);
      // And animate the leaf queues
      //Queue* ql = b.LeftQueue(i);
      //ql->Animate(true);

#ifdef HAVE_QT
      l->Color(Qt::blue);
      r->Color(Qt::blue);
#endif
      // Create a star networks with this each leaf as hub
      Star* sl = new Star(ns,l,lk, IPAddr("192.168.1.1")+IPAddr("0.0.1.0")*i);
      Star* sr = new Star(ns,r,lk, IPAddr("192.169.1.1")+IPAddr("0.0.1.0")*i);

      sl->BoundingBox(Location(0,0), Location(2,2), thetaL, M_PI / 2);
      sr->BoundingBox(Location(0,0), Location(2,2), thetaR, M_PI / 2);

      thetaL += adderL;
      thetaR += adderR;

      // Add a cbr application at each left side node
      for (Count_t j = 0; j < sl->LeafCount(); ++j)
        {
          Node* ll = sl->GetLeaf(j); // Left leaf
          Node* rl = sr->GetLeaf(j); // Right leaf
          // Add the CBR application with specified rate
          CBRApplication* cbrApp =
              (CBRApplication*)ll->AddApplication(
                  CBRApplication(rl->GetIPAddr(), 12345, NO_PORT, cbrRate));
          // Add the CBR Sync for tracing
          UDP* sync = new UDP(rl);
          sync->Bind(12345);
          sync->SetTrace(Trace::ENABLED);

#ifdef HAVE_QT
          ll->Color(Qt::yellow);
#endif
#ifdef HAVE_QT
          rl->Color(Qt::green);
          if (j == 0 && i == 0)
            { // Debug..set packets to red for this flow
              cbrApp->GetL4()->SetColor(Qt::red);
              // Set the diffserv codepoint
              cbrApp->GetL4()->TOS(2);
            }
          else if (j == 0 && i == 1)
            { // Debug..set packets to green for this flow
              cbrApp->GetL4()->SetColor(Qt::green);
              // Set the diffserv codepoint
              cbrApp->GetL4()->TOS(1);
            }
#endif
          cbrApp->Start(startRng.Value());
        }
    }

  // Specify animation
  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Progress(1);
  s.StopAt(100);
  s.Run();
  tr->Close();
}
