// Generated automatically by make-gtimage from ../IMAGES/Aircraft.png
// DO NOT EDIT

#include "image.h"

class UAVImage : public Image {
public:
  UAVImage(){}
  const char* Data() const { return data;}
  int Size() const { return size;}
  operator const char*() { return data;}
  static const char* data;
  static int size;
};
