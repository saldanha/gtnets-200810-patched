// Test the SynFlood attack model
// George F. Riley, Georgia Tech, Summer 2003

#include <stdio.h>

#include <iostream>
#include <vector>

#include "simulator.h"
#include "dumbbell.h"
#include "ratetimeparse.h"
#include "application-synflood.h"
#include "application-webserver.h"
#include "linkp2p.h"
#include "validation.h"  

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Count_t   lcount = 10;
  Count_t   rcount = 10;
  if (argc > 1) lcount = atol(argv[1]);
  if (argc > 2) rcount = atol(argv[2]);
  Simulator s;
  Linkp2p   l(Rate("1Mb"), Time("10ms"));
  Dumbbell  d(lcount, rcount, 0.1,
              IPAddr("192.168.0.1"), IPAddr("192.168.2.1"), l);

  // Specify the bounding box
  d.BoundingBox(Location(0,0), Location(10,10));

  Uniform startRng(0, 1.0);
  Uniform stopRng(50.0, 80.0);
  vector<WebServer*> servers;
  for (Count_t r = 0; r < d.RightCount(); ++r)
    { // Add the web server applications
      WebServer* app = (WebServer*)d.Right(r)->AddApplication(WebServer());
      app->EnableLog(); // Log the table size over time
      servers.push_back(app); // Save for later statistics
    }
  for (Count_t l = 0; l < d.LeftCount(); ++l)
    { // Add the syn flood app to the left nodes
      SynFlood* synFlood = (SynFlood*)d.Left(l)->AddApplication(SynFlood());
      // Add each right node as a victim
      for (Count_t r = 0; r < d.RightCount(); ++r)
        {
          synFlood->Victim(d.Right(r)->GetIPAddr());
        }
      // Start and stop the app at random tmies
      synFlood->Start(startRng.Value());
      synFlood->Stop(stopRng.Value()); 
    }
  // Specify animation
  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Progress(1);
  s.StopAt(100.0);
  s.Run();
  for (Count_t i = 0; i < servers.size(); ++i)
    {
      string fn("TimeCountLog.");
      char work[100];
      sprintf(work, "%d", i);
      fn += string(work) + string(".txt");
      ofstream ofs(fn.c_str());
      servers[i]->PrintTimeCount(ofs);
      ofs.close();
    }
  Stats::Print(); // Print the statistics
}
