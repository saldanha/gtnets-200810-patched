#include <iostream>

#include "node.h"
#include "node-taclane.h"
#include "image-gb_taclane.h"
#include "image-rb_taclane.h"
#include "image-rg_taclane.h"
#include "l3prototaclane.h"

using namespace std;

TACLANENode::TACLANENode(IPAddr_t ptipaddr, IPAddr_t ctipaddr,
                         TACLANEType tlt, SystemId_t sysId) 
    : Node(new NodeTACLANEImpl(this, ptipaddr, ctipaddr), sysId),
      enabled(true)
{
  // Create the taclane L3 protocol
  new L3ProtoTACLANE(this, GetIfByIP(ptipaddr), GetIfByIP(ctipaddr));
#ifdef HAVE_QT
  // Set the custom image for animation
  switch(tlt) {
    case RB :
      CustomShapeImage(rb_taclaneImage());
      break;
    case GB :
      CustomShapeImage(gb_taclaneImage());
      break;
    case RG :
      CustomShapeImage(rg_taclaneImage());
      break;
    default:
      break;
  }
#endif
}
