// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: rtt-estimator.cc 92 2004-09-14 18:18:43Z dheeraj $



// Georgia Tech Network Simulator - Round Trip Time Estimation Class
// George F. Riley.  Georgia Tech, Spring 2002

// Implements several variations of round trip time estimators

#include <iostream>

//#define DEBUG_MASK 0x01
#include "debug.h"
#include "rtt-estimator.h"
#include "simulator.h"
#include "statistics.h"

using namespace std;

RTTHistory::RTTHistory(Seq_t s, Count_t c, Time_t t)
    : seq(s), count(c), time(t), retx(false)
{
}

RTTHistory::RTTHistory(const RTTHistory& h)
    : seq(h.seq), count(h.count), time(h.time), retx(h.retx)
{
}

// RTTEstimator Static Member variables
Time_t        RTTEstimator::initialEstimate = 1.0; // Default initial estimate
RTTEstimator* RTTEstimator::defaultRTT = new RTTMDev(0.1, 1.0);
Statistics*   RTTEstimator::globalStats = nil; // No global stats
Mult_t        RTTEstimator::maxMultiplier = 64.0;  // Maximum value of multiplier

// Base class methods

RTTEstimator::RTTEstimator(const RTTEstimator& c)
  : next(c.next), history(c.history), est(c.est), nSamples(c.nSamples),
    multiplier(c.multiplier), privateStats(nil)
{
  if (c.privateStats) privateStats = c.privateStats->Copy();
}

RTTEstimator::~RTTEstimator()
{
  if (privateStats) delete privateStats;
}

void RTTEstimator::SentSeq(Seq_t s, Count_t c)
{ // Note that a particular sequence has been sent
  if (s == next)
    { // This is the next expected one, just log at end
      history.push_back(RTTHistory(s, c, Simulator::Now()));
      next = s + c; // Update next expected
    }
  else
    { // This is a retransmit, find in list and mark as re-tx
      for (RTTHistory_t::iterator i = history.begin(); i != history.end(); ++i)
        {
          if ((s >= i->seq) && (s < (i->seq + i->count)))
            { // Found it
              i->retx = true;
              // One final test..be sure this re-tx does not extend "next"
              if ((s + c) > next)
                {
                  next = s + c;
                  i->count = ((s + c) - i->seq); // And update count in hist
                }
              break;
            }
        }
    }
}

Time_t RTTEstimator::AckSeq(Seq_t a)
{ // An ack has been received, calculate rtt and log this measurement
  // Note we use a linear search (O(n)) for this since for the common
  // case the ack'ed packet will be at the head of the list
  Time_t m = 0.0;
  if (history.size() == 0) return(m);    // No pending history, just exit
  RTTHistory& h = history.front();
  if (!h.retx && a >= (h.seq + h.count))
    { // Ok to use this sample
      m = Simulator::Now() - h.time; // Elapsed time
      DEBUG0((cout << "RTT logging estimate of " << m << " seconds" 
              << " ack " << a << " h.seq " << h.seq 
              << " h.count " << h.count << endl));
      Measurement(m);                // Log the measurement
      ResetMultiplier();             // Reset multiplier on valid measurement
      if (globalStats) globalStats->Record(m);   // Log statistics if present
      if (privateStats) privateStats->Record(m); // Log private stats
    }
  // Now delete all ack history with seq <= ack
  while(history.size() > 0)
    {
      RTTHistory& h = history.front();
      if ((h.seq + h.count) > a) break;                // Done removing
      history.pop_front(); // Remove
    }
  return m;
}

void RTTEstimator::ClearSent()
{ // Clear all history entries
  next = 0;
  history.clear(); 
}

void RTTEstimator::IncreaseMultiplier()
{
  multiplier = min(multiplier * 2.0, maxMultiplier);
}

void RTTEstimator::ResetMultiplier()
{
  multiplier = 1.0;
}

void RTTEstimator::Reset()
{ // Reset to initial state
  next = 0;
  est = initialEstimate;
  history.clear();         // Remove all info from the history
  if(privateStats) privateStats->Reset();   // Clear statistics
  nSamples = 0;
  ResetMultiplier();
}

void RTTEstimator::SetPrivateStatistics(const Statistics& s)
{
  privateStats = s.Copy();
}

  
// Base class, static methods
void RTTEstimator::InitialEstimate(Time_t e)
{ // Set a new default initial estimate
  initialEstimate = e;
  defaultRTT->est = e;
}

void RTTEstimator::Default(const RTTEstimator& rtt)
{
  if (defaultRTT) delete defaultRTT;
  defaultRTT = rtt.Copy();
}

RTTEstimator* RTTEstimator::Default()
{
  return defaultRTT;
}

void RTTEstimator::SetStatistics(Statistics* s)
{
  globalStats = s;
}

// Mean-Deviation Estimator methods

RTTMDev::RTTMDev(const RTTMDev& c)
  : RTTEstimator(c), gain(c.gain), variance(c.variance)
{
}

void RTTMDev::Measurement(Time_t m)
{
  if (nSamples)
    { // Not first
      Time_t err = m - est;
      est = est + gain * err;         // estimated rtt
      if (err < 0) err = -err;        // absolute value of error
      variance = variance + gain * (err - variance); // variance of rtt
    }
  else
    { // First sample
      est = m;                        // Set estimate to current
      //variance = m / 2;               // And variance to current / 2
      variance = m; // try this
    }
  nSamples++;
}

Time_t RTTMDev::RetransmitTimeout()
{
  // If not enough samples, justjust return 2 times estimate   
  //if (nSamples < 2) return est * 2;
  if (variance < est / 4.0)
    return est * 2 * multiplier;            // At least twice current est
  return (est + 4 * variance) * multiplier; // As suggested by Jacobson
}

RTTEstimator* RTTMDev::Copy() const
{
  return new RTTMDev(*this);
}

void RTTMDev::Reset()
{ // Reset to initial state
  variance = 0.0;
  RTTEstimator::Reset();
}
