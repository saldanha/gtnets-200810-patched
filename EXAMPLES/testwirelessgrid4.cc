// Test program for Wireless Grid object
// George F. Riley.  Georgia Tech, Fall 2002

#include <iostream>
#include <stdlib.h>

#include "simulator.h"
#include "wireless-grid-manual.h"
#include "mobility-specific-waypoint.h"
#include "ipaddr.h"
#include "rng.h"
#include "node.h"
#include "ratetimeparse.h"
#include "validation.h"  

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  WirelessGridManual g(Location(10,10), Location(20,20), IPADDR_NONE);
  Node* n1 = g.AddNode(Location(10,10));
  SpecificWaypoint* m1 =
    (SpecificWaypoint*)n1->AddMobility(SpecificWaypoint());
  m1->AddWaypoint(n1, Waypoint(1,  Location(10,10))); // Stationary till time 1
  m1->AddWaypoint(n1, Waypoint(10, Location(10,20)));
  m1->AddWaypoint(n1, Waypoint(20, Location(10,20)));
  m1->AddWaypoint(n1, Waypoint(30, Location(20,20)));
  m1->AddWaypoint(n1, Waypoint(40, Location(20,20)));
  m1->AddWaypoint(n1, Waypoint(50, Location(20,10)));
  m1->AddWaypoint(n1, Waypoint(60, Location(20,10)));
  //m1->AddWaypoint(n1, Waypoint(70, Location(10,10))); // Last loc == first
  m1->SetLooping(true);
  
  Node* n2 = g.AddNode(Location(10,20));
  SpecificWaypoint* m2 =
    (SpecificWaypoint*)n2->AddMobility(SpecificWaypoint());
  m2->AddWaypoint(n2, Waypoint(1,  Location(10,20)));
  m2->AddWaypoint(n2, Waypoint(10, Location(20,20)));
  m2->AddWaypoint(n2, Waypoint(20, Location(20,20)));
  m2->AddWaypoint(n2, Waypoint(30, Location(20,10)));
  m2->AddWaypoint(n2, Waypoint(40, Location(20,10)));
  m2->AddWaypoint(n2, Waypoint(50, Location(10,10)));
  m2->AddWaypoint(n2, Waypoint(60, Location(10,10)));
  //m2->AddWaypoint(n2, Waypoint(70, Location(10,20)));
  m2->SetLooping(true);
  

  Node* n3 = g.AddNode(Location(20,20));
  SpecificWaypoint* m3 =
    (SpecificWaypoint*)n3->AddMobility(SpecificWaypoint());
  m3->AddWaypoint(n3, Waypoint(1,  Location(20,20)));
  m3->AddWaypoint(n3, Waypoint(10, Location(20,10)));
  m3->AddWaypoint(n3, Waypoint(20, Location(20,10)));
  m3->AddWaypoint(n3, Waypoint(30, Location(10,10)));
  m3->AddWaypoint(n3, Waypoint(40, Location(10,10)));
  m3->AddWaypoint(n3, Waypoint(50, Location(10,20)));
  m3->AddWaypoint(n3, Waypoint(60, Location(10,20)));
  //m3->AddWaypoint(n3, Waypoint(70, Location(20,20)));
  m3->SetLooping(true);

  Node* n4 = g.AddNode(Location(20,10));
  SpecificWaypoint* m4 =
    (SpecificWaypoint*)n4->AddMobility(SpecificWaypoint());
  m4->AddWaypoint(n4, Waypoint(1,  Location(20,10)));
  m4->AddWaypoint(n4, Waypoint(10, Location(10,10)));
  m4->AddWaypoint(n4, Waypoint(20, Location(10,10)));
  m4->AddWaypoint(n4, Waypoint(30, Location(10,20)));
  m4->AddWaypoint(n4, Waypoint(40, Location(10,20)));
  m4->AddWaypoint(n4, Waypoint(50, Location(20,20)));
  m4->AddWaypoint(n4, Waypoint(60, Location(20,20)));
  //m4->AddWaypoint(n4, Waypoint(70, Location(20,10)));
  m4->SetLooping(true);

  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      //s.PauseAnimation(99);
      s.AnimationUpdateInterval(Time("1ms")); // 1ms initial update rate
    }
  s.StopAt(1000);
  s.Run();
}
