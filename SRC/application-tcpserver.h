// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-tcpserver.h 92 2004-09-14 18:18:43Z dheeraj $



// Georgia Tech Network Simulator - TCP Server Application class
// George F. Riley.  Georgia Tech, Summer 2002

// Define a TCP Server Application Class
// Just sends back the amount of data specified in datapdu, "responseSize"

#ifndef __tcp_appserver_h__
#define __tcp_appserver_h__

#include "common-defs.h"
#include "application-tcp.h"
#include "statistics.h"
#include "tcp.h"
#include "rng.h"

//Doc:ClassXRef
class TCPServer : public TCPApplication {
  //Doc:Class Class {\tt TCPServer} creates a simple model of a
  //Doc:Class request/response
  //Doc:Class {\tt TCP} server.  The server binds to a specified port, and
  //Doc:Class listens for connection requests from {\tt TCP} peers.  The 
  //Doc:Class data received from the peers specifies how much data to send
  //Doc:Class or receive.

public:
  // Constructor
  //Doc:Method
  TCPServer(const TCP& = TCP::Default());   // TCP Type
    //Doc:Desc Constructor for {\tt TCPServer} objects.  Optionally
    //Doc:Desc specify a reference to any {\tt TCP} variant to use
    //Doc:Desc for the listening {\tt TCP} endpoint.
    //Doc:Arg1 A reference to any {\tt TCP} variant to use for
    //Doc:Arg1 the listening endpoint.

  //Doc:Method
  TCPServer(const TCPServer& c);               // Copy constructor
    //Doc:Desc Copy constructor.
    //Doc:Arg1 {\tt TCPServer} object to copy.

  // Upcalls from L4 protocol
  virtual void Receive(Packet*,L4Protocol*,Seq_t);   // Data received
  virtual void Sent(Count_t, L4Protocol*);     // Data has been sent
  virtual void CloseRequest(L4Protocol*);      // Close request from peer
  virtual void Closed(L4Protocol*);            // Connection has closed

  // Specific to TCPServer app
  //Doc:Method
  virtual void BindAndListen(Node*, PortId_t); // Bind to node/port
    //Doc:Desc Bind the application to the specified port and start 
    //Doc:Desc listening for connections.
    //Doc:Arg1 Node pointer to the node this application is associated with.
    //Doc:Arg2 Port number to bind to.

  //Doc:Method
  virtual void BindAndListen(PortId_t); // Bind to node/port
    //Doc:Desc Bind the application to the specified port and start 
    //Doc:Desc listening for connections.  Node is already known.
    //Doc:Arg1 Port number to bind to.

  virtual Application* Copy() const;           // Make a copy of application
  //Doc:Method
  void SetStatistics(Statistics* s) { stats = s;}
    //Doc:Desc Set a statistics object to use for statistics collection.
    //Doc:Desc This application will record the response time of each
    //Doc:Desc message sent to the statistics object.
    //Doc:Arg1 Pointer to a statistics object.

public:
  //Doc:Member
  Count_t       totSent;      // Total bytes sent
    //Doc:Desc Count of the total number of bytes sent by this application.

  //Doc:Member
  Count_t       totAck;       // Total bytes acknowledged
    //Doc:Desc Count of total bytes acknowledged by this application.

  //Doc:Member
  Count_t       totRx;        // Total bytes received
    //Doc:Desc Count of total bytes received by this application.
  Statistics*   stats;        // Stats object for logging response time

  //Doc:Member
  Time_t        msgStart;     // Time message request started
    //Doc:Desc Time the last message started transmission or receipt.

  //Doc:Member
  bool          timeKnown;    // True if above time is known
    //Doc:Desc True if the start time of the message is known.
};

#endif

