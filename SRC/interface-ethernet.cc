// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: interface-ethernet.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - InterfaceEthernet class
// Mohamed Abd Elhafez.  Georgia Tech, Spring 2004

// Define class to model network link interfaces
// Implements the Layer 2 protocols

#include <iostream>

//#define DEBUG_MASK 0x01
#include "debug.h"
#include "hex.h"
#include "interface-ethernet.h"
#include "link.h"
#include "queue.h"
#include "droppdu.h"
#include "node.h"
#include "trace.h"
#include "globalstats.h"
#include "ipv4.h"
#include "llcsnap.h"
#include "ratetimeparse.h"

using namespace std;

InterfaceEthernet::InterfaceEthernet(const L2Proto& l2, IPAddr_t i,
				     Mask_t m, MACAddr mac,
				     bool bootstrap)  
  : InterfaceReal(l2, i, m, mac), busyendtime(0), holdtime(0),
    maxbackoff(INITIAL_BACKOFF), backofftimer(0), maxwaittime(0),
    busycount(1), lastpacketsent(0), lastrxevent(0), lastchanacq(0), 
    lasttransmit(0), tx_finishtime(0), bCast(false), collision(false),
    slottime(0), rtime(0)
{ 
  generator =  new  Uniform(0, INITIAL_BACKOFF);
}

InterfaceEthernet::~InterfaceEthernet()
{
  if(lastpacketsent)
    delete lastpacketsent;
  if(lastrxevent)
    delete lastrxevent;
  if(lastchanacq)
    delete lastchanacq;
 if(lasttransmit)
    delete lasttransmit;
  if(generator)
    delete generator;
}

void InterfaceEthernet::RegisterRxEvent(LinkEvent* le)
{

  lastrxevent = le;
}

void InterfaceEthernet::AddRxEvent(LinkEvent* le)   // add to the list of rx events , used in case of broadcast
{
  rxEvents.push_back(le);
}

Time_t   InterfaceEthernet::SlotTime()
{
    slottime = SLOT_TIME / GetLink()->Bandwidth();
    return slottime;
}

bool InterfaceEthernet::SenseChannel()
{
  if (Simulator::Now() < tx_finishtime || Simulator::Now() < holdtime )  // we are already transmitting
    {
      // Resend when we finish the current transmission
      Time_t temp = tx_finishtime > holdtime ? tx_finishtime : holdtime; 
      
      rtime = temp - Simulator::Now();
      return false;
    }
  if  (Simulator::Now() < busyendtime)  // The link is busy
    {
      // Resense the channel after freetime 
      rtime =  busyendtime - Simulator::Now();
      return false;
    }
  collision = false;
  return true;
      
}


bool InterfaceEthernet::Retransmit(Packet* p)
{
  //  cout << "Retransmit::Queued packets = " << GetQueue()->LengthPkts() << ", at Node " << pNode->Id()<< endl;

  // Tracing
  if(GetL2Proto()->GetTrace() == Trace::ENABLED)
    {
      Trace::Instance()->NewNode(pNode);
      Trace::Instance()->GetTS() << " L2-RA " << p->retx << " " << p->uid;
    }

  if(SenseChannel())
    {
      for(NodeTimeVec_t::size_type  i = 0; i < nodeTimes.size(); ++i)
	{
	  EthernetEvent* evFBR = new EthernetEvent(EthernetEvent::FBR, p->Size());
	  DEBUG0((cout << "Scheduling FBR to node " <<  nodeTimes[i].iface->GetNode()->Id()
		  << " after " << nodeTimes[i].time 
		  << " current time " <<  Simulator::Now() << endl)); 
	  Scheduler::Schedule(evFBR, nodeTimes[i].time , nodeTimes[i].iface);
	}

      // check for broadcast FIX THIS
      bCast = false;

      Time_t txTime = ((Rate_t)p->Size() * 8) / GetLink()->Bandwidth() ; // Time to transmit

      // Count as enque/deque to get proper queue statistics
      Stats::pktsEnqued++;
      Stats::pktsDequed++;     
      
      // cout << "packet size = " << p->Size() << ", Bandwidth = " << GetLink()->Bandwidth() << ", txTime = " << txTime << endl;
      tx_finishtime = Simulator::Now() + txTime ;       // Free time for next packet
  
      holdtime = tx_finishtime + INTER_FRAME_GAP / GetLink()->Bandwidth(); 
      
      rtime = holdtime - Simulator::Now();
	
       if(!lasttransmit && GetQueue()->LengthPkts() > 0)
 	{
 	  lasttransmit = new EthernetEvent(EthernetEvent::RETRANSMIT);      
 	  Scheduler::Schedule(lasttransmit, holdtime - Simulator::Now(), this);
	  // cout << "rtime = " << rtime << endl;
 	}

       lastpacketsent = p->Copy();

       /* the queue can check if it is full and drop the packet */
       GetL2Proto()->DataRequest(lastpacketsent);
       
       if(!lastchanacq)
	 lastchanacq = new EthernetEvent(EthernetEvent::CHAN_ACQ);
      
       // Schedule the channel acquired event
       Scheduler::Schedule(lastchanacq, 2 * maxwaittime, this);

       //  cout << "Retransmit::Sending Queued packets = " << GetQueue()->LengthPkts() << ", at Node " << pNode->Id()<< endl;
       delete p;
   
       return true;
    }

  if(!lasttransmit)
    {
      lasttransmit = new EthernetEvent(EthernetEvent::RETRANSMIT);      
      Scheduler::Schedule(lasttransmit, rtime, this);
    }

  --p->retx;
  // Tracing
  if(GetL2Proto()->GetTrace() == Trace::ENABLED)
    {
      Trace::Instance()->NewNode(pNode);
      Trace::Instance()->GetTS() << " L2-B " << p->uid;
    }     
	  

  if (!GetQueue()->Enque(p))
    {
      DropPDU d("L2-QD", p);
      pNode->TracePDU(nil, &d);
      Stats::pktsDropped++; // Count dropped packets
      DEBUG0((cout << "Can't enque, Interface " << this << " dropping pkt" << endl));
      delete p;     // Delete the packet
      return false; // Dropped
    }
  //cout << "Retransmit::Queueing Queued packets = " << GetQueue()->LengthPkts() << ", at Node " << pNode->Id()<< endl;

  return true;
}


bool InterfaceEthernet::Send(Packet* p, const MACAddr& dst, int type)
{
  if(!p) return false;

 
  DEBUG0((cout << " calling send from interface ethernet of node " << pNode->Id() << " for packet " << p->uid << endl));

  LLCSNAPHeader* llc = new LLCSNAPHeader(type);
  p->PushPDU(llc);
  
  GetL2Proto()->BuildDataPDU(macAddr, dst, p);

  if(lasttransmit) 
    {
      Scheduler::Cancel(lasttransmit);
      delete lasttransmit;
      
      lasttransmit = nil;
    }


  if(SenseChannel())
    {
         
      //      cout << "packet size = " << p->Size() << ", Bandwidth = " << GetLink()->Bandwidth() << ", txTime = " << txTime << endl;
      if ( !GetQueue()->CheckForcedLoss(false))
	{ // Link is free, and no forced losses presently for the queue
	  // schedule FBR to all neighbours	
	  for(NodeTimeVec_t::size_type  i = 0; i < nodeTimes.size(); ++i)
	    {
	      EthernetEvent* evFBR = new EthernetEvent(EthernetEvent::FBR, p->Size());
	      DEBUG0((cout << "Scheduling FBR to node " <<  nodeTimes[i].iface->GetNode()->Id()
		      << " after " << nodeTimes[i].time 
		      << " current time " <<  Simulator::Now() << endl)); 
	      Scheduler::Schedule(evFBR, nodeTimes[i].time , nodeTimes[i].iface);
	    }

	  bCast = dst.IsBroadcast();
	  
	  // Count as enque/deque to get proper queue statistics
	  Stats::pktsEnqued++;
	  Stats::pktsDequed++;     

	  Time_t txTime = ((Rate_t)p->Size() * 8) / GetLink()->Bandwidth(); // Time to transmit

	  // cout << "packet size = " << p->Size() << ", Bandwidth = " << GetLink()->Bandwidth() << ", txTime = " << txTime << endl;
	  tx_finishtime = Simulator::Now()  + txTime ;       // Free time for next packet

	  holdtime = tx_finishtime + INTER_FRAME_GAP / GetLink()->Bandwidth();

	  if(!lasttransmit  && GetQueue()->LengthPkts() > 0)
	    {
	      lasttransmit = new EthernetEvent(EthernetEvent::RETRANSMIT);      
	      Scheduler::Schedule(lasttransmit, holdtime - Simulator::Now(), this);
	    }
	  // Tracing
	  if(GetL2Proto()->GetTrace() == Trace::ENABLED)
	    {
	      Trace::Instance()->NewNode(pNode);
	      Trace::Instance()->GetTS() << " L2-RA " << p->retx  << " " << p->uid;
	    }     
	  
	  lastpacketsent = p->Copy();

	  //  cout << "Send::Sending Queued packets = " << GetQueue()->LengthPkts() << ", at Node " << pNode->Id()<< endl;

	  GetL2Proto()->DataRequest(lastpacketsent);
	  
	  if(!lastchanacq)
	    lastchanacq = new EthernetEvent(EthernetEvent::CHAN_ACQ);

	  // Schedule the channel acquired event
	  Scheduler::Schedule(lastchanacq, 2 * maxwaittime, this);

	  delete p;
	  return true;
	}      
    }

  if(!lasttransmit)
    {
      lasttransmit = new EthernetEvent(EthernetEvent::RETRANSMIT);      
      Scheduler::Schedule(lasttransmit, rtime, this);
    }
  --p->retx;

  if (!GetQueue()->Enque(p))
    {
      DropPDU d("L2-QD", p);
      pNode->TracePDU(nil, &d);
      Stats::pktsDropped++; // Count dropped packets
      DEBUG0((cout << "Can't enque, Interface " << this << " dropping pkt" << endl));
      delete p;     // Delete the packet
      return false; // Dropped
    }
  //cout << "Send::Queueing Queued packets = " << GetQueue()->LengthPkts() << ", at Node " << pNode->Id()<< endl;


  return true;
}

  
// Handler methods
void InterfaceEthernet::Handle(Event* e, Time_t t)
{ 
 
  EthernetEvent* ee = (EthernetEvent*)e; // Convert to right event type

  switch (ee->event) {
  
  case EthernetEvent::PACKET_RX :
    DEBUG0((cout << "Packet rx Event at Node " << pNode->Id() << ", time " << t << " packet " <<  ee->p->uid << " Euid " << ee->uid << endl));
    //  // notify sender that it has acquired the chanel
    //     l2pdu = (L2802_3Header*)ee->p->PeekPDU(); // Get a ptr to l2 hdr
    //     src_if = ((Ethernet*)GetLink())->GetIfByMac(l2pdu->src);
    //     Scheduler::Schedule(new EthernetEvent(EthernetEvent::PACKET_TX), 0.0, src_if);
    InterfaceReal::Handle(e, t);
    ee = nil;    // do not delete the event here
    break;
  case EthernetEvent::NOTIFY :
    InterfaceReal::Handle(e, t);
    ee = nil;   // do not delete the event here
    break;
  case  EthernetEvent::FBR :
    DEBUG0((cout << "FBR Event at Node " << pNode->Id()
            << " for mac " << ee->mac.macAddr
            << ", time " << t << " Euid " << e->uid << endl));
    HandleFBR(ee, t);
    break;
    
  case EthernetEvent::CLR :  // wait for all sending stations to send the clear signal then set the link status to not busy
    DEBUG0((cout << "CLR Event at Node " << pNode->Id() << ", time " << t << endl));
   
    if (--busycount <= 0)
      {
	busycount = 1;
	busyendtime = t;

	if(!collision)
	  {
	    holdtime = busyendtime + JAM_TIME / GetLink()->Bandwidth();

	    if(lasttransmit) 
	      {
		Scheduler::Cancel(lasttransmit);
		delete lasttransmit;
		
		lasttransmit = nil;
		ee=nil;
	      }
	    
	    if(!lasttransmit)
	      {
		lasttransmit = new EthernetEvent(EthernetEvent::RETRANSMIT);
		Scheduler::Schedule(lasttransmit, holdtime - Simulator::Now(), this);
	      }
	  }
      }
    break;
  case EthernetEvent::RETRANSMIT :  // Retransmit the packet  
 
    DEBUG0((cout << "RETRANSMIT Event at Node " << pNode->Id()
            << ", time " << t << " destination " << ee->mac.macAddr << endl));
    //cout << "Retransmission " << ee->p->retx <<  " for packet " << ee->p->uid << endl;
    if(lasttransmit) 
      {
	Scheduler::Cancel(lasttransmit);
	delete lasttransmit;
	
	lasttransmit = nil;
	ee=nil;

      }

    lastpacketsent = GetQueue()->Deque();

    if (lastpacketsent)
      { // Packet exists
	
	if( ++lastpacketsent->retx > ATTEMPT_LIMIT)  // if we have reached the retransmission limit then drop the packet
	  {
	    //	DropPDU d("L2-QD", lastpacketsent);
	    DropPDU d("L2-QD", lastpacketsent);
	    pNode->TracePDU(nil, &d);
	    Stats::pktsDropped++; // Count dropped packets
	    DEBUG0((cout << "Rx Limit reached, at Node " <<  pNode->Id() << " dropping pkt "  <<  ee->p->uid << endl));	  
	    //	    droppedpkts++;

	    if(GetQueue()->LengthPkts() > 0)
	      {
		lasttransmit = new EthernetEvent(EthernetEvent::RETRANSMIT);
		Scheduler::Schedule(lasttransmit, 0.0, this);
	      }

	    return; // Dropped
	  }

	Retransmit(lastpacketsent);  
      }
 
    break;
 
  case EthernetEvent::UPDATE_NODES : // update the list of nodes and their delays
    DEBUG0((cout << "UPDATE_NODES Event at Node " << pNode->Id() << ", time " << t << endl));
    holdtime = INTER_FRAME_GAP / GetLink()->Bandwidth(); // interframe gap
    NeighbourList(nodeTimes);
    break;
  case EthernetEvent::CHAN_ACQ :  // We have succesfully sent the packet so reset the contention window 
    DEBUG0((cout << "Channel acquired Event at Node " << pNode->Id() << ", time " << t << endl));
    maxbackoff = INITIAL_BACKOFF;
    ee = nil;
  }
  if (ee) delete ee;
}

  
// Private  methods
void  InterfaceEthernet::NeighbourList(NodeTimeVec_t& ntv)
{ // Make a list of all neighbors. 
  Count_t esize  =  ((Ethernet*)GetLink())->Size();

  maxwaittime = 0;
  Time_t temp = 0;

  for (NodeId_t ni = 0; ni < esize; ++ni)
    {
      Interface* iface =  ((Ethernet*)GetLink())->GetIf(ni);
      if (!iface) cout << "HuH?  InterfaceEthernet::NeighborList nill iface for " 
                       << ni << endl;
      else
	{
	  Node* n = iface->GetNode();
	  if(n != pNode) // interface exists and it's node is not this one
	    {
	      temp = TimeDelay(n);
	      ntv.push_back(NodeIfTime(n, iface, temp ) );
	      maxwaittime = maxwaittime > temp ? maxwaittime : temp;
	    }
	}
    }
 
  if(!slottime) slottime = SLOT_TIME / GetLink()->Bandwidth();  // calculate the slot time in seconds
}



Time_t InterfaceEthernet::TimeDelay(Node* n)
{
  return pNode->Distance(n) / SPEED_LIGHT;
}



void InterfaceEthernet::HandleFBR(EthernetEvent* e, Time_t t)
{
   if (t < tx_finishtime) // collision occured
    {
      //      busycount++;	      
      //busyendtime = INFINITE_TIME;   // stay busy until you get the Clear event

      if(collision) return; // if we have already experienced collision then do nothing
      collision = true;
      // cancel the rx event(s)
      if (bCast)
	{
	  for (LinkEventVec_t::size_type i = 0; i < rxEvents.size(); ++i)
	    {
	      Scheduler::Cancel(rxEvents.front());
	      rxEvents.erase(rxEvents.begin());
	    }
	  //bCast = false;
	}
      else
	{
	  //cout << "Cancelling the rx event for packet " << lastrxevent->p->uid << endl;
	  //cout << "Cancelling the rx event  " << lastrxevent->uid << " for time " << lastrxevent->Time()<< endl;
	  if(lastrxevent)
	    {
	      Scheduler::Cancel(lastrxevent);
	      delete lastrxevent;
	      lastrxevent = nil;
	    }
	}

      DEBUG0((cout << "Collison occured sending clear event to all nodes " << pNode->Id() << ", time " << t << endl));      
      
      // Tracing
      if(GetL2Proto()->GetTrace() == Trace::ENABLED)
	{
	  Trace::Instance()->NewNode(pNode);
	  Trace::Instance()->GetTS() << " L2-C " << lastpacketsent->uid;
	}
      // Canceling the acquire channel event
      Scheduler::Cancel(lastchanacq);

      // schedule CLR to all neighbours	
      for(NodeTimeVec_t::size_type  i = 0; i < nodeTimes.size(); ++i)
	{
	  EthernetEvent* evCLR = new EthernetEvent(EthernetEvent::CLR);
	  Scheduler::Schedule(evCLR, nodeTimes[i].time , nodeTimes[i].iface);
	}
      
      // set the new value for the backoff timer and schedule sensing the chanell after the backoff
      tx_finishtime = t; 

      maxbackoff *= 2;
      
      if(maxbackoff > BACKOFF_LIMIT)   maxbackoff = BACKOFF_LIMIT; 
      
      backofftimer =  ceil(generator->Value() * maxbackoff) * slottime;

       // hold all packets back
      holdtime =  backofftimer +  JAM_TIME/ GetLink()->Bandwidth() + tx_finishtime; // Jam time;

      // DEBUG0((cout << "Rescheduling packet for transmit at " << holdtime
      //        << " for "<< lastdest.macAddr << endl));

      if (!GetQueue()->Enque(lastpacketsent))
	{
	  DropPDU d("L2-QD", lastpacketsent);
	  pNode->TracePDU(nil, &d);
	  Stats::pktsDropped++; // Count dropped packets
	  DEBUG0((cout << "Can't enque, Interface " << this << " dropping pkt" << endl));
	  delete lastpacketsent;     // Delete the packet
	}

      if(lasttransmit) 
	{
	  Scheduler::Cancel(lasttransmit);
	  delete lasttransmit;
	  lasttransmit = nil;
	}
      lasttransmit = new EthernetEvent(EthernetEvent::RETRANSMIT);
      Scheduler::Schedule(lasttransmit, holdtime - Simulator::Now(), this);
     
      return;
    }
                  
  if (t < busyendtime) // external collision
    {	  
      busycount++;	      
      busyendtime = INFINITE_TIME;   // stay busy until you get the Clear event
      return;
    }      

  //   Set the interface mode to busy until the other node finishes its transmission
  Time_t txTime = ((Rate_t)e->size * 8)  / GetLink()->Bandwidth(); // Time to transmit

  busyendtime = t + txTime; // Note time link will be free
  holdtime = busyendtime +  INTER_FRAME_GAP / GetLink()->Bandwidth();

  if(!lasttransmit && GetQueue()->LengthPkts() > 0 )
   {
       lasttransmit = new EthernetEvent(EthernetEvent::RETRANSMIT);
       Scheduler::Schedule(lasttransmit, holdtime - Simulator::Now(), this);
   }
}

   
   
void InterfaceEthernet::Notify(void* v)
{
  return;
}   
