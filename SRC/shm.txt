Shared memory, NER Version
TM-Stastics
-----------
NEpochs=             811760
NLBTS=               811760
Tot-trials=          811760
Max-trials-per-LBTS= 1
Avg-trials-per-LBTS= 1.000000
Time-per-trial=         4046.35270523 microsecs
Time-per-LBTS=          4046.35270523 microsecs
NLBTS-per-sec=           247.13614281
Total events processed 211365064
Total pkt-hops 105672252
Setup time 1.24
Route time 0
Run time 3113.53
Total time 3114.77
Pkt-hops/sec 33926.2


Shared memory, TAR Version
TM-Stastics
-----------
NEpochs=             201
NLBTS=               201
Tot-trials=          201
Max-trials-per-LBTS= 1
Avg-trials-per-LBTS= 1.000000
Time-per-trial=      13247769.73631853 microsecs
Time-per-LBTS=       13247769.73631853 microsecs
NLBTS-per-sec=             0.07548440
-----------
Total events processed 211314764
Total pkt-hops 105647102
Setup time 0.77
Route time 0
Run time 2657.8
Total time 2658.57
Pkt-hops/sec 39738.3

