
//
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
//
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
//
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
//
// 1.  This License allows you to:
//
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
//
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//
//
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
//
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
//
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
//
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
//
//
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
//
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
//
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
//
//
// 3.  Export Law Assurance.
//
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//
// 4.  Termination.
//
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
//
//
// 5.  Disclaimer of Warranties and Limitation on Liability.
//
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
//
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
//
// GTRC shall have no obligation for support or maintenance of Program.
//
// 6.  Copyright Notice.
//
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
//
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
//
// Said copyright notice shall read as follows:
//
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: l2proto802.11.cc 514 2007-10-02 13:18:43Z johnece $



// Georgia Tech Network Simulator - Layer 2 802.3 class

// Implementation for 802.11

#include <iostream>

#include "debug.h"
#include "ieee802.11.h"
#include "l2proto802.11.h"
#include "interface.h"
#include "ipv4.h"
#include "trace.h"
#include "node.h"
#include "hex.h"
#include "macaddr.h"
#include "queue.h"
#include "link.h"
#include "wlan.h"
#include "macaddr.h"
#include "droppdu.h"

#include <signal.h>

// Uncomment below to enable debug level 0
//#define DEBUG_MASK 0x01

#define MYIP	((string)iface->GetIPAddr())
#define areEqual(a,b) (fabs(a-b) < PRECISION )

// debug, time in microseconds
#define TIMEUS ((int)(Simulator::Now()*1000000))


using namespace std;

// debug  stats
static Count_t backoffCountByCW[DSSS_CWMax + 1];
static Count_t medBusy = 0;
static Count_t vcsBusy = 0;

void PrintBackoffStats()
{
  Count_t total = 0;
  for (Count_t i = 0; i <= DSSS_CWMax; ++i)
    {
      if (backoffCountByCW[i])
        { // Value is non-zero
          cout << "Bin " << i
               << " backoffCount " << backoffCountByCW[i] << endl;
          total += backoffCountByCW[i];
        }
    }
  cout << "Total   " << total << endl;
  cout << "medBusy " << medBusy << endl;
  cout << "vcsBusy " << vcsBusy << endl;
  cout << "durationRx    " << L2Proto802_11::durationRx << endl;
  cout << "navTimerCount " << L2Proto802_11::navTimerCount << endl;
  cout << "totPacketRx   " << L2Proto802_11::totPacketsRx << endl;
}

// Default setting
Word_t L2Proto802_11::defaultSeqCacheSize = SEQ_CACHE_SIZE;

Random* L2Proto802_11::backoffRng = nil;
Rate_t  L2Proto802_11::defaultBasicRate = Rate("11Mb");
Rate_t  L2Proto802_11::defaultDataRate = Rate("11Mb");

Count_t L2Proto802_11::rtsCount = 0;
Count_t L2Proto802_11::ctsCount = 0;
Count_t L2Proto802_11::dataCount = 0;
Count_t L2Proto802_11::ackCount = 0;
Count_t L2Proto802_11::rtsDelCount = 0;
Count_t L2Proto802_11::ctsDelCount = 0;
Count_t L2Proto802_11::dataDelCount = 0;
Count_t L2Proto802_11::ackDelCount = 0;
Count_t L2Proto802_11::beaconCount = 0;
Count_t L2Proto802_11::beaconDelCount = 0;
Count_t L2Proto802_11::assocReqCount = 0;
Count_t L2Proto802_11::assocRepCount = 0;
Count_t L2Proto802_11::disassocCount = 0;
Count_t L2Proto802_11::ctsTOCount = 0;
Count_t L2Proto802_11::ackTOCount = 0;
Count_t L2Proto802_11::navTimerCount = 0;
Count_t L2Proto802_11::durationRx = 0;
Count_t L2Proto802_11::totPacketsRx = 0;

Count_t L2Proto802_11::RTSTHRESHOLD=0;

L2Proto802_11::L2Proto802_11() : L2Proto(), basicRate(defaultBasicRate),
				 dataRate(defaultDataRate)
{ // Constructor
  navTimer = 0.0;
  backoffTimer = 0.0;
  cw = PMIB.CWMin;
  sifs = PMIB.SIFSTime;
  difs = sifs + 2.0*PMIB.SlotTime;
  eifs = sifs + difs + (8 * ETHER_ACK_LEN / PMIB.PLCPDataRate);
  // basicRate = defaultbasicRate;
  // dataRate =  defaultdataRate;
  //dataRate = 11000000.0;
  //dataRate = iface->pLink->Bandwidth();    // iface not available here
  shortRetryCount = 0;
  longRetryCount = 0;
  pendingPkt = nil;
  navTimeout = nil;
  backoffTimeout = nil;
  ctsTimeout = nil;
  ackTimeout = nil;
  dataTimeout = nil;
  associationTimeout = nil;
  beaconTimeout = nil;
  mac_state = IDLE;
  beaconRxCount = 0;
  xmitFailHandler = nil;
  mevFirstAttempt = false;
  mevBackoff = nil;
  backingOff = false;
  backoffTimerState = NONE;
  firstAttempt = true;  // very 1st pkt in a session
  nextCacheEntry = 0;   // sequence cache
  seqNo = 0;            // data frame sequence number
  ibfTime = 100;        // 100 * 1024 usecs; the std is ambiguous about it
  associated = false;
  associationPending = false;
  currDist = INFINITE_VALUE;
  lastBeacon = 0;
  pendingBeacon = false;
  pendingAssocReq = nil;
  pendingAssocResp = nil;
  pendingDissoc = nil;
  cfp_dur = 0;
  inCfp= false;
  failCount = 0;
  RTSThreshold=RTSTHRESHOLD;
}

L2Proto802_11::~L2Proto802_11()
{
  CancelTimer(backoffTimeout, true);
  CancelTimer(navTimeout, true);
  CancelTimer(ctsTimeout, true);
  CancelTimer(ackTimeout, true);
  CancelTimer(dataTimeout, true);
  CancelTimer(associationTimeout, true);
  CancelTimer(beaconTimeout, true);
}

void L2Proto802_11::BuildDataPDU(MACAddr src, MACAddr dst, Packet* p)
{
  Word_t dur = usec(sifs + txtime(ETHER_ACK_LEN, basicRate)
		    + DSSS_MaxPropagationDelay);
//   if(dst.macAddr==0) {
//     cout <<"PANIC" << endl;
//     exit(1);
//  }
  if ( ((InterfaceWireless*)iface)->GetOpMode()
       == InterfaceWireless::BSS ) {
    if ((dst.macAddr==0) && (associated == true)) { /* XXX - Unexplained */
      cout << "RECORD DIFF " << endl;
      dst = assocBssid;
    }
  }
  DATA_frame* l2pdu = new DATA_frame(dur, dst, src, ++seqNo, 0);
  p->PushPDU(l2pdu);
}

void L2Proto802_11::DataRequest(Packet* p)
{
  static Count_t totalDR = 0;
  DEBUG0((cout << "node " << iface->GetNode()->Id()
          << " 802.11 DR, count " << ++totalDR
          << " ql " << iface->GetQueue()->LengthPkts()
          << " assoc " << associated
          << endl));
  DEBUG(1,(cout << "node " << iface->GetNode()->Id()
           << " 802.11 DR, time " << TIMEUS
           << " backingOff " << backingOff
           << " pcsBusy " << PcsBusy()
           << " vcsBusy " << VcsBusy()
           << " NAV " << navTimer
           << endl));
  
  // First, if we are in BSS or HOSTAP mode, make sure we have
  // any associations
  InterfaceWireless* ifw = (InterfaceWireless*)iface;
  bool drop = false;
  firstAttempt=true;
  DATA_frame* df = (DATA_frame*)p->PeekPDU();

  if (!df->ra.IsBroadcast())
    { // If not broadcast, check if we can send this
      switch (ifw->GetOpMode()) {
        case InterfaceWireless::BSS:
	/* XXX - Unexplained
	if ((df->ra == MACAddr(MACAddr::NONE))
	  || (!associated))
	    drop = true;
	*/
	drop = !associated;
          break;
        case InterfaceWireless::HOSTAP:
          // Drop if no associations
          drop = btv.empty();
          break;
        default:
          break;
      }
    }

  if (drop)
    {
      pendingPkt = p; // DropPacket clears this
      // Here we need to drop all enqueued packets as well
      while(pendingPkt)
        {
          DropPacket("NA", false);
          pendingPkt = iface->GetQueue()->Deque();
        }
      return;
    }

  shortRetryCount = 0;
  longRetryCount = 0;
  cw = PMIB.CWMin;    // revert the cw back
  backingOff = false;
  backoffTimerState = NONE;

  if (pendingPkt)
    {
      // This cannot happen because if pendingPkt is not null,
      // L2 protocol is busy and DataRequest() is never invoked
      cout << "HuH??? Data request with pending packet\n";
    }
  // Hold here till the RTS/CTS exchange is over
  pendingPkt = p;

  if (!MediumBusy())
    {
      // The medium is idle
      DEBUG0(cout << "  medium is idle\n");
      if (firstAttempt) {
        InitiateFirstAttempt();
        firstAttempt = false; /* XXX brought it from last line of this func */
      }
    }
  else
    {
      // The medium is busy
      DEBUG0(cout << "  medium is busy\n");
      InitiateBackoff();
      medBusy++;
      if (VcsBusy())
        {
          vcsBusy++;
          ScheduleTimer(MACTimerEvent::NAV_TIMEOUT, navTimeout,
                        navTimer - Simulator::Now());
        }
    }
}

void L2Proto802_11::DataIndication(Packet* p, LinkEvent* le)
{ // Received a packet from the wireless interface
  bool fbrx = false; // True if first bit rx event
  totPacketsRx++;    // Count for statistics
  if (le)
    {
      fbrx = (le->event == LinkEvent::PACKET_FIRSTBIT_RX);
    }
  if (le && !p)
    { // Use packet from link event
      p = le->p;
    }
  if (le) delete le;

  L2Header802_11* l2pdu = (L2Header802_11*)p->PopPDU();
  MACAddr dst;
  bool toact = false;
  // Trace the PDU received
  bool toMe = (l2pdu->ra == iface->GetMACAddr() ||
               l2pdu->ra.IsBroadcast());
  if (toMe || !L2Proto::destinationOnly)
    iface->GetNode()->TracePDU(this, l2pdu, p, "+");
  // Ignore pkts that don't have ta
  if ((l2pdu->type != CTS) && (l2pdu->type != ACK))
    {
      if (((InterfaceWireless*)iface)->GetOpMode() == InterfaceWireless::BSS)
	{
	  if (l2pdu->ta == assocBssid)
	    {
	      toact = true;
	    }
	}
      else
	{
	  if (l2pdu->ra == iface->GetMACAddr())
	    {
	      toact = true;
	    }
	}
    }

  if (toact == true)
    {
      DEBUG0((cout << "MAC " << iface->GetMACAddr()
              << ":Inside LH DataIndication to "
              << l2pdu->ta << " at " <<  Simulator::Now() << std::endl));
      LastHeardMap_t::iterator k = lastH.find(l2pdu->ta);
      if ( k != lastH.end())
        {
          CancelTimer(k->second, true);
          k->second = new MACTimerEvent(MACTimerEvent::LH_TIMEOUT);
          k->second->xmt = l2pdu->ta;
          ScheduleTimer(MACTimerEvent::LH_TIMEOUT, k->second,
                        3.0);
        }
      else
        {
          MACTimerEvent* lht= new MACTimerEvent(MACTimerEvent::LH_TIMEOUT);
          lht->xmt = l2pdu->ta;
          lastH.insert(LastHeardMap_t::value_type(l2pdu->ta, lht));
          ScheduleTimer(MACTimerEvent::LH_TIMEOUT, lht, 3.0);
          // Nothing
        }
    }

  switch(l2pdu->type) {
    case RTS:
      {
        if (mac_state != IDLE)
          {
            DEBUG0(cout << MYIP << ": RTS received in wrong state\n");
            delete p;
            break;
          }
        RTS_frame* rtsFrame = (RTS_frame*)l2pdu;
        if (rtsFrame->ra == iface->GetMACAddr())
          {
            // This is for me
            if (VcsBusy())
              {
                // Don't respond if the NAV indicates busy
                delete p;
                break;
              }
            mac_state =  RTS_RCVD;
            DEBUG0(cout << MYIP << ": got RTS uid " << p->uid
                   << " at " << Simulator::Now() << endl);
            Word_t cts_time = usec(sifs + DSSS_MaxPropagationDelay
                                   + txtime(ETHER_CTS_LEN, basicRate));
            // Trace the receipt of the rts
            //iface->GetNode()->TracePDU(this, rtsFrame, p, "+");
            // Send the CTS
            SendCTS(rtsFrame->ta, rtsFrame->duration - cts_time);
          }
        else
          {
            // Else set my NAV timer
            durationRx++;
            Time_t new_nav = Simulator::Now() + rtsFrame->duration / 1e6;
            if (new_nav > navTimer) {
              DEBUG0((cout << "Node " << iface->GetNode()->Id()
                      << " time " << TIMEUS
                      << " overheard RTS, new_nav " << new_nav
                      << " navTimer " << navTimer 
                      << " backingOff " << backingOff
                      << endl));
              navTimer = new_nav;
              CancelTimer(navTimeout);
              ScheduleTimer(MACTimerEvent::NAV_TIMEOUT, navTimeout,
                            navTimer - Simulator::Now());
              navTimerCount++;
            }
          }
        delete p;
        break;
      }
    case CTS:
      {
        CTS_frame* ctsFrame = (CTS_frame*)l2pdu;
        if (ctsFrame->ra == iface->GetMACAddr())
          { // This is for me
            if (mac_state != CTS_WAIT)
              {
                DEBUG0(cout << MYIP << ": CTS received in wrong state\n");
                delete p;
                break;
              }
            DEBUG0(cout << MYIP << ": got CTS uid " << p->uid
                   << " at " << Simulator::Now() << endl);
            mac_state = CTS_RCVD;
            shortRetryCount = 0;
            CancelTimer(ctsTimeout);
            // Trace the receipt of the cts
            //iface->GetNode()->TracePDU(this, ctsFrame, p, "+");
            DEBUG0((cout << "node " << iface->GetNode()->Id()
                    << " got cts, state " << mac_state
                    << " pp " << pendingPkt
                    << " time " << Simulator::Now() << endl));
            SendDATA();
	  }
        else
          {
            // Update the NAV in case we haven't heard from original sender
            durationRx++;
            Time_t new_nav = Simulator::Now() + ctsFrame->duration / 1e6;
            if (new_nav > navTimer)
              {
                DEBUG0((cout << "Node " << iface->GetNode()->Id()
                        << " time " << TIMEUS
                        << " overheard CTS, new_nav " << new_nav
                        << " navTimer " << navTimer << endl));
                navTimer = new_nav;
                CancelTimer(navTimeout);
                ScheduleTimer(MACTimerEvent::NAV_TIMEOUT,  navTimeout,
                              navTimer - Simulator::Now());
                navTimerCount++;
              }
	  }
        delete p;
        break;
      }
    case DATA:
      {
        DATA_frame* dataFrame = (DATA_frame*)l2pdu;
        dst = dataFrame->addr1;
        if (dst.IsBroadcast())
          { // Broadcast
            if (mac_state != IDLE)
              {
                DEBUG0(cout << MYIP << ": received BC-DATA in wrong state\n");
                delete p;
                break;
              }
            DEBUG0(cout << MYIP << ": received BC-DATA uid " << p->uid
                   << " at " << Simulator::Now() << endl);
            // Trace the receipt of the broadcast data frame
            //iface->GetNode()->TracePDU(this, dataFrame, p, "+");
            iface->HandleLLCSNAP(p, fbrx);
	  }
        else
          {
            if (dst == iface->GetMACAddr())
              { // Unicast, it's for me

		// IF RTSThreshold on the sender side is not met, RTS-CTS will not be enabled
		if (mac_state != DATA_WAIT && mac_state != IDLE )
                  {
       	            DEBUG0(cout << MYIP << ": received DATA in wrong state\n");
               	    delete p;
                    break;
       	          }

		DEBUG0(cout << MYIP << ": received DATA uid " << p->uid
                       << " at " << Simulator::Now() << endl);
		//The spec says ACK transmission shall commence after
                // a SIFS period
		// without regard to the busy/idle state of the medium
		// Refer to chapter 9.2.8, IEEE Std 802.11, 1999 Edition
		CancelTimer(dataTimeout);
		mac_state = DATA_RCVD;
                // Trace the receipt of the unicast data frame
                //iface->GetNode()->TracePDU(this, dataFrame, p, "+");
		SendACK(dataFrame->addr2); // src-dest pair

		// Need to check if this data frame is retry and received
                // before If so, drop silently
		if (!dataFrame->fc.fc_retry)
                  { // first try, first received
                    AddSeqCache(dataFrame->addr2, dataFrame->seqno);
		    DEBUG0((cout << MYIP
                            << " DataIndication uid " << p->uid
                            << "from "  << dataFrame->addr2
                            << " to " << iface->GetMACAddr()
                            << " at " << Simulator::Now()
                            << endl));
                    iface->HandleLLCSNAP(p, fbrx);
                  }
		else
                  { // retry
                    if (FindSeqCache(dataFrame->addr2, dataFrame->seqno))
                      {
                        // already received
                        delete p;
                      }
                    else
                      { // first received
			AddSeqCache(dataFrame->addr2, dataFrame->seqno);
			iface->HandleLLCSNAP(p, fbrx);
                      }
                  }
              }
            else
              {
                // Unicast, not for me
		delete p;
              }
          }
        break;
      }

    case ACK:
      {
        ACK_frame* ackFrame = (ACK_frame*)l2pdu;
        if (ackFrame->ra == iface->GetMACAddr())
          {
            if (mac_state != ACK_WAIT)
              {
                DEBUG0(cout << MYIP << ": received ACK in wrong state\n");
                delete p;
                break;
              }
            DEBUG0(cout << MYIP << ": received ACK uid " << p->uid
                   << " at " << Simulator::Now() << endl);
            DEBUG0(cout << "  Data send complete\n");
            mac_state = IDLE;
            DEBUG0(cout << "  mac_state set to IDLE\n");
            longRetryCount = 0;
            CancelTimer(ackTimeout);
            if (pendingPkt) delete pendingPkt;
            pendingPkt = nil;
            // Trace the receipt of the ack frame
            //iface->GetNode()->TracePDU(this, ackFrame, p, "+");
            // L2 protocol became idle
            // Process next packet if existing
	    if(pendingBeacon) {
              SendBeacon();
	    } else if (pendingAssocResp){
	      ScheduleSendAfterIFS(pendingAssocResp, sifs);
	    } else {
	      DataRequestFromIFQ();
	    }
	  }
        delete p;
        break;
        // we are not intended for this ack so ignore
      }
    case BEACON :
      {
	Beacon_frame* beaconFrame = (Beacon_frame*)l2pdu;
        // Trace the receipt of the beacon frame
        //iface->GetNode()->TracePDU(this, beaconFrame, p, "+");
        // Get the interface from which this beacon was received
        WirelessLink* wlink = (WirelessLink*)iface->GetLink();
        InterfaceWireless* ifw = (InterfaceWireless*)iface;
	Interface* tif = wlink->GetIfByMac(beaconFrame->ta);
	if(!tif)
	  cout << "Can't find iface for mac "
               << beaconFrame->ta << endl;
	else
          {
            Node* bNode = tif->GetNode();
            Meters_t dist = iface->GetNode()->Distance(bNode);
            if (ifw->GetOpMode() == InterfaceWireless::BSS)
              {
		if(0)cout << "at " << Simulator::Now() << " Mac "
                          << iface->GetMACAddr() << " got BCKN   from "
                          << beaconFrame->ta
                          << "  opmode " << ifw->GetOpMode()
                          << " associated " << associated
                          << " pending " << associationPending << endl;

                if (associated)
                  {
                    // if from different BS, check distance
                    if (!(beaconFrame->ta == assocBssid))
                      {

			Time_t elapsed = Simulator::Now() - lastBeacon;
                        if (elapsed > 1.0 || dist < (currDist * 0.9))
                          { // New one is closer than 90% old one distance
                            // Send a dissoc to old and asoc req to new
                            // Or we haven't heard from old BS in 1 sec
                            if (!associationPending)
                              { // Only if no current association pending
                                DEBUG0((cout << "Node "
                                        << iface->GetNode()->Id()
                                        << " switching associations" << endl));
                                SendAssocReq(sifs, beaconFrame->ta);
                                // Disassociation will be sent when
                                // AssocResponse received
                              }
                          }
                      }
                    else
                      { // Beacon from existing association. Update distance
                        DEBUG0((cout << "Got beacon from current BS,  "
                                << " curDist " << currDist
                                << " dist " << dist << endl));
                        DEBUG0((cout << "node " << iface->GetNode()->Id()
                                << " reassociating time "
                                << Simulator::Now() << endl));
                        currDist = dist;
                        lastBeacon = Simulator::Now();
                        if (++beaconRxCount == 10)
                          { // Time to re-associate
                            SendReassocReq(sifs, beaconFrame->ta);
                            beaconRxCount = 0;
                          }
                      }
                  }
                else
                  { // not associated
                    if (!associationPending)
                      {
                        DEBUG0((cout << "Got beacon from BS not associated"
                                << ", sending assocReq to node "
                                << beaconFrame->ta << endl));
                        SendAssocReq(sifs, beaconFrame->ta);
                      }
                    else
                      {
                        DEBUG0((cout << "MAC " << iface->GetMACAddr()
                                << " got beacon with assocPending, ignoring"
                                << endl));
                      }
                  }
              }
          }
	delete p;
	break;
      }
    case ASSOCREQ:
      {
	Assocreq_frame* asreqFrame = (Assocreq_frame*) l2pdu;
        if (asreqFrame->ra == iface->GetMACAddr())
          { // Got association request addressed to me
            DEBUG0((cout << "MAC " << iface->GetMACAddr()
                    << " received an association request packet from "
                    << asreqFrame->ta << endl));
            if ( ((InterfaceWireless*)iface)->GetOpMode()
                 == InterfaceWireless::HOSTAP)
              {
                SendAssocResp(sifs, asreqFrame->ta);
                // Get the iface from which association request was received
                WirelessLink* wlink = (WirelessLink*)iface->GetLink();
                Interface* tif = wlink->GetIfByMac(asreqFrame->ta);
                if(!tif)
                  cout << "Can't find iface for mac "
                       << asreqFrame->ta << endl;
                else
                  { // Add this to tye BTV
                    AddToBTV(asreqFrame->sip, asreqFrame->ta);
                  }
              }
          }
        delete p;
        break;
      }
    case ASSOCRESP:
      {
	Assocresp_frame* asrespFrame = (Assocresp_frame*) l2pdu;
        if (asrespFrame->ra == iface->GetMACAddr())
          { // Addressed to me
            DEBUG0((cout << "MAC " << iface->GetMACAddr()
                    << " received an association response packet from "
                    << asrespFrame->ta << endl));
            if ( ((InterfaceWireless*)iface)->GetOpMode()
                 == InterfaceWireless::BSS)
              {
                // If currently associated, send a disassociation to
                // original BS
                if (associated)
                  {
                    SendDisassocFrame(sifs);
                  }
		DEBUG0((cout << "HANDOFF: " <<  Simulator::Now() << " "
                        << iface->GetMACAddr() << " "
                        << iface->GetNode()->Id() << std::endl));
                DEBUG0((cout << "at " << Simulator::Now() << "   MAC "
                        << iface->GetMACAddr()
                        << " node " << iface->GetNode()->Id()
                        << " is now associated with MAC "
                        << asrespFrame->ta << endl));
                associated = true;
                associationPending = false;
                if (associationTimeout)
                  {
                    CancelTimer(associationTimeout);
                  }
                assocBssid = asrespFrame->ta;
                // Get the iface from which this beacon was received
                WirelessLink* wlink = (WirelessLink*)iface->GetLink();
                Interface* tif = wlink->GetIfByMac(asrespFrame->ta);
                if(!tif)
                  cout << "Can't find iface for mac "
                       << asrespFrame->ta << endl;
                else
                  {
                    Node* bNode = tif->GetNode();
                    Meters_t dist = iface->GetNode()->Distance(bNode);
                    currDist = dist;
                  }

		// get the peer l2proto802.11
		L2Proto802_11* pL2proto = (L2Proto802_11*)tif->GetL2Proto();
		// notify EIGRP on interface up
                DEBUG0((cout << "node " << iface->GetNode()->Id()
                        << " eigrp notif up "
                        << " ip " << (string)IPAddr(iface->GetIPAddr())
                        << " time " << Simulator::Now()
                        << endl));
		pL2proto->SendNotification(iface->GetIPAddr());
              }
	  }
	delete p;
	break;
      }
    case DISASSOC:
      {
	DEBUG0((cout << "MAC " << iface->GetMACAddr()
                << ": Inside DISASSOC from DataIndication" << std::endl));
	Disassoc_frame* dframe = (Disassoc_frame*) l2pdu;
        if (dframe->ra == iface->GetMACAddr())
          { // Got disassoc frame addressed to me
            if ( ((InterfaceWireless*)iface)->GetOpMode()
                 == InterfaceWireless::HOSTAP) {
              DEBUG0((cout << "MAC " << iface->GetMACAddr()
                      << "  Received a disaccociation  packet from "
                      << dframe->ta << "at "  << Simulator::Now()
                      << std::endl));
              // Remove the IPAddress and MAC address from bridge table btv
              RemoveFromBTV(dframe->sip);

	      if(iface) {
		// get the l2proto802.11 for iface
		L2Proto802_11* pL2proto = (L2Proto802_11*)iface->GetL2Proto();
		// notify EIGRP on interface down
                DEBUG0((cout << "notifying EIGRP on interface down for IP "
                        << IPAddr::ToDotted(dframe->sip) << "at "
                        << iface->GetMACAddr()
                        << " at " << Simulator::Now() << std::endl));
                DEBUG0((cout << "node " << iface->GetNode()->Id()
                        << " eigrp notif down "
                        << " time " << Simulator::Now()
                        << endl));
		pL2proto->SendNotification(dframe->sip);
	      }
            } else {
	      associated = false;
	      assocBssid = MACAddr();
	    }
	  }
	delete p;
	break;
      }
    case REASSOCREQ:
      {
        if (l2pdu->ra == iface->GetMACAddr())
          {
            LastHeardMap_t::iterator k = lastH.find(l2pdu->ta);
            if (k != lastH.end())
              {
                MACTimerEvent* lht = k->second;
                CancelTimer(lht);
                ScheduleTimer(MACTimerEvent::LH_TIMEOUT, lht, 3.0);
              }
          }
        delete p;
        break;
      }

    default:
      cout << "HuH? unknown 802.11 packet type\n";
      delete p;
    }
}

bool L2Proto802_11::Busy()
{
  // Determine if protocol is busy
  return (!!pendingPkt || mac_state != IDLE);
}

bool L2Proto802_11::VcsBusy()
{
  return navTimer > Simulator::Now();
}

bool L2Proto802_11::PcsBusy()
{
  return ((InterfaceWireless*)iface)->GetLinkState() != InterfaceWireless::IDLE;
}

bool L2Proto802_11::MediumBusy()
{
  // Determine if the medium is busy by
  // either virtual or physical carrier sense
  return (VcsBusy() || PcsBusy());
}

// L2802_11Header methods

L2Proto* L2Proto802_11::Copy() const
{
  return new L2Proto802_11(*this);
}

// Sending the packets

bool L2Proto802_11::SendRTS(MACAddr ta, MACAddr ra, Word_t dur, bool imm)
{
  RTS_frame* rf = new RTS_frame(dur, ra, ta, 0);
  Packet* p = new Packet();
  p->PushPDU(rf);
  if(imm)
  	ScheduleSendAfterIFS(p, 0);
  else
	ScheduleSendAfterIFS(p, difs);
  return true;
}

bool L2Proto802_11::SendCTS(MACAddr ra, Word_t dur)
{
  CTS_frame* cf = new CTS_frame(dur, ra, 0);
  Packet* p = new Packet();
  p->PushPDU(cf);
  //  failCount=0;
  ScheduleSendAfterIFS(p, sifs);
  return true;
}

bool L2Proto802_11::SendDATA(bool imm)
{
  if (!pendingPkt) {
    cout << "HuH? null data pointer\n";
	mac_state = IDLE;
    return false;
  }

  DATA_frame* df = (DATA_frame*)pendingPkt->PeekPDU();
  MACAddr dst = df->addr1;

  /* Unicast means we seized the medium and we need to keep it
	 Thus we use the smallest gap, SIFS
  */
  //  failCount=0;
  /*If RTSThreshold is not met, treat as Broadcast */

  Time_t ifs =
    (dst.IsBroadcast()|| pendingPkt->Size()<RTSThreshold) ? difs : sifs;
  if(imm)
	ScheduleSendAfterIFS(pendingPkt, 0);
  else
	ScheduleSendAfterIFS(pendingPkt, ifs);
  return true;
}

bool L2Proto802_11::SendACK(MACAddr ra)
{
  ACK_frame* af = new ACK_frame(0, ra, 0);
  Packet *p = new Packet();
  p->PushPDU(af);
  ScheduleSendAfterIFS(p, sifs);
  return true;
}

void L2Proto802_11::BackoffProcedure()
{
  DEBUG0(cout << MYIP << ": backing off at " << Simulator::Now() << endl);
  mac_state = IDLE;
  backingOff = true;
  backoffTimerState = NONE;
  CancelTimer(backoffTimeout);
  if (!backoffRng) backoffRng = new Uniform();
  Count_t ibot = (Count_t)(backoffRng->Value()*cw); // Round to iteger
  if (!ibot) ibot++; // Can't be zero
  Time_t bot = ibot*PMIB.SlotTime;
  // debug
  backoffCountByCW[cw]++;
  // end debug
  incr_cw();
  DEBUG0((cout << "Sim time " << Simulator::Now()
          << " Backoff time " << bot << " cw " << cw <<  endl));
  ScheduleTimer(MACTimerEvent::BACKOFF_TIMEOUT, backoffTimeout, bot);
}

bool L2Proto802_11::ScheduleSendAfterIFS(Packet* p, Time_t t)
{
  MACEvent* mev = new MACEvent(MACEvent::PACKET_TX, p);
  Scheduler::Schedule(mev, t, this);
  return true;
}

void L2Proto802_11::Handle(Event* ev, Time_t t )
{
  // Need to send the packet down to link layer and for that we need
  // interface handle.
  // Now we are at the IFS slot boundary and need to check if the medium
  // is idle in such case we are allowed to transmit

  Time_t txtime1, txtime2, to_value;
  MACEvent* mev = (MACEvent*)ev;
  MACAddr recv;
  bool successfulTx = true;
  switch (mev->event) {
    case MACEvent::PACKET_TX :
      {
        mevFirstAttempt = false;
        L2Header802_11* l2pdu = (L2Header802_11*)mev->p->PeekPDU();
        // Trace this pdu being transmitted
        // This is actually wrong, since we don't always tx the pkt
        // Should be moved below
        switch (l2pdu->type) {
          case RTS:
	    {
              DEBUG0((cout << "node " << iface->GetNode()->Id()
                      << " PACKET_TX (RTS), time " << TIMEUS
                      << " busy " << MediumBusy() << endl));
	      if (MediumBusy())
		{
		  mac_state = IDLE;
                  delete mev->p; /* SDR */
                  DEBUG0((cout << "node " << iface->GetNode()->Id()
                          << " medium busy on rts, state " << mac_state
                          << " time " << Simulator::Now() << endl));
		  break;
		}
              else
                {
                  shortRetryCount++;
                  BackoffProcedure(); /* SDR DEBUG */
                }

	      // Trace this rts
	      iface->GetNode()->TracePDU(this, l2pdu, mev->p, "-");
	      successfulTx =
		iface->GetLink()->Transmit( mev->p, iface, iface->GetNode(),
					    txtime(ETHER_RTS_LEN, basicRate));
              if (!successfulTx)
                {
                  DEBUG0((cout <<"RTS SEND FAILED " << endl));
                  mac_state=IDLE;
                  delete mev->p; /* SDR */
                  break;
              }
	      RTS_frame* rtsFrame = (RTS_frame*)l2pdu;
	      recv = rtsFrame->ra;

	      txtime1 = txtime(ETHER_RTS_LEN, basicRate);
	      txtime2 = txtime(ETHER_CTS_LEN, basicRate);
	      to_value = txtime1 + sifs + txtime2
                + DSSS_MaxPropagationDelay +  DSSS_MaxPropagationDelay;
	      ScheduleTimer(MACTimerEvent::CTS_TIMEOUT, ctsTimeout, to_value);
	      mac_state = CTS_WAIT;

	      delete mev->p; /* SDR */
	      break;
	    }
	case CTS :
	  {
            if (MediumBusy())
              {
                mac_state = IDLE;
                delete mev->p; /* SDR */
                break;
              }
            // Trace this cts
            iface->GetNode()->TracePDU(this, l2pdu, mev->p, "-");
            successfulTx =
	      iface->GetLink()->Transmit(mev->p, iface,
					 iface->GetNode(),
					 txtime(ETHER_CTS_LEN, basicRate));

	    CTS_frame* ctsFrame = (CTS_frame*)l2pdu;
	    recv = ctsFrame->ra;

	    if (!successfulTx)
              {
                DEBUG0((cout <<"CTS SEND FAILED " << endl));
                mac_state=IDLE;
                delete mev->p; /* SDR */
                break;
              }
            to_value = l2pdu->duration*1e-6 - sifs
	      + DSSS_MaxPropagationDelay + DSSS_MaxPropagationDelay;
            ScheduleTimer(MACTimerEvent::DATA_TIMEOUT, dataTimeout, to_value);
            mac_state = DATA_WAIT;

	    delete mev->p; /* SDR */
            break;
	  }
	case DATA:
	  {
	    if (!pendingPkt)
	      {
		// No data pkt while l2 is about to send it
		cout << "HuH? no pendingPkt to send\n";
		mac_state = IDLE;
		break;
	      }
	    DATA_frame* df = (DATA_frame*)l2pdu;
	    recv = df->ra;
	    if (df->addr1.IsBroadcast())
	      {
		if (MediumBusy())
		  {
		    longRetryCount++;
		    if(longRetryCount > MMIB.LongRetryLimit)
		    {
			//Drop
			longRetryCount =0 ;
			mac_state = IDLE;
			pendingPkt = nil;
			break;
		    }
		    else
		    {
			BackoffProcedure();
			break;
		    }
		}
                    //delete mev->p; /* SDR *//* John */
		iface->GetNode()->TracePDU(this, df, mev->p, "-");
		successfulTx =
		  iface->GetLink()->Transmit(mev->p, iface, iface->GetNode(),
					     txtime(mev->p->Size(), dataRate));
		//delete mev->p;
		pendingPkt = nil;
		break;
	      }
            else
              {
                if (MediumBusy())
                  {
                    DEBUG0(cout << MYIP << ": linkBusy DATA "
                           << ((InterfaceWireless*)iface)->GetLinkState()
                           << endl);
                    delete mev->p; /* SDR */
                    pendingPkt = nil;
                    mac_state = IDLE;
                    break;
                  }
                // Trace this data pkt
                iface->GetNode()->TracePDU(this, df, mev->p, "-");
                successfulTx =
		  iface->GetLink()->Transmit(mev->p, iface, iface->GetNode(),
					     txtime(mev->p->Size(), dataRate));
                if (!successfulTx)
                  {
                    delete mev->p; /* SDR */
                    pendingPkt = nil;
                    mac_state = IDLE;
                    break;
                  }

                txtime1 = txtime(pendingPkt->Size(), dataRate);
                txtime2 = txtime(ETHER_ACK_LEN, basicRate);
                to_value = txtime1 + sifs + txtime2
                  + DSSS_MaxPropagationDelay + DSSS_MaxPropagationDelay;
                ScheduleTimer(MACTimerEvent::ACK_TIMEOUT,ackTimeout,to_value);
                mac_state = ACK_WAIT;
                break;
              }
          }


	case ACK:
	  {
            if (MediumBusy())
              {
                delete mev->p; /* SDR */
                mac_state = IDLE;
                DEBUG0(cout << MYIP << ": linkBusy ACK "
                       << ((InterfaceWireless*)iface)->GetLinkState()
                       << endl);
                break;
              }
            else
              {
                iface->GetNode()->TracePDU(this, l2pdu, mev->p, "-");
                successfulTx =
		  iface->GetLink()->Transmit(mev->p, iface, iface->GetNode(),
					     txtime(ETHER_ACK_LEN, basicRate));
		ACK_frame* ackFrame = (ACK_frame*)l2pdu;
		recv = ackFrame->ra;
              }
            mac_state = IDLE;
            if (!Busy())
              {
                // L2 protocol became idle
                if (pendingBeacon) SendBeacon();
                else               DataRequestFromIFQ();
              }
	    delete mev->p; /* SDR */
            break;
	  }
	case BEACON:
          if (MediumBusy())
            {
              //if (pendingBeacon) {
              //  delete pendingBeacon;
              //  pendingBeacon=nil;
              //}
              //cout << "beacon, pkt " << mev->p << endl;
              //pendingBeacon = mev->p->Copy();

              //pendingBeacon = mev->p; /*GFR */
              //mev->p = nil; // GFR pending beacon re-used

              // try to be a little optimistic
              if((navTimer - Simulator::Now()) > 1e-4)
                ScheduleSendAfterIFS(mev->p,
                                     (navTimer - Simulator::Now())+0.000005);
              else
                ScheduleSendAfterIFS(mev->p, 0.0000075);

              DEBUG0((cout << "BEACON send fail " << Simulator::Now()
                      << " " << ((InterfaceWireless*)iface)->GetLinkState()
                      << endl));
            }
          else
            {
              // Trace this send
              iface->GetNode()->TracePDU(this, l2pdu, mev->p, "-");
              successfulTx =
		iface->GetLink()->Transmit(mev->p, iface,iface->GetNode(),
					   txtime(mev->p->Size(), basicRate));
              DEBUG0((cout << "node " << iface->GetNode()->Id()
                      << " sent beacon, success " << successfulTx
                      << " time " << Simulator::Now() << endl));
              if (!successfulTx)
                {
                  ScheduleSendAfterIFS(mev->p, 0.0000075);
                  DEBUG0((cout <<"BEACON "
                          << " FAILED at 0x" << hex << iface
                          << "  " << Simulator::Now() << endl));
                  break;
                }
              else
                {
                  if (mac_state == IDLE)
                    if (pendingBeacon) SendBeacon();
                  delete mev->p;
                }
            }
          break;

	case ASSOCREQ:
	  {
            if (MediumBusy())
              {
// 		if(pendingAssocReq) {
// 		  delete pendingAssocReq;
// 		}
// 		pendingAssocReq = mev->p->Copy();
                //      pendingAssocReq = mev->p;
		// try to be a little optimistic
		if(((navTimer - Simulator::Now()) > 1e-4)
		   && ((navTimer - Simulator::Now()) < 0.1))
		  ScheduleSendAfterIFS(mev->p,
				       (navTimer - Simulator::Now())+0.000005);
		else
		  ScheduleSendAfterIFS(mev->p, 0.0000075);
                DEBUG0(cout << MYIP << ": linkBusy ASSOCREQ "
                       << ((InterfaceWireless*)iface)->GetLinkState()
                       << endl);
              }
            else
              {
                // Trace this send
                iface->GetNode()->TracePDU(this, l2pdu, mev->p, "-");
                successfulTx =
		  iface->GetLink()->Transmit(mev->p, iface, iface->GetNode(),
					     txtime(mev->p->Size(), basicRate));
		Assocreq_frame* assocFrame = (Assocreq_frame*)l2pdu;
		recv = assocFrame->ra;

		if (!successfulTx)
                  {
                    DEBUG0((cout << "Send AssocReq failed" << endl));
                    ScheduleSendAfterIFS(mev->p, 0.0000075);
                    break;
                  }
                else
                  {
                    delete mev->p; /* SDR */
                  }
              }
            break;
	  }
	case ASSOCRESP:
	  {
            if (MediumBusy())
              {
// 		if(pendingAssocResp) {
// 		  delete pendingAssocResp;
// 		}
// 		pendingAssocResp = mev->p->Copy();
//                pendingAssocResp = mev->p;
//                mev->p = nil; // pending beacon re-used
		/* try to be a little optimistic */
		if(((navTimer - Simulator::Now()) > 1e-4)
		   && ((navTimer - Simulator::Now()) < 0.1))
		  ScheduleSendAfterIFS(mev->p,
				       (navTimer - Simulator::Now())+0.000005);
		else
		  ScheduleSendAfterIFS(mev->p, 0.0000075);

                DEBUG0(cout << MYIP << ": linkBusy ASSOCRESP "
                       << ((InterfaceWireless*)iface)->GetLinkState()
                       << endl);
              }
            else
              {
                // Trace this send
                iface->GetNode()->TracePDU(this, l2pdu, mev->p, "-");
                successfulTx =
		  iface->GetLink()->Transmit(mev->p, iface, iface->GetNode(),
					     txtime(mev->p->Size(), basicRate));
		Assocresp_frame* AssocrespFrame = (Assocresp_frame*)l2pdu;
		recv = AssocrespFrame->ra;

		if(!successfulTx)
                  {
                    DEBUG0((cout << "Send Assoc Resp  failed" << endl));
                    ScheduleSendAfterIFS(mev->p, 0.0000075);
                    break;
                  }
                else
                  {
                    delete mev->p; /* SDR */
                  }
              }
            break;
	  }
	case DISASSOC:
	  {
            if (MediumBusy())
              {
// 		if (pendingDissoc) {
// 		  delete pendingDissoc;
// 		}
// 		pendingDissoc = mev->p->Copy();
//                pendingDissoc = mev->p;
//                mev->p = nil; // pending beacon re-used
		/* try to be a little optimistic */
		if((navTimer - Simulator::Now()) > 1e-4)
		  ScheduleSendAfterIFS(mev->p,
				       (navTimer - Simulator::Now())+0.000005);
		else
		  ScheduleSendAfterIFS(mev->p, 0.0000075);

                DEBUG0((cout << MYIP << ": linkBusy DISASSOC "
                        << ((InterfaceWireless*)iface)->GetLinkState()
                        << endl));
              }
            else
              {
                // Trace this send
                iface->GetNode()->TracePDU(this, l2pdu, mev->p, "-");
                successfulTx =
		  iface->GetLink()->Transmit(mev->p, iface, iface->GetNode(),
					     txtime(mev->p->Size(), basicRate));
		Disassoc_frame* disassocFrame = (Disassoc_frame*)l2pdu;
		recv = disassocFrame->ra;

		if(!successfulTx)
                  {
                    DEBUG0((cout << "Send Dissoc failed at "
                            <<  iface->GetNode()->Id()
                            << "  at " << Simulator::Now() << endl));
                    ScheduleSendAfterIFS(mev->p, 0.0000075);
                  }
                else
                  {
                    DEBUG0((cout << "Dissoc success at"
                            << iface->GetNode()->Id()
                            << "  at " << Simulator::Now() << endl));

                    delete mev->p; /* SDR */
                  }
              }
            break;
	  }
	case REASSOCREQ:
	  {
            if (MediumBusy())
              {
		// try to be a little optimistic
		if((navTimer - Simulator::Now()) > 1e-4)
		  ScheduleSendAfterIFS(mev->p,
				       (navTimer - Simulator::Now())+0.000005);
		else
		  ScheduleSendAfterIFS(mev->p, 0.0000075);
              }
            else
              {
                // Trace this send
                iface->GetNode()->TracePDU(this, l2pdu, mev->p, "-");
                successfulTx =
		  iface->GetLink()->Transmit(mev->p, iface, iface->GetNode(),
					     txtime(mev->p->Size(), basicRate));
		if(!successfulTx)
                  {
                    DEBUG0((cout << "Send Ressoc failed at "
                            << iface->GetNode()->Id()
                            << "  at " << Simulator::Now() << endl));
                    ScheduleSendAfterIFS(mev->p, 0.0000075);
                  }
                else
                  {
                    DEBUG0((cout << "Reassoc success at"
                            << iface->GetNode()->Id()
                            << "  at " << Simulator::Now() << endl));
                    delete mev->p; /* SDR */
                  }
              }
            break;
	  }
	default:
	  DEBUG0(cout << "HuH? unknown packet type while sending\n");
        } //switch (l2pdu->type)

        DEBUG0(cout.precision(8));
        DEBUG0(cout << MYIP << ": transmit"
               << " at " << Simulator::Now() << endl);
        break;
      } //case MACEvent::PACKET_TX

    case MACEvent::BACKOFF:
      mevBackoff = nil;
#ifdef TRY_THIS
      if (MediumBusy())
        {
          mac_state = IDLE;
          break;
        }
#endif
      if (!backingOff)
        {  // need to start backoff procedure
          BackoffProcedure();
        }
      else
        {  // already in backoff, need to resume backoff timer
          if (backoffTimerState == SUSPENDED)
            ResumeTimer(backoffTimeout);
        }
      break;

    default:
      break;
    } //switch
  delete mev; /* Dheeraj */
}

void L2Proto802_11::Timeout(TimerEvent *ev)
{
  MACTimerEvent* mtev = (MACTimerEvent*)ev;
  DEBUG0((cout << "node " << iface->GetNode()->Id()
          << " Timeout, type " << mtev->event
          << " mac state " << mac_state
          << " time " << Simulator::Now()
          << endl));
  switch (mtev->event) {
    case MACTimerEvent::BACKOFF_TIMEOUT:
      // Backoff timer expried, which means we can access the medium
      //  Send immediately if there is a pending MSDU
      DEBUG0(cout << MYIP << ": BACKOFF_TIMEOUT at " << Simulator::Now()
             << endl);
      if (MediumBusy())
        {
          // This is a double check, remove later
          // This timeout cannot occur if the medium became busy
          // because Idle2Busy() suspends this timer event
          //cout << "HuH? medium busy at the BACKOFF_TIMEOUT\n";
          //cout << "  PCS: "
          //     << ((InterfaceWireless*)iface)->GetLinkState() << endl
          //     << "  VCS: NAV " << navTimer
          //     << " now " << Simulator::Now()
          //     << endl;
          //cout << "  mac_state " << mac_state << endl;
        }
      backingOff = false;
      backoffTimerState = NONE;
      if (pendingPkt)
        {
          InitiateDataSequence();
        }
      break;

    case MACTimerEvent::NAV_TIMEOUT:
      // The NAV timer expired, which means virtual CS indicates idle
      // Thus need to ckeck if the medium became idle
      if (!areEqual(navTimer,Simulator::Now()))
        {
          cout << "HuH? navTimer != now at the NAV_TIMEOUT"
               << "  NAV " << navTimer << " now " << Simulator::Now() << endl;
        }
      DEBUG0(cout << MYIP << ": NAV_TIMEOUT at " << Simulator::Now() << endl);
      if (!MediumBusy()) /* Gurashish 16Sep2005*/
        {
          Busy2Idle();
        }
      break;

    case MACTimerEvent::CTS_TIMEOUT:
      {
        ctsTOCount++;
        DEBUG0((cout << "node " << iface->GetNode()->Id()
                << " CTSTimeout, time " << TIMEUS << endl));
        DEBUG0((cout << "node " << iface->GetNode()->Id()
                << " src " << shortRetryCount
                << " srl " << MMIB.ShortRetryLimit
                << " mac state " << mac_state
                << " mb " << MediumBusy()
                << " backing off " << backingOff
                << " time " << Simulator::Now()
                << endl));
        DEBUG0(cout << MYIP << ": CTS_TIMEOUT at " << Simulator::Now()<<endl);
        shortRetryCount++;
        mac_state = IDLE;
        backingOff = false;
        backoffTimerState = NONE;
        CancelTimer(backoffTimeout);
        DATA_frame* df = (DATA_frame*)pendingPkt->PeekPDU();

        if (shortRetryCount > MMIB.ShortRetryLimit)
          {  // drop
	    shortRetryCount=0;
	    DEBUG0((cout <<"Reached maxlim " << df->ra << endl));
            DEBUG0((cout << "node " << iface->GetNode()->Id()
                    << " dropping due to CTSTimeout" << endl));
	    DropPacket("CTS-TO");
            if (!VcsBusy()) Busy2Idle(); // GFR
            break;
          }

        // Retry
        df->fc.fc_retry = 1;
        //if (MediumBusy()) InitiateBackoff();
        InitiateBackoff();
        break;
      }

    case MACTimerEvent::ACK_TIMEOUT:
      {
        ackTOCount++;
        DEBUG0(cout << MYIP << ": ACK_TIMEOUT at " << Simulator::Now()<<endl);
        DATA_frame* df = (DATA_frame*)pendingPkt->PeekPDU();
	MACAddr_t macadd = iface->GetMACAddr();
        DEBUG0((cout << "ACK_TIMEOUT, ra " << df->ra
	     << " mac " << macadd
	     << " btv.size() " << btv.size()
	     << " assocBSS " << assocBssid
	     << " lrc " << longRetryCount
             << " pkt " << pendingPkt << endl));
        longRetryCount++;
        mac_state = IDLE;
        backingOff = false;
        backoffTimerState = NONE;
        CancelTimer(backoffTimeout);
        if (longRetryCount > MMIB.LongRetryLimit)
          {  // drop
	    longRetryCount=0;
            // Drop any base station associations with this receiver
	    DEBUG0((cout << "ACK_TIMEOUT :: ForcedDisssoc(" << df->ra
                    << ") at "  << Simulator::Now() << endl));
	    DropPacket("ACK-TO");
            //ForcedDisassociation(df->ra);
	    //SendDisassocFrame(sifs);
            if (pendingBeacon) SendBeacon();
            break;
          }
        // Retry
        df->fc.fc_retry = 1;
        //if (MediumBusy()) InitiateBackoff();
	InitiateBackoff();

	//delete ev;
        break;
      }

    case MACTimerEvent::DATA_TIMEOUT:
      DEBUG0((cout << MYIP << ": DATA_TIMEOUT at " << Simulator::Now()
              << endl));
      DEBUG0(cout << "  mac_state set to IDLE\n");
      mac_state = IDLE;
      //delete ev;
      //if (pendingPkt && !MediumBusy()) InitiateBackoff();
      //if (!Busy()) DataRequestFromIFQ();
      break;

    case MACTimerEvent::LH_TIMEOUT:
      DEBUG0((cout << "HandlingCandRes at " << iface->GetMACAddr()
              << " for " << mtev->xmt
              << " with EventID " << mtev
              << " at " << Simulator::Now() << endl));
      DEBUG0((cout << iface->GetMACAddr() << " Inside LH_TIMEOUT:: for "
              << mtev->xmt << " at " << Simulator::Now() << std::endl));
      if ( ((InterfaceWireless*)iface)->GetOpMode()
	   == InterfaceWireless::HOSTAP)
	{
	  for( BridgeTable_t::iterator i = btv.begin(); i != btv.end(); ++i)
	    {
	      if (i->second == mtev->xmt)
		{ // Found it
		  IPAddr_t dstIP = i->first;
		  btv.erase(i);
		  SendDisassocFrame(i->second, sifs);

		  if(iface) {
		    // get the l2proto802.11 for iface
		    L2Proto802_11* pL2proto = (L2Proto802_11*)iface->GetL2Proto();
		    // notify EIGRP on interface down
		    DEBUG0((cout << "notifying EIGRP interface down for IP "
                            << IPAddr::ToDotted(dstIP)
                            << " at " << iface->GetMACAddr()
                            << " at " << Simulator::Now()
                            << " and MAC " << mtev->xmt << endl));
                    DEBUG0((cout << "node " << iface->GetNode()->Id()
                            << " eigrp notif down, lh_timeout "
                            << " ip " << (string)IPAddr(dstIP)
                            << " time " << Simulator::Now()
                            << endl));
		    pL2proto->SendNotification(dstIP);
		  }
		  return;
		}
	    }
	}
      else {
	DEBUG0((cout << "LHT size:: " << lastH.size() << endl));
	DEBUG0((cout << " BSS " << iface->GetMACAddr()
                << "dissociating at " << Simulator::Now() << endl));
	associated = false;
	assocBssid = MACAddr();
      }
       break;

    case MACTimerEvent::ASSOCIATION_TIMEOUT:
      associationPending = false;
      break;

    case MACTimerEvent::BEACON_TIMEOUT:
      cout << "Beacon timeout at time " << Simulator::Now() << endl;
      if (mac_state == IDLE) SendBeacon();  // Ok to send now
      else                   pendingBeacon = true;  // Send on next idle
      ScheduleBeaconTimer(ibfTime*1e-3);    // And restart the timer
      break;

    default:
      break;
    }
  //delete ev;
  // ev=nil;
}

void L2Proto802_11::ScheduleTimer(Event_t e, MACTimerEvent*& ev, Time_t t)
{
  if (t <= 0) {
	cout << "HuH? schedule negative time " << t << endl;
	return;
  }

  Time_t now = Simulator::Now();
  if (ev)
    {
      if (ev->Time() == t + now)
		return;
      CancelTimer(ev);
    }
  if (!ev)
    {
      ev = new MACTimerEvent(e);
    }
  timer.Schedule(ev, t, this);
}

void L2Proto802_11::CancelTimer(MACTimerEvent*& ev, bool deltimer)
{
  if (ev)
    {
      timer.Cancel(ev);
      if (deltimer)
        {
          delete ev;
          ev = nil;
        }
    }
}

void L2Proto802_11::SuspendTimer(MACTimerEvent* ev)
{
  // This function assumes backoff timer
  if (!ev) {
	cout << "HuH? no timer event to suspend\n";
	return;
  }
  timer.Pause(ev);
  backoffTimerState = SUSPENDED;
}

void L2Proto802_11::ResumeTimer(MACTimerEvent* ev)
{
  // This function assumes backoff timer
  if (!ev) {
	cout << "HuH? no timer event to resume\n";
	return;
  }
  timer.Resume(ev);
  backoffTimerState = RESUMED;
}

void L2Proto802_11::InitiateDataSequence()
{
  DATA_frame* df = (DATA_frame*)pendingPkt->PeekPDU();
  MACAddr src = df->addr2;
  MACAddr dst = df->addr1;

  /* After Backoff we can transmit immediately, no need to wait for IFS */

  if (dst.IsBroadcast()||pendingPkt->Size()<RTSThreshold)
   {
      //We use the standard Handling procedure for immediate Transmit
      // Setting IFS = 0
     SendDATA(); /* immediate cleanup */
    }
  else
    {
      Word_t duration = usec(sifs +
			     txtime(ETHER_CTS_LEN, basicRate)
                             + DSSS_MaxPropagationDelay
                             + sifs
                             + txtime(pendingPkt->Size(), dataRate)
                             + DSSS_MaxPropagationDelay
                             + sifs
                             + txtime(ETHER_ACK_LEN, basicRate)
                             + DSSS_MaxPropagationDelay);

      //Send RTS immediately no IFS required
      SendRTS(src,dst,duration); /* immediate cleanup */
    }
}

void L2Proto802_11::Idle2Busy()
{
  // This is called whenever the medium becomes busy
  DEBUG0(cout << MYIP << ": Idle2Busy() at " << Simulator::Now() << endl);

  if (!MediumBusy()) {  // this cannot happen
	cout << "HuH? medium is idle in Idle2Busy()\n";
  }

  if (backingOff) {  // in backoff procedure, need to suspend backoff timer
	if (backoffTimerState != SUSPENDED) {
	  SuspendTimer(backoffTimeout);
	  DEBUG0(cout << "  backoff timer suspened"
			 << " remaining_time " << backoffTimeout->remainingTime << endl);
	}
  }
}

void L2Proto802_11::Busy2Idle()
{
  // This is called whenever the medium becomes idle
  DEBUG0(cout << MYIP << ": Busy2Idle() at " << Simulator::Now() << endl);

  static Count_t b2i = 0;
  DEBUG0((cout << "node " << iface->GetNode()->Id()
          << " busy to idle, count " << ++b2i << " pp " << pendingPkt
          << " mac state " << mac_state
          << " assoc " << associated
          << " time " << Simulator::Now() << endl));
  if (!pendingPkt)
    { // Try to deque a packet and send it (GFR)
      //DataRequestFromIFQ();
      return;
    }

  if (mac_state != IDLE) return;
  /* mevFirstAttempt and mevBackoff are mutually excusive
	 Both cannot be on simultaneously
	 If mevFirstAttempt is on, don't initiate backoff procedure
  */
  if (mevFirstAttempt) return;
  InitiateBackoff();
}

void L2Proto802_11::DataRequestFromIFQ()
{
  static Count_t rfi = 0;
  DEBUG0((cout << "node " << iface->GetNode()->Id()
          << " rfifq, count " << ++rfi << " pp " << pendingPkt
          << " mac state " << mac_state
          << " length " << iface->GetQueue()->LengthPkts()
          << " assoc " << associated
          << " time " << Simulator::Now() << endl));
  // Get the next packet from ifque
  if (iface->GetQueue()->LengthPkts() > 0)
    {
      Packet* p = iface->GetQueue()->Deque();
      DEBUG0((cout << "node " << iface->GetNode()->Id()
              << " wireless deque, size " << iface->GetQueue()->LengthPkts()
              << endl));
      firstAttempt=true;
      DataRequest(p);
    }
}

void L2Proto802_11::InitiateFirstAttempt()
{
  if (mevBackoff) {
	/* This should NOT happen!!!
	   because mevFirstAttempt is mutually exclusive with mevBackoff
	*/
	cout << "HuH? backoff initiated with 1st attempt\n";
  }

  if (mevFirstAttempt) // MACEvent::PACKET_TX for 1st attempt already scheduled
	return;
  mevFirstAttempt = true;  // this is on for RTS or BC-DATA
  DATA_frame* df = (DATA_frame*)pendingPkt->PeekPDU();
  MACAddr dst = df->addr1;

  /* Beacons are high priority */

  //if (pendingBeacon) {
  //  ScheduleSendAfterIFS(pendingBeacon, sifs);
  // }  // Removed by GFR Sept 2007; don't understand why this is needed
  /* If the PAcket Size doesnt exceed RTSThreshold */
  if (dst.IsBroadcast()|| pendingPkt->Size()<RTSThreshold) {
	SendDATA();
  }
  else {
	MACAddr src = df->addr2;
	Word_t duration = usec(sifs
			       + txtime(ETHER_CTS_LEN, basicRate)
			       + DSSS_MaxPropagationDelay
			       + sifs
			       + txtime(pendingPkt->Size(), dataRate)
			       + DSSS_MaxPropagationDelay
			       + sifs
			       + txtime(ETHER_ACK_LEN, basicRate)
			       + DSSS_MaxPropagationDelay);
	SendRTS(src, dst, duration);
  }
}

void L2Proto802_11::InitiateBackoff()
{
  if (mevFirstAttempt) {
	/* This should NOT happen!!!
	   because mevBackoff is mutually exclusive with mevFirstAttempt
	*/
    cout << "HuH? 1st attempt initiated with backoff initiation\n";
  }

  if (mevBackoff) return;    // MACEvent::BACKOFF already scheduled
  mevBackoff = new MACEvent(MACEvent::BACKOFF);
  Scheduler::Schedule(mevBackoff, 0, this);
}

void L2Proto802_11::DropPacket(const string& reason, bool getNext)
{
  shortRetryCount = 0;
  longRetryCount = 0;

  // Trace this packet drop
  Stats::pktsDropped++; // Count dropped packets

  string r = "L2-" + reason;
  if (pendingPkt) {
    DropPDU d(r, pendingPkt);

    iface->GetNode()->TracePDU(this, &d);

    if (pendingPkt->notification)
      { // Sender has requested notification, so we need to
	// schedule one.  However, we need to avoid infinite loops,
	// as sometimes the notification causes an immediate DataRequest
	// of another packet.  So we calculate how long this tx
	// would have taken if successful, and notify after that amount
	// of time.
        // (Changed below to * 8 -SDR)
	Time_t txTime = pendingPkt->Size() * 8 / iface->GetLink()->Bandwidth();
	Simulator::instance->AddNotify(pendingPkt->notification, txTime, nil);
      }

    if (xmitFailHandler)
      {
	// L2 notifies L3 of transmit failure
	pendingPkt->PopPDU();    // Remove L2 header
	pendingPkt->PopPDU();    // Remove LLCSNAP header
        DEBUG0((cout << "xmitFailHandler, pp " << pendingPkt << endl));
	xmitFailHandler->Notify(pendingPkt);
      }
    else
      {
        delete pendingPkt;
      }
    pendingPkt = nil;
  }

  // L2 protocol became idle
  // Process next packet if existing
  if (getNext) DataRequestFromIFQ();
}

void L2Proto802_11::AddSeqCache(MACAddr ta, Word_t sn)
{
  if (seqCache.size() >= defaultSeqCacheSize) {
	// Cache is full, replace the oldest entry first
	seqCache[nextCacheEntry++] = SeqCacheEntry(ta, sn);
  }
  else {
	seqCache.push_back(SeqCacheEntry(ta, sn));
	nextCacheEntry++;
  }
  nextCacheEntry %= defaultSeqCacheSize;
}

bool L2Proto802_11::FindSeqCache(MACAddr ta, Word_t sn)
{
  for (SeqCache_t::iterator i = seqCache.begin(); i != seqCache.end(); ++i)
	if (i->ta == ta && i->sn == sn) return true;
  return false;
}

void L2Proto802_11::incr_cw()
{
  cw = (cw << 1) + 1;
  if (cw > PMIB.CWMax) cw = PMIB.CWMax;
}

void L2Proto802_11::Notify(void* v)
{
  /* Calling this function means MAC received
	 a PHY-RXSTART, PHY-RXEND, PHY-TXSTART, or PHY-TXEND.indication,
	 which means physical CS indicates busy/idle
	 Thus need to check if the medium became busy/idle
  */
  InterfaceWireless::PhyInd_t* phyInd = (InterfaceWireless::PhyInd_t*)v;
  DEBUG0((cout << "node " << iface->GetNode()->Id()
          << " notify, type " << *phyInd << endl));
  switch (*phyInd)
	{
	case InterfaceWireless::PHY_RXSTART:
	case InterfaceWireless::PHY_TXSTART:
	  /* This is a PHY-RXSTART or PHY-TXSTART.indication
		 PCS: was idle --> is busy
		 VCS: was idle ?
	  */
	  if (!VcsBusy()) Idle2Busy();
	  break;
	case InterfaceWireless::PHY_RXEND_ERR:
	case InterfaceWireless::PHY_RXEND:
	case InterfaceWireless::PHY_TXEND:
	  /* This is a PHY-RXEND or PHY-TXEND.indication
		 PCS: was busy --> is idle
		 VCS:          --> is idle ?
	  */
	  if (!VcsBusy()) Busy2Idle();
	  break;
	default:
	  break;
	} //switch
}

void L2Proto802_11::Bootstrap(void)
{
  /* Verify if we are a access point OR a station */
  InterfaceWireless* ifw = (InterfaceWireless*) iface;
  Uniform rng;
  Time_t time = rng.Value();
  if (ifw->GetOpMode() == InterfaceWireless::HOSTAP)
    {
      ScheduleBeaconTimer(time);
    }
}

void L2Proto802_11::AddToBTV(IPAddr_t ip, MACAddr macAddr)
{
  btv.insert(BridgeTable_t::value_type(ip, macAddr));
}

void L2Proto802_11::RemoveFromBTV(IPAddr_t ip)
{
  btv.erase(ip);
}

bool L2Proto802_11::SendBeacon()
{
  // Mac state should be idle here
  if (mac_state != IDLE)
    {
      cout << "HuH? SendBeacon on non-idle  channel" << endl;
      return false;
    }
  cout << "Node " << iface->GetNode()->Id()
       << " sending beacon at time " << Simulator::Now() << endl;
  ScheduleSendAfterIFS(BuildBeaconPacket(), difs);
  pendingBeacon = false;
  return true;
}
 
bool L2Proto802_11::SendAssocReq(Time_t time, MACAddr da)
{
  Assocreq_frame* AReqf = new Assocreq_frame(
      iface->GetMACAddr(), iface->GetIPAddr(), da);
  Packet* p = new Packet();
  p->PushPDU(AReqf);
  ScheduleSendAfterIFS(p, time);
  ScheduleTimer(MACTimerEvent::ASSOCIATION_TIMEOUT, associationTimeout, 0.5);
  associationPending = true;
  cout << "Node " << iface->GetNode()->Id()
       << " sending assoc reqeust at time " << Simulator::Now() << endl;
  return true;
}

bool L2Proto802_11::SendAssocResp(Time_t time, MACAddr da)
{

  Assocresp_frame* ARespf = new Assocresp_frame(iface->GetMACAddr(),
						da);
  Packet* p = new Packet();
  p->PushPDU(ARespf);

  ScheduleSendAfterIFS(p, time);

  return true;
}

bool L2Proto802_11::SendDisassocFrame(MACAddr addr_, Time_t time)
{
    Disassoc_frame* Disassf = new Disassoc_frame(
        iface->GetMACAddr(),
        iface->GetIPAddr(),
        addr_);
    Packet* p = new Packet();
    p->PushPDU(Disassf);

    ScheduleSendAfterIFS(p, time);
    return true;
}

bool L2Proto802_11::SendDisassocFrame(Time_t time)
{
  if (associated) {
    Disassoc_frame* Disassf = new Disassoc_frame(
        iface->GetMACAddr(),
        iface->GetIPAddr(),
        assocBssid);
    Packet* p = new Packet();
    p->PushPDU(Disassf);

    ScheduleSendAfterIFS(p, time);
    associated = false;
    return true;
  }
  else
    return false;
}


bool L2Proto802_11::SendReassocReq(Time_t time, MACAddr da)
{
  Reassocreq_frame* AReqf = new Reassocreq_frame(iface->GetMACAddr(), da);
  Packet* p = new Packet();
  p->PushPDU(AReqf);
  ScheduleSendAfterIFS(p, time);
  return true;
}

void L2Proto802_11::AddNotify(NotifyHandler* n, void* v)
{ // Add a notify client
  if (notifications.empty())
    notifications.push_back(Notification(n,v));
  else {
    NList_t::iterator i;
    for (i = notifications.begin(); i != notifications.end(); ++i)
      if (*((Time_t*)v) < *((Time_t*)i->userData)) break;
    notifications.insert(i, Notification(n,v));
  }
}

void L2Proto802_11::SendNotification(IPAddr_t ip)
{ // Notify the entry in the notificatin list
  if (notifications.size() == 0) return;   // No pending
  Notification n = notifications.front();  // Get next notifier
  notifications.pop_front();               // And remove it
  n.handler->Notify((void*)ip);           // Call the notification handler
}

Packet* L2Proto802_11::BuildBeaconPacket()
{
  Beacon_frame* bf = new Beacon_frame(iface->GetMACAddr());

  bf->timestamp = Simulator::Now();
  bf->beacon_int = ibfTime;
  bf->cap_info = 0x00;

  InterfaceWireless* ifw = (InterfaceWireless*) iface;
  if(ifw->GetOpMode() == InterfaceWireless::HOSTAP)
    bf->cap_info &= 0x8000;
  else                        /* Beacons can also be sent in Ad-Hoc mode */
    bf->cap_info &= 0x4000;

  bf->ssidel.el_id = 0;
  bf->ssidel.len = 0x20;
  bf->ssidel.ssid = ((InterfaceWireless*)(iface))->getSSID();

  bf->ssrel.el_id = 1;
  bf->ssrel.len = 8;
  bf->ssrel.supp[0] = 0x82;
  bf->ssrel.supp[1] = 0x04;
  bf->ssrel.supp[2] = 0x0B;
  bf->ssrel.supp[3] = 0x16;

  bf->dsps.el_id = 3;
  bf->dsps.len = 1;
  bf->dsps.currChan = 6;

  bf->tim.el_id = 5;
  bf->tim.len = 0; /* No such info until i'm clear about functionality */

  Packet* p = new Packet();

  p->PushPDU(bf);
  return p;
}

void L2Proto802_11::ScheduleBeaconTimer(Time_t t)
{
  ScheduleTimer(MACTimerEvent::BEACON_TIMEOUT, beaconTimeout, t);
}
  
// L2802-3.11 header definitions
void L2Proto802_11::ForcedDisassociation(MACAddr m)
{
  InterfaceWireless* ifw = (InterfaceWireless*)iface;
  if (ifw->GetOpMode() == InterfaceWireless::BSS)
    { // See if the failed mac is our current BS.  Disassociate if so
      if (associated && m == assocBssid && !associationPending)
        { // Yep, ours failed, remove the association
	  DEBUG0((cout << "Node " << ifw->GetNode()->Id()
                  << " forced disassoc from bssid " << assocBssid
                  << endl));
          associated = false;
          assocBssid = MACAddr();
	  mac_state = IDLE;
        }
      return;
    }
  if (ifw->GetOpMode() == InterfaceWireless::HOSTAP)
    { // Host Adapter mode, remove this guy from the bridge table
      for( BridgeTable_t::iterator i = btv.begin(); i != btv.end(); ++i)
        {
          if (i->second == m)
            { // Found it
	      IPAddr_t dstIP = i->first;
              btv.erase(i);

	      if(iface) {
		// get the l2proto802.11 for iface
		L2Proto802_11* pL2proto = (L2Proto802_11*)iface->GetL2Proto();
		// notify EIGRP on interface down
		DEBUG0((cout << "notifying EIGRP on interface down for IP "
                        << IPAddr::ToDotted(dstIP) << std::endl));
                DEBUG0((cout << "node " << iface->GetNode()->Id()
                        << " eigrp forced disassoc "
                        << " time " << Simulator::Now()
                        << endl));
		pL2proto->SendNotification(dstIP);
	      }
              return;
            }
        }
    }
}

Rate_t L2Proto802_11::DataRate(void)
{
  return dataRate;
}

void L2Proto802_11::DataRate(Rate_t rate)
{
  dataRate = rate;
}

Rate_t L2Proto802_11::BasicRate(void)
{
  return basicRate;
}

void L2Proto802_11::BasicRate(Rate_t rate)
{
  basicRate = rate;
}

// Static methods
void L2Proto802_11::DefaultBasicRate(Rate_t dbr)
{
  defaultBasicRate = dbr;
}

void L2Proto802_11::DefaultDataRate(Rate_t ddr)
{
  defaultDataRate = ddr;
}

// L2802-11 header definitions
void L2Header802_11::CommonPDUTrace(Tfstream& ts, Bitmap_t,
                                    const char* s, const char* v)
{
  ts << " ";
  if (s) ts << s;
  // ! Need the detail flags here!
  ts << "L2-802.11-" << v
     <<  " " << ta << " " << ra;;
}



// RTS_frame
RTS_frame::RTS_frame (Word_t dur, MACAddr rcv, MACAddr xmt, Long_t fcs_)
  : L2Header802_11(dur, fcs_, RTS)
{
  fc.fc_proto_ver  = MAC_ProtocolVersion;
  fc.fc_type       = MAC_Type_Control;
  fc.fc_subtype    = MAC_Subtype_RTS;
  fc.fc_to_ds      = 0;
  fc.fc_from_ds    = 0;
  fc.fc_more_frag  = 0;
  fc.fc_retry      = 0;
  fc.fc_pwr_mgt    = 0;
  fc.fc_more_data  = 0;
  fc.fc_wep        = 0;
  fc.fc_order      = 0;

  ra   = rcv;
  ta   = xmt;
  L2Proto802_11::rtsCount++;
}

RTS_frame::RTS_frame(const RTS_frame& f)
  : L2Header802_11(f)
{
  L2Proto802_11::rtsCount++;
}


Size_t RTS_frame::Size() const
{
  return ETHER_RTS_LEN;
}

void   RTS_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{ // Trace this PDU
  CommonPDUTrace(ts, b, s, "RTS");
}



// CTS_frame
CTS_frame::CTS_frame(Word_t dur, MACAddr rcv, Long_t fcs_)
  : L2Header802_11(dur, fcs_, CTS)
{
  fc.fc_proto_ver  = MAC_ProtocolVersion;
  fc.fc_type       = MAC_Type_Control;
  fc.fc_subtype    = MAC_Subtype_CTS;
  fc.fc_to_ds      = 0;
  fc.fc_from_ds    = 0;
  fc.fc_more_frag  = 0;
  fc.fc_retry      = 0;
  fc.fc_pwr_mgt    = 0;
  fc.fc_more_data  = 0;
  fc.fc_wep        = 0;
  fc.fc_order      = 0;

  ra = rcv;
  L2Proto802_11::ctsCount++;
}

CTS_frame::CTS_frame(const CTS_frame& f)
  : L2Header802_11(f)
{
  L2Proto802_11::ctsCount++;
}

Size_t CTS_frame::Size() const
{
  return ETHER_CTS_LEN;
}

void   CTS_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{ // Trace this PDU
  CommonPDUTrace(ts, b, s, "CTS");
}

// ACK_frame
ACK_frame::ACK_frame(Word_t dur, MACAddr rcv, Long_t fcs_)
  : L2Header802_11(dur, fcs_, ACK)
{
  fc.fc_proto_ver  = MAC_ProtocolVersion;
  fc.fc_type       = MAC_Type_Control;
  fc.fc_subtype    = MAC_Subtype_ACK;
  fc.fc_to_ds      = 0;
  fc.fc_from_ds    = 0;
  fc.fc_more_frag  = 0;
  fc.fc_retry      = 0;
  fc.fc_pwr_mgt    = 0;
  fc.fc_more_data  = 0;
  fc.fc_wep        = 0;
  fc.fc_order      = 0;

  ra = rcv;
  L2Proto802_11::ackCount++;
}

ACK_frame::ACK_frame(const ACK_frame& f)
  : L2Header802_11(f)
{
  L2Proto802_11::ackCount++;
}

Size_t ACK_frame::Size() const
{
  return ETHER_ACK_LEN;
}

void   ACK_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{ // Trace this PDU
  CommonPDUTrace(ts, b, s, "ACK");
}

// DATA_frame
DATA_frame::DATA_frame(Word_t dur, MACAddr rcv, MACAddr xmt, Word_t sqn, Long_t fcs_)
  : L2Header802_11(dur, fcs_, DATA)
{
  fc.fc_proto_ver  = MAC_ProtocolVersion;
  fc.fc_type       = MAC_Type_Data;
  fc.fc_subtype    = MAC_Subtype_DATA;
  fc.fc_to_ds      = 0;
  fc.fc_from_ds    = 0;
  fc.fc_more_frag  = 0;
  fc.fc_retry      = 0;
  fc.fc_pwr_mgt    = 0;
  fc.fc_more_data  = 0;
  fc.fc_wep        = 0;
  fc.fc_order      = 0;

  /* could juggle with to_ds and from_ds fields here */

  ra = addr1 = rcv;
  ta = addr2 = xmt;
  addr3 = MACAddr::NONE;
  addr4 = MACAddr::NONE;
  seqno = sqn;
  L2Proto802_11::dataCount++;
}

Count_t dfCopies = 0;

DATA_frame::DATA_frame(const DATA_frame& f)
  : L2Header802_11(f),
    addr1(f.addr1), addr2(f.addr2), addr3(f.addr3), addr4(f.addr4),
    seqno(f.seqno)
{
  L2Proto802_11::dataCount++;
  dfCopies++;
}

Size_t DATA_frame::Size() const
{
  return ETHER_DATA_LEN;
}

void   DATA_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{ // Trace this PDU
  CommonPDUTrace(ts, b, s, "DATA");
}


// BSSBaseFrame methods
void BSSBaseFrame::CommonTrace(Tfstream& ts, Bitmap_t,
                               const char* s, const char* v)
{
  ts << " ";
  if (s) ts << s;
  // ! Need the detail flags here!
  ts << "L2-802.11-" << v
     <<  " " << ta << " " << ra << " " << seqno;
}

Beacon_frame::Beacon_frame(const Beacon_frame& f)
  : BSSBaseFrame(f),
    timestamp(f.timestamp), beacon_int(f.beacon_int),
    cap_info(f.cap_info), ssidel(f.ssidel),
    ssrel(f.ssrel), fhps(f.fhps), dsps(f.dsps),
    ibssps(f.ibssps), tim(f.tim)
{
  L2Proto802_11::beaconCount++;
}

Size_t Beacon_frame::Size() const
{
  return 100;
}

void   Beacon_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{
  CommonTrace(ts, b, s, "BEACON");
}

Size_t Disassoc_frame::Size() const
{
  return 100;
}

void   Disassoc_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{
  CommonTrace(ts, b, s, "DISASSOC");
  ts << " " << reason_code;
}

Size_t Assocreq_frame::Size() const
{
  return 100;
}

void   Assocreq_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{
  CommonTrace(ts, b, s, "ASSOCREQ");
}

Size_t Assocresp_frame::Size() const
{
  return 100;
}

void   Assocresp_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{
  CommonTrace(ts, b, s, "ASSOCRESP");
}

Size_t Reassocreq_frame::Size() const
{
  return 100;
}

void   Reassocreq_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{
  CommonTrace(ts, b, s, "REASSOCREQ");
}

Size_t Reassocresp_frame::Size() const
{
  return 100;
}

void   Reassocresp_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{
  CommonTrace(ts, b, s, "REASSOCRESP");
}

Size_t Probereq_frame::Size() const
{
  return 100;
}

void   Probereq_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{
  CommonTrace(ts, b, s, "PROBEREQ");
}

Size_t Proberesp_frame::Size() const
{
  return 100;
}

void   Proberesp_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{
  CommonTrace(ts, b, s, "PROBERESP");
}

Size_t Auth_frame::Size() const
{
  return 100;
}

void   Auth_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{
  CommonTrace(ts, b, s, "AUTH");
}

Size_t Deauth_frame::Size() const
{
  return 100;
}

void   Deauth_frame::Trace(Tfstream& ts, Bitmap_t b, Packet*, const char* s)
{
  CommonTrace(ts, b, s, "DEAUTH");
}


