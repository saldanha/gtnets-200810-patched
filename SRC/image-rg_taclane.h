// Generated automatically by make-gtimage from ../IMAGES/rg_taclane.png
// DO NOT EDIT

#include "image.h"

class rg_taclaneImage : public Image {
public:
  rg_taclaneImage(){}
  const char* Data() const { return data;}
  int Size() const { return size;}
  operator const char*() { return data;}
  static const char* data;
  static int size;
};
