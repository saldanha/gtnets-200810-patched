// Sample test program to instantiate a BGP router
// Sunitha Beeram, Georgia Tech, Spring 2005

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "bgp.h"


using namespace std;

int main(int argc,char ** argv)
{
  Simulator Sim;

  int StopTime=100;


  /* If stop time is specified as cli arg*/

  if ( argc > 1)
  {
	StopTime= atoi(argv[1]);
	
	if (StopTime < 100)
		StopTime =100;
  }

  char command1[] = "show ip prefix-list PR_LIST";
  char command2[] = "show ip bgp";
  char command3[] = "no ip prefix-list PR_LIST seq 10 permit 0.0.0.0/0 le 19";
  char command4[] = "ip prefix-list PR_LIST seq 10 permit 0.0.0.0/0 ge 18";
  char command5[] = "clear ip bgp 1";

  Node::DefaultShape(Node::CIRCLE);
  //Linkp2p lk(Rate("1.5Mb"), Time("1ms"));
 Linkp2p lk(Rate("500Kb"), Time("20ms"));
  Uniform startRng(0,0.1);

  /*First BGP Router*/
  Node* node1 = new Node();
  BGP* bgpRouter1 = new BGP(0);

  bgpRouter1->AttachNode(node1);
  bgpRouter1->config_file("./bgpd1.conf");
  node1->SetLocation(4,0);

  /*Second BGP Router*/
  Node *node2 = new Node();
  BGP* bgpRouter2 = new BGP(0);
  
  bgpRouter2->AttachNode(node2);
  bgpRouter2->config_file("./bgpd2.conf");
  node2->SetLocation(6,0);
  node1->AddDuplexLink(node2,lk);

  BGPEvent *ev1 = new BGPEvent(BGPEvent::COMMAND);
  ev1->argv= (char*)malloc(sizeof(char)*(strlen(command1)+1));
  strcpy(ev1->argv,command1);

  BGPEvent *ev2 = new BGPEvent(BGPEvent::COMMAND);
  ev2->argv= (char*)malloc(sizeof(char)*(strlen(command2)+1));
  strcpy(ev2->argv,command2);

  BGPEvent *ev3 = new BGPEvent(BGPEvent::COMMAND);
  ev3->argv=(char*)malloc(sizeof(char)*(strlen(command3)+1));
  strcpy(ev3->argv,command3);

  BGPEvent *ev4 = new BGPEvent(BGPEvent::COMMAND);
  ev4->argv= (char*)malloc(sizeof(char)*(strlen(command4)+1));
  strcpy(ev4->argv,command4);


  BGPEvent *ev5 = new BGPEvent(BGPEvent::COMMAND);
  ev5->argv= (char*)malloc(sizeof(char)*(strlen(command5)+1));
  strcpy(ev5->argv,command5);

  BGPEvent *ev6 = new BGPEvent(BGPEvent::COMMAND);
  ev6->argv = (char*)malloc(sizeof(char)*(strlen(command2)+1));
  strcpy(ev6->argv,command2);

  bgpRouter2->timer->Schedule(10,ev1);
  bgpRouter2->timer->Schedule(40,ev2);
  bgpRouter2->timer->Schedule(50,ev3);
  bgpRouter2->timer->Schedule(50,ev4);
  bgpRouter2->timer->Schedule(51,ev5);
  bgpRouter2->timer->Schedule(StopTime-1,ev6);

  bgpRouter1->StartAt(0.1);
  bgpRouter2->StartAt(0.1);
  bgpRouter1->StopAt(StopTime);
  bgpRouter2->StopAt(StopTime);
  
  Sim.StopAt(StopTime);
  Sim.Run();
}
 
