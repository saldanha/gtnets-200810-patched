#include <iostream>
#include <sstream>
#include <string>
#include <strstream>

#include "debug.h"
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "node-satellite.h"
#include "node-taclane.h"
#include "star.h"
#include "linkp2p.h"
#include "application-cbr.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "application-udpsink.h"
#include "rng.h"
#include "routing.h"
#include "routing-manual.h"
#include "diffserv-queue.h"
#include "time-value.h"
#include "time-value-graph.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "udp.h"
#include "validation.h"
#include "qtwindow.h"

// Images
#include "image-RedRouter.h"
#include "image-BlackRouter.h"
#include "image-GreenRouter.h"
#include "image-Host.h"
#include "image-HostSmall.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

using namespace std;



int main(int argc, char** argv)
{
	
	Validation::Init(argc, argv);
	Simulator s;
	// Set node shape to a circle for animation
	Node::DefaultShape(Node::CIRCLE);
	
//	// Set manual routing
//	Routing::SetRouting(new RoutingManual());
	
/*	Trace* tr = Trace::Instance(); // Get a pointer to global trace object
	tr->Open("testsat1.txt");
	IPV4::Instance()->SetTrace(Trace::ENABLED);
*/	
	s.StartAnimation(0, true);
	
	
	/******************************JRAE 04-4 IP Address Initialization******************************/
	
	//PEO C3T - Ft. Hood, TX network elements
	IPAddr_t abrigade = IPAddr("192.169.1.1");			//Brigade
	IPAddr_t aR40_LAN = IPAddr("192.169.1.2");			//Brigade
	IPAddr_t aR40_H8 = IPAddr("192.168.1.2");			//Brigade
	IPAddr_t aH8_R40 = IPAddr("192.168.1.1");			//Brigade
	IPAddr_t aH8_R41 = IPAddr("134.207.136.186");		//Brigade
	IPAddr_t aR41_H8 = IPAddr("134.207.136.185");		//Brigade
	IPAddr_t aR41_SAT2b = IPAddr("134.207.136.98");		//Brigade
	
	//JITC - Ft. Huachuca, AZ network elements
	IPAddr_t asoc = IPAddr("192.169.2.1");				
	IPAddr_t aR23_LAN = IPAddr("192.169.2.2");			//Spec. Ops. Command
	IPAddr_t aR23_H5 = IPAddr("192.168.2.2");			//Spec. Ops. Command
	IPAddr_t aH5_R23 = IPAddr("192.168.2.1");			//Spec. Ops. Command
	IPAddr_t aH5_R8 = IPAddr("134.207.136.34");			//Spec. Ops. Command
	
	//AFCA - Scott AFB, IL network elements
	IPAddr_t afs1c = IPAddr("192.169.10.1");			//Fighter Squadron 1 Commander
	IPAddr_t aR22_LAN = IPAddr("192.169.10.2");			//Fighter Squadron 1 Commander
	IPAddr_t aR22_H6 = IPAddr("192.168.10.2");			//Fighter Squadron 1 Commander
	
	//BCBL - Ft. Gordon, GA network elements
	IPAddr_t abatallion = IPAddr("192.169.3.1");		//Batallion
	IPAddr_t aR25_LAN = IPAddr("192.169.3.2");			//Batallion
	IPAddr_t aR25a_H9 = IPAddr("192.168.3.2");			//Batallion
	IPAddr_t aH9_R25a = IPAddr("192.168.3.1");			//Batallion
	IPAddr_t aH9_R14 = IPAddr("134.207.136.110");		//Batallion
	IPAddr_t aR14_H9 = IPAddr("134.207.136.109");		//Batallion
	IPAddr_t aR14_SAT2a = IPAddr("134.207.136.106");	//Batallion
	IPAddr_t aR14_R15 = IPAddr("134.207.136.158");		//Batallion
	IPAddr_t aplatoon = IPAddr("192.169.4.1");			//Platoon
	IPAddr_t aR26_LAN = IPAddr("192.169.4.2");			//Platoon
	IPAddr_t aR26_H10 = IPAddr("192.168.4.2");			//Platoon
	IPAddr_t aH10_R26 = IPAddr("192.168.4.1");			//Platoon
	IPAddr_t aH10_R13 = IPAddr("134.207.136.102");		//Platoon
	IPAddr_t aR13_H10 = IPAddr("134.207.136.101");		//Platoon
	IPAddr_t aR13_R19 = IPAddr("134.207.136.150");		//Platoon
	
	//NRL - Washington, D.C. network elements
	IPAddr_t aR12_R7 = IPAddr("134.207.136.46");		//Teleport 2
	IPAddr_t aR12_SAT2d = IPAddr("134.207.136.86");		//Teleport 2
	IPAddr_t aR12_SAT2c = IPAddr("134.207.136.89");		//Teleport 2
	IPAddr_t aR12_SAT2b = IPAddr("134.207.136.97");		//Teleport 2
	IPAddr_t aR12_SAT2a = IPAddr("134.207.136.105");	//Teleport 2
	IPAddr_t auav = IPAddr("192.169.5.1");				//UAV
	IPAddr_t aR27_LAN = IPAddr("192.169.5.2");			//UAV
	IPAddr_t aR27_H3 = IPAddr("192.168.5.2");			//UAV
	IPAddr_t aH3_R27 = IPAddr("192.168.5.1");			//UAV
	IPAddr_t aH3_R6 = IPAddr("134.207.136.78");			//UAV
	IPAddr_t aR6_H3 = IPAddr("134.207.136.77");			//UAV
	IPAddr_t aR6_SAT1a = IPAddr("134.207.136.74");		//UAV
	IPAddr_t aR4_SAT1c = IPAddr("134.207.136.173");		//Teleport 1
	IPAddr_t aR4_SAT1b = IPAddr("134.207.136.65");		//Teleport 1
	IPAddr_t aR4_SAT1a = IPAddr("134.207.136.73");		//Teleport 1
	IPAddr_t aR4_R2 = IPAddr("134.207.136.18");			//Teleport 1
	IPAddr_t aR43_H20 = IPAddr("192.168.6.2");			//GIG-BE POP1
	IPAddr_t aH20_R43 = IPAddr("192.168.6.1");			//GIG-BE POP1
	IPAddr_t aH20_R42 = IPAddr("134.207.136.190");		//GIG-BE POP1
	IPAddr_t aR42_H20 = IPAddr("134.207.136.189");		//GIG-BE POP1
	IPAddr_t aR42_R2 = IPAddr("134.207.136.38");		//GIG-BE POP1
	IPAddr_t aR2_R1 = IPAddr("134.207.136.6");			//GIG-BE POP1
	IPAddr_t aR2_R25b = IPAddr("134.207.136.57");		//GIG-BE POP1
	IPAddr_t aR2_R4 = IPAddr("134.207.136.17");			//GIG-BE POP1
	IPAddr_t aR2_R7 = IPAddr("134.207.136.25");			//GIG-BE POP1
	IPAddr_t aR2_R8 = IPAddr("134.207.136.21");			//GIG-BE POP1
	IPAddr_t aR2_R3 = IPAddr("134.207.136.9");			//GIG-BE POP1
	IPAddr_t aR2_R42 = IPAddr("134.207.136.37");		//GIG-BE POP1
	IPAddr_t aaorc = IPAddr("192.169.7.1");			//AOR Commander
	IPAddr_t aR28_LAN = IPAddr("192.169.7.2");			//AOR Commander
	IPAddr_t aR28_H18 = IPAddr("192.168.7.2");			//AOR Commander
	IPAddr_t aH18_R28 = IPAddr("192.168.7.1");			//AOR Commander
	IPAddr_t aH18_R25b = IPAddr("134.207.136.62");		//AOR Commander
	IPAddr_t aR25b_H18 = IPAddr("134.207.136.61");		//AOR Commander
	IPAddr_t aR25b_R2 = IPAddr("134.207.136.58");		//AOR Commander
	
	//MIT-LL - Lexington, MA network elements
	IPAddr_t aR8_H5 = IPAddr("134.207.136.33");			//GIG-BE POP3
	IPAddr_t aR8_R2 = IPAddr("134.207.136.22");			//GIG-BE POP3
	IPAddr_t aR8_R7 = IPAddr("134.207.136.30");			//GIG-BE POP3
	IPAddr_t aR8_R16 = IPAddr("134.207.136.41");		//GIG-BE POP3
	IPAddr_t aSAT1a_R6 = IPAddr("134.207.137.73");		//Satellite 1
	IPAddr_t aSAT1a_R4 = IPAddr("134.207.137.74");		//Satellite 1
	IPAddr_t aSAT1b_R5 = IPAddr("134.207.137.65");		//Satellite 1
	IPAddr_t aSAT1b_R4 = IPAddr("134.207.137.66");		//Satellite 1
	IPAddr_t aSAT1c_R24 = IPAddr("134.207.137.173");	//Satellite 1
	IPAddr_t aSAT1c_R4 = IPAddr("134.207.137.174");		//Satellite 1
	IPAddr_t aSAT2a_R14 = IPAddr("134.207.137.105");	//Satellite 2
	IPAddr_t aSAT2a_R12 = IPAddr("134.207.137.106");	//Satellite 2
	IPAddr_t aSAT2b_R41 = IPAddr("134.207.137.97");		//Satellite 2
	IPAddr_t aSAT2b_R12 = IPAddr("134.207.137.98");		//Satellite 2
	IPAddr_t aSAT2c_R11 = IPAddr("134.207.137.89");		//Satellite 2
	IPAddr_t aSAT2c_R12 = IPAddr("134.207.137.90");		//Satellite 2
	IPAddr_t aSAT2d_R10 = IPAddr("134.207.137.86");		//Satellite 2
	IPAddr_t aSAT2d_R12 = IPAddr("134.207.137.85");		//Satellite 2
	IPAddr_t aSAT3_R17 = IPAddr("134.207.137.121");		//Satellite 3
	IPAddr_t aSAT3_R16 = IPAddr("134.207.137.122");		//Satellite 3
	
	//DISA network elements
	IPAddr_t aR7_R8 = IPAddr("134.207.136.29");			//GIG-BE POP2
	IPAddr_t aR7_R12 = IPAddr("134.207.136.45");		//GIG-BE POP2
	IPAddr_t aR7_R11 = IPAddr("134.207.136.49");		//GIG-BE POP2
	IPAddr_t aR7_R10 = IPAddr("134.207.136.53");		//GIG-BE POP2
	IPAddr_t aR7_R2 = IPAddr("134.207.136.26");			//GIG-BE POP2
	
	//JTEO - McLean, VA network elements
	IPAddr_t afs2c = IPAddr("192.169.8.1");				//Fighter Squadron 2 Commander
	IPAddr_t aR29_LAN = IPAddr("192.169.8.2");			//Fighter Squadron 2 Commander
	IPAddr_t aR29_H2 = IPAddr("192.168.8.2");			//Fighter Squadron 2 Commander
	IPAddr_t aH2_R29 = IPAddr("192.168.8.1");			//Fighter Squadron 2 Commander
	IPAddr_t aH2_R5 = IPAddr("134.207.136.70");			//Fighter Squadron 2 Commander
	IPAddr_t aR5_H2 = IPAddr("134.207.136.69");			//Fighter Squadron 2 Commander
	IPAddr_t aR5_SAT1b = IPAddr("134.207.136.66");		//Fighter Squadron 2 Commander
	IPAddr_t auavcrew = IPAddr("192.169.9.1");			//UAV Crew
	IPAddr_t aR30_LAN = IPAddr("192.169.9.2");			//UAV Crew
	IPAddr_t aR30_H4 = IPAddr("192.168.9.2");			//UAV Crew
	IPAddr_t aH4_R30 = IPAddr("192.168.9.1");			//UAV Crew
	IPAddr_t aH4_R3 = IPAddr("134.207.136.14");			//UAV Crew
	IPAddr_t aR3_H4 = IPAddr("134.207.136.13");			//UAV Crew
	IPAddr_t aR3_R2 = IPAddr("134.207.136.10");			//UAV Crew
	
	//SSC-SD - San Diego, CA network elements
	IPAddr_t aH6_R22 = IPAddr("192.168.10.1");			//CJTF
	IPAddr_t aH6_R10 = IPAddr("134.207.136.82");		//CJTF
	IPAddr_t aR10_H6 = IPAddr("134.207.136.81");		//CJTF
	IPAddr_t aR10_R7 = IPAddr("134.207.136.54");		//CJTF
	IPAddr_t aR10_SAT2d = IPAddr("134.207.136.85");		//CJTF
	IPAddr_t ajsotf = IPAddr("192.169.11.1");			//JSOTF
	IPAddr_t aR31_LAN = IPAddr("192.169.11.2");			//JSOTF
	IPAddr_t aR31_H17 = IPAddr("192.168.11.2");			//JSOTF
	IPAddr_t aH17_R31 = IPAddr("192.168.11.1");			//JSOTF
	IPAddr_t aH17_R24 = IPAddr("134.207.136.178");		//JSOTF
	IPAddr_t aR24_H17 = IPAddr("134.207.136.177");		//JSOTF
	IPAddr_t aR24_SAT1c = IPAddr("134.207.136.174");	//JSOTF
	IPAddr_t aship2g = IPAddr("192.169.12.1");			//ESG Commander (Ship 2)
	IPAddr_t aR32_LAN = IPAddr("192.169.12.2");			//ESG Commander (Ship 2)
	IPAddr_t aR32_H14 = IPAddr("192.168.12.2");			//ESG Commander (Ship 2)
	IPAddr_t aH14_R32 = IPAddr("192.168.12.1");			//ESG Commander (Ship 2)
	IPAddr_t aH14_R17 = IPAddr("134.207.136.130");		//ESG Commander (Ship 2)
	IPAddr_t aship2r = IPAddr("192.169.13.1");			//ESG Commander (Ship 2)
	IPAddr_t aR33_LAN = IPAddr("192.169.13.2");			//ESG Commander (Ship 2)
	IPAddr_t aR33_H13 = IPAddr("192.168.13.2");			//ESG Commander (Ship 2)
	IPAddr_t aH13_R33 = IPAddr("192.168.13.1");			//ESG Commander (Ship 2)
	IPAddr_t aH13_R17 = IPAddr("134.207.136.126");		//ESG Commander (Ship 2)
	IPAddr_t aship2tl = IPAddr("192.168.14.2");			//ESG Commander (Ship 2)
	IPAddr_t aH16_LAN = IPAddr("192.168.14.1");			//ESG Commander (Ship 2)
	IPAddr_t aH16_R17 = IPAddr("134.207.136.182");		//ESG Commander (Ship 2)
	IPAddr_t aR17_SAT3 = IPAddr("134.207.136.122");		//ESG Commander (Ship 2)
	IPAddr_t aR17_H16 = IPAddr("134.207.136.181");		//ESG Commander (Ship 2)
	IPAddr_t aR17_H13 = IPAddr("134.207.136.125");		//ESG Commander (Ship 2)
	IPAddr_t aR17_H14 = IPAddr("134.207.136.129");		//ESG Commander (Ship 2)
	IPAddr_t aR17_R18 = IPAddr("134.207.136.166");		//ESG Commander (Ship 2)
	IPAddr_t aR20_R21 = IPAddr("134.207.136.137");		//JTRS Cloud
	IPAddr_t aR20_JTRS = IPAddr("134.207.136.138");		//JTRS Cloud
	IPAddr_t aR19_R13 = IPAddr("134.207.136.145");		//JTRS Cloud
	IPAddr_t aR19_JTRS = IPAddr("134.207.136.146");		//JTRS Cloud
	IPAddr_t aR18_R17 = IPAddr("134.207.136.161");		//JTRS Cloud
	IPAddr_t aR18_JTRS = IPAddr("134.207.136.162");		//JTRS Cloud
	IPAddr_t aR15_R14 = IPAddr("134.207.136.153");		//JTRS Cloud
	IPAddr_t aR15_JTRS = IPAddr("134.207.136.154");		//JTRS Cloud
	IPAddr_t aJTRS_R20 = IPAddr("134.207.136.139");		//JTRS Cloud
	IPAddr_t aJTRS_R19 = IPAddr("134.207.136.147");		//JTRS Cloud
	IPAddr_t aJTRS_R18 = IPAddr("134.207.136.163");		//JTRS Cloud
	IPAddr_t aJTRS_R15 = IPAddr("134.207.136.155");		//JTRS Cloud
	
	//CPSG - Brooks AFB, SC network elements
	IPAddr_t aaoc1 = IPAddr("192.168.15.2");			//Air Ops Center
	IPAddr_t aH7_LAN = IPAddr("192.168.15.1");			//Air Ops Center
	IPAddr_t aH7_R11 = IPAddr("134.207.136.94");		//Air Ops Center
	IPAddr_t aaoc2 = IPAddr("192.168.16.2");			//Air Ops Center
	IPAddr_t aH19_LAN = IPAddr("192.168.16.1");			//Air Ops Center
	IPAddr_t aH19_R11 = IPAddr("134.207.136.194");		//Air Ops Center
	IPAddr_t aR11_SAT2c = IPAddr("134.207.136.90");		//Air Ops Center
	IPAddr_t aR11_H19 = IPAddr("134.207.136.193");		//Air Ops Center
	IPAddr_t aR11_H7 = IPAddr("134.207.136.93");		//Air Ops Center
	IPAddr_t aR11_R7 = IPAddr("134.207.136.50");		//Air Ops Center
	IPAddr_t aap = IPAddr("192.168.17.2");				//Air Platform
	IPAddr_t aH15_LAN = IPAddr("192.168.17.1");			//Air Platform
	IPAddr_t aH15_R21 = IPAddr("134.207.136.134");		//Air Platform
	IPAddr_t aR21_H15 = IPAddr("134.207.136.133");		//Air Platform
	IPAddr_t aR21_R20 = IPAddr("134.207.136.142");		//Air Platform
	
	//SSC-CH - Charleston, SC network elements
	IPAddr_t aisufist = IPAddr("192.169.18.1");			//CONUS ISU/FIST
	IPAddr_t aR34_LAN = IPAddr("192.169.18.2");			//CONUS ISU/FIST
	IPAddr_t aR34_H1 = IPAddr("192.168.18.2");			//CONUS ISU/FIST
	IPAddr_t aH1_R34 = IPAddr("192.168.18.1");			//CONUS ISU/FIST
	IPAddr_t aH1_R1 = IPAddr("134.207.136.2");			//CONUS ISU/FIST
	IPAddr_t aR1_H1 = IPAddr("134.207.136.1");			//CONUS ISU/FIST
	IPAddr_t aR1_R2 = IPAddr("134.207.136.5");			//CONUS ISU/FIST
	IPAddr_t aship1g = IPAddr("192.169.27.1");			//DDG (Ship 1)
	IPAddr_t aR38_LAN = IPAddr("192.169.27.2");			//DDG (Ship 1)
	IPAddr_t aR38_Hx = IPAddr("192.168.27.2");			//DDG (Ship 1)
	IPAddr_t aHx_R38 = IPAddr("192.168.27.1");			//DDG (Ship 1)
	IPAddr_t aHx_R37 = IPAddr("192.168.26.1");			//DDG (Ship 1)
	IPAddr_t aship1r = IPAddr("192.169.25.1");			//DDG (Ship 1)
	IPAddr_t aR39_LAN = IPAddr("192.169.25.2");			//DDG (Ship 1)
	IPAddr_t aR39_R37 = IPAddr("192.168.25.2");			//DDG (Ship 1)
	IPAddr_t aR37_Hx = IPAddr("192.168.26.2");			//DDG (Ship 1)
	IPAddr_t aR37_R39 = IPAddr("192.168.25.1");			//DDG (Ship 1)
	IPAddr_t aR37_SAT4 = IPAddr("192.168.24.2");		//DDG (Ship 1)
	IPAddr_t aSAT4_R37 = IPAddr("192.168.24.1");		//Satellite 4
	IPAddr_t aSAT4_R35 = IPAddr("192.168.23.1");		//Satellite 4
	IPAddr_t aR35_SAT4 = IPAddr("192.168.23.2");		//NCTAMS
	IPAddr_t aR35_H11 = IPAddr("192.168.19.2");			//NCTAMS
	IPAddr_t aH11_R35 = IPAddr("192.168.19.1");			//NCTAMS
	IPAddr_t aH11_R16 = IPAddr("134.207.136.114");		//NCTAMS
	IPAddr_t aR36_H12 = IPAddr("192.168.20.2");			//NCTAMS
	IPAddr_t aH12_R36 = IPAddr("192.168.20.1");			//NCTAMS
	IPAddr_t aH12_R16 = IPAddr("134.207.136.118");		//NCTAMS
	IPAddr_t aR16_H11 = IPAddr("134.207.136.113");		//NCTAMS
	IPAddr_t aR16_H12 = IPAddr("134.207.136.117");		//NCTAMS
	IPAddr_t aR16_SAT3 = IPAddr("134.207.136.121");		//NCTAMS
	IPAddr_t aR16_R8 = IPAddr("134.207.136.42");		//NCTAMS
	IPAddr_t aR36_Hy = IPAddr("192.168.21.2");			//NCTAMS
	IPAddr_t aHy_R36 = IPAddr("192.168.21.1");			//NCTAMS
	IPAddr_t aHy_R35 = IPAddr("192.168.22.1");			//NCTAMS
	IPAddr_t aR35_Hy = IPAddr("192.168.22.2");			//NCTAMS
	
	/******************************JRAE 04-4 IP Address Initialization******************************/
		
	
	/******************************JRAE 04-4 Network Node Declarations******************************/
	
	//PEO C3T - Ft. Hood, TX network elements
	Node* brigade;			//Brigade
	Node* R40;				//Brigade
	TACLANENode* H8;		//Brigade
	Node* R41;				//Brigade

	//JITC - Ft. Huachuca, AZ network elements
	Node* soc;				//Spec. Ops. Command
	Node* R23;				//Spec. Ops. Command
	TACLANENode* H5;		//Spec. Ops. Command
	
	//AFCA - Scott AFB, IL network elements
	Node* fs1c;				//Fighter Squadron 1 Commander
	Node* R22;				//Fighter Squadron 1 Commander
	
	//BCBL - Ft. Gordon, GA network elements
	Node* batallion;		//Batallion
	Node* R25a;				//Batallion
	TACLANENode* H9;		//Batallion
	Node* R14;				//Batallion
	Node* platoon;			//Platoon
	Node* R26;				//Platoon
	TACLANENode* H10;		//Platoon
	Node* R13;				//Platoon
	
	//NRL - Washington, D.C. network elements
	Node* R12;				//Teleport 2
	Node* uav;				//UAV
	Node* R27;				//UAV
	TACLANENode* H3;		//UAV
	Node* R6;				//UAV
	Node* R4;				//Teleport 1
	Node* R43;				//GIG-BE POP1
	TACLANENode* H20;		//GIG-BE POP1
	Node* R42;				//GIG-BE POP1
	Node* R2;				//GIG-BE POP1
	Node* aorc;				//AOR Commander
	Node* R28;				//AOR Commander
	TACLANENode* H18;		//AOR Commander
	Node* R25b;				//AOR Commander
	
	//MIT-LL - Lexington, MA network elements
	Node* R8;				//GIG-BE POP3
	SatelliteNode* SAT1a;	//Satellite 1
	SatelliteNode* SAT1b;	//Satellite 1
	SatelliteNode* SAT1c;	//Satellite 1
	SatelliteNode* SAT2a;	//Satellite 2
	SatelliteNode* SAT2b;	//Satellite 2
	SatelliteNode* SAT2c;	//Satellite 2
	SatelliteNode* SAT2d;	//Satellite 2
	SatelliteNode* SAT3;	//Satellite 3
	
	//DISA network elements
	Node* R7;				//GIG-BE POP2
	
	//JTEO - McLean, VA network elements
	Node* fs2c;				//Fighter Squadron 2 Commander
	Node* R29;				//Fighter Squadron 2 Commander
	TACLANENode* H2;		//Fighter Squadron 2 Commander
	Node* R5;				//Fighter Squadron 2 Commander
	Node* uavcrew;			//UAV Crew
	Node* R30;				//UAV Crew
	TACLANENode* H4;		//UAV Crew
	Node* R3;				//UAV Crew
	
	//SSC-SD - San Diego, CA network elements
	TACLANENode* H6;		//CJTF
	Node* R10;				//CJTF
	Node* jsotf;			//JSOTF
	Node* R31;				//JSOTF
	TACLANENode* H17;		//JSOTF
	Node* R24;				//JSOTF
	Node* ship2g;			//ESG Commander (Ship 2)
	Node* R32;				//ESG Commander (Ship 2)
	TACLANENode* H14;		//ESG Commander (Ship 2)
	Node* ship2r;			//ESG Commander (Ship 2)
	Node* R33;				//ESG Commander (Ship 2)
	TACLANENode* H13;		//ESG Commander (Ship 2)
	Node* R17;				//ESG Commander (Ship 2)
	Node* ship2tl;			//ESG Commander (Ship 2)
	TACLANENode* H16;		//ESG Commander (Ship 2)
	Node* R20;				//JTRS Cloud
	Node* R19;				//JTRS Cloud
	Node* R18;				//JTRS Cloud
	Node* R15;				//JTRS Cloud
	Node* JTRS;				//JTRS Router

	//CPSG - Brooks AFB, SC network elements
	Node* aoc1;				//Air Ops Center
	TACLANENode* H7;		//Air Ops Center
	Node* aoc2;				//Air Ops Center
	TACLANENode* H19;		//Air Ops Center
	Node* R11;				//Air Ops Center
	Node* ap;				//Air Platform
	TACLANENode* H15;		//Air Platform
	Node* R21;				//Air Platform
	
	//SSC-CH - Charleston, SC network elements
	Node* isufist;			//CONUS ISU/FIST
	Node* R34;				//CONUS ISU/FIST
	TACLANENode* H1;		//CONUS ISU/FIST
	Node* R1;				//CONUS ISU/FIST
	Node* ship1g;			//DDG (Ship 1)
	Node* R38;				//DDG (Ship 1)
	TACLANENode* Hx;		//DDG (Ship 1)
	Node* ship1r;			//DDG (Ship 1)
	Node* R39;				//DDG (Ship 1)
	Node* R37;				//DDG (Ship 1)
	SatelliteNode* SAT4;	//Satellite 4
	Node* R35;				//NCTAMS
	TACLANENode* H11;		//NCTAMS
	Node* R36;				//NCTAMS
	TACLANENode* H12;		//NCTAMS
	Node* R16;				//NCTAMS
	TACLANENode* Hy;		//NCTAMS
	
	/******************************JRAE 04-4 Network Node Declarations******************************/

	
	/*****************************JRAE 04-4 Network Node Initialization*****************************/

	//PEO C3T - Ft. Hood, TX network elements
	brigade = new Node();								//Brigade
	R40 = new Node();									//Brigade
	H8 = new TACLANENode(aH8_R40, aH8_R41, RB);			//Brigade
	R41 = new Node();									//Brigade
	
	//JITC - Ft. Huachuca, AZ network elements
	soc = new Node();									//Spec. Ops. Command
	R23 = new Node();									//Spec. Ops. Command
	H5 = new TACLANENode(aH5_R23, aH5_R8, RB);			//Spec. Ops. Command
	
	//AFCA - Scott AFB, IL network elements
	fs1c = new Node();									//Fighter Squadron 1 Commander
	R22 = new Node();									//Fighter Squadron 1 Commander
	
	//BCBL - Ft. Gordon, GA network elements
	batallion = new Node();								//Batallion
	R25a = new Node();									//Batallion
	H9 = new TACLANENode(aH9_R25a, aH9_R14, RB);		//Batallion
	R14 = new Node();									//Batallion
	platoon = new Node();								//Platoon
	R26 = new Node();									//Platoon
	H10 = new TACLANENode(aH10_R26, aH10_R13, RB);		//Platoon
	R13 = new Node();									//Platoon
	
	//NRL - Washington, D.C. network elements
	R12 = new Node();									//Teleport 2
	uav = new Node();									//UAV
	R27 = new Node();									//UAV
	H3 = new TACLANENode(aH3_R27, aH3_R6, RB);			//UAV
	R6 = new Node();									//UAV
	R4 = new Node();									//Teleport 1
	R43 = new Node();									//GIG-BE POP1
	H20 = new TACLANENode(aH20_R43, aH20_R42, RB);		//GIG-BE POP1
	R42 = new Node();									//GIG-BE POP1
	R2 = new Node();									//GIG-BE POP1
	aorc = new Node();									//AOR Commander
	R28 = new Node();									//AOR Commander
	H18 = new TACLANENode(aH18_R28, aH18_R25b, RB);		//AOR Commander
	R25b = new Node();									//AOR Commander
	
	//MIT-LL - Lexington, MA network elements
	R8 = new Node();									//GIG-BE POP3
	SAT1a = new SatelliteNode();						//Satellite 1
	SAT1b = new SatelliteNode();						//Satellite 1
	SAT1c = new SatelliteNode();						//Satellite 1
	SAT2a = new SatelliteNode();						//Satellite 2
	SAT2b = new SatelliteNode();						//Satellite 2
	SAT2c = new SatelliteNode();						//Satellite 2
	SAT2d = new SatelliteNode();						//Satellite 2
	SAT3 = new SatelliteNode();							//Satellite 3
	
	//DISA network elements
	R7 = new Node();									//GIG-BE POP2
	
	//JTEO - McLean, VA network elements
	fs2c = new Node();									//Fighter Squadron 2 Commander
	R29 = new Node();									//Fighter Squadron 2 Commander
	H2 = new TACLANENode(aH2_R29, aH2_R5, RB);			//Fighter Squadron 2 Commander
	R5 = new Node();									//Fighter Squadron 2 Commander
	uavcrew = new Node();								//UAV Crew
	R30 = new Node();									//UAV Crew
	H4 = new TACLANENode(aH4_R30, aH4_R3, RB);			//UAV Crew
	R3 = new Node();									//UAV Crew
	
	//SSC-SD - San Diego, CA network elements
	H6 = new TACLANENode(aH6_R22, aH6_R10, RB);			//CJTF
	R10 = new Node();									//CJTF
	jsotf = new Node();									//JSOTF
	R31 = new Node();									//JSOTF
	H17 = new TACLANENode(aH17_R31, aH17_R24, RB);		//JSOTF
	R24 = new Node();									//JSOTF
	ship2g = new Node();								//ESG Commander (Ship 2)
	R32 = new Node();									//ESG Commander (Ship 2)
	H14 = new TACLANENode(aH14_R32, aH14_R17, GB);		//ESG Commander (Ship 2)
	ship2r = new Node();								//ESG Commander (Ship 2)
	R33 = new Node();									//ESG Commander (Ship 2)
	H13 = new TACLANENode(aH13_R33, aH13_R17, RB);		//ESG Commander (Ship 2)
	ship2tl = new Node();								//ESG Commander (Ship 2)
	H16 = new TACLANENode(aH16_LAN, aH16_R17, RB);		//ESG Commander (Ship 2)
	R17 = new Node();									//ESG Commander (Ship 2)
	R20 = new Node();									//JTRS Cloud
	R19 = new Node();									//JTRS Cloud
	R18 = new Node();									//JTRS Cloud
	R15 = new Node();									//JTRS Cloud
	JTRS = new Node();									//JTRS Router
	
	//CPSG - Brooks AFB, SC network elements
	aoc1 = new Node();									//Air Ops Center
	H7 = new TACLANENode(aH7_LAN, aH7_R11, RB);			//Air Ops Center
	aoc2 = new Node();									//Air Ops Center
	H19 = new TACLANENode(aH19_LAN, aH19_R11, RB);		//Air Ops Center
	R11 = new Node();									//Air Ops Center
	ap = new Node();									//Air Platform
	H15 = new TACLANENode(aH15_LAN, aH15_R21, RB);		//Air Platform
	R21 = new Node();									//Air Platform
	
	//SSC-CH - Charleston, SC network elements
	isufist = new Node();								//CONUS ISU/FIST
	R34 = new Node();									//CONUS ISU/FIST
	H1 = new TACLANENode(aH1_R34, aH1_R1, RB);			//CONUS ISU/FIST
	R1 = new Node();									//CONUS ISU/FIST
	ship1g = new Node();								//DDG (Ship 1)
	R38 = new Node();									//DDG (Ship 1)
	Hx = new TACLANENode(aHx_R38, aHx_R37, RG);			//DDG (Ship 1)
	ship1r = new Node();								//DDG (Ship 1)
	R39 = new Node();									//DDG (Ship 1)
	R37 = new Node();									//DDG (Ship 1)
	SAT4 = new SatelliteNode();							//Satellite 4
	R35 = new Node();									//NCTAMS
	H11 = new TACLANENode(aH11_R35, aH11_R16, RB);		//NCTAMS
	R36 = new Node();									//NCTAMS
	H12 = new TACLANENode(aH12_R36, aH12_R16, GB);		//NCTAMS
	R16 = new Node();									//NCTAMS
	Hy = new TACLANENode(aHy_R36, aHy_R35, RG);			//NCTAMS
	
	/*****************************JRAE 04-4 Network Node Initialization*****************************/

	
	/*********************************JRAE 04-4 Link Initialization*********************************/
	
	Linkp2p tl_link(Rate("10Mb"), Time("0ms"));
	Linkp2p lan_link(Rate("100Mb"), Time("0ms"));
	Linkp2p gigcore_link(Rate("1Gb"), Time("0ms"));
	Linkp2p gigedge_link(Rate("1Gb"), Time("0ms"));
	Linkp2p jtrs_link(Rate("100Mb"), Time("0ms"));
	Linkp2p sat_link1(Rate("45Mb"), Time("250ms"));
	Linkp2p sat_link2(Rate("1.5Mb"), Time("350ms"));
	sat_link1.BitErrorRate(1e-6); // Satellite bit error rate
	sat_link2.BitErrorRate(1e-6); // Satellite bit error rate

	//PEO C3T - Ft. Hood, TX links
	brigade->AddDuplexLink(R40, lan_link, abrigade, Mask(32), aR40_LAN, Mask(32));		//Brigade
	R40->AddDuplexLink(H8, tl_link, aR40_H8, Mask(32), aH8_R40, Mask(32));				//Brigade
	R41->AddDuplexLink(H8, tl_link, aR41_H8, Mask(32), aH8_R41, Mask(32));				//Brigade
	R41->AddDuplexLink(SAT2b, sat_link1, aR41_SAT2b, Mask(32), aSAT2b_R41, Mask(32));	//Brigade
	
	//JITC - Ft. Huachuca, AZ links
	soc->AddDuplexLink(R23, lan_link, asoc, Mask(32), aR23_LAN, Mask(32));				//Spec. Ops. Command
	R23->AddDuplexLink(H5, tl_link, aR23_H5, Mask(32), aH5_R23, Mask(32));				//Spec. Ops. Command
	R8->AddDuplexLink(H5, gigedge_link, aR8_H5, Mask(32), aH5_R8, Mask(32));			//Spec. Ops. Command
	
	//AFCA - Scott AFB, IL links
	fs1c->AddDuplexLink(R22, lan_link, afs1c, Mask(32), aR22_LAN, Mask(32));			//FighterSquadron 1 Commander
	R22->AddDuplexLink(H6, tl_link, aR22_H6, Mask(32), aH6_R22, Mask(32));				//FighterSquadron 1 Commander
	
	//BCBL - Ft. Gordon, GA links
	batallion->AddDuplexLink(R25a, lan_link, abatallion, Mask(32), aR25_LAN, Mask(32));	//Batallion
	R25a->AddDuplexLink(H9, tl_link, aR25a_H9, Mask(32), aH9_R25a, Mask(32));			//Batallion
	R14->AddDuplexLink(H9, tl_link, aR14_H9, Mask(32), aH9_R14, Mask(32));				//Batallion
	R14->AddDuplexLink(SAT2a, sat_link1, aR14_SAT2a, Mask(32), aSAT2a_R14, Mask(32));	//Batallion
	R14->AddDuplexLink(R15, jtrs_link, aR14_R15, Mask(32), aR15_R14, Mask(32));			//Batallion
	platoon->AddDuplexLink(R26, lan_link, aplatoon, Mask(32), aR26_LAN, Mask(32));		//Platoon
	R26->AddDuplexLink(H10, tl_link, aR26_H10, Mask(32), aH10_R26, Mask(32));			//Platoon
	R13->AddDuplexLink(H10, tl_link, aR13_H10, Mask(32), aH10_R13, Mask(32));			//Platoon
	R13->AddDuplexLink(R19, jtrs_link, aR13_R19, Mask(32), aR19_R13, Mask(32));			//Platoon
	
	//NRL - Washington, D.C. links
	R12->AddDuplexLink(R7, gigedge_link, aR12_R7, Mask(32), aR7_R12, Mask(32));			//Teleport 2
	R12->AddDuplexLink(SAT2d, sat_link1, aR12_SAT2d, Mask(32), aSAT2d_R12, Mask(32));	//Teleport 2
	R12->AddDuplexLink(SAT2c, sat_link1, aR12_SAT2c, Mask(32), aSAT2c_R12, Mask(32));	//Teleport 2
	R12->AddDuplexLink(SAT2b, sat_link1, aR12_SAT2b, Mask(32), aSAT2b_R12, Mask(32));	//Teleport 2
	R12->AddDuplexLink(SAT2a, sat_link1, aR12_SAT2a, Mask(32), aSAT2a_R12, Mask(32));	//Teleport 2
	uav->AddDuplexLink(R27, lan_link, auav, Mask(32), aR27_LAN, Mask(32));				//UAV
	R27->AddDuplexLink(H3, tl_link, aR27_H3, Mask(32), aH3_R27, Mask(32));				//UAV
	R6->AddDuplexLink(H3, tl_link, aR6_H3, Mask(32), aH3_R6, Mask(32));					//UAV
	R6->AddDuplexLink(SAT1a, sat_link1, aR6_SAT1a, Mask(32), aSAT1a_R6, Mask(32));		//UAV
	R4->AddDuplexLink(SAT1c, sat_link1, aR4_SAT1c, Mask(32), aSAT1c_R4, Mask(32));		//Teleport 1
	R4->AddDuplexLink(SAT1b, sat_link1, aR4_SAT1b, Mask(32), aSAT1b_R4, Mask(32));		//Teleport 1
	R4->AddDuplexLink(SAT1a, sat_link1, aR4_SAT1a, Mask(32), aSAT1a_R4, Mask(32));		//Teleport 1
	R4->AddDuplexLink(R2, gigedge_link, aR4_R2, Mask(32), aR2_R4, Mask(32));			//Teleport 1
	R43->AddDuplexLink(H20, tl_link, aR43_H20, Mask(32), aH20_R43, Mask(32));			//GIG-BE POP1
	R42->AddDuplexLink(H20, tl_link, aR42_H20, Mask(32), aH20_R42, Mask(32));			//GIG-BE POP1
	R42->AddDuplexLink(R2, gigedge_link, aR42_R2, Mask(32), aR2_R42, Mask(32));			//GIG-BE POP1
	aorc->AddDuplexLink(R28, lan_link, aaorc, Mask(32), aR28_LAN, Mask(32));			//AOR Commander
	R28->AddDuplexLink(H18, tl_link, aR28_H18, Mask(32), aH18_R28, Mask(32));			//AOR Commander
	R25b->AddDuplexLink(H18, tl_link, aR25b_H18, Mask(32), aH18_R25b, Mask(32));		//AOR Commander
	R25b->AddDuplexLink(R2, gigedge_link, aR25b_R2, Mask(32), aR2_R25b, Mask(32));		//AOR Commander
	
	//MIT-LL - Lexington, MA links
	R8->AddDuplexLink(R2, gigcore_link, aR8_R2, Mask(32), aR2_R8, Mask(32));			//GIG-BE POP3
	R8->AddDuplexLink(R7, gigcore_link, aR8_R7, Mask(32), aR7_R8, Mask(32));			//GIG-BE POP3
	
	//DISA links
	R7->AddDuplexLink(R2, gigcore_link, aR7_R2, Mask(32), aR2_R7, Mask(32));			//GIG-BE POP2
	
	//JTEO - McLean, VA links
	fs2c->AddDuplexLink(R29, lan_link, afs2c, Mask(32), aR29_LAN, Mask(32));			//Fighter Squadron 2 Commander
	R29->AddDuplexLink(H2, tl_link, aR29_H2, Mask(32), aH2_R29, Mask(32));				//Fighter Squadron 2 Commander
	R5->AddDuplexLink(H2, tl_link, aR5_H2, Mask(32), aH2_R5, Mask(32));					//Fighter Squadron 2 Commander
	R5->AddDuplexLink(SAT1b, sat_link1, aR5_SAT1b, Mask(32), aSAT1b_R5, Mask(32));		//Fighter Squadron 2 Commander
	uavcrew->AddDuplexLink(R30, lan_link, auavcrew, Mask(32), aR30_LAN, Mask(32));		//UAV Crew
	R30->AddDuplexLink(H4, tl_link, aR30_H4, Mask(32), aH4_R30, Mask(32));				//UAV Crew
	R3->AddDuplexLink(H4, tl_link, aR3_H4, Mask(32), aH4_R3, Mask(32));					//UAV Crew
	R3->AddDuplexLink(R2, gigedge_link, aR3_R2, Mask(32), aR2_R3, Mask(32));			//UAV Crew
		
	//SSC-SD - San Diego, CA links
	R10->AddDuplexLink(H6, tl_link, aR10_H6, Mask(32), aH6_R10, Mask(32));				//CJTF
	R10->AddDuplexLink(R7, gigedge_link, aR10_R7, Mask(32), aR7_R10, Mask(32));			//CJTF
	R10->AddDuplexLink(SAT2d, sat_link1, aR10_SAT2d, Mask(32), aSAT2d_R10, Mask(32));	//CJTF
	jsotf->AddDuplexLink(R31, lan_link, ajsotf, Mask(32), aR31_LAN, Mask(32));			//JSOTF
	R31->AddDuplexLink(H17, tl_link, aR31_H17, Mask(32), aH17_R31, Mask(32));			//JSOTF
	R24->AddDuplexLink(H17, tl_link, aR24_H17, Mask(32), aH17_R24, Mask(32));			//JSOTF
	R24->AddDuplexLink(SAT1c, sat_link1, aR24_SAT1c, Mask(32), aSAT1c_R24, Mask(32));	//JSOTF
	ship2g->AddDuplexLink(R32, lan_link, aship2g, Mask(32), aR32_LAN, Mask(32));		//ESG Commander (Ship 2)
	R32->AddDuplexLink(H14, tl_link, aR32_H14, Mask(32), aH14_R32, Mask(32));			//ESG Commander (Ship 2)
	ship2r->AddDuplexLink(R33, lan_link, aship2r, Mask(32), aR33_LAN, Mask(32));		//ESG Commander (Ship 2)
	R33->AddDuplexLink(H13, tl_link, aR33_H13, Mask(32), aH13_R33, Mask(32));			//ESG Commander (Ship 2)
	ship2tl->AddDuplexLink(H16, lan_link, aship2tl, Mask(32), aH16_LAN, Mask(32));		//ESG Commander (Ship 2)
	R17->AddDuplexLink(H14, tl_link, aR17_H14, Mask(32), aH14_R17, Mask(32));			//ESG Commander (Ship 2)
	R17->AddDuplexLink(H13, tl_link, aR17_H13, Mask(32), aH13_R17, Mask(32));			//ESG Commander (Ship 2)
	R17->AddDuplexLink(H16, tl_link, aR17_H16, Mask(32), aH16_R17, Mask(32));			//ESG Commander (Ship 2)
	R17->AddDuplexLink(SAT3, sat_link1, aR17_SAT3, Mask(32), aSAT3_R17, Mask(32));		//ESG Commander (Ship 2)
	R17->AddDuplexLink(R18, jtrs_link, aR17_R18, Mask(32), aR18_R17, Mask(32));			//ESG Commander (Ship 2)
	JTRS->AddDuplexLink(R20, jtrs_link, aJTRS_R20, Mask(32), aR20_JTRS, Mask(32));		//JTRS Cloud
	JTRS->AddDuplexLink(R19, jtrs_link, aJTRS_R19, Mask(32), aR19_JTRS, Mask(32));		//JTRS Cloud
	JTRS->AddDuplexLink(R18, jtrs_link, aJTRS_R18, Mask(32), aR18_JTRS, Mask(32));		//JTRS Cloud
	JTRS->AddDuplexLink(R15, jtrs_link, aJTRS_R15, Mask(32), aR15_JTRS, Mask(32));		//JTRS Cloud
	
	//CPSG - Brooks AFB, SC links
	aoc1->AddDuplexLink(H7, lan_link, aaoc1, Mask(32), aH7_LAN, Mask(32));				//Air Ops Center
	R11->AddDuplexLink(H7, tl_link, aR11_H7, Mask(32), aH7_R11, Mask(32));				//Air Ops Center
	aoc2->AddDuplexLink(H19, lan_link, aaoc2, Mask(32), aH19_LAN, Mask(32));			//Air Ops Center
	R11->AddDuplexLink(H19, tl_link, aR11_H19, Mask(32), aH19_R11, Mask(32));			//Air Ops Center
	R11->AddDuplexLink(SAT2c, sat_link1, aR11_SAT2c, Mask(32), aSAT2c_R11, Mask(32));	//Air Ops Center
	R11->AddDuplexLink(R7, gigedge_link, aR11_R7, Mask(32), aR7_R11, Mask(32));			//Air Ops Center
	ap->AddDuplexLink(H15, lan_link, aap, Mask(32), aH15_LAN, Mask(32));				//Air Platform
	R21->AddDuplexLink(H15, tl_link, aR21_H15, Mask(32), aH15_R21, Mask(32));			//Air Platform
	R21->AddDuplexLink(R20, jtrs_link, aR21_R20, Mask(32), aR20_R21, Mask(32));			//Air Platform
	
	//SSC-CH - Charleston, SC links
	isufist->AddDuplexLink(R34, lan_link, aisufist, Mask(32), aR34_LAN, Mask(32));		//CONUS ISU/FIST
	R34->AddDuplexLink(H1, tl_link, aR34_H1, Mask(32), aH1_R34, Mask(32));				//CONUS ISU/FIST
	R1->AddDuplexLink(H1, tl_link, aR1_H1, Mask(32), aH1_R1, Mask(32));					//CONUS ISU/FIST
	R1->AddDuplexLink(R2, gigedge_link, aR1_R2, Mask(32), aR2_R1, Mask(32));			//CONUS ISU/FIST
	ship1g->AddDuplexLink(R38, lan_link, aship1g, Mask(32), aR38_LAN, Mask(32));		//DDG (Ship 1)
	R38->AddDuplexLink(Hx, tl_link, aR38_Hx, Mask(32), aHx_R38, Mask(32));				//DDG (Ship 1)
	ship1r->AddDuplexLink(R39, lan_link, aship1r, Mask(32), aR39_LAN, Mask(32));		//DDG (Ship 1)
	R37->AddDuplexLink(Hx, tl_link, aR37_Hx, Mask(32), aHx_R37, Mask(32));				//DDG (Ship 1)
	R37->AddDuplexLink(R39, lan_link, aR37_R39, Mask(32), aR39_R37, Mask(32));			//DDG (Ship 1)
	R37->AddDuplexLink(SAT4, sat_link2, aR37_SAT4, Mask(32), aSAT4_R37, Mask(32));		//DDG (Ship 1)
	R35->AddDuplexLink(SAT4, sat_link2, aR35_SAT4, Mask(32), aSAT4_R35, Mask(32));		//NCTAMS
	R35->AddDuplexLink(H11, tl_link, aR35_H11, Mask(32), aH11_R35, Mask(32));			//NCTAMS
	R36->AddDuplexLink(H12, tl_link, aR36_H12, Mask(32), aH12_R36, Mask(32));			//NCTAMS
	R16->AddDuplexLink(H11, tl_link, aR16_H11, Mask(32), aH11_R16, Mask(32));			//NCTAMS
	R16->AddDuplexLink(H12, tl_link, aR16_H12, Mask(32), aH12_R16, Mask(32));			//NCTAMS
	R16->AddDuplexLink(SAT3, sat_link1, aR16_SAT3, Mask(32), aSAT3_R16, Mask(32));		//NCTAMS
	R16->AddDuplexLink(R8, gigedge_link, aR16_R8, Mask(32), aR8_R16, Mask(32));			//NCTAMS
	R36->AddDuplexLink(Hy, tl_link, aR36_Hy, Mask(32), aHy_R36, Mask(32));				//NCTAMS
	R35->AddDuplexLink(Hy, tl_link, aR35_Hy, Mask(32), aHy_R35, Mask(32));				//NCTAMS
	
	/*********************************JRAE 04-4 Link Initialization*********************************/	
	
	
	/*********************************JRAE 04-4 Network Node Shapes*********************************/
	
	//PEO C3T - Ft. Hood, TX network elements
	brigade->CustomShapeImage(HostSmallImage());		//Brigade
	R40->CustomShapeImage(RedRouterImage());	//Brigade
	R41->CustomShapeImage(BlackRouterImage());	//Brigade
	
	//JITC - Ft. Huachuca, AZ network elements
	soc->CustomShapeImage(HostSmallImage());			//Spec. Ops. Command
	R23->CustomShapeImage(RedRouterImage());	//Spec. Ops. Command
	
	//AFCA - Scott AFB, IL network elements
	fs1c->CustomShapeImage(HostSmallImage());		//Fighter Squadron 1 Commander
	R22->CustomShapeImage(RedRouterImage());	//Fighter Squadron 1 Commander
	
	//BCBL - Ft. Gordon, GA network elements
	batallion->CustomShapeImage(HostSmallImage());	//Batallion
	R25a->CustomShapeImage(RedRouterImage());	//Batallion
	R14->CustomShapeImage(BlackRouterImage());	//Batallion
	platoon->CustomShapeImage(HostSmallImage());		//Platoon
	R26->CustomShapeImage(RedRouterImage());	//Platoon
	R13->CustomShapeImage(BlackRouterImage());	//Platoon
	
	//NRL - Washington, D.C. network elements
	R12->CustomShapeImage(BlackRouterImage());	//Teleport 2
	uav->CustomShapeImage(HostSmallImage());			//UAV
	R27->CustomShapeImage(RedRouterImage());	//UAV
	R6->CustomShapeImage(BlackRouterImage());	//UAV
	R4->CustomShapeImage(BlackRouterImage());	//Teleport 1
	R43->CustomShapeImage(RedRouterImage());	//GIG-BE POP1
	R42->CustomShapeImage(BlackRouterImage());	//GIG-BE POP1
	R2->CustomShapeImage(BlackRouterImage());	//GIG-BE POP1
	aorc->CustomShapeImage(HostSmallImage());		//AOR Commander
	R28->CustomShapeImage(RedRouterImage());	//AOR Commander
	R25b->CustomShapeImage(BlackRouterImage());	//AOR Commander
	
	//MIT-LL - Lexington, MA network elements
	R8->CustomShapeImage(BlackRouterImage());	//GIG-BE POP3
	
	//DISA network elements
	R7->CustomShapeImage(BlackRouterImage());	//GIG-BE POP2
	
	//JTEO - McLean, VA network elements
	fs2c->CustomShapeImage(HostSmallImage());		//Fighter Squadron 2 Commander
	R29->CustomShapeImage(RedRouterImage());	//Fighter Squadron 2 Commander
	R5->CustomShapeImage(BlackRouterImage());	//Fighter Squadron 2 Commander
	uavcrew->CustomShapeImage(HostSmallImage());		//UAV Crew
	R30->CustomShapeImage(RedRouterImage());	//UAV Crew
	R3->CustomShapeImage(BlackRouterImage());	//UAV Crew
	
	//SSC-SD - San Diego, CA network elements
	R10->CustomShapeImage(BlackRouterImage());	//CJTF
	jsotf->CustomShapeImage(HostSmallImage());		//JSOTF
	R31->CustomShapeImage(RedRouterImage());	//JSOTF
	R24->CustomShapeImage(BlackRouterImage());	//JSOTF
	ship2g->CustomShapeImage(HostSmallImage());		//ESG Commander (Ship 2)
	R32->CustomShapeImage(GreenRouterImage());	//ESG Commander (Ship 2)
	ship2r->CustomShapeImage(HostSmallImage());		//ESG Commander (Ship 2)
	R33->CustomShapeImage(RedRouterImage());	//ESG Commander (Ship 2)
	ship2tl->CustomShapeImage(HostSmallImage());		//ESG Commander (Ship 2)
	R17->CustomShapeImage(BlackRouterImage());	//ESG Commander (Ship 2)
	R20->CustomShapeImage(BlackRouterImage());	//JTRS Cloud
	R19->CustomShapeImage(BlackRouterImage());	//JTRS Cloud
	R18->CustomShapeImage(BlackRouterImage());	//JTRS Cloud
	R15->CustomShapeImage(BlackRouterImage());	//JTRS Cloud
	JTRS->CustomShapeImage(BlackRouterImage());	//JTRS Router
	
	//CPSG - Brooks AFB, SC network elements
	aoc1->CustomShapeImage(HostSmallImage());		//Air Ops Center
	aoc2->CustomShapeImage(HostSmallImage());		//Air Ops Center
	R11->CustomShapeImage(BlackRouterImage());	//Air Ops Center
	ap->CustomShapeImage(HostSmallImage());			//Air Platform
	R21->CustomShapeImage(BlackRouterImage());	//Air Platform
	
	//SSC-CH - Charleston, SC network elements
	isufist->CustomShapeImage(HostSmallImage());		//CONUS ISU/FIST
	R34->CustomShapeImage(RedRouterImage());	//CONUS ISU/FIST
	R1->CustomShapeImage(BlackRouterImage());	//CONUS ISU/FIST
	ship1g->CustomShapeImage(HostSmallImage());		//DDG (Ship 1)
	R38->CustomShapeImage(GreenRouterImage());	//DDG (Ship 1)
	ship1r->CustomShapeImage(HostSmallImage());		//DDG (Ship 1)
	R39->CustomShapeImage(RedRouterImage());	//DDG (Ship 1)
	R37->CustomShapeImage(RedRouterImage());	//DDG (Ship 1)
	R35->CustomShapeImage(RedRouterImage());	//NCTAMS
	R36->CustomShapeImage(GreenRouterImage());	//NCTAMS
	R16->CustomShapeImage(BlackRouterImage());	//NCTAMS
	
	/*********************************JRAE 04-4 Network Node Shapes*********************************/
	
	
	/********************************JRAE 04-4 Network Node Locations*******************************/
	
	//PEO C3T - Ft. Hood, TX network elements
	brigade->SetLocation(0.5, 21);			//Brigade
	R40->SetLocation(0.5, 19);				//Brigade
	H8->SetLocation(0.5, 17.5);				//Brigade
	R41->SetLocation(0.5, 16);				//Brigade
	
	//JITC - Ft. Huachuca, AZ network elements
	soc->SetLocation(3.5, 19.5);			//Spec. Ops. Command
	R23->SetLocation(3.5, 17.5);			//Spec. Ops. Command
	H5->SetLocation(3.5, 16);				//Spec. Ops. Command
	
	//AFCA - Scott AFB, IL network elements
	fs1c->SetLocation(16.5, 3);				//Fighter Squadron 1 Commander
	R22->SetLocation(16.5, 5);				//Fighter Squadron 1 Commander
	
	//BCBL - Ft. Gordon, GA network elements
	batallion->SetLocation(5, 21);			//Batallion
	R25a->SetLocation(5, 19);				//Batallion
	H9->SetLocation(5, 17.5);				//Batallion
	R14->SetLocation(5, 16);				//Batallion
	platoon->SetLocation(12.5, 21);			//Platoon
	R26->SetLocation(12.5, 19);				//Platoon
	H10->SetLocation(12.5, 17.5);			//Platoon
	R13->SetLocation(12.5, 16);				//Platoon
	
	//NRL - Washington, D.C. network elements
	R12->SetLocation(6.5, 16);				//Teleport 2
	uav->SetLocation(8, 21);				//UAV
	R27->SetLocation(8, 19);				//UAV
	H3->SetLocation(8, 17.5);				//UAV
	R6->SetLocation(8, 16);					//UAV
	R4->SetLocation(9.5, 16);				//Teleport 1
	R43->SetLocation(14, 19);				//GIG-BE POP1
	H20->SetLocation(14, 17.5);				//GIG-BE POP1
	R42->SetLocation(15.5, 17.5);			//GIG-BE POP1
	R2->SetLocation(15.5, 16);				//GIG-BE POP1
	aorc->SetLocation(11, 21);				//AOR Commander
	R28->SetLocation(11, 19);				//AOR Commander
	H18->SetLocation(11, 17.5);				//AOR Commander
	R25b->SetLocation(11, 16);				//AOR Commander
	
	//MIT-LL - Lexington, MA network elements
	R8->SetLocation(12.5, 12);				//GIG-BE POP3
	SAT1a->SetLocation(8.5, 12);			//Satellite 1
	SAT1b->SetLocation(19.5, 12);			//Satellite 1
	SAT1c->SetLocation(15, 12);				//Satellite 1
	SAT2a->SetLocation(3.5, 12);			//Satellite 2
	SAT2b->SetLocation(1, 12);				//Satellite 2
	SAT2c->SetLocation(7, 12);				//Satellite 2
	SAT2d->SetLocation(16.5, 12);			//Satellite 2
	SAT3->SetLocation(5, 12);				//Satellite 3
	
	//DISA network elements
	R7->SetLocation(10, 12);				//GIG-BE POP2
	
	//JTEO - McLean, VA network elements
	fs2c->SetLocation(19.5, 3);				//Fighter Squadron 2 Commander
	R29->SetLocation(19.5, 5);				//Fighter Squadron 2 Commander
	H2->SetLocation(19.5, 6.5);				//Fighter Squadron 2 Commander
	R5->SetLocation(19.5, 8);				//Fighter Squadron 2 Commander
	uavcrew->SetLocation(18, 3);			//UAV Crew
	R30->SetLocation(18, 5);				//UAV Crew
	H4->SetLocation(18, 6.5);				//UAV Crew
	R3->SetLocation(18, 8);					//UAV Crew
	
	//SSC-SD - San Diego, CA network elements
	H6->SetLocation(16.5, 6.5);				//CJTF
	R10->SetLocation(16.5, 8);				//CJTF
	jsotf->SetLocation(15, 3);				//JSOTF
	R31->SetLocation(15, 5);				//JSOTF
	H17->SetLocation(15, 6.5);				//JSOTF
	R24->SetLocation(15, 8);				//JSOTF
	ship2g->SetLocation(12.5, 1.5);			//ESG Commander (Ship 2)
	R32->SetLocation(12.5, 3.5);			//ESG Commander (Ship 2)
	H14->SetLocation(12.5, 5);				//ESG Commander (Ship 2)
	ship2r->SetLocation(11, 0);				//ESG Commander (Ship 2)
	R33->SetLocation(11, 2);				//ESG Commander (Ship 2)
	H13->SetLocation(11, 3.5);				//ESG Commander (Ship 2)
	R17->SetLocation(11, 5);				//ESG Commander (Ship 2)
	ship2tl->SetLocation(9.5, 3);			//ESG Commander (ship 2)
	H16->SetLocation(9.5, 5);				//ESG Commander (Ship 2)
	R20->SetLocation(13.5, 8);				//JTRS Cloud
	R19->SetLocation(12.5, 8);				//JTRS Cloud
	R18->SetLocation(11.5, 8);				//JTRS Cloud
	R15->SetLocation(10.5, 8);				//JTRS Cloud
	JTRS->SetLocation(12, 6.5);				//JTRS Router
	
	//CPSG - Brooks AFB, SC network elements
	aoc1->SetLocation(8, 4.5);				//Air Ops Center
	H7->SetLocation(8, 6.5);				//Air Ops Center
	aoc2->SetLocation(7.5, 0);				//Air Ops Center
	H19->SetLocation(7.5, 2);				//Air Ops Center
	R11->SetLocation(7.5, 8);				//Air Ops Center
	ap->SetLocation(6.5, 4.5);				//Air Platform
	H15->SetLocation(6.5, 6.5);				//Air Platform
	R21->SetLocation(6.5, 8);				//Air Platform
	
	//SSC-CH - Charleston, SC network elements
	isufist->SetLocation(5, 3);				//CONUS ISU/FIST
	R34->SetLocation(5, 5);					//CONUS ISU/FIST
	H1->SetLocation(5, 6.5);				//CONUS ISU/FIST
	R1->SetLocation(5, 8);					//CONUS ISU/FIST
	ship1g->SetLocation(5, 0.5);			//CONUS ISU/FIST
	R38->SetLocation(3.5, 0.5);				//DDG (Ship 1)
	Hx->SetLocation(3.5, 2);				//DDG (Ship 1)
	ship1r->SetLocation(0.5, 0);			//DDG (Ship 1)
	R39->SetLocation(0.5, 2);				//DDG (Ship 1)
	R37->SetLocation(2, 3.5);				//DDG (Ship 1)
	SAT4->SetLocation(3.5, 3.5);			//Satellite 4
	R35->SetLocation(3.5, 5);				//NCTAMS
	H11->SetLocation(3.5, 6.5);				//NCTAMS
	R36->SetLocation(0.5, 5);				//NCTAMS
	H12->SetLocation(0.5, 6.5);				//NCTAMS
	R16->SetLocation(2, 8);					//NCTAMS
	Hy->SetLocation(2, 5);					//NCTAMS
	
	/********************************JRAE 04-4 Network Node Locations*******************************/

	
	/****************************JRAE 04-4 TACLANE Security Associations****************************/
	
/*
	//Red Network
	H8->AddSecurityAssociation(abrigade, aH8_R41);
	H8->AddSecurityAssociation(asoc, aH5_R8);
	H8->AddSecurityAssociation(afs1c, aH6_R10);
	H8->AddSecurityAssociation(abatallion, aH9_R14);
	H8->AddSecurityAssociation(aplatoon, aH10_R13);
	H8->AddSecurityAssociation(auav, aH3_R6);
	H8->AddSecurityAssociation(aaorc, aH18_R25b);
	H8->AddSecurityAssociation(afs2c, aH2_R5);
	H8->AddSecurityAssociation(auavcrew, aH4_R3);
	H8->AddSecurityAssociation(ajsotf, aH17_R24);
	H8->AddSecurityAssociation(aship2r, aH13_R17);
	H8->AddSecurityAssociation(aship2tl, aH16_R17);
	H8->AddSecurityAssociation(aaoc1, aH7_R11);
	H8->AddSecurityAssociation(aaoc2, aH19_R11);
	H8->AddSecurityAssociation(aap, aH15_R21);
	H8->AddSecurityAssociation(aisufist, aH1_R1);
	H8->AddSecurityAssociation(aship1r, aH11_R16);		
 
	//Green network
	H12->AddSecurityAssociation(aship2g, aH14_R17);
	H14->AddSecurityAssociation(aship1g, aH12_R16);
	Hy->AddSecurityAssociation(aship1g, aHx_R37);
	Hx->AddSecurityAssociation(aship2g, aHy_R35);
*/		

	//PEO C3T - Ft. Hood, TX TACLANE
	H8->AddSecurityAssociation(asoc, aH5_R8);
	H8->AddSecurityAssociation(afs1c, aH6_R10);
	H8->AddSecurityAssociation(abatallion, aH9_R14);
	H8->AddSecurityAssociation(aplatoon, aH10_R13);
	H8->AddSecurityAssociation(auav, aH3_R6);
	H8->AddSecurityAssociation(aaorc, aH18_R25b);
	H8->AddSecurityAssociation(afs2c, aH2_R5);
	H8->AddSecurityAssociation(auavcrew, aH4_R3);
	H8->AddSecurityAssociation(ajsotf, aH17_R24);
	H8->AddSecurityAssociation(aship2r, aH13_R17);
	H8->AddSecurityAssociation(aship2tl, aH16_R17);
	H8->AddSecurityAssociation(aaoc1, aH7_R11);
	H8->AddSecurityAssociation(aaoc2, aH19_R11);
	H8->AddSecurityAssociation(aap, aH15_R21);
	H8->AddSecurityAssociation(aisufist, aH1_R1);
	H8->AddSecurityAssociation(aship1r, aH11_R16);		

	//JITC - Ft. Huachuca, AZ TACLANE
	H5->AddSecurityAssociation(abrigade, aH8_R41);
	H5->AddSecurityAssociation(afs1c, aH6_R10);
	H5->AddSecurityAssociation(abatallion, aH9_R14);
	H5->AddSecurityAssociation(aplatoon, aH10_R13);
	H5->AddSecurityAssociation(auav, aH3_R6);
	H5->AddSecurityAssociation(aaorc, aH18_R25b);
	H5->AddSecurityAssociation(afs2c, aH2_R5);
	H5->AddSecurityAssociation(auavcrew, aH4_R3);
	H5->AddSecurityAssociation(ajsotf, aH17_R24);
	H5->AddSecurityAssociation(aship2r, aH13_R17);
	H5->AddSecurityAssociation(aship2tl, aH16_R17);
	H5->AddSecurityAssociation(aaoc1, aH7_R11);
	H5->AddSecurityAssociation(aaoc2, aH19_R11);
	H5->AddSecurityAssociation(aap, aH15_R21);
	H5->AddSecurityAssociation(aisufist, aH1_R1);
	H5->AddSecurityAssociation(aship1r, aH11_R16);		

	//AFCA - Scott AFB, IL TACLANE
	H6->AddSecurityAssociation(abrigade, aH8_R41);
	H6->AddSecurityAssociation(asoc, aH5_R8);
	H6->AddSecurityAssociation(abatallion, aH9_R14);
	H6->AddSecurityAssociation(aplatoon, aH10_R13);
	H6->AddSecurityAssociation(auav, aH3_R6);
	H6->AddSecurityAssociation(aaorc, aH18_R25b);
	H6->AddSecurityAssociation(afs2c, aH2_R5);
	H6->AddSecurityAssociation(auavcrew, aH4_R3);
	H6->AddSecurityAssociation(ajsotf, aH17_R24);
	H6->AddSecurityAssociation(aship2r, aH13_R17);
	H6->AddSecurityAssociation(aship2tl, aH16_R17);
	H6->AddSecurityAssociation(aaoc1, aH7_R11);
	H6->AddSecurityAssociation(aaoc2, aH19_R11);
	H6->AddSecurityAssociation(aap, aH15_R21);
	H6->AddSecurityAssociation(aisufist, aH1_R1);
	H6->AddSecurityAssociation(aship1r, aH11_R16);		

	//BCBL - Ft. Gordon, GA TACLANEs
	H9->AddSecurityAssociation(abrigade, aH8_R41);
	H9->AddSecurityAssociation(asoc, aH5_R8);
	H9->AddSecurityAssociation(afs1c, aH6_R10);
	H9->AddSecurityAssociation(aplatoon, aH10_R13);
	H9->AddSecurityAssociation(auav, aH3_R6);
	H9->AddSecurityAssociation(aaorc, aH18_R25b);
	H9->AddSecurityAssociation(afs2c, aH2_R5);
	H9->AddSecurityAssociation(auavcrew, aH4_R3);
	H9->AddSecurityAssociation(ajsotf, aH17_R24);
	H9->AddSecurityAssociation(aship2r, aH13_R17);
	H9->AddSecurityAssociation(aship2tl, aH16_R17);
	H9->AddSecurityAssociation(aaoc1, aH7_R11);
	H9->AddSecurityAssociation(aaoc2, aH19_R11);
	H9->AddSecurityAssociation(aap, aH15_R21);
	H9->AddSecurityAssociation(aisufist, aH1_R1);
	H9->AddSecurityAssociation(aship1r, aH11_R16);
	
	H10->AddSecurityAssociation(abrigade, aH8_R41);
	H10->AddSecurityAssociation(asoc, aH5_R8);
	H10->AddSecurityAssociation(afs1c, aH6_R10);
	H10->AddSecurityAssociation(abatallion, aH9_R14);
	H10->AddSecurityAssociation(auav, aH3_R6);
	H10->AddSecurityAssociation(aaorc, aH18_R25b);
	H10->AddSecurityAssociation(afs2c, aH2_R5);
	H10->AddSecurityAssociation(auavcrew, aH4_R3);
	H10->AddSecurityAssociation(ajsotf, aH17_R24);
	H10->AddSecurityAssociation(aship2r, aH13_R17);
	H10->AddSecurityAssociation(aship2tl, aH16_R17);
	H10->AddSecurityAssociation(aaoc1, aH7_R11);
	H10->AddSecurityAssociation(aaoc2, aH19_R11);
	H10->AddSecurityAssociation(aap, aH15_R21);
	H10->AddSecurityAssociation(aisufist, aH1_R1);
	H10->AddSecurityAssociation(aship1r, aH11_R16);		
	
	//NRL - Washington, D.C. TACLANEs
	H3->AddSecurityAssociation(abrigade, aH8_R41);
	H3->AddSecurityAssociation(asoc, aH5_R8);
	H3->AddSecurityAssociation(afs1c, aH6_R10);
	H3->AddSecurityAssociation(abatallion, aH9_R14);
	H3->AddSecurityAssociation(aplatoon, aH10_R13);
	H3->AddSecurityAssociation(aaorc, aH18_R25b);
	H3->AddSecurityAssociation(afs2c, aH2_R5);
	H3->AddSecurityAssociation(auavcrew, aH4_R3);
	H3->AddSecurityAssociation(ajsotf, aH17_R24);
	H3->AddSecurityAssociation(aship2r, aH13_R17);
	H3->AddSecurityAssociation(aship2tl, aH16_R17);
	H3->AddSecurityAssociation(aaoc1, aH7_R11);
	H3->AddSecurityAssociation(aaoc2, aH19_R11);
	H3->AddSecurityAssociation(aap, aH15_R21);
	H3->AddSecurityAssociation(aisufist, aH1_R1);
	H3->AddSecurityAssociation(aship1r, aH11_R16);
	
	H18->AddSecurityAssociation(abrigade, aH8_R41);
	H18->AddSecurityAssociation(asoc, aH5_R8);
	H18->AddSecurityAssociation(afs1c, aH6_R10);
	H18->AddSecurityAssociation(abatallion, aH9_R14);
	H18->AddSecurityAssociation(aplatoon, aH10_R13);
	H18->AddSecurityAssociation(auav, aH3_R6);
	H18->AddSecurityAssociation(afs2c, aH2_R5);
	H18->AddSecurityAssociation(auavcrew, aH4_R3);
	H18->AddSecurityAssociation(ajsotf, aH17_R24);
	H18->AddSecurityAssociation(aship2r, aH13_R17);
	H18->AddSecurityAssociation(aship2tl, aH16_R17);
	H18->AddSecurityAssociation(aaoc1, aH7_R11);
	H18->AddSecurityAssociation(aaoc2, aH19_R11);
	H18->AddSecurityAssociation(aap, aH15_R21);
	H18->AddSecurityAssociation(aisufist, aH1_R1);
	H18->AddSecurityAssociation(aship1r, aH11_R16);		
	
	//JTEO - McLean, VA TACLANEs
	H2->AddSecurityAssociation(abrigade, aH8_R41);
	H2->AddSecurityAssociation(asoc, aH5_R8);
	H2->AddSecurityAssociation(afs1c, aH6_R10);
	H2->AddSecurityAssociation(abatallion, aH9_R14);
	H2->AddSecurityAssociation(aplatoon, aH10_R13);
	H2->AddSecurityAssociation(auav, aH3_R6);
	H2->AddSecurityAssociation(aaorc, aH18_R25b);
	H2->AddSecurityAssociation(auavcrew, aH4_R3);
	H2->AddSecurityAssociation(ajsotf, aH17_R24);
	H2->AddSecurityAssociation(aship2r, aH13_R17);
	H2->AddSecurityAssociation(aship2tl, aH16_R17);
	H2->AddSecurityAssociation(aaoc1, aH7_R11);
	H2->AddSecurityAssociation(aaoc2, aH19_R11);
	H2->AddSecurityAssociation(aap, aH15_R21);
	H2->AddSecurityAssociation(aisufist, aH1_R1);
	H2->AddSecurityAssociation(aship1r, aH11_R16);
	
	H4->AddSecurityAssociation(abrigade, aH8_R41);
	H4->AddSecurityAssociation(asoc, aH5_R8);
	H4->AddSecurityAssociation(afs1c, aH6_R10);
	H4->AddSecurityAssociation(abatallion, aH9_R14);
	H4->AddSecurityAssociation(aplatoon, aH10_R13);
	H4->AddSecurityAssociation(auav, aH3_R6);
	H4->AddSecurityAssociation(aaorc, aH18_R25b);
	H4->AddSecurityAssociation(afs2c, aH2_R5);
	H4->AddSecurityAssociation(ajsotf, aH17_R24);
	H4->AddSecurityAssociation(aship2r, aH13_R17);
	H4->AddSecurityAssociation(aship2tl, aH16_R17);
	H4->AddSecurityAssociation(aaoc1, aH7_R11);
	H4->AddSecurityAssociation(aaoc2, aH19_R11);
	H4->AddSecurityAssociation(aap, aH15_R21);
	H4->AddSecurityAssociation(aisufist, aH1_R1);
	H4->AddSecurityAssociation(aship1r, aH11_R16);		

	//SSC-SD - San Diego, CA TACLANEs
	H17->AddSecurityAssociation(abrigade, aH8_R41);
	H17->AddSecurityAssociation(asoc, aH5_R8);
	H17->AddSecurityAssociation(afs1c, aH6_R10);
	H17->AddSecurityAssociation(abatallion, aH9_R14);
	H17->AddSecurityAssociation(aplatoon, aH10_R13);
	H17->AddSecurityAssociation(auav, aH3_R6);
	H17->AddSecurityAssociation(aaorc, aH18_R25b);
	H17->AddSecurityAssociation(afs2c, aH2_R5);
	H17->AddSecurityAssociation(auavcrew, aH4_R3);
	H17->AddSecurityAssociation(aship2r, aH13_R17);
	H17->AddSecurityAssociation(aship2tl, aH16_R17);
	H17->AddSecurityAssociation(aaoc1, aH7_R11);
	H17->AddSecurityAssociation(aaoc2, aH19_R11);
	H17->AddSecurityAssociation(aap, aH15_R21);
	H17->AddSecurityAssociation(aisufist, aH1_R1);
	H17->AddSecurityAssociation(aship1r, aH11_R16);
	
	H13->AddSecurityAssociation(abrigade, aH8_R41);
	H13->AddSecurityAssociation(asoc, aH5_R8);
	H13->AddSecurityAssociation(afs1c, aH6_R10);
	H13->AddSecurityAssociation(abatallion, aH9_R14);
	H13->AddSecurityAssociation(aplatoon, aH10_R13);
	H13->AddSecurityAssociation(auav, aH3_R6);
	H13->AddSecurityAssociation(aaorc, aH18_R25b);
	H13->AddSecurityAssociation(afs2c, aH2_R5);
	H13->AddSecurityAssociation(auavcrew, aH4_R3);
	H13->AddSecurityAssociation(ajsotf, aH17_R24);
	H13->AddSecurityAssociation(aship2tl, aH16_R17);
	H13->AddSecurityAssociation(aaoc1, aH7_R11);
	H13->AddSecurityAssociation(aaoc2, aH19_R11);
	H13->AddSecurityAssociation(aap, aH15_R21);
	H13->AddSecurityAssociation(aisufist, aH1_R1);
	H13->AddSecurityAssociation(aship1r, aH11_R16);	
	
	H16->AddSecurityAssociation(abrigade, aH8_R41);
	H16->AddSecurityAssociation(asoc, aH5_R8);
	H16->AddSecurityAssociation(afs1c, aH6_R10);
	H16->AddSecurityAssociation(abatallion, aH9_R14);
	H16->AddSecurityAssociation(aplatoon, aH10_R13);
	H16->AddSecurityAssociation(auav, aH3_R6);
	H16->AddSecurityAssociation(aaorc, aH18_R25b);
	H16->AddSecurityAssociation(afs2c, aH2_R5);
	H16->AddSecurityAssociation(auavcrew, aH4_R3);
	H16->AddSecurityAssociation(ajsotf, aH17_R24);
	H16->AddSecurityAssociation(aship2r, aH13_R17);
	H16->AddSecurityAssociation(aaoc1, aH7_R11);
	H16->AddSecurityAssociation(aaoc2, aH19_R11);
	H16->AddSecurityAssociation(aap, aH15_R21);
	H16->AddSecurityAssociation(aisufist, aH1_R1);
	H16->AddSecurityAssociation(aship1r, aH11_R16);		

	H14->AddSecurityAssociation(aship1g, aH12_R16);

	//CPSG - Brooks AFB, SC TACLANEs
	H7->AddSecurityAssociation(abrigade, aH8_R41);
	H7->AddSecurityAssociation(asoc, aH5_R8);
	H7->AddSecurityAssociation(afs1c, aH6_R10);
	H7->AddSecurityAssociation(abatallion, aH9_R14);
	H7->AddSecurityAssociation(aplatoon, aH10_R13);
	H7->AddSecurityAssociation(auav, aH3_R6);
	H7->AddSecurityAssociation(aaorc, aH18_R25b);
	H7->AddSecurityAssociation(afs2c, aH2_R5);
	H7->AddSecurityAssociation(auavcrew, aH4_R3);
	H7->AddSecurityAssociation(ajsotf, aH17_R24);
	H7->AddSecurityAssociation(aship2r, aH13_R17);
	H7->AddSecurityAssociation(aship2tl, aH16_R17);
	H7->AddSecurityAssociation(aaoc2, aH19_R11);
	H7->AddSecurityAssociation(aap, aH15_R21);
	H7->AddSecurityAssociation(aisufist, aH1_R1);
	H7->AddSecurityAssociation(aship1r, aH11_R16);		

	H19->AddSecurityAssociation(abrigade, aH8_R41);
	H19->AddSecurityAssociation(asoc, aH5_R8);
	H19->AddSecurityAssociation(afs1c, aH6_R10);
	H19->AddSecurityAssociation(abatallion, aH9_R14);
	H19->AddSecurityAssociation(aplatoon, aH10_R13);
	H19->AddSecurityAssociation(auav, aH3_R6);
	H19->AddSecurityAssociation(aaorc, aH18_R25b);
	H19->AddSecurityAssociation(afs2c, aH2_R5);
	H19->AddSecurityAssociation(auavcrew, aH4_R3);
	H19->AddSecurityAssociation(ajsotf, aH17_R24);
	H19->AddSecurityAssociation(aship2r, aH13_R17);
	H19->AddSecurityAssociation(aship2tl, aH16_R17);
	H19->AddSecurityAssociation(aaoc1, aH7_R11);
	H19->AddSecurityAssociation(aap, aH15_R21);
	H19->AddSecurityAssociation(aisufist, aH1_R1);
	H19->AddSecurityAssociation(aship1r, aH11_R16);
	
	H15->AddSecurityAssociation(abrigade, aH8_R41);
	H15->AddSecurityAssociation(asoc, aH5_R8);
	H15->AddSecurityAssociation(afs1c, aH6_R10);
	H15->AddSecurityAssociation(abatallion, aH9_R14);
	H15->AddSecurityAssociation(aplatoon, aH10_R13);
	H15->AddSecurityAssociation(auav, aH3_R6);
	H15->AddSecurityAssociation(aaorc, aH18_R25b);
	H15->AddSecurityAssociation(afs2c, aH2_R5);
	H15->AddSecurityAssociation(auavcrew, aH4_R3);
	H15->AddSecurityAssociation(ajsotf, aH17_R24);
	H15->AddSecurityAssociation(aship2r, aH13_R17);
	H15->AddSecurityAssociation(aship2tl, aH16_R17);
	H15->AddSecurityAssociation(aaoc1, aH7_R11);
	H15->AddSecurityAssociation(aaoc2, aH19_R11);
	H15->AddSecurityAssociation(aisufist, aH1_R1);
	H15->AddSecurityAssociation(aship1r, aH11_R16);		
	
	//SSC-CH - Charleston, SC TACLANEs
	H1->AddSecurityAssociation(abrigade, aH8_R41);
	H1->AddSecurityAssociation(asoc, aH5_R8);
	H1->AddSecurityAssociation(afs1c, aH6_R10);
	H1->AddSecurityAssociation(abatallion, aH9_R14);
	H1->AddSecurityAssociation(aplatoon, aH10_R13);
	H1->AddSecurityAssociation(auav, aH3_R6);
	H1->AddSecurityAssociation(aaorc, aH18_R25b);
	H1->AddSecurityAssociation(afs2c, aH2_R5);
	H1->AddSecurityAssociation(auavcrew, aH4_R3);
	H1->AddSecurityAssociation(ajsotf, aH17_R24);
	H1->AddSecurityAssociation(aship2r, aH13_R17);
	H1->AddSecurityAssociation(aship2tl, aH16_R17);
	H1->AddSecurityAssociation(aaoc1, aH7_R11);
	H1->AddSecurityAssociation(aaoc2, aH19_R11);
	H1->AddSecurityAssociation(aap, aH15_R21);
	H1->AddSecurityAssociation(aship1r, aH11_R16);		

	H11->AddSecurityAssociation(abrigade, aH8_R41);
	H11->AddSecurityAssociation(asoc, aH5_R8);
	H11->AddSecurityAssociation(afs1c, aH6_R10);
	H11->AddSecurityAssociation(abatallion, aH9_R14);
	H11->AddSecurityAssociation(aplatoon, aH10_R13);
	H11->AddSecurityAssociation(auav, aH3_R6);
	H11->AddSecurityAssociation(aaorc, aH18_R25b);
	H11->AddSecurityAssociation(afs2c, aH2_R5);
	H11->AddSecurityAssociation(auavcrew, aH4_R3);
	H11->AddSecurityAssociation(ajsotf, aH17_R24);
	H11->AddSecurityAssociation(aship2r, aH13_R17);
	H11->AddSecurityAssociation(aship2tl, aH16_R17);
	H11->AddSecurityAssociation(aaoc1, aH7_R11);
	H11->AddSecurityAssociation(aaoc2, aH19_R11);
	H11->AddSecurityAssociation(aap, aH15_R21);
	H11->AddSecurityAssociation(aisufist, aH1_R1);
	
	H12->AddSecurityAssociation(aship1g, aH14_R17);
	Hy->AddSecurityAssociation(aship1g, aHx_R37);
	Hx->AddSecurityAssociation(aship2g, aHy_R35);
	
	/****************************JRAE 04-4 TACLANE Security Associations****************************/

	
	/******************************JRAE 04-4 TACLANE Routing Entries*******************************/
/*	
//	tl1->AddRoute(c1->GetIPAddr(), Mask(32), tl1->GetIfByNode(c1), c1->GetIPAddr());

	//PEO C3T - Ft. Hood, TX TACLANE
	H8->DefaultRoute(R41);
	H8->AddRoute(abrigade, Mask(32), H8->GetIfByNode(R40), aR40_H8);

	//JITC - Ft. Huachuca, AZ TACLANE
	H5->DefaultRoute(R8);

	//AFCA - Scott AFB, IL TACLANE
	H6->DefaultRoute(R10);

	//BCBL - Ft. Gordon, GA TACLANEs
	H9->DefaultRoute(R14);
	
	H10->DefaultRoute(R13);
	
	//NRL - Washington, D.C. TACLANEs
	H3->DefaultRoute(R6);
	
	H18->DefaultRoute(R25b);
	
	//JTEO - McLean, VA TACLANEs
	H2->DefaultRoute(R5);
	
	H4->DefaultRoute(R3);

	//SSC-SD - San Diego, CA TACLANEs
	H17->DefaultRoute(R24);
	
	H13->DefaultRoute(R17);
	
	H16->DefaultRoute(R17);

	H14->DefaultRoute(R17);

	//CPSG - Brooks AFB, SC TACLANEs
	H7->DefaultRoute(R11);

	H19->DefaultRoute(R11);
	
	H15->DefaultRoute(R21);
	
	//SSC-CH - Charleston, SC TACLANEs
	H1->DefaultRoute(R1);

	H11->DefaultRoute(R16);
	
	H12->DefaultRoute(R16);

	Hy->DefaultRoute(R36);

	Hx->DefaultRoute(R37);
*/	
	/******************************JRAE 04-4 TACLANE Routing Entries*******************************/
	

	s.NewLocation(-1.25, -0.5);
	s.NewLocation(20.75, 21);

	// Specify animation
	s.AnimationUpdateInterval(Time("2ms")); // 2ms initial update rate
	s.Progress(1);
	s.StopAt(100);
	s.Run();	
}
