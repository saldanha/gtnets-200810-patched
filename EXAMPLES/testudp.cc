// Georgia Tech Network Simulator - Test UDP
// George F. Riley.  Georgia Tech, Spring 2002

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#include <iostream>
#include <fstream>

#include "debug.h"
#include "simulator.h"
#include "node.h"
#include "duplexlink.h"
#include "application-cbr.h"
#include "ratetimeparse.h"
#include "trace.h"
#include "udp.h"
#include "validation.h"  

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;

 // Open the trace file
 Trace* gs = Trace::Instance();
 gs->Open("testudp.txt");
 gs->IPDotted(true);

 Node* n0 = new Node();
 Node* n1 = new Node();
 Node* pn = n0; // Prior
 Interface* pi = pn->AddInterface(L2Proto802_3(), 
                                  0xc0010001, Mask(24), MACAddr::Allocate());
 Link::DefaultRate(Rate("10Mb")); // Set to 10Megabits /sec 
 //Link::DefaultRate(Rate("1Mb"));  // Set to 1Megabits /sec 
 // Create 3 intermediate nodes
 for (int i = 0; i < 3; ++i)
   {
     Node* nn = new Node();
     nn->SetTrace(Trace::DISABLED);  // Trace nothing on this node
     Interface* ni = nn->AddInterface();
     DuplexLink(pi, ni);
     pi = nn->AddInterface(); // add right side interface
   }
 Interface* ni = n1->AddInterface(L2Proto802_3(),
                                  0xc0020001, Mask(24), MACAddr::Allocate());
 DuplexLink(pi, ni);

 // Increase detail of L3 trace messages
 IPV4::Instance()->DetailOn(IPV4::TOTALLENGTH);
 IPV4::Instance()->SetTrace(Trace::ENABLED);

 UDP* u0 = new UDP(n0);
 UDP* u1 = new UDP(n1);
 u0->Bind();
 u1->Bind(20000);
 n0->SetTrace(Trace::ENABLED); // Trace information to/from this protocol
 n1->SetTrace(Trace::ENABLED); // Trace information to/from this protocol
 u0->AddExtraTxDelay(Time("10ms"));
  u0->SendTo(100000, IPAddr("192.2.0.1"), 20000);

 // Let simulator know when to stop
 s.StopAt(10.0);

 // Start the simulation
 s.Run();
}
