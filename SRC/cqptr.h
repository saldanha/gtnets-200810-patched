// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: cqptr.h 410 2005-09-09 21:21:42Z dheeraj $



// Circular message passing queue, pointer references
// George F. Riley, Georgia Tech, Winter 2001

// Define pointer classes, for putting data in/out of CQueues
// Behave as pointers, with ++ and * operators
// Need a separate one for input pointers and output pointers

#ifndef __CQPTR_H__
#define __CQPTR_H__
#include "cq.h"
#include <cstring>

#ifdef OLD_JUST_TESTING
class CQBase {
public:
  CQBase(CQueue& cq, char* pBase)
    : m_cq(cq), m_base(pBase), m_current(cq.pFirst) { }
  CQBase(const CQBase& c)
    : m_cq(c.CQ()), m_base(c.Base()), m_current(c.Current()) { }
  CQueue& CQ() const { return m_cq;}
  size_t Current() const { return m_current;}
  void   Current(size_t c) { m_current = c;}
  char*  Base() const { return m_base;}
protected:
  CQueue& m_cq;
  char*   m_base;
  size_t  m_current;
};
#endif

//template <class T>class CQPtr : public CQBase {
template <class T>class CQPtr {
public :
  //  CQPtr(CQueue& cq, char* pBase) : CQBase(cq, pBase) { }
  CQPtr(CQueue& cq, char* pBase)
    : m_cq(cq), m_base(pBase) { }
  CQPtr(const CQPtr& c)
    : m_cq(c.CQ()), m_base(c.Base()), m_current(c.Current()) { }
  int Full(int k, int verbose)
    { // Will buffer hold k additional items?
      int r;
      size_t pOut = m_cq.pOut;
      if (m_current < pOut)
        {
          r = !((m_current+sizeof(T)*k) < pOut);
          if (r)
            { // Debug..log returning full and why
              return(r);
            }
        }
      if ((m_current + sizeof(T)*k) < m_cq.pLimit)
        return 0;//No wrap, not full
      r =  !(((m_current + sizeof(T)*k) - (m_cq.pLimit - m_cq.pFirst)) <
             pOut);
      if (r)
        {
        }
      return(r);
    }
  int Full(int k)
    { // Will buffer hold k additional items?
      return Full(k, 0);
    }
  int Full() { return Full(1);}
  int Empty() { return Empty(m_cq.pIn);}
  int Empty(size_t pIn) { return m_current == pIn;}
  void Round(int m)
    {
      if (!m) return; // No alignment needed
      while((m_current - m_cq.pFirst) & m)
        {
          if(Full())
            {
              int x = Full(1,1); // Call again for verbose
              std::cout << "Buffer overflow on alignment x " << x << std::endl;
              if (x)
                exit(1);
            }
          m_base[m_current++] = 0;
          if (m_current >= m_cq.pLimit) m_current = m_cq.pFirst;
        }
    }
  CQueue& CQ() const { return m_cq;}
  size_t  Current() const { return m_current;}
  void    Current(size_t c) { m_current = c;}
  char*   Base() const { return m_base;}
  void Adjust()
    {
      if (m_current >= m_cq.pLimit)
        m_current -= (m_cq.pLimit - m_cq.pFirst);
    }
  CQPtr& operator++()
    { m_current += sizeof(T); Adjust(); return *this;}
  CQPtr& operator++(int)
    { CQPtr& tmp = *this; m_current+=sizeof(T); Adjust(); return tmp;}
  CQPtr& operator+=(int k)
    { m_current += sizeof(T)*k; Adjust(); return *this;}
  T& operator*() {
    return *((T*)(&m_base[m_current]));
  }
  operator char*() { return &m_base[m_current];} // char* typecast
  virtual void Update() = 0; // Update pIn or pOut
  virtual void Memcpy(T* s, size_t lth) = 0; // Bulk copy to/from CQueue
  void DBDump()
    {
      std::cout << "pFirst " << m_cq.pFirst
           << " pIn " << m_cq.pIn << " pOut " << m_cq.pOut
           << " pLimit " << m_cq.pLimit << std::endl;
      std::cout << "m_base " << m_base << "  m_current " << m_current
                << std::endl;
    }
protected:
  CQueue& m_cq;
  char*   m_base;
  size_t  m_current;
};


template <class T> class CQOutPtr : public CQPtr<T> {
public:
  CQOutPtr(CQueue& cq, char* pBase)
    : CQPtr<T>(cq, pBase)
  { CQPtr<T>::m_current = cq.pOut;}
  CQOutPtr(const CQOutPtr& c) : CQPtr<T>(c) { } // Copy constructor
public:
  void Update() { CQPtr<T>::m_cq.pOut = CQPtr<T>::m_current; }
  void Memcpy(T* s, size_t lth)
    { // Copy data out of a CQueue to specified target
      if ((CQPtr<T>::m_current + lth*sizeof(T)) >= CQPtr<T>::m_cq.pLimit)
        { // Copy first part
          memcpy((char*)s, (char*)&CQPtr<T>::m_base[CQPtr<T>::m_current],
                 (CQPtr<T>::m_cq.pLimit-CQPtr<T>::m_current));
          lth -= (CQPtr<T>::m_cq.pLimit-CQPtr<T>::m_current)/sizeof(T);
          s += (CQPtr<T>::m_cq.pLimit-CQPtr<T>::m_current)/sizeof(T);
          CQPtr<T>::m_current = CQPtr<T>::m_cq.pFirst;
        }
      if (lth > 0)
        { // Copy last (only) part
          memcpy((char*)s, (char*)&CQPtr<T>::m_base[CQPtr<T>::m_current],
                 lth*sizeof(T));
        }
      CQPtr<T>::m_current += lth*sizeof(T);
      CQPtr<T>::Adjust();
    }
};

template <class T> class CQInPtr : public CQPtr<T> {
public:
  CQInPtr(CQueue& cq, char* pBase)
    : CQPtr<T>(cq, pBase)
  { CQPtr<T>::m_current = cq.pIn;}
  CQInPtr(const CQInPtr& c) : CQPtr<T>(c) { } // Copy constructor
public:
  void Update() { CQPtr<T>::m_cq.pIn = CQPtr<T>::m_current; }
  void Memcpy(T* s, size_t lth)
    {
      if ((CQPtr<T>::m_current + lth*sizeof(T)) >= CQPtr<T>::m_cq.pLimit)
        { // Copy first part
          memcpy((char*)&CQPtr<T>::m_base[CQPtr<T>::m_current], (char*)s, (CQPtr<T>::m_cq.pLimit-CQPtr<T>::m_current));
          lth -= (CQPtr<T>::m_cq.pLimit-CQPtr<T>::m_current)/sizeof(T);
          s += (CQPtr<T>::m_cq.pLimit-CQPtr<T>::m_current)/sizeof(T);
          CQPtr<T>::m_current = CQPtr<T>::m_cq.pFirst;
        }
      if (lth > 0)
        { // Copy last (only) part
          memcpy((char*)&CQPtr<T>::m_base[CQPtr<T>::m_current],
                 (char*)s, lth*sizeof(T));
        }
      CQPtr<T>::m_current += lth*sizeof(T);
      CQPtr<T>::Adjust();
    }
};

#endif
