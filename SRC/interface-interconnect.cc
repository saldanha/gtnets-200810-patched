// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: interface-interconnect.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - Supercomputer Interconnect Interface
// George F. Riley.  Georgia Tech, Spring 2002

#include <iostream>

#include "interface-interconnect.h"
#include "node-interconnect.h"
#include "l2proto-interconnect.h"
#include "queue.h"
#include "ratetimeparse.h"
#include "link.h"
#include "linkp2p.h"
#include "eventcq.h"
#include "mpi.h"

using namespace std;

//Rate_t          InterfaceInterconnect::defaultBw = Rate("1Gb");
//Time_t          InterfaceInterconnect::defaultDelay = Time("1ns");
//Time_t          InterfaceInterconnect::defaultDelay = Time("1us");
Rate_t          InterfaceInterconnect::defaultBw = Rate("10Mb");
Time_t          InterfaceInterconnect::defaultDelay = Time("10ms");


InterfaceInterconnect::InterfaceInterconnect(NodeInterconnect* n)
    : InterfaceBasic(n), l2proto(new L2ProtoInterconnect()),
      linkBw(defaultBw), linkDelay(defaultDelay),
      busy(false), peer(nil), pQueue(Queue::Default().Copy())
{ // Use a simple p2p link
  SetLink(new Linkp2p());
  GetLink()->SetLocalIf((Interface*)this);
}

InterfaceInterconnect::~InterfaceInterconnect()
{
  delete pQueue;
  delete l2proto;
  delete pLink;
}

// Inherited from InterfaceBasic
// Return the queue pointer
Queue*   InterfaceInterconnect::GetQueue() const
{
  return pQueue;
}

// Set a new queue object
void     InterfaceInterconnect::SetQueue(const Queue& q)
{
  delete pQueue;
  pQueue = q.Copy();
}

void     InterfaceInterconnect::Neighbors(NodeWeightVec_t& nwv, bool)
{ // Use constant link weights
  nwv.push_back(NodeIfWeight(peer->GetNode(), (Interface*)peer, 1));
}
      
// Inherited from Handler
void InterfaceInterconnect::Handle(Event* e, Time_t t)
{
  LinkEvent* le = (LinkEvent*)e; // Convert to right event type

  PDU*    l3p;
  Packet* p;
  switch (le->event) {
    case LinkEvent::PACKET_TX:
      // Link is free, deque next packet and send it
      busy = false;
      p = pQueue->Deque();
      if (p)
        { // Packet exists, send it
          Send(p);
        }
      break;
    case LinkEvent::PACKET_RX:
      evList->pop_front(); // Remove from fifo event list
      if (!evList->empty())
        { // Need to schedule the next simulator event
          EventPair* ep1 = evList->front();
          Time_t when = ep1->time - Simulator::Now();
          Scheduler::ScheduleEarly(ep1->event, when, this);
        }
      // Route the packet
      p = le->p;
      l3p = p->PeekPDU();
      if (l3p->Layer() != 3)
        {
          cout << "HuH?  Not expected l3 pdu in II::Handle" << endl;
          // Drop the pkt
          // ! Add tracing!
          delete p;
        }
      else
        {
          MPIHeader* mpiH = (MPIHeader*)l3p;
          GridLocation nh;
          GridLocation& dstGrid = mpiH->dstGrid;
          // See if at destination
          NodeInterconnect* ni = (NodeInterconnect*)pNode;
          if (dstGrid == ni->GetGridLocation())
            {
              MPI* mpi = ni->mpiDemux;
              mpi->DataIndication(p);
            }
          else
            { // Find appropriate output interface
              InterfaceInterconnect* nextIf = ni->Route(dstGrid, nh);
              if (!nextIf)
                {
                  cout << "HuH?  No output if for dst " << dstGrid 
                       << " at gridloc " << GetGridLocation() << endl;
                  delete p;
                  // ! Tracing !
                }
              else
                {
                  nextIf->Send(p);
                }
            }
        }
    default:
      break;
  }
  delete e;
}

// Send a packet to a specific MPId process
bool InterfaceInterconnect::Send(Packet* p)
{
  // If already sending, queue and return
  if (busy) return pQueue->Enque(p);

  // Set l2src and l2dst in header
  MPIHeader* mpiH = (MPIHeader*)p->PeekPDU();
  mpiH->l2SrcGrid = GetGridLocation();
  mpiH->l2DstGrid = peer->GetGridLocation();

  // Schedule link free event
  Time_t lft = p->Size() * 8.0 / linkBw;
  LinkEvent* lfe = new LinkEvent(LinkEvent::PACKET_TX);
  Scheduler::Schedule(lfe, lft, this);
  
  // Schedule packet rx event at peer interface
  Time_t rxt = lft + linkDelay; // Packet rx time
  LinkEvent* rxe = new LinkEvent(LinkEvent::PACKET_RX, p);
  peer->ScheduleRx(rxe, rxt);
  return true;
}

GridLocation InterfaceInterconnect::GetGridLocation()
{
  NodeInterconnect* ni = (NodeInterconnect*)GetNode();
  return ni->GetGridLocation();
}

