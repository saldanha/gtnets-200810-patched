// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: histograph.cc 153 2004-10-15 18:07:39Z riley $



// Georgia Tech Network Simulator - Histograph Class
// George F. Riley.  Georgia Tech, Fall 2003

// Defines a Histogram and CDF statistics collection
// and displays graphically using Qt.

#include <iostream>

#include "debug.h"
#include "histograph.h"

#ifdef HAVE_QT
#include <qapplication.h>
#include <qcanvas.h>
#endif

using namespace std;

Histograph::Histograph()
    : Histogram(), initialized(false), showCDF(false),
      updateRate(1), updateCountdown(1)
#ifdef HAVE_QT
      , app(nil), canvas(nil), view(nil)
#endif
{
}

Histograph::Histograph(double m)
    : Histogram(m), initialized(false), showCDF(false),
      updateRate(1), updateCountdown(1), 
      canvasXY(nil), app(nil), canvas(nil), view(nil),
      textRect(nil)
{
}

Histograph::Histograph(double m, Count_t b, double mn)
    : Histogram(m, b, mn), initialized(false), showCDF(false),
      updateRate(1), updateCountdown(1),
      canvasXY(nil), app(nil), canvas(nil), view(nil),
      textRect(nil)
{
}

Histograph::Histograph(const Histograph& c) // Copy constructor
    : Histogram(c), initialized(false), showCDF(c.showCDF),
      updateRate(c.updateRate), updateCountdown(c.updateRate),
      canvasXY(nil), app(nil), canvas(nil), view(nil),
      textRect(nil)
{
}


Histograph::~Histograph()
{
#ifdef HAVE_QT
  if (app) delete app;
#endif
}

Statistics* Histograph::Copy() const   // Make a copy of the histograph
{
  return new Histograph(*this);
}

void Histograph::UpdateRate(Count_t c )
 {
   updateRate = c;
 }

// Overridden methods from Histogram
bool Histograph::Record(double v)
{
  bool r = Histogram::Record(v);
  if (!updateCountdown || --updateCountdown == 0)
    { // Time to update
      if (showCDF)
        {
          UpdateCDF();
        }
      else
        {
          UpdateHisto();
        }
      updateCountdown = updateRate;
    }
  return r;
}

void Histograph::Reset()
{
  Histogram::Reset();
  if (showCDF)
    {
      UpdateCDF();
    }
  else
    {
      UpdateHisto();
    }
}

// Private methods
void Histograph::UpdateCDF()
{
#ifdef HAVE_QT
  if (!initialized) Initialize();
  Count_t total = 0;
  Count_t nBins = bins.size();
  for (Count_t i = 0; i < nBins; ++i)
    {
      total += bins[i];
    }
  total += outOfRange;
  double  x = textRect->width(); // Initial x coordinate
  double  xAdder = (double)(canvasXY->x() - textRect->width()) / bins.size();
  Count_t cumulative = 0;
  Count_t usableY = canvasXY->y() - (int)(textRect->height() * 1.5);
  QPoint prior((int)x, canvasXY->y() - (int)(textRect->height()));

  for (Count_t i = 0; i < nBins; ++i)
    { //  Draw the cdf
      if (items.size() <= i)
        { // too small, add empty
          items.push_back(nil);
        }
      cumulative += bins[i];
      x += xAdder;
      double height = (double)cumulative / total * usableY;
      QPoint p1((int)x, (int)(canvasXY->y()-int(textRect->height())-height));
      // check if same, and if so don't change (code later)
      if (items[i]) delete items[i];
      QCanvasLine* l = new QCanvasLine(canvas);
      items[i] = l;
      l->setPoints(prior.x(), prior.y(), p1.x(), p1.y());
      l->setPen(Qt::black);
      l->show();
      prior = p1;
    }
  
  // Draw the registration lines
  const Count_t n = 10;
  for (Count_t i = 0; i <= n; ++i)
    {
      double v  = (double)i / n;
      Count_t h = (Count_t)(usableY * v);
      
      // labels and lines always same size, so we take a shortcut here
      if (i < labels.size())
        {
          delete labels[i];
          delete lines[i];
        }
      else
        {
          labels.push_back(nil);
          lines.push_back(nil);
        }
      QString qs = QString("%1").arg(v);
      labels[i] = new QCanvasText(qs, canvas);
      labels[i]->moveBy(0,
                        canvasXY->y() - (int)(textRect->height()*1.5) - h);
      labels[i]->show();
      lines[i] = new QCanvasLine(canvas);
      lines[i]->setPen(Qt::lightGray);
      lines[i]->setPoints(textRect->width(), 
                          canvasXY->y() - (int)(textRect->height()) - h,
                          canvasXY->x(), 
                          canvasXY->y() - (int)(textRect->height()) - h);
      lines[i]->show();
    }
  canvas->update(); // Show the updates
  
  while(app->hasPendingEvents())
    {
      app->processEvents(1);
    }
#endif
}

void Histograph::UpdateHisto()
{
#ifdef HAVE_QT
  if (!initialized) Initialize();
  if (bins.size())
    { // If bins not empty
      Count_t largest = 0;
      for (Count_t i = 0; i < bins.size(); ++i)
        {
          if (bins[i] > largest) largest = bins[i];
        }
      
      // Compute height of each bar
      Count_t nBins = bins.size();
      Count_t usableY = canvasXY->y() - (int)(textRect->height() * 1.5);
      for (Count_t b = 0; b < nBins; ++b)
        {
          Count_t height = (Count_t)(usableY * (bins[b]/(double)largest));
          DEBUG0((cout << "Height of bin " << b << " is " << height << endl));
          QCanvasRectangle* rect = (QCanvasRectangle*)items[b];
          rect->setSize(rect->width(), -height);
        }
      // Draw the registration lines and labels
      Count_t n = 10;
      if (largest < n) n = largest;
      double each = (double)largest / n;
      
      for (Count_t i = 0; i <= n; ++i)
        {
          double v  = each * i;
          Count_t h = (Count_t)(usableY * v / largest);
          Count_t intV = (Count_t)v;

          // labels and lines always same size, so we take a shortcut here
          if (i < labels.size())
            {
              delete labels[i];
              delete lines[i];
            }
          else
            {
              labels.push_back(nil);
              lines.push_back(nil);
            }
          QString qs = QString("%1").arg(intV);
          labels[i] = new QCanvasText(qs, canvas);
          labels[i]->moveBy(textRect->width() -
                            labels[i]->boundingRect().width(),
                            canvasXY->y() - (int)(textRect->height()*1.5) - h);
          labels[i]->show();
          lines[i] = new QCanvasLine(canvas);
          lines[i]->setPen(Qt::lightGray);
          lines[i]->setPoints(textRect->width(), 
                              canvasXY->y() - (int)(textRect->height()) - h,
                              canvasXY->x(), 
                              canvasXY->y() - (int)(textRect->height()) - h);
          lines[i]->show();
        }
      canvas->update(); // Show the updates
    }
  
  while(app->hasPendingEvents())
    {
      app->processEvents(1);
    }
#endif
}

void Histograph::Initialize()
{
#ifdef HAVE_QT
  int argc = 1;
  char* argv[] = { "GTNetS", nil};
  app = new QApplication(argc, argv);

  canvasXY = new QPoint(640, 640);
  QString qs;
  if (showCDF)
    {
      qs = QString("%1").arg(0.9);
    }
  else
    {
      qs = QString("%1").arg(12345);
    }
  // Fudge the Y size by one and a half times
  // text height.  This allows for a bottom row
  // of box labels and half of the x-axis label
  // on top
  QCanvasText txt(qs, canvas);
  textRect = new QRect(txt.boundingRect());
  canvasXY->setY(canvasXY->y() + (int)(textRect->height() * 1.5));
  // Adjust x by size of left side labels
  canvasXY->setX(canvasXY->x() + textRect->width());

  Count_t xAdder = 0;
  if (showCDF)
    { // Need to finish coding this...
    }
  else
    {
      // xAdder is width of each bar (integer pixels)
      xAdder = (canvasXY->x() - textRect->width()) / bins.size();
      // Since possibly not evenly divisible, resize canvas
      // to be correct size. Fudge two extra pixels
      xAdder+=2; // two extra pixels between bars
      //canvasXY->setX(textRect->width() + xAdder * bins.size() + xAdder);
      // And fudge the X size by the width of a 5 digit label
      canvasXY->setX(textRect->width() + xAdder * bins.size() + 4); // + 4???
    }
  
  // Create the canvas
  canvas = new QCanvas(canvasXY->x()-4, canvasXY->y());
  canvas->setDoubleBuffering(true);

  // Create the bars for the histogram or lines for CDF
  for (Count_t i = 0; i < bins.size(); ++i)
    {
      if (showCDF)
        {
          QCanvasLine* cp = new QCanvasLine(canvas);
          items.push_back(cp);
          // Will set endpoints later
        }
      else
        {
          QCanvasRectangle* cp = new QCanvasRectangle(canvas);
          // Set  zero height for now, will adjust later
          cp->setSize(xAdder, 0);
          // And move to correct position
          cp->moveBy(textRect->width() + xAdder * i,
                     canvasXY->y() - textRect->height());
          cp->setPen(Qt::black);
          cp->setBrush(Qt::red);
          cp->show();
          items.push_back(cp);
        }
    }
  
  view   = new QCanvasView(canvas);
  //view->setHScrollBarMode(QScrollView::AlwaysOff);
  //view->setVScrollBarMode(QScrollView::AlwaysOff);
  view->resize(canvasXY->x(), canvasXY->y());
  view->show();
  app->setMainWidget(view);
  while(app->hasPendingEvents())
    {
      app->processEvents(1);
    }
#endif
  initialized = true;
}
