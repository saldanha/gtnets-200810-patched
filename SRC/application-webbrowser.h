// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-webbrowser.h 500 2006-07-20 21:37:22Z rsimpson $



// Georgia Tech Network Simulator - Web Browser Application Class
// George F. Riley.  Georgia Tech, Fall 2002

// Define a Web Browser Applicatoinn class

#ifndef __tcp_application_browser_h__
#define __tcp_application_browser_h__

#include "common-defs.h"
#include "application.h"
#include "tcp.h"
#include "rng.h"
#include "statistics.h"
//#include "http-distributions.h" // models based on Bruce Mah, Infocom 1997
#include "netihttp-distributions.h"	// models based on Charles R. Simpson, Jr., Dheeraj Reddy, George Riley, PADS 2006

#define HTTP_PORT 80
#define CONNECT_RETRY 3

typedef std::vector<IPAddr_t> IPAddrVec_t;     // List of ip addresses

// Define a class with info for a single connection
//Doc:ClassXRef
class BrowserConnection {
public:
  BrowserConnection();
  BrowserConnection(const TCP& tcp);
public:
  TCP*    t;       // TCP Connection
  Time_t  start;   // Time request initialized
  Count_t rqSize;  // Request size
  Count_t rsSize;  // Response size
  Count_t rxSize;  // Bytes received so far
  Count_t cnCount; // Connection re-try Count
  bool    active;  // True if active
  bool    logged;  // True if already logged
  bool    closed;  // True if already closed
  bool    first;   // True if first connection
};

typedef std::vector<BrowserConnection> ConnectionVec_t;

// Define the start/stop events
//Doc:ClassXRef
class AppWebBrowserEvent : public Event {
public:
  typedef enum { REQUEST_PAGE = 100 } AppWebBrowserEvent_t;
  AppWebBrowserEvent() { }
  AppWebBrowserEvent(Event_t ev) : Event(ev) { }
};

//Doc:ClassXRef
class WebBrowser : public Application {
  //Doc:Class Simulation model of a web browser.  The browser behavior uses
  //Doc:Class empirical distributions (based on a study by Bruce Mah
  //Doc:Class published in Infocom 1997) to define web browsing actions.
  //Doc:Class Each browser performs the following actions.  First a
  //Doc:Class server is selected randomly from the server list passed
  //Doc:Class in the constructor, and a connection with that server
  //Doc:Class is established.  Then the number of objects (individual
  //Doc:Class connections) needed to get the web page is sampled from
  //Doc:Class the empirical distributions.  For each object needed,
  //Doc:Class a request size and a response size are sampled from the
  //Doc:Class empirical distribution, and the request is sent to the server.
  //Doc:Class If more than one object is needed, a second simultaneous
  //Doc:Class connection is opened and a second request is sent.
  //Doc:Class When the entire request has been received by a web server,
  //Doc:Class the number of bytes from the response size are sent to the
  //Doc:Class browser, and the connection is closed.  When the
  //Doc:Class browser receives all the bytes of the requested object
  //Doc:Class and detects the connection closing, another object is
  //Doc:Class requested (if any remain for this page).  After all
  //Doc:Class of the objects for a given page are received, the browser
  //Doc:Class "sleeps" for a period of time (sampled from the empirical
  //Doc:Class distribution).  After the sleep time is over, another
  //Doc:Class web page (again with multiple objects) is requested,
  //Doc:Class either from the same server or a new one, depending
  //Doc:Class on the empirical random distribution of {\em Consecutive
  //Doc:Class Pages}, which indicates how many times the same server
  //Doc:Class is accessed consecutively.


public:
  // Constructor
  //Doc:Method
  WebBrowser(const IPAddrVec_t&,           // List of servers
             const Random&,                // Random to select server
             const TCP& = TCP::Default()); // TCP Variant to us
    //Doc:Desc Constructor for Web Browser objects.
    //Doc:Arg1 A vector of \IPA{s} to sample for selecting a web server.
    //Doc:Arg2 A random number generator to sample for selecting a web server.
    //Doc:Arg3 A reference to a {\tt TCP} variant to use for all connections.

  //Doc:Method
  WebBrowser(const WebBrowser& c);             // Copy constructor
    //Doc:Desc Copy Constructor
    //Doc:Arg1 Web Browser object to copy.

  virtual ~WebBrowser();
  void Handle(Event*, Time_t);
  // Upcalls from L4 protocol
  virtual void Receive(Packet*,L4Protocol*,Seq_t);   // Data received
  virtual void CloseRequest(L4Protocol*);      // Close request from peer
  virtual void Closed(L4Protocol*);            // Connection has closed
  virtual void ConnectionComplete(L4Protocol*);// Connection request succeeded
  virtual void ConnectionFailed(L4Protocol*, bool);// Connection request failed
  // Called from base application class
  virtual void StartApp();            // Called at time specified by Start
  virtual void StopApp();             // Called at time specified by Stop
  virtual void AttachNode(Node*);     // Note which node attached to
  virtual Application* Copy() const;  // Make a copy of the application
  // Browser application methods
  
  //Doc:Method
  TCP* GetTCP() { return tcp; }
    //Doc:Desc   Get the TCP prototype used for this browser.
    //Doc:Return Pointer to TCP prototype

  //Doc:Method
  void ConcurrentConnections(Count_t);// Specify number concurrent conn's
    //Doc:Desc Specifies the number of concurrent connections for this browser.
    //Doc:Arg1 Count of concurrent connections.

  //Doc:Method
  void SetStatistics(Statistics* s) { stats = s;}
    //Doc:Desc Set a statistics object used for logging web response time.
    //Doc:Arg1 A pointer to any statistics object (such as a histogram).

  void SetDebugStats(Statistics* s) { dbstats = s;}
  //Doc:Method
  void IdleTime(Time_t i) { idleTime = i;}
    //Doc:Desc Specify the idle time.  The browser will stop initiating requests
    //Doc:Desc at this time, allowing the simulation to (eventually) finish.
    //Doc:Arg1 Idle time in simulation seconds.

  void DBDump();                      // Debug data dump
  void Verbose(bool v = true) { verbose = v;}

public:
  //Doc:Method
  void        ThinkTimeBound(Time_t b) { thinkTimeBound = b;}
    //Doc:Desc Specify a bound on the think time.  The empirical distributions
    //Doc:Desc for think time allow a fairly large think time, which
    //Doc:Desc can result in a simulation where most of the browsers
    //Doc:Desc are thinking.  This allows an arbitrary upper bound
    //Doc:Desc on the think time.
    //Doc:Arg1 Think time bound in simulation seconds.

  //Doc:Method
  static void InitializeRandom();
    //Doc:Desc Initialize the random numbers used by web browsers.

  //Doc:Method
  static void DefaultConcurrentConnections(Count_t);
    //Doc:Desc Specify the default number of concurrent connections
    //Doc:Desc for all web browser objects.
    //Doc:Arg1 Number of concurrent connections.

private:
  // Find the connection info given the protocol pointer
  BrowserConnection* FindConnection(L4Protocol*);
  void               RequestWebPage();// Request the next web page
  void               RequestOneFile(BrowserConnection*); // Request object
  void               ObjectComplete(BrowserConnection*); // Object completed
public:
  AppWebBrowserEvent* pendingEvent;   // Pending SendData event
  Node*              node;            // Attached node
  TCP*               tcp;             // TCP to use for connections
  Statistics*        stats;           // Stats object for logging response time
  Statistics*        dbstats;         // Stats for debugging
  const IPAddrVec_t  servers;         // List of servers
  Random*            selectServer;    // Server selector RNG
  Count_t            concurrentConn;  // Number of concurrent connections
  ConnectionVec_t    connections;     // Existing connections
  IPAddr_t           serverIP;        // IP Address of last server used
  Count_t            consecutive;     // Remaining number of consecutive req
  Count_t            nObjects;        // Number of web objects this page
  Count_t            nObjectsRq;      // Number of web objects requested
  Count_t            nObjectsRx;      // Number of web objects received
  Time_t             thinkTimeBound;  // Bound on think time
  Time_t             idleTime;        // Time to stop requesting
  bool               verbose;         // True for verbose debugging
public:
/*
  // To save memory, we use shared RNG's for the Mah RV's
  static HttpPrimaryRequest*   primaryRequest;
  static HttpSecondaryRequest* secondaryRequest;
  static HttpPrimaryReply*     primaryReply;
  static HttpSecondaryReply*   secondaryReply;
  static HttpFilesPerPage*     filesPerPage;
  static HttpConsecutivePages* consecutivePages;
  static HttpThinkTime*        thinkTime;
*/
  // To save memory, we use shared RNG's for the RV's
  static NETIHttpRequest*          request;
  static NETIHttpReply*            reply;
  static NETIHttpConsecutivePages* consecutivePages;
  static NETIHttpDiffThinkTime*    diffThinkTime;
  static NETIHttpSameThinkTime*    sameThinkTime;

  // Default concurrent connection count
  static Count_t               defaultConcurrentConn;

  // Statistics for debugging
  static Count_t               activeBrowsers;
  static Count_t               activeConnections;
  static Count_t               completedConnections;
  static Count_t               failedConnections;
  static Count_t               startedThisPeriod;
  static Count_t               completedThisPeriod;
  static Time_t                totRespTime;
  static DCount_t              totRespSize;
  static Count_t               totObjectsPerPage;
  static Count_t               totBrowserSessions;
  static Time_t                largestRespTime;
  static Count_t               largestRespSize;
  static Time_t                totWaitTime;
  static Time_t                largestWaitTime;
  static Count_t               waitCount;
  static DCount_t              totReqBytes;   // Total size of requests
  static DCount_t              totRespBytes;  // Total size of responses
  static Count_t               cleanConnections; // Number completed cn's with no retx or timeout
  static Count_t               dirtyConnections; // Number completed cn's not clean
  static Count_t               clientTimeouts;
  static Count_t               serverTimeouts;
};

#endif

