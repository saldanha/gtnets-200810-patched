#include <iostream>
#include <sstream>
#include <string>
#include <strstream>

#include "debug.h"
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "node-satellite.h"
#include "node-taclane.h"
#include "star.h"
#include "linkp2p.h"
#include "application-cbr.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "application-udpsink.h"
#include "rng.h"
#include "routing.h"
#include "routing-manual.h"
#include "diffserv-queue.h"
#include "time-value.h"
#include "time-value-graph.h"
#include "tcp-tahoe.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "udp.h"
#include "validation.h"
#include "qtwindow.h"
#include "args.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

using namespace std;



int main(int argc, char** argv)
{
  bool enabled = true;
  Arg("enabled", enabled, true);
  Validation::Init(argc, argv);
  Arg::ProcessArgs(argc, argv);
  
  Simulator s;
  // Set node shape to a circle for animation
  //	Node::DefaultShape(Node::CIRCLE);
	
  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->Open("testtl1.txt");
  IPV4::Instance()->SetTrace(Trace::ENABLED);
	
  s.StartAnimation(0, true);
	
  IPAddr_t ac1_tl1 = IPAddr("192.168.1.2");
  IPAddr_t atl1_c1 = IPAddr("192.168.1.1");
  IPAddr_t atl1_tl2 = IPAddr("134.207.136.1");
  IPAddr_t atl2_tl1 = IPAddr("134.207.136.2");
  IPAddr_t atl2_c2 = IPAddr("192.168.2.1");
  IPAddr_t ac2_tl2 = IPAddr("192.168.2.2");

  Node* c1 = new Node();
  TACLANENode* tl1 = new TACLANENode(atl1_c1, atl1_tl2, RB);
  Node* c2 = new Node();
  TACLANENode* tl2 = new TACLANENode(atl2_c2, atl2_tl1, RB);

  if (!enabled) cout << "Disabling taclanes" << endl;
  tl1->Enable(enabled);
  tl2->Enable(enabled);
  
  //c1->CustomShapeFile(redrouter);
  //tl1->CustomShapeFile(rbtaclane);
  //c2->CustomShapeFile(redrouter);
  //tl2->CustomShapeFile(rbtaclane);
	
  c1->SetLocation(0.5, 1);
  tl1->SetLocation(2.5, 1);
  c2->SetLocation(6.5, 1);
  tl2->SetLocation(4.5, 1);
	
  s.NewLocation(0, 0);
  s.NewLocation(7, 2);
	
  tl1->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
  tl2->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	
  Linkp2p tl_link(Rate("1Mb"), Time("1ms"));

  c1->AddDuplexLink(tl1, tl_link, ac1_tl1, Mask(32), atl1_c1, Mask(32));
  c2->AddDuplexLink(tl2, tl_link, ac2_tl2, Mask(32), atl2_c2, Mask(32));
  tl1->AddDuplexLink(tl2,tl_link, atl1_tl2,Mask(32), atl2_tl1,Mask(32));
	
  tl1->AddSecurityAssociation(IPAddr(ac2_tl2), IPAddr(atl2_tl1));
  tl2->AddSecurityAssociation(IPAddr(ac1_tl1), IPAddr(atl1_tl2));

  // Create the TCP Server
  TCPServer* server1 = (TCPServer*)c2->AddApplication(
      TCPServer(TCPTahoe()));
  if (server1)
    {
      server1->BindAndListen(80); // Application on s1, port 80
      server1->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
      server1->GetL4()->SetColor(Qt::blue);
    }
	 
  // Create the TCP Sending Applications
  TCPSend* client1 = (TCPSend*)c1->AddApplication(
      TCPSend(ac2_tl2, 80, Constant(500000), TCPTahoe()));
  if (client1)
    {
      // Enable TCP trace for this client
      client1->SetTrace(Trace::ENABLED);
      // Set random starting time
      client1->Start(0);
      client1->GetL4()->SetColor(Qt::green);
    }
  // Specify animation
  s.AnimationUpdateInterval(Time("1us")); // 1us initial update rate
  s.Progress(1);
  s.StopAt(10);
  s.Run();	
}
