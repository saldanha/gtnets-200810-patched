// GENERAL PUBLIC LICENSE AGREEMENT
//
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
//
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
//
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
//
// 1.  This License allows you to:
//
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
//
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//
//
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
//
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
//
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
//
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
//
//
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
//
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
//
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
//
//
// 3.  Export Law Assurance.
//
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//
// 4.  Termination.
//
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
//
//
// 5.  Disclaimer of Warranties and Limitation on Liability.
//
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
//
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
//
// GTRC shall have no obligation for support or maintenance of Program.
//
// 6.  Copyright Notice.
//
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
//
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
//
// Said copyright notice shall read as follows:
//
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


// Uncomment below to enable debug level 0
//#define DEBUG_MASK 0x01

#include <iostream>
#include "debug.h"
#include "routing-nvr.h"
#include "simulator.h"
#include "node.h"
#include "globalstats.h"
#include "queue.h"
#include "priqueue.h"
#include "interface.h"
#include "packet.h"
#include "l2proto802.11.h"

#define DEBUGX(x)
//#define DEBUGX(x)  x

#define TO_INIT  (defaultRTT0 * (TTL_INIT + 2))
#define MYIP     ((string)IPAddr(node->GetIPAddr()))

using namespace std;

// Default setting
bool RoutingNVR::enableL2Notify = true;
bool RoutingNVR::enableRingSearch = true;
bool RoutingNVR::enableDelayedNVReq = true;
bool RoutingNVR::enableOnlyDstReply = false;
bool RoutingNVR::enableNCUnicast = true;  // do not change
bool RoutingNVR::enablePnumFilter = false;
bool RoutingNVR::enableNVCheck = false;
bool RoutingNVR::enableBlackList = false;
Word_t RoutingNVR::defaultNVReqListSize = NVREQ_LIST_SIZE;
Byte_t RoutingNVR::defaultNVReqRetry = 2;
Word_t RoutingNVR::defaultSendBufSize = SEND_BUF_SIZE;
Time_t RoutingNVR::defaultSendBufPeriod = 0;
Time_t RoutingNVR::defaultSendBufTimeout = 30;
Time_t RoutingNVR::defaultRTT0 = 0.08;  // 80 msec


// NVRLiteHeader

Size_t NVRLiteHeader::Size() const
{
  Count_t sz =
      1 +                 // type
      sizeof(pidOrg) +
      sizeof(pidQnum) +
      sizeof(pidPnum) +
      nvRoute.SSize();
  return sz;
}


// NCSymReqHeader

Size_t NCSymReqHeader::Size() const
{
  Count_t sz =
      NVRLiteHeader::Size() +
      2 +                // id, mid
      //sizeof(src) +
      sizeof(dst) +
      9 +                // sizeof(mv) = 1 + 4 + 4
      6 +                // from_inf MAC address
      rnv.SSize();
  return sz;
}


// NCSymRepHeader

Size_t NCSymRepHeader::Size() const
{
  Count_t sz =
      NVRLiteHeader::Size() +
      1 +                // mid
      //sizeof(src) +
      sizeof(dst) +
      9 +                // sizeof(mv)
      9 +                // sizeof(cmv)
      6 +                // from_inf MAC address
      nv.SSize();
  return sz;
}


// NVRFailHeader
NVRFailHeader::NVRFailHeader(IPAddr_t d, IPAddr_t o, IPAddr_t e)
    : NVRLiteHeader(NV_ROT_FAIL), dst(d), org(o), end(e)
{
}

NVRFailHeader::NVRFailHeader(IPAddr_t d, IPAddr_t o, IPAddr_t e,
                             const CRouteInfo* ri)
    : NVRLiteHeader(NV_ROT_FAIL), dst(d), org(o), end(e)
{
  pidOrg = ri->pid.org;
  pidQnum = ri->pid.sn;
  pidPnum = ri->pid.dn;
  nvRoute = ri->nv;
}

Size_t NVRFailHeader::Size() const
{
  Count_t sz =
      NVRLiteHeader::Size() +
      sizeof(dst) + sizeof(org) + sizeof(end);
  return sz;
}


// RoutingNVR

RoutingNVR::RoutingNVR()
{
  nvQnum = 0;
  nvPnum = 0;
  evSendBuf = nil;
  evSendBufTimeout = nil;
  nvreqQue.reserve(5);
  //nvreqSlot = new Uniform(0, 20);           // time slot
  //nvreqJitter = new Uniform(0, 0.00001);   // jitter, 10 microsec
  nvreqJitter = new Uniform(0, 0.01);
  //nvreqJitter = new Uniform(0, 0.3);
}

RoutingNVR::~RoutingNVR()
{
  if (evSendBuf) delete evSendBuf;
  //if (nvreqSlot) delete nvreqSlot;
  if (nvreqJitter) delete nvreqJitter;
}

void RoutingNVR::InitializeRoutes(Node* n)
{
  node = n;

  // Register L2 xmit failure handler
  if (enableL2Notify) {
    IFVec_t ifv = n->Interfaces();
    for (IFVec_t::iterator i = ifv.begin(); i != ifv.end(); ++i)
      ((L2Proto802_11*)(*i)->GetL2Proto())->xmitFailHandler = this;
  }
}

// external, called from IP
void RoutingNVR::DataRequest(Node* n, Packet* p, void* v)
{
  IPV4ReqInfo* ipInfo = (IPV4ReqInfo*)v;
  DEBUG0(cout << MYIP << ": data request to " << (string)ipInfo->dst
         << " uid " << p->uid << " time " << Simulator::Now() << endl
         << "  data request size " << p->Size() << endl);

  CRouteInfo* ri = GetRouteInfo(ipInfo->dst);
  if (!ri) {
    // Need queueing this packet, process this pending packet later
    AddIPHeader(p, ipInfo->dst, ipInfo->ttl, ipInfo->l4proto);
    SendBufferEnque(p, ipInfo->dst);

    // Check if there is already pending NV request for the same dst
    if (FindNVReq(ipInfo->dst) >= 0) return;

    // Black list check
    if (enableBlackList)
      if (blackList.Find(ipInfo->dst)) return;

    // Invoke NV creation process
    if (enableRingSearch) {
      Byte_t ttl;
      Time_t to;
      int i = NVFIB.Find(ipInfo->dst, true);  // forced search
      //if (i >= 0 && Simulator::Now()-NVFIB.Item[i]->time <= 10.0) {
      if (i >= 0) {
        ttl = NVFIB.Item[i]->mv.hop;
        if (ttl > TTL_THRESH) ttl = TTL_MAX;
        to = ttl < TTL_MAX ? defaultRTT0*(ttl+2) : defaultRTT0*ttl;
      }
      else { ttl = TTL_INIT; to = TO_INIT; }
      SendNVRequest(n, ipInfo->dst, ttl, to);
    }
    else SendNVRequest(n, ipInfo->dst, TTL_MAX, 10.0);
    return;
  }

  if (defaultSendBufPeriod > 0) {
    // Store this pkt if there is another pending pkt for the same dst
    SendBuf_t::iterator i = SendBufferFind(ipInfo->dst);
    if (i != sendBuffer.end()) {  // found
      AddIPHeader(p, ipInfo->dst, ipInfo->ttl, ipInfo->l4proto);
      SendBufferEnque(p, ipInfo->dst);
      return;
    }
  }

  // Update NV timestamp
  UpdateNVTime(ri->pid);

  // Unicast
  NVRLiteHeader* nvrhdr =
      new NVRLiteHeader(ri->pid.org, ri->pid.sn, ri->pid.dn, ri->nv);
  L3Transmit(p, ipInfo->dst, ipInfo->ttl, ipInfo->l4proto, nvrhdr);
}

// internal
void RoutingNVR::DataSend(Node* n, Packet* p)
{
  IPV4Header* iph = (IPV4Header*)p->PeekPDU();
  CRouteInfo* ri = GetRouteInfo(iph->dst);
  if (!ri) {
    cout << "HuH? no NV for " << (string)iph->dst << endl;
    delete p;  // L3 drop
    return;
  }

  // Update NV timestamp
  UpdateNVTime(ri->pid);

  // Unicast
  NVRLiteHeader* nvrhdr =
      new NVRLiteHeader(ri->pid.org, ri->pid.sn, ri->pid.dn, ri->nv);
  iph->options.push_back(nvrhdr);
  MACAddr next = Lookup(nvrhdr->nvRoute, true);
  iph->totalLength = (Word_t)p->SizeUpdate();
  Unicast(n->Interfaces()[0], p, next);
}

// Add L3 header and transmit
void RoutingNVR::L3Transmit(Packet* p, IPAddr_t dst, Count_t ttl,
                            Proto_t l4proto, NVROption* option)
{
  IPV4Header* iphdr = AddIPHeader(p, dst, ttl, l4proto);
  if (option) iphdr->options.push_back(option);

  if ((IPAddr_t)iphdr->dst == IPAddrBroadcast) {
    // Broadcast
    iphdr->totalLength = (Word_t)p->SizeUpdate();
    Broadcast(node->Interfaces()[0], p);
    return;
  }
  // Unicast
  // Process NVR option, only NV_ROT and NV_ROT_LITE subtypes
  NVRLiteHeader* nvrhdr = (NVRLiteHeader*)option;
  if (nvrhdr->nvRoute.IsEnd()) { // nv exhausted
    cout << "HuH? NV exhausted\n";
    delete p;
    return;
  }
  //MACAddr next = Lookup(nvrhdr->nvRoute, nvrhdr->opt & NVR_OPT_SYM);
  MACAddr next = Lookup(nvrhdr->nvRoute, true);
  iphdr->totalLength = (Word_t)p->SizeUpdate();
  Unicast(node->Interfaces()[0], p, next);
}

CRouteInfo* RoutingNVR::GetRouteInfo(IPAddr_t dst)
{
  int i = NVFIB.Find(dst);
  return i < 0 ? nil : NVFIB.Item[i];
}

CRouteInfo* RoutingNVR::GetRouteInfo(const PathId& pid, bool force)
{
  int i = NVFIB.Find(pid, force);
  return i < 0 ? nil : NVFIB.Item[i];
}

MACAddr RoutingNVR::Lookup(CNixVector& nvec, bool reduce, bool force)
{
  MACAddr aix, next_hop;
  Word_t old_nvu = nvec.nvu;

  if (nvec.Get(aix)) {
    // Other color nix
    //if (!NbrTable.Nbr[aix.Lower()].Valid()) {
    if (!NbrTable.Nbr[aix.macAddr].Valid()) {
      DEBUG0(cout << "  trying to access invalid neighbor at index "
             << aix.macAddr << endl);

      /* The force flag is used for forced sending of NV_ROT_FAIL
         This is one of solutions for "Trapped Packet Problem"
      */
      if (!force) return MACAddr::NONE;
    }
    next_hop = NbrTable.Nbr[aix.macAddr].mac;
    DEBUG0(cout << "next_hop " << next_hop << endl);
  }
  else {
    // White color nix
    next_hop = aix;
    int nix = NbrTable.Find(aix.macAddr);
    if (nix < 0) nix = NbrTable.Add(aix.macAddr);
    else NbrTable.Nbr[nix].val = true;    // validate the neighbor
    if (nix < 0) {
      DEBUG0(cout << "Lookup(): fail to NbrTable::Add()\n");
      return MACAddr::NONE;
    }

    CNixVector tnv, nnv;
    tnv.Set(nix);
    nnv.ExpandBits(nvec.nvl+tnv.nvl-NO_COLOR_BITS-NO_MAC_BITS);

    bitcpy(nnv.nv, 0, nvec.nv, 0, old_nvu);
    nnv.nvl += old_nvu;
    bitcpy(nnv.nv, nnv.nvl, tnv.nv, 0, tnv.nvl);
    nnv.nvl += tnv.nvl;
    bitcpy(nnv.nv, nnv.nvl, nvec.nv, nvec.nvu, nvec.nvl-nvec.nvu);
    nnv.nvl += nvec.nvl-nvec.nvu;
    nnv.nvu = old_nvu + tnv.nvl;
    nvec = nnv;
  }

  if (reduce) {
    CNixVector tnv;
    tnv.ExpandBits(nvec.nvl-nvec.nvu);
    tnv.nvl = nvec.nvl-nvec.nvu;
    bitcpy(tnv.nv, 0, nvec.nv, nvec.nvu, tnv.nvl);
    nvec = tnv;
  }

  return next_hop;
}

bool RoutingNVR::DataIndication(Interface* i, Packet* p)
{
  IPV4Header* iph = (IPV4Header*)p->PeekPDU();
  NVROption* opt = (NVROption*)iph->options[0];
  //VQue.Update(p->Size());

  /*
  DEBUG0(cout << MYIP << ": NVR_DataIndication"
         << " type " << opt->type << " uid " << p->uid
         << " time " << Simulator::Now() << endl
         << "  load updated " << VQue.Load()
         << " Byteseconds " << VQue.Byteseconds << endl);
  */

  switch (opt->type) {
    case NVROption::NV_ROT_LITE: return DoNVRotLite(i, p, opt);
    case NVROption::NV_ROT_FAIL: DoNVRotFail(i, p, opt);  break;
    case NVROption::NC_REQ_SYM:  DoNCSymReq(i, p, opt);   break;
    case NVROption::NC_REP_SYM:  DoNCSymRep(i, p, opt);   break;
    default:
      cout << MYIP << ": HuH? received unknown packet\n";
      delete p;
  }
  return true;
}

// Private Methods
bool RoutingNVR::DoNVRotLite(Interface* i, Packet* p, NVROption* opt)
{
  // Process NV route-lite option
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
  NVRLiteHeader* hdr = (NVRLiteHeader*)opt;

  DEBUGX(cout << MYIP << ": NVR_DoNVRotLite"
         << " time " << Simulator::Now() << endl
         << "  src " << (string)iphdr->src << " dst " << (string)iphdr->dst
         << " uid " << p->uid << endl);

  if (node->GetIPAddr() == iphdr->src) {
    // Got packet originated by itself, just drop it
    DEBUGX(cout << MYIP << ": astray packet uid " << p->uid << endl);
    delete p;
    Stats::pktsDropped++;
    return true;
  }

  // Update NV timestamp
  UpdateNVTime(PathId(iphdr->dst,hdr->pidOrg,hdr->pidQnum,hdr->pidPnum));

  if (node->GetIPAddr() == iphdr->dst) {
    // Destined for this node
    DEBUG0(cout << MYIP << ": received data uid " << p->uid
           << " time " << Simulator::Now() << " src " << (string)iphdr->src
           << " size " << p->Size() << endl);
    return false;
  }

  // Need to forward
  if (hdr->nvRoute.IsEnd()) { // nv exhausted
    cout << "HuH? NV exhausted\n";
    delete p;
    return true;
  }

  // Construct RNV back to src
  if (!enableNCUnicast) {
    DATA_frame *df = (DATA_frame*)p->PeekPDU(-2);
    CNixVector rnv;
    ConstructNV(rnv, hdr->rnvRoute, df->addr2);
    hdr->rnvRoute = rnv;
  }

  // Forward to the next hop
  MACAddr next = Lookup(hdr->nvRoute, true);
  iphdr->totalLength = (Word_t)p->SizeUpdate();
  Unicast(i, p, next);
  return true;
}

//#define DST_SUPPRESS

void RoutingNVR::DoNCSymReq(Interface* i, Packet* p, NVROption* opt)
{
  NCSymReqHeader* hdr = (NCSymReqHeader*)opt;

  /*
  PriQueue* ifq = (PriQueue*)i->pQueue;
  cout << MYIP << ": received NVREQ time " << Simulator::Now() << endl
       << "  dst " << (string)hdr->dst << " src " << (string)hdr->src
       << " nc-id " << hdr->id
       << " hop " << hdr->mv.hop+1 << " load " << hdr->mv.pks << endl
       << "  ifq_len " << ifq->AverageLength() << endl;
  */

  if (node->GetIPAddr() == hdr->src) {  // NVREQ originated by self
    delete p;
    return;
  }

  CRepItem* item;
  MACAddr mac = i->GetMACAddr();

  if (node->GetIPAddr() == hdr->dst) {
    // Destined for this node
    //hdr->mv.pks = (Long_t)(hdr->mv.pks/hdr->mv.hop);
    hdr->mv.hop++;
    item = NRList.Find(hdr->src, hdr->id, NVROption::NC_REP_SYM);

    if (item) {  // already replied
      /* Don't do this because
         we want to reply to the 1st request only. 10/06/2003

         if (item->mv.hop <= hdr->mv.hop) {
           delete p;
           DEBUG0(cout << "  already processed, deleted" << endl);
           return;
         }
      */
      if (++item->cnt > 0) {
        delete p;
        DEBUG0(cout << "  already processed, deleted" << endl);
        return;
      }
    }

    // Construct RNV and store it
    // RNV, the NV back to org
    CNixVector rnv;
    if (!ConstructNV(rnv, hdr->rnv, hdr->from_inf)) return;

    // Store RNV
    NVFIB.Add(PathId(hdr->src, hdr->dst, hdr->id, ++nvPnum),
              hdr->mid, hdr->mv, rnv);

    DEBUG0(cout << MYIP << ": nix vector cached"
           << " time " << Simulator::Now() << endl
           << "  dst " << (string)hdr->src << " org " << (string)hdr->dst
           << " sn " << hdr->id << " dn " << nvPnum << endl
           << "  hop " << hdr->mv.hop << " nvl " << rnv.nvl << endl);
    DEBUG0(cout << "  nv ");
    DEBUG0(rnv.Dump());

    // NVREP packet
    Packet* rp = new Packet();
    NCSymRepHeader* rhdr = CreateNCSymRep(hdr, mac);

    IPV4Header* iphdr = new IPV4Header();
    iphdr->ttl = IPV4::DefaultTTL;
    iphdr->protocol = (Byte_t)Proto();
    iphdr->src = node->GetIPAddr();
    iphdr->dst = rhdr->src;
    iphdr->options.push_back(rhdr);
    rp->PushPDU(iphdr);
    iphdr->totalLength = (Word_t)rp->Size();

    MACAddr next_hop = hdr->from_inf;
    delete p;
    Unicast(i, rp, next_hop, true);
    if (item)
      item->mv = hdr->mv;
    else
      NRList.Add(rhdr->src, rhdr->pidQnum, rhdr->type, rhdr->mid, rhdr->mv);

    DEBUGX(cout << MYIP << ": sent NVREP time " << Simulator::Now() << endl
           << "  src " << (string)rhdr->src << " dst " << (string)rhdr->dst
           << " sn " << rhdr->pidQnum << " dn " << rhdr->pidPnum << endl
           << " hop " << rhdr->mv.hop
           << " load " << (double)rhdr->mv.pks << endl
           << "  nvl " << rhdr->nv.nvl
           << " rnvl " << rhdr->nvRoute.nvl << endl);
    return;
  }

  // Need to forward
  // Check if this request was recently processed
  if (NCList.Find(hdr->id, hdr->src, hdr->dst) >= 0) {
    // Just ignore, already processed it
    delete p;
    DEBUG0(cout << "  already processed, deleted\n");
    return;
  }
  NCList.Add(hdr->id, hdr->src, hdr->dst, hdr->from_inf);
  DEBUG0(cout << "# of nc items " << NCList.Item.size() << endl);

  // Process currnet metric vector
  hdr->mv.hop++;

  // Forward, or do NV reply if has NV for dst
  // Construct RNV
  CNixVector rnv;
  if (!ConstructNV(rnv, hdr->rnv, hdr->from_inf)) {
    delete p;
    return;
  }

  if (enableNCUnicast) {
    // Unicast to dst using NV in the header if it is valid
    if (hdr->nvRoute.IsValid()) {
      if (enableNVCheck) {
        // Check availability of the NV in NV-FIB
        int j =
            NVFIB.Find(PathId(hdr->dst,hdr->pidOrg,hdr->pidQnum,hdr->pidPnum),
                       true);  // forced
        if (j < 0) {  // not available
          DEBUGX(cout << MYIP << ": nv unavailable dst " << (string)hdr->dst
                 << " org " << (string)hdr->pidOrg
                 << " sn " << hdr->pidQnum << " dn " << hdr->pidPnum << endl);
          delete p;
          return;
        }
      }

      if (hdr->nvRoute.IsEnd()) { // nv exhausted
        cout << "HuH? NV exhausted\n";
        delete p;
        return;
      }
      hdr->rnv = rnv;
      hdr->from_inf = mac;
      MACAddr next_hop = Lookup(hdr->nvRoute, true);
      IPV4Header* iph = (IPV4Header*)p->PeekPDU();
      iph->totalLength = (Word_t)p->SizeUpdate();
      Unicast(i, p, next_hop, true);
      return;
    }
  }

  // Check if has NV for dst
  CRouteInfo* ri = enableOnlyDstReply ? nil : GetRouteInfo(hdr->dst);
  if (ri && enablePnumFilter)
    if (ri->pid.dn < hdr->pidPnum) ri = nil;  // force broadcast

  if (!ri) {
    // Check TTL in IP header before forwarding
    IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
    if (!iphdr->ttl) {
      delete p;
      DEBUG0(cout << "  TTL expired, deleted\n");
      return;
    }
    hdr->rnv = rnv;
    iphdr->totalLength = (Word_t)p->SizeUpdate();
    hdr->from_inf = mac;
    if (enableDelayedNVReq) {
      // Delay sending out NCREQ to avoid collision by broadcast
      //Time_t nc_delay = nvreqSlot->IntValue() * 100e-6;
      //Time_t nc_delay = nvreqSlot->IntValue() * 510e-6 + nvreqJitter->Value();
      Time_t nc_delay = nvreqJitter->Value();
      //PriQueue* ifq = (PriQueue*)i->pQueue;
      //double len = min(30, ifq->LengthPkts());
      //Time_t nc_delay = 0.01*(exp(0.693147/30*len)-1 + nvreqJitter->Value());
      //Time_t nc_delay = 0.01 * (len/30 + nvreqJitter->Value());
      DEBUG0(cout << "nc_delay " << nc_delay << endl);
      if (nc_delay == 0)
        Broadcast(node->Interfaces()[0], p);
      else {
        AddNCDelay(p, nc_delay);
        DEBUG0(cout << "# of delayed NVREQs " << nvreqQue.size() << endl);
      }
    }
    else
      Broadcast(node->Interfaces()[0], p);
  }
  else {
    // Has a valid NV to dst stored
    if (enableNCUnicast) {
      // Forward NVREQ to dst using the NV
      if (hdr->nvRoute.IsValid()) cout << "HuH? valid NV in nvreq\n";
      // Copy stored NV to the header
      hdr->nvRoute = ri->nv;
      hdr->pidOrg = ri->pid.org;
      hdr->pidQnum = ri->pid.sn;
      hdr->pidPnum = ri->pid.dn;
      /* Get IP header and set the src field to itself
         Also set the dst field to the NVREQ target
         and need to set the ttl field to default
      */
      IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
      iphdr->ttl = IPV4::DefaultTTL;
      iphdr->src = node->GetIPAddr();
      iphdr->dst = hdr->dst;

      // Unicast to dst using NV in the header
      hdr->rnv = rnv;
      hdr->from_inf = mac;
      if (hdr->nvRoute.IsEnd()) { // nv exhausted
        cout << "HuH? NV exhausted\n";
        delete p;
        return;
      }
      MACAddr next_hop = Lookup(hdr->nvRoute, true);
      iphdr->totalLength = (Word_t)p->SizeUpdate();
      Unicast(i, p, next_hop, true);
    }
    else {
      // Return NV reply
      // Store RNV
      CMetricVec mv;
      mv.hop = hdr->mv.hop;
      mv.pks = hdr->mv.pks/hdr->mv.hop;    // wrong, yj
      NVFIB.Add(PathId(hdr->src, node->GetIPAddr(), hdr->id, ri->pid.dn),
                hdr->mid, mv, rnv, NV_ATTR_UNI);

      Packet* rp = new Packet();
      NCSymRepHeader* rhdr = CreateNCSymRep(hdr, mac, ri);
      IPV4Header* iphdr = new IPV4Header();
      iphdr->ttl = IPV4::DefaultTTL;
      iphdr->protocol = (Byte_t)Proto();
      iphdr->src = node->GetIPAddr();
      iphdr->dst = rhdr->src;
      iphdr->options.push_back(rhdr);
      rp->PushPDU(iphdr);
      iphdr->totalLength = (Word_t)rp->Size();

      MACAddr next_hop = hdr->from_inf;
      delete p;
      Unicast(i, rp, next_hop, true);

      DEBUG0(cout << MYIP << ": sent NVREP time " << Simulator::Now() << endl
             << "  src " << (string)rhdr->src << " dst " << (string)rhdr->dst
             << " nvl " << rhdr->nv.nvl
             << " rnvl " << rhdr->nvRoute.nvl << endl);
    }
  }
}

void RoutingNVR::DoNCSymRep(Interface* i, Packet* p, NVROption* opt)
{
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
  NCSymRepHeader* hdr = (NCSymRepHeader*)opt;

  Time_t now = Simulator::Now();
  DEBUG0(cout << MYIP << ": NVREP received time " << now << endl
         << "  src " << (string)hdr->src << " dst " << (string)hdr->dst
         << " hop " << hdr->mv.hop << " nvl " << hdr->nv.nvl << endl);

  if (node->GetIPAddr() == hdr->src) {
    // Destined for this node
    //hdr->cmv.hop++;

    // Construct NV
    CNixVector nnv;
    if (!ConstructNV(nnv, hdr->nv, hdr->from_inf)) {
      delete p;
      return;
    }

    // Cancel NVREQ timeout
    Time_t acq_time = 0;
    int j = FindNVReq(hdr->dst);
    if (j >= 0) {
      acq_time = Simulator::Now() - nvreqList[j].time;
      CancelTimer(nvreqList[j].timer, true);
    }

    // Store NV, need more comparison, yj
    CRouteInfo* ri = nil;
    //ri = GetRouteInfo(hdr->dst);
    //if (ri && ri->pid.dn <= hdr->pidPnum) ri = nil;  // force to store
    ri = GetRouteInfo(PathId(hdr->dst,hdr->src,hdr->pidQnum,hdr->pidPnum));
    Byte_t attr = iphdr->src.ip==hdr->dst.ip ? 0 : NV_ATTR_UNI;
    if (!ri) {
      // First reply, store NV
      NVFIB.Add(PathId(hdr->dst, hdr->src, hdr->pidQnum, hdr->pidPnum),
                hdr->mid, hdr->mv, nnv, attr, true);

      DEBUGX(cout << MYIP << ": NV stored"
             << " acq_time " << acq_time << " time " << now << endl
             << "  dst " << (string)hdr->dst << " org " << (string)hdr->src
             << " sn " << hdr->pidQnum << " dn " << hdr->pidPnum << endl
             << "  hop " << hdr->mv.hop
             << " load " << (double)hdr->mv.pks << " psi " << hdr->mv.sig
             << " nvl " << nnv.nvl << endl);
      DEBUG0(cout << "  nv ");
      DEBUG0(nnv.Dump());
      DEBUGX(PrintRoute(nnv));

    }
    else {
      /*
      // Already has it, update only when it is better
      bool update = false;
      if (update) {
        ri->pid.sn = hdr->pidQnum;
        ri->pid.dn = hdr->pidPnum;
        ri->mid = hdr->mid;
        ri->attr = attr;
        ri->mv = hdr->mv;
        ri->nv = nnv;
        ri->time = now;
        timer.Cancel(&ri->ev);
        timer.Schedule(&ri->ev, CNVFIB::defaultTimeout1, &NVFIB);

        DEBUGX(cout << MYIP << ": nix vector updated"
               << " time " << Simulator::Now() << endl
               << "  dst " << (string)hdr->dst << " hop " << hdr->mv.hop
               << " sn " << hdr->pidQnum << " dn " << hdr->pidPnum << endl
               << "  load " << (double)hdr->mv.pks << " psi " << hdr->mv.sig
               << " nvl " << nnv.nvl << endl);
        DEBUG0(cout << "  nv ");
        DEBUG0(nnv.Dump());
        DEBUGX(PrintRoute(nnv));
      }
      */
    }

    IPAddr_t dst = hdr->dst;  // copy dst for pending pkt process
    delete p;  // done with this

    // Process pending packets
    if (defaultSendBufPeriod <= 0)
      ProcessPending(node, dst);  // immediatley
    else
      ProcessPendingPeriod(node, dst);  // periodically
    return;
  }

  // Need to forward
  // Check routing loop before normal processing
  PathId pid(hdr->dst, hdr->src, hdr->pidQnum, hdr->pidPnum);
  int j = NVFIB.Find(pid, true);
  if (j >= 0) {  // loop found
    DEBUGX(cout << MYIP << ": detected loop\n");
    //NVFIB.Remove(j);  // remove nv
    //j = NVFIB.Find(PathId(pid.org, pid.dst, pid.sn, pid.dn), true);
    //if (j >= 0) NVFIB.Remove(j);  // remove rnv
    delete p;
    return;
  }

  // Normal processing
  hdr->cmv.hop++;

  // Construct NV and store it, store RNV
  // Construct NV
  CNixVector nnv;
  if (!ConstructNV(nnv, hdr->nv, hdr->from_inf)) {
    delete p;
    return;
  }
  hdr->nv = nnv;

  // Store NV to dst
  CMetricVec mv;
  mv.hop = hdr->cmv.hop;
  mv.pks = hdr->mv.pks;
  Byte_t attr = iphdr->src.ip==hdr->dst.ip ? 0 : NV_ATTR_UNI;
  NVFIB.Add(pid, hdr->mid, mv, nnv, attr);

  DEBUG0(cout << MYIP << ": nix vector cached time " << now << endl
         << "  dst " << (string)hdr->dst << " org " << (string)hdr->src
         << " sn " << hdr->pidQnum << " dn " << hdr->pidPnum << endl
         << "  hop " << mv.hop << " load " << mv.pks
         << " nvl " << nnv.nvl << endl);
  DEBUG0(cout << "  nv ");
  DEBUG0(nnv.Dump());
  DEBUG0(PrintRoute(nnv));

  // Store RNV, the NV back to org
  mv.hop = hdr->mv.hop - hdr->cmv.hop;
  mv.pks = hdr->mv.pks;   // wrong, yj
  NVFIB.Add(PathId(hdr->src, iphdr->src, hdr->pidQnum, hdr->pidPnum),
            hdr->mid, mv, hdr->nvRoute, attr);

  DEBUG0(cout << MYIP << ": nix vector cached time " << now << endl
         << "  dst " << (string)hdr->src << " org " << (string)iphdr->src
         << " sn " << hdr->pidQnum << " dn " << hdr->pidPnum << endl
         << "  hop " << mv.hop << " load " << mv.pks
         << " nvl " << hdr->nvRoute.nvl << endl);
  DEBUG0(cout << "  nv ");
  DEBUG0(hdr->nvRoute.Dump());
  DEBUG0(PrintRoute(hdr->nvRoute));

  if (hdr->nvRoute.IsEnd()) { // nv exhausted
    cout << "HuH? NV exhausted\n";
    delete p;
    return;
  }
  MACAddr next_hop = Lookup(hdr->nvRoute, true);
  iphdr->totalLength = (Word_t)p->SizeUpdate();
  DEBUG0(cout << "next by nvRoute " << next_hop << endl);

  /*
  int j = NCList.Find(hdr->pidQnum, hdr->src, hdr->dst);
  MACAddr from;
  if (j < 0) {
    DEBUG0(cout << "can't find nc item\n");
    from = MACAddr::NONE;
  }
  else
    from = NCList.At(j)->from_inf;
  DEBUG0(cout << "from " << (string)from << endl);
  if (!(from == next_hop))
    DEBUG0(cout << "HuH? next_hop doesn't coincide with from"
           << " next_hop " << next_hop.Lower()
           << " from " << from.Lower() << endl);
  */

  hdr->from_inf = i->GetMACAddr();
  Unicast(i, p, next_hop, true);
}

void RoutingNVR::DoNVRotFail(Interface* i, Packet* p, NVROption* opt)
{
  NVRFailHeader* hdr = (NVRFailHeader*)opt;

  DEBUG0(cout << MYIP << ": NVRFAIL received"
         << " time " << Simulator::Now() << endl
         << "  org " << (string)hdr->org << " dst " << (string)hdr->dst
         << " end " << (string)hdr->end << endl);

  // Replace NV to dst with NV to end
  //CRouteInfo* ri =
  //GetRouteInfo(PathId(hdr->dst,hdr->org,hdr->pidQnum,hdr->pidPnum));
  int j;
  if (enableNCUnicast)
    j = NVFIB.Find(PathId(hdr->dst,hdr->org,hdr->pidQnum,hdr->pidPnum));
  else
    j = NVFIB.FindByDst(hdr->dst, hdr->pidPnum);

  if (j >= 0) {
    /* Invalidate instead of replacing

    if (ri->nv.Advance(hdr->cmv.hop)) {
    CNixVector nnv;
    nnv.ExpandBits(ri->nv.nvu);
    bitcpy(nnv.nv, 0, ri->nv.nv, 0, ri->nv.nvu);
    nnv.nvl = ri->nv.nvu;

    ri->target = hdr->end;
    ri->mv.hop = hdr->cmv.hop;
    ri->nv = nnv;

    DEBUG0(cout << MYIP << ": nix vector replaced"
    << " time " << Simulator::Now() << endl
    << "  dst " << (string)hdr->end << " hop " << ri->mv.hop
    << " nvl " << ri->nv.nvl << endl
    << "  nv ");
    DEBUG0(ri->nv.Dump());
    }
    else {
    // Something wrong in the NV
    NVFIB.Remove(hdr->dst, hdr->org);
    NVFIB.Remove(hdr->org, hdr->dst);
    }
    */

    // Invalidate the NV
    NVFIB.Invalidate(j);
  }

  if (node->GetIPAddr() == hdr->org) {
    // Destined for this node
    DEBUGX(cout << MYIP << ": received NV_ROT_FAIL at "
           << Simulator::Now() << endl);
    delete p;  // done with this
    return;
  }

  /* Don't do this
     Maybe need to be invalidate???

     // Change origin of stored NV to org into end
     ri = GetRouteInfo(hdr->org, hdr->dst);
     if (ri) ri->origin = hdr->end;
  */

  // Need to forward
  /* The Lookup()'s 3rd parameter is the force flag
     The force flag is turned on to solve the "Trapped Packet Problem"
     when forwarding NV_ROT_FAIL messages
  */
  if (hdr->nvRoute.IsEnd()) { // nv exhausted
    cout << "HuH? NV exhausted\n";
    delete p;
    return;
  }
  MACAddr next_hop = Lookup(hdr->nvRoute, true, true);  // forced search
  IPV4Header* iph = (IPV4Header*)p->PeekPDU();
  iph->totalLength = (Word_t)p->SizeUpdate();

  Unicast(i, p, next_hop, true);
}

void RoutingNVR::DoL2Fail(Interface* i, Packet* p)
{
  DEBUG0(cout << MYIP << ": detected L2 error uid " << p->uid
         << " time " << Simulator::Now() << endl);

  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
  NVROption* opt = (NVROption*)iphdr->options[0];
  switch (opt->type) {
    case NVROption::NV_ROT_LITE:
      DEBUGX(cout << "  NV_ROT_LITE\n"); break;

    case NVROption::NC_REP_SYM:
      DEBUGX(cout << "  NC_REP_SYM\n");  break;

    case NVROption::NC_REQ_SYM: {
      DEBUGX(NCSymReqHeader* hdr = (NCSymReqHeader*)opt);
      DEBUGX(cout << "  NC_REQ_SYM dst " << (string)hdr->dst
             << " src " << (string)hdr->src << " id " << hdr->id << endl);
      break;
    }

    case NVROption::NV_ROT_FAIL: {
      DEBUGX(cout << "  NV_ROT_FAIL\n");
      /* Don't break but return here.
         Otherwise, NV back to src may not be found.
         We need quite acurate movement detection method
         to determine if a neighbor really moved out or not.
         Theshold-based re-xmit could be one option.

      break;
      */
    }

    default:
      delete p;
      Stats::pktsDropped++;
      return;
  } //switch

  NVRLiteHeader* hdr = (NVRLiteHeader*)opt;
  IPAddr dst = iphdr->dst;
  IPAddr org = hdr->pidOrg;
  Byte_t sn = hdr->pidQnum;
  Byte_t dn = hdr->pidPnum;

  // Remove NV to dst and invalidate associated neighbor
  unsigned int inbr=0;
  bool nv_found = true;
  //int j = NVFIB.Find(dst, org, true);  // forced search
  int j;
  if (enableNCUnicast)
    j = NVFIB.Find(PathId(dst,org,sn,dn), true);  // forced search
  else
    j = NVFIB.FindByDst(dst, dn, true);

  if (j < 0) {
    DEBUGX(cout << "  HuH? fail to find NV dst " << (string)dst
           << " org " << (string)org << " sn " << sn << " dn " << dn << endl);
    nv_found = false;
  }
  else {
    // Find associated neighbor from NV and invalidate it
    CNixVector nv = NVFIB.Item[j]->nv;  // must copy
    MACAddr aix;
    nv.Get(aix);
    inbr = aix.macAddr;
    if (!NbrTable.InvalidateIndex(inbr)) {  // retry
      delete p;
      Stats::pktsDropped++;
      return;
    }
    DEBUGX(cout << "  invalidated " << NbrTable.Nbr[inbr].mac.macAddr << endl);

    // Invalidate NV
    //cout << MYIP << ": ";
    NVFIB.Invalidate(j);
  }

  // Check if the packet is originated by NV origin
  if (node->GetIPAddr() != org)
    ReturnNVRotFail(PathId(dst,org,sn,dn), true, hdr->rnvRoute);  // forced

  // Also search for other NVs using the same link
  // If so, invalidate the NV and send error message too
  if (nv_found) {
    // Search NV-FIB for valid NVs using the same link
    for (RIVec_t::size_type i = 0; i < NVFIB.Item.size(); ++i) {
      CRouteInfo* ri = NVFIB.Item[i];
      if (!ri || !ri->Valid()) continue;
      CNixVector nv = ri->nv;  // must copy
      MACAddr next;
      nv.Get(next);
      if (inbr != next.macAddr) continue;
      DEBUGX(cout << "wow\n");
      //cout << MYIP << ": ";
      NVFIB.Invalidate(i);
      //if (ri->pid.org == org && ri->pid.dst == dst) continue;
      if (node->GetIPAddr() != ri->pid.org)
        ReturnNVRotFail(ri->pid, false, CNixVector());
    }
  }

  delete p;
  Stats::pktsDropped++;
}

void RoutingNVR::ReturnNVRotFail(const PathId& pid, bool force,
                                 const CNixVector& rnv)
{
  // Check if the packet is originated by NV origin
  if (node->GetIPAddr() == pid.org) return;

  NVRFailHeader* hdr;
  // If got RNV, use it
  if (rnv.nvl) {
    hdr = new NVRFailHeader(pid.dst, pid.org, node->GetIPAddr());
    hdr->pidOrg = node->GetIPAddr();
    hdr->pidQnum = pid.sn;
    hdr->pidPnum = pid.dn;
    hdr->nvRoute = rnv;
  }
  else {
    // Retrieve RNV from NV-FIB, the NV back to org
    //CRouteInfo* ri = GetRouteInfo(org, dst, force);  // forced search if true
    CRouteInfo* ri = GetRouteInfo(PathId(pid.org,pid.dst,pid.sn,pid.dn), force);
    if (!ri) {
      DEBUGX(cout << "  no NV back to org " << (string)IPAddr(pid.org) << endl);
      return;
    }
    DEBUGX(cout << "  found NV back to org " << (string)IPAddr(pid.org)
           << " dst " << (string)IPAddr(pid.dst) << " sn " << ri->pid.sn
           << " dn " << ri->pid.dn << " nvl " << ri->nv.nvl
           << " nvu " << ri->nv.nvu << endl);
    DEBUG0(cout << " nv ");
    DEBUG0(ri->nv.Dump());
    DEBUGX(PrintRoute(ri->nv));

    /* Don't do this.
       Otherwise, NV back to org for in-flight packets cannot be found.
       It is hopefully ok to leave it as it is because now it is end point.

       // Change origin of NV back to org into current node
       ri->origin = node->GetIPAddr();
    */

    hdr = new NVRFailHeader(pid.dst, pid.org, node->GetIPAddr(), ri);
  }

  // Return NVRFail message to origin
  Packet* p = new Packet();

  IPV4Header* iph = new IPV4Header();
  iph->ttl = IPV4::DefaultTTL;
  iph->protocol = (Byte_t)Proto();
  iph->src = node->GetIPAddr();
  iph->dst = pid.org;
  iph->options.push_back(hdr);
  p->PushPDU(iph);

  /* The Lookup()'s 3rd parameter is the force flag
     The force flag is turned on to solve the "Trapped Packet Problem"
     when forwarding NV_ROT_FAIL messages
  */
  if (hdr->nvRoute.IsEnd()) { // nv exhausted
    cout << "HuH? NV exhausted\n";
    delete p;
    return;
  }
  MACAddr next_hop = Lookup(hdr->nvRoute, true, true);
  iph->totalLength = (Word_t)p->SizeUpdate();
  Unicast(node->Interfaces()[0], p, next_hop, true);
}

NCSymRepHeader* RoutingNVR::CreateNCSymRep(NCSymReqHeader* hdr, MACAddr& mac,
                                           CRouteInfo* ri)
{
  // Construct NV reply option from NV request option
  NCSymRepHeader* rhdr = new NCSymRepHeader();
  rhdr->src = hdr->src;
  rhdr->dst = hdr->dst;
  rhdr->from_inf = mac;
  rhdr->pidOrg = node->GetIPAddr();
  rhdr->pidQnum = hdr->id;
  rhdr->pidPnum = ri ? ri->pid.dn : nvPnum;
  rhdr->nvRoute = hdr->rnv;
  if (!ri) {
    rhdr->mid = hdr->mid;
    rhdr->mv = hdr->mv;
  }
  else {
    rhdr->mid = METRIC_HOP;
    rhdr->mv.hop = hdr->mv.hop + ri->mv.hop;
    rhdr->mv.pks = hdr->mv.pks/hdr->mv.hop;
    //rhdr->mv.pks =
    //(Long_t)(hdr->mv.pks + ri->mv.pks*(ri->mv.hop-1))/(rhdr->mv.hop-1);
    rhdr->cmv = ri->mv;
    rhdr->nv = ri->nv;
  }
  return rhdr;
}

void RoutingNVR::Broadcast(Interface* i, Packet* p)
{
  // Assumes all broadcast is control packet
  MACAddr dst;
  dst.SetBroadcast();
  i->Send(p, dst, IPV4::Instance()->Proto());
}

// Unicast for wireless network
void RoutingNVR::Unicast(Interface* i, Packet* p, MACAddr& dst_mac, bool ctrl)
{
  /* dst_mac was obtained from Lookup()
     Invalid dst_mac means that the corresponding neighbor was invalidated
     or incorrect
     Thus need to process it in the same way as in case of L2 xmit failure
  */
  if (dst_mac.macAddr == MACAddr::NONE) {
    DEBUGX(cout << MYIP << ": unicast failed at " << Simulator::Now() << endl);
    DoL2Fail(i, p);
    return;
  }

  // Link layer unicast
  DEBUG0(cout << MYIP << ": unicast to mac " << dst_mac.macAddr
         << " uid " << p->uid << " time " << Simulator::Now() << endl);

  i->Send(p, dst_mac, IPV4::Instance()->Proto());
}

void RoutingNVR::ScheduleTimer(Event_t ev, NVREvent*& tev, Time_t when)
{
  if (when <= 0) {
    cout << "HuH? schedule negative time " << when << endl;
    return;
  }

  Time_t now = Simulator::Now();
  if (tev) {
    // Check if this event already scheduled at same time, ignore if so
    // Already scheduled for correct time
    if (tev->Time() == now+when) return;
    CancelTimer(tev);
  }
  if (!tev) {
    tev = new NVREvent(ev);
  }

  DEBUG0(cout << "TimerTest " << this << " scheduling new timer "
         << tev->event << " time " << now
         << " for " << when << " secs in future" << endl);

  timer.Schedule(tev, when, this);
}

void RoutingNVR::CancelTimer(NVREvent*& tev, bool delTimer)
{
  if (!tev) return;
  timer.Cancel(tev);
  if (delTimer) {
    delete tev;
    tev = 0;
  }
}

void RoutingNVR::Timeout(TimerEvent* ev)
{
  NVREvent* tev = (NVREvent*)ev;
  switch (tev->event) {
    case NVREvent::NV_REQ: {
      // NV request timed out
      NVReqItem item = nvreqList[tev->h];
      DEBUGX(cout << MYIP << ": NVREQ timeout id " << item.id
             << " ttl " << item.ttl << " target " << (string)IPAddr(item.dst)
             << " time " << Simulator::Now() << endl);
      nvreqList[tev->h].timer = nil;    // free timer
      delete tev;
      // Expanding ring search
      if (enableRingSearch) {
        if (item.ttl >= TTL_MAX)
          if (++item.cnt > defaultNVReqRetry) {
            SendBufferClear(item.dst);
            if (enableBlackList) { blackList.Add(item.dst); break; }
            //break;
          }

        // from aodv
        Byte_t ttl = item.ttl >= TTL_MAX ? TTL_INIT : item.ttl + TTL_INC;
        if (ttl > TTL_THRESH) ttl = TTL_MAX;
        Time_t t1 = defaultRTT0;
        if (item.cnt > defaultNVReqRetry) {
          ttl = TTL_MAX;  // test
          Byte_t x = item.cnt - defaultNVReqRetry;
          if (x > 2) x = 2;
          t1 = defaultRTT0 * (1 << (x+1));
          //t1 = defaultRTT0 * 4;
        }
        Time_t to = ttl < TTL_MAX ? t1*(ttl+2) : t1*ttl;
        if (to > 10.24) to = 10.24;

        /*
        // from dsr
        Byte_t ttl = item.ttl >= TTL_MAX ? TTL_INIT : item.ttl + item.ttl;
        if (ttl > TTL_MAX) ttl = TTL_MAX;
        if (item.cnt > defaultNVReqRetry) {
          ttl = TTL_MAX;  // test
        }
        Time_t to_old = Simulator::Now() - item.time;
        Time_t to = item.ttl == TTL_INIT ? TO_INIT : to_old + to_old;
        if (to > 10) to = 10;
        */

        SendNVRequest(node, item.dst, ttl, to, item.cnt);
      }
      break;
    }

    case NVREvent::NV_REQ_DELAY: {
      // Send out delayed NV request
      Packet* p = nvreqQue[tev->h].p;
      nvreqQue[tev->h].timer = nil;    // free timer
      delete tev;
      Broadcast(node->Interfaces()[0], p);
      break;
    }

    case NVREvent::NV_SENDBUF:
      delete tev;
      evSendBuf = nil;
      // Send out the oldest packet in the SendBuffer
      if (!sendBuffer.empty()) ProcessPendingPeriod();
      break;

    case NVREvent::NV_SENDBUF_TO:
      SendBufferCheck();
      if (!sendBuffer.empty())
        ScheduleTimer(NVREvent::NV_SENDBUF_TO, evSendBufTimeout, 1);
      else {
        delete tev;
        evSendBufTimeout = nil;
      }
      break;

    default:
      DEBUG0(cout << "Unknown NVREvent type " << tev->event << endl);
      break;
  } //switch
}

bool RoutingNVR::ConstructNV(CNixVector& dnv, CNixVector& snv, MACAddr& mac)
{
  // dnv = nix_from_mac + snv
  int nix = NbrTable.Find(mac);
  if (nix < 0) nix = NbrTable.Add(mac);
  else NbrTable.Nbr[nix].val = true;    // validate the neighbor
  if (nix < 0) {
    DEBUG0(cout << "ConstructNV(): fail to NbrTable::Add()\n");
    return false;
  }
  dnv.Set(nix);

  DEBUG0(cout << "# of nbrs " << NbrTable.Nbr.size() << endl);
  DEBUG0(cout << "nix " << nix << " dnv ");
  DEBUG0(dnv.Dump());
  DEBUG0(cout << "from " << mac
         << " nbr " << NbrTable.Nbr[nix].mac << endl);

  //if (!(mac.macAddr == NbrTable.Nbr[nix].mac))
  //cout << "GOGO\n";

  dnv.ExpandBits(snv.nvl);
  bitcpy(dnv.nv, dnv.nvl, snv.nv, 0, snv.nvl);
  dnv.nvl += snv.nvl;
  return true;
}

void RoutingNVR::SendNVRequest(Node* n, IPAddr dst, Byte_t ttl, Time_t to,
                               Byte_t cnt)
{
  // NVREQ packet
  Packet* p = new Packet();

  NCSymReqHeader* hdr = new NCSymReqHeader();
  hdr->id = GetNCReqID();
  hdr->src = n->GetIPAddr();
  hdr->dst = dst;
  hdr->mid = METRIC_HOP;
  hdr->from_inf = n->Interfaces()[0]->GetMACAddr();
  if (enablePnumFilter) {
    int i = NVFIB.Find(dst, true);  // forced search
    hdr->pidPnum = i >= 0 ? NVFIB.Item[i]->pid.dn : 0;
  }

  AddNVReq(dst, hdr->id, ttl, cnt, to);
  NCList.Add(hdr->id, hdr->src, hdr->dst);

  // Broadcast
  L3Transmit(p, IPAddrBroadcast, ttl, Proto(), hdr);

  DEBUG0(cout << MYIP << ": sent NVREQ uid " << p->uid
         << " time " << Simulator::Now() << endl
         << "  target " << (string)hdr->dst << " ncid " << hdr->id
         << " nvreq size " << p->Size() << endl);
}

void RoutingNVR::PrintRoute(CNixVector& nv)
{
  cout << MYIP << ": route\n";
  CNixVector nvec = nv;
  vector<Long_t> routes;  // for loop check

  RoutingNVR* nvr = this;
  while (nvec.nvu < nvec.nvl) {
    MACAddr next = nvr->Lookup(nvec, false, true);
    if (next.macAddr == MACAddr::NONE) break;
    NodeVec_t::size_type i = next.macAddr-1;
    nvr = (RoutingNVR*)Node::nodes[i]->GetRouting();
    cout << " -> " << next.macAddr;
    routes.push_back(next.macAddr);
  }
  cout << endl;

  // Check if loop found
  for (vector<Long_t>::size_type i=0; i<routes.size()-1; ++i)
    for (vector<Long_t>::size_type j=i+1; j<routes.size(); ++j)
      if (routes[i] == routes[j]) {
        cout << "LOOP!\n";
        return;
      }
}

void RoutingNVR::AddNCDelay(Packet* p, Time_t delay)
{
  // Find available slot in nvreqQue
  NCQue_t::size_type i;
  for (i = 0; i < nvreqQue.size(); ++i)
    if (!nvreqQue[i].timer) break;
  if (i == nvreqQue.size()) // no available slot
    nvreqQue.push_back(CNCQueItem());  // add one

  // Store NVREQ
  nvreqQue[i].p = p;
  nvreqQue[i].timer = new NVREvent(NVREvent::NV_REQ_DELAY, i);

  // Schedule timer
  ScheduleTimer(NVREvent::NV_REQ_DELAY, nvreqQue[i].timer, delay);
}

bool RoutingNVR::AddNVReq(IPAddr_t dst, Byte_t id, Byte_t ttl, Byte_t cnt,
                          Time_t to)
{
  // Search for available slot in NV request list
  NVReqList_t::size_type i;
  for (i = 0; i < nvreqList.size(); ++i)
    if (!nvreqList[i].timer) break;
  if (i == nvreqList.size()) {  // no available one
    if (i < defaultNVReqListSize)  // expand
      nvreqList.push_back(NVReqItem());
    else {
      // NVReqList full
      DEBUGX(cout << "HuH? NVReqList full\n");
      return false;
    }
  }

  // Store item
  nvreqList[i] = NVReqItem(dst, id, ttl, cnt, Simulator::Now(), nil);
  nvreqList[i].timer = new NVREvent(NVREvent::NV_REQ, i);

  // Schedule NV request timeout
  ScheduleTimer(NVREvent::NV_REQ, nvreqList[i].timer, to);

  return true;
}

int RoutingNVR::FindNVReq(IPAddr_t dst)
{
  // Search NV request list for dst
  NVReqList_t::size_type i;
  for (i = 0; i < nvreqList.size(); ++i) {
    if (!nvreqList[i].timer) continue;
    if (nvreqList[i].dst == dst)
      return i;
  }
  return -1;
}

void RoutingNVR::SendBufferEnque(Packet* p, IPAddr_t dst)
{
  if (sendBuffer.size() >= defaultSendBufSize) {
    DEBUG0(cout << MYIP << ": send buffer full oldest item dropped\n");
    SendBufItem e = sendBuffer.front();
    sendBuffer.pop_front();
    delete e.p;  // L3 drop
  }

  // Store packet
  sendBuffer.push_back(SendBufItem(p, dst, Simulator::Now()));
  if (!evSendBufTimeout)
    ScheduleTimer(NVREvent::NV_SENDBUF_TO, evSendBufTimeout, 1);
}

SendBuf_t::iterator RoutingNVR::SendBufferFind(IPAddr_t dst)
{
  SendBuf_t::iterator i = sendBuffer.begin();
  for (; i != sendBuffer.end(); ++i)
    if (dst == i->dst) return i;
  return i;
}

void RoutingNVR::SendBufferCheck()
{
  Time_t now = Simulator::Now();
  for (SendBuf_t::iterator i=sendBuffer.begin(); i!=sendBuffer.end(); )
    {
      if (now - i->t > defaultSendBufTimeout) {  // timed out
        delete i->p;  // L3 drop
        sendBuffer.erase(i++);
        continue;
      }
      ++i;
    }
}

void RoutingNVR::SendBufferClear(IPAddr_t dst)
{
  for (SendBuf_t::iterator i=sendBuffer.begin(); i!=sendBuffer.end(); )
    {
      if (dst == i->dst) {
        delete i->p;  // L3 drop
        sendBuffer.erase(i++);
        continue;
      }
      ++i;
    }
}

// called when nvrep arrives
void RoutingNVR::ProcessPending(Node* n, IPAddr_t dst)
{
  SendBuf_t::iterator i = sendBuffer.begin();
  while (i != sendBuffer.end()) {
    if (dst == i->dst) {
      DEBUG0(cout << "  pending packet sent uid " << i->p->uid
             << " time " << Simulator::Now() << endl);
      DataSend(n, i->p);
      sendBuffer.erase(i++);
      continue;
    }
    ++i;
  }
}

// called when nvrep arrives
void RoutingNVR::ProcessPendingPeriod(Node* n, IPAddr_t dst)
{
  SendBuf_t::iterator i = SendBufferFind(dst);
  if (i == sendBuffer.end()) return;  // not found

  DEBUG0(cout << "  pending packet sent uid " << i->p->uid
         << " at " << Simulator::Now() << endl);
  DataSend(n, i->p);
  sendBuffer.erase(i);
  if (!evSendBuf && !sendBuffer.empty())
    ScheduleTimer(NVREvent::NV_SENDBUF, evSendBuf, defaultSendBufPeriod);
}

// called when timeout occurs
void RoutingNVR::ProcessPendingPeriod()
{
  vector<IPAddr_t> dVec;
  bool needSchedule = false;

  SendBuf_t::iterator i = sendBuffer.begin();
  while (i != sendBuffer.end()) {
    vector<IPAddr_t>::iterator p = find(dVec.begin(), dVec.end(), i->dst);
    if (p != dVec.end()) { ++i; continue; }  // already processed
    dVec.push_back(i->dst);
    CRouteInfo* ri = GetRouteInfo(i->dst);
    if (ri) {
      DEBUG0(cout << "  sent pending packet uid " << i->p->uid
             << " time " << Simulator::Now() << endl);
      DataSend(node, i->p);
      sendBuffer.erase(i++);
      needSchedule = true;
    }
    else {
      if (FindNVReq(i->dst) < 0) {
        if (enableRingSearch)
          SendNVRequest(node, i->dst, TTL_INIT, TO_INIT);
        else
          SendNVRequest(node, i->dst, TTL_MAX, 10.0);
      }
      ++i;
    }
  }
  if (needSchedule)
    ScheduleTimer(NVREvent::NV_SENDBUF, evSendBuf, defaultSendBufPeriod);
}

void RoutingNVR::UpdateNVTime(const PathId& pid)
{
  Time_t now = Simulator::Now();
  CRouteInfo* ri = GetRouteInfo(pid, true);  // forced
  if (ri) {
    ri->val = true;
    ri->time = now;
    timer.Cancel(&ri->ev);
    timer.Schedule(&ri->ev, CNVFIB::defaultTimeout1, &NVFIB);
  }
  ri = GetRouteInfo(PathId(pid.org,pid.dst,pid.sn,pid.dn), true);  // forced
  if (ri) {
    ri->val = true;
    ri->time = now;
    timer.Cancel(&ri->ev);
    timer.Schedule(&ri->ev, CNVFIB::defaultTimeout1, &NVFIB);
  }
}

IPV4Header*
RoutingNVR::AddIPHeader(Packet* p, IPAddr_t dst, Count_t ttl, Proto_t l4proto)
{
  IPV4Header* iphdr = new IPV4Header();
  iphdr->src = node->GetIPAddr();
  iphdr->dst = dst;
  iphdr->ttl = (Byte_t)ttl;
  iphdr->protocol = (Byte_t)l4proto;
  p->PushPDU(iphdr);
  return iphdr;
}

void RoutingNVR::Notify(void* v)
{
  // Handle transmit failure notification from L2
  DEBUG0(cout << MYIP << ": detected L2 xmit failure\n");
  Packet* p = (Packet*)v;
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
  if (IPAddrBroadcast == iphdr->dst) {
    // Don't process broadcast packet
    delete p;  // L2 drop
    return;
  }

  // Clear pkts over broken link from ifq
  vector<IPAddr_t> sVec;
  vector<Packet*> pVec;
  IPV4Header* iph = (IPV4Header*)p->PeekPDU();
  sVec.push_back(iph->src);
  pVec.push_back(p);
  // identify dst mac addr
  DATA_frame *df = (DATA_frame*)p->PeekPDU(-2);
  MACAddr dst = df->addr1;

  // get the pointer to the ifq
  Interface* inf = node->Interfaces()[0];
  Queue* ifq = inf->GetQueue();

  // search the ifq for dst
  while ((p = ifq->DequeOneDstMac(dst)))
    {
      iph = (IPV4Header*)p->PeekPDU(2);
      vector<IPAddr_t>::iterator j = find(sVec.begin(), sVec.end(), iph->src);
      if (j == sVec.end()) {  // not found
        sVec.push_back(iph->src);
        p->PopPDU();  // remove L2 header
        p->PopPDU();  // remove LLCSNAP header
        pVec.push_back(p);
      }
      else {
        delete p;  // drop
        Stats::pktsDropped++;
      }
    }

  // Trigger the NVR mobility management function
  //DoL2Fail(inf, p);
  for (vector<Packet*>::size_type k = 0; k < pVec.size(); ++k)
    DoL2Fail(inf, pVec[k]);
  return;
}
