// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: gb_dijk.c 92 2004-09-14 18:18:43Z dheeraj $



/*2:*/
#line 57 "gb_dijk.w"

#include "gb_graph.h" 
#define dist z.I \

#define backlink y.V \

#define hh_val x.I \

#define print_dijkstra_result p_dijkstra_result \

#define llink v.V
#define rlink w.V \


#line 59 "gb_dijk.w"

/*16:*/
#line 308 "gb_dijk.w"

static Vertex head[128];

void init_dlist(d)
long d;
{
head->llink= head->rlink= head;
head->dist= d-1;
}

/*:16*//*17:*/
#line 327 "gb_dijk.w"

void enlist(v,d)
Vertex*v;
long d;
{register Vertex*t= head->llink;
v->dist= d;
while(d<t->dist)t= t->llink;
v->llink= t;
(v->rlink= t->rlink)->llink= v;
t->rlink= v;
}

/*:17*//*18:*/
#line 339 "gb_dijk.w"

void reenlist(v,d)
Vertex*v;
long d;
{register Vertex*t= v->llink;
(t->rlink= v->rlink)->llink= v->llink;
v->dist= d;
while(d<t->dist)t= t->llink;
v->llink= t;
(v->rlink= t->rlink)->llink= v;
t->rlink= v;
}

/*:18*//*19:*/
#line 352 "gb_dijk.w"

Vertex*del_first()
{Vertex*t;
t= head->rlink;
if(t==head)return NULL;
(head->rlink= t->rlink)->llink= head;
return t;
}

/*:19*//*21:*/
#line 371 "gb_dijk.w"

static long master_key;

void init_128(d)
long d;
{register Vertex*u;
master_key= d;
for(u= head;u<head+128;u++)
u->llink= u->rlink= u;
}

/*:21*//*22:*/
#line 385 "gb_dijk.w"

Vertex*del_128()
{long d;
register Vertex*u,*t;
for(d= master_key;d<master_key+128;d++){
u= head+(d&0x7f);
t= u->rlink;
if(t!=u){
master_key= d;
(u->rlink= t->rlink)->llink= u;
return t;
}
}
return NULL;
}

/*:22*//*23:*/
#line 401 "gb_dijk.w"

void enq_128(v,d)
Vertex*v;
long d;
{register Vertex*u= head+(d&0x7f);
v->dist= d;
(v->llink= u->llink)->rlink= v;
v->rlink= u;
u->llink= v;
}

/*:23*//*24:*/
#line 424 "gb_dijk.w"

void req_128(v,d)
Vertex*v;
long d;
{register Vertex*u= head+(d&0x7f);
(v->llink->rlink= v->rlink)->llink= v->llink;
v->dist= d;
(v->llink= u->llink)->rlink= v;
v->rlink= u;
u->llink= v;
if(d<master_key)master_key= d;
}

/*:24*/
#line 60 "gb_dijk.w"

/*8:*/
#line 161 "gb_dijk.w"

static long dummy(v)
Vertex*v;
{return 0;}

/*:8*//*15:*/
#line 294 "gb_dijk.w"

void(*init_queue)()= init_dlist;
void(*enqueue)()= enlist;
void(*requeue)()= reenlist;

Vertex*(*del_min)()= del_first;

/*:15*/
#line 61 "gb_dijk.w"

/*9:*/
#line 168 "gb_dijk.w"

long dijkstra(uu,vv,gg,hh)
Vertex*uu;
Vertex*vv;
Graph*gg;
long(*hh)();
{register Vertex*t;
if(!hh)hh= dummy;
/*10:*/
#line 194 "gb_dijk.w"

for(t= gg->vertices+gg->n-1;t>=gg->vertices;t--)t->backlink= NULL;
uu->backlink= uu;
uu->dist= 0;
uu->hh_val= (*hh)(uu);
(*init_queue)(0L);

/*:10*/
#line 176 "gb_dijk.w"
;
t= uu;
if(verbose)/*12:*/
#line 231 "gb_dijk.w"

{printf("Distances from %s",uu->name);
if(hh!=dummy)printf(" [%ld]",uu->hh_val);
printf(":\n");
}

/*:12*/
#line 178 "gb_dijk.w"
;
while(t!=vv){
/*11:*/
#line 203 "gb_dijk.w"

{register Arc*a;
register long d= t->dist-t->hh_val;
for(a= t->arcs;a;a= a->next){
register Vertex*v= a->tip;
if(v->backlink){
register long dd= d+a->len+v->hh_val;
if(dd<v->dist){
v->backlink= t;
(*requeue)(v,dd);
}
}else{
v->hh_val= (*hh)(v);
v->backlink= t;
(*enqueue)(v,d+a->len+v->hh_val);
}
}
}

/*:11*/
#line 181 "gb_dijk.w"
;
t= (*del_min)();
if(t==NULL)
return-1;

if(verbose)/*13:*/
#line 237 "gb_dijk.w"

{printf(" %ld to %s",t->dist-t->hh_val+uu->hh_val,t->name);
if(hh!=dummy)printf(" [%ld]",t->hh_val);
printf(" via %s\n",t->backlink->name);
}

/*:13*/
#line 186 "gb_dijk.w"
;
}
return vv->dist-vv->hh_val+uu->hh_val;
}

/*:9*/
#line 62 "gb_dijk.w"

/*14:*/
#line 256 "gb_dijk.w"

void print_dijkstra_result(vv)
Vertex*vv;
{register Vertex*t,*p,*q;
t= NULL,p= vv;
if(!p->backlink){
printf("Sorry, %s is unreachable.\n",p->name);
return;
}
do{
q= p->backlink;
p->backlink= t;
t= p;
p= q;
}while(t!=p);
do{
printf("%10ld %s\n",t->dist-t->hh_val+p->hh_val,t->name);
t= t->backlink;
}while(t);
t= p;
do{
q= t->backlink;
t->backlink= p;
p= t;
t= q;
}while(p!=vv);
}

/*:14*/
#line 63 "gb_dijk.w"


/*:2*/
