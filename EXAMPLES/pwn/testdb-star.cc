// Test animation of dumbbell with stars at leaf nodes.
// George F. Riley, Georgia Tech, Fall 2002

#include <iostream>

#include "debug.h"
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "dumbbell.h"
#include "star.h"
#include "queue.h"
#include "tcp-tahoe.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "l2proto802.3.h"
#include "validation.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

#define N_LEAF 5

using namespace std;

// Demonstrate the trace filtering callback
bool TraceCB(Node* n, Protocol* p, PDU* h, bool b)
{
  DEBUG0((cout << "hello from trace cb" << endl));
  if (!p) return false;      // No protocol, no trace
    Layer_t l = p->Layer();
  if (l != 4)  return false; // Not layer 4
  Proto_t pr = h->Proto();
  if (pr != 6) return false; // Not tcp
  TCPHeader* th = (TCPHeader*)h;
  if (th->flags & (TCP::SYN | TCP::FIN | TCP::RST)) return true;
  return false; // Only trace tcp pkts with SYN/FIN/RST
}

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  Count_t nl = N_LEAF; // Number leaf nodes for dumbbell
  Count_t ns = N_LEAF; // Number leaf nodes for each star
  Rate_t  bw = Rate("10Mb");
  
  if (argc > 1) nl = atol(argv[1]);
  if (argc > 2) ns = atol(argv[2]);
  if (argc > 3) bw = Rate(argv[3]);
  
  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);

  // Request a callback on every trace decision
  Trace::AddCallback(TraceCB);

  // Use delayed ack
  //TCP::DefaultDelAckTimeout(Time("200ms"));
  // Use 2 pkts for default initial cwnd
  //TCP::DefaultInitialCWnd(2);
  
  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->Open("testdb-star.txt");
  IPV4::Instance()->SetTrace(Trace::ENABLED);
  L2Proto802_3::GlobalSetTrace(Trace::ENABLED);
  Queue::DefaultLimitPkts(20);
  
  TCP::LogFlagsText(true);       // Log TCP flags in text mode
  Linkp2p lk(bw, Time("1ms"));
  Dumbbell b(nl, nl, 1.0,
             IPAddr("192.168.0.1"), IPAddr("192.169.0.1"), lk);
  // Specify the bounding box
  b.BoundingBox(Location(0,0), Location(10,10));
  
  Uniform startRng(0, 0.1); // Random number generator for start times
  // Create the Star networks and assign clients/servers
  Angle_t adderL = -M_PI / (b.LeftCount() + 1.0);
  Angle_t adderR = M_PI / (b.RightCount() + 1.0);
  Angle_t thetaL = -M_PI_2 + adderL;
  Angle_t thetaR = -M_PI_2 + adderR;

  // Animate the queues
  b.RightQueue()->Animate(true);
  b.LeftQueue()->Animate(true);
  
#ifdef HAVE_QT
  b.Left()->Color(Qt::blue);
  b.Right()->Color(Qt::blue);
#endif
  for (Count_t i = 0; i < b.LeftCount(); ++i)
    {
      Node* l = b.Left(i);
      Node* r = b.Right(i);
#ifdef HAVE_QT
      l->Color(Qt::blue);
      r->Color(Qt::blue);
#endif
      // Create a star networks with this each leaf as hub
      Star* sl = new Star(ns,l,lk, IPAddr("192.168.1.1")+IPAddr("0.0.1.0")*i);
      Star* sr = new Star(ns,r,lk, IPAddr("192.169.1.1")+IPAddr("0.0.1.0")*i);
      
      sl->BoundingBox(Location(0,0), Location(2,2), thetaL, M_PI / 2);
      sr->BoundingBox(Location(0,0), Location(2,2), thetaR, M_PI / 2);

      thetaL += adderL;
      thetaR += adderR;
      
      // Add a tcp server at each left side node and client at right side
      for (Count_t j = 0; j < sl->LeafCount(); ++j)
        {
          Node* ll = sl->GetLeaf(j); // Left leaf
          TCPServer* tcpApp =
            (TCPServer*)ll->AddApplication(TCPServer());
          tcpApp->BindAndListen(ll, 80);
          tcpApp->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
#ifdef HAVE_QT
          ll->Color(Qt::yellow);
#endif          
          Node* rl = sr->GetLeaf(j); // Right leaf
          TCPSend* ts = (TCPSend*)rl->AddApplication(
              TCPSend(ll->GetIPAddr(),80,
                      Constant(1000000)));
#ifdef HAVE_QT
          rl->Color(Qt::green);
          if (j == 0 && i == 0)
            { // Debug..set packets to red for one flow only
              ts->GetL4()->SetColor(Qt::red);
            }
#endif
          ts->SetTrace(Trace::ENABLED);
          ts->Start(startRng.Value());
        }
    }

  // Specify animation
  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  
  s.Progress(1);
  s.StopAt(100);
  s.Run();
  tr->Close();
}
