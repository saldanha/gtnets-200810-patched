
#ifndef _ipv6addr_h_
#define _ipv6addr_h_

#include <sys/types.h>

#include "addr.h"

#define IN6ADDRSZ 16

#ifdef WIN32

#if !defined u_int8_t
typedef unsigned char u_int8_t ;
#endif

#if !defined u_int16_t
typedef unsigned short u_int16_t;
#endif

#if !defined u_int32_t
typedef unsigned int u_int32_t;
#endif

#endif



class IPV6Addr : public Addr {
public:
        IPV6Addr();
        IPV6Addr(const char* addr_);
        IPV6Addr(const IPV6Addr& addr);
        virtual bool operator==(const Addr&) const;
        ~IPV6Addr();
public:
        union {
                u_int8_t  _var8[16];
                u_int16_t _var16[8];
                u_int32_t _var32[4];
        } addr;
        u_int16_t mask;
#define addrb addr._var8
#define addrs addr._var16
#define addrl addr._var32
};


std::ostream& operator <<(std::ostream&, const IPV6Addr&);

#endif
