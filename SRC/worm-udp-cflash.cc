// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//



// Georgia Tech Network Simulator - UDP Compact flash Worm class

// Mohamed Abdelhafez,  Georgia Tech, Spring 2005

// Define a Compact flash Worm Application class

// Uncomment below to enable debug level 0

//#define DEBUG_MASK 0x1


#include "worm-udp-cflash.h"
#include "math.h"
#include <cstring>

#ifdef HAVE_QT
#include <qnamespace.h>
#endif

#define FOUR_BYTE_OFFSET  0
#define TWO_BYTE_OFFSET  -1
#define END              -2


using namespace std;

//char WormUDPCFlash::endchar = END;
bool WormUDPCFlash::doubleinfect = false;

// Constructors
WormUDPCFlash::WormUDPCFlash():WormUDPFlash(), nextIP(0), secondIP(0),skip(0)
{
}


WormUDPCFlash::~WormUDPCFlash()
{
}


WormUDPCFlash::WormUDPCFlash(const WormUDPCFlash& w): nextIP(w.nextIP), secondIP(w.secondIP),skip(w.skip)
{
}


Application* WormUDPCFlash::Copy() const
{
  return new WormUDPCFlash(*this);
}


void WormUDPCFlash::SendWormToChildren(char* mlist)
{
  lastIP = nextIP = node->GetIPAddr();

  //  cout << "lastIP = " <<  IPAddr::ToDotted(lastIP) << " nextIP = " << IPAddr::ToDotted(nextIP) << endl;
  Count_t packetsize = childlistsize + COUNTSIZE + 1;
  char* d = new char[packetsize + payloadSize];
  char* temp = new char[packetsize];

  //  Count_t limit = numofchildren;
  //if (numofchildren > vulhostssize ) limit = vulhostssize;

  for(int i = 0; i < numofchildren; ++i)
    {
      // cout << "i = " << i << " nchildren = " << numofchildren << endl;
      int childsize = SplitList(mlist, i, temp);
      packetsize = childsize + COUNTSIZE + 1;

      memcpy(d + payloadSize, &childsize, COUNTSIZE);      
      memcpy(d + payloadSize + COUNTSIZE, temp, childsize + 1);

      udp->SendTo(d,packetsize + payloadSize, nextIP, infectionport);

      if(doubleinfect)
	{
	  if(secondIP)
	    {
	      int newsize = childsize - skip;
	      memcpy(d + payloadSize, &newsize, COUNTSIZE);

	      if(newsize > 0)
		memcpy(d + payloadSize + COUNTSIZE, temp + skip, newsize + 1 );

	      // send the same list to the next child in the list
	      udp->SendTo(d, packetsize + payloadSize - skip, secondIP, infectionport);
	    }

	}

      if (childsize < childlistsize || vulhosts[index] == END)
      	{
	  // zero child list size
	  break;
      	}
      nextIP = lastIP;

    
    }
      //testing
  
  //  delete d;
  //delete temp;
}


int WormUDPCFlash::SplitList(char* main, int i, char* child)
{
  int j; // loop counter 
  short increment = main[index];

  
  if(increment == TWO_BYTE_OFFSET)
    {
      index++;
      memcpy(&increment, main + index, WORDSIZE);
      nextIP += increment;
      index += WORDSIZE;
    }
  else if(increment == FOUR_BYTE_OFFSET)
    {
      index++;
      memcpy(&nextIP, main + index, IPSIZE);
      index += IPSIZE;
    }
  else
    {
      nextIP += increment;
      index++;
    }
  lastIP = nextIP;

  for(j = 0; (j < childlistsize) && ((j + index) < vulhostssize); ++j)
    {
      increment = main[index + j];
      
      if(increment == TWO_BYTE_OFFSET)
	{
	  memcpy(&increment, main + index + j + 1, WORDSIZE);
	  // increment = main[index + j + 1] + main[index + j + 2] * 256;
	  //cout << "index = " << index << ", j = " << j << endl;
	  //cout << "main[index + j + 1] = " << main[index + j + 1] <<  " main[index + j + 2] = " << main[index + j + 2]<< endl;
	  lastIP += increment;

	  if(doubleinfect)
	    {
	      if(j == 0)
		{
		  secondIP = lastIP;
		  skip = WORDSIZE + 1;
		}
	    }
	  j += WORDSIZE;
	  
	}
      else if(increment == FOUR_BYTE_OFFSET)
	{
	  memcpy(&lastIP, main + index + j + 1, IPSIZE);
	  //	  increment = main[index + j + 1] + main[index + j + 2] * 256 + ;
	  //	  lastIP = main[index + j + 1] + main[index + j + 2] * 256 + main[index + j + 3]*65536 + main[index + j + 4] * 16777216;
	  if(doubleinfect)
	    {
	      if(j == 0)
		{
		  secondIP = lastIP;
		  skip = IPSIZE + 1;
		}
	    }
	  j += IPSIZE;
	}
      else
	{
	  lastIP += increment;

	  if(doubleinfect)
	    {
	      if(j == 0)
		{
		  secondIP = lastIP;
		  skip = 1;
		}
	    }
	}
    }
  // j now holds the address list size
  //  memcpy(child, &j, COUNTSIZE);
 
  // skip my entry here
  memcpy(child, main + index , j );
  child[j] = END;
  //  memcpy(child + COUNTSIZE +  j + payloadSize, &endchar ,1);
  index += j;

  return j;
}


void WormUDPCFlash::Receive(Packet *p, L4Protocol *proto, Seq_t)
{
  //  exit(0);

  DEBUG0((cout<<"Receive["<<IPAddr::ToDotted(node->GetIPAddr())<<"]: Received UDP packet of size "<<p->Size()<<endl));
   
   if (PacketIsWorm(p)) {
     if (vulnerable && !infected) {
       Data* d = (Data*)p->PopPDU();

       if(d)
	 {
	   if(index == 0)
	     {
	       memcpy(&vulhostssize, d->data + payloadSize, COUNTSIZE);
	    
	       if(vulhostssize > 0)
		 {
		   //  Count_t size = vulhostssize * IPSIZE; // allocate the maximum possible size

		   vulhosts = new char[vulhostssize + 1];
	       
		   memcpy(vulhosts, d->data + COUNTSIZE + payloadSize, d->Size() - COUNTSIZE - payloadSize);
		   index = d->Size() - COUNTSIZE - payloadSize;
		 }
	     }
	   else
	     {
	       memcpy(vulhosts + index, d->data , d->Size());
	       index += d->Size(); 
	       // cout << "Remaining size again = " << vulhostssize*IPSIZE - index << endl;
	     }
	   
	   // cout << "index = " << index << " Vluhostssize * word = " << vulhostssize * WORDSIZE << endl;
	   if(!vulhostssize || vulhosts[index-1] == END)
	     {
	       //  cout << "vulhosts[index - 1] = " << (int)vulhosts[index - 1] << endl;
	       
	       //  cout << "vulhosts[index] = " << (int)vulhosts[index] << endl;
	       //	   cout << "Packet complete Index = " << index << endl;
	       index = 0;
	       Infect();
	     }
	 }
     }
   }
   delete p;
}


void WormUDPCFlash::DoubleInfect(bool d)
{
  doubleinfect = d;
}
