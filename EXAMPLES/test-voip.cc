// Test Voice over IP, with animation of dumbbell.
// George F. Riley, Georgia Tech, Fall 2002

#include <iostream>

#include "debug.h"
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "dumbbell.h"
#include "star.h"
#include "tcp-tahoe.h"
#include "application-voip.h"
#include "application-onoff.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "l2proto802.3.h"
#include "validation.h"
#include "diffserv-queue.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

#define N_LEAF 5

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  if (argc < 5)
    {
      cout << "Usage:  test-voip in-voice-left.aiff in-voice-right.aiff "
           << "out-voice-left out-voice-right on-off-load noanim" << endl;
      exit(1);
    }
  
  Simulator s;
  Count_t nl = N_LEAF; // Number leaf nodes for dumbbell
  Rate_t  bw = Rate("10Mb");
  Rate_t  bbw = bw * 0.5; // Bottleneck bandwidth
   
  string inVoiceL = string(argv[1]);
  string inVoiceR = string(argv[2]);
  string outVoiceL = string(argv[3]);
  string outVoiceR = string(argv[4]);
  Mult_t onoffLoad = 1.0; // Fraction of bottleneck link bw consumed by oo
  bool   noAnim = false;
  bool   useQos = false;
  
  if (argc > 5)
    {
      onoffLoad = atof(argv[5]);
    }
  Mult_t loadPerSource = bbw * onoffLoad / (N_LEAF - 1);
  if (argc > 6) noAnim = true;
  if (argc > 7) useQos = true;
  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);
  Linkp2p lk(bw, Time("5ms"));
  Dumbbell b(nl, nl, 0.5,
             IPAddr("192.168.0.1"), IPAddr("192.169.0.1"), lk);
  // Specify the bounding box
  b.BoundingBox(Location(0,0), Location(10,10));
  Uniform delayRng(Time("5ms"), Time("10ms")); // Randomize delay a bit
  Uniform startRng(0, 0.1); // Random number generator for start times

  // Set up trace file
  // Open the trace file
  Trace* tf = Trace::Instance();
  tf->Open("test-voip.txt");
  
  // Use diffserv queues in each direction
  Queue* lq = b.LeftQueue();
  Interface* liface = lq->interface;
  // Set two priorities with equal sharing of the bottleneck link
  liface->SetQueue(DiffServQueue(2, bbw));
  
  // Get the diffserv queue pointer
  DiffServQueue* ldq = (DiffServQueue*)b.LeftQueue();
  ldq->Animate(true);
  ldq->SetLimitPkts(30);

  // Set 50% each for codepoints 0 and 1
  ldq->RateAllocation(0, bbw * 0.5);
  ldq->RateAllocation(1, bbw * 0.5);

  Queue* rq = b.RightQueue();
  Interface* riface = rq->interface;
  // Set two priorities with equal sharing of the bottleneck link
  riface->SetQueue(DiffServQueue(2, bbw));
  
  // Get the diffserv queue pointer
  DiffServQueue* rdq = (DiffServQueue*)b.RightQueue();
  rdq->Animate(true);
  rdq->SetLimitPkts(30);
  
  // Set 50% each for codepoints 0 and 1
  rdq->RateAllocation(0, bbw * 0.5);
  rdq->RateAllocation(1, bbw * 0.5);

#ifdef HAVE_QT
  b.Left()->Color(Qt::blue);
  b.Right()->Color(Qt::blue);
#endif
  for (Count_t i = 0; i < b.LeftCount(); ++i)
    {
      Node* l = b.Left(i);
      Node* r = b.Right(i);
#ifdef HAVE_QT
      l->Color(Qt::blue);
      r->Color(Qt::blue);
#endif
      // Add a VOIP app at each left and right side, node zero
      // OnOff apps at others
      if (i == 0)
        { // Add the VOIP App
          Random_t voipStart = startRng.Value();
          voipStart = 0.0; // ! Debug..start at zero
          
          VOIPApplication* voipl = 
            (VOIPApplication* )l->AddApplication(VOIPApplication(
                               r->GetIPAddr(), 10000, 
                               inVoiceL, 80));
          voipl->RecordingFile(outVoiceL);
          voipl->Start(voipStart);
          voipl->Stop(60.0); // Call is < 1 minute
          voipl->GetL4()->SetColor(Qt::red);
          cout << "VOIP-L, inVoice " << inVoiceL << " out voice " << outVoiceL
               << endl;
          // Different codepoint if using Qos
          if (useQos) voipl->GetL4()->TOS(1);
          //voipl->UseSilenceSupression(1000); // Enable silence supression
          //voipl->verbose = true;
          //voipl->verbose1 = true;
          
          VOIPApplication* voipr = 
            (VOIPApplication* )r->AddApplication(VOIPApplication(
                               l->GetIPAddr(), 10000, 
                               inVoiceR, 80));
          voipr->RecordingFile(outVoiceR);
          voipr->Start(voipStart);
          voipr->Stop(60.0); // Call is < 1 minute
          voipr->GetL4()->SetColor(Qt::red);
          cout << "VOIP-R, inVoice " << inVoiceR << " out voice " << outVoiceR
               << endl;
          // Different codepoint if using Qos
          if (useQos) voipr->GetL4()->TOS(1);
          //voipr->UseSilenceSupression(1000); // Enable silence supression
          //voipr->verbose = true;
          //voipr->verbose1 = true;
          // Enable tracing
          l->SetTrace(Trace::ENABLED);
          r->SetTrace(Trace::ENABLED);
        }
      else
        {
          OnOffApplication* ooal = 
            (OnOffApplication* )l->AddApplication(OnOffApplication(
                                r->GetIPAddr(), 10000, 
                                Exponential(Time("10ms")), // On period
                                Exponential(Time("10ms")), // Off
                                UDP(), loadPerSource * 2)); // *2 since 50% duty
          ooal->Start(startRng.Value());
          OnOffApplication* ooar = 
            (OnOffApplication* )r->AddApplication(OnOffApplication(
                                l->GetIPAddr(), 10000, 
                                Exponential(Time("10ms")), // On period
                                Exponential(Time("10ms")), // Off
                                UDP(), loadPerSource * 2)); // *2 since 50% duty
          ooar->Start(startRng.Value());
        }
      // Randomize the link delay
      Link* lk1 = b.LeftLink(i);
      lk1->Delay(Time("10ms") + delayRng.Value());
      // Randomize the link delay
      Link* lk2 = b.RightLink(i);
      lk2->Delay(Time("10ms") + delayRng.Value());
    }

  // Specify animation
  if (!Validation::noAnimation && !noAnim)
    {
      // Enable recorder
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  
  s.Progress(1);
  s.StopAt(60);
  s.Run();
  tf->Close();
}
