// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: simulator.h 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - Simulator class
// George F. Riley.  Georgia Tech Spring 2002

#ifndef __simulator_h__
#define __simulator_h__

#include <iostream>
#include <map>
#include <set>
#include <list>
#include <vector>
#include <string>
#include <math.h>
#include <fstream>

//#define	CTRLFIFO true

#include "common-defs.h"
#include "object.h"
#include "handler.h"
#include "event.h"
#include "timer.h"
#include "location.h"
#include "rng.h"

// For testing, include scheduler.h..remove later
#include "scheduler.h"

class Interface;
class Node;
class NotifyHandler;

class QTWindow;
class QCanvas;
// Define a callback for custom animation background
typedef void(*CustomBackground_t)(QCanvas*);

typedef void (*ProgressHook_t)(Time_t);
typedef std::vector<ProgressHook_t> PHVec_t;  // List of progress callbacks
typedef std::vector<std::string> StringVec_t; // Vector of strings


// Definen a callback for "Node Selected" (mouse click) on animation
typedef void (*NodeSelected_t)(Node*);

const double DegreesToMeters = 111319.5; // Degrees of latitude to meters
const double DegreesToRadians = (2.0 * M_PI) / 360.0;

typedef enum{
	      CIRCLES = 1,
              DIRECTED_ARR = 2,
            }ANIMATE_WIRELESS_TX_FORM;

class SimulatorEvent : public Event {
public:
  typedef enum { HALT, DELETE_OBJECT, NOTIFY, 
                 NODE_DOWN, NODE_UP,
				 BASEBAND_TX,
                 INTERFACE_DOWN, INTERFACE_UP } SimulatorEvent_t;
  SimulatorEvent() 
      : object(nil), c(nil), node(nil), iFace(nil) { }
  SimulatorEvent(Event_t ev) 
      : Event(ev), object(nil), c(nil), node(nil), iFace(nil) { }
  virtual ~SimulatorEvent() { }
public:
  Object*    object;
  void*      c;
  Node*      node;
  Interface* iFace;
};

class EventPair {
public:
  EventPair() : time(0.0), event(nil) { }
  EventPair(Time_t t, Event* e) : time(t), event(e) { }
public:
  Time_t time;
  Event* event;
};

#ifndef __scheduler_h__
class KeyPair {
public:
  KeyPair(Time_t t, SimulatorUid_t u) : time(t), uid(u) { }
public:
  Time_t         time;
  SimulatorUid_t uid;
};

// Definitions for event list managemet
typedef std::pair<Time_t,SimulatorUid_t> KeyPair_t; // The key for the map
//typedef std::pair<Time_t,Event*> EventPair_t;       // For non-sorted ev lists (obsolete)
class key_less
{
public:
  key_less() { }
  bool operator()(const KeyPair& l, const KeyPair& r) const {
    return (l.time < r.time ||
            l.time == r.time && l.uid < r.uid);
    //bool operator()(const KeyPair& l, const KeyPair& r) const {
    //if (l.time > r.time) return false;
    //if (l.time < r.time) return true;
    //return (l.uid < r.uid);
  }
};

class event_less
{
public:
  event_less() { }
  inline bool operator()(Event* const & l, const Event* const & r) const {
    return (l->Time() < r->Time() ||
            l->Time() == r->Time() && l->uid < r->uid);
  }
};

// Define a sorted event list (EventMap_t) and fifo list (EventDeq_t)
//typedef std::map<KeyPair, Event*, std::less<KeyPair> > EventMap_t;
#endif
typedef std::map<KeyPair, Event*, key_less> EventMap_t;
//typedef std::set<Event*, event_less>        EventSet_t;
typedef std::list<EventPair >               EventList_t;

class ProgressTimer : public Timer {
public:
  virtual void Timeout(TimerEvent*);  // Override the timeout method
};

class ProgressEvent : public TimerEvent {
public:
  ProgressEvent(Time_t i) 
    : TimerEvent(), interval(i) { }
public:
  Time_t   interval;
};

//Doc:ClassXRef
class Simulator : public Handler {
  //Doc:Class Class {\tt Simulator} defines the simulator object required for
  //Doc:Class all \GTNS\ simulations.  The simulator object controlls all
  //Doc:Class actions once the simulation has started executing.  Each
  //Doc:Class \GTNS\ simulation should declare a single {\tt Simulator} object,
  //Doc:Class usually as a local variable in the {\tt main} program.
public:
  friend class ProgressTimer; // Allow timer access to "ProgressResched"
  //Doc:Method
  Simulator();
    //Doc:Desc The constructor for the {\tt Simulator} object takes no
    //Doc:Desc arguments unless using the distibuted simulation feature of
    //Doc:Desc \GTNS.

  virtual ~Simulator();
public:   
  //Doc:Method
  virtual void Handle(Event*, Time_t);
    //Doc:Desc The Simulator class is a subclass of {\tt Handler}, 
    //Doc:Desc since the simulator object must handle events, specificaly
    //Doc:Desc the progress events and the halt event.
    //Doc:Arg1 A pointer to the event being handled.
    //Doc:Arg2 The current simulation time.

#ifdef MOVED_TO_SCHEDULER
  //Doc:Method
  virtual void Cancel(Event*);	     // cancel event
    //Doc:Desc Cancels a pending event.
    //Doc:Arg1 A pointer to the event to be canceled.  When scheduling events
    //Doc:Arg1 that will later be canceled, a pointer to the event must
    //Doc:Arg1 be retained to allow cancellation.

  //Doc:Method
  virtual void Schedule(Event*);     // schedule event
    //Doc:Desc Schedules a new event.  The simulation time for the
    //Doc:Desc event must be stored in the event class, member
    //Doc:Desc {\tt time}, by the caller prior to calling Schedule.  The
    //Doc:Desc handler for the event is caller, which must be a subclass
    //Doc:Desc of  {\tt Handler}
    //Doc:Arg1 A pointer to the event being scheduled.

  //Doc:Method
  virtual void Schedule(Event*, Time_t);  // schedule event
    //Doc:Desc Schedules a new event.  The simulation time for the
    //Doc:Desc event is specified in the argument list and will
    //Doc:Desc be stored in the event class, member {\tt time},
    //Doc:Desc by this method.  The
    //Doc:Desc handler for the event must be already specified
    //Doc:Desc in the passed {\tt Event}.
    //Doc:Arg1 A pointer to the event being scheduled.
    //Doc:Arg2 A amount of time {\em in the future} for this event.
    //Doc:Arg2 Note the time is a relative time from the current simulation
    //Doc:Arg2 time. The actual time for the event is computed by adding the
    //Doc:Arg2 current simulation time to the value specified.

  //Doc:Method
  virtual void Schedule(Event*, Time_t, Handler*);
    //Doc:Desc Schedules a new event.  The simulation time for the
    //Doc:Desc event is specified in the argument list and will
    //Doc:Desc be stored in the event class, member {\tt time},
    //Doc:Desc by this method.  The 
    //Doc:Desc handler for the event is also specified in the
    //Doc:Desc argument list.
    //Doc:Arg1 A pointer to the event being scheduled.
    //Doc:Arg2 A amount of time {\em in the future} for this event.
    //Doc:Arg2 Note the time is a relative time from the current simulation
    //Doc:Arg2 time. The actual time for the event is computed by adding the
    //Doc:Arg2 current simulation time to the value specified.
    //Doc:Arg3 A pointer to any object that is a subclass of class
    //Doc:Arg3 {\tt Handler}, specifying the event handler for this event.

  //Doc:Method
  virtual void Schedule(Event*, Time_t, Handler&);
    //Doc:Desc Schedules a new event.  The simulation time for the
    //Doc:Desc event is specified in the argument list and will
    //Doc:Desc be stored in the event class, member {\tt time},
    //Doc:Desc by this method.  The 
    //Doc:Desc handler for the event is also specified in the
    //Doc:Desc argument list.
    //Doc:Arg1 A pointer to the event being scheduled.
    //Doc:Arg2 A amount of time {\em in the future} for this event.
    //Doc:Arg2 Note the time is a relative time from the current simulation
    //Doc:Arg2 time. The actual time for the event is computed by adding the
    //Doc:Arg2 current simulation time to the value specified.
    //Doc:Arg3 A reference to any object that is a subclass of class
    //Doc:Arg3 {\tt Handler}, specifying the event handler for this event.

  // Schedule an "early" event (use STL hint at beginning of queue)
  // Not documented in manual at this point.
  virtual void ScheduleEarly(Event*, Time_t, Handler*);
#endif

  //Doc:Method
  Scheduler* SetScheduler(const Scheduler&);
    //Doc:Desc Specify which even scheduler is desired
    //Doc:Arg1 Temporary object of desired scheduler class.
    //Doc:Arg1 This is copied and set as the default scheduler.

  //Doc:Method
  virtual void Progress(Time_t); //Print debug progress message
    //Doc:Desc Request a progress message be printed on {\tt stdout}
    //Doc:Desc periodically.
    //Doc:Desc The message is printed either by a \GTNS\ supplied progress
    //Doc:Desc method,
    //Doc:Desc or by a method supplied using the {\tt ProgressHook} method of
    //Doc:Desc class {\tt Simulator }
    //Doc:Arg1 The progress interval, specified in seconds.
    //Doc:Arg1 Note that this specifies
    //Doc:Arg1 the progress method is called periodically in
    //Doc:Arg1 {\em Simulation Time},
    //Doc:Arg1 not in wall--clock time.

  //Doc:Method
  virtual SimulatorEvent* NodeDownAt(Node*, const Random&);
    //Doc:Desc Schedule a node failure at a specified time.
    //Doc:Arg1 Node to fail.
    //Doc:Arg2 A random variable specifying time to fail.
    //Doc:Return  Scheduled event (in case caller wants to cancel)
    
  //Doc:Method
  virtual SimulatorEvent* NodeUpAt(Node*, const Random&);
    //Doc:Desc Schedule a node recovery at a specified time.
    //Doc:Arg1 Node to recover
    //Doc:Arg2 A random variable specifying time to recover
    //Doc:Return  Scheduled event (in case caller wants to cancel)
    
  //Doc:Method
  virtual SimulatorEvent* InterfaceDownAt(Interface*, const Random&);
    //Doc:Desc Schedule a node failure at a specified time.
    //Doc:Arg1 Interface to fail.
    //Doc:Arg2 A random variable specifying time to fail.
    //Doc:Return  Scheduled event (in case caller wants to cancel)
    
  //Doc:Method
  virtual SimulatorEvent* InterfaceUpAt(Interface*, const Random&);
    //Doc:Desc Schedule a node recovery at a specified time.
    //Doc:Arg1 Interface to recover
    //Doc:Arg2 A random variable specifying time to recover
    //Doc:Return  Scheduled event (in case caller wants to cancel)
    
#ifdef MOVED_TO_SCHEDULER
  //Doc:Method
  virtual Event* DeQueue();	     // get next event
    //Doc:Desc Removes the earliest pending event from the event queue
    //Doc:Desc and returns a pointer to it.
    //Doc:Return A pointer to the earliest pending event in the
    //Doc:Return event queue.

  //Doc:Method
  virtual Event* PeekEvent();        // Peek at next event (do not remove)
    //Doc:Desc   Returns a pointer to the earliest pending event in the event
    //Doc:Desc   queue, but does not remove it from the queue.
    //Doc:Return A pointer to the earliest pending event in the
    //Doc:Return event queue.
#endif

  //Doc:Method
  void    PrintStats();              // Print debug statistics
    //Doc:Desc Prints some fairly detailed statistics about the simulation.
    //Doc:Desc Normally would be called only after the 
    //Doc:Desc {\tt Run} method has exited and the simulation has completed.

  //Doc:Method
  void CommonRunInit();              // Common code prior to Run()
    //Doc:Desc Prevents code duplication in Simulator::Run and
    //Doc:Desc DistributedSimulator::Run();

  //Doc:Method
  virtual bool DistributedSim()  {return false; }
    //Doc:Desc   Query if distributed sim (overridden by DistributedSimulator)
    //Doc:Return Always false here.
  
  //Doc:Method
  virtual void Run();                 // Begin processing events
    //Doc:Desc Initiates the simulation.  The {\tt Run} method should be 
    //Doc:Desc called after all the topology and data flow information
    //Doc:Desc has been defined.  {\tt Run} will start processing events,
    //Doc:Desc and will continue until the {\em Stop} event is processed,
    //Doc:Desc the {\tt Halt()} method is called, or until the event
    //Doc:Desc list becomes empty.

  //Doc:Method
  void    StopAt(Time_t t);          // Schedule Halt event at future time
    //Doc:Desc Schedules a {\tt Stop} event at the specified time.  This
    //Doc:Desc will cause the {\tt Run()} method to exit at the specified
    //Doc:Desc time, even if there are more pending unprocessed events.
    //Doc:Arg1 The simulation time to schedule  the {\tt Stop} event.

  //Doc:Method
  void    Halt();                    // Stop processing and exit
    //Doc:Desc Immediately stops processing events and causes the {\tt Run()}
    //Doc:Desc method to exit.  

   //Doc:Method
  void    Silent(bool s) { silent = s; }
    //Doc:Desc   Set silent mode on or off  When silent, the simulator produces
    //Doc:Desc no printed information on stdout.
    //Doc:Arg1 true if silent mode desired.

  //Doc:Method
  void    Name(const std::string&);
    //Doc:Desc Specify a name for this simulation.
    //Doc:Desc Used by to http web interface
    //Doc:Desc to identify the simulation.
    //Doc:Arg1 Name for this simulation

  //Doc:Method
  void    DeleteObject(Object*);     // Schedule a delete object event
    //Doc:Desc Schedules an event at the current simulation time which deletes
    //Doc:Desc the specified {\tt Object}.  Sometimes, a \GTNS\ object will
    //Doc:Desc determine that it is no longer needed, but  a {\em C++} object
    //Doc:Desc cannot delete itself.  This allows an object to schedule its own
    //Doc:Desc deletion.
    //Doc:Arg1 A pointer to the object to be deleted.

  //Doc:Method
  void    ProgressHook(ProgressHook_t); // Add a progress callback
    //Doc:Desc Specifies a subroutine to call each time a {\tt Progress} event
    //Doc:Desc is processed.  The {\tt Process} events are scheduled
    //Doc:Desc periodically, at an interval specified by the {\tt Progress}
    //Doc:Desc method.  The {\tt ProgressHook} method must be a static
    //Doc:Desc method, and must have a single {\tt Time_t} argument and return
    //Doc:Desc {\tt void}.  When the {\tt ProgressHook} method is called
    //Doc:Desc the argument passed is the current simulation time.
    //Doc:Arg1 The static method to call on each {\tt Progress} interval.

  //Doc:Method
  void    AddNotify(NotifyHandler*, Time_t, void*);
    //Doc:Desc Call a notification at the specified time in the future
    //Doc:Arg1 NotifyHandler object to call
    //Doc:Arg2 Time in future.
    //Doc:Arg3 Pointer to pass to the notifier.


  // CleanupOnExit is used to delete all nodes when Simulator destructed
  // This is just for debugging memory leaks
  //Doc:Method
  void    CleanupOnExit(bool c = true) { cleanUp = c;}
    //Doc:Desc Specifies that the {\tt Simulator} object should clean up
    //Doc:Desc all allocated memory objects when destructed.  This would
    //Doc:Desc normally be used when debugging the simulator, to help
    //Doc:Desc locate memory leaks.
    //Doc:Arg1 A boolean specifying whether the {\tt Simulator} object
    //Doc:Arg1 should cleanup when destructed.

  //Doc:Method
  void TopologyChanged(bool);
    //Doc:Desc Notify the simulator that a topology change has occured.
    //Doc:Arg1  True if node or interface UP, false if DOWN

  bool    IsScheduled(Event*);// Debug...see if event is scheduled

  // QTWindow interface

  //Doc:Method
  QTWindow* GetQTWindow() { return qtWin; }
    //Doc:Desc Returns a pointer to the QT Window object
    //Doc:Return Pointer to the QT Window object  

  //Doc:Method
  void    DisplayTopology();         // Show the topology using qt window
    //Doc:Desc Causes a graphical animation window to be opened with a visual
    //Doc:Desc display of the topology.

  //Doc:Method
  void    DisplayTopologyAndReturn(); // Show the topology using qt window
    //Doc:Desc Causes a graphical animation window to be opened with a visual
    //Doc:Desc display of the topology.  Returns immediately.

  //Doc:Method
  void    UpdateTopology();
    //Doc:Desc Causes the existing animiation window to update the state
    //Doc:Desc of all animated objects.

  // Start the animation at a specific time
  //Doc:Method
  void    StartAnimation(Time_t, bool = true, bool = false);
    //Doc:Desc Starts an animation on at the specified time.
    //Doc:Arg1 The time to start the animation.
    //Doc:Arg2 {\tt True} if the animation should initially be paused.
    //Doc:Arg3 {\tt True} if the animation should use the full screen.
    
  
  //Doc:Method
  void    StopAnimation(Time_t);     // Stop and close the animation window
    //Doc:Desc Stops the animiation and closes the animation window
    //Doc:Desc at the specified time.
    //Doc:Arg1 The time to stop the animation.

  //Doc:Method
  void    PauseAnimation(Time_t);    // Pause the animation window
    //Doc:Desc Pauses the animiation
    //Doc:Desc at the specified time.
    //Doc:Arg1 The time to pause the animation.

  //Doc:Method
  void    AnimationUpdateInterval(Time_t);// Initial animation update interval
    //Doc:Desc Specifies the initial update interval for the animation display.
    //Doc:Desc The update interval is adjustable via a slider on the animation
    //Doc:Desc window.
    //Doc:Arg1 The simulation time between animation updates.
  // Statistics

  //Doc:Method
  void    AnimateWirelessTx(bool a, ANIMATE_WIRELESS_TX_FORM wirelessTxForm = CIRCLES);
    //Doc:Desc Specify  detailed  animiation of wireless transmissions
    //Doc:Arg1 true if wireless animation desired
    //Doc:Arg2 Default is CIRCLES. Can be specified to be DIRECTED ARROWS.

  //Doc:Method
  bool    AnimateWirelessTx();
    //Doc:Desc Determine  if wireless transmit animation selected
    //Doc:Return true if wireless animation selected.

  //Doc:Method
  ANIMATE_WIRELESS_TX_FORM AnimateWirelessTxForm();
    //Doc:Desc Determine the wireless transmit animation form selected
    //Doc:Return wireless animation form selected.

  //Doc:Method
  void    AnimateBasebandTx(Time_t when, bool a);
    //Doc:Desc Specify  detailed  animiation of Bluetoth transmissions
    //Doc:Arg1 time to start Bluetooth Tx animation
    //Doc:Arg2 true if Bluetooth animation desired

  //Doc:Method
  bool    AnimateBasebandTx();
    //Doc:Desc Determine  if Bluetooth transmit animation selected
    //Doc:Return true if Bluetooth animation selected.
	
  //Doc:Method
  bool    BasebandTxStart();
    //Doc:Desc Determine  if Bluetooth transmit animation started
    //Doc:Return true if Bluetooth animation started.

  //Doc:Method
  void    PauseOnWirelessTx(bool p) { pauseWirelessTx = p;}
    //Doc:Desc The wireless transmit animations are usually fast,
    //Doc:Desc and can easily
    //Doc:Desc be missed.  Setting TRUE will pause the animation on each
    //Doc:Desc transmit, to allow a pause--based stepping through
    //Doc:Desc the tranmissions for debugging.
    //Doc:Arg1 TRUE if pause before each wireless transmit is desired.
  
  //Doc:Method
  bool    PauseOnWirelessTx() { return pauseWirelessTx; }
    //Doc:Desc Query of pauseOnWirelessTransmit is set.  Used by qtwindow.cc.
    //Doc:Return TRUE if pause on wireless transmit set.
  
  //Doc:Method
  bool    AddBackgroundMap(const std::string&,
                           const RectRegion& = RectRegion());
    //Doc:Desc Add background map lines from the specified map file.  The
    //Doc:Desc maps can be either in the CIA World Databank II format,
    //Doc:Desc or  a simple file of lat/lon coordinates with a single
    //Doc:Desc header line specifying the number of points.  The CIA
    //Doc:Desc database files  are available on the  GTNetS web page.
    //Doc:Arg1 Name of map database file.
    //Doc:Arg2 Bounding Region
    //Doc:Return True if successfully loaded

  bool    AddBackgroundMap(const StringVec_t&,
                           const RectRegion& = RectRegion());
    //Doc:Desc Add background map lines from the specified map files.  The
    //Doc:Desc maps can be either in the CIA World Databank II format,
    //Doc:Desc or  a simple file of lat/lon coordinates with a single
    //Doc:Desc header line specifying the number of points.  The CIA
    //Doc:Desc database files  are available on the  GTNetS web page.
    //Doc:Arg1 Vector of map file  names
    //Doc:Arg2 Bounding Region
    //Doc:Return True if successfully loaded

  //Doc:Method
  bool    PlaybackTraceFile(const char*);
    //Doc:Desc GTNetS can animate a previously run simulation, from the trace
    //Doc:Desc file logged by that simulation.  The topology MUST be identical
    //Doc:Desc to that of the original simulation.
    //Doc:Arg1 Trace file name
    //Doc:Return True if trace file successfully opened

  //Doc:Method
  bool    StartPlayback();
    //Doc:Desc Starts a trace file playback animation
    //Doc:Return True if successful.

  //Doc:Method
  void    CustomBackground(CustomBackground_t);
    //Doc:Desc Specifies a custom background callback
    //Doc:Arg1 Pointer to custom background callback function

  //Doc:Method
  void    NodeSelectedCallback(NodeSelected_t);
    //Doc:Desc Specifies a method to call when nodes are selected
    //Doc:Desc on the animation
    //Doc:Desc display.
    //Doc:Arg1 Callback function pointer.

  //Doc:Method
  void    EnableAnimationRecorder(bool);
    //Doc:Desc Enables or Disables the recorder icon on the animation display.
    //Doc:Desc     Recording takes huge amounts of disk space, so we only
    //Doc:Desc enable this when requested.
    //Doc:Arg1 True if recorder icon enabled.

  //Doc:Method
  void    RecorderMPEGSpeedup(Mult_t);
    //Doc:Desc When recording the MPEG file, the normal frame rate is 25fps.
    //Doc:Desc However, in a network simulation, this means 40ms
    //Doc:Desc between frames, and is much too long to make a 
    //Doc:Desc meaningful simulation animation.  Specifying the speedup
    //Doc:Desc increases the frames per second rate by the specified
    //Doc:Desc value.  For example, a speedup of 10.0 (the default value)
    //Doc:Desc will result in  250 fps.
    //Doc:Arg1 Desired animation recorder speedup.

  //Doc:Method
  void    EnableHTTPServer();
    //Doc:Desc GTNetS has an optional HTTP interface to query the
    //Doc:Desc state of the simulation.  Enabling this causes a
    //Doc:Desc separate thread to be spawned that will service
    //Doc:Desc HTTP request on port 8081. (If port 8081
    //Doc:Desc is not availble, consecutive ports are tried until
    //Doc:Desc one is found.

private:
  // Used internally by the Playback
  void FillEventsFromTrace(Time_t);

public:

  // Various statistics
  //Doc:Method
  Time_t  SetupTime();               // Wall clock time for initializatoin
    //Doc:Desc Returns the amount of wall clock (CPU) time used to initialize
    //Doc:Desc the simulation. This includes all actions from the start of
    //Doc:Desc the {\tt main} function and the call to {\tt Simulator::Run()}.
    //Doc:Return The CPU time used to initialize the simulation.

  //Doc:Method
  Time_t  RouteTime();               // Wall clock time for route computations
    //Doc:Desc Returns the amount of wall clock (CPU) time used to calculate
    //Doc:Desc routing information for the simulation.
    //Doc:Return The CPU time used to calculate routing information.

  //Doc:Method
  Time_t  RunTime();                 // Wall clock time for Run()
    //Doc:Desc Returns the amount of wall clock (CPU) time used by
    //Doc:Desc the event processing phase of the simulation.
    //Doc:Return The CPU time used to by the event processing.

  //Doc:Method
  Time_t  TotalTime();               // Total time of execution
    //Doc:Desc Returns the amount of wall clock (CPU) time used by
    //Doc:Desc the simulation.
    //Doc:Return The CPU time used to by the entire simulation.
#ifdef MOVED_TO_SCHEDULER
  //Doc:Method
  Count_t TotalEventsProcessed() { return totevp;} // Events processed
    //Doc:Desc Returns the count of total events processed by the simulation.
    //Doc:Return The total number of events processed.

  //Doc:Method
  Count_t TotalEventsScheduled() { return totevs;} // Events scheduled
    //Doc:Desc Returns the count of total events scheduled by the simulation.
    //Doc:Return The total number of events scheduled.

  //Doc:Method
  Count_t EventListSize() { return eventList.size();} // Number pending events
    //Doc:Desc Returns the current size of the event list, which is the
    //Doc:Desc number of scheduled, but unprocessed, events.
    //Doc:Return The current size of the pending events list.
#endif

  // X-Y Positions
  //Doc:Method
  void     AmimationRegion(const Location&, const Location&);
    //Doc:Desc Specifies the lower left and upper right corner for the
    //Doc:Desc animation display.
    //Doc:Arg1 Lower left corner
    //Doc:Arg1 Upper right corner.

  //Doc:Method
  void     NewLocation(Meters_t, Meters_t); // Set new x/y location
    //Doc:Desc Informs the animation processor of a new location.
    //Doc:Desc Will expand the AnimationRegion (See above) if needed.
    //Doc:Arg1 x-coordinate
    //Doc:Arg2 y-coordinate

  //Doc:Method
  void     NewLocation(const Location&);    // Set new x/y location
    //Doc:Desc Informs the animation processor of a new location.
    //Doc:Desc     Will expand the AnimationRegion (See above) if needed.
    //Doc:Arg1 x-y location to add.

  Meters_t SmallestX();              // Get the smallest X coordinate
  Meters_t SmallestY();              // Get the smallest Y coordinate
  Meters_t LargestX();               // Get the smallest X coordinate
  Meters_t LargestY();               // Get the smallest Y coordinate
  bool     HasMobility() { return hasMobility;} // True if mobility enabled
  void     HasMobility(bool m) { hasMobility = m;}

protected:
  virtual void ProgressResched(Time_t);
  Time_t       GetSec();             // Get CPU time used (rounded to seconds)

  bool         ProcessOneMap(std::string, const RectRegion&,
                             LocationVecVec_t&);
  RectRegion   FindBoundingBox(LocationVecVec_t&);

public:
  bool            verbose;           // True if verbose debug
  unsigned long   verbosemod;  // Mod factor for verbosity
  bool            cleanUp;     // Cleanup on delete
  Time_t          lastTopologyChange; // Time of last failure or recovery
  bool            supercomputerInterconnect; // True if modeling SC/IC
protected:
  Time_t          simtime;     // Current Simulation Time
  //EventMap_t      eventList;   // The actual event list (moved to scheduler)
  //EventSet_t      eventList;   // The actual event list
public:
#ifdef MOVED_TO_SCHEDULER
  //Doc:Member
  Count_t         totevs;      // Total events scheduled (debug)
    //Doc:Desc Count of the total number of events that have been scheduled

  //Doc:Member
  Count_t         totrm;       // Total events removed (debug)
    //Doc:Desc Count of the total number of events that have been removed

  //Doc:Member
  Count_t         totevp;      // Total events processed (debug)
    //Doc:Desc Count  of the total number of event processed

  //Doc:Member
  Count_t         totevc;      // Total events cancelled (debug)
    //Doc:Desc Count of total number of event cancelled

  //Doc:Member
  Count_t         evlSize;     // Number of events in list
    //Doc:Desc Current size of pending event list

  Count_t         maxSize;
#endif
  //  EventMap_t::iterator hint;   // Hint for insertions (right after prior)
  bool            silent;          // True if no printouts from Simulator
  bool            halted;          // True if halted
protected:
  SimulatorUid_t  uid;              // Current UID
  Time_t          progressInterval; // Interval for progress messages
  bool            serverEnabled;    // True if HTTP server enabled
public:
  ProgressEvent*  progressEvent;    // Pending progress event (if any);
protected:
  ProgressTimer   progressTimer;    // Progress timer
  Time_t          startRouteTime;   // CPU time used before Route computations
  Time_t          startRunTime;     // CPU time used before the "Run" call

  QTWindow*       qtWin;            // qt display window
  bool            animate;          // True if animation active
  bool            playbackAnimate;  // True if animate from trace file
  std::ifstream   playbackFile;     // Input stream for playback file
  bool            animateWirelessTx;// True if wireless tx animation desired
  ANIMATE_WIRELESS_TX_FORM animateWirelessTxForm; // Specifies if wireless tx animation is in the form of circles or directed arrows
  bool            animateBasebandTx;// True if Bluetooth tx animation desired
  bool            basebandTxStart;  // True if Bluetooth tx animation started
  bool            pauseWirelessTx;  // True if pause on wireless transmit 
  bool            haltEventSched;   // True if the halt event is scheduled
  
  // Location min/max
  Meters_t        smallestX;
  Meters_t        smallestY;
  Meters_t        largestX;
  Meters_t        largestY;
  bool            firstLocation;    // True if first call to set location
  bool            hasMobility;      // True if any mobility models used
public:
  static Simulator* instance;
  static PHVec_t    progressHooks;  // List of callbacks on progress
  static RectRegion boundingBox;    // Min/Max X/Y positions
  static std::string simName;       // Name of this simulation for display
  static CustomBackground_t pCustomBackground; // Custom background callback
  static NodeSelected_t     pNodeSelected;     // Node selected callback
  static bool       recorderEnabled;// True if animation recorder icon enabled
  static Mult_t     recorderSpeedup;// Animation recorder speedup rate

#ifdef CTRLFIFO
  static std::string     name;
  static int             sockfd;
  static void            io_signal_handler(int);
  static void            toBlock(int);
  static void            toUnblock(int);
#endif
  
  //Doc:Method
  static Time_t   Now();            // Returns current time (0 if no instance)
    //Doc:Desc Returns the current simulation time.
    //Doc:Return The current simulation time.

  //Doc:Method
  static Size_t  ReportMemoryUsage();  // Memory usage in bytes
    //Doc:Desc Returns the total amount of memory (bytes) used by the
    //Doc:Desc simulation.
    //Doc:Return The total number of memory bytes used by the simulation.

  //Doc:Method
  static Size_t  ReportMemoryUsageMB();// Memory usage in megabytes
    //Doc:Desc Returns the total amount of memory (MegaBbytes) used by the
    //Doc:Desc simulation.
    //Doc:Return The total number of memory MegaBytes used by the simulation.
};

#endif


