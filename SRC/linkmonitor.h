// Defines a class that monitors link performance by service class
// George F. Riley, Georgia Tech, Fall 2004

#include <iostream>
#include <vector>
#include "common-defs.h"

class Packet;

class LinkMonitor
{
public:
  LinkMonitor();
  virtual ~LinkMonitor();
  virtual LinkMonitor* Copy() const;
  void Transmit(Packet*);  // Called on every packet transmit
  void Log(std::ostream&, Mult_t = 0.0, const char* = nil, char = ' ');
public:
  std::vector<double> history;  // Number bits currently tx'ed for each class
};
