// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: node-ghost.cc,v 1.15 2005/11/15 20:48:03 riley Exp $



// Georgia Tech Network Simulator - Node class
// George F. Riley.  Georgia Tech, Spring 2002

#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#ifdef HAVE_QT
#include <qcolor.h>
#include <qcanvas.h>
#endif

//#define DEBUG_MASK 0x02
#include "debug.h"
#include "common-defs.h"
#include "node-ghost.h"
#include "interface-ghost.h"
#include "duplexlink.h"

using namespace std;

// Static members
Count_t        NodeGhost::count = 0;
RadioVec_t     NodeGhost::emptyIf;

NodeGhost::NodeGhost(Node* n)
    : NodeImpl(n), isSatellite(false)
{
  count++; // debug
}

NodeGhost::~NodeGhost()
{
}

NodeImpl* NodeGhost::Copy() const
{ // Return a copy of this node
  return new NodeGhost(*this);
}

bool NodeGhost::IsReal()
{ // True if Real node.  Always false for Ghosts
  return false;
}


void NodeGhost::Broadcast(Packet* p, Proto_t proto)
{ // Broadcast a packet on all interfaces
  delete p; // Should never be called for ghosts
}

// Queue management

Queue* NodeGhost::GetQueue()
{
  return nil;
}

Queue* NodeGhost::GetQueue(Node* n)
{
  return nil;
}

// Applications
Application* NodeGhost::AddApplication(const Application& a)
{
  return nil;
}

// Routing Interface
void NodeGhost::DefaultRoute(RoutingEntry)
{ // Specify default route
}

void NodeGhost::DefaultRoute(Node*)
{ // Specify default route
}

void NodeGhost::AddRoute(IPAddr_t, Count_t, Interface*, IPAddr_t)
{
}

RoutingEntry NodeGhost::LookupRoute(IPAddr_t)
{ // Find a routing entry
  // Still not found, see if on directly connected interface
  RoutingEntry r;
  return r;
}

RoutingEntry NodeGhost::LookupRouteNix(Count_t)
{ // Lookup route by Neighbor Index
  RoutingEntry r;
  return r;
}

AODVRoutingEntry* NodeGhost::LookupRouteAODV(IPAddr_t)
{
  return nil;
}

void NodeGhost::SetRoutingAODV(void*)
{
}

RoutingAODV* NodeGhost::GetRoutingAODV()
{
  return nil;
}

Routing::RType_t NodeGhost::RoutingType()
{
  return Routing::STATIC;
}

void NodeGhost::InitializeRoutes()
{ // Routing initialization (if any)
}

void NodeGhost::ReInitializeRoutes(bool)
{ // Routing initialization (if any)
}

Count_t NodeGhost::RoutingFibSize() const
{ // Get the size of the routing FIB for statistics measurement
  return 0;
}

RoutingNixVector* NodeGhost::GetNixRouting()
{ // Returns a pointer to the routing object if it's NixVector routing,
  // otherwise return nil
  return nil; // Not NixVector
}

// Protocol Graph Interface
#ifdef COMPACT
// No private protocol graphs in compact mode
Protocol* NodeGhost::LookupProto(Layer_t l, Proto_t p) // Lookup protocol by layer
{
  return ProtocolGraph::common->Lookup(l,p);
}

void NodeGhost::InsertProto(Layer_t, Proto_t, Protocol*) // Insert a protocol
{
  cout << "Private protocol graph not supported in compact mode" << endl;
  return;
}
#else
Protocol* NodeGhost::LookupProto(Layer_t, Proto_t)
{ // Lookup protocol by layer
  return nil;
}

void NodeGhost::InsertProto(Layer_t, Proto_t, Protocol*)
{ // Insert proto
}
#endif

// Port Demux Interface
bool NodeGhost::Bind(Proto_t, PortId_t, Protocol*)
{
  return false;
}

bool NodeGhost::Bind(Proto_t, PortId_t, IPAddr_t, PortId_t, IPAddr_t, Protocol*)
{ // Register port usage
  return false;
}

PortId_t NodeGhost::Bind(Proto_t, Protocol*)
{ // Choose available port and register
  return NO_PORT;
}

bool NodeGhost::Unbind(Proto_t, PortId_t, Protocol*)
{ // Remove port binding
  return false;
}

bool NodeGhost::Unbind(Proto_t proto, PortId_t, IPAddr_t, PortId_t, IPAddr_t,
                       Protocol*)
{ // Remove port binding
  return false;
}

Protocol* NodeGhost::LookupByPort(Proto_t, PortId_t)
{
  return nil;
}

Protocol* NodeGhost::LookupByPort(Proto_t proto, PortId_t, IPAddr_t, PortId_t,
                                  IPAddr_t)
{
  return nil;
}

// Packet tracing interface
// Trace header if enabled
bool NodeGhost::TracePDU(Protocol*, PDU*, Packet*, char*)
{
  return false;
}

void NodeGhost::SetTrace(Trace::TraceStatus)    // Set trace level this node
{
}

// Node Location Interface
void NodeGhost::SetLocation(Meters_t, Meters_t, Meters_t)
{ // Set x/y/z location
}

void NodeGhost::SetLocation(const Location& l)
{
}

void NodeGhost::SetLocationLongLat(const Location& l)
{
}

bool NodeGhost::HasLocation()
{
  return false;
}

Meters_t NodeGhost::LocationX()
{
  return 0;
}

Meters_t NodeGhost::LocationY()
{
  return 0;
}

Meters_t NodeGhost::LocationZ()
{
  return 0;
}

Location NodeGhost::GetLocation()
{
  return Location(0, 0);
}

Location NodeGhost::UpdateLocation()
{
  return GetLocation();
}

Mobility* NodeGhost::AddMobility(const Mobility&)
{ // Add a new mobility model for this node
  return nil;
}

Mobility* NodeGhost::GetMobility() const
{ // Get the mobility model for this node
  return nil;
}

bool NodeGhost::IsMoving()
{
  return false;
}

bool NodeGhost::IsMobile()
{
  return false;
}

QCanvasItem* NodeGhost::Display(QTWindow*)
{
  return nil;
}

QCanvasItem* NodeGhost::Display(const QPoint&, QTWindow*)
{
  return nil;
}

void NodeGhost::WirelessTxColor(const QColor&)
{
}

const QColor& NodeGhost::WirelessTxColor()
{
#ifdef HAVE_QT
  return Qt::black;
#else
#endif
}

bool NodeGhost::PushWirelessTx(QCanvasItem*)
{
  return false;
}

QCanvasItem* NodeGhost::PopWirelessTx()
{
  return nil;
}

void NodeGhost::PixelSize(Count_t)  // Set node size
{
}

Count_t NodeGhost::PixelSizeX()
{
  return 0;
}

Count_t NodeGhost::PixelSizeY()
{
  return 0;
}

void NodeGhost::Shape(Node::Shape_t ns) // Set node shape
{
}

Node::Shape_t NodeGhost::Shape()
{
  return Node::NONE;
}

void NodeGhost::Color(const QColor&)
{
}

bool NodeGhost::HasColor()
{
  return false;
}

static QColor noColor;
QColor& NodeGhost::Color()
{
  return noColor;
}

bool NodeGhost::WirelessTx()
{
  return false;
}

bool NodeGhost::WirelessRx()
{
  return false;
}

bool NodeGhost::WirelessCx()
{
  return false;
}

bool NodeGhost::WirelessRxMe()
{
  return false;
}

bool NodeGhost::WirelessRxZz()
{
  return false;
}

NodeAnimation* NodeGhost::GetNodeAnimation() const
{
  return nil;
}

void NodeGhost::UserInformation(void* info)
{ // Nothing to do
}

void* NodeGhost::UserInformation()
{
  return nil;
}

// Callback management

void      NodeGhost::AddCallback(Layer_t, Proto_t,
                                 PacketCallbacks::Type_t,
                                 Interface*, 
                                 PacketCallbacks::Function_t)
{
  // Nothing needed
}

void      NodeGhost::AddCallbackHead(Layer_t, Proto_t,
                                     PacketCallbacks::Type_t,
                                     Interface*, 
                                     PacketCallbacks::Function_t)
{
  // Nothing needed
}

void      NodeGhost::DeleteCallback(Layer_t, Proto_t, 
                                    PacketCallbacks::Type_t, Interface*)
{
  // Nothing needed
}

bool      NodeGhost::CallCallbacks(Layer_t, Proto_t,
                                   PacketCallbacks::Type_t,
                                   Packet*, Interface*)
{
  return false;
}

void NodeGhost::IsSwitchNode(bool)
{
}

void NodeGhost::PacketRX(Packet*, Interface*)
{
}
