\contentsline {chapter}{\numberline {1}Introduction}{3}
\contentsline {chapter}{\numberline {2}Overview}{5}
\contentsline {paragraph}{Include Files.}{7}
\contentsline {paragraph}{Main Program.}{7}
\contentsline {paragraph}{Simulator Object.}{7}
\contentsline {paragraph}{Defining the Trace File.}{7}
\contentsline {paragraph}{Create Simulated Nodes.}{8}
\contentsline {paragraph}{Create Simulated Links.}{8}
\contentsline {paragraph}{Create the TCP Servers.}{8}
\contentsline {paragraph}{Create the TCP Clients.}{9}
\contentsline {paragraph}{Start the Client Applications.}{9}
\contentsline {paragraph}{Start the Simulation.}{9}
\contentsline {chapter}{\numberline {3}The Simulator Object}{10}
\contentsline {chapter}{\numberline {4}Defining Topologies}{18}
\contentsline {section}{\numberline {4.1}Nodes}{18}
\contentsline {section}{\numberline {4.2}Interfaces}{39}
\contentsline {section}{\numberline {4.3}Queues}{41}
\contentsline {section}{\numberline {4.4}Links}{49}
\contentsline {section}{\numberline {4.5}Stock Objects}{68}
\contentsline {subsection}{\numberline {4.5.1}GTitm Modeller}{68}
\contentsline {subsection}{\numberline {4.5.2}TransitNode}{69}
\contentsline {subsection}{\numberline {4.5.3}SubDomain}{71}
\contentsline {subsection}{\numberline {4.5.4}Star Topology}{71}
\contentsline {subsection}{\numberline {4.5.5}Grid Topology}{74}
\contentsline {subsection}{\numberline {4.5.6}Dumbbell Topology}{76}
\contentsline {subsection}{\numberline {4.5.7}Tree Topology}{81}
\contentsline {subsection}{\numberline {4.5.8}Random Topology}{83}
\contentsline {chapter}{\numberline {5}Defining Applications}{84}
\contentsline {section}{\numberline {5.1}Application Base Class}{84}
\contentsline {section}{\numberline {5.2}CBRApplication}{87}
\contentsline {section}{\numberline {5.3}OnOffApplication}{88}
\contentsline {section}{\numberline {5.4}TCPApplication}{89}
\contentsline {section}{\numberline {5.5}TCPReceive}{90}
\contentsline {section}{\numberline {5.6}TCPServer}{91}
\contentsline {section}{\numberline {5.7}WebServer}{92}
\contentsline {section}{\numberline {5.8}WebBrowser}{93}
\contentsline {chapter}{\numberline {6}Layer 4 Protocols}{96}
\contentsline {section}{\numberline {6.1}L4Protocol Base Class}{96}
\contentsline {section}{\numberline {6.2}TCP Protocol Base Class and Variations}{101}
\contentsline {section}{\numberline {6.3}Round--Trip Time Estimators}{108}
\contentsline {section}{\numberline {6.4}UDP Protocol}{111}
\contentsline {chapter}{\numberline {7}Layer 3 Protocols}{112}
\contentsline {chapter}{\numberline {8}Layer 2 Protocols}{114}
\contentsline {section}{\numberline {8.1}IEEE 802.2}{114}
\contentsline {section}{\numberline {8.2}IEEE 802.11}{115}
\contentsline {chapter}{\numberline {9}Routing}{116}
\contentsline {section}{\numberline {9.1}Routing Base Class}{116}
\contentsline {section}{\numberline {9.2}Nix--Vector Routing}{118}
\contentsline {section}{\numberline {9.3}Static Routing}{120}
\contentsline {section}{\numberline {9.4}Manual Routing}{122}
\contentsline {section}{\numberline {9.5}Wireless Dynamic Source Routing}{123}
\contentsline {chapter}{\numberline {10}Tracing Packets}{124}
\contentsline {section}{\numberline {10.1}Trace File Object}{125}
\contentsline {chapter}{\numberline {11}Miscellaneous Support Objects}{128}
\contentsline {section}{\numberline {11.1}Random Number Generators}{128}
\contentsline {subsection}{\numberline {11.1.1}Random Number Base Class}{128}
\contentsline {subsection}{\numberline {11.1.2}Uniform Random}{129}
\contentsline {subsection}{\numberline {11.1.3}Constant Random}{130}
\contentsline {subsection}{\numberline {11.1.4}Sequential Random}{130}
\contentsline {subsection}{\numberline {11.1.5}Exponential Random}{131}
\contentsline {subsection}{\numberline {11.1.6}Pareto Random}{131}
\contentsline {subsection}{\numberline {11.1.7}Empirical Random}{132}
\contentsline {subsection}{\numberline {11.1.8}Integer Empirical Random}{132}
\contentsline {section}{\numberline {11.2}Statistics Gathering}{133}
\contentsline {subsection}{\numberline {11.2.1}Statistics Base Class}{133}
\contentsline {subsection}{\numberline {11.2.2}Histogram Class}{133}
\contentsline {subsection}{\numberline {11.2.3}Average--Min--Max Class}{134}
\contentsline {section}{\numberline {11.3}Packets and PDUs}{135}
\contentsline {subsection}{\numberline {11.3.1}Packets}{135}
\contentsline {subsection}{\numberline {11.3.2}Protocol Data Units}{138}
\contentsline {section}{\numberline {11.4}Command Line Argument Processing}{140}
\contentsline {section}{\numberline {11.5}{\em IP Address}\ Management Object}{143}
\contentsline {section}{\numberline {11.6}Time and Rate Parsing Objects}{143}
\contentsline {subsection}{\numberline {11.6.1}Time Parsing Object}{144}
\contentsline {subsection}{\numberline {11.6.2}Rate Parsing Object}{144}
