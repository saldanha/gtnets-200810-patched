/*

* GTNetS provides a portable C++ class library for network simulations
*
* Copyright (C) 2003 George F. Riley
*
* No guarantees or warranties or anything are provided or implied in any way
* whatsoever.  Use this program at your own risk.  Permission to use this
* program for any purpose is given, as long as the copyright is kept intact.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

* Permission is hereby granted for any non-commercial use of this code

*/

// Georgia Tech Network Simulator - DAW class
// Mohamed Abd Elhafez.  Georgia Tech, Summer 2004

// Implements the DAW algorithm for worm containment

#ifndef __daw_h__
#define __daw_h__

#include <queue>

#include "common-defs.h"
#include "pdu.h"
#include "ipaddr.h"
#include "ipv4.h"
#include "node.h"
#include "tcp.h"
#include "worm-containment.h"

#define TCP_PROTOCOL 6
#define UDP_PROTOCOL 17

class FailureRecord {

 public:
  FailureRecord();

 public:
  Count_t  failCount;      // How many failure replys did that host receive
  Time_t   t;              // timestamp
  double   failRate;       // failure rate
};

class BlockRecord {
 public:
  BlockRecord();
  BlockRecord(FailureRecord*,int, Time_t);

 public:
  FailureRecord* record;
  int tokens;
  Time_t time;
};

typedef std::map<IPAddr_t, FailureRecord> FailMap_t;
typedef std::map<IPAddr_t, BlockRecord> BlockMap_t;
typedef std::vector<Interface*> IFVec_t;


//Doc:ClassXRef
class DAW : public WormContainment{
//Doc:Class The class {\tt DAW} implements the DAW algorithm
	
public:
  //Doc:Method
  DAW();
  //Doc:Desc Default constructor

  virtual ~DAW() {}
  
  //Doc:Method
  void ProcessInPacket(Packet*, Interface*);
  //Doc:Desc Process the given packet for TCP SYN packets 

  //Doc:Method
  void ProcessOutPacket( Packet*, IPAddr_t,  int, Interface*);
  //Doc:Desc Process the given packet for ICMP packets 

  //Doc:Method
  void UpdateFailureRateRecord(FailMap_t::iterator);
  //Doc:Desc Process the given packet for TCP SYN packets 
  
 //Doc:Method
  void BasicRateLimit(Packet*, BlockMap_t::iterator, Interface*);
  //Doc:Desc Process the given packet for TCP SYN packets 

 //Doc:Method
  void TemporalRateLimit(Packet*, BlockMap_t::iterator, Interface*);
  //Doc:Desc Process the given packet for TCP SYN packets 

  bool IsOutIface(Interface*);

  void SetOutIfs(IFVec_t);
  
  void SetOutIf(Interface*);

  static void SetTimeout(Time_t);

private:
  FailMap_t failureratemap;
  BlockMap_t blockmap; 
  IFVec_t    outIfs;
  static Time_t timeout;

 
};

#endif

