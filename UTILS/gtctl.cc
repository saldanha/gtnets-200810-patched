/*
  The controller
 */


#include <stdio.h>

#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <string>
#include <iostream>


#include "SRC/ctldefs.h"

using namespace std;

int main (int argc,
	  char* argv[])
{
	int sockfd;
        struct sockaddr_un saun;
	char code = 0x32;
	int itr, num, found=0;

	string name= getenv("HOME");
	name.append("/.gtsock");
	
	if (argc < 2) {
		printf("Usage: %s <action> [arguement] \n", argv[0] );
		return -1;
	}

        if ((sockfd = socket(PF_UNIX, SOCK_DGRAM, 0)) < 0) {
              perror("socket");
  	}
	memset(&saun, 0, sizeof(saun));
        saun.sun_family = PF_UNIX;
        strcpy(saun.sun_path, name.c_str());

	if ( connect (sockfd, (struct sockaddr* ) &saun, sizeof(saun)) < 0)
		perror("connect");

	num = sizeof(knowncodes) / sizeof (struct codemap);

	for (itr = 0; itr < num; itr++) {
		if (strcasecmp(knowncodes[itr].name, argv[1]) == 0) {
			found = 1;
			code = knowncodes[itr].code;
			if (write (sockfd, &code, sizeof(code)) < 0 ) {
				printf("Error writing to socket \n");
			}
		}
	}
	if (!found)
		printf("%s:  %s Unknown arguement \n", argv[0], argv[1]);

	return 0;
}
