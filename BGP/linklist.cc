/****************************************************************************/
/*  File:            linklist.cc                                            */
/*  Author:          Sunitha Beeram                                         */
/*  Email:           sunithab@ece.gatech.edu                                */
/*  Documentation:   TBD                                                    */
/*                                                                          */
/*  Version:         1.0beta                                                */
/*                                                                          */
/*  Copyright (c) 2005 Georgia Institute of Technology                      */
/*  This program is free software; you can redistribute it and/or           */
/*  modify it under the terms of the GNU General Public License             */
/*  as published by the Free Software Foundation; either version 2          */
/*  of the License, or (at your option) any later version.                  */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful,         */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*  GNU General Public License for more details.                            */
/*                                                                          */
/****************************************************************************/

#include "bgp.h"

/* Allocate new list. */
struct llist *
BGP::list_new ()
{
  struct llist *New;

  New = (struct llist*)XMALLOC (MTYPE_LINK_LIST, sizeof (struct llist));
  memset (New, 0, sizeof (struct llist));
  return New;
}

/* Free list. */
void
BGP::list_free (struct llist *l)
{
  XFREE (MTYPE_LINK_LIST, l);
}

/* Allocate new listnode.  Internal use only. */
struct listnode *
BGP::listnode_new ()
{
  struct listnode *node;

  node = (struct listnode*)XMALLOC (MTYPE_LINK_NODE, sizeof (struct listnode));
  memset (node, 0, sizeof (struct listnode));
  return node;
}

/* Free listnode. */
void
BGP::listnode_free (struct listnode *node)
{
  XFREE (MTYPE_LINK_NODE, node);
}

/* Add new data to the list. */
void
BGP::listnode_add (struct llist *llist, void *val)
{
  struct listnode *node;

  node = listnode_new ();

  node->prev = llist->tail;
  node->data = val;

  if (llist->head == NULL)
    llist->head = node;
  else
    llist->tail->next = node;
  llist->tail = node;

  llist->count++;
}

/* Add new node with sort function. */
void
BGP::listnode_add_sort (struct llist *llist, void *val)
{
  struct listnode *n;
  struct listnode *New;

  int res;
	
	New = listnode_new ();
  New->data = val;

  if (llist->cmp)
    {
      for (n = llist->head; n; n = n->next)
			{
	  		res = (this->*(llist->cmp)) (val, n->data);
				if ( res < 0 ) 
	    	{	    
	      	New->next = n;
	      	New->prev = n->prev;

	      	if (n->prev)
						n->prev->next = New;
	      	else
						llist->head = New;

	      	n->prev = New;
	      	llist->count++;
	      	return;
	    }
		}
	}

  New->prev = llist->tail;

  if (llist->tail)
    llist->tail->next = New;
  else
    llist->head = New;

  llist->tail = New;
  llist->count++;
}

void
BGP::listnode_add_after (struct llist *llist, struct listnode *pp, void *val)
{
  struct listnode *nn;

  nn = listnode_new ();
  nn->data = val;

  if (pp == NULL)
    {
      if (llist->head)
	llist->head->prev = nn;
      else
	llist->tail = nn;

      nn->next = llist->head;
      nn->prev = pp;

      llist->head = nn;
    }
  else
    {
      if (pp->next)
	pp->next->prev = nn;
      else
	llist->tail = nn;

      nn->next = pp->next;
      nn->prev = pp;

      pp->next = nn;
    }
}


/* Delete specific date pointer from the list. */
void
BGP::listnode_delete (struct llist *llist, void *val)
{
  struct listnode *node;

  for (node = llist->head; node; node = node->next)
    {
      if (node->data == val)
	{
	  if (node->prev)
	    node->prev->next = node->next;
	  else
	    llist->head = node->next;

	  if (node->next)
	    node->next->prev = node->prev;
	  else
	    llist->tail = node->prev;

	  llist->count--;
	  listnode_free (node);
	  return;
	}
    }
}

/* Return first node's data if it is there.  */
void *
BGP::listnode_head (struct llist *llist)
{
  struct listnode *node;

  node = llist->head;

  if (node)
    return node->data;
  return NULL;
}

/* Delete all listnode from the list. */
void
BGP::list_delete_all_node (struct llist *llist)
{
  struct listnode *node;
  struct listnode *next;

  for (node = llist->head; node; node = next)
    {
      next = node->next;
      if (llist->del)
	(this->*llist->del) (node->data);
      listnode_free (node);
    }
  llist->head = llist->tail = NULL;
  llist->count = 0;
}

/* Delete all listnode then free list itself. */
void
BGP::list_delete (struct llist *llist)
{
  struct listnode *node;
  struct listnode *next;

  for (node = llist->head; node; node = next)
    {
      next = node->next;
      if (llist->del)
	(this->*llist->del) (node->data);
      listnode_free (node);
    }
  list_free (llist);
}

/* Lookup the node which has given data. */
struct listnode *
BGP::listnode_lookup (struct llist *llist, void *data)
{
  struct listnode *node;

  for (node = llist->head; node; nextnode (node))
    if (data == getdata (node))
      return node;
  return (struct listnode*)NULL;
}

/* Delete the node from list.  For ospfd and ospf6d. */
void
BGP::list_delete_node (struct llist* llist, struct listnode *node)
{
  if (node->prev)
    node->prev->next = node->next;
  else
    llist->head = node->next;
  if (node->next)
    node->next->prev = node->prev;
  else
    llist->tail = node->prev;
  llist->count--;
  listnode_free (node);
}

/* ospf_spf.c */
void
BGP::list_add_node_prev (struct llist *llist, struct listnode *current, void *val)
{
  struct listnode *node;

  node = listnode_new ();
  node->next = current;
  node->data = val;

  if (current->prev == NULL)
    llist->head = node;
  else
    current->prev->next = node;

  node->prev = current->prev;
  current->prev = node;

  llist->count++;
}

/* ospf_spf.c */
void
BGP::list_add_node_next (struct llist *llist,struct listnode *current, void *val)
{
  struct listnode *node;

  node = listnode_new ();
  node->prev = current;
  node->data = val;

  if (current->next == NULL)
    llist->tail = node;
  else
    current->next->prev = node;

  node->next = current->next;
  current->next = node;

  llist->count++;
}

/* ospf_spf.c */
void
BGP::list_add_list (struct llist *l, struct llist *m)
{
  struct listnode *n;

  for (n = listhead (m); n; nextnode (n))
    listnode_add (l, n->data);
}
