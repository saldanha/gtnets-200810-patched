// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: node-real.h 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - Node class
// George F. Riley.  Georgia Tech, Spring 2002

#ifndef __nodereal_h__
#define __nodereal_h__

#include <map>
#include <deque>
#include <string>

#include "common-defs.h"
#include "node-impl.h"

#include "handler.h"
#include "routing.h"
#include "trace.h"
#include "macaddr.h"
#include "routing-aodv.h"
#define DEFAULT_SW_TIMEOUT 10   // default timeout for the switch map  secs



class Link;
class Node;
class Interface;
class Routing;
class Protocol;
class ProtocolGraph;
class PortDemux;
class Linkp2p;
class Queue;
class Application;
class RoutingNixVector;
class Mobility;
class Node;
class Image;
class QPixmap;
class QTWindow;
class QCanvasPolygonalItem;
class QCanvasSprite;
class QCanvasItem;

#ifdef HAVE_QT
#include <qcolor.h>


class NodeAnimation {
public:
  NodeAnimation() : show(false),
                    pixelSizeX(Node::DefaultPixelSize()),
                    pixelSizeY(Node::DefaultPixelSize()),
                    shape(Node::DefaultShape()),
                    csCallback(nil),
                    color(Qt::black),
                    hasColor(false), placeImage(false), nodePolygon(nil),
                    nodeSprite(nil) {}

public:
  bool          show;         // True if animation display desired
  Count_t       pixelSizeX;   // Size of node in pixels (X)
  Count_t       pixelSizeY;   // Size of node in pixels (Y)
  Node::Shape_t shape;        // Shape of node
  CustomShape_t csCallback;   // Custom Shape Callback
  QColor        color;        // Color for this node on animation display
  bool          hasColor;     // True if color specified
  bool          placeImage;   // True if image sprite needs to be moved
  QCanvasPolygonalItem*  nodePolygon;// Polygonal item (if not image)
  QCanvasSprite*         nodeSprite; // Image item (for IMAGE shape only)
};

// Define a deque for managing the QCanvasItem pointers for
// wireless animation

typedef std::deque<QCanvasItem*>        CanvasItemDeq_t;

#endif

typedef unsigned int Port_t;

class PortPair {
public:
  PortPair() : port(0), time(0) {}
  PortPair(Port_t p, Time_t t) : port(p), time(t) {}

public:
  Port_t  port;
  Time_t  time;
};

typedef std::map<MACAddr, PortPair> MACPort_t;

// Define a map for cacheing nixvector route lookups
typedef std::map<Count_t, RoutingEntry> RENixMap_t;
// And a vector of RoutingEntries for Nix lookup (obviates above)
typedef std::vector<RoutingEntry>       RENixVec_t;




//Doc:ClassXRef
class NodeReal : public NodeImpl {
public:
  NodeReal(Node*);
  ~NodeReal();

  NodeImpl* Copy() const;                // All nodeimpl's must be copy'able
public:
  // IP Address Management is all done by node-impl

  // Real/Ghost test
  bool     IsReal();                     // True if node is real

  // Satellite
  virtual bool     IsSatellite() const { return false;}
  virtual void     IsSatellite(bool)   {  }

  // Interface management is all done by node-impl

  // Broadcast a packet
  void       Broadcast(Packet*, Proto_t);// Broadcast packet on all interfaces

  // All link managment done by node-impl.h

  // Queues
  Queue*     GetQueue();                    // Return queue if only one i/f
  Queue*     GetQueue(Node*);               // Get the queue to specified node

  // Applications
  Application* AddApplication(const Application& a);

  // Neighbor management, done by node-impl

  // Routing
  void DefaultRoute(RoutingEntry);       // Specify default route
  void DefaultRoute(Node*);              // Specify default route
  void AddRoute(IPAddr_t, Count_t, Interface*, IPAddr_t);
  RoutingEntry LookupRoute(IPAddr_t);    // Find a routing entry
  RoutingEntry LookupRouteNix(Count_t);  // Lookup route by Neighbor Index
  Routing::RType_t RoutingType();        // Report type of routing in use
  // LocalRoute used from node-impl
  void       InitializeRoutes();         // Routing initialization (if any)
  void       ReInitializeRoutes(bool);   // Routing re-initialization
  Count_t    RoutingFibSize() const;     // Statistics, size of FIB
  // GetNix used from node-impl
  RoutingNixVector* GetNixRouting();     // Returns Nix rtg pointer (if Nix)
  Routing*   GetRouting() { return pRouting;} // Returns the routing object

  //AODV Routing
  //Specially for AODV routing, it is not compatiable with
  //other route entry.
  //
  AODVRoutingEntry *LookupRouteAODV(IPAddr_t);
  void      SetRoutingAODV(void *pRouting);
  RoutingAODV * GetRoutingAODV(); //return the RoutingAODV object pointer.

  // Protocol Graph Interface
  Protocol* LookupProto(Layer_t, Proto_t);// Lookup protocol by layer
  void      InsertProto(Layer_t, Proto_t, Protocol*); // Insert a protocol

  // Port Demux Interface
  bool      Bind(Proto_t, PortId_t, Protocol*);    // Register port usage
  bool      Bind(Proto_t,
                 PortId_t, IPAddr_t,
                 PortId_t, IPAddr_t,
                 Protocol*);                       // Register port usage
  PortId_t  Bind(Proto_t, Protocol*);              // Choose port and register
  bool      Unbind(Proto_t, PortId_t, Protocol*);  // Remove port binding
  bool      Unbind(Proto_t,
                   PortId_t, IPAddr_t,
                   PortId_t, IPAddr_t,
                   Protocol*);                     // Remove port binding
  Protocol* LookupByPort(Proto_t, PortId_t);       // Lookup by port
  Protocol* LookupByPort(Proto_t,
                         PortId_t, IPAddr_t,
                         PortId_t, IPAddr_t);      // Lookup by s/d port/ip

  // Packet tracing interface
  // Trace header if enabledP
  bool      TracePDU(Protocol*, PDU*, Packet*, char*);
  void      SetTrace(Trace::TraceStatus);          // Set trace level this node

  // Node location (x, y) interface
  void      SetLocation(Meters_t, Meters_t, Meters_t);  // Set x/y/z location
  void      SetLocation(const Location&);          // Set x/y/z location
  void      SetLocationLongLat(const Location&);   // Set x/y/z location
  bool      HasLocation();                         // True if location assigned
  Meters_t  LocationX();
  Meters_t  LocationY();
  Meters_t  LocationZ();
  Location  GetLocation();                         // Return current location
  Location  UpdateLocation();                      // Get updated location
  Mobility* AddMobility(const Mobility&);          // Add a mobility model
  Mobility* GetMobility() const;                   // Get the mobility model
  bool      IsMobile();                            // True if mobility enabled
  bool      IsMoving();                            // True if not pausing

#ifdef HAVE_QT  
  bool      WirelessTx();                          // True if wireless transmit
  bool      WirelessRx();                          // True if wireless receive
  bool      WirelessCx();                          // True if wireless collid
  bool      WirelessRxMe();                        // True if wireless receive
  bool      WirelessRxZz();                        // True if wireless receive
  void      UserInformation(void*);                // Specify user info
  void*     UserInformation();                     // Return user info

  // QTWindow information
  void           Show(bool);                        // Turn on/off display
  bool           Show();                            // True if display enabled
  QCanvasItem*   Display(QTWindow*);
  QCanvasItem*   Display(const QPoint&, QTWindow*);
  void           WirelessTxColor(const QColor&);
  const          QColor& WirelessTxColor();
  bool           PushWirelessTx(QCanvasItem*);
  QCanvasItem*   PopWirelessTx();
  void           PixelSize(Count_t);                // Set/query node size
  Count_t        PixelSizeX();
  Count_t        PixelSizeY();
  void           Shape(Node::Shape_t);              // Set/query node shape
  Node::Shape_t  Shape();
  CustomShape_t  CustomShape();                     // Custom Shape Callback
  void           CustomShape(CustomShape_t);        // Specify Sustom Shape CB
  bool           CustomShapeFile(const char*);      // Custom shape image file
  bool           CustomShapeImage(const Image&);    // Custom shape image file
  void           Color(const QColor&);
  bool           HasColor();
  QColor&        Color();
  NodeAnimation* GetNodeAnimation() const;

#endif
  
  // ICMP related
  bool   ICMPEnabled() const;
  void   DisableICMP();

#ifdef MOVED_TO_IMPL
  // Failure management
  void   Down();
  void   Up();
  bool   IsDown();
#endif

  Meters_t  Distance(Node*);
  void      BuildRadioInterfaceList(WirelessLink*);
  const RadioVec_t& GetRadioInterfaceList();
  void      SetRadioRange(Meters_t range) { radioRange = range; }
  Meters_t  GetRadioRange() { return radioRange; }

  void      AddCallback(Layer_t, Proto_t,
                        PacketCallbacks::Type_t,
                        Interface*,
                        PacketCallbacks::Function_t);
  void      AddCallbackHead(Layer_t, Proto_t,
                            PacketCallbacks::Type_t,
                            Interface*,
                            PacketCallbacks::Function_t);
  void      DeleteCallback(Layer_t, Proto_t, PacketCallbacks::Type_t,
                           Interface*);
  bool      CallCallbacks(Layer_t, Proto_t, PacketCallbacks::Type_t,
                          Packet*, Interface*);

  void      setBattery(Joules_t joules) { battery = joules;}
  Joules_t  getBattery(void) { return battery; }
  void      setComputePower(double pow) { computePower = pow; }
  double    getComputePower(void) { return computePower; }

  void         setRouteTable(RouteTable* rtable_) { rtable = rtable_;}
  RouteTable*  getRouteTable(void) { return rtable;}
  // Switching functions
  void        SetTimeout(Time_t);
  int         FindSRCPort(Interface*);
  void        PacketRX(Packet*, Interface*);
  MACPort_t*  GetPortMap(){ return &portmap; }
  Time_t      GetTimeout(){ return timeout; }
  bool        IsSwitchNode() { return isswitch; }  // True if node is a switch
  void        IsSwitchNode(bool);

  // worm containment
  void SetWormContainment(WormContainment*);

  WormContainment* GetWormContainment() { return wormcontainment; } 

  void UseARP(bool);

private:
  bool        CheckTrace(Protocol*, PDU*);         // Check if trace enabled
#ifdef HAVE_QT
  bool        CustomShapeHelper(QPixmap&);
#endif
  // Data Members
private:
  Routing*   pRouting;    // Points to Routing Object for this node
  //RENixMap_t nixCache;    // Cache for nix lookups
  RENixVec_t nixLookup;   // Cache for nix lookups

  //specially for AODV
  RoutingAODV*  pRoutingAODV;

  Meters_t x;             // x location
  Meters_t y;             // y location
  Meters_t z;             // z location
  bool     hasLocation;   // True if location assigned
#ifndef COMPACT
  ProtocolGraph* graph;   // Private protocol graph (if not null)
#endif
  Trace::TraceStatus traceStatus; // Trace level for this node
  Mobility* mobility;     // Current mobility model
public:
  void*       userInfo;           // User specified  information

  PortDemux*       demux;           // Port Demultiplexer map
  Meters_t         radioRange;      // this belongs in intefaces, not nodes
  Joules_t         battery;         // in case node is powered by battery
  double           computePower;    // CPU power cocnsumption
  RadioVec_t       radioInterfaces; // List of nodes in range
  RadioVec_t       radioInf2;       // List of nodes in range potentially
  Time_t           radioMarginTime;
  PacketCallbacks  callbacks;       // Packet callbacks for this node

#ifdef HAVE_QT  
  CanvasItemDeq_t  canvasItems;     // The circles for animating  wireless tx
  NodeAnimation*   animInfo;  
  QColor           wirelessColor;   // Color for wireless tx animation
#endif
  
  bool             icmpEnabled;     // True if ICMP enabled here, false if not

  RouteTable*      rtable;          // the routing table object
  
private:
  // switch members
  MACPort_t        portmap;
  Time_t           timeout;
  bool             isswitch;
  WormContainment* wormcontainment;

public:
  static Count_t count;    // debug
  static Count_t count1;   // debug
  static Count_t count2;   // debug
  static Count_t count3;   // debug
  static Count_t count4;   // debug
};

//#include "interface.h"

#endif
