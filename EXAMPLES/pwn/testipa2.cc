// Test the Two-Stage IPA Derivatives and Adjustments
// George F. Riley.  Georgia Tech, Spring 2002

#include "simulator.h"
#include "application-onoff.h"
#include "duplexlink.h"
#include "queue.h"
#include "ratetimeparse.h"
#include "udp.h"
#include "star.h"
#include "droptail-ipa.h"
#include "validation.h"  

using namespace std;

Time_t adjustInterval = Time("10sec");
Mult_t wL = 512;     // Weight for lossts
Mult_t wQ = 1;       // Weight for queuing delay
Mult_t gammaMult = 10.0; // GammaMult for gradient multiplier

class MyTimerEvent : public TimerEvent {
public:
  MyTimerEvent(DropTailIPA* q1, DropTailIPA* q2) 
    : TimerEvent(), queue1(q1), queue2(q2) { };
public:
  DropTailIPA* queue1;
  DropTailIPA* queue2;
};
  
class MyTimer : public Timer {
public:
  virtual void Timeout(TimerEvent*);  // Called when timer expires
};

void MyTimer::Timeout(TimerEvent* ev)
{
  Time_t now = Simulator::Now();
  MyTimerEvent* e = (MyTimerEvent*)ev;
  cout << "Hello from MyTimer, time " << now << endl;
  Schedule(e, adjustInterval);
}

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  int qLimit = 10;
  if (argc > 1) qLimit = atol(argv[1]);
  if (argc > 2) adjustInterval = atof(argv[2]);
  if (argc > 3) wL = atof(argv[3]);
  if (argc > 4) wQ = atof(argv[4]);
  if (argc > 5) gammaMult = atof(argv[5]);

  int nSource = 20;
  Time_t duration = Time("100ms");
  Mult_t dutyCycle = 0.5;  // On half, off half (on average)
  Mult_t rho = 0.9;        // Traffic intensity
  Rate_t bw = Rate("1Mb"); // Bottleneck link rate
  Rate_t cbrRate = (bw * rho) / (nSource * dutyCycle);
  Size_t pktSize = 512;
  Linkp2p l1;
  l1.Bandwidth(bw);
  Linkp2p l2;
  l2.Bandwidth(bw * 2);
  Simulator s;
  Queue::Default(DropTailIPA(qLimit*(pktSize+28))); // Use IPA Queue by default
  cout << "qlim " << qLimit << " pktsize " << pktSize << endl;
  Trace* gs = Trace::Instance();
  gs->Open("testipa2.txt");
  gs->IPDotted(true);
  IPV4::Instance()->SetTrace(Trace::ENABLED);

  // Create the router nodes
  Node* n1 = new Node();
  Node* n2 = new Node();
  Node* n3 = new Node();
  Node* n4 = new Node(IPAddr("192.3.0.1"));
  Node* n5 = new Node(IPAddr("192.4.0.1"));
  n1->AddDuplexLink(n2, l1);
  n2->AddDuplexLink(n3, l2);
  n4->AddDuplexLink(n2, l2);
  n3->AddDuplexLink(n5, l2);
  Star l(nSource, n1, l1, IPAddr("192.1.0.1"));
  Star r(nSource, n3, l1, IPAddr("192.2.0.1"));
  DropTailIPA* q1 = (DropTailIPA*)n1->GetQueue(n2);
  DropTailIPA* q2 = (DropTailIPA*)n2->GetQueue(n3);
  q1->Stage2(q2);
  q2->Stage1(q1);
  MyTimer t;
  t.Schedule(new MyTimerEvent(q1, q2), adjustInterval);

  // Add the competing On/Off data source from n4 to n5
  // which takes up half of b/w from stage 2 link
  OnOffApplication* ooabk = 
           (OnOffApplication*)n4->AddApplication(
                              OnOffApplication( n5->GetIPAddr(), 1000, 
                              Exponential(duration * dutyCycle), // On period
                              Exponential(duration * (1 - dutyCycle)), // Off
                              UDP(), bw * 2, pktSize));
  ooabk->Start(0.0); // Start at time 0
  ooabk->Stop(100.0);
  for (Count_t i = 0; i < l.LeafCount(); ++i)
    { // Add an exponential On/Off source for each leaf
      OnOffApplication* ooa = 
           (OnOffApplication*)l.GetLeaf(i)->AddApplication( 
                              OnOffApplication(r.GetLeaf(i)->GetIPAddr(), 1000,
                              Exponential(duration * dutyCycle), // On period
                              Exponential(duration * (1 - dutyCycle)), // Off
                              UDP(), cbrRate, pktSize));
      ooa->Start(0.0); // Start at time 0
      ooa->Stop(100.0);
    }
  s.StopAt(100);
  s.Progress(1);
  s.Run();
  //Stats::Print(); // Print the statistics
  cout << "Q1 Limit " << qLimit 
       << " total Losses " << q1->Losses()
       << " dL1dB1 " << q1->DL1dB1() 
       << " prediction " << q1->Losses() - q1->DL1dB1() << endl;
  cout << "           "
       << " workload " << q1->TotalWorkload()/(pktSize+28)
       << " dQ1dB1 " << q1->DQ1dB1() 
       << " prediction " << q1->TotalWorkload()/(pktSize+28) + q1->DQ1dB1()
       << endl;
  cout << "Bottleneck Link1 Utilization " << 
    n1->GetLink(n2)->Utilization() << endl;
  cout << "Bottleneck Link2 Utilization " << 
    n2->GetLink(n3)->Utilization() << endl;
}

  
