// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-webserver.h 185 2004-11-05 16:40:27Z riley $



// Georgia Tech Network Simulator - Web Server Application class
// George F. Riley.  Georgia Tech, Spring 2003

// Define a Web Server Application Class
// Responds to HTTP-style web GET commands.  

#ifndef __webserver_h__
#define __webserver_h__

#include <map>
#include <deque>
#include <vector>

#include "common-defs.h"
#include "application-tcp.h"
#include "statistics.h"
#include "tcp.h"
#include "rng.h"

// Define a class to track child TCP processes
//Doc:ClassXRef
class HTTPInfo {
public:
  HTTPInfo() : totSent(0), totAck(0), totRx(0), stats(nil),
               msgStart(0), timeKnown(false) { }
public:
  Count_t       totSent;      // Total bytes sent
  Count_t       totAck;       // Total bytes acknowledged
  Count_t       totRx;        // Total bytes received
  Statistics*   stats;        // Stats object for logging response time
  Time_t        msgStart;     // Time message request started
  bool          timeKnown;    // True if above time is known
};

// Keep track of open connections using protocol pointer -> info map
typedef std::map<L4Protocol*, HTTPInfo> L4Map_t;

// Class TimeCount is  used to log connection table size vs. time
class TimeCount {
public:
  TimeCount(Time_t t, Count_t s) : time(t), count(s) { };
  TimeCount(const TimeCount& c) : time(c.time), count(c.count) { }
public:
  Time_t   time;
  Count_t  count;
};
typedef std::vector<TimeCount>    TimeCount_t;    // Time/sequence logs

//Doc:ClassXRef
class WebServer : public TCPApplication {
  //Doc:Class Class {\tt WebServer} defines the behavior of a web server
  //Doc:Class application, including the Gnutella {\em GCache} scripts.
public:
  // Constructor
  //Doc:Method
  WebServer(const TCP& = TCP::Default());   // TCP Type
    //Doc:Desc Constructor for {\tt WebServer} object.  Single (optional)
    //Doc:Desc argument is a reference to a {\tt TCP} object to use
    //Doc:Desc for the listening {\tt TCP} endponit.
    //Doc:Arg1 Reference to a {\tt TCP} object to copy for listening endpoint.

  //Doc:Method
  WebServer(const WebServer& c);               // Copy constructor
    //Doc:Desc Copy constructor
    //Doc:Arg1 Object to copy

  virtual ~WebServer();
  // From application.h
  virtual void AttachNode(Node*);              // Note which node attached to
  // Upcalls from L4 protocol
  virtual void Receive(Packet*,L4Protocol*,Seq_t); // Data received
  virtual void Sent(Count_t, L4Protocol*);     // Data has been sent
  virtual void CloseRequest(L4Protocol*);      // Close request from peer
  virtual void Closed(L4Protocol*);            // Connection has closed
  virtual void ConnectionFailed(L4Protocol*,bool);// Connection request failed
  // Listener has cn rq from peer 
  virtual bool ConnectionFromPeer(L4Protocol*, IPAddr_t, PortId_t);
  virtual Application* Copy() const;           // Make a copy of application
  // Specific to WebServer objects
  //Doc:Method
  void    EnableGCache(bool egc = true);       // Enable Gnutalla Cache
    //Doc:Desc Specify enabling or disabling of the Gnutella GCache
    //Doc:Desc processing at this web server.
    //Doc:Arg1 True if GCache processing enabled.

  //Doc:Method
  void    EnableLog(bool el = true);           // Enable Time/Count log
    //Doc:Desc Specify enabling or disabling of the connection count versus
    //Doc:Desc time history logging.
    //Doc:Arg1 True if count vs. time logging desired.

  //Doc:Method
  void    MaxConnections(Count_t m);           // Limit number of connections
    //Doc:Desc Specify a limit on the maximum number of simulataneous
    //Doc:Desc connections that can be open for this server.
    //Doc:Arg1 Connection limit.

  //Doc:Method
  void    PrintTimeCount(std::ostream&, char sep = ' ' ); // Print the T/C
    //Doc:Desc Print the count versus time history log on an output stream.
    //Doc:Arg1 Output stream on which to log the count versus time history.
    //Doc:Arg2 Separator character to use.

  Node*   AttachedNode() { return node;}       // Return the node
  // GCache methods
  //Doc:Method
  void    MaxIP(Count_t m);                    // Limit number of IP's
    //Doc:Desc Specify the maximum number of \IPA{s} to log in the GCache.
    //Doc:Desc If more than this limit are posted, the earlier ones roll off.
    //Doc:Arg1 Limit on the number of posted \IPA{s}

  //Doc:Method
  void    MaxURL(Count_t m);                   // Limit number of URL's
    //Doc:Desc Specify the maximum number of URLs to log in the GCache.
    //Doc:Desc If more than this limit are posted, the earlier ones roll off.
    //Doc:Arg1 Limit on the number of posted URLs

  // Private methods
  void ProcessHTTPRequest(L4Map_t::iterator, Packet*, L4Protocol*);
  void SendHostFile(L4Map_t::iterator, L4Protocol*);
  void SendURLFile(L4Map_t::iterator, L4Protocol*);
  void LogTimeCount();                         // Log the time and count
  // Static Methods
  //Doc:Method
  static void  Port(PortId_t p) { port = p;};  // Allow non-standard port
    //Doc:Desc Specify a non--default port to use for newly created 
    //Doc:Desc {\tt WebServer} objects.
    //Doc:Arg1 Port number.

  //Doc:Method
  static void  DefaultMaxConnections(Count_t m); // Set maximum connections
    //Doc:Desc Specify a default limit on the maximum number of simulataneous
    //Doc:Desc connections for newly created  {\tt WebServer} objects.
    //Doc:Arg1 Connection limit.

  //Doc:Method
  static void  DefaultMaxIP(Count_t m);        // Set default IP limit
    //Doc:Desc Specify a default limit on the number of GCache posted \IPA{s}
    //Doc:Desc for newly created  {\tt WebServer} objects.
    //Doc:Arg1 GCache {IPA} limit.

  //Doc:Method
  static void  DefaultMaxURL(Count_t m);       // Set default URL Limit
    //Doc:Desc Specify a default limit on the number of GCache posted URLs
    //Doc:Desc for newly created  {\tt WebServer} objects.
    //Doc:Arg1 GCache URL limit.

private:
  Node*              node;                     // Attached node
  bool               gCache;                   // True if GCache enabled
  bool               logEnabled;               // Enable log of Time/Count
  Count_t            maxConnections;           // Max number concurrent conn.
  L4Map_t            activeConnections;        // Map of current connections
  TimeCount_t        timeCountLog;             // Vector of time/count entries
  // GCache variables
  std::deque<IPPort_t> gcIPAddrs;              // Deque of ip/ports
  Count_t            maxIP;                    // Maximum number of IP's
  Count_t            maxURL;                   // Maximum number of URL's
  // Static members
  static PortId_t    port;                     // Port number to bind to
  static Count_t     defaultMaxConnections;    // Default Max connections
  static Count_t     defaultMaxIP;             // Default Max IP Count
  static Count_t     defaultMaxURL;            // Default Max URL Count
};

#endif

