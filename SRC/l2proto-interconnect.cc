#include "l2proto-interconnect.h"

#include <iostream>

using namespace std;

L2ProtoInterconnect::L2ProtoInterconnect()
    : L2Proto802_3()
{
}

void L2ProtoInterconnect::DataRequest(Packet* p)
{
  // Code later
}

void L2ProtoInterconnect::DataIndication(Packet* p, LinkEvent* le)
{ // from lower layer
  L2Proto802_3::DataIndication(p, le);
}

L2ProtoInterconnect* L2ProtoInterconnect::Copy() const
{
  return new L2ProtoInterconnect(*this);
}

// Header methods

L2SCHeader::L2SCHeader(const GridLocation& s, const GridLocation& d)
    : src(s), dst(d)
{ // Nothing else needed
}

// Construct from serialized buffer
L2SCHeader::L2SCHeader(char*, Size_t&, Packet*)
{
  // code later
}

Size_t  L2SCHeader::Size() const
{
  return 14;
}

PDU*    L2SCHeader::Copy() const
{ 
  return new L2SCHeader(*this);
}

Layer_t L2SCHeader::Layer()
{
  return 2;
}

// Trace the contents of this pdu 
void    L2SCHeader::Trace(Tfstream& tos, Bitmap_t b,
                          Packet*, const char*)
{
  // Code later
}

// Serialization
Size_t L2SCHeader::SSize()                   // Size needed for serialization
{
  return 0;
  // code later
}

char*  L2SCHeader::Serialize(char*, Size_t&) // Serialize to a buffer
{
  return nil;
}

char*  L2SCHeader::Construct(char*, Size_t&) // Construct from buffer
{
  return nil;
}


  
