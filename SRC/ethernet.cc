// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: ethernet.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - Ethernet LAN class
// George F. Riley.  Georgia Tech, Spring 2002

// Define the Ethernet LAN Class

#include <iostream>

#include "debug.h"
#include "ethernet.h"
#include "node.h"
#include "queue.h"

using namespace std;

Ethernet::Ethernet(Count_t n,               // Node count
                   IPAddr_t i, Mask_t m,    // IP addr start, mask
                   SystemId_t id,           // Responsible system
                   Rate_t r, Time_t dly, Detail_t d)
  : LinkReal(r, dly), gw(nil), gwIf(nil),
    receiveOwnBroadcast(false), size(n), l4proto(nil)
{
  ConstructorHelper(n, nil, i, m, id, nil, r, dly, d);
}

Ethernet::Ethernet(Count_t n,             // Node count
                   IPAddr_t i, Mask_t m,  // IP addr start, mask
                   const L4Protocol& l4,  // L4 protocol to bind to each
                   SystemId_t id,         // Responsible system
                   Rate_t r, Time_t dly, Detail_t d)
  : LinkReal(r, dly), gw(nil), gwIf(nil),
    receiveOwnBroadcast(false), size(n), l4proto(l4.Copy())
{
  ConstructorHelper(n, nil, i, m, id, l4.Copy(), r, dly, d);
}

Ethernet::Ethernet(Count_t n, Node* g,    // Node count, gateway node
                   IPAddr_t i, Mask_t m,  // IP addr start, mask
                   SystemId_t id,         // Responsible system
                   Rate_t r, Time_t dly, Detail_t d)
  : LinkReal(r, dly), gw(g), gwIf(nil),
    receiveOwnBroadcast(false), size(n), l4proto(nil)
{
  ConstructorHelper(n, g, i, m, id, nil, r, dly, d);
}

Ethernet::Ethernet(Count_t n, Node* g,    // Node count, gateway node
                   IPAddr_t i, Mask_t m,  // IP addr start, mask
                   const L4Protocol& l4,  // L4 protocol to bind to each
                   SystemId_t id,         // Responsible system
                   Rate_t r, Time_t dly, Detail_t d)
  : LinkReal(r, dly), gw(g), gwIf(nil),
    receiveOwnBroadcast(false), size(n), l4proto(l4.Copy())
{
  ConstructorHelper(n, g, i, m, id, l4.Copy(), r, dly, d);
}

Ethernet::~Ethernet()
{ // Destructor, nothing to do
}

// Link methods
Link* Ethernet::Copy() const
{
  return new Ethernet(*this);
}

InterfaceBasic* Ethernet::GetPeer(Packet* p)
{
  if (p)
    { // Packet is specified, peek and find the neighbor
      L2802_3Header* l2pdu = (L2802_3Header*)p->PeekPDU();
      return GetIfByMac(l2pdu->dst);  
    }
  // No packet?
  return nil;
}

InterfaceBasic* Ethernet::GetPeer(Count_t i)
{ // Get the i'th peer's interface
  return GetIf(i);
}

void  Ethernet::SetPeer(InterfaceBasic* ifcb) 
{
  Interface* ifc = (Interface*)ifcb;
  Node* n = ifc->GetNode();
  if (NodeIsPeer(n))
    {
      cout << "Associated node is already peer " << endl;
      return;
    }
  nodes.push_back(n);
  size++;
  ifc->SetIPAddr(nextIP);
  nextIP++;
  ifc->SetLink(this);          // Uses this link
  Queue* q = ifc->GetQueue();
  if (q)
    {
      q->Detailed(true);  // Can't use non-detailed q
    }
  if (l4proto)
    { // Layer 4 specified
      PortId_t port = l4proto->localPort;  // Port to use
      L4Protocol* p = l4proto->Copy();     // Get a copy of this protocol
      p->Attach(n);                // Set attached node
      p->Bind(port);                  // Bind to specified port
    }

  
  // Sending update node events to all nodes connected to this link 
  if(detail == FULL)
    {
      for (Count_t j = 0; j < size; ++j)
	{ 	  
	  Interface* ifc = GetIf(j);
	  EthernetEvent* upd = new EthernetEvent(EthernetEvent::UPDATE_NODES);
	  Scheduler::Schedule(upd, 0.0, ifc);
	}
    }    
}

Count_t  Ethernet::Size()
{ // Return number of peers
  return size; // subtract one since not self peer
}

Count_t  Ethernet::PeerCount()
{ // Return number of peers
  return size - 1; // subtract one since not self peer
}

bool     Ethernet::NodeIsPeer(Node* n)
{ // Return true of node is a peer
  if (n == gw) return true; // Definitely true if gateway
  NodeId_t id = n->Id();
  if(id >= first && id < last) return true; // True if in range

  for(NodeVec_t::size_type  i = 0; i < nodes.size(); ++i)
    {
      if( id == nodes[i]->Id())
	{
	  return true;
	}
    }
    
  return false; 
}

Count_t  Ethernet::NodePeerIndex(Node* n)
{ // Return the peer index.  Assumed to be in range
  NodeId_t id = n->Id();
  Count_t extra = gw ? 1 : 0;

  if (n == gw) return 0;  
  if (id >= first && id < last)
    {          
      return id - first + extra;
    }
 
  for(NodeVec_t::size_type  i = 0; i < nodes.size(); ++i)
    {
      if( n->Id() == nodes[i]->Id())
	{
	  return last + extra + i - first;
	}
    } 
  return MAX_COUNT;
}

// IPAddr_t Ethernet::NodePeerIP(Node* n)
// { // Get peer IP Address for the specified node
//   if (n == gw) return firstIP;
//   for (Count_t i = 0; i < PeerCount(); ++i)
//     {
//       Node* peerNode = GetNode(i);
//       if (peerNode == n) return GetIP(i); // Found it, return IP Addresss
//     }
//   return IPADDR_NONE;        // Not found, return none
// }

IPAddr_t Ethernet::NodePeerIP(Node* n)
{ // Get peer IP Address for the specified node
  if (n == gw) return firstIP;
  for (Count_t i = 0; i < size; ++i)
    {
      Node* peerNode = GetNode(i);
      if (peerNode == n) return GetIP(i); // Found it, return IP Addresss
    }
  return IPADDR_NONE;        // Not found, return none
}


// MACAddr  Ethernet::IPToMac(IPAddr_t ip)
// { // Convert peer IP to peer MAC
//   Count_t extra = (gw) ? 1 : 0;
//   DEBUG0((cout << "Ethernet::IPToMac, ip " << (string)IPAddr(ip)
//           << " firstIP " << (string)IPAddr(firstIP)
//           << " lastIP " << (string)IPAddr(firstIP + (last - first + extra))
//           << endl));
//   if (ip < firstIP)
//     {
//       return MACAddr::NONE; // Out of range
//     }
//   if (ip >= (firstIP + (last - first + extra)))
//     {
//       return MACAddr::NONE;
//     }
//   Count_t delta = ip - firstIP;
//   return firstMac + delta;
// }

MACAddr  Ethernet::IPToMac(IPAddr_t ip)
{ // Convert peer IP to peer MAC
  Count_t extra = (gw) ? 1 : 0;
  DEBUG0((cout << "Ethernet::IPToMac, ip " << (string)IPAddr(ip)
          << " firstIP " << (string)IPAddr(firstIP)
          << " lastIP " << (string)IPAddr(firstIP + (last - first + extra))
          << endl));

  if (ip >= firstIP && ip < (firstIP + (last - first + extra)))
    {
      Count_t delta = ip - firstIP;
      return MACAddr(firstMac + delta);
    }

  for(NodeVec_t::size_type  j = 0; j < nodes.size(); ++j)
    {
      if(ip == nodes[j]->GetIfByLink(this)->GetIPAddr())
	{
	  return nodes[j]->GetIfByLink(this)->GetMACAddr();
	}
    } 
  return MACAddr::NONE;
}


// Ethernet Methods
// Node* Ethernet::GetNode(Count_t i)
// { // Get the i'th node
//   Count_t extra = (gw) ? 1 : 0;
//   if (gw && i == 0) return gw;         // Gateway is zero'th
//   if ((first + i) >= (last+extra)) return nil; // Out of range
//   return Node::GetNode(first + i - extra);     // Return the specified node
// }

Node* Ethernet::GetNode(Count_t i)
{ // Get the i'th node
  Count_t extra = (gw) ? 1 : 0;
  if (gw && i == 0) return gw;         // Gateway is zero'th
  if ((first + i) < (last + extra))  // Original range 
    {
      return Node::GetNode(first + i - extra);     // Return the specified node
    }
  //cout << "Node not in original range " << endl;
  //cout << "index " << i << " last " << last << " size " << size << endl;

  // dynamic added nodes
  if( (i - (last - first + extra)) <= nodes.size() )
    {
      //cout << "Return node index " << i - last - extra + first << endl; 
      return nodes[i - (last - first + extra) - 1];
    }
      
  return nil;
}

// IPAddr_t   Ethernet::GetIP(Count_t i)
// { // Get the i'th ip address
//   Count_t extra = (gw) ? 1 : 0;
//   if ((first + i) >= (last+extra)) return IPADDR_NONE; // Out of range
//   return (IPAddr_t)(firstIP + i);
// }

IPAddr_t   Ethernet::GetIP(Count_t i)
{ // Get the i'th ip address
  Count_t extra = (gw) ? 1 : 0;
  if ((first + i) < (last+extra)) // Original range
    return (IPAddr_t)(firstIP + i);

  if( (i - (last - first + extra)) <= nodes.size() )
    return nodes[i - (last - first + extra) - 1]->GetIfByLink(this)->GetIPAddr();

  return IPADDR_NONE;
}

// Interface* Ethernet::GetIfByMac(MACAddr m)
// { // Get the interface corresponding to the specified MAC address
//   Node* n = GetNode(m - firstMac);    // First get the node
//   if (n) return n->GetIfByLink(this); // Find the interface for this MAC
//   return nil;                         // Out of range
// }

Interface* Ethernet::GetIfByMac(MACAddr m)
{ // Get the interface corresponding to the specified MAC address
  MACAddr_t extra = 0;
  MACAddr_t ma = m.macAddr;
  if(gw)
    {
      extra = 1;
      if (ma == firstMac)
	return gwIf;
    }
  if(((ma - firstMac) >= 0) &&
     (ma - firstMac) <= (last - first + extra))
    {
      Node* n = GetNode(ma - firstMac);    // First get the node
      if (n) return n->GetIfByLink(this); // Find the interface for this MAC
    }
  
  for(NodeVec_t::size_type j = 0; j < nodes.size(); ++j)
    {
      Interface* iface =  nodes[j]->GetIfByLink(this);
      if(m ==iface->GetMACAddr())
	{
	  return iface;
	}
    } 

  return nil;                         // Out of range
}

void  Ethernet::AddNode(Node* n)
{
  if(NodeIsPeer(n))
    {
      cout << "N" << n->Id() << " is already in list " << endl;
      return;
    }
  nodes.push_back(n);
  size++;

  // Add interface
  Interface* iface;

  if(detail == FULL)
    {	   
      iface = n->AddInterface(L2Proto802_3(), InterfaceEthernet(), nextIP, mask );
    }
  else
    {
      iface = n->AddInterface(L2Proto802_3(), nextIP, mask);
    }
  nextIP++;
  iface->SetLink(this);          // Uses this link
  Queue* q = iface->GetQueue();
  if (q)
    {
      q->Detailed(true);  // Can't use non-detailed q
    }
  if (l4proto)
    { // Layer 4 specified
      PortId_t port = l4proto->localPort;  // Port to use
      L4Protocol* p = l4proto->Copy();     // Get a copy of this protocol
      p->Attach(n);                // Set attached node
      p->Bind(port);                  // Bind to specified port
    }
  
  // Sending update node events to all nodes connected to this link 
  if(detail == FULL)
    {
      for (Count_t j = 0; j < size; ++j)
	{ 	  
	  Interface* ifc = GetIf(j);
	  EthernetEvent* upd = new EthernetEvent(EthernetEvent::UPDATE_NODES);
	  Scheduler::Schedule(upd, 0.0, ifc);
	}
    }    
}


// Private methods
void Ethernet::ConstructorHelper(
           Count_t n,               // Node count
           Node*   g,               // Gateway node (nil if none)
           IPAddr_t i, Mask_t mask, // IP addr start, mask
           SystemId_t id,
           L4Protocol* l4,          // L4 protocol to bind to each
           Rate_t r, Time_t dly, Detail_t d)
{
  DEBUG0((cout << "Creating Ethernet, firstIP " << (string)IPAddr(i)
       << " nnodes " << n << " detail "<< d << endl));
  first = Node::nextId;             // Set the address of first node
  firstMac = MACAddr::next;         // First MAC address
  firstIP = i;                      // First IP Address
  detail = d;                       // Detail level
  bw = r;                           // Bandwidth
  delay = dly;                      // Delay

  if (g)
    { // Gateway specified, add the interface
      size++;
      if(detail == FULL)
	{	    
	  gwIf = g->AddInterface(L2Proto802_3(), InterfaceEthernet(), i, mask );  
	  g->SetLocation(0, 0);
	}
      else
	{
	  gwIf = g->AddInterface(L2Proto802_3(), i, mask);
	}
      gwIf->SetLink(this);           // Uses this link
      Queue* q = gwIf->GetQueue();
      if (q) 
        {
          q->Detailed(true);  // Can't use non-detailed q
        }
      i++;                          // Advance the ip address
    }
  for (Count_t j = 0; j < n; ++j)
    { // Create the nodes
      Node* node = new Node(id);
      Interface* iface;

      if(detail == FULL)
	{	   
	  iface = node->AddInterface(L2Proto802_3(), InterfaceEthernet(), i, mask );
	  node->SetLocation(0, j + 1 );
	}
      else
	{
	  iface = node->AddInterface(L2Proto802_3(), i, mask);
	}
      iface->SetLink(this);          // Uses this link
      Queue* q = iface->GetQueue();
      if (q)
        {
          q->Detailed(true);  // Can't use non-detailed q
        }
      if (l4)
        { // Layer 4 specified
          PortId_t port = l4->localPort;  // Port to use
          L4Protocol* p = l4->Copy();     // Get a copy of this protocol
          p->Attach(node);                // Set attached node
          p->Bind(port);                  // Bind to specified port
        }
      ++i;                          // Advance IP Address
    }
  last = Node::nextId;              // Last id + 1
  nextIP = i;
  // Sending update node events to all nodes connected to this link
  if(detail == FULL)
    {
      UseSeqEvents(false);
      for (Count_t j = 0; j < size; ++j)
	{ 	  
	  Interface* iface = GetIf(j);
	  EthernetEvent* upd = new EthernetEvent(EthernetEvent::UPDATE_NODES);
	  Scheduler::Schedule(upd, 0.0, iface);
	}
    }    
} 


bool Ethernet::Transmit(Packet* p, Interface* i, Node* n)
{
  L2802_3Header* l2pdu = (L2802_3Header*)p->PeekPDU(); // Get a ptr to l2 hdr
  Interface* targ_if = GetIfByMac(l2pdu->src);
  if (l2pdu->dst.IsBroadcast())
    { // Broadcast, pass interface of source, not dst
      return TransmitHelper(p, i, n, targ_if, true);
    }
  else
    {
      Interface* targ_if = (Interface*)GetPeer(p);
      if (!targ_if)
        {
          cout << "HuH?  Ethernet::Transmit null peer i/f" << endl;
          L2802_3Header* l2pdu = (L2802_3Header*)p->PeekPDU();
          cout << "Mac dst " << l2pdu->dst.macAddr << endl;
          IPV4Header* l3pdu = (IPV4Header*)p->PeekPDU(2);
          cout << "Src IP "  << (string)IPAddr(l3pdu->src)
               << " dst IP " << (string)IPAddr(l3pdu->dst) << endl;
          return false;
        }
      return TransmitHelper(p, i, n, targ_if);
    }
}

bool Ethernet::Transmit(Packet* p, Interface* i, Node* n, Rate_t)
{
  return Transmit(p, i, n);
}


IPAddr_t Ethernet::PeerIP(Count_t npeer)
{ // Get peer IP Address (if known)
  Node* n = GetNode(npeer);  // Get the node    
  if (n) return n->GetIPAddr(); // get the node's ip address
  return IPADDR_NONE;        // Problem, return none
}


IPAddr_t Ethernet::DefaultPeer(Interface* fromIf)
{ // Get IP Addr of default peer
  if (gw)
    {
      IPMaskVec_t ipmasks;
      gw->IPAddrs(ipmasks); // Get the list of ipaddresses for gateway
      // Find the one matching my subnet
      IPAddr_t ip = fromIf->GetIPAddr();
      DEBUG0((cout << "Ethernet::DefaultPeer, fromIF " 
              << (string)IPAddr(ip) << endl));
      
      for (IPMaskVec_t::size_type i = 0; i < ipmasks.size(); ++i)
        {
          DEBUG0((cout << "Ethernet::DefaultPeer, checking "
                  << (string)IPAddr(ipmasks[i].ip)
                  << " vs " << (string)IPAddr(ip) << endl));
          IPAddr_t pip = ipmasks[i].ip;
          Mask_t   m   = ipmasks[i].mask;
          if ((ip & m) == (pip & m))
            {
              DEBUG0((cout << "Ethernet::DefaultPeer, returning " 
                      << (string)IPAddr(pip) << endl));
              return pip; // found it
            }
        }
    }
  DEBUG0((cout << "Ethernet::DefaultPeer returning IPADDR_NONE" << endl));
  return IPADDR_NONE;  // No default peer if no gateway
}

Count_t  Ethernet::NeighborCount(Node* n)
{ // Number of routing neighbors
  if (gw)
    {
      if (gw != n)
        {
          return 1;// If a gw system and not the gw, return just one
        }
    }
  return PeerCount(); // Otherwise return number of peers
}

void     Ethernet::Neighbors(NodeWeightVec_t& nwv, Interface* i, bool forceAll)
{ // Add to neighbor list
  // If a gateway system, all leafs just claim the gateway as
  // a single neighbor.  The gateway itself claims no neighbors.
  if (gw && !forceAll)
    {
      Node* n = i->GetNode();
      if (n == gw) return;  // This is the gateway node, no neighbors
      nwv.push_back(NodeIfWeight(gw, i, Weight())); // Otherwise gw is neighbor
      return;
    }
  // Not a single gateway, a bit more complex
  // Add each node in the list.  Note that the list does include
  // all nodes, which means certain route calculations must take
  // care to ignore "self" adjacenies
  for (NodeId_t ni = first; ni < last; ++ni)
    {
       nwv.push_back(NodeIfWeight(Node::GetNode(ni), i, Weight()));
    }
  // adding dynamically added nodes
  for (NodeVec_t::size_type j= 0; j < nodes.size(); ++j)
    {
      nwv.push_back(NodeIfWeight(nodes[j], i, Weight()));
    }

}

void     Ethernet::AllNeighbors(NodeWeightVec_t& nwv)
{ // Make a list of all neighbors.  Note the the "Interface" pointer
  // in the NodeIfWeight vector is the TARGET interface, not the
  // source as in Neighbors above.  This function is used by L2
  // broadcast to determine where to schedule receive events.
  // 
  int extra = 0 ;
  if(gw)
    {
      extra = 1;
      Interface* iface = gwIf;
      if (!iface) cout << "HuH?  Ethernet::AllNeighbors nill iface for " 
                       << gw->Id() << endl;
      nwv.push_back(NodeIfWeight(gw, gwIf, Weight()));
    }

  for (NodeId_t ni = first; ni < last; ++ni)
    {
      Interface* iface = GetIf(ni - first + extra);
      if (!iface) cout << "HuH?  Ethernet::AllNeighbors nill iface for " 
                       << ni << endl;
      nwv.push_back(NodeIfWeight(Node::GetNode(ni), iface, Weight()));
    }

 // adding dynamically added nodes
  for (NodeVec_t::size_type j = 0; j < nodes.size(); ++j)
    {
      Interface* iface = nodes[j]->GetIfByLink(this);

      if (!iface) cout << "HuH?  Ethernet::AllNeighbors nill iface for " 
                       << nodes[j]->Id() << endl;
      nwv.push_back(NodeIfWeight(nodes[j], iface, Weight()));
    }
 }


Interface* Ethernet::GetIf(Count_t i)
{ // Get the interface for the i'th node
  Node* n = GetNode(i);
  if (!n) return nil;
  return n->GetIfByLink(this);
}


Node* Ethernet::Gateway()
{
  return gw; // Return the gateway node
}

void  Ethernet::SetLeafQLimit(Count_t l)
{
  //  Count_t r = last - first;     // Number nodes
  //if (gw) r++;                  // Include gateway
  for (Count_t t = 0; t < size; ++t)
    {
      Node* n = GetNode(t);
      if (n)
        {
          Queue* q = n->GetQueue(); // Get the queue for single interface
          if (q)
            { // Found single interface node, set specified limit
              q->SetLimit(l);
            }
        }
    }
}


bool Ethernet::ScheduleFailure(Time_t t)
{
  for (NodeId_t ni = first; ni < last; ++ni)
    {
      LinkEvent* evFail = new LinkEvent (LinkEvent::LINK_FAIL, nil);
      Interface* iface = GetIf(ni);
      Scheduler::Schedule(evFail, t, iface);
    }
  return true; // ? Why do we need a return value here?
}

bool Ethernet::ScheduleRestore(Time_t t)
{
  for (NodeId_t ni = first; ni < last; ++ni)
    {
      LinkEvent* evFail = new LinkEvent (LinkEvent::LINK_RESTORE, nil);
      Interface* iface = GetIf(ni);
      Scheduler::Schedule(evFail, t, iface);
    }
  return true; // ? Why do we need a return value here?
}

