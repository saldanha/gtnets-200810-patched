// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


// Georgia Tech Network Simulator - Bluetooth Baseband class
// George F. Riley, Georgia Tech, Spring 2004

#ifndef __baseband_h__
#define __baseband_h__

#include <assert.h>

#include "packet.h"
#include "timer.h"
#include "link-real.h"
#include "bluetus.h"


#include "node-blue.h"
#include "node-blue-impl.h"

#include <assert.h>
//
//PLEASE NOTE: don't use sizeof in the case of baseband packet.
//
const double Msec = 1e-3; //msec
const double Usec = 1e-6; //usec
const double PropDelay = 5 * Usec;
const double SLOTTIME = 625.0 * Usec;
const double CLOCKTICK = 312.5 * Usec;
const double NEWCONNECTIONTIMEOUT= 32 * SLOTTIME; //11.25sec
const double PAGETIMEOUT = 8192 * CLOCKTICK; //2.56
const double PAGERSPTIMEOUT = 36 * CLOCKTICK;
const double INQUIRYTIMEOUT = 5.0; 
const double TwPageScan = 36 * CLOCKTICK;
const double TPageScan = 4096 * CLOCKTICK;
const int    Npage = 128;
const int 	 Ninq = 256;

const double TXRXPOWER = 37.6;
const double RXACERRPOWER = 24.2;
const double ACTIVENOPOWER = 20.0;
const double HOLDPOWER = 0.06;
const double IDPOWER = 23.3;
const double POLLNULLPOWER = 26.0;

#define BT_MAXRANGE		10 //meter
//packet type
#define NULL_PKT	0x00
#define POLL_PKT	0x01
#define FHS_PKT		0x02
#define DM1_PKT		0x03
#define DH1_PKT		0x04 //for ACL only
#define HV1_PKT		0x05 //for SCO only
#define HV2_PKT		0x06 //for SCO only
#define HV3_PKT		0x07 //for SCO only
#define DV_PKT		0x08 //for SCO only
#define AUX1_PKT	0x09 //for ACL only
#define DM3_PKT		0x0A //for ACL only
#define DH3_PKT		0x0B //for ACL only
				//  0x0C
				//  0x0D
#define DM5_PKT		0x0E //for ACL only
#define DH5_PKT		0x0F //for ACL only

// There are 64 IACs whose LAPs lie between 0x9e8b00 - 0x9e8b3f
// Reservation addresses for GIAC and DIAC
//
const int LAP_GIAC = 0x9e8b33;
const int LAP_IACLow  = 0x9e8b00; // start of dedicated inquiry access code
const int LAP_IACHigh = 0x9e8b3F; // end of dedicated inquiry access code

class AccessCode72 {
	public:
		AccessCode72() {
			ucPreamble = ucTrailer = 0x00;
			ucSynWord[0] = ucSynWord[1] = ucSynWord[2] = ucSynWord[3]
			= ucSynWord[4] = ucSynWord[5] = ucSynWord[6] = ucSynWord[7]
			=0x00;
		}
		
		AccessCode72(uChar *pSynWord) {
			assert(pSynWord!=NULL);
			//pSynWord must contain 8 bytes.
			uChar ucTemp = 0;
			ucTemp = *pSynWord;
			if((ucTemp & 0x01))
				ucPreamble = 0x05; //0101
			else
				ucPreamble = 0x0A; //1010
			
			ucTemp = *(pSynWord+7);
			if(ucTemp>>7)
				ucTrailer = 0x0A; //1010
			else
				ucTrailer = 0x05; //0101
			memcpy(&ucSynWord,pSynWord, 8);
		}
		
		AccessCode72(const AccessCode72& orig) {
			ucPreamble = orig.ucPreamble;
			memcpy(&ucSynWord, &(orig.ucSynWord),8);
			ucTrailer = orig.ucTrailer;
		}
						
		~AccessCode72() {}

		uChar * GetSynWord() {
			return (uChar *)&ucSynWord;
		}
			
	private:
		uChar ucPreamble:4 ;
		uChar ucSynWord[8] ; //64 bits;
		uChar ucTrailer:4  ; 
};

class AccessCode68 {
	public:
		AccessCode68() {
			ucPreamble = 0x00;
			ucSynWord[0] = ucSynWord[1] = ucSynWord[2] = ucSynWord[3]
			= ucSynWord[4] = ucSynWord[5] = ucSynWord[6] = ucSynWord[7]
			=0x00;
		}
		
		AccessCode68(uChar *pSynWord) {
			assert(pSynWord!=NULL);
			//pSynWord must contain 8 bytes.
			uChar ucTemp = 0;
			ucTemp = *pSynWord ; //the LSB 
			if((ucTemp&0x01))
				ucPreamble = 0x05; //0101
			else
				ucPreamble = 0x0A; //1010
			
			memcpy(&ucSynWord,pSynWord, 8);
		}
		
		AccessCode68(const AccessCode68& orig) {
			ucPreamble = orig.ucPreamble;
			memcpy(&ucSynWord, &(orig.ucSynWord),8);
		}
		
		~AccessCode68() {}
		
		uChar * GetSynWord() {
			return (uChar *)&ucSynWord;
		}
	private:
		uChar ucPreamble:4 ;
		uChar ucSynWord[8] ; //64 bits;
};

/*class*/
struct Header {
		uChar ucAMAddr:3;
		uChar ucType:4;
		uChar ucFlow:1;
		uChar ucArqn:1;
		uChar ucSeqn:1;
		uChar ucHec;
};

//logic channel field
#define LM_CH		0x03
#define L2CAP_CH	0x02 //start L2CAP msg or no fragmentation
#define L2CAP_CON_CH	0x01	//continue fragment of an L2CAP msg

//flow bit
#define STOPFLOW	0x00	// 1 bit used
#define GOFLOW		0x01	// 1 bit used.

//ARQ scheme
#define NAK		0x00
#define ACK		0x01
//single slot payload header.
class PayloadHdr {
	public:
		PayloadHdr(uChar ucL_CH, uChar ucFlag, uChar ucLen, 
						uChar *pData) {
			ucLogicChannel = ucL_CH;
			ucFlow = ucFlag;
			ucLength = ucLen;
			this->pData = pData;
		}
		
		PayloadHdr(const PayloadHdr& hdr) {
			ucLogicChannel = hdr.ucLogicChannel;
			ucFlow = hdr.ucFlow;
			ucLength = hdr.ucLength;
			pData = hdr.pData;
		}
		~PayloadHdr() {
			//don't not delete pData.
		}
			
	public:	
		uChar ucLogicChannel:2;
		uChar ucFlow:1;
		uChar ucLength:5;
		uChar *pData;  
};

//multislot payload header.
class PayloadHdrMultiSlot {
	public:
		PayloadHdrMultiSlot(uChar ucL_CH, uChar ucFlag, uShort usLen,
						uChar *pData) {
			ucLogicChannel = ucL_CH;
			ucFlow = ucFlag;
			ucLengthL = (uChar) (usLen & 0x001F);
			ucLengthH = (uChar) (usLen & 0x01E0) >>5 ;
			ucUnused = 0;
			this->pData = pData;
		}
	public:	
		uChar ucLogicChannel:2;
		uChar ucFlow:1;
		uChar ucLengthL:5;
		uChar ucLengthH:4;
		uChar ucUnused:4;
		uChar *pData; // 
				  
};
//SR mode
#define R0	0x00
#define R1	0x01
#define R2	0x02
//SP mode
#define P0	0x00
#define P1	0x01
#define P2 	0x02
//Page scan mode
#define MANDATORY_MODE	0x00
#define OPTIONAL_MODE1	0x01
#define OPTIONAL_MODE2	0x02
#define OPTIONAL_MODE3	0x03

//the parameters of Inquiry and page.
#define TRAINSIZE	16


struct FHSPayload {
	uLong ulParityBits; //lower 32 bits
	uChar ucCon2Bits:2;    //another higher 2 bits
	uLong ulLAP:24;  // 24 bits LAP
	uChar ucUndef:2; //undefine 2 bits. set to zero
	uChar ucSR:2; // 2 bits, scan repetition field.
	uChar ucSP:2; //2 bits,  scan period
	uChar ucUAP;  // 8 bits UAP
	uShort usNAP; // 16 bits NAP
	uLong ulClassService:24; // 24 bits
	uChar ucAMAddr:3; //AM addresss
	uLong ulClk27_2:26; //CLK27_2
	uChar ucPageScanMode:3;
};

//the common part between IDPacket header and 
//BaseBandPacket header

class CommonHdr : public PDU {
	public:
	CommonHdr() {
	}
	CommonHdr(uChar ucFreq, uShort usLen) {
		this->ucFreq = ucFreq;
		this->usLen = usLen;
				
	}
	virtual Size_t Size() const = 0; 
	public:
	uChar ucFreq;
	uShort usLen;
	
};

class IDPacket : public CommonHdr {
	public:
		IDPacket():CommonHdr(0, 0) {
		}
		IDPacket(const IDPacket &pkt) {
			this->accessCode68 = pkt.accessCode68;
		}
		IDPacket *
		Copy() const {
			IDPacket *pTemp = new IDPacket;//char[sizeof(IDPacket)];
			pTemp->ucFreq = this->ucFreq;
			pTemp->usLen = this->usLen;
			pTemp->accessCode68 = this->accessCode68;
			return pTemp;
		}
		Size_t Size() const {
			return sizeof(ucFreq) + sizeof(usLen) + sizeof(accessCode68);
		}

		//destructor
		~IDPacket() {
		}
	/*
	public: //for simulation convenience only!
		uChar ucFreq;
		uShort usLen;
	*/
	public: //the content.
		AccessCode68 accessCode68;
};
	
class BaseBandPacket : public CommonHdr {
	public:
		
		BaseBandPacket(uChar freq, uShort len, //include accessCode,etc. 
						AccessCode72 code72, Header hdr, 
						uChar *ucPayload) : CommonHdr(freq, len) {
			ucFreq = freq;
			usLen = len;
			accessCode72 = code72;
			Hdr = hdr;
			pPayload = ucPayload; 
		}

		//copy constructor
		BaseBandPacket(const BaseBandPacket &pkt) {
			this->ucFreq = pkt.ucFreq;
			this->usLen = pkt.usLen;
			this->accessCode72 = pkt.accessCode72;
			this->Hdr = pkt.Hdr;
			uShort usTempLen = this->usLen - 
					sizeof(accessCode72) - sizeof(Hdr);
			this->pPayload = new uChar[usTempLen];
			assert(this->pPayload!=NULL);
				
			memcpy(this->pPayload, pkt.pPayload, 
							usTempLen);
		}

		~BaseBandPacket() {
			if(pPayload !=NULL)
				delete []pPayload;
		}

	public:
		BaseBandPacket *
		Copy() const {
			BaseBandPacket *pTemp = new BaseBandPacket(
							this->ucFreq,
							this->usLen,
							this->accessCode72,
							this->Hdr,
							this->pPayload);
			uShort usTempLen = this->usLen - 
					sizeof(accessCode72) - sizeof(Hdr);
			pTemp->pPayload = new uChar[usTempLen];
			memcpy(pTemp->pPayload, this->pPayload, usTempLen);
			return pTemp;
		}
	
		Size_t Size() const {
			return sizeof(ucFreq) + sizeof(usLen) + sizeof(accessCode72)
					+ sizeof(Hdr) + usLen;
		}
	
	public:
		AccessCode72 accessCode72;
		Header Hdr; //link control info
		uChar *pPayload;
};

typedef enum { STANDBY, PAGE, PAGE_SCAN, INQUIRY, INQUIRY_SCAN,
	MASTER_RSP, SLAVE_RSP, INQUIRY_RSP, CONNECTION } StateType; 

typedef enum {PAGE_HOPPING, MASTER_PAGE_RSP_HOPPING, SLAVE_PAGE_RSP_HOPPING, 
		INQUIRY_HOPPING, INQUIRY_RSP_HOPPING, PAGE_SCAN_HOPPING, INQUIRY_SCAN_HOPPING,
		CHANNEL_HOPPING } HoppingSeqType;

struct FIFOBuf {
	BaseBandPacket *pCurrent;
	BaseBandPacket *pNext;
};

struct TXBuf {
	BdAddr addr; //slave addr or remote entity addr.
	FIFOBuf *pTXBuf;
};

typedef std::list<TXBuf *>  TXBufList;

struct PktStruct {
	Time_t TimeStamp;
	BaseBandPacket *pPacket;
};
typedef std::list<PktStruct>  PktQueue;

//Timer events
class BaseBandEvent : public TimerEvent {
	public:
  		// the types of the Baseband timer events
  		typedef enum {CLOCK_TICK, NEW_CONNECTION_TO,
				  PAGE_TO, PAGE_RSP_TO, INQUIRY_TO, 
				  START_RUN, STOP_RUN
  		} BaseBandTimeout_t;
	public:
  		BaseBandEvent(Event_t ev) : TimerEvent(ev) { };
  		virtual ~BaseBandEvent() {};
};

//
//class Baseband.
//
class L2cap;
class LMP;

//store the communication context with different slaves. 
//Used by master only
struct Context {
		//flags to control flow,acknowledge, sequence numbering.
	uChar ucAMAddr; 
	BdAddr SlaveAddr;

	uChar ucFlow; //flag to other side.
	uChar ucArqn;
	uChar ucSeqn;

	uChar ucSeqnOld; //extracted from received pkt, used to compare.
	uChar ucAcked; //Most recent data pkt:1 acked, 0 unacked yet
	uChar ucFlowRevert; // this flag from other side.

	uChar ucRecvFreq; //the frequence that bluetooth device receive at

	uShort usN; //a counter used in Hop selection. shared in different states.
	uChar ucIdNo; //ID packet number sent.
	uShort usTrainSent; //the number of trains sent.
	uChar ucTrainType; 
	uChar ucKoffsetStar; //this is the frozen value of Koffset.
							//Used in master rsp
	BaseBandEvent*	CLOCKTICKTimeout;
	BaseBandEvent* 	NewConnectionTimeout;
	BaseBandEvent* 	PageTimeout;
	BaseBandEvent* 	PageRspTimeout;
	BaseBandEvent*  InquiryTimeout;

	RoleType role;// the role in current piconet.
};

typedef std::vector<Context>	ContextVec_t;

//store the communication context between master and slaves
//or same slave in different piconet.
struct PiconetContext {
	//piconet specific:
	uLong ulClk;  //28 bits master clock
	BdAddr MasterAddr; //master address.

	StateType State; //state of the unit.
	
	//ACL link buffer
	FIFOBuf *pRXBuf; //shared among slaves
	TXBufList TxBufferList; //each slave has an entry.
	
	uLong ulClkN; //28 bits native clock
	uLong ulClkF; //frozen clk.
	uLong ulClkE; //28 bits estimated clock
	
	Context MyContext; //store my context, as master or slave
	char   cActiveList[7]; //when as master, the number of active members.
	BdAddr ActiveAddr[7];
	//store the context as slaves in the piconet in which I am the MASTER
	ContextVec_t SlaveContexts; 
};

typedef std::vector<PiconetContext>	PiconetContextVec_t;

class BaseBand : public Handler, public TimerHandler {
	public:
		BaseBand();
		~BaseBand() {
		}
	public:
		void Start(RoleType myRole, double t);
		void Stop(double t);
		void Reset();
		RoleType GetRole() {
			return role;
		}

		void TestFreq();
		
		BdAddr GetMyAddr() {
			return pNode->GetBdAddr();
		}
		BdAddr GetPeerAddr() {
			if(role == MASTER)
				return SlaveAddr;
			else
				return MasterAddr;
		}
		bool CheckAccessCode(uChar *DstCode, uLong ulLAP);
		void AttachNode(BlueNode *pNodeAttached) {
			if(pNode==NULL)
				pNode = pNodeAttached;
			return;
		}
		void InstallLMP(LMP *pLMPStack);
		void InstallL2CAP(L2cap *pL2CAPStack);
		L2cap* GetL2CAP() {return pL2CAP;}
		LMP* GetLMP() {return pLMP;}

		void Initialize(StateType currentState = STANDBY,
				RoleType myRole = SLAVE); 
		void Inquiry();
		void PageDevice(BdAddr slave);
		void PageDevices();
		void DataIndication(BaseBandPacket *pBaseBandPkt);
		void DataIndication(IDPacket *pIDPacket);
		void DataRequest(uChar ucLogicChannel, uShort usLen, 
						uChar *ucPayload, BdAddr DstAddr);
		void SendPacket(uChar ucL_CH, uShort usLen, uChar *ucPayload,
										BdAddr DstAddr,
						                uChar ucSlot = 1 );
		void Send(AccessCode72 *pAccessCode72, Header *pPktHdr,
				uChar *pPayload, double dTime, uChar ucFreq);
		void SendIDPacket(uLong ulLAP, uChar ucFreq, double dTime=0.0);
		void SendPollPacket(uChar ucAMAddr);
		void SendNullPacket(AccessCode72 *pAccessCode72, 
				Header *pPktHdr, uChar ucFreq);
	
	public:
		void SynWordConstruct(uLong ulLAP, uChar *pSynWord);
		void SynWordConstruct(BdAddr addr, uChar *pSynWord);
		
	public:
		bool isWithinRange(double dX, double dY);
		bool isWithinRange(BlueNode *pNode);

	public:
		Timer timer;
		bool useTimerBuckets;
		bool	bStartPoll;
  		// Timer handle methods
  		virtual void Timeout(TimerEvent*); // Called when timers expire
		void ScheduleTimer(Event_t e, BaseBandEvent*& ev, Time_t t);
		void CancelTimer(BaseBandEvent*& ev, bool delTimer);

		//handler.
		void Handle(Event*, Time_t);	

	public:
		uChar Freq();	//QT color for display

	public:
		void SwitchPiconet();
		bool SwitchContext(uChar ucAddrToSave, uChar ucAddrToActive);
		void SaveContext(uChar ucActiveAddr);
		bool RemoveContext(uChar ucAddr);
		void UpdateNeighborClockE();
		//for sniff mode
		uLong GetClk() {
			return ulClk;
		}

		void CheckPiconetSetup();
		void LMPReadyIndication(void);
		
		//methods for power stat output
		double StatTxPower() {return TxPower;}
		double StatRxPower() {return RxPower;}
		double StatRxAcErrPower() {return RxAcErrPower;}
		double StatActiveNoPower() {return ActiveNoPower;}
		double StatHoldPower() {return HoldPower;}
		
	private:	
		char AllocAmAddr();
		uChar DetermineARQN(BaseBandPacket *pBaseBandPkt);
		uChar RetransmitFilter(uChar ucPktType);
		void DetermineFlowFlag();
		
		void Trigger(Packet *pPacket);
		void UpdateRecvFreq();
		void Transmit(IDPacket *pIDPacket, double dTime);
		void Transmit(BaseBandPacket *pPacket, double dTime);

		void ScheduleRx(LinkEvent *e, Time_t t);
		void ScheduleTx(LinkEvent *e, Time_t t);
		void UpdateInqPageParameter();
		void InitInqPageParameter();

		//Hop selection related routines.
		uChar HopSelection(uLong ulClkFrozen,
						    HoppingSeqType hopSeqType, BdAddr addr);
		void ButterFly(uChar ucCtrl, uChar& ucVar1, uChar& ucVar2);
		uChar Perm5(uChar ucZ, uChar ucCY1, uShort usD);
		uShort GetBits(uShort usVar, uChar ucStartBit,
						uChar ucHowmany, uChar ucLeftShift = 0);
		
		void CRC16(uShort& usCrc, uChar *ucData, uShort usLen);
		uChar Hec(uChar ucCrc, uChar *ucData, uChar ucLen);
		void Fec23(uChar *ucData, uShort usLen);
		void Fec13(uChar *ucData, uShort usLen);

	private:
		BlueNode *pNode; 
		L2cap *pL2CAP;
		LMP *pLMP;

		//Note: since a unit can involve more than a piconet, the following
		// parameter should be associated with a piconet.
		//
		StateType State; //state of the unit.
		
		//ACL link buffer
		FIFOBuf *pRXBuf; //shared among slaves
		TXBufList TxBufferList; //each slave has an entry.
		
		PktQueue LmpPktSlaveQueue;
		PktQueue L2capPktSlaveQueue;
		PktQueue LmpPktMasterQueue[7];
		PktQueue L2capPktMasterQueue[7];
		PktQueue PollNullPktQueue;
		
		BdAddr MasterAddr; //master address.
		
		//different clock
		uLong ulClkN; //28 bits native clock
		uLong ulClkF; //frozen clk.
		uLong ulClkE; //28 bits estimated clock
		uLong ulClk;  //28 bits master clock

		StateType PreviousState;
		uShort InquiryBackoff; 

		BdAddr SlaveAddr;
		uChar ucFlow; //flag to other side.
		uChar ucArqn;
		uChar ucSeqn;
		uChar ucAMAddr;

		uChar ucSeqnOld; //extracted from received pkt, used to compare.
		uChar ucAcked; //Most recent data pkt:1 acked, 0 unacked yet
		uChar ucFlowRevert; // this flag from other side.

		uChar ucRecvFreq; //the frequence that bluetooth device receive at

		uShort usN; //a counter used in Hop selection. shared in different states.
		uChar ucIdNo; //ID packet number sent.
		uShort usTrainSent; //the number of trains sent.
		uChar ucTrainType; 
		uChar ucKoffsetStar; //this is the frozen value of Koffset.
							//Used in master rsp

		BaseBandEvent*	CLOCKTICKTimeout;
		BaseBandEvent* 	NewConnectionTimeout;
		BaseBandEvent* 	PageTimeout;
		BaseBandEvent* 	PageRspTimeout;
		BaseBandEvent*  InquiryTimeout;

		RoleType role;// the role in current piconet.
		char   cActiveList[7]; //when as master, the number of active members.
		BdAddr ActiveAddr[7];
		bool bPiconetSetupDone;
		char cLMPConnected[7]; //each byte corresponding one node
								//default to '1', when connected set to '0'
		
		AccessCode72 channelAC; //Channel access code
		AccessCode72 DeviceAC; //device access code
		
		Time_t dTotalDelay;	//stat for packet delay for hold mode
		uLong  ulPktCount;	//stat for number of L2CAP packet
		ContextVec_t SlaveContexts; //Piconet contents
		uChar	ucRRQueue;
		bool	bMyTurn;
		uShort	usTpoll[7];
		uShort	usTpollCount[7];
			
		double TxPower;
		double RxPower;
		double RxAcErrPower;
		double ActiveNoPower;
		double HoldPower;
};

#endif
