// Test the distributed simulation using RTIKIT
// George F. Riley.  Georgia Tech, Fall 2002

#include <stdio.h>

#include "distributed-simulator.h"
#include "scheduler.h"
#include "tree.h"
#include "linkp2p.h"
#include "ratetimeparse.h"
#include "tcp-tahoe.h"
#include "application-tcpsend.h"
#include "globalstats.h"
#include "droptail.h"
#include "mask.h"
#include "ipv4.h"
#include "routing-nixvector.h"
#include "validation.h"  

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  if (argc < 3)
    {
      cout << "Usage: testrti mysysid totalsystems (height) (fanout)" << endl;
      exit(0);
    }
  int height = 5;
  int fanout = 2;
  int mySysId = atol(argv[1]);
  int totSys  = atol(argv[2]);
  if (argc > 3) height = atol(argv[3]);
  if (argc > 3) fanout = atol(argv[3]);

  DistributedSimulator s(mySysId);

  Routing::SetRouting(new RoutingNixVector());
  Queue::Default(DropTail()); // Set default to non-detailed drop tail
  Trace* gs = Trace::Instance();
  gs->IPDotted(true);
  char work[20];
  sprintf(work, "%d", mySysId);
  string logfn = string("testrti") + string(work) + string(".txt");
  gs->Open(logfn.c_str());
  TCP::LogFlagsText(true);    // Log flags in text mode
  // Enable all l3 tracing
  IPV4::Instance()->SetTrace(Trace::ENABLED);

  Linkp2p l;
  l.Bandwidth(Rate("100Mb"));
  IPAddr_t baseIp  = IPAddr("0.1.0.1");
  IPAddr_t ipAdder = IPAddr("0.1.0.0"); // Add this value for each system
  int nextSysId = (mySysId + 1) % totSys;
  int priorSysId = mySysId ? mySysId - 1 : totSys - 1;
  IPAddr_t myBaseIp    = baseIp + ipAdder * mySysId;
  IPAddr_t nextBaseIp  = baseIp + ipAdder * nextSysId;
  IPAddr_t priorBaseIp = baseIp + ipAdder * priorSysId;

  Tree t(height, fanout, l, myBaseIp, Mask(32));
  Count_t leafCount = t.LeafCount();
  cout << "LeafCount is " << leafCount << endl;
  for (Count_t i = 0; i < leafCount; ++i)
    { // For each leaf, add a TCP server and a single TCP sender
      Node* n = t.GetLeaf(i);
      // Add a tcp server at each leaf
      TCPTahoe* p = new TCPTahoe(n);
      p->SetTrace(Trace::ENABLED);
      p->Bind(40);
      p->Listen();

      // Create a TCP Sending app at each leaf, connecting to next tree
      TCPSend* app = (TCPSend*)n->AddApplication(
          TCPSend(nextBaseIp + i,
                  40, Constant(100000)));
      app->GetL4()->SetTrace(Trace::ENABLED);
      if (!mySysId) app->Start(1.0 * i + 0.1 * mySysId); // DEBUG! only start 0
    }
  // Add RTI Links from root to all other root nodes
  // The IP addresses are of the form m.0.0.1 or m.0.0.2,
  // where m is the systemId of the larger of the two systems
  // being connected.  If m is mySysId, use .1, otherwise, use .2
  int useSysId = max(mySysId, nextSysId);
  IPAddr_t rlinkIp;
  IPAddr_t rlinkBaseIp = IPAddr("1.0.0.0");
  IPAddr_t rlinkAdder = IPAddr("1.0.0.0");
  Node* root = t.GetRoot();
  cout << "useSysId " << useSysId << " mySysId " << mySysId
       << " nextSysId " << nextSysId << endl;
  if (useSysId == mySysId)
    rlinkIp = rlinkBaseIp + rlinkAdder * useSysId + 1;
  else
    rlinkIp = rlinkBaseIp + rlinkAdder * useSysId + 2;
  Interface* iFace = root->AddRemoteLink(rlinkIp, Mask(24),
                                         Rate("10Mb"), Time("10ms"));
  // Add the remote ip's reachable from this link
  iFace->AddRemoteIP(nextBaseIp, Mask(16));

  if (nextSysId != priorSysId)
    { // Insure not same (only possible with two nodes)
      // Add the rtilink to prior system
      useSysId = max(mySysId, priorSysId);
      cout << "useSysId " << useSysId << " mySysId " << mySysId
           << " priorSysId " << priorSysId << endl;
      if (useSysId == mySysId)
        rlinkIp = rlinkBaseIp + rlinkAdder * useSysId + 1;
      else
        rlinkIp = rlinkBaseIp + rlinkAdder * useSysId + 2;
      iFace = root->AddRemoteLink(rlinkIp, Mask(24),
                                  Rate("10Mb"), Time("10ms"));
      // Add the remote ip's reachable from this link
      iFace->AddRemoteIP(priorBaseIp, Mask(16));
    }
#ifdef VERBOSE_DBG
  // Debug...
  const NodeVec_t nodes = Node::GetNodes();
  for (NodeVec_t::size_type i = 0; i < nodes.size(); ++i)
    {
      cout << "Node id " << nodes[i]->Id()
           << " ipaddr " << (string)IPAddr(nodes[i]->GetIPAddr()) 
           << " nc " << nodes[i]->NeighborCount()
           << endl;
      NodeWeightVec_t nwv;
      nodes[i]->Neighbors(nwv);
      cout << "        "
           << " nwv size " << nwv.size() << endl;
    }
#endif
  s.Progress(1);
  s.StopAt(leafCount + 10.0);
  s.Run();
  //Stats::Print(); // Print the statistics
  cout << "Memory usage after run " 
       << s.ReportMemoryUsageMB() << "MB" << endl;
  cout << "Total events processed "
       << Scheduler::Instance()->TotalEventsProcessed() << endl;
  cout << "Setup time " << s.SetupTime() << endl;
  cout << "Route time " << s.RouteTime() << endl;
  cout << "Run time "   << s.RunTime() << endl;
}
