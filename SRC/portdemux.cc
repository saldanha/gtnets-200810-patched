// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: portdemux.cc 147 2004-10-14 22:21:23Z riley $



// Georgia Tech Network Simulator - Port Demultiplexer
// George F. Riley.  Georgia Tech, Spring 2002

// Implements a map of protocol instances by port number

#include <iostream>

//#define DEBUG_MASK 0x02
#include "debug.h"
#include "portdemux.h"

using namespace std;

// Bindings are by local port, local IP, remote port, remote IP
// Local port can be IPADDR_NONE
// Both remote port, remote IP can be NO_PORT, IPADDR_NONE

// Define some syntactic macros, since the lookup key is a pair of pairs
#define LOCAL_PORT  first.second
#define LOCAL_IP    first.first
#define REMOTE_PORT second.second
#define REMOTE_IP   second.first

PortDemux::PortDemux()
{
}

bool PortDemux::Bind(Proto_t proto, PortId_t port, Protocol* p)
{ // Local port only
  return Bind(proto, port, IPADDR_NONE, p);
}

bool PortDemux::Bind(Proto_t proto, PortId_t port, IPAddr_t ip, Protocol* p)
{ // Local port, local ip
  return Bind(proto, port, ip, NO_PORT, IPADDR_NONE, p);
}

bool PortDemux::Bind(Proto_t proto,
                     PortId_t lport, IPAddr_t laddr,
                     PortId_t rport, IPAddr_t raddr,
                     Protocol* p)
{ // Local port/ip, remote port/ip
  PortDemux_t::iterator i;
  SrcDstPair_t key;
  key.LOCAL_PORT = lport;
  key.LOCAL_IP = laddr;
  key.REMOTE_PORT = rport;
  key.REMOTE_IP = raddr;
  i = protoDemux[proto].find(key);
  if (i != protoDemux[proto].end())
    {
      DEBUG0((cout << "PortDemux::Bind, port " << lport 
              << " already in use" << endl));
      return false; // port already in use
    }
  
  protoDemux[proto].insert(PortDemux_t::value_type(key,p)); // Insert protocol 
  DEBUG0((cout << "After bind lip " << (string)IPAddr(laddr)
          << " lport " << lport
          << " rip " << (string)IPAddr(raddr)
          << " rport " << rport
          << " proto " << p
          << endl));
  DEBUG0((DBDump(protoDemux[proto])));
  return true;
}

PortId_t PortDemux::Bind(Proto_t proto, Protocol* p)
{ // Bind to an available port (port must be > MIN_TRANSIENT)
  NextPort_t::const_iterator i = nextPort.find(proto);
  if (i == nextPort.end())
    { // First lookup, initialize to MIN_TRANSIENT
      nextPort.insert(NextPort_t::value_type(proto, MIN_TRANSIENT));
    }
  PortId_t port = nextPort[proto]++;
  Bind(proto, port, IPADDR_NONE, p);
  return port;
  // Need better..need to bound ports at MAX_PORT
}

bool PortDemux::Rebind(Proto_t proto,
                       PortId_t lport, IPAddr_t laddr,
                       PortId_t rport, IPAddr_t raddr,
                       Protocol* p)
{ // Change to more specific binding
  PortDemux_t::iterator i;
  i = Locate(proto, lport, laddr, rport, raddr);
  if (i == protoDemux[proto].end()) return false; // Not found
  protoDemux[proto].erase(i); // Remove old binding
  return Bind(proto, lport, laddr, rport, raddr, p);
}
                       
Protocol* PortDemux::Lookup(Proto_t proto, PortId_t port)
{
  return Lookup(proto, port, IPADDR_NONE, NO_PORT, IPADDR_NONE);
}

Protocol* PortDemux::Lookup(Proto_t proto,
                             PortId_t lport, IPAddr_t laddr,
                             PortId_t rport, IPAddr_t raddr)
{
  PortDemux_t::iterator i;
  i = Locate(proto, lport, laddr, rport, raddr);
  if (i == protoDemux[proto].end()) return NULL; // Not found
  return i->second;                // Return the protocol pointer
}

bool PortDemux::Unbind(Proto_t proto, PortId_t port, Protocol* p)
{
  return Unbind(proto, port, IPADDR_NONE, NO_PORT, IPADDR_NONE, p);
}


bool PortDemux::Unbind(Proto_t proto,
                        PortId_t lport, IPAddr_t laddr,
                        PortId_t rport, IPAddr_t raddr,
                        Protocol* p)
{
  PortDemux_t::iterator i;
  SrcDstPair_t key;
  key.LOCAL_PORT = lport;
  key.LOCAL_IP = laddr;
  key.REMOTE_PORT = rport;
  key.REMOTE_IP = raddr;
  DEBUG0((DBDump(protoDemux[proto])));
  i = protoDemux[proto].find(key);
  if (i == protoDemux[proto].end())
    {
      DEBUG0((cout << "Can't find unbind for localport " << lport
              << " rport " << rport
              << " raddr " << (string)IPAddr(raddr)
              << endl));
      return false; // Not bound
    }
  
  if (i->second != p)
    {
      DEBUG0((cout << "Protocol mis-match localport " << lport
              << " rport " << rport
              << " raddr " << (string)IPAddr(raddr)
              << " p " << p
              << " second " << i->second
              << endl));
      return false;               // Not bound to this protocol
    }
  
  DEBUG0((cout << "Erasing localport " << lport
          << " rport " << rport
          << " raddr " << (string)IPAddr(raddr)
          << endl));
  protoDemux[proto].erase(i);                     // Remove binding
  DEBUG(1,(cout << "portdemux unbinding" << endl));
  return true;
}

PortDemux_t::iterator PortDemux::Locate(Proto_t proto,
                                        PortId_t lport, IPAddr_t laddr,
                                        PortId_t rport, IPAddr_t raddr)
{ // Locate the binding
  PortDemux_t::iterator i;
  SrcDstPair_t key;
  key.LOCAL_PORT = lport;
  key.LOCAL_IP = laddr;
  key.REMOTE_PORT = rport;
  key.REMOTE_IP = raddr;
  DEBUG0((DBDump(protoDemux[proto])));
  i = protoDemux[proto].find(key);
  if (i != protoDemux[proto].end()) return i; // Found it
  // Try without rport/raddr
  key.REMOTE_PORT = NO_PORT;
  key.REMOTE_IP = IPADDR_NONE;
  i = protoDemux[proto].find(key);
  if (i != protoDemux[proto].end()) return i; // Found it
  // Try without source ip
  key.LOCAL_IP = IPADDR_NONE;
  return protoDemux[proto].find(key);
}

 void      PortDemux::DBDump(PortDemux_t& dm) // debug
{
  PortDemux_t::iterator i;
  cout << "protoDemux size " 
       << dm.size() << endl;
  for (i = dm.begin(); i != dm.end(); ++i)
    {
      cout << "DM srcip " << (string)IPAddr(i->first.first.first)
           << " srdport " << i->first.first.second
           << " dstip " << (string)IPAddr(i->first.second.first)
           << " dstport " << i->first.second.second << endl;
    }

}


