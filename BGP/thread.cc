/****************************************************************************/
/*  File:            thread.cc                                              */
/*  Author:          Sunitha Beeram                                         */
/*  Email:           sunithab@ece.gatech.edu                                */
/*  Documentation:   TBD                                                    */
/*                                                                          */
/*  Version:         1.0beta                                                */
/*                                                                          */
/*  Copyright (c) 2005 Georgia Institute of Technology                      */
/*  This program is free software; you can redistribute it and/or           */
/*  modify it under the terms of the GNU General Public License             */
/*  as published by the Free Software Foundation; either version 2          */
/*  of the License, or (at your option) any later version.                  */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful,         */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*  GNU General Public License for more details.                            */
/*                                                                          */
/****************************************************************************/

/* #define DEBUG */

#include "bgp.h"

/* Struct timeval's tv_usec one second value.  */
#define TIMER_SECOND_MICRO 1000000L

Time_t
BGP::thread_timer_sub(Time_t a, Time_t b)
{
  return a-b;
}

int
BGP::thread_timer_cmp (Time_t a, Time_t b)
{
   if ( a>b)
      return 1;
   if ( a<b)
      return -1;
   return 0;
}
unsigned long
BGP::thread_timer_remain_second (struct thread *thread)
{
  struct timeval timer_now;

  gettimeofday (&timer_now, NULL);

  if (thread->u.time - (double)timer_now.tv_sec > 0)
    return (unsigned long)(thread->u.time - (double) timer_now.tv_sec);
  else
    return 0;
}

/* List allocation and head/tail print out. */
void
BGP::thread_list_debug (struct thread_list *list)
{
  printf ("count [%d] head [%p] tail [%p]\n",
	  list->count, list->head, list->tail);
}

/* Debug print for thread_master. */
void
BGP::thread_master_debug (struct thread_master *m)
{
  printf ("-----------\n");
  printf ("readlist  : ");
  thread_list_debug (&m->read);
  printf ("writelist : ");
  thread_list_debug (&m->write);
  printf ("timerlist : ");
  thread_list_debug (&m->timer);
  printf ("eventlist : ");
  thread_list_debug (&m->event);
  printf ("unuselist : ");
  thread_list_debug (&m->unuse);
  printf ("total alloc: [%ld]\n", m->alloc);
  printf ("-----------\n");
}

/* Allocate new thread master.  */
struct thread_master *
BGP::thread_master_create ()
{
  return (struct thread_master *) XCALLOC (MTYPE_THREAD_MASTER,
					   sizeof (struct thread_master));
}

/* Add a new thread to the list.  */
void
BGP::thread_list_add (struct thread_list *list, struct thread *thread)
{

  thread->next = NULL;
  thread->prev = list->tail;
  if (list->tail)
    list->tail->next = thread;
  else
    list->head = thread;
  list->tail = thread;
  list->count++;
}

/* Add a new thread just before the point.  */
void
BGP::thread_list_add_before (struct thread_list *list, 
			struct thread *point, 
			struct thread *thread)
{
  thread->next = point;
  thread->prev = point->prev;
  if (point->prev)
    point->prev->next = thread;
  else
    list->head = thread;
  point->prev = thread;
  list->count++;
}

/* Delete a thread from the list. */
struct thread *
BGP::thread_list_delete (struct thread_list *list, struct thread *thread)
{
  if (thread->next)
    thread->next->prev = thread->prev;
  else
    list->tail = thread->prev;
  if (thread->prev)
    thread->prev->next = thread->next;
  else
    list->head = thread->next;
  thread->next = thread->prev = NULL;
  list->count--;
  return thread;
}

/* Move thread to unuse list. */
void
BGP::thread_add_unuse (struct thread_master *m, struct thread *thread)
{
  assert (m != NULL);
  assert (thread->next == NULL);
  assert (thread->prev == NULL);
  assert (thread->type == THREAD_UNUSED);
  thread_list_add (&m->unuse, thread);
}

/* Free all unused thread. */
void
BGP::thread_list_free (struct thread_master *m, struct thread_list *list)
{
  struct thread *t;
  struct thread *next;

  for (t = list->head; t; t = next)
    {
      next = t->next;
      XFREE (MTYPE_THREAD, t);
      list->count--;
      m->alloc--;
    }
}

/* Stop thread scheduler. */
void
BGP::thread_master_free (struct thread_master *m)
{
  thread_list_free (m, &m->read);
  thread_list_free (m, &m->write);
  thread_list_free (m, &m->timer);
  thread_list_free (m, &m->event);
  thread_list_free (m, &m->ready);
  thread_list_free (m, &m->unuse);

  XFREE (MTYPE_THREAD_MASTER, m);
}

/* Delete top of the list and return it. */
struct thread *
BGP::thread_trim_head (struct thread_list *list)
{
  if (list->head)
    return thread_list_delete (list, list->head);
  return NULL;
}

/* Thread list is empty or not.  */
int
BGP::thread_empty (struct thread_list *list)
{
  return  list->head ? 0 : 1;
}

struct thread *
BGP::thread_new (struct thread_master *m)
{
    struct thread *New;

    New = ( struct thread* ) XMALLOC (MTYPE_THREAD, sizeof (struct thread));
    bzero (New, sizeof (struct thread));
    m->alloc++;
    return New;
}


/* Get new thread.  */
struct thread *
BGP::thread_get (struct thread_master *m, u_char type,
	    int (BGP::*func) (struct thread *), void *arg)
{
  struct thread *thread;

  if (m->unuse.head)
    thread = thread_trim_head (&m->unuse);
  else
    {
      thread = (struct thread*)XCALLOC (MTYPE_THREAD, sizeof (struct thread));
      m->alloc++;
    }
  thread->type = type;
  thread->master = m;
  thread->func = func;
  thread->arg = arg;
  
  return thread;
}

/* Add new read thread. */
struct thread *
BGP::thread_add_read (struct thread_master *m, 
		 int (BGP::*func) (struct thread *), void *arg, TCP* listenAgent)
{
  struct thread *thread;

  assert (m != NULL);

  thread = thread_new(m);
  thread->type = THREAD_READ;
  thread->master = m;
  thread->arg = arg;
  thread->u.listenAgent = listenAgent;
  thread->func = func;
  thread_list_add (&m->read, thread);

  return thread;
}

/* Add new write thread. */
struct thread *
BGP::thread_add_ready (struct thread_master *m,
		 int (BGP::*func) (struct thread *), void *arg )
{
  struct thread *thread;

  assert (m != NULL);
  
  thread = thread_new(m);
  thread->type = THREAD_READY;
  thread->master=m;
  thread->func = func;
  thread->arg = arg;
  thread->u.listenAgent = NULL;
  thread_list_add (&m->ready, thread);

  return thread;
}

/* Add timer event thread. */
struct thread *
BGP::thread_add_timer (struct thread_master *m,
		  int (BGP::*func) (struct thread *), void *arg, long timer)
{
  Time_t timer_now;
  struct thread *thread;
  struct thread *tt;

  assert (m != NULL);

//  thread = thread_get (m, THREAD_TIMER, func, arg);

  thread = thread_new(m);
  thread->type = THREAD_TIMER;
  thread->master= m;
  thread->func=func;
  thread->arg=arg;

  /* Do we need jitter here? */
  timer_now = Simulator::instance->Now();
  timer_now += timer;
  thread->u.time = (time_t)timer_now;

  /* Sort by timeval. */
  for (tt = m->timer.head; tt; tt = tt->next)
    if (thread_timer_cmp (thread->u.time, tt->u.time) <= 0)
      break;

  if (tt)
    thread_list_add_before (&m->timer, tt, thread);
  else
    thread_list_add (&m->timer, thread);

  return thread;
}

/* Add simple event thread. */
struct thread *
BGP::thread_add_event (struct thread_master *m,
		  int (BGP::*func) (struct thread *), void *arg, int val)
{
  struct thread *thread;

  assert (m != NULL);

  thread = thread_new(m);
  thread->type = THREAD_EVENT;
  thread->master= m;
  thread->func=func;
  thread->arg=arg;
  thread->u.val = val;
  thread_list_add (&m->event, thread);

  return thread;
}

/* Cancel thread from scheduler. */
void
BGP::thread_cancel (struct thread *thread)
{
  switch (thread->type)
    {
    case THREAD_READ:
      thread_list_delete (&thread->master->read, thread);
      break;
    case THREAD_WRITE:
      thread_list_delete (&thread->master->write, thread);
      break;
    case THREAD_TIMER:
      thread_list_delete (&thread->master->timer, thread);
      break;
    case THREAD_EVENT:
      thread_list_delete (&thread->master->event, thread);
      break;
    case THREAD_READY:
      thread_list_delete (&thread->master->ready, thread);
      break;
    default:
      break;
    }
  thread->type = THREAD_UNUSED;

  //Thread is freed on calling thread_call ; I am not sure if freeing here is correct*/

  /*thread->master->alloc--;
  XFREE(MTYPE_THREAD,thread);*/
}

/* Delete all events which has argument value arg. */
void
BGP::thread_cancel_event (struct thread_master *m, void *arg)
{
  struct thread *thread;

  thread = m->event.head;
  while (thread)
    {
      struct thread *t;

      t = thread;
      thread = t->next;

      if (t->arg == arg)
	{
	  thread_list_delete (&m->event, t);
	  t->type = THREAD_UNUSED;
	  thread_add_unuse (m, t);
	}
    }
}

struct thread *
BGP::thread_run (struct thread_master *m, struct thread *thread,
	    struct thread *fetch)
{
  *fetch = *thread;
  thread->type = THREAD_UNUSED;
  thread_add_unuse (m, thread);
  return fetch;
}

/* We check thread consumed time. If the system has getrusage, we'll
   use that to get indepth stats on the performance of the thread.  If
   not - we'll use gettimeofday for some guestimation.  */
void
BGP::thread_call (struct thread *thread)
{

  #ifdef HAVE_PERFCTR
  struct perfctr_sum_ctrs before;
  if (workload_model == TIME_SAMPLE_WORKLOAD_MODEL) do_read(&before);
  #endif /*  HAVE_PERFCTR */

  (this->*thread->func) (thread);
  thread_list_delete(&thread->master->unuse,thread);
  thread->master->alloc--;
  XFREE(MTYPE_THREAD,thread);


  type = TYPE_FETCH1;
  BGPEvent *ev= new BGPEvent(BGPEvent::FETCH1);

#ifdef HAVE_PERFCTR
  timer->Schedule(workload(before),ev); 
#else
  timer->Schedule(0,ev);
#endif
}

/* Execute thread */
struct thread *
BGP::thread_execute (struct thread_master *m,
                int (BGP::*func)(struct thread *), 
                void *arg,
                int val)
{
  struct thread dummy; 

  memset (&dummy, 0, sizeof (struct thread));

  dummy.type = THREAD_EVENT;
  dummy.master = (struct thread_master*)NULL;
  dummy.func = func;
  dummy.arg = arg;
  dummy.u.val = val;
  thread_call (&dummy);

  return (struct thread*)NULL;
}

/* Fetch next ready thread. */
void
BGP::thread_fetch_part1(struct thread_master *m)
{
    struct thread *thread;
    Time_t timer_now;
    Time_t timer_min;
    Time_t timer_wait ;
    BGPEvent *ev;
    
    assert (m != NULL);
    
    if (0) thread_master_debug(m);
    
    /* If there are events process them first. */
    
    while ((thread = thread_trim_head (&m->event)))
    {
        thread->type = THREAD_UNUSED;
        thread_add_unuse (m, thread);
        type = TYPE_EXECUTE;
        ev = new BGPEvent(BGPEvent:: EXECUTE);
        ev->thread_to_be_fetched = thread;
        
        timer->Schedule(0,ev);
        return ;
    }
    
    /* If there are ready threads process them */
    while ((thread = thread_trim_head (&m->ready)))
    {
        
        thread->type = THREAD_UNUSED;
        thread_add_unuse (m, thread);
        type = TYPE_EXECUTE;
        ev = new BGPEvent(BGPEvent:: EXECUTE);
        ev->thread_to_be_fetched = thread;
        timer->Schedule(0,ev);
        return ;
    }
    
    /* Calculate wait timer. */
    
    if (m->timer.head)
    {
        timer_now = Simulator::instance->Now();
        timer_min = thread_timer_sub (m->timer.head->u.time, timer_now);
        
        if (timer_min < 0)
        {
            timer_min = 0;
        }
        timer_wait = timer_min;
        
    } else {
        timer_wait = finish_time;
    }
    
    
    // Here, in the original Zebra code there was a select() call,
    // the select would block for timer_wait. The simulated
    // bgp instance schedules a timeout for "timer_wait",
    // if a msg arrives before the
    // timer_wait expires the reception of the message
    // will cancel the "timer_wait" timeout and the bgp instance will
    // process the received msg.
    // What if a msg is received when the bgp instance  is not waiting
    // for the "time_wait" to expire but instead it is doing sth
    // else ( such as processing an other msg, sending a msg,etc )?
    // In this case the bgp_interrupt function will insert an entry
    // in the InterruptQueue. After the bgp instance finishes the work in
    // progress, it will check the InterruptQueue to see if there was
    // any interrupt. If so, it will process the interrupt(s),
    // otherwise it will issue a timeout for "timer_wait" and it will
    // return control to ns Scheduler.

    type = TYPE_FETCH2;
    ev = new BGPEvent(BGPEvent:: FETCH2);
    if (InterruptQueue.size()==0)
    {
        InterruptAgent=NULL;
        timer->Schedule(timer_wait,ev);
    }
    else
    {
        //  The InterruptAgent is the local Tcp Listenning Agent
        //  that received the msg
    	
        InterruptAgent = InterruptQueue.front().IntAgent;
        Data* intmsg_ = InterruptQueue.front().intmsg;

	    TCPMsgBuf::iterator iter;
        MessageBuffer *buffer;

    	iter = MsgBuffer.find(InterruptAgent);

    	if ( iter == MsgBuffer.end())
    	{
        	buffer = new MessageBuffer(intmsg_,false,true);
        	MsgBuffer[InterruptAgent]=buffer;
            InterruptQueue.pop_front();
            timer->Schedule(0,ev);
       		return;
    	}

	    buffer = iter->second;

        if ( buffer->partialRead)
        {

            //append
            int size = buffer->ReceivedMsg->Size();

            size += intmsg_->Size();

            char *buf = (char*) malloc(sizeof(char)*size);

            memcpy(buf,buffer->ReceivedMsg->data,buffer->ReceivedMsg->Size());
            memcpy(buf+buffer->ReceivedMsg->Size(),intmsg_->data,intmsg_->Size());

            Data *New = new Data(size,buf);

            free(buf);
            free(intmsg_);

            //check if New is NULL?

            delete buffer->ReceivedMsg;

            buffer->ReceivedMsg = New;

        }
        else
        {
            //overwrite
            if (buffer->ReceivedMsg)
                delete buffer->ReceivedMsg;

            buffer->ReceivedMsg =intmsg_;

        }

        buffer->dataAvailable=true;
        InterruptQueue.pop_front();
        timer->Schedule(0,ev);
    }

}

void
BGP::thread_fetch_part2 (struct thread_master *m,TCP * AgentCausedTimeout)
{
    struct thread *thread;
    Time_t timer_now;
	BGPEvent *ev;

    if(0) thread_master_debug(m);

    /* Read thead. */
    thread = m->read.head;
    while (thread)
    {
        struct thread *t;

        t = thread;
        thread = t->next;
        if (t->u.listenAgent == AgentCausedTimeout)
        {
            thread_list_delete (&m->read, t);
            thread_list_add (&m->ready, t);
            t->type = THREAD_READY;
        }
    }


    /* Timer update. */
    timer_now = Simulator::instance->Now();

    thread = m->timer.head;
    while (thread)
    {
        struct thread *t;
        t = thread;
        thread = t->next;

        if (thread_timer_cmp (timer_now, t->u.time) >= 0)
        {
            thread_list_delete (&m->timer, t);
            thread_list_add (&m->ready, t);
            t->type = THREAD_READY;
        }
    }
    /* Return one event. */
    thread = thread_trim_head (&m->ready);

    /* There is no ready thread. */
    if (!thread)
      {
		type= TYPE_FETCH1;
		ev= new BGPEvent(BGPEvent::FETCH1);	
    	timer->Schedule(0,ev);
    	return ;
      }

    thread->type = THREAD_UNUSED;
    thread_add_unuse (m, thread);

    type = TYPE_EXECUTE;
	ev = new BGPEvent(BGPEvent::EXECUTE);
	ev->thread_to_be_fetched = thread;
    timer->Schedule(0,ev);
}

/* Make thread master. */
struct thread_master *
BGP::thread_make_master ()
{
    struct thread_master *New;

    New = ( struct thread_master * ) XMALLOC (MTYPE_THREAD_MASTER, sizeof (struct thread_master));
    bzero (New, sizeof (struct thread_master));

    return New;
}

