// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: bfs.cc 446 2005-11-15 20:48:03Z riley $



// Implementation of Breadth First Search algorithm
// George Riley, Georgia Tech, Winter 2000

// Converted to work with GTNetS, Summer 2002

#include <iostream>

//#define DEBUG_MASK 0x08
#include "debug.h"
#include "bfs.h"        
#include "node.h"
#include "link.h"
#include "bitmap.h"
#include "interface.h"

using namespace std;

int bfs_calls=0;
int bfs_nodessearched=0;

#define PROXY_PRUNE

#ifndef TEST_BFS

NodeId_t BFS(
         const NodeVec_t& N,
         Node*            root,
         NodeIfVec_t&     NextHop,
         NodeVec_t&       Parent,
         IPAddr_t         targetIP,
         IPAddrVec_t&     aliases)
{  // Compute shortest path to all nodes from root via Breadth First Search
 BitMap B(N.size());           // Bitmap for the colors (grey/white) (white = 0)
 NodeDeque_t Q;                // Vector for the Q list
 DistVec_t   D(N.size(), INF); // Distance vector, initialized to infinity
 NodeId_t    r = NODE_NONE;    // Return value

 bool        pruned;           // Boolean that is used for proxy pruning
 IPAddr_t    rootIP;

#ifdef DEBUG_QSIZE
Count_t maxQSize = 0; // debug
#endif

 bfs_calls++;
 
 // Insure vectors passed in are empty 
 NextHop.clear();
 Parent.clear();
 // Fill in all "NONE" in the next hop neighbors and predecessors
 NextHop.reserve(N.size());
 NextHop.insert(NextHop.begin(), N.size(), NodeIf(nil,nil));
 Parent.reserve(N.size());
 Parent.insert(Parent.begin(), N.size(), nil);


 // Set up information for root node for proxy routing
 int longestPrefix, uPrefix;

 if (root->CanProxyRouteIP(targetIP)) {
   longestPrefix = root->GetLongestPrefixLength(targetIP);
   r = root->Id();
 }
 else
   longestPrefix = 0;

 // Start the algorithm
 B.Set(root->Id()); // Color the root grey
 DEBUG(1,(B.DBPrint()));
 Q.push_back(NodeIf(root,nil)); // And put the root in Q
 D[root->Id()] = 0;
 rootIP = root->GetIPAddr();
 if (rootIP==IPADDR_NONE && root->HasProxyRoutingConfig())
   rootIP = root->GetProxyIP();


 DEBUG0((cout << "Starting BFS Loop" << endl));
 while(Q.size() != 0)
   {
     NodeIf& nodeIf = Q.front();
     Node* u = nodeIf.node;
     DEBUG0((cout << "Working on node " << u->Id() << endl));
     // Get the neighbor and link weight vector
     NodeWeightVec_t nwv;
     DEBUG0((cout << "Getting neighbors for node " << u->Id() << endl));

     uPrefix = 0; // by default no prefix match for proxy routing
     bfs_nodessearched++;

     if (targetIP != IPADDR_NONE)
       { // See if this node is the target
	 DEBUG0((cout << "Checking local ip" << endl));
	 bool localIP = u->LocalIP(targetIP);
	 DEBUG(1,(cout << "NodeId " << u->Id()
		  << " targetIP " << (string)IPAddr(targetIP)
		  << " localIP " << localIP << endl));
	 Interface* localIf = nil;
	 DEBUG0((cout << "Checking local route" << endl));
	 if (!localIP) localIf = u->LocalRoute(targetIP);
	 DEBUG(1,(cout << "NodeId " << u->Id()
		  << " targetIP " << (string)IPAddr(targetIP)
		  << " localIf " << localIf << endl));

	 // added for proxy routing
	 DEBUG0((cout << "Checking proxy routing ability" << endl));

	 if (!localIf) {
	   if (u->HasProxyRoutingConfig()) {
	     DEBUG(1, (cout<< "NodeId " << u->Id() <<" targetIP "
		       << (string)IPAddr(targetIP)
		       << " Proxy Route Config=> IP:"
		       << (string)IPAddr(u->GetProxyIP())
		       << "NetMask:" <<(string)IPAddr(u->GetProxyMask())
		       <<endl));
	     if (u->CanProxyRouteIP(targetIP)) {
	       uPrefix = u->GetLongestPrefixLength(targetIP);
	       // if the longest prefix match is not better than where
	       // we started from then the node should not participate
	       // in proxy routing
	       DEBUG(1, (cout<<"Prefix len="<<uPrefix
			 <<" longest prefix len="<<longestPrefix<<endl));
	       if (uPrefix>longestPrefix) {
		 longestPrefix = uPrefix;
		 r = u->Id();
		 DEBUG(1, (cout<<"Found a better one  Routable!"<<endl));
	       }
	     } else
	       DEBUG(1, (cout<<"Cannot proxy route this IP"<<endl));
	   }
	 }
	 
         if (localIP || localIf)
	   { // Found the target
	     r = u->Id();
#undef USE_ALIASES
#ifdef USE_ALIASES
	     if (localIf)
	       { // Found last hop with local route, get aliases
		 DEBUG(1,(cout << "Found NV, getting neighbors" << endl));
		 const IFVec_t ifaces = u->Interfaces();
		 DEBUG(1,(cout << "Found it...ifsize " << ifaces.size()
			  << " calculating aliases" << endl));
		 for (IFVec_t::size_type k = 0; k < ifaces.size(); ++k)
		   {
		     IPAddrVec_t maybeAliases;
		     u->NeighborsByIf(ifaces[k], maybeAliases);
		     DEBUG(1,(cout << "Iteration " << k
			      << " maybeAliaas size " << maybeAliases.size()
			      << endl));
		     // See if local route to each potential alias
		     for (IPAddrVec_t::size_type j = 0;
			  j < maybeAliases.size(); ++j)
		       {
			 if (u->LocalRoute(maybeAliases[j]))
			   { // insure not parent in the tree
			     Node* myParent = Parent[u->Id()];
			     if (!myParent->LocalIP(maybeAliases[j]))
			       {
				 aliases.push_back(maybeAliases[j]);
			       }
			   }
		       }
		   }
		 DEBUG(1,(cout << "Found NV to " <<(string)IPAddr(targetIP)
			  << " last hop " << (string)IPAddr(u->GetIPAddr())
			  << " lhid " << u->Id()
			  << " with " << aliases.size() << " aliases"
			  << endl));
		 DEBUG(1,(cout << "Done with alias calculation" << endl));
#ifdef  VERBOSE_DEBUG1
		 for (IPAddrVec_t::size_type k = 0; k < aliases.size();++k)
		   {
		     cout << (string)IPAddr(aliases[k]) << endl;
		   }
#endif
	       }
#endif
             if (localIf)
	       { // ok, go ahead and calculate all the way to end target
		 DEBUG(4,(cout << "localif nc " << localIf->NeighborCount(u) <<
			  endl));
		 if (localIf->NeighborCount(u) == 1)
		   { // Only one neighbor, find it
		     Link* l = localIf->GetLink();
		     if (l)
		       {
			 DEBUG(3,(cout << "Getting peer" << endl));
			 InterfaceBasic* pPeer = l->GetPeer(Count_t(0));
			 if (pPeer && pPeer != localIf)
			   {
			     DEBUG(3,(cout <<  "Getting node" << endl));
			     Node* n1 = pPeer->GetNode();
			     if (n1)
			       {
				 DEBUG(3,(cout << "returning based on localif"
					  << endl));
				 Parent[n1->Id()] = u; // Set parent
				 if (n1->Id() == u->Id())
				   {
				     cout << "HuH?  BFS Self Parent"<<endl;
				     exit(1);
				   }
				 return n1->Id();
			       }
			   }
		       }
		   }
	       }

	     return r;
	   }
       }

     pruned = false;

#ifdef PROXY_PRUNE

     DEBUG0((cout<<"Proxy routing Config:"<<u->HasProxyRoutingConfig()<<endl));
     if (u->HasProxyRoutingConfig()) {
       DEBUG0((cout<<"RootIP:"<<IPAddr::ToDotted(rootIP)
	       <<" TargetIP:"<<IPAddr::ToDotted(rootIP)<<endl));
       DEBUG0((cout<<"Can do root IP:"<<u->CanProxyRouteIP(rootIP)
	       <<" targetIP:"<<u->CanProxyRouteIP(targetIP)<<endl));

       if (u->HasProxyRoutingConfig() && !u->CanProxyRouteIP(rootIP)
	   && !u->CanProxyRouteIP(targetIP)) {
	 DEBUG0((cout<<"Pruned..."<<endl));
	 pruned = true;
       }
     }

#endif
     
     if (!pruned) {
	 
       u->Neighbors(nwv, true); // Get neighbors, ignoring leaf nodes
       DEBUG0((cout << "Number adj " << nwv.size() << endl));
       DEBUG0((cout << "Neighbor count is " << nwv.size() << endl));
       
       for (NodeWeightVec_t::size_type i = 0; i < nwv.size(); ++i)
	 {
	   NodeIfWeight& nw = nwv[i];
	   Node*         n = nw.node;
	   Interface*    iface = (Interface*)nw.iface;
	   NodeId_t      id = n->Id();
           DEBUG0((cout << "Node " << u->Id() << " checking iface "
                   << iface << " down " << iface->IsDown() << endl));
           if (iface->IsDown()) continue;
	   if (n == u) continue;              // Insure not self adjacancy


	   DEBUG0((cout << " Working on adj " << id << endl));

	   // before enqueuing a new node we check for proxy routing
	   // we mainly want to prune subtrees that will not have the target
	   // node at anycost

	   // now n is the node that we want to add.

	   bool discarded=false;


	   if (!discarded && B.Get(id) == 0)
	     { // White
	       Q.push_back(NodeIf(n, iface)); // Add to Q set
#ifdef DEBUG_QSIZE
	       if (Q.size() > maxQSize)
		 {
		   maxQSize = Q.size();
		   cout << "New Q size " << maxQSize << endl;
		 }
#endif
	       B.Set(id);                     // Change to grey
	       D[id] = D[u->Id()] + 1;        // Set new distance
	       Parent[id] = u;                // Set parent
	       if (u == root)
		 { // First hop is new node since this is root
		   NextHop[id] = NodeIf(n, iface);
		 }
	       else
		 { // First hop is same as this one
		   NextHop[id] = NextHop[u->Id()];
		 }
	       DEBUG0((cout << "Enqueued " << id << endl));
	     }
	 }
     }

     Q.pop_front();
   }
 return r;
}
#endif
	     

#ifdef TEST_BFS
RNodeVec_t Nodes;

int main()
{
  // See the sample BFS search in Fig23.3, p471 CLR Algorithms book
Node N0(0);
Node N1(1);
Node N2(2);
Node N3(3);
Node N4(4);
Node N5(5);
Node N6(6);
Node N7(7);
RoutingVec_t NextHop;
RoutingVec_t Parent;

 N0.AddAdj(1);
 N0.AddAdj(2);

 N1.AddAdj(0);

 N2.AddAdj(0);
 N2.AddAdj(3);

 N3.AddAdj(2);
 N3.AddAdj(4);
 N3.AddAdj(5);

 N4.AddAdj(3);
 N4.AddAdj(5);
 N4.AddAdj(6);

 N5.AddAdj(4);
 N5.AddAdj(7); 

 N6.AddAdj(4);
 N6.AddAdj(7);

 N7.AddAdj(5);
 N7.AddAdj(6);

 Nodes.push_back(&N0);
 Nodes.push_back(&N1);
 Nodes.push_back(&N2);
 Nodes.push_back(&N3);
 Nodes.push_back(&N4);
 Nodes.push_back(&N5);
 Nodes.push_back(&N6);
 Nodes.push_back(&N7);

 for (nodeid_t i = 0; i < Nodes.size(); i++)
   { // Get shortest path for each root node
     printf("\nFrom root %ld\n", i);
     BFS(Nodes, i, NextHop, Parent);
     PrintParents(Parent);
     for (unsigned int k = 0; k < Nodes.size(); k++)
       printf("Next hop for node %d is %ld\n", k, NextHop[k]);
     printf("Printing paths\n");
     for (nodeid_t j = 0; j < Nodes.size(); j++)
       {
         PrintRoute(i, j, Parent);
       }
   }
 return(0);
}
#endif
