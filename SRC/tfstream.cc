// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: tfstream.cc 279 2005-01-19 22:07:25Z riley $



// Georgia Tech Network Simulator - Trace fstream object class
// George F. Riley.  Georgia Tech, Spring 2002

// Define trace stream object

#include <stdio.h>

#include <iostream>

#include "tfstream.h"
#include "simulator.h"
#include "node.h"

using namespace std;

Tfstream::Tfstream() : pendingEOL(false), lastLogTime(0.0),  timePrecision(5),
                       node(0), ipDotted(true), digitsThreshold(10.0),
                       format("%7.5f")
{
}

void Tfstream::TimePrecision(Count_t d)
{
  char work[20];

  timePrecision = d; // Number of significant digits
  if (timePrecision > 10) timePrecision = 10; // Bound at 10 for sprintf
  sprintf(work, "%c%ld%c%ld%c", '%', timePrecision+2, '.', timePrecision, 'f');
  format = string(work);
}

// Output operators

Tfstream& Tfstream::operator<<(char c)
{
  CheckLogTime();
  (ostream&)*this << c;
  pendingEOL = true;
  return *this;
}

Tfstream& Tfstream::operator<<(const char *s)
{
  CheckLogTime();
  (ostream&)*this << s;
  pendingEOL = true;
  return *this;
}

Tfstream& Tfstream::operator<<(const void *p)
{
  CheckLogTime();
  (ostream&)*this << p;
  pendingEOL = true;
  return *this;
}

Tfstream& Tfstream::operator<<(int n)
{
  CheckLogTime();
  (ostream&)*this << n;
  pendingEOL = true;
  return *this;
}

Tfstream& Tfstream::operator<<(unsigned int n)
{
  CheckLogTime();
  (ostream&)*this << n;
  pendingEOL = true;
  return *this;
}

Tfstream& Tfstream::operator<<(long n)
{
  CheckLogTime();
  (ostream&)*this << n;
  pendingEOL = true;
  return *this;
}

Tfstream& Tfstream::operator<<(unsigned long n)
{
  CheckLogTime();
  (ostream&)*this << n;
  pendingEOL = true;
  return *this;
}

Tfstream& Tfstream::operator<<(double n)
{
  CheckLogTime();
  (ostream&)*this << n;
  pendingEOL = true;
  return *this;
}

Tfstream& Tfstream::operator<<(IPAddr& i)
{
  CheckLogTime();
  if (ipDotted) (ostream&)*this << ((string)i).c_str();
  else          (ostream&)*this << i;
  pendingEOL = true;
  return *this;
}

Tfstream& Tfstream::operator<<(MACAddr& m)
{
  CheckLogTime();
  (ostream&)*this << m;
  pendingEOL = true;
  return *this;
}

bool Tfstream::AppendEOL()
{
  if (pendingEOL)
    {
      (ostream&)*this << '\n';
      pendingEOL = false;
      return true; // appended
    }
  return false; // not needed
}

void Tfstream::NewNode(Node* n) // Log the node for protocol logging
{
  if (n == node) return;        // Same as existing
  AppendEOL();
  node = n;
}

void Tfstream::SetNode(Node* n) // Set node pointer for subsequent traces
{
  node = n;
}

// Private methods
void Tfstream::LogSimTime()
{
  char work[256];
  Time_t now = Simulator::Now();
  if (now >= digitsThreshold)
    { // Need to make new format
      Count_t newTot = 0;
      if      (now >= 1000000) newTot = 8 + timePrecision;
      else if (now >= 100000 ) newTot = 7 + timePrecision;
      else if (now >= 10000  ) newTot = 6 + timePrecision;
      else if (now >= 1000   ) newTot = 5 + timePrecision;
      else if (now >= 100    ) newTot = 4 + timePrecision;
      else if (now >= 10     ) newTot = 3 + timePrecision;
      else                     newTot = 2 + timePrecision;
      sprintf(work, "%c%ld%c%ld%c", '%', newTot, '.', timePrecision, 'f');
      format = string(work);
      switch (newTot - timePrecision) {
        case 8 :
          digitsThreshold = 10000000;
          break;
        case 7 :
          digitsThreshold = 1000000;
          break;
        case 6 :
          digitsThreshold = 100000;
          break;
        case 5 :
          digitsThreshold = 10000;
          break;
        case 4 :
          digitsThreshold = 1000;
          break;
        case 3 :
          digitsThreshold = 100;
          break;
        case 2 :
          digitsThreshold = 10;
          break;
      }
    }
  sprintf(work, format.c_str(), Simulator::Now());
  if (pendingEOL) (ostream&)*this << endl;
  (ostream&)*this << work;
  (ostream&)*this << " N";
  (ostream&)*this << node->Id();
  pendingEOL = true;
  lastLogTime = now;
}

