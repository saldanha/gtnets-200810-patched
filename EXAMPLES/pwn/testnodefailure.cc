// Test the node failure code.
// George F. Riley, Georgia Tech, Summer 2004

#include <iostream>

#include "debug.h"
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "dumbbell.h"
#include "star.h"
#include "application-cbr.h"
#include "application-tcpsend.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "l2proto802.3.h"
#include "validation.h"
#include "rng.h"
#include "routing.h"
#include "routing-static.h"
#include "queue.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

#define N_LEAF 5

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;

  // Use static routing for testing failures
  //Routing::SetRouting(new RoutingStatic());
  
  Count_t nl = N_LEAF; // Number leaf nodes for dumbbell
  Count_t ns = N_LEAF; // Number leaf nodes for each star
  
  if (argc > 1) nl = atol(argv[1]);
  if (argc > 2) ns = atol(argv[2]);

  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);

  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->Open("testnodefailure.txt");
  IPV4::Instance()->SetTrace(Trace::ENABLED);
  L2Proto802_3::GlobalSetTrace(Trace::ENABLED);

  Linkp2p lk(Rate("1Mb"), Time("10ms"));
  Dumbbell b(nl, nl, 1.0,
             IPAddr("192.168.0.1"), IPAddr("192.169.0.1"), lk);
  // Specify the bounding box
  b.BoundingBox(Location(0,0), Location(10,10));
  
  // Animate the queue
  Queue* q = b.RightQueue();
  if (q) q->Animate(true);
  
  // Add a third node between the two gateway routers
  Node* l = b.Left();
  Node* r = b.Right();
  Node* n = new Node();
  n->SetLocation(l->LocationX() + (r->LocationX() - l->LocationX())/2,
                                   l->LocationY());
  
  Interface* lif = l->GetIfByNode(r);
  Interface* rif = r->GetIfByNode(l);
  Interface* nlif = n->AddInterface();
  Interface* nrif = n->AddInterface();
  delete lif->GetLink();
  delete rif->GetLink();
  lif->SetLink(nil);
  rif->SetLink(nil);
  n->AddDuplexLink(nlif, lif, lk);
  n->AddDuplexLink(nrif, rif, lk);

  // Add an alternate (longer) path around the failed  node
  Node* n1 = new Node();
  Node* n2 = new Node();
  n1->SetLocation(l->LocationX(),
                  l->LocationY() -
                  (b.Left(nl-1)->LocationY() - l->LocationY())/2);
  
  n2->SetLocation(r->LocationX(),
                  r->LocationY() -
                  (b.Right(nl-1)->LocationY() - r->LocationY())/2);
  l->AddDuplexLink(n1, lk);
  r->AddDuplexLink(n2, lk);
  n1->AddDuplexLink(n2, lk);

  Uniform startRng(0, 0.5); // Random number generator for start times
  // Create the Star networks and assign clients/servers
  Angle_t adderL = -M_PI / (b.LeftCount() + 1.0);
  Angle_t adderR = M_PI / (b.RightCount() + 1.0);
  Angle_t thetaL = -M_PI_2 + adderL;
  Angle_t thetaR = -M_PI_2 + adderR;

  for (Count_t i = 0; i < b.LeftCount(); ++i)
    {
      Node* l = b.Left(i);
      Node* r = b.Right(i);
#ifdef HAVE_QT
      l->Color(Qt::blue);
      r->Color(Qt::red);
#endif
      // Create a star networks with this each leaf as hub
      Star* sl = new Star(ns,l,lk, IPAddr("192.168.1.1")+IPAddr("0.0.1.0")*i);
      Star* sr = new Star(ns,r,lk, IPAddr("192.169.1.1")+IPAddr("0.0.1.0")*i);
      
      sl->BoundingBox(Location(0,0), Location(2,2), thetaL, M_PI / 2);
      sr->BoundingBox(Location(0,0), Location(2,2), thetaR, M_PI / 2);

      thetaL += adderL;
      thetaR += adderR;
      
      // Add a CBR application at each right side leaf
      for (Count_t j = 0; j < sr->LeafCount(); ++j)
        {
          Node* rl = sr->GetLeaf(j); // Right leaf
          CBRApplication* ca = (CBRApplication*)rl->AddApplication(
              CBRApplication(sl->GetLeaf(j)->GetIPAddr(), 12345, NO_PORT,
                             Rate("100kb")));
          ca->Start(startRng.Value());
        }
    }

  // Specify animation
  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  
  s.Progress(1);
  s.StopAt(10);
  // Specify node failure and recovery times
  s.NodeDownAt(n, Constant(1));
  s.NodeUpAt(n, Constant(2));
  //Interface* iface1 = b.Right()->GetIfByNode(n);
  //s.InterfaceDownAt(iface1, Constant(0.8));
  // And fail one of the dumbbell leafs, for partial invalidatoin
  //s.NodeDownAt(b.Right(0), Constant(0.5));
  //s.NodeUpAt(b.Right(0), Constant(0.8));
  s.Run();
  tr->Close();
}
