// Recreate the Kevin Jeffay web browsing experiment
// George F. Riley.  Georgia Tech, Fall 2002

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <string.h>

#include "simulator.h"
#include "scheduler.h"
#include "dumbbell.h"
#include "linkp2p.h"
#include "ratetimeparse.h"
#include "tcp-tahoe.h"
#include "tcp-reno.h"
#include "tcp-newreno.h"
#include "rtt-estimator.h"
#include "application-tcpserver.h"
#include "application-webbrowser.h"
#include "histogram.h"
#include "globalstats.h"
#include "droptail.h"
#include "red.h"
#include "star.h"
#include "ipv4.h"
#include "average-min-max.h"
#include "validation.h"

using namespace std;

#define SEG_SIZE 1500
#define HDR_SIZE   40

#define N_SERVERS     7
#define N_CLIENT_NETS 7

#define RESET_TIME 1200
//#define IDLE_TIME  5400.0
//#define STOP_TIME  6000
//#define STOP_TIME  20
//#define RESET_TIME 600
#define IDLE_TIME  100000
#define STOP_TIME  101000

// Round trip delays per the Tuning Web paper (see Appendix A)
Time_t delayMs[] = {
  81, 105,  64,  64,  67, 147, 114,
 126, 137,  47,  53,  41,  86, 114,
  33,  42,  40, 114, 112, 117, 108,
  35,  45,  95, 100,  31, 100, 116,
 105,  92,  78,  41,  53, 109,  66,
  85, 112,  38,  83,  55,   8,  41,
 124,  87, 101,  87,  95,   7,  61
};

Queue*  bnQueue = nil;      // Bottleneck queue
AverageMinMax rttStats;
Histogram h(2.0, 20);       // 0-2.0 seconds, 20 bins
Histogram toh(20, 20);      // 20 bins, 1 each

bool    unconstrained = false; // True if 100Mbps bottleneck

// Simple timer class for 1pps debugging info
class MyTimer : public Timer {
public:
  virtual void Timeout(TimerEvent*);  // Called when timer expires
};

void MyTimer::Timeout(TimerEvent* ev)
{
  Time_t now = Simulator::Now();
  double dropPct = 100.0 * 
    ((double)bnQueue->DropCount() / 
     (double)bnQueue->EnqueueCount());
  char work[20];
  sprintf(work, "%4.1f", dropPct);
  cout << "T " << now
       << " aBr " << setw(3) << WebBrowser::activeBrowsers
       << " aCn " << setw(3) << WebBrowser::activeConnections
       << " sT " << setw(4) << WebBrowser::startedThisPeriod
       << " cT " << setw(4) << WebBrowser::completedThisPeriod
       << " cCn " << WebBrowser::completedConnections
    // << " DP " << bnQueue->DropCount();
       << " DP " << work << "%";
  double occupancyPct = 100.0 * 
    (double)bnQueue->Length() / (double)bnQueue->GetLimit();
  sprintf(work, "%4.1f", occupancyPct);
  cout << " QO "  << work << "%";
  cout << " TO " << Stats::tcpRetxTimeouts << endl;
  
  WebBrowser::startedThisPeriod = 0;
  WebBrowser::completedThisPeriod = 0;
  int inow = (int)now;
#ifdef LOG_RTT_INFO
  if ((inow % 100) == 0)
    {
      cout << "RttStats ";
      rttStats.Log(cout); // Print the rtt stats
    }
#endif
  if (inow == RESET_TIME)
    { // Time to reset all stats
      bnQueue->ResetStats();
      bnQueue->ResetAverage();
      h.Reset();
      toh.Reset();
      Stats::tcpRetxTimeouts = 0;
      WebBrowser::completedConnections = 0;
      WebBrowser::totRespTime = 0;
      WebBrowser::totRespSize = 0;
      WebBrowser::totObjectsPerPage = 0;
      WebBrowser::totBrowserSessions = 0;
      WebBrowser::totReqBytes = 0;
      WebBrowser::totRespBytes = 0;
      WebBrowser::cleanConnections = 0;
      WebBrowser::dirtyConnections = 0;
      WebBrowser::clientTimeouts = 0;
      WebBrowser::serverTimeouts = 0;
    }
  if (unconstrained)
    Schedule(ev, 10.0);
  else
    Schedule(ev, 1.0);
}

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Count_t nBrowsers = 3000;      // Default numberof browsers
  Count_t qLimit = 30;           // Default Queue Limit
  Count_t qMax = 90;             // Max_q if RED
  bool    red = false;           // True if red queue
  Mult_t  constrainMult = 0.1;   // 10% constraint on bottleneck (default)

  // debug
  Time_t totDel = 0;
  for (int i = 0; i < 49; ++i) totDel += delayMs[i];
  cout << "Avg Delay " << totDel / 49.0 << endl;

  if (strstr(argv[0], "uncon") != NULL)
    {
      unconstrained = true;
      constrainMult = 1.0;
      cout << "Using unconstrained network" << endl;
    }
  if (argc > 1) nBrowsers = atol(argv[1]);
  if (argc > 2) 
    { // See if RED specified
      int l = strlen(argv[2]);
      for (int i = 0; i < l; ++i) argv[2][i] = tolower(argv[2][i]);
      string arg(argv[2]);
      red = (arg.find("red") != string::npos);
    }
  if (argc > 3) qLimit = atol(argv[3]);
  if (argc > 4) qMax = atol(argv[4]);

  Simulator s;
  if (red)
    {
      Queue::Default(REDQueue());
    }
  else
    {
      Queue::Default(DropTail());
      Queue::DefaultLength(50000); // Set large limit for all queues
      // execept bottleneck (set below)
      //Queue::DefaultLength(qLimit * (SEG_SIZE + HDR_SIZE));
      //Queue::DefaultLimitPkts(qLimit); // ! debug
    }

  Trace* gs = Trace::Instance();
  gs->IPDotted(true);
  if (nBrowsers == 1)
    gs->Open("jeffay.txt");
  TCP::LogFlagsText(true);    // Log flags in text mode
  // Enable all l3 tracing
  IPV4::Instance()->SetTrace(Trace::ENABLED);

  // Set some TCP defaults per the Tuning Web paper
  TCP::DefaultSegSize(1500);  // Ethernet segment size
  TCP::DefaultAdvWin(0x4000); // 16kb Window
  TCP::DefaultSSThresh(1000000); // 1Mb slow start threshold
  TCP::Default().SetTrace(Trace::ENABLED); // Enable trace on default tcp
  // Set statistics collection for RTT, for debugging
  RTTEstimator::SetStatistics(&rttStats);

  Linkp2p l1;
  l1.Bandwidth(Rate("100Mb"));
  l1.Delay(Time("10us"));
  Linkp2p l2;
  l2.Bandwidth(Rate("10Mb"));
  l2.Delay(Time("10us"));

  // See Jeffay Fig 2 for topology
  Dumbbell b(1, 1, constrainMult, l1); // Enet sw, campus rtr, isp rtr, Enet sw
  bnQueue = b.RightQueue(); // Bottleneck queue
  bnQueue->SetLimit(qLimit * (SEG_SIZE + HDR_SIZE));
  Queue* bnQueueL = b.LeftQueue(); // Request side
  bnQueueL->SetLimit( 240 * (SEG_SIZE + HDR_SIZE)); // Large for req pkts ?
  bnQueueL->Verbose(); // debug...see if we drop any req pkts (should not!)
  //bnQueue->SetLimitPkts(qLimit); // dbg .. limiting in pkts makes difference?
  // Create the seven servers
  Star serverStar(N_SERVERS, b.Right(0), l2, IPAddr("192.168.2.0"));
  // Create the servers
  IPAddrVec_t servers; // IP Addresses of possible servers
  for (Count_t i = 0; i < N_SERVERS; ++i)
    {
      Node* n = serverStar.GetLeaf(i);
      IPAddr serverIP = n->GetIPAddr();
      DEBUG0((cout << "Right node " << i 
              << " ipaddr " << (string)serverIP << endl));
      servers.push_back(serverIP);
      // Add a tcp server on right side
      TCPServer* app = (TCPServer*)n->AddApplication(TCPServer());
      app->Bind(HTTP_PORT);
      app->CloseOnEmpty();
      app->DeleteOnComplete();
    }
  // Create the web browser nodes
  Star clientStar(N_CLIENT_NETS, b.Left(0), l2);

  vector <Star*> stars;
  IPAddr_t starIP = IPAddr("192.169.1.1");
  for (Count_t i = 0; i < N_CLIENT_NETS; ++i)
    {
      Node* n = clientStar.GetLeaf(i);
      Star* s = new Star(N_SERVERS, n, l2, starIP);
      stars.push_back(s); // Save the star networks for browser instantiation
      for (Count_t j = 0; j < N_SERVERS; ++j)
        { // Set link delay to get round trip variations, per Jeffay Paper
          // Subtract 5 since we have 5 hops with 1ms delay + this one
          Time_t d = delayMs[i * N_SERVERS + j] - 5;
          s->HubLink(j)->Delay((d/1000.0)/2.0);
          s->LeafLink(j)->Delay((d/1000.0)/2.0);
        }
      starIP += 0x0100; // Advance to next set of IP addresses
    }

  // Create the browsers
  Count_t si = 0; // Star index
  Count_t li = 0; // Leaf index
  Uniform startRng(0,600);                // RNG for start times
  vector <WebBrowser*> browsers;
  for (Count_t i = 0; i < nBrowsers; ++i)
    {
      WebBrowser* wb = (WebBrowser*)stars[si]->GetLeaf(li)->AddApplication(
          WebBrowser(servers, Uniform(0, servers.size())));
      wb->GetTCP()->GetRTTEstimator()->SetPrivateStatistics(AverageMinMax());
      wb->ThinkTimeBound(600.0);
      wb->SetStatistics(&h);
      wb->SetDebugStats(&toh);
      wb->IdleTime(IDLE_TIME);
      wb->Start(startRng.Value());
      browsers.push_back(wb); // Save for later
      // Advance to next browser
      if (++li == N_SERVERS)
        {
          li = 0;
          if (++si == N_SERVERS) si = 0;
        }
    }

  //s.Progress(10);
  MyTimer t;
  if (unconstrained)
    t.Schedule(new TimerEvent, 10.0);
  else
    t.Schedule(new TimerEvent, 1.0); // Simple debug info

  s.StopAt(STOP_TIME);
  s.StopAt(200);
  s.Run();
  cout << "Preparing for cdf file" << endl;
  char work[100];
  char work1[100];
  sprintf(work, "%d", qLimit);
  sprintf(work1, "%d", nBrowsers);
  string fn = string("jeffay-") + string(work1) + string("-");
  string fnh = string("jeffay-") + string(work1) + string("-");
  if (red)
    {
      fn = fn + string("RED-") + string(work) + string(".cdf");
      fnh = fnh + string("RED-") + string(work) + string(".histo");
    }
  else
    {
      fn = fn + string("DropTail-") + string(work) + string(".cdf");
      fnh = fnh + string("DropTail-") + string(work) + string(".histo");
    }

  ofstream cdf(fn.c_str());
  h.CDF(cdf, "# CDF of Web Object Response Time", ' ');
  cout << "Creating cdf " << fn << endl;
  cdf.close();
  ofstream histo(fnh.c_str());
  h.Log(histo, "# Histogram of Web Object Response Time", ' ');
  histo.close();
  toh.Log(cout);
  // Debug..dump hung connections
  cout << "Dumping hung connections" << endl;
  for (Count_t i = 0; i < browsers.size(); ++i)
    {
      browsers[i]->DBDump();
    }
  cout << "Cleaning up" << endl;
  // Cleanup for memory leak report
  for(vector<Star*>::size_type i = 0; i < stars.size(); ++i)
    {
      delete stars[i];
    }
  for(vector<WebBrowser*>::size_type i = 0; i < browsers.size(); ++i)
    {
      delete browsers[i];
    }
  cout << "Printing rtt stats" << endl;
  rttStats.Log(cout); // Print the rtt stats
#define VERBOSE
#ifdef VERBOSE
  cout << "CleanConn " << WebBrowser::cleanConnections
       << " Dirty " << WebBrowser::dirtyConnections 
       << " client TO " << WebBrowser::clientTimeouts
       << " server TO " << WebBrowser::serverTimeouts
       << endl;
  Stats::Print(); // Print the statistics
  rttStats.Log(cout); // Print the rtt stats
  cout << "Memory usage after run " 
       << s.ReportMemoryUsageMB() << "MB" << endl;
  cout << "Total events processed "
       << Scheduler::Instance()->TotalEventsProcessed() << endl;
  cout << "Setup time " << s.SetupTime() << endl;
  cout << "Route time " << s.RouteTime() << endl;
  cout << "Run time "   << s.RunTime() << endl;
  cout << "Average Response Time " << WebBrowser::totRespTime / 
    WebBrowser::completedConnections << endl;
  cout << "Average Response Size " << WebBrowser::totRespSize / 
    WebBrowser::completedConnections << endl;
  cout << "Average Objects/Page " << (double)WebBrowser::totObjectsPerPage / 
    (double)WebBrowser::totBrowserSessions << endl;
  cout << "Tot objects " << WebBrowser::totObjectsPerPage << endl;
  cout << "Tot sessions " << WebBrowser::totBrowserSessions << endl;
  cout << "Max Response Time " << WebBrowser::largestRespTime << endl;
  cout << "Max Response Size " << WebBrowser::largestRespSize << endl;
  cout << "Avg Sleep Time " << WebBrowser::totWaitTime / 
    WebBrowser::waitCount << endl;
  double aol = WebBrowser::totRespBytes / (IDLE_TIME - RESET_TIME);
  cout << "Avg Offered Load "
       << aol << " bytes/sec" << endl;
  cout << "Offered load percent " << 100.0 * (aol * 8.0)/ Rate("10Mb") << endl;
  cout << "Average Queue Size (bytes) " << bnQueue->Average() 
       << " (pkts) " << (double)bnQueue->Average()/1500.0 << endl;
#endif
}
