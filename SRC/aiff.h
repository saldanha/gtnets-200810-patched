// Definitions for reading/writing AIFF formats
// Used by the GTNetS VOIP models
// George F. Riley, Georgia Tech, Spring 2005

#ifndef __AIFF_H__
#define __AIFF_H__

#include <string>
#include <fstream>

class AIFF {
public:
  AIFF();
  ~AIFF();
  bool Open(const std::string&);  // Open specified AIFF File for reading
  bool Create(const std::string&);// Open specified AIFF file for writing 
  bool Close();
  // Reading the AIFF
  bool   Read(char*, size_t);
  bool   Read(short&);        // Read a short to output file
  bool   Read(long&);         // Read a long to output file
  bool   Read(unsigned long&);// Read a unsigned long to output file
  bool   ReadSoundData(long& l, long& r);
  size_t ReadSoundData(long* l, long* r, size_t count);
  size_t ReadSoundDataSingle(long* l, size_t count);
  bool   ReadCommonHdr();     // Read the COMM header
  bool   SkipRemainingChunk();
  // Writing the AIFF
  bool   Write(const char*, long);// Write bytes to output file
  bool   Write(short);        // Write a short to output file
  bool   Write(long);         // Write a long to output file
  bool   Write(unsigned long);// Write a unsigned long to output file
  bool   WriteSoundData(long l, long r);
  size_t WriteSoundData(long* l, long* r, size_t count);
  size_t WriteSoundDataSingle(long* l, size_t count);
  // Manage the parameters
  short  NumChannels();
  short  SampleSize();
  double SampleRate();
  bool   NumChannels(short);  // Specify number of channels
  bool   SampleSize(short);   // Specify sample size
  bool   SampleRate(double);  // Specify sample rate
private:
  // Private methods
  bool ReadChunkHeader(char* id);
  bool WriteChunkHeader(const char* id, long);
public:
  // Static methods
  static double ConvertFromExtended(char* d);
  static void   ConvertToExtended(double, char*);
  // Computes sum of squares of sample values.
  // Used for silence supression
  static double SoundIntensity(long*, size_t);
private:
  std::ifstream i; // Input file
  std::ofstream o; // Output file
  double   sampleRate;
  short    numChannels;
  short    sampleSize;
  long     formBlockSize;   // Block size in form header (written at close)
  long     ssndBlockSize;   // Block size for SSND (written at close)
  long     numSampleFrames; // Read from input
  char     sampleRateCh[10];// Extended format sample rate
  std::streampos formSizePos;  // Positions for the various sizes
  std::streampos ssndSizePos;
  std::streampos numSamplePos;
  long     remainingChunkBytes; // Bytes left in current chunk (reading)
  bool     COMMHdrWritten;          // True if COMM Header already written
  bool     SSNDHdrWritten;          // True if SSND Header already written 
  bool     oddChunkSize;            // True if chunk size is odd
};

typedef struct 
{
  char  form[4];
  long  length;
  char  type[4];
} FormHdr_t;

// Below is documentary only, as the packing is messed up.
// Subrouting ReadCommonHdr actually reads this one field at a time

typedef struct    
{
  char  id[4];
  long  chunkSize;
  short numChannels;
  long  numSampleFrames;
  short sampleSize;
  char  sampleRate[10];
} CommonHdr_t;

// Sound chunk
typedef struct
{
  char  id[4];
  long  chunkSize;
  long  offset;
  long  blockSize;
  // Data follows
} SSNDHdr_t;

#endif
