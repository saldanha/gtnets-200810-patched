// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-tcpsend.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - TCP Bulk Data Transfer Application class
// George F. Riley.  Georgia Tech, Spring 2002

// Define a TCP Bulk Data class

// Uncomment below to enable debug level 0
//#define DEBUG_MASK 0x1
#include "debug.h"
#include "application-tcpsend.h"
#include "tcp.h"
#include "simulator.h"
#include "globalstats.h"

using namespace std;

// Static members
Count_t TCPSend::totalStarted = 0;// Number of TCP applications started
Count_t TCPSend::totalEnded = 0;  // Number of TCP applications ended

// Constructors
// TCP Type, peerIp, peerPort, RandomSize, Random Sleep
TCPSend::TCPSend(IPAddr_t ip, PortId_t p, 
                 const Random& szRv, 
                 const TCP& t, 
                 const Random& sleepTime,
                 Count_t loopCount)
  : TCPApplication(t), peerIP(ip), peerPort(p),
    sizeRV(szRv.Copy()), sleepRV(sleepTime.Copy()), 
    lCount(loopCount), rCount(0), pendingEvent(0),
    sent(0), sentAck(0),
    connected(false), ended(false)
{
}

TCPSend::TCPSend(const TCPSend& c)
  : TCPApplication(c),
    peerIP(c.peerIP), peerPort(c.peerPort),
    sizeRV(c.sizeRV->Copy()), sleepRV(c.sleepRV->Copy()),
    lCount(c.lCount), rCount(c.rCount),
    pendingEvent(0), sent(0), sentAck(0),
    connected(c.connected), ended(c.ended)
{ // Copy constructor
}

TCPSend::~TCPSend()
{
  delete sizeRV;
  delete sleepRV;
}

// Handler Methods
void TCPSend::Handle(Event* e, Time_t t)
{
  AppTCPEvent* ev = (AppTCPEvent*)e;
  switch (ev->event) {
    case AppTCPEvent::SEND_DATA :
      DEBUG0((cout << "Send Data Event" << endl));
      SendData();
      delete e;
      return;
    default :
      break;
  }
  Application::Handle(e, t);  // May be application event
}

  // Upcalls from L4 protocol
void TCPSend::Receive(Packet*,L4Protocol*,Seq_t)
{ // Data received (ignored)
}

void TCPSend::Sent(Count_t c, L4Protocol*)
{ // Data has been sent
  if (!c) return;
  sentAck += c; // Note received
  DEBUG0((cout << "Application::Sent c " << c << " sentAck " << sentAck
          << " sent " << sent << endl));
  if (sentAck >= sent)
    { // All sent, schedule sleep event if more loops
      DEBUG(0,(cout << "All sent, acked " << sentAck 
               << " send " << sent
               << " rCount " << rCount 
               << " lCount " << lCount << endl));
      if ( ++rCount < lCount)
        { // Need wakeup event
          AppTCPEvent* ev = new AppTCPEvent(AppTCPEvent::SEND_DATA);
          pendingEvent = ev;
          // Schedule again    
          Time_t st = sleepRV->Value(); // Sleep time
          DEBUG0((cout << "Sleeping for " << st << " secs" << endl));
          cout << "Sleeping for " << st << " secs" << endl;
          Scheduler::Schedule(ev, st, this);
        }
      else
        { // No (more) repeats, just close
          DEBUG0((cout << "Closing" << endl));
          l4Proto->Close();
          totalEnded++;
          ended = true;
          DEBUG0((cout << "TCP App ended " << totalEnded
                  << " of " << totalStarted 
                  << " mem " << Simulator::instance->ReportMemoryUsageMB()
                  << "Mb" 
                  << " evlSize " << Scheduler::Instance()->EventListSize() 
                  << " pkts " << Stats::pktsAllocated - Stats::pktsDeleted
                  << endl));
        }
    }
}

void TCPSend::CloseRequest(L4Protocol*)
{ // Close request from peer
  cout << "TCPSend::CloseRequest() " << endl;
  l4Proto->Close(); // Close in response
  Delete();  // Delete the application object 
}

void TCPSend::Closed(L4Protocol*)
{ // Connection has closed
  // Should we delete the TCP object here?
  DEBUG0((cout << "TCPSend::Closed() " << endl));
  Delete();// Delete the application object    
}

void TCPSend::ConnectionComplete(L4Protocol*)
{ // Connection request succeeded
  connected = true;
  DEBUG0((cout << "TCPSend::ConnectionComplete tcp " << l4Proto << endl));
  SendData(); // Connection successful, send the data
}

void TCPSend::ConnectionFailed(L4Protocol* l4, bool aborted)
{ // Connection request failed
  // Possibly print a debug msg
  IPAddr_t peer = l4->peerIP;
  if (aborted)
    {
      cout << "TCPSend::ConnectionAborted, peer " 
           << (string)IPAddr(peer) << endl;
    }
  else
    {
      cout << "TCPSend::ConnectionFailed, peer " 
           << (string)IPAddr(peer) << endl;
    }
  totalEnded++; // Count as ended
}

// Application Methods
void TCPSend::StartApp()    // Called at time specified by Start
{
  StopApp();                       // Insure no pending event
  SendData();                      // Send the data block
  totalStarted++;                  // Count started apps
  DEBUG0((cout << "TCP App started " << totalStarted 
          << " mem " << Simulator::instance->ReportMemoryUsageMB() 
          << "Mb" << " evlSize " << Scheduler::Instance()->EventListSize() 
          << " time " << Simulator::Now()
          << endl));
}

void TCPSend::StopApp()     // Called at time specified by Stop
{
  if (pendingEvent)
    { // Cancel the pending transmit event
      Scheduler::Cancel(pendingEvent);
      delete pendingEvent;
      pendingEvent = NULL;
    }
}

Application* TCPSend::Copy() const
{ // Make a copy of this application
  return new TCPSend(*this);
}

void TCPSend::DBDump()
{
  if (!ended)
    {
      cout << "TCPSend " << this 
           << " ended " << ended
           << " sip " << (string)IPAddr(srcip)
           << " spt " << srcPort
           << " rip " << (string)IPAddr(dstip)
           << " rpt " << dstPort
           << " sent " << sent
           << " sentAck " << sentAck
           << endl;
    }
}

// Private Methods

void TCPSend::SendData()
{
  if (!connected)
    {
      DEBUG0((cout << "TCPSend::SendData not connected, tcp " << l4Proto << endl));
      l4Proto->Connect(peerIP, peerPort);
      return;
    }
  sent = (Count_t)sizeRV->Value();  // Number of bytes to send
  if (sent == 0) sent++;   // Never 0
  sentAck = 0;                 // Reset sent count
  Data d(sent);                // Message size
  d.msgSize = sent;            // Message size
  d.responseSize = 0;          // Response size
  l4Proto->Send(d);            // Send the data
}

