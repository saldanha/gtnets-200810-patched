// Test the AODV Protocol
// George F. Riley.  Georgia Tech, Fall 2002

#include "simulator.h"
#include "application-aodv.h"
#include "star.h"
#include "ratetimeparse.h"
#include "validation.h"

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;

  // Create a star topology
  Star b(10, IPAddr("192.168.1.0"));
  // Put an AODV application at the hub
  AODVApplication* a = new AODVApplication(b.GetHub());
  a->Start(0.0);  // Start at time 0
  for (Count_t i = 0; i < b.LeafCount(); ++i)
    { // Put an AODV app at each leaf
      AODVApplication* a = new AODVApplication(b.GetLeaf(i));
      a->Start((i+1.0));  // Start at ascending times
    }
  s.Run();
}
