// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-webserver.cc 185 2004-11-05 16:40:27Z riley $



// Georgia Tech Network Simulator - Web Server Application class
// George F. Riley.  Georgia Tech, Spring 2003

// Define a Web Server Application Class
// Responds to HTTP-style web GET commands.  

#include <stdio.h>
#include <map>
#include <cstring>

// Uncomment below to enable debug
//#define DEBUG_MASK 0x02
#include "debug.h"
#include "application-webserver.h"
#include "tcp.h"
#include "simulator.h"
#include "globalstats.h"

using namespace std;

// Static variables
PortId_t    WebServer::port = 80; // Port number to bind to
Count_t     WebServer::defaultMaxConnections = 128; // Default Max connections
Count_t     WebServer::defaultMaxIP          = 20;  // Default Max connections
Count_t     WebServer::defaultMaxURL         = 20;  // Default Max connections

// Constructors
// TCP Type
WebServer::WebServer(const TCP& t)
  : TCPApplication(t), 
    node(nil), gCache(false), logEnabled(false),
    maxConnections(defaultMaxConnections),
    maxIP(defaultMaxIP), maxURL(defaultMaxURL)
{
  CopyOnConnect(false); // Do not copy this application on connect requests
  // Debug only..poplulate the ipaddrs table
  gcIPAddrs.push_back(IPPort_t(IPAddr("192.168.100.100"), 6347));
}

WebServer::WebServer(const WebServer& c)
  : TCPApplication(c), 
    node(c.node), gCache(c.gCache), logEnabled(c.logEnabled),
    maxConnections(c.maxConnections), 
    maxIP(c.maxIP), maxURL(c.maxURL)
{ // Copy constructor
}

WebServer::~WebServer()
{
  // cout << "hello from web server destructor" << endl;
}

void WebServer::AttachNode(Node* n)
{
  node = n;
  // Inform l4protocol of node, bind to port, and listen for connections
  l4Proto->Attach(n);
  l4Proto->Bind(port);
  DEBUG0((cout << "Node " << (string)IPAddr(n->GetIPAddr())
          << " binding to port " << port 
          << " tcp " << l4Proto
          << endl));
  l4Proto->AttachApplication(this);
  ((TCP*)l4Proto)->Listen();
  ((TCP*)l4Proto)->CloseOnEmpty();
}

// Upcalls from L4 protocol
void WebServer::Receive(Packet* p, L4Protocol* l4, Seq_t)
{ // Data received
  // Find the HTTP info object
  DEBUG0((cout << "WebServer::Receive, " << this << " l4 " << l4 << endl));
  L4Map_t::iterator i = activeConnections.find(l4);
  if (i == activeConnections.end())
    {
      cout << "HuH? WebServer::Receive can't find active connection" << endl;
      return;
    }

  Data* d = (Data*)p->PeekPDU();  // Get the data pdu
  if (d->Contents())
    { // Actual HTTP Request
      DEBUG0((cout << "WebServer::Receive, dataSize " << d->Size()
              << " Contents " << d->Contents() << endl));
      ProcessHTTPRequest(i, p, l4);
      return;
    }

  // Just the dummy request type
  TCP* t = (TCP*)i->first;
  i->second.totRx += d->Size();          // Log bytes received
  DEBUG0((cout << "AppWebServer " << this << " RxBytes " << d->Size()
          << " totRx " << i->second.totRx << " msgsize " << d->msgSize <<
          endl));
  if (!i->second.timeKnown)
    { // Note start time
      i->second.msgStart = p->time;
      i->second.timeKnown = true;
    }
  if (i->second.totRx >= d->msgSize)
    {
      i->second.totRx -= d->msgSize;       // Subtract for next message
      i->second.totSent = d->responseSize; // Number of bytes to sent     
      i->second.totAck = 0;                // Clear ack count     
      DEBUG0((cout << "AppWebServer " << this << " responding with "
              << i->second.totSent << " bytes " << endl));
      if (i->second.stats)
        { // Record response time
          i->second.stats->Record(Simulator::Now() - i->second.msgStart);
        }
      i->second.timeKnown = false;
      if (i->second.totRx)
        { // If got partial of next
          i->second.msgStart = p->time;
          i->second.timeKnown = true;
        }
      if (i->second.totSent)
        {
          DEBUG0((cout << "AppTS " << this 
                  << " Rq " << d->msgSize
                  << " Rx " << d->responseSize
                  << " peer " << t->Peer() << endl));
          t->Send(i->second.totSent);// Send the response   
        }
    }
  delete p;                      // Delete the packet
}

void WebServer::Sent(Count_t c, L4Protocol* l4)
{ // Data has been sent
  L4Map_t::iterator i = activeConnections.find(l4);
  if (i == activeConnections.end())
    {
      cout << "HuH? WebServer::Sent " << this 
           << " can't find connection " << l4 << endl;
      cout << "acCount " << activeConnections.size() << endl;
      return;
    }
  HTTPInfo& hInfo = i->second; // Get the http info record
  hInfo.totAck += c; // Note received
  DEBUG0((cout << "WebServer " << this 
          << " sent c " << c << " totAck " << hInfo.totAck
          << " totSent " << hInfo.totSent << endl));
  if (hInfo.totAck >= hInfo.totSent)
    { // All sent, close
      DEBUG0((cout << "WebServer closing connection" << endl));
      l4->Close();
    }
}

void WebServer::CloseRequest(L4Protocol* l4)
{ // Close request from peer
  DEBUG0((cout << "WebServer::CloseRequest() " << this 
          << " l4 " << l4 
          << " time " << Simulator::Now()
          << endl));
  l4->Close(); // Close in response
  // Remove from the map of open connections
  L4Map_t::iterator i = activeConnections.find(l4);
  if (i == activeConnections.end())
    {
      cout << "HuH? WebServer::CloseRequest can't find connection" << endl;
      return;
    }
  activeConnections.erase(i); // Remove from the list
  LogTimeCount();
}

void WebServer::Closed(L4Protocol* l4)
{ // Connection has closed
  // Remove this connectionn from pending
  DEBUG0((cout << "WebServer::Closed() " << this 
          << " l4 " << l4 << endl));
  // Remove from the map of open connections
  L4Map_t::iterator i = activeConnections.find(l4);
  if (i == activeConnections.end())
    {
      cout << "HuH? WebServer::CloseRequest can't find connection" << endl;
      return;
    }
  activeConnections.erase(i); // Remove from the list
  LogTimeCount();
}

Application* WebServer::Copy() const
{ // Make a copy of this application
  return new WebServer(*this);
}

void WebServer::ConnectionFailed(L4Protocol* l4, bool aborted)
{ // The connection did not complete 3-way handshake
  // Find in the active list and remove
  L4Map_t::iterator i = activeConnections.find(l4);
  if (i == activeConnections.end()) return; // Nothing to do
  // Found it
  L4Protocol* t = i->first;
  // Unbind and delete
  DEBUG(1,(cout << "AppWS unbinding" << endl));
  // The TCP Destructor will do this, so we don't need to
  //t->localNode->Unbind(t->ProtocolNumber(), t->localPort, IPADDR_NONE, 
  //          t->peerPort, t->peerIP, t);
  activeConnections.erase(i);
  LogTimeCount();
  DEBUG(1,(cout << "Deleted failed connection, tcp " << t 
           << " count " << activeConnections.size()
           << endl));
  // We can't delete the TCP object just yet, since we have been
  // called from this object and it will continue to processes 
  // when this method exits.
  Simulator::instance->DeleteObject(t);
}

bool WebServer::ConnectionFromPeer(L4Protocol* l4, IPAddr_t, PortId_t)
{ // Listener has connection request from peer
  // First see if we can accept more connections
  DEBUG(1, (cout << "WebServer::ConnectionFromPeer, ac.size " 
            << activeConnections.size() << " maxC " << maxConnections 
            << " tcp " << l4
            << " time " << Simulator::Now()
            << endl));
  if (activeConnections.size() == maxConnections) return false;
  // Next double check that specified is not already active (should not happen)
  L4Map_t::iterator i = activeConnections.find(l4);
  if (i != activeConnections.end())
    { // ?
      cout << "HuH?  WebServer::ConnectFromPeer already active" << endl;
      activeConnections.erase(i); // Erase it and continue
      LogTimeCount();
    }
  TCP* t = (TCP*)l4;
  t->DeleteOnTWait(); // Delete this endpoint when done
  activeConnections[l4] = HTTPInfo(); // Add a new entry
  LogTimeCount();
  return true;
}

void WebServer::EnableGCache(bool egc)
{
  gCache = egc;
}

void WebServer::EnableLog(bool el)
{ // Enable the logging of table size vs. time
  logEnabled = el;
}

void WebServer::MaxConnections(Count_t m)
{ // Set a limit on number of simulataneous connectoins
  maxConnections = m;
}

void WebServer::PrintTimeCount(std::ostream& os, char sep )
{  // Print the Time/Count history
  for (TimeCount_t::size_type i = 0; i < timeCountLog.size(); ++i)
    {
      os << timeCountLog[i].time << sep << timeCountLog[i].count << endl;
    }
}

void WebServer::MaxIP(Count_t m)
{ // Set a limit on number of IP's in the GCache
  maxIP = m;
}

void WebServer::MaxURL(Count_t m)
{ // Set a limit on number of URL's in the GCache
  maxURL = m;
}

// Private member functions
void  WebServer::ProcessHTTPRequest(
          L4Map_t::iterator i,
          Packet* p, L4Protocol* l4)
{
  Data* d = (Data*)p->PeekPDU();  // Get the data pdu
  string r(d->Contents());        // Get the request
  delete p;                       // Done with the packet
  DEBUG0((cout << "WebServer::ProcessHTTPRequest " << r << endl));
  if (gCache)
    { // Check for the gcache commands
      if (r.find("gcache.php?") == 0)
        { // Found a gcache command
          string s = r.substr(11, string::npos); // the command
          string::size_type j;
          if (s.find("hostfile=1") == 0)
            { // Found host file request
              cout << "Found hostfile request " << r << endl;
              SendHostFile(i, l4);
            }
          if (s.find("urlfile=1") == 0)
            { // Found url file request
              cout << "Found urlfile request " << r << endl;
              SendURLFile(i, l4);
            }
          j = s.find("ip=");
          if (j != string::npos)
            { // Found ip update request
              cout << "Found ip update request " << r << endl;
              j = s.find(':');
              string ipa = s.substr(3, j - 3);
              string port = s.substr(j+1, string::npos);
              gcIPAddrs.push_back(
                  IPPort_t(IPAddr(ipa.c_str()),
                           atol(port.c_str())));
              cout << "Added " << ipa << ":" << port << " to cache" << endl;
              if (gcIPAddrs.size() > maxIP)
                { // Too many, remove one
                  gcIPAddrs.pop_front();
                }
            }
          j = s.find("url=");
          if (j != string::npos)
            { // Found url update erquest
            }
        }
      return; // Done
    }
  // Check HTTP Requests
}

void  WebServer::SendHostFile(L4Map_t::iterator i, L4Protocol* l4)
{
  Data d;
  deque<IPPort_t>::iterator j;
  if (!gcIPAddrs.size())
    { // deubg...just add one
      gcIPAddrs.push_back(IPPort_t(IPAddr("192.168.100.100"), 6347));
    }
  for (j = gcIPAddrs.begin(); j != gcIPAddrs.end(); ++j)
    { // Send each ip address
      string ip(IPAddr(j->first));
      d.Add(ip.length(), ip.c_str());
      d.Add(1, ":");
      char work[128];
      sprintf(work, "%ld", j->second);
      d.Add(strlen(work), work);
      d.Add(1, "\n");
    }
  d.Add(1, "\n");
  d.Add(1, "\0");
  HTTPInfo& hInfo = i->second; // Get the http info record
  hInfo.totSent = d.Size();
  hInfo.totAck = 0;
  l4->Send(d); // Send the data
}

void  WebServer::SendURLFile(L4Map_t::iterator i, L4Protocol* l4)
{
}

void WebServer::LogTimeCount()
{
  if (logEnabled)
    {
      timeCountLog.push_back(
          TimeCount(Simulator::Now(), 
                    activeConnections.size()));
    }
}

// Static member functions
void  WebServer::DefaultMaxConnections(Count_t m) // Set maximum connections
{
  defaultMaxConnections = m;
}

void  WebServer::DefaultMaxIP(Count_t m) // Set maximum IP GCache
{
  defaultMaxIP = m;
}

void  WebServer::DefaultMaxURL(Count_t m) // Set maximum URL GCache
{
  defaultMaxURL = m;
}


