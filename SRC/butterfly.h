// Define a Butterfly Supercomputer Interconnect Network
// George F. Riley, Georgia Tech, Spring 2005

#ifndef __BUTTERFLY_H__
#define __BUTTERFLY_H__
#include <vector>
#include "common-defs.h"
#include "node.h"
#include "node-firstbit.h"

// Make a vector of node vectors. One per stage
typedef std::vector<NodeVec_t> NodeVecVec_t;

class Butterfly {
public:
  Butterfly(Count_t n, Count_t r, Count_t s, IPAddr_t lIP, IPAddr_t rIP);
  FirstBitRxNode* GetSwitch(Count_t s, Count_t i); // Get the i'th switch from stage s
  FirstBitRxNode* GetINode(Count_t i);             // Get the i'th input node
  FirstBitRxNode* GetONode(Count_t i);             // Get the i'th output node 
  void  BoundingBox(const Location&, const Location&); // Lower left, up right
public:
  Count_t      nNodes;                   // Number input/output nodes
  Count_t      radix;                    // Radix (links per switch)
  Count_t      stages;                   // Number stages
  NodeVec_t    iNodes;                   // Input nodes
  NodeVec_t    oNodes;                   // Output nodes
  NodeVecVec_t switches;                 // Router switches
};

#endif
