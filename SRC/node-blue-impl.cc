// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//     
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


// Georgia Tech Network Simulator - Bluetooth Node Impl class
// George F. Riley, Georgia Tech, Spring 2004
#ifndef WIN32
#include <sys/time.h>
#else
#include <time.h>
#endif
#include "node-blue-impl.h"
#include "node-blue.h"
#include "baseband.h"
#include "lmp.h"
#include "l2cap.h"
#include "bnep.h"
#include "rng.h"
#include "qtwindow.h"

#ifdef HAVE_QT
#include <qcanvas.h>
#include <qpoint.h>
#endif

using namespace std;

NodeBlueImpl::NodeBlueImpl(Node* n)
    : NodeReal(n) {
  pBaseband = new BaseBand;
  pBaseband->AttachNode((BlueNode*) n); 
  pLMP = new LMP(pBaseband);
  pBaseband->InstallLMP(pLMP);
  pL2CAP = new L2cap; 
  pBaseband->InstallL2CAP(pL2CAP);
  pBNEP = new Bnep(pL2CAP); 
}

NodeBlueImpl::~NodeBlueImpl() {
  delete pBaseband;
  delete pLMP;
  delete pL2CAP;
  delete pBNEP;
}
void
NodeBlueImpl::AllocBdAddr() {
  //automatically allocate
  for(;;) {
    Uniform valRand1(0, 1);
    Uniform valRand2(0, 1);
    Uniform valRand3(0, 1);
    localAddr.usAddr[0] = 1+(int)(65535*valRand1.Value());
    localAddr.usAddr[1] = 1+(int)(65535*valRand2.Value());
    localAddr.usAddr[2] = 1+(int)(65535*valRand3.Value());
    if(((localAddr.usAddr[1]&0x0F)==0x009E)
        &&(localAddr.usAddr[0]<=0x8B3E)
        &&(localAddr.usAddr[0]>=0x8B00))
      continue; //reserved LAP addr
    else
      break;
  }
  return;
}

//lookup Node according to BdAddr
BlueNode* 
NodeBlueImpl::LookupNode(BdAddr addr) {
  BlueNode *pNodeList = static_cast<BlueNode*>(pNode);
  BlueNodeVec_t::iterator i = pNodeList->Bluenodes.begin();
  for(;i!=pNodeList->Bluenodes.end(); i++) {
    BdAddr tmpAddr = ((BlueNode *)(*i))->GetBdAddr();
    if(!memcmp(&tmpAddr, &addr, sizeof(addr)))
      return (BlueNode *)(*i); 
  }
  return NULL;
}

//update the neighbor in my list.
void
NodeBlueImpl::UpdateNeighbor() {
  BlueNeighborVec_t::iterator i=blueNb.begin();
  for(;i!=blueNb.end(); i++) {
    BlueNeighbor *pNb= (BlueNeighbor *)(&(*i));
    if(static_cast<NodeBlueImpl*>((pNb->pBlueNode)->pImpl) == this)
      continue;
    if(!(pBaseband->isWithinRange(pNb->pBlueNode))) {
      blueNb.erase(i);
      i--;
      cout << "Node" << &(*i) << "is out of range." << endl;
    }
  }
  
  /*BlueNeighbor::iterator i=blueNb.begin();
  for(;i!=blueNb.end(); i++) {
    BlueNode *pNb = (BlueNode *)(*i);
    if(static_cast<NodeBlueImpl*>(pNb->pImpl) == this)
      continue;
    if(!(pBaseband->isWithinRange(pNb))) {
      blueNb.erase(i);
      i--;
      cout << "Node" << i << "is out of range." << endl;
    }
  }*/
  
  return;
}

bool
NodeBlueImpl::LookupNeighbor(BlueNode *pBlueNode) {
  BlueNeighborVec_t::iterator i=blueNb.begin();
  for(;i!=blueNb.end(); i++) {
    if(pBlueNode == (BlueNode *)(((BlueNeighbor *)(&(*i)))->pBlueNode) )
      return true;
  }
  
  /*BlueNeighbor::iterator i=blueNb.begin();
  for(;i!=blueNb.end(); i++) {
    if(pBlueNode == (BlueNode *)(*i))
      return true;
  }*/
  return false;
}

void
NodeBlueImpl::Start(RoleType myRole, double t) {
  pBaseband->Start(myRole, t);
  return;
}

#ifdef HAVE_QT
QCanvasItem* NodeBlueImpl::Display(QTWindow* qtw)
{
  if (!qtw) return nil;
  QPoint p = qtw->LocationToPixels(GetLocation());
  return Display(p, qtw);

}

QCanvasItem* NodeBlueImpl::Display(const QPoint& p, QTWindow* qtw)
{
  if (!qtw) return nil; // Not animating
  QCanvas* canvas = qtw->Canvas();
  CustomShape_t cs;
  if (!animInfo)
    {
      animInfo = new NodeAnimation(); // Allocate the animation info
      animInfo->show = HasLocation();
    }
  if (Shape() != Node::IMAGE && !animInfo->nodePolygon)
    { // No existing polygon, make one
      switch(Shape()) {
        case Node::NONE:
          return nil;
        case Node::CIRCLE:
          animInfo->nodePolygon =
              new QCanvasEllipse(animInfo->pixelSizeX, animInfo->pixelSizeY,
                                 canvas);
          animInfo->nodePolygon->move(p.x(), p.y());
          break;
        case Node::SQUARE:
          animInfo->nodePolygon = new QCanvasRectangle(
              p.x() - animInfo->pixelSizeX / 2,
              p.y() - animInfo->pixelSizeY / 2,
              animInfo->pixelSizeX,
              animInfo->pixelSizeY,
              canvas);
           break;
        case Node::HEXAGON:
          break;
        case Node::OCTAGON:
          break;
        case Node::CUSTOM:
          cs = CustomShape();
          if (cs)
            {
              animInfo->nodePolygon = cs(canvas, pNode, p, animInfo->nodePolygon);
            }
          break;
        default:
          break;
      }
      if (!animInfo->nodePolygon) return nil; // Oops!  No known shape
      animInfo->nodePolygon->show();
    }
  if (IsMobile() || animInfo->placeImage)
    { // Need to (possibly) move to new location
      if (Shape() == Node::CUSTOM)
        { // Custom items may redraw depending on motion, we need to call
          cs = CustomShape();
          if (cs)
            {
              animInfo->nodePolygon = cs(canvas,pNode,p, animInfo->nodePolygon);
            }
        }
      else
        {
          QCanvasItem* pi;
          if (Shape() == Node::IMAGE)
            pi = animInfo->nodeSprite;
          else
            pi = animInfo->nodePolygon;
          pi->move( p.x() - animInfo->pixelSizeX / 2,
                    p.y() - animInfo->pixelSizeY / 2);
          animInfo->placeImage = false;
          if (Shape() == Node::IMAGE)
            { // We want images behind links and packets
              pi->setZ(-1);
            }
          else
            {
              pi->setZ(1.0); // We want nodes "in front of" links
            }
        }
    }
  
  QCanvasPolygonalItem* pi = (QCanvasPolygonalItem*)animInfo->nodePolygon;
  int FreqColor = (int)pBaseband->Freq();
  //cout<<"FreqColor "<<FreqColor<<endl;
  if(FreqColor%3 == 0) {
    pi->setBrush(QColor(FreqColor*3, FreqColor*3, 255));
    red = FreqColor*3;
    green = FreqColor*3;
    blue = 255;
  }
  else if(FreqColor%3 == 1) {
    pi->setBrush(QColor(FreqColor*3, 255, FreqColor*3));
    red = FreqColor*3;
    green = 255;
    blue = FreqColor*3;
  }
  else {
    pi->setBrush(QColor(255, FreqColor*3, FreqColor*3));
    red = 255;
    green = FreqColor*3;
    blue = FreqColor*3;
  }
  
  return animInfo->nodePolygon;

}

int
NodeBlueImpl::GetRed() {
  return red;
}

int
NodeBlueImpl::GetGreen() {
  return green;
}
int
NodeBlueImpl::GetBlue() {
  return blue;
}
#endif  

