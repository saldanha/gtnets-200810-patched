// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-gnutella.cc 185 2004-11-05 16:40:27Z riley $



// Implementation of the Gnutella Peer-to-Peer Protocol
// George F. Riley, Georgia Tech, Spring 2003

#include <stdio.h>

#ifdef HAVE_QT
#include <qnamespace.h>
#endif

#include "application-gnutella.h"
#include "tcp.h"
#include "rng.h"

using namespace std;

// Static members
IPAddrSet_t Gnutella::gcacheHosts;        // IPAddress of list of GCache's
PortId_t    Gnutella::defaultPort = 6346; // Default port for incoming report
Count_t     Gnutella::outPeerCount = 4;   // Limit on desired out connections
Count_t     Gnutella::inPeerCount = 4;    // Limit on desired in connections
Count_t     Gnutella::transferCount = 4;  // Limit on simulataneous tranfers

// Gnutella specific methods

Gnutella::Gnutella() 
  : node(nil),
    gcacheTCPh(nil),
    gcacheTCPu(nil),
    listenTCP((TCP*)TCP::Default().Copy()),
    gcacheActive(false),
    gcacheIP(IPADDR_NONE),
    gnuPort(defaultPort),
    outConnectionLimit(outPeerCount), inConnectionLimit(inPeerCount),
    transferLimit(transferCount),
    goodOutConnections(0), failedOutConnections(0),
    goodInConnections(0), rejectedInConnections(0),
    gcacheRequests(0), gcacheFailed(0)
{
  CopyOnConnect(false); // Do not copy this app on incomming connections
  listenTCP->AttachApplication(this);
}

Gnutella::Gnutella(const TCP& t) 
  : node(nil),
    gcacheTCPh(nil),
    gcacheTCPu(nil),
    listenTCP((TCP*)t.Copy()),
    gcacheActive(false),
    gcacheIP(IPADDR_NONE),
    gnuPort(defaultPort),
    outConnectionLimit(outPeerCount), inConnectionLimit(inPeerCount),
    transferLimit(transferCount),
    goodOutConnections(0), failedOutConnections(0),
    goodInConnections(0), rejectedInConnections(0),
    gcacheRequests(0), gcacheFailed(0)
{
  CopyOnConnect(false); // Do not copy this app on incomming connections
  listenTCP->AttachApplication(this);
}

Gnutella::Gnutella(const Gnutella& c)
  : Application(c),
    node(nil),
    gcacheTCPh(nil),
    gcacheTCPu(nil),
    listenTCP((TCP*)c.listenTCP->Copy()),
    gcacheActive(false),
    gcacheIP(c.gcacheIP),
    gnuPort(c.gnuPort),
    outConnectionLimit(c.outConnectionLimit),
    inConnectionLimit(c.inConnectionLimit),
    transferLimit(c.transferLimit),
    outConnections(c.outConnections),
    inConnections(c.inConnections),
    transferConnections(c.transferConnections),
    goodOutConnections(0), failedOutConnections(0),
    goodInConnections(0), rejectedInConnections(0),
    gcacheRequests(0), gcacheFailed(0)
{
  listenTCP->AttachApplication(this);
}

Gnutella::~Gnutella()
{
  //delete listenTCP;
}

void Gnutella::Receive(Packet* p, L4Protocol* l4,Seq_t)
{   // Data received
  if (l4 == gcacheTCPh)
    {
      cout << "Gnutella " << this << " got response from gcache" << endl;
      Data* d = (Data*)p->PeekPDU();
      if (d->Contents())
        {
          string r(d->Contents());
          cout << "Response is " << r <<endl;
          gcacheRequests++;
          while(r.length())
            {
              string::size_type j = r.find('\n');
              if (j > 0)
                { // Found a non-empty line
                  string t = r.substr(0, j);
                  string::size_type k = t.find(':');
                  string ipa = t.substr(0, k);
                  string port = t.substr(k+1, string::npos);
                  AddPossiblePeer(IPPort_t(
                                           IPAddr(ipa.c_str()),
                                           atol(port.c_str())));
                }
              r = r.substr(j+1, string::npos);  // remove this one
            }
          ConnectToPeer(); // Try a new connection(s)
        }
    }
  else if (l4 == gcacheTCPu)
    { // ?? Do we get a response to the url update?
    }
  else
    {
      cout << "Gnutella " << this << " got message from peer" << endl;
    }
  delete p;
}

void Gnutella::Sent(Count_t, L4Protocol*)
{     // Data has been sent
}

void Gnutella::CloseRequest(L4Protocol* l4)
{      // Close request from peer
  l4->Close(); // Just close in response
  if (l4 == gcacheTCPh)
    {
      // Time to post the update
      // Make a copy of the listening tcp endpoint.
      gcacheTCPu = (TCP*)listenTCP->Copy();
      gcacheTCPu->AttachApplication(this);
      gcacheTCPu->Attach(node);
      gcacheTCPu->DeleteOnTWait();        // Delete this one when done
      gcacheTCPu->Bind();                 // Get a local port number
#ifdef HAVE_QT
      gcacheTCPu->SetColor(Qt::green);    // Use green for updates
#endif
      gcacheTCPu->Connect(gcacheIP, 80);  // Establish another connection
      gcacheActive = false;
      if (outConnections.size() < outConnectionLimit)
        { // Need more hosts to choose from
          GCacheConnect();
        }
    }
}

void Gnutella::Closed(L4Protocol*)
{ // Connection has closed
}

void Gnutella::ConnectionComplete(L4Protocol* l4) // Connection rq success
{
  if (l4 == gcacheTCPh)
    {
      cout << "Gnutella::ConnectionComplete sending hostfile" << endl;
      string hostfile("gcache.php?hostfile=1\n");
      Data d(hostfile);
      l4->Send(d);
      return;
    }
  if (l4 == gcacheTCPu)
    {
      cout << "Gnutella::ConnectionComplete sending update" << endl;
      string ipUpdate("gcache.php?ip=");
      char work[255];
      sprintf(work, "%ld", gnuPort);
      ipUpdate = ipUpdate + 
        (string)IPAddr(gcacheTCPu->localNode->GetIPAddr()) +
        string(":") +
        string(work);
            
      Data d1(ipUpdate);
      cout << "Gnutella update sending " << d1.Contents() << endl;
      gcacheTCPu->Send(d1);
      return;
    }
  // Check for peer connection
  PeerList_t::iterator i = FindPeerInfo(outConnections, (TCP*)l4);
  if (i != outConnections.end())
    { // Successful connection
      i->connected = true;
      cout << "Gnutella " << (string)IPAddr(node->GetIPAddr())
           << " successful connection to peer " 
           << (string)IPAddr(i->ipPort.first)
           << " port " << i->ipPort.second << endl;
      goodOutConnections++;
    }
}

// Connection request failed
void Gnutella::ConnectionFailed(L4Protocol* l4, bool aborted)
{
  if (l4 == gcacheTCPh)
    {
      cout << "Gnutella::ConnectionFailed gcache, retrying" << endl;
      gcacheActive = false;
      gcacheFailed++;
      GCacheConnect();
      return;
    }
  // Check for peer connection
  PeerList_t::iterator i = FindPeerInfo(outConnections, (TCP*)l4);
  if (i != outConnections.end())
    { // Out connection failed
      cout << "Gnutella " << (string)IPAddr(node->GetIPAddr())
           << " failed connection to " << (string)IPAddr(i->ipPort.first)
           << " port " << i->ipPort.second << endl;
      failedOutConnections++;
      outConnections.erase(i); // Remove from list
      ConnectToPeer(); // Try another
      return;
    }
  // Check for file transfer connection
}

bool Gnutella::ConnectionFromPeer(L4Protocol* l4, IPAddr_t i, PortId_t p)
{// Listener has cn rq from peer with specified ip/port
  // ?? Is this right?
  if (inConnections.size() >= inConnectionLimit)
    {
      rejectedInConnections++;
      return false; // Cannot accept
    }
  inConnections.push_back(
      GnuPeerInfo((TCP*)l4,
                  IPPort_t(i,p)));             // Add a new connection
#ifdef HAVE_QT
  l4->SetColor(Qt::blue);                      // Use blue for p2p connections
#endif
  GnuPeerInfo& gpi = outConnections.back();    // Get the one just added
  gpi.connected = true;                       // Note connected
  cout << "Gnutella " << (string)IPAddr(node->GetIPAddr())
       << " accepted connection from " << (string)IPAddr(i)
       << " port " << p << endl;
  goodInConnections++;
  return true;
}

void Gnutella::StartApp()
{ // Called at time specified by Start
  GCacheConnect(); // Initiate a GCache connection
}
void Gnutella::StopApp()
{     // Called at time specified by Stop
}

void Gnutella::AttachNode(Node* n)
{
  node = n;
  listenTCP->Attach(n);
  if (!listenTCP->Bind(gnuPort))
    {
      cout << "Gnutella::AttachNode, can't bind port " << gnuPort << endl;
    }
  listenTCP->Listen();

}

Application* Gnutella::Copy() const
{    // Make a copy of the application
  return new Gnutella(*this);
}

void Gnutella::DBDump()
{ // Print out debug info
  cout << "Debug info from Gnutella " << (string)IPAddr(node->GetIPAddr())
       << " port " << gnuPort << endl
       << "    Active outconnections " << outConnections.size()
       << " goodOutConnections " << goodOutConnections
       << " failedOutConnections " << failedOutConnections << endl
       << "    inConnections " << inConnections.size()
       << " goodInConnections " << goodInConnections
       << " rejectedInConnections " << rejectedInConnections << endl
       << "    gcacheRequests " << gcacheRequests
       << " gcacheFailed " << gcacheFailed << endl;
}

// Private members
void Gnutella::GCacheConnect()      // Initiate a gcache connection
{
  if (gcacheActive) return;         // Already connecting
  if (gcacheHosts.size() == 0)
    {
      cout << "GnutellaStartApp, no gcache's to choose from" << endl;
      return;
    }
  Uniform r(0, gcacheHosts.size());
  Count_t gc = r.IntValue(); // Choose a random gcache
  IPAddrSet_t::iterator i = gcacheHosts.begin();
  for (Count_t j = 0; j < gc; ++j) i++; // Advance interator random times
  gcacheIP = *i;                        // Get the IPAddress of the gcache
  gcacheTCPh = (TCP*)listenTCP->Copy(); // Get a tcp to use
  gcacheTCPh->AttachApplication(this);
  gcacheTCPh->Attach(node);             // Attach to this node
  gcacheTCPh->DeleteOnTWait();          // Delete this agent when done
  gcacheTCPh->Bind();                   // Get transient port
#ifdef HAVE_QT
  gcacheTCPh->SetColor(Qt::red);        // Use green for host file requests
#endif
  gcacheTCPh->Connect(gcacheIP, 80);    // Connect to the gcache
  gcacheActive = true;
  cout << "Gnutella::GCacheConnect " << this
       << " connecting to gcache " << (string)IPAddr(gcacheIP) << endl;
}

void Gnutella::AddPossiblePeer(const IPPort_t& ipport)
{
  cout << "Adding peer " << (string)IPAddr(ipport.first)
       << " port " << ipport.second << endl;
  if (node->LocalIP(ipport.first)) return; // Don't add self
  // First see if duplicate
  deque<IPPort_t>::iterator i;
  for (i = pendingHosts.begin(); i != pendingHosts.end(); ++i)
    {
      if (ipport == *i) break;    // Found duplicate
    }
  if (i == pendingHosts.end())
    { // Not duplicate, insert this one
      cout << "Not duplicate, adding" << endl;
      pendingHosts.push_back(ipport); // Add to list of potential hosts
    }
  
}

void Gnutella::ConnectToPeer()
{
  cout << "CTP oc.size() " << outConnections.size()
       << " oclimit " << outConnectionLimit << endl;
  while (outConnections.size() < outConnectionLimit)
    { // Need more
      if (pendingHosts.empty()) break;         // No more known peers
      IPPort_t ipport = pendingHosts.front();  // Get the next
      pendingHosts.pop_front();                // And remove
      outConnections.push_back(
          GnuPeerInfo((TCP*)listenTCP->Copy(),
                      ipport));                // Add a new connection
      GnuPeerInfo& gpi = outConnections.back();// Get the one just added
      // Initialize the TCP endpoint and start the connection
#ifdef HAVE_QT
      // Use blue for peer connections
      gpi.tcp->SetColor(Qt::blue);
#endif
      gpi.tcp->AttachApplication(this);
      gpi.tcp->Attach(node);
      gpi.tcp->DeleteOnTWait();        // Delete this one when done
      gpi.tcp->Bind();                 // Get a local port number
      // Establish another connection
      cout << "Gnutella " << (string)IPAddr(node->GetIPAddr())
           << " connecting to " << (string)IPAddr(gpi.ipPort.first)
           << " port " << gpi.ipPort.second << endl;
      gpi.tcp->Connect(gpi.ipPort.first, gpi.ipPort.second);
    }
  if (outConnections.size() < outConnectionLimit)
    { // Need more hosts to choose from
      GCacheConnect();
    }
}

PeerList_t::iterator Gnutella::FindPeerInfo(PeerList_t& peers , TCP* t)
{
  PeerList_t::iterator i;
  for (i = peers.begin(); i != peers.end(); ++i)
    {
      if (i->tcp == t) return i; // Found it
    }
  return i; // Not found
}


// Static members
void Gnutella::AddGCache(IPAddr_t ip)
{
  gcacheHosts.insert(ip); // Add to the list of known gcaches
}
