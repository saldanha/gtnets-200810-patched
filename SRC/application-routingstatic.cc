

#include <vector>


#include "application-routingstatic.h"
#include "bfs.h"
#include "interface.h"


using namespace std;

ApplicationRoutingStatic::ApplicationRoutingStatic()
{
  variable1=0;
}

ApplicationRoutingStatic::~ApplicationRoutingStatic()
{
}

void ApplicationRoutingStatic::StartApp()
{
  InitializeRoutes();
}

void ApplicationRoutingStatic::StopApp()
{
  RouteTable* rtetable = l4proto->localNode->getRouteTable();
  rtetable->Flush(); /* Clean the routing table */
  return;
}

void ApplicationRoutingStatic::DefaultRoute(rt_value_t* value)
{
  rt_key_t* key = new rt_key_t(0,0);
  RouteTable* rtetable = l4proto->localNode->getRouteTable();

  rtetable->AddRoute(key, value);
}

void ApplicationRoutingStatic::ReInitializeRoutes(void)
{
	return;
}

void ApplicationRoutingStatic::InitializeRoutes(void)
{
  Node* thisnode = l4proto->localNode;

  Count_t nc = thisnode->NeighborCount();
  if (nc <=1 ) {
    /* Add that neighbor as the default gateway ??*/
    return;
  }
  
  NodeIfVec_t nextHop;      // Next hop vector
  NodeVec_t   parent;       // Parent vector

  const NodeVec_t&  nodes = Node::GetNodes();
  IPAddrVec_t       aliases;

  BFS(nodes, thisnode, nextHop, parent, IPADDR_NONE, aliases);

  // First count the most common nexthop, and use that for default
  vector<Count_t> c(nodes.size(), 0);
  Count_t maxCount = 0;      // Maximum value found
  NodeIf  maxNodeIf(nil,nil);

  for (NodeVec_t::size_type i = 0; i < nextHop.size(); ++i)
    {
      if (nextHop[i].node)
        { // Next hop exists
          if (++c[nextHop[i].node->Id()] > maxCount)
            { // Found new maximum
              maxCount  = c[nextHop[i].node->Id()];
              maxNodeIf = nextHop[i];
            }
        }
    }

  if (maxNodeIf.iface)
    { // At least one found
      DefaultRoute(new rt_value_t(maxNodeIf.iface->NodePeerIP(maxNodeIf.node),
		   maxNodeIf.iface ));
    }

  // Now create routing table entries
  for (NodeVec_t::size_type i = 0; i < nextHop.size(); ++i)
    {
      Node*      target = nodes[i];
      Node*      node = nextHop[i].node;
      Interface* iface = nextHop[i].iface;
      if (node)
        { // Node exists
          if (node  != maxNodeIf.node ||
              iface != maxNodeIf.iface)
            { // Non-default found, add the routes
              // Get a list of all IP addresses for the target node
              IPMaskVec_t ips;
              target->IPAddrs(ips);
              for (IPMaskVec_t::size_type j = 0; j < ips.size(); ++j)
                {
                  // Add to routing table
		  rt_key_t* key = new rt_key_t(ips[j].ip,
					       Mask(ips[j].mask).NBits());

		  rt_value_t* value = new rt_value_t(iface->NodePeerIP(node),
						     iface);
		  thisnode->getRouteTable()->AddRoute(key, value);
                }
            }
        }
    }
  
}
