// Test the Satellite node
// George F. Riley, Georgia Tech, Fall 2002

#include <iostream>
#include <sstream>
#include <string>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "node-satellite.h"
#include "star.h"
#include "linkp2p.h"
#include "application-cbr.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "application-udpsink.h"
#include "rng.h"
#include "routing.h"
#include "routing-manual.h"
#include "diffserv-queue.h"
#include "time-value.h"
#include "time-value-graph.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "udp.h"
#include "validation.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

using namespace std;

// Make the nodes global so we can access them in the progress callback
Node*          ship;
Node*          conus;
SatelliteNode* sat1;
Node*          uav;
SatelliteNode* sat2;
DiffServQueue* q;
L4Protocol*    uavL4;

Count_t progressCount;

// Progress hook
static void Progress(Time_t now)
{
  cout << "Progress to " << now << endl;
  ++progressCount;
  if (progressCount == 5)
    { // Time to crash the uav to sat2 link, send through ship
      cout << "UAV Satellite Link Failure. Switching to ship" << endl;
      Interface* i = uav->GetIfByNode(sat2);
      i->Down(); // This link goes down
      // Adjust routing to route through ship
      uav->GetRouting()->Default(RoutingEntry(uav->GetIfByNode(ship),
                                              IPAddr("192.169.0.1")));
    }
  if (progressCount == 10)
    { // Change the DS codepoint mapping to give priority to uav traffic
      cout << "Increasing priority on UAV traffic" << endl;
      uavL4->TOS(1);
    }
}


int main(int argc, char** argv)
{
  Simulator s;
  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);

  // Set manual routing
  Routing::SetRouting(new RoutingManual());
  
  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->Open("testsat1.txt");
  IPV4::Instance()->SetTrace(Trace::ENABLED);

  s.StartAnimation(0, true);

  ship  = new Node();
  conus = new Node();
  sat1  = new SatelliteNode();
  uav   = new Node();
  sat2  = new SatelliteNode();
  
  ship->CustomShapeFile("Ship.png");
  conus->CustomShapeFile("SatGroundLeft.png");
  //uav->CustomShapeFile("Aircraft.png");
  uav->CustomShapeFile("UAV.png");
  
  ship->SetLocation (1.5, 1.0);
  sat1->SetLocation (2.5, 2.0);
  conus->SetLocation(3.5, 1.0);
  uav->SetLocation  (1.0, 1.5);
  sat2->SetLocation (2.5, 3.0);

  // Create a star on the ship with VOIP connections
  Star st(4, ship, Linkp2p(Rate("1Mb"), Time("1ms")), IPAddr("192.170.0.1"));
  st.BoundingBox(Location(0,0), Location(0.5, 0.5), -M_PI / 2.0, M_PI / 2.0);
  
  // Make the displayed region slightly larger
  s.NewLocation(0.5, 0.5);
  s.NewLocation(4.0, 3.5);

  Linkp2p lk(Rate("128Kb"), Time("120ms"));
  Linkp2p lk1(Rate("1Mb"), Time("5ms"));
  lk.BitErrorRate(1e-6); // Satellite bit error rate
  
  ship->AddDuplexLink(sat1, lk,  IPAddr("192.168.0.1"), Mask(32),
                                 IPAddr("192.168.0.2"));
  conus->AddDuplexLink(sat1, lk, IPAddr("192.168.1.2"), Mask(32),
                                 IPAddr("192.168.1.1"));
  uav->AddDuplexLink(sat2, lk,   IPAddr("192.169.1.1"), Mask(32),
                                 IPAddr("192.169.1.2"));
  conus->AddDuplexLink(sat2, lk, IPAddr("192.169.3.2"), Mask(32),
                                 IPAddr("192.169.3.1"));
  uav->AddDuplexLink(ship, lk1,  IPAddr("192.169.0.2"), Mask(32),
                                 IPAddr("192.169.0.1"));

  // Set up proper routes
  ship->GetRouting()->Default(RoutingEntry(ship->GetIfByNode(sat1),
                                         IPAddr("192.168.0.2")));
  uav->GetRouting()->Default(RoutingEntry(uav->GetIfByNode(sat2), 
                                          IPAddr("192.169.1.2")));

  // Set a diffserv queue at ship to sat1 queue
  ship->GetIfByNode(sat1)->SetQueue(DiffServQueue(2, lk.Bandwidth()));
  q = (DiffServQueue*)ship->GetQueue(sat1);
  // Allocate 1/8 to best effort, 7/8 to uav traffic
  Rate_t m = lk.Bandwidth() / 8.0;
  
  q->RateAllocation(0,  m);
  q->RateAllocation(1,  m * 7);
  // Try m/4 for best effort and 3m/4 for uav
  m = lk.Bandwidth() / 4.0;
  q->RateAllocation(0,  m);
  q->RateAllocation(1,  m * 3);
  
  // However, initially map both tos 0 and tos 1 to priority 0
  //q->PriorityMap(0, 0);
  //q->PriorityMap(1, 0);
  // Set the the limit to just a few packet
  q->SetLimitPkts(30);
  // Animate the queue
  q->Animate(true);

  // Set up VOIP applications on the star nodes.
  // Use bw/(k) * loadfactor data rate for each, where bw is the bandwidth of
  // the satellite uplink, and k is number of star nodes.
  // This uses more than 100% of satellite uplink and starts the
  // congestion.
#define PORT1 12340
#define PORT2 13000

  Rate_t rate = lk.Bandwidth() / st.LeafCount();
  Mult_t loadFactor = 2.0;
  
  rate *= loadFactor;
  
  Uniform startRNG(0, 0.1);
  for (Count_t i = 0; i < st.LeafCount(); ++i)
    {
      Node* leaf = st.GetLeaf(i);
      CBRApplication* cbrApp =
          (CBRApplication*)leaf->AddApplication(
              CBRApplication(conus->GetIPAddr(), PORT1 + i, NO_PORT, rate));
      cbrApp->Start(startRNG.Value());
    }
  
  // And start the application for uav
  Rate_t uavRate = Rate("64Kb");
  CBRApplication* cbrApp1 =
      (CBRApplication*)uav->AddApplication(
          CBRApplication(conus->GetIPAddr(), PORT2, NO_PORT, uavRate));
  uavL4 = cbrApp1->GetL4();
  uavL4->SetColor(Qt::green);
  // Set the type of service field to 0.  Will later remap to 1
  uavL4->TOS(0);
  cbrApp1->Start(0);
  
  // Add a UDP Sink app at CONUS to measure loss rate
  UDPSink* udpSink = (UDPSink*)conus->AddApplication(UDPSink(PORT2));
  // Add a statistics measurement for loss rate vs. time
  //Statistics* stats = udpSink->SetStatistics(TimeValueStats());
  Statistics* stats = udpSink->SetStatistics(TimeValueGraph(0,20,0,uavRate));
  udpSink->StatsLogType(UDPSink::BANDWIDTH);
  udpSink->StatsUpdateInterval(1.0);
  udpSink->Start(0);
  
  // Specify animation
  s.AnimationUpdateInterval(Time("500us")); // 500us initial update rate
  s.ProgressHook(Progress);
  s.Progress(1);
  s.StopAt(100);
  s.Run();
  ofstream ofs("LossVsTime.txt");
  stats->Log(ofs);
  ofs.close();
}
