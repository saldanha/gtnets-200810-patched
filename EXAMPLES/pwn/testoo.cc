// Test the On-Off data source appication
// George F. Riley.  Georgia Tech, Spring 2002

#include "simulator.h"
#include "application-onoff.h"
#include "duplexlink.h"
#include "queue.h"
#include "ratetimeparse.h"
#include "udp.h"
#include "dumbbell.h"
#include "droptail-ipa.h"
#include "validation.h"  

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  int qLimit = 10;
  if (argc > 1) qLimit = atol(argv[1]);
  int nSource = 20;

  Time_t duration = Time("100ms");
  Mult_t dutyCycle = 0.5;  // On half, off half (on average)
  Mult_t rho = 0.9;        // Traffic intensity
  Rate_t bw = Rate("10Mb");// Bottleneck link rate
  Rate_t cbrRate = (bw * rho) / (nSource * dutyCycle);
  Size_t pktSize = 512;
  Linkp2p l;
  l.Bandwidth(Rate("100Mb"));
  for (qLimit = 10; qLimit < 50; qLimit++)
    {
      Simulator s;
      Queue::Default(DropTailIPA(qLimit*pktSize)); // Use IPA Queue by default
      Dumbbell b(nSource, nSource, 0.1,
             IPAddr("192.168.1.0"), IPAddr("192.169.1.0"), l);
      DropTailIPA* q = (DropTailIPA*)b.LeftQueue();
      //q->Detailed(false); // Use the non-detailed model
      for (Count_t i = 0; i < b.LeftCount(); ++i)
        { // Add an exponential On/Off source for each leaf
          OnOffApplication* ooa = 
                (OnOffApplication* )b.Left(i)->AddApplication(
                                OnOffApplication(
                              b.Right(i)->GetIPAddr(), 1000, 
                              Exponential(duration * dutyCycle), // On period
                              Exponential(duration * (1 - dutyCycle)), // Off
                              UDP(), cbrRate));
          ooa->Start(0.0); // Start at time 0
          ooa->Stop(100.0);
        }
      s.Progress(1);      
      s.StopAt(100);
      s.Run();
      //Stats::Print(); // Print the statistics
      cout << "QLimit " << qLimit 
           << " total Losses " << q->Losses()
           << " dL1dB1 " << q->DL1dB1() 
           << " prediction " << q->Losses() - q->DL1dB1() << endl;
      cout << "         "
           << " workload " << q->TotalWorkload()/pktSize
           << " dQ1dB1 " << q->DQ1dB1() 
           << " prediction " << q->TotalWorkload()/pktSize + q->DQ1dB1()
           << endl;
    }
      //cout << "Bottleneck Link Utilization " << b.LeftLink()->Utilization() << endl;
}

  
