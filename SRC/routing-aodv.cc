// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: routing-aodv.cc 135 2004-10-07 13:38:15Z riley $


// Georgia Tech Network Simulator - AODV Routing class
// George F. Riley.  Georgia Tech, Summer 2003

//
// AODV routing.

#include <string>
#include <iostream>
#include <assert.h>

// Uncomment below to enable debug level 0
// #define DEBUG_MASK 0x01
#include "debug.h"
#include "routing-aodv.h"
#include "mask.h"
#include "ipaddr.h"

using namespace std;

AODVRoutingEntry::AODVRoutingEntry(){
	DBG((Stats::routingEntAllocated++));
	int i;

	ReqTimeout = 0.0;
	ReqCount = 0;
	
	SeqNo = 0;
	Hops = LastHopCount = INFINITY2;
	Expire = 0.0;
	Flags = RTF_DOWN;

	for (i=0; i<MAX_HISTORY; i++)
		DiscLatency[i] = 0.0;
	
	HistoryIndex = 0;
	RequestLastTTL = 0;

	pInterface= 0; 
	DstIPAddr = IPADDR_NONE;
	Address = IPADDR_NONE; //this is actually next hop in ad hoc
}

AODVRoutingEntry::AODVRoutingEntry(Interface *pIf, IPAddr_t a){
	DBG((Stats::routingEntAllocated++));
	int i;

	ReqTimeout = 0.0;
	ReqCount = 0;
	
	SeqNo = 0;
	Hops = LastHopCount = INFINITY2;
	Expire = 0.0;
	Flags = RTF_DOWN;

	for (i=0; i<MAX_HISTORY; i++)
		DiscLatency[i] = 0.0;
	
	HistoryIndex = 0;
	RequestLastTTL = 0;

	pInterface= pIf;
	DstIPAddr = a;
	Address = IPADDR_NONE; //this is the next hop in ad hoc
}
	
AODVRoutingEntry::~AODVRoutingEntry() {
	// Delete each members in NeighborList and PrecursorList;
	 DBG((Stats::routingEntDeleted++));
	 while(!PrecursorList.empty()) {
		AODVPrecursor *pc = PrecursorList.front();
		PrecursorList.pop_front();
		if(pc)
		    delete pc;
	 }
	while(!NeighborList.empty()) {
		AODVNeighbor *nb = NeighborList.front();
		NeighborList.pop_front();
		if(nb)
			delete nb;
	}	
}

void
AODVRoutingEntry::NeighborInsert(IPAddr_t id) {
	//Insert the neighbor id into the list.		
	AODVNeighbor *pNeighbor = new AODVNeighbor(id);
	assert(pNeighbor);
	pNeighbor->Expire = 0;
	NeighborList.push_front(pNeighbor);
}	

AODVNeighbor *
AODVRoutingEntry::NeighborLookup(IPAddr_t id) {
	//look for if the neighbor is in the list
	for(AODVNeighborList_t::iterator i=NeighborList.begin();
					i!=NeighborList.end(); i++ ) {
		if((*i)->Address == id)
			return (*i);
	}
	return nil;
}

void
AODVRoutingEntry::PrecursorInsert(IPAddr_t id) {
	if(PrecursorLookup(id) == NULL) {
		AODVPrecursor *pPrecursor = new AODVPrecursor(id);
		assert(pPrecursor);
		PrecursorList.push_front(pPrecursor);
	}
}

AODVPrecursor *
AODVRoutingEntry::PrecursorLookup(IPAddr_t id) {
	for(AODVPrecursorList_t::iterator i = PrecursorList.begin(); 
					i!=PrecursorList.end(); i++) {
		if((*i)->Address == id)
			return (*i);
	}
	return NULL;
}

void 
AODVRoutingEntry::PrecursorDelete(IPAddr_t id) {
	for(AODVPrecursorList_t::iterator i = PrecursorList.begin();
					i!=PrecursorList.end(); i++) {
		if((*i)->Address == id) {
			AODVPrecursor *pPrecursor = *i;
			PrecursorList.erase(i);
			delete pPrecursor;
			break;
		}
	}
}

// if no parameters, PrecursorDelete is to empty!
void 
AODVRoutingEntry::PrecursorDelete() {
	 while(!PrecursorList.empty()) {
		AODVPrecursor *pPrecursor = PrecursorList.front();
		PrecursorList.pop_front();
		if(pPrecursor)
				delete pPrecursor;
	}
}

bool
AODVRoutingEntry::PrecursorEmpty(void) {
	return PrecursorList.empty();
}

//
// The routing table related functions used by normal applications.
// TODO:
//      Maybe some functions to be erased.
//
void 
RoutingAODV::Default(AODVRoutingEntry r) {
  defaultRoute = new AODVRoutingEntry(r); //TODO: define a copyise contructor.
 // defaultRoute = r; // Set the default route
  DEBUG0((cout << "RoutingAODV::Default if " << r.pInterface
          << " ip " << (string)IPAddr(r.Address) << endl));
}

RoutingAODV::~RoutingAODV() {
	if(defaultRoute)
		delete defaultRoute;
}
//TODO:
//NOTE: Probably network mask will be used later.
// 
AODVRoutingEntry *
RoutingAODV::Add(IPAddr_t a, Mask_t m, Interface* i, IPAddr_t ip){
	assert(i!=NULL);//test purpose.
	AODVRoutingEntry *pRt = new AODVRoutingEntry(i, ip);
  	fib.push_front(pRt); // Insert in FIB
	return pRt;
}

AODVRoutingEntry * 
RoutingAODV::Lookup(Node*, IPAddr_t t) { 
	// Lookup routing entry for specified target address
	for (AODVFIB_t::reverse_iterator ri = fib.rbegin(); 
					ri != fib.rend(); ++ri) {
		 if((*ri)->DstIPAddr == t)
		 	return *ri; // Return the pointer to routing entry
    }
	return NULL; //in aodv no default route. 
}

RoutingAODV* 
RoutingAODV::Clone() {
		
	return new RoutingAODV();
}

RoutingAODV::RType_t RoutingAODV::Type() {
		
  return AODV; //defined in routing.h
   
}

Size_t RoutingAODV::Size() const{ 
	// For statistics, return the size (bytes) of the FIB
  return fib.size();
}
//NOTE:
//Normally the following function should be called
//because aodv routing entry maintained by aodv 
//"applications" (application-aodv.{cc,h}
//
void RoutingAODV::Delete(IPAddr_t id) {
	
	AODVRoutingEntry *rt = Lookup(nil, id);
	if(rt->DstIPAddr == id) {
        fib.remove(rt); // delete 
		delete rt; //remove it from memory
	}
	//TODO: add some debug info here.
}
