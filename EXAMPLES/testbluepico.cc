#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include "simulator.h"
#include "bluetus.h"
#include "node-blue.h"
#include "node-blue-impl.h"
#include "node.h"
#include "node-impl.h"
#include "node-real.h"
#include "baseband.h"
#include "ratetimeparse.h"
#include "l2cap.h"
#include "lmp.h"
#include "bnep.h"
#include "application-blue.h"

int main() {
	Simulator s;
	
	s.HasMobility(true);

	BlueNode *pBlueNode = new BlueNode[8];
	sleep(1);
	
	pBlueNode[0].SetLocation(10.0,5.0);
	pBlueNode[1].SetLocation(3.0,5.0);
	pBlueNode[2].SetLocation(15.0,5.0);
	pBlueNode[3].SetLocation(10.0,0.0);
	pBlueNode[4].SetLocation(10.0,10.0);
	pBlueNode[5].SetLocation(15.0,10.0);
	pBlueNode[6].SetLocation(5.0,10.0);
	pBlueNode[7].SetLocation(5.0,0.0);
	
	pBlueNode[0].Shape(Node::CIRCLE);

	int i;	
	for(i=0;i<8;i++) {
		BdAddr addr = pBlueNode[i].GetBdAddr();
		printf("Node %d addr =0x%x%x%x\n", i, addr.usAddr[2], addr.usAddr[1], addr.usAddr[0]);
	}

///*
	s.StartAnimation(20, true);
	s.AnimateBasebandTx(20.1, true);
	s.AnimationUpdateInterval(Time("100us"));
//*/
	pBlueNode[0].Start(MASTER, 4.2);
	for(i=1;i<8;i++)
		pBlueNode[i].Start(SLAVE, 4.0);
	
//create and configure the application 
	Time_t duration = Time("100ms");
    Mult_t dutyCycle = 0.2;  // On half, off half (on average)
	BlueApplication BlueApp(&pBlueNode[0], &pBlueNode[1], 
							Exponential(duration * dutyCycle), // On/Off period
							static_cast<NodeBlueImpl*>(pBlueNode[0].pImpl)->pBNEP, 100000);
	BlueApp.Start(20.0);
	
	s.StopAt(40.0);

	s.Run();

	delete []pBlueNode;

	return 0;
}
