// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: link-rti.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - Point to Point Link class
// George F. Riley.  Georgia Tech, Spring 2002

//#define DEBUG_MASK 0x04
//#define DEBUG_MASK 0x08
#include "debug.h"
#include "link-rti.h"
#include "distributed-simulator.h"
#include "macaddr.h"
#include "hex.h"
#include "ipv4.h"
#include "routing-nixvector.h"
#include "link.h"
#include "tcp.h"
#include "l2proto802.3.h"
#include "llcsnap.h"

using namespace std;

#ifdef HAVE_RTI
char       LinkRTI::rxbuf[MAX_MSG_LENGTH];
char       LinkRTI::txbuf[MAX_MSG_LENGTH];
#endif
Time_t     LinkRTI::minTxTime;
Count_t    LinkRTI::nUpdates;  // Count number of update messages
Count_t    LinkRTI::nReflects; // Count number of reflect messages
Count_t    LinkRTI::ignoredReflects; // Count number of ignored reflect msgs

// Constructors
LinkRTI::LinkRTI(IPAddr_t i, Mask_t m, Interface* interface) 
    : LinkReal(),
      ip(i), mask(m), peerIP(IPADDR_NONE), iFace(interface), pPeer(nil)
{
  ConstructorHelper(interface);
}

LinkRTI::LinkRTI(IPAddr_t i, Mask_t m, Interface* iface, Rate_t b, Time_t d, NodeId_t rId) 
    : LinkReal( b, d),
      ip(i), mask(m), peerIP(IPADDR_NONE), iFace(iface), pPeer(nil), ghost(rId)
{
  ConstructorHelper(iface);
}

LinkRTI::LinkRTI(const LinkRTI& c) 
  : LinkReal(c), ip(c.ip), mask(c.mask), peerIP(c.peerIP),
    peerMAC(c.peerMAC), iFace(c.iFace)
{
  ConstructorHelper(iFace);
}

Link* LinkRTI::Copy() const
{
  return new LinkRTI(*this);
}

bool LinkRTI::Transmit(Packet* p, Interface* i, Node*)
{
#ifdef HAVE_RTI
  Time_t txTime = ((Rate_t)p->Size() * 8) / Bandwidth(); // Time to transmit
  // Compute received time
  Time_t txDelay = Delay();
  Time_t jit = jitter->Value();
  Time_t rxTime = Simulator::Now() + txTime + txDelay + jit;
  if (busy) cout << "HuH?  LinkRTI " << this 
                 << " Transmit on busy link!" << endl;
  busy = true; // Set link busy
  // Create and schedule the packet transmit complete event
  LinkEvent* evTx = new LinkEvent(LinkEvent::PACKET_TX);
  evTx->numberBytes = p->Size();
  Scheduler::Schedule(evTx, txTime, this);
  DEBUG0((cout << "LinkRTI " << this << " Transmit, scheduling link free at "
          << txTime + Simulator::Now() << endl));
  struct MsgS* pMyMsg = (struct MsgS*)txbuf;// Use static allocated buffer
  pMyMsg->TimeStamp = rxTime;

  // Find size needed to serialize       
  Size_t sz = p->SSize() + sizeof(Size_t);
  if (sz + sizeof(MsgS) + sizeof(int) > MAX_MSG_LENGTH)
    {
      cout << "HuH?  Packet serialized size exceeds MAX_MSG_LENGTH" << endl;
      delete p;
      return false;
    }


  // Allocate a place for RTIKITnodeid
  unsigned int* pMyNodeId = (unsigned int*)&pMyMsg[1];
  // debug follows
  unsigned int* pMyDebugSize = (unsigned int*)&pMyNodeId[1];
  *pMyDebugSize = p->Size();                // Debug..store size
  // end debug
  char*  b = (char*)&pMyDebugSize[1];       // Data immediately follows nodeid
  Size_t saveSz = sz;                       // Save for RTI Update
  *pMyNodeId = RTIKIT_nodeid;               // Used to ignore own messages
  b = Serializable::PutSize(b, sz, sz);     // Add the total length to buffer
  p->Serialize(b, sz);                      // Serialize the packet
  // Send to peer using RTIKIT
  DEBUG(2,(cout << "Updating" << endl));
  // Debug follows
#undef VERBOSE_DEBUG1
#ifdef VERBOSE_DEBUG1
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU(2); // Get ip hdr (don't remove)
  TCPHeader* tcp = (TCPHeader*)p->PeekPDU(3); // Get the TCP header
  cout << "TX " << this
       << " sip " << (string)IPAddr(iphdr->src)
       << " sp " << tcp->sourcePort
       << " dip " << (string)IPAddr(iphdr->dst)
       << " dp " << tcp->destPort
       << " seq " << tcp->sequenceNumber 
       << " ack "<< tcp->ackNumber
       << " lth " << tcp->dataLength
       << endl;
  // End debug
#endif  
  RTI_UpdateAttributeValues(
      objInstance, pMyMsg,
      saveSz + sizeof(struct MsgS) + sizeof(Size_t) + sizeof(unsigned int),
      0);
  DEBUG(2,(cout << "Done" << endl));
  nUpdates++;
#endif
  DEBUG0((cout << "LinkRTI " << this << " Transmit on link " 
          << (string)IPAddr(ip) 
          << " time " << Simulator::Now() 
          << " pktsize " << p->Size()
          << " busy " << busy
          << endl));
  delete p; // Delete the packet since we forwarded it to peer (or ignored)
  // Add a notification to deque and sent next packet
  AddNotify(iFace, nil);
  return true;
}

bool LinkRTI::Transmit(Packet* p, Interface* i, Node* n, Rate_t)
{
  return Transmit(p, i, n);
}

MACAddr LinkRTI::IPToMac(IPAddr_t)
{ // Convert peer IP to peer MAC
  // For RTI Links we have no idea, so just return None.
  // Really need to implement ARP to do this
  return MACAddr::NONE; // Else none
}

Count_t LinkRTI::NeighborCount(Node*)
{ // Get number of routing neighbors.
  // One if peer known, zero otherwise
  if (pPeer) return 1;
  return 0;
}

void LinkRTI::Neighbors(NodeWeightVec_t& nwv, Interface* i, bool)
{ // Add to neighbor list if peer is known
  if (pPeer)
    {
      nwv.push_back(NodeIfWeight(pPeer->GetNode(), i, Weight()));
    }
}

void LinkRTI::AllNeighbors(NodeWeightVec_t& nwv)
{ // Add to neighbor list
  if (pPeer)
    {
      nwv.push_back(NodeIfWeight(pPeer->GetNode(), pPeer, Weight()));
    }
}

Count_t  LinkRTI::PeerCount()       // Return number of peers
{
  return 1; // Point to point links have exactly 1 peer
}

IPAddr_t LinkRTI::PeerIP(Count_t npeer)  // Get peer IP Address (if known)
{
  if (npeer > 0)
    return IPADDR_NONE;      // Oops, value out of range
  return peerIP;             // Use known value
}

IPAddr_t LinkRTI::NodePeerIP(Node*)    // Get peer IP Address (if known)
{ // Find the peer's ip address for peer n on this link
  // For LinkRTI, we don't know peer nodes, so just return known IP
  return peerIP;             // Return one and only peer's ip address  
}

bool     LinkRTI::NodeIsPeer(Node* n)
{ // Return true if specified node is a peer
  if (!pPeer) return false;
  Node* peerNode = pPeer->GetNode();
  return (peerNode == n);
}

Count_t  LinkRTI::NodePeerIndex(Node* n)
{
  return 0; // RTI Links always have only one peer
}

IPAddr_t LinkRTI::DefaultPeer(Interface*)
{ // Get IP Addr of default peer
  return nil;       // Unknown for RTI Links
}

// debug
bool LinkRTI::IsRTI()
{ // true if rtilink for distributed simulations
  return true;
}

// Private Methods
void LinkRTI::ConstructorHelper(Interface* interface)
{
#ifdef HAVE_RTI
  if (!Simulator::instance) return; // just for debugging, remove later
  
  NodeId_t real = (interface->GetNode())->Id();
  
  // get the broadcast strings for RTI class registration
  // and save them for later on usage
  char bcastString[20];
  sprintf(bcastString, "%ld:%ld", real, ghost);
  
  char SbcastString[20];
  sprintf(SbcastString, "%ld:%ld", ghost, real);
  
  DistributedSimulator::rti_BCast.push_back(InitGroup(bcastString, this));
  DistributedSimulator::rti_MGroup.push_back(MGroup(SbcastString, iFace));

  // Compute the txtime + delay for smallest packet (used for lookahead)
  Time_t delay = Delay();
  Rate_t bw = Bandwidth();
  Time_t totTime = delay + 40 * 8.0 / bw; // Assumes minimum 40 byte pkt
  if (totTime)
    {
      if (totTime < minTxTime || minTxTime == 0) minTxTime = totTime;
    }
#endif
}
  




