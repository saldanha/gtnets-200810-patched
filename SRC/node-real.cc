// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: node-real.cc 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - Node class
// George F. Riley.  Georgia Tech, Spring 2002

#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif

#ifdef HAVE_QT
#include <qcanvas.h>
#include <qpoint.h>
#include "qtwindow.h"
#endif

//#define DEBUG_MASK 0x02
#include "debug.h"
#include "protograph.h"
#include "portdemux.h"
#include "ipaddr.h"
#include "duplexlink.h"
#include "application.h"
#include "mobility.h"
#include "link-rti.h"
#include "mask.h"
#include "interface-real.h"
#include "interface-wireless.h"
#include "node-real.h"
#include "image.h"
#include "wlan.h"

#define NOT_FOUND -1

using namespace std;

Count_t NodeReal::count = 0;
Count_t NodeReal::count1 = 0;
Count_t NodeReal::count2 = 0;
Count_t NodeReal::count3 = 0;
Count_t NodeReal::count4 = 0;

NodeReal::NodeReal(Node* n)
    : NodeImpl(n),
      pRoutingAODV(nil),
      x(0.0), y(0.0), hasLocation(false),
#ifndef COMPACT
      graph(0),
#endif
      traceStatus(Trace::DEFAULT), mobility(nil),userInfo(nil),
      demux(0), radioRange(0), battery(INFINITE_VALUE), computePower(0),
      radioMarginTime(0),
#ifdef HAVE_QT
      animInfo(nil), wirelessColor(Qt::black), 
#endif
        icmpEnabled(true),
      timeout(DEFAULT_SW_TIMEOUT), isswitch(false),
      wormcontainment(nil)
{
  pRouting = Routing::Default()->Clone(); // Copy of default routing object
  DEBUG0((cout << "Node " << this << " prouting " << pRouting << endl));
  pRouting->node = n;    // yj
  rtable = new RouteTable();
  count++;
}

NodeReal::~NodeReal()
{
  if (mobility) delete mobility;
  if (pRouting) delete pRouting;
#ifndef COMPACT
  if (graph) delete graph;
#endif
  if (demux) delete demux;
  if(wormcontainment) delete wormcontainment;
  count--;
}

// Make a copy
NodeImpl* NodeReal::Copy() const
{
  return new NodeReal(*this);
}

// Real/Ghost test
bool NodeReal::IsReal()
{ // Always true for Real nodes
  return true;
}

// Broadcast a packet
void NodeReal::Broadcast(Packet* p, Proto_t proto)
{ // Broadcast a packet on all interfaces
  const IFVec_t ifVec = Interfaces();  // Vector of all interface this node
  MACAddr macBC;
  macBC.SetBroadcast(); // Set destination to broadcast
  for (IFVec_t::size_type i = 0; i < ifVec.size(); ++i)
    {
      Interface* iface = ifVec[i];
      if (iface->GetLink())
        { // This interface has associated link (not rx end of simplex)
          iface->Send(p->Copy(), (Word_t)proto);
#ifdef OLD_SUPERCEEDED
          L2Info802_3 l2Info(iface->GetMACAddr(),   // Src MAC Addr
                             macBC,                 // Broadcast
                             proto);                // Protocol
          L2Proto* l2proto = iface->GetL2Proto();   // Get l2 protocol obj
          l2proto->DataRequest(iface, p->Copy(), &l2Info);  // Pass down to l2
#endif
        }
    }
  delete p; // Since we replicated the packet on each iface, done with this one
}

// Queue management

Queue* NodeReal::GetQueue()
{ // Return queue if only one i/f
  // This is simply a syntactic shortcut to GetQueue(Node*) below
  Count_t ifCount = InterfaceCount();
  if (!ifCount) return nil;         // No interfaces
  if (ifCount > 1) return nil;      // Multiple interfaces
  return interfaces[0]->GetQueue(); // Return the one and only queue
}

Queue* NodeReal::GetQueue(Node* n)
{ // Get the queue to specified node
  Interface* i = GetIfByNode(n);
  if (!i) return nil;   // Not found
  return i->GetQueue(); // Return the associated queue
}

// Applications
Application* NodeReal::AddApplication(const Application& a)
{
  Application* pa = a.Copy(); // Make a copy of this application
  pa->AttachNode(pNode);      // Notify the application of attached node
  return pa;
}

// Routing Interface
void NodeReal::DefaultRoute(RoutingEntry r)
{ // Specify default route
  // First insure the interface specified is resident on this node
  for (IFVec_t::iterator i = interfaces.begin(); i != interfaces.end(); ++i)
    {
#ifndef WIN32
      if ((*i) == r.interface)
#else
	  if ((*i) == r.interface_)
#endif
        { // Found it
          pRouting->Default(r);
          return;
        }
    }
#ifndef WIN32
  DEBUG0(cout << "Ignoring Default Route for Node " << pNode->Id()
         << " interface " << r.interface 
         << " not present" << endl);
#else
  DEBUG0(cout << "Ignoring Default Route for Node " << pNode->Id()
         << " interface " << r.interface_
         << " not present" << endl);

#endif
  pRouting->Default(r);
}

void NodeReal::DefaultRoute(Node* n)
{ // Specify default route
  RoutingEntry r;
#ifndef WIN32
  r.interface = GetIfByNode(n);
  DEBUG0((cout << "NR::DefRoute, if " << r.interface << endl));
  if (!r.interface) return; // Nope, not a routing neighbor
#else
  r.interface_ = GetIfByNode(n);
  DEBUG0((cout << "NR::DefRoute, if " << r.interface_ << endl));
  if (!r.interface_) return; // Nope, not a routing neighbor
#endif
  Interface* peerIFace = n->GetIfByNode(pNode);
  if (peerIFace) r.nexthop = peerIFace->GetIPAddr();
  DEBUG0((cout << "NR::DefRoute, nh " << (string)IPAddr(r.nexthop) << endl));
  pRouting->Default(r);
}

void NodeReal::AddRoute(IPAddr_t ip, Count_t mask, Interface* i, IPAddr_t ip1)
{ // Routing entry
  pRouting->Add(ip, mask, i, ip1);
}

RoutingEntry NodeReal::LookupRoute(IPAddr_t ip)
{ // Find a routing entry
  // Still not found, see if on directly connected interface
  RoutingEntry r;
  // First see if locally accessible subnet
#ifndef WIN32
  r.interface = LocalRoute(ip);
  if (r.interface)
#else
  r.interface_ = LocalRoute(ip);
  if (r.interface_)

#endif
    { // Found it
      r.nexthop =  ip;
      return r;
    }
  // Not local subnet, find a route
  // First see if we only have one interface and one neighbor
  if (InterfaceCount() == 1)
    { // See if only one peer on this interface
      Interface* i = interfaces[0];
      if (i->NeighborCount(pNode) <= 1)
        { // Ok, just use the single peer
          return RoutingEntry(i, i->DefaultPeer());
        }
    }
  // Multiple choices, lookup in the FIB
  r = pRouting->Lookup(pNode, ip);
  return r;
}

RoutingEntry NodeReal::LookupRouteNix(Count_t nix)
{ // Lookup route by Neighbor Index
#ifdef OLD_WAY
  // First see if cached
  RENixMap_t::iterator k = nixCache.find(nix);
  if (k != nixCache.end())
    { // Found it,
      return k->second;
    }
  // Need to calculate (less efficient)
  Count_t cnc = 0;      // Cumulative neighbor count
  // Find which interface has the specified nix
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      Count_t nc = interfaces[i]->NeighborCount();
      Count_t priorCnc = cnc;
      cnc += nc; // Advance cumulative neighbor count
      if (nix < cnc)
        { // Yes, it's on this interface
          RoutingEntry re;
          re.interface = interfaces[i];
          re.nexthop   = interfaces[i]->PeerIP(nix-priorCnc);
          // insert in the cache for later reuse
          nixCache[nix] = re;
          return re;
        }
    }
  return RoutingEntry(); // HuH? Not found
#endif
  // See if the lookup vector exists, create if not
  if (nixLookup.empty())
    { // build it
      for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
        {
          Count_t nc = interfaces[i]->NeighborCount(pNode);
          Interface* iface = interfaces[i];
          for (Count_t n = 0; n < nc; ++n)
            {
              nixLookup.push_back(RoutingEntry(iface, iface->PeerIP(n)));
            }
        }
    }
  if (nix >= nixLookup.size()) return RoutingEntry(); // HuH?
  return nixLookup[nix];
}

//AODV Routing
//Specially for AODV routing, it is not compatiable with
//other route entry.

AODVRoutingEntry* NodeReal::LookupRouteAODV(IPAddr_t ip)
{
  if (pRoutingAODV)
    return pRoutingAODV->Lookup(pNode, ip);
  return nil;
}

void NodeReal::SetRoutingAODV(void* aodv)
{
  pRoutingAODV = (RoutingAODV*)aodv;
}

RoutingAODV* NodeReal::GetRoutingAODV()
{
  return pRoutingAODV;
}

Routing::RType_t NodeReal::RoutingType()
{
  return pRouting->Type();
}

void NodeReal::InitializeRoutes()
{ // Routing initialization (if any)
  DEBUG0((cout << "Node " << pNode->Id() << " initializing routes, routing "
          << " prouting " << pRouting << " rtype "
          << pRouting->Type() << endl));
  pRouting->InitializeRoutes(pNode);
}

void NodeReal::ReInitializeRoutes(bool u)
{ // Routing re-initialization
  DEBUG0((cout << "Node " << pNode->Id() << " reinitializing routes, rtype "
          << pRouting->Type() << endl));
  pRouting->ReInitializeRoutes(pNode, u);
}

Count_t NodeReal::RoutingFibSize() const
{ // Get the size of the routing FIB for statistics measurement
  return pRouting->Size();
}

RoutingNixVector* NodeReal::GetNixRouting()
{ // Returns a pointer to the routing object if it's NixVector routing,
  // otherwise return nil
  if (RoutingType() == Routing::NIXVECTOR)
    {
      return (RoutingNixVector*)pRouting;
    }
  return nil; // Not NixVector
}

// Protocol Graph Interface
#ifdef COMPACT
// No private protocol graphs in compact mode
Protocol* NodeReal::LookupProto(Layer_t l, Proto_t p)// Lookup protocol by layer
{
  return ProtocolGraph::common->Lookup(l,p);
}

void NodeReal::InsertProto(Layer_t, Proto_t, Protocol*) // Insert a protocol
{
  cout << "Private protocol graph not supported in compact mode" << endl;
  return;
}
#else
Protocol* NodeReal::LookupProto(Layer_t l, Proto_t proto)
{ // Lookup protocol by layer
  Protocol* p = NULL;
  if (graph)
    { // private map exists
      p = graph->Lookup(l,proto);
    }
  if (!p)
    { // No private map
      return ProtocolGraph::common->Lookup(l,proto);
    }
  return p; // Return value from private map
}

void NodeReal::InsertProto(Layer_t l, Proto_t proto, Protocol* p)
{ // Insert proto
  if (!graph) graph = new ProtocolGraph;
  graph->Insert(l, proto, p);
}
#endif

// Port Demux Interface
bool NodeReal::Bind(Proto_t proto, PortId_t port , Protocol* p)
{
  if (!demux) demux = new PortDemux();
  return demux->Bind(proto, port, p);
}

bool NodeReal::Bind(Proto_t proto,
                    PortId_t lport, IPAddr_t laddr,
                    PortId_t rport, IPAddr_t raddr,
                    Protocol* p)
{ // Register port usage
  if (!demux) demux = new PortDemux();
  DEBUG0((cout << "Node " << pNode->Id() << " binding proto " << p
          << " to lport " << lport << " laddr " << (string)::IPAddr(laddr)
          << " to rport " << rport << " raddr " << (string)::IPAddr(raddr)
          << endl));
  return demux->Bind(proto, lport, laddr, rport, raddr, p);
}

PortId_t NodeReal::Bind(Proto_t proto, Protocol* p)
{ // Choose available port and register
  if (!demux) demux = new PortDemux();
  return demux->Bind(proto, p);
}

bool NodeReal::Unbind(Proto_t proto, PortId_t port, Protocol* p)
{ // Remove port binding
  if (!demux) demux = new PortDemux();
  return demux->Unbind(proto, port, p);
}

bool NodeReal::Unbind(Proto_t proto,
                      PortId_t lport, IPAddr_t laddr,
                      PortId_t rport, IPAddr_t raddr,
                      Protocol* p)
{ // Remove port binding
  if (!demux) demux = new PortDemux();
  return demux->Unbind(proto, lport, laddr, rport, raddr, p);
}

Protocol* NodeReal::LookupByPort(Proto_t proto, PortId_t port)
{
  if (!demux) demux = new PortDemux();
  return demux->Lookup(proto, port);
}

Protocol* NodeReal::LookupByPort(Proto_t proto,
                                 PortId_t sport, IPAddr_t sip,
                                 PortId_t dport, IPAddr_t dip)
{
  if (!demux) demux = new PortDemux();
  return demux->Lookup(proto, sport, sip, dport, dip);
}

// Packet tracing interface
// Trace header if enabled
bool NodeReal::TracePDU(Protocol* proto, PDU* h, Packet* p, char* s)
{
  if (!CheckTrace(proto,h)) return false;
  // Need to trace this object
  //Trace::Instance()->SetNode(pNode);               // Inform which node
  Trace::Instance()->NewNode(pNode);               // Inform which node
  Bitmap_t bm;
  if (proto) bm = proto->Details();
  else   bm = ALLBITS;                             // No proto, trace all
  DEBUG(0,(cout << "NodeReal::TracePDU bm " << bm << endl));
  h->Trace(Trace::Instance()->GetTS(), bm, p, s);  // Trace the pdu
  return true;
}

// Set trace level this node
void NodeReal::SetTrace(Trace::TraceStatus t)
{
  traceStatus = t;
}

// Node Location Interface
void NodeReal::SetLocation(Meters_t xl, Meters_t yl, Meters_t zl)
{ // Set x/y/z location
  x = xl;
  y = yl;
  z = zl;
  if (Simulator::instance)
    { // Simulator object tracks min/max for displaying
      Simulator::instance->NewLocation(x,y);
    }
  hasLocation = true;
#ifdef HAVE_QT  
  if (animInfo) animInfo->show = true;
#endif
}

void NodeReal::SetLocation(const Location& l)
{
  SetLocation(l.X(), l.Y(), l.Z());
}

void NodeReal::SetLocationLongLat(const Location& l)
{ // Adjust the x (longitude) coordinate by cos(lat), using
  // a centerline projection.
  RectRegion& bb = Simulator::boundingBox;
  Meters_t centerLine =
      bb.LowerLeft().X() +
      (bb.UpperRight().X() - bb.LowerLeft().X()) / 2;

  Meters_t x = l.X();
  Meters_t y = l.Y();
  Meters_t delta = x - centerLine;
  delta *= cos(y * DegreesToRadians);
  cout << "Setting adjusted location " << centerLine + delta
       << " " << y << endl;
  SetLocation(centerLine + delta, y, l.Z());
}

bool NodeReal::HasLocation()
{
  return hasLocation;
}

Meters_t NodeReal::LocationX()
{
  return x;
}

Meters_t NodeReal::LocationY()
{
  return y;
}

Meters_t NodeReal::LocationZ()
{
  return z;
}

Location NodeReal::GetLocation()
{
  return Location(x, y, z);
}

Location NodeReal::UpdateLocation()
{
  if (mobility) mobility->UpdateLocation(pNode);
  return GetLocation();
}

Mobility* NodeReal::AddMobility(const Mobility& m)
{ // Add a new mobility model for this node
  if (mobility) delete mobility;
  mobility = m.Copy();
  mobility->Initialize();
  Simulator::instance->HasMobility(true); // Note we have mobility
  return mobility;
}

Mobility* NodeReal::GetMobility() const
{
  return mobility;
}

bool NodeReal::IsMoving()
{
  if (!mobility) return false;
  return mobility->IsMoving();
}

bool NodeReal::IsMobile()
{
  if (!mobility) return false;
  return true;
}

#ifdef HAVE_QT
QCanvasItem* NodeReal::Display(QTWindow* qtw)
{
  if (!qtw) return nil;
  QPoint p = qtw->LocationToPixels(GetLocation());
  return Display(p, qtw);

}

QCanvasItem* NodeReal::Display(const QPoint& p, QTWindow* qtw)
{
  if (!qtw) return nil; // Not animating
  QCanvas* canvas = qtw->Canvas();
  CustomShape_t cs;
  if (!animInfo)
    {
      animInfo = new NodeAnimation(); // Allocate the animation info
      animInfo->show = hasLocation;
    }
  if (Shape() != Node::IMAGE && !animInfo->nodePolygon)
    { // No existing polygon, make one
      switch(Shape()) {
        case Node::NONE:
          return nil;
        case Node::CIRCLE:
          animInfo->nodePolygon =
              new QCanvasEllipse(animInfo->pixelSizeX, animInfo->pixelSizeY,
                                 canvas);
          animInfo->nodePolygon->move(p.x(), p.y());
          break;
        case Node::SQUARE:
          animInfo->nodePolygon = new QCanvasRectangle(
              p.x() - animInfo->pixelSizeX / 2,
              p.y() - animInfo->pixelSizeY / 2,
              animInfo->pixelSizeX,
              animInfo->pixelSizeY,
              canvas);
           break;
        case Node::HEXAGON:
          break;
        case Node::OCTAGON:
          break;
        case Node::CUSTOM:
          cs = CustomShape();
          if (cs)
            {
              animInfo->nodePolygon = cs(canvas, pNode, p, animInfo->nodePolygon);
            }
          break;
        default:
          break;
      }
      if (!animInfo->nodePolygon) return nil; // Oops!  No known shape
      animInfo->nodePolygon->show();
    }
  if (IsMobile() || animInfo->placeImage)
    { // Need to (possibly) move to new location
      if (Shape() == Node::CUSTOM)
        { // Custom items may redraw depending on motion, we need to call
          cs = CustomShape();
          if (cs)
            {
              animInfo->nodePolygon = cs(canvas,pNode,p, animInfo->nodePolygon);
            }
        }
      else
        {
          QCanvasItem* pi;
          if (Shape() == Node::IMAGE)
            pi = animInfo->nodeSprite;
          else
            pi = animInfo->nodePolygon;
          pi->move( p.x() - animInfo->pixelSizeX / 2,
                    p.y() - animInfo->pixelSizeY / 2);
          animInfo->placeImage = false;
          if (Shape() == Node::IMAGE)
            { // We want images behind links and packets
              pi->setZ(-1);
            }
          else
            {
              pi->setZ(1.0); // We want nodes "in front of" links
            }
        }
    }
  // Choose color of node object based on wireless conditions
  // or user specified color.
  if (Shape() != Node::CUSTOM && Shape() != Node::IMAGE)
    { // Custom shapes and images set their own color
      QCanvasPolygonalItem* pi = (QCanvasPolygonalItem*)animInfo->nodePolygon;
      if (IsDown())
        {
          pi->setBrush(Qt::red);
        }
      else if (HasColor())
        { // Use user specified color if one is specified
          pi->setBrush(Color());
        }
      else if (WirelessCx())
        {
          pi->setBrush(Qt::red);
        }
      else if (WirelessTx())
        {
          pi->setBrush(Qt::cyan);
        }
      else if (WirelessRx())
        {
          pi->setBrush(Qt::green);
        }
      else if (WirelessRxMe())
        {
          pi->setBrush(Qt::blue);
        }
      else if (WirelessRxZz())
        {
          pi->setBrush(Qt::yellow);
        }
#ifdef DONT_DO_THIS
      else if (mobility && mobility->IsMoving())
        {
          pi->setBrush(Qt::magenta);
        }
#endif
      else
        {
          pi->setBrush(Qt::black);
        }
    }
  return animInfo->nodePolygon;

}

void NodeReal::WirelessTxColor(const QColor& c)
{
  wirelessColor = c;
}

const QColor& NodeReal::WirelessTxColor()
{
  return wirelessColor;
}

bool NodeReal::PushWirelessTx(QCanvasItem* c)
{
  canvasItems.push_back(c);
  return true;
}

QCanvasItem* NodeReal::PopWirelessTx()
{
  if (canvasItems.empty()) return nil;
  QCanvasItem* c = canvasItems.front();
  canvasItems.pop_front();
  return c;
}

void NodeReal::PixelSize(Count_t nps)  // Set node size
{
  if (!animInfo)
    {
      animInfo = new NodeAnimation(); // Allocate the animation info
      animInfo->show = hasLocation;
    }
  animInfo->pixelSizeX = nps;
  animInfo->pixelSizeY = nps;
}

Count_t NodeReal::PixelSizeX()
{
  if (!animInfo)
    {
      animInfo = new NodeAnimation(); // Allocate the animation info
      animInfo->show = hasLocation;
    }
  return animInfo->pixelSizeX;
}

Count_t NodeReal::PixelSizeY()
{
  if (!animInfo)
    {
      animInfo = new NodeAnimation(); // Allocate the animation info
      animInfo->show = hasLocation;
    }
  return animInfo->pixelSizeY;
}

void NodeReal::Shape(Node::Shape_t ns) // Set node shape
{
  if (!animInfo)
    {
      animInfo = new NodeAnimation(); // Allocate the animation info
      animInfo->show = hasLocation;
    }
  animInfo->shape = ns;
}

Node::Shape_t NodeReal::Shape()
{
  if (!animInfo)
    {
      animInfo = new NodeAnimation(); // Allocate the animation info
      animInfo->show = hasLocation;
    }
  return animInfo->shape;
}

CustomShape_t NodeReal::CustomShape()
{
  if (!animInfo) return nil;
  return animInfo->csCallback;
}

void NodeReal::CustomShape(CustomShape_t cs)
{
  if (!animInfo)
    {
      animInfo = new NodeAnimation(); // Allocate the animation info
      animInfo->show = hasLocation;
    }
  animInfo->csCallback = cs;
}

bool NodeReal::CustomShapeFile(const char*  fn)
{
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  if (!qtw) return false; // Simulator::StartAnimation must be already called
  ifstream t(fn);
  if (!t)
    {
      cout << "Can't open custom shape file " << fn << endl;
      return false;
    }
  t.close();
  QPixmap qpm;
  if (!qpm.load(fn))
    {
      cout << "Can't load pixmap " << fn << endl;
      return false;
    }
  return CustomShapeHelper(qpm);
}

bool NodeReal::CustomShapeImage(const Image& im)
{ // Use one of the built-in images
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  if (!qtw) return false; // Simulator::StartAnimation must be already called
  QPixmap qpm;
  qpm.loadFromData((const uchar*)im.Data(), im.Size());
  if (!qpm.width() || !qpm.height()) return false; // Failed
  return CustomShapeHelper(qpm);
}

void NodeReal::Color(const QColor& c)
{
  if (!animInfo)
    {
      animInfo = new NodeAnimation();
      animInfo->show = hasLocation;
    }
  animInfo->color = c;
  animInfo->hasColor = true;
  if (animInfo->nodePolygon)
    { // Already displayed, update the canvas item
      animInfo->nodePolygon->setBrush(c);
    }
}

bool NodeReal::HasColor()
{
  if (!animInfo) return false;
  return animInfo->hasColor;
}

QColor& NodeReal::Color()
{
  if (!animInfo)
    {
      animInfo = new NodeAnimation();
      animInfo->show = hasLocation;
    }
  return animInfo->color;
}

NodeAnimation* NodeReal::GetNodeAnimation() const
{
  return animInfo;
}

#endif

// ICMP
bool NodeReal::ICMPEnabled() const
{
  return icmpEnabled;
}

void NodeReal::DisableICMP()
{
  icmpEnabled = false;
}

#ifdef MOVED_TO_IMPL
// Failure management
void NodeReal::Down()
{ // specify each interface is down
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      interfaces[i]->Down();
    }
  down = true;
  Simulator::instance->TopologyChanged();
#ifdef HAVE_QT  
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  Display(qtw);
#endif
}

void NodeReal::Up()
{ // Node has recovered from failure
  // specify each interface is up
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      interfaces[i]->Up();
    }
  down = false;
  Simulator::instance->TopologyChanged();
#ifdef HAVE_QT
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  Display(qtw);
#endif
}

bool NodeReal::IsDown()
{
  return down;
}
#endif

Meters_t NodeReal::Distance(Node* n)
{
  Meters_t xdiff = LocationX() - n->LocationX();
  Meters_t ydiff = LocationY() - n->LocationY();
  Meters_t zdiff = LocationZ() - n->LocationZ();
  return sqrt(xdiff * xdiff + ydiff * ydiff + zdiff * zdiff);
}

//#define RADIO_OLD_WAY
//#define RADIO_ADAPT_WAY

void NodeReal::BuildRadioInterfaceList(WirelessLink* wLink)
{
#ifdef DONT_DUPLICATE_CODE
  // If no mobility assumed, need to build the list only once.
  if (!Simulator::instance->HasMobility()) {
    if (!radioInterfaces.empty()) return;
    if (IsMobile()) UpdateLocation();
    const NodeVec_t& nodes = Node::GetNodes();
    for (NodeVec_t::const_iterator i = nodes.begin(); i != nodes.end(); ++i)
      {
        Node* n = *i;
        if (Id() == n->Id()) continue;    // skip self node
        if (n->IsMobile()) n->UpdateLocation();
        Meters_t d = Distance(n);
        if (d <= GetRadioRange()) {
          Interface* iface = n->GetIfByNode(pNode);
          if (iface->IsWireless())
            {
              InterfaceWireless* ifW = (InterfaceWireless*)iface;
              radioInterfaces.push_back(RadioItem(ifW,d));
            }
        }
      }
    return;
  }
#endif
  if (!Simulator::instance->HasMobility())
    {  // Nothing to  do if no mobility and already built
      if (!radioInterfaces.empty()) return;
      radioMarginTime = 0.0; // list is empty, force one-time rebuile
    }
  
      
#ifdef RADIO_OLD_WAY
  radioInterfaces.clear();
  if (IsMobile()) UpdateLocation();
  for (NodeVec_t::iterator i = nodes.begin(); i != nodes.end(); ++i)
    {
      Node* n = *i;
      if (id == n->id) continue;    // skip self node
      if (n->IsMobile()) n->UpdateLocation();
      Meters_t d = Distance(n);
      if (d <= GetRadioRange()) {
        Interface* iface = n->GetIfByNode(this);
        radioInterfaces.push_back(RadioItem(iface,d));
      }
    }
#elif defined RADIO_ADAPT_WAY
  Time_t now = Simulator::Now();
  double radio_range = GetRadioRange();
  if (IsMobile()) UpdateLocation();
  if (now >= radioMarginTime) {
    // need to re-build radio interface list
    radioInterfaces.clear();
    radioInf2.clear();
    double max_dist = 0;
#ifdef OLD_INEFFICIENT
    for (NodeVec_t::iterator i = nodes.begin(); i != nodes.end(); ++i)
      {
        Node* n = *i;
        if (id == n->id) continue;    // skip self node
        if (n->IsMobile()) n->UpdateLocation();
        Meters_t d = Distance(n);
        Interface* iface = n->GetIfByNode(this);
        if (!iface) cout << "HuH? nil iface???\n";
        if (d <= radio_range) {
          radioInterfaces.push_back(RadioItem(iface,d));
          if (d > max_dist) max_dist = d;
        }
        else
          radioInf2.push_back(RadioItem(iface,d));
      }
#else // use wlan object instead
    for (NodeId_t nid = wLink->first; nid < wLink->last; ++nid)
      { // Check each node in the wlan object
        if (nid == Id()) continue; // Skip self
        Node* n = Node::GetNode(nid);
        if (n->IsMobile()) n->UpdateLocation();
        Meters_t d = Distance(n); // Distance to this node
        const IFVec_t ifaces = n->Interfaces();
        for (IFVec_t::size_type i = 0; i < ifaces.size(); ++i)
          {
            Interface* iface = ifaces[i];
            if (iface->IsWireless())
              { // Found a wireless
                if (d <= radio_range)
                  {
                    radioInterfaces.push_back(RadioItem(iface,d));
                    if (d > max_dist) max_dist = d;
                  }
                else
                  {
                    radioInf2.push_back(RadioItem(iface,d));
                  }
              }
          }
      }
#endif

    double mar_dist = radio_range - max_dist;
    Time_t mar_time = mar_dist / (2*defaultMaxSpeed);
    double out_bound = radio_range + mar_dist;
    for (RadioVec_t::iterator i = radioInf2.begin(); i != radioInf2.end(); )
      {
        if (i->dist > out_bound)
          radioInf2.erase(i++);
        else
          ++i;
      }
    radioMarginTime = now + mar_time;
  }
  else {
    // use only radioInterfaces and radioInf2
    // need only to update distance
    RadioVec_t::iterator i;
    for (i = radioInterfaces.begin(); i != radioInterfaces.end(); )
      {
        Node* n = i->inf->GetNode();
        if (n->IsMobile()) n->UpdateLocation();
        i->dist = Distance(n);
        if (i->dist > radio_range) {
          radioInf2.push_back(*i);
          radioInterfaces.erase(i++);
        }
        else
          ++i;
      }
    for (i = radioInf2.begin(); i != radioInf2.end(); )
      {
        Node* n = i->inf->GetNode();
        if (n->IsMobile()) n->UpdateLocation();
        i->dist = Distance(n);
        if (i->dist <= radio_range) {
          radioInterfaces.push_back(*i);
          radioInf2.erase(i++);
        }
        else
          ++i;
      }
  }
#else
  Time_t now = Simulator::Now();
  double radio_range = GetRadioRange();
  if (IsMobile()) UpdateLocation();
  if (now > radioMarginTime)
    {
      // need to re-build radio interface list
      radioInterfaces.clear();
      radioInf2.clear();
      Time_t mar_time = 2.0;
      double out_bound = radio_range + 2*Node::defaultMaxSpeed*mar_time;
#ifdef OLD_INEFFICIENT
      const NodeVec_t& nodes = Node::GetNodes();
      for (NodeVec_t::const_iterator i = nodes.begin(); i != nodes.end(); ++i)
        {
          Node* n = *i;
          if (Id() == n->Id()) continue;    // skip self node
          if (n->IsMobile()) n->UpdateLocation();
          Meters_t d = Distance(n);
          Interface* iface = n->GetIfByNode(pNode);
          if (!iface) cout << "HuH? nil iface???\n";
          if (iface->IsWireless())
            {
              InterfaceWireless* ifW = (InterfaceWireless*)iface;
              if (d <= radio_range)
                radioInterfaces.push_back(RadioItem(ifW,d));
              else if (d <= out_bound)
                radioInf2.push_back(RadioItem(ifW,d));
            }

        }
#else // Use wlan object
      IFVec_t& ifaces = wLink->ifaceVec;
      for (IFVec_t::size_type i = 0; i < ifaces.size(); ++i)
        { // Check each interface in the wlan object
          Node* n = ifaces[i]->GetNode();
          if (n->Id() == Id()) continue; // Skip self
          if (n->IsMobile()) n->UpdateLocation();
          Meters_t d = Distance(n); // Distance to this node
          InterfaceWireless* ifW = (InterfaceWireless*)ifaces[i];
          if (d <= radio_range)
            radioInterfaces.push_back(RadioItem(ifW,d));
          else if (d <= out_bound)
            radioInf2.push_back(RadioItem(ifW,d));
        }
#endif
      radioMarginTime = now + mar_time;
    }
  else
    {
      // use only radioInterfaces and radioInf2
      // need only to update distance
      RadioVec_t::iterator i;
      for (i = radioInterfaces.begin(); i != radioInterfaces.end(); )
        {
          Node* n = i->inf->GetNode();
          if (n->IsMobile()) n->UpdateLocation();
          i->dist = Distance(n);
          if (i->dist > radio_range) {
            radioInf2.push_back(*i);
            radioInterfaces.erase(i++);
          }
          else
            ++i;
        }
      for (i = radioInf2.begin(); i != radioInf2.end(); )
        {
          Node* n = i->inf->GetNode();
          if (n->IsMobile()) n->UpdateLocation();
          i->dist = Distance(n);
          if (i->dist <= radio_range)
            {
              radioInterfaces.push_back(*i);
              radioInf2.erase(i++);
            }
          else
            ++i;
        }
    }
#endif
}

const RadioVec_t& NodeReal::GetRadioInterfaceList()
{
  return radioInterfaces;
}

// Callback management

void      NodeReal::AddCallback(Layer_t l, Proto_t p,
                                PacketCallbacks::Type_t t,
                                Interface* i,
                                PacketCallbacks::Function_t f)
{
  callbacks.Add(l, p, t, i, f);
}

void      NodeReal::AddCallbackHead(Layer_t l, Proto_t p,
                                    PacketCallbacks::Type_t t,
                                    Interface* i,
                                    PacketCallbacks::Function_t f)
{
  callbacks.AddHead(l, p, t, i, f);
}

void      NodeReal::DeleteCallback(Layer_t l, Proto_t p, 
                                   PacketCallbacks::Type_t t,
                                   Interface* i)
{
  callbacks.Delete(l, p, t, i);
}

bool      NodeReal::CallCallbacks(Layer_t l, Proto_t p,
                                  PacketCallbacks::Type_t t,
                                  Packet* pkt, Interface* i)
{
  return callbacks.Call(l, p, t, pkt, pNode, i);
}


#ifdef HAVE_QT

bool NodeReal::WirelessTx()
{
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      if (interfaces[i]->IsWireless())
        {
          InterfaceWireless* iw = (InterfaceWireless*)interfaces[i];
          if (iw->GetLinkState() == InterfaceWireless::TX)
            {
              return true;
            }
        }
    }
  return false;
}

bool NodeReal::WirelessRx()
{
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      if (interfaces[i]->IsWireless())
        {
          InterfaceWireless* iw = (InterfaceWireless*)interfaces[i];
          if (iw->GetLinkState() == InterfaceWireless::RX)
            {
              return true;
            }
        }
    }
  return false;
}

bool NodeReal::WirelessCx()
{
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      if (interfaces[i]->IsWireless())
        {
          InterfaceWireless* iw = (InterfaceWireless*)interfaces[i];
          if (iw->GetLinkState() == InterfaceWireless::CX)
            {
              return true;
            }
        }
    }
  return false;
}

bool NodeReal::WirelessRxMe()
{
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      if (interfaces[i]->IsWireless())
        {
          InterfaceWireless* iw = (InterfaceWireless*)interfaces[i];
          if (iw->GetLinkState() == InterfaceWireless::RX_ME)
            {
              return true;
            }
        }
    }
  return false;
}

bool NodeReal::WirelessRxZz()
{
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      if (interfaces[i]->IsWireless())
        {
          InterfaceWireless* iw = (InterfaceWireless*)interfaces[i];
          if (iw->GetLinkState() == InterfaceWireless::RX_ZZ)
            {
              return true;
            }
        }
    }

  return false;
}

void NodeReal::Show(bool s)
{ // Set the animation display status of this node
  if (!animInfo) animInfo = new NodeAnimation();
  animInfo->show = true;
}

bool NodeReal::Show()
{ // Return the animation display status of this node
  if (!animInfo)
    { // No animation info, just use "hasLocation"
      return hasLocation;
    }
  return animInfo->show;
}

void NodeReal::UserInformation(void* info)
{
  userInfo = info;
}

void* NodeReal::UserInformation()
{
  return userInfo;
}
#endif

// Switching functions

void NodeReal::SetTimeout(Time_t t)
{
  timeout = t;
}

int NodeReal::FindSRCPort(Interface* ifc)
{
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      if (interfaces[i] == ifc)
        { // Found it
          return i;
        }
    }
  return NOT_FOUND;
}

void NodeReal::PacketRX(Packet* p, Interface* iface)
{
  // Find which port is that interface connected to
  int port = FindSRCPort(iface);

  if(port == NOT_FOUND)
    {
      cout << "huh? interface not connected to a port! " << endl;
      return ;
    }
  L2802_3Header* l2pdu = (L2802_3Header*)p->PeekPDU();

  MACPort_t::iterator i = portmap.find(l2pdu->src); // do we have an entry for the source mac
  if(i != portmap.end())
    {
      // found then update its timeout
      (*i).second.time = Simulator::Now() + GetTimeout();
    }
  else
    {
      // not found then create a new one
      portmap.insert(
          MACPort_t::value_type(l2pdu->src, PortPair(port, Simulator::Now() + GetTimeout())));
    }


  // Do we have an entry for the destination mac
  i = portmap.find(l2pdu->dst);
  if (i != portmap.end() && (*i).second.time > Simulator::Now())
    {
      // found and not expired
      // forward packet to the corresponding port only
      if((*i).second.port != (unsigned int)port)
	{
	  DEBUG0((cout << "Forwarding packet to corresponding port " << (*i).second.port << endl ));
	  interfaces[(*i).second.port]->GetL2Proto()->DataRequest(p);
	}
      return ;
    }

  // No entry found for the destination mac or the entry is expired

  DEBUG0((cout << "----------------Forwarding packet to all  ports-------------- "  << endl));

  // forward packet to all ports except the src port
  for( Count_t j = 0; j < interfaces.size(); j++ )
    {
      if (j != (unsigned int)port)
	{
	  DEBUG0((cout << "Forwarding packet to corresponding port " << j << endl));
	  interfaces[j]->GetL2Proto()->DataRequest(p->Copy());
 	}
    }
  if (p) delete p;
  return;
}

void NodeReal::IsSwitchNode(bool b)
{
  isswitch = b;
}

void NodeReal::UseARP(bool b)
{
  for (IFVec_t::size_type j = 0; j < interfaces.size(); ++j)
    {
      Interface* i = interfaces[j];
      i->UseARP(b);
    }
}

void NodeReal::SetWormContainment(WormContainment* cont)
{
  wormcontainment = cont;
  usewormcontainment = true;
    
     
  // set the worm containment module on all connected interface
  for (IFVec_t::size_type j = 0; j < interfaces.size(); ++j)
    {
      Interface* i = interfaces[j];
      i->SetWormContainment(cont);
    }
}

// Private Methods

bool NodeReal::CheckTrace(Protocol* p, PDU* h) // Check if trace enabled
{
  // The trace enabling/disabling is a hierarchy.
  // If global tracing not enabled (ie. no trace file opened) return false;
  DEBUG(0,(cout << "Trace::Instance()->IsEnabled "
           << Trace::Instance()->IsEnabled()<<endl));
  if (!Trace::Instance()->IsEnabled()) return false;
  DEBUG(0,(cout << "traceStatus " << traceStatus << endl));
  // First make a decision, then call the callback (if defined)
  Trace::TraceStatus ts = Trace::DEFAULT;
  // Global trace is on, first check node
  if (ts == Trace::DEFAULT) ts = traceStatus;
  // Node trace is default, next check this protocol
  if (!p) ts = Trace::ENABLED;  // If called with no protocol, is a drop
  DEBUG(0,(cout << "GetTrace() " << p->GetTrace() << endl));
  if (ts == Trace::DEFAULT) ts = p->GetTrace();
  // Protocol trace is also default, check global settings
  if (ts == Trace::DEFAULT)
    if (Trace::Instance()->IsOn(p->Layer())) ts = Trace::ENABLED;
  bool tb = (ts == Trace::ENABLED);

  if (Trace::callback)
    { // Allow the callback to override the decision
      tb = Trace::callback(pNode, p, h, tb);
    }

  return tb;  // True or False to trace this pdu
}

#ifdef HAVE_QT
bool NodeReal::CustomShapeHelper(QPixmap& qpm)
{
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  // Must be non-nil at this point
  QValueList<QPixmap> list;
  list.push_back(qpm);
  QCanvasPixmapArray* qcpa = new QCanvasPixmapArray(list);
  if (!qcpa->isValid())
    { // Failed
      delete qcpa;
      return false;
    }
  if (!animInfo)
    {
      animInfo = new NodeAnimation(); // Allocate the animation info
      animInfo->show = hasLocation;
    }
  if (animInfo->nodePolygon) delete animInfo->nodePolygon;
  animInfo->nodePolygon = nil;
  animInfo->shape = Node::IMAGE;
  animInfo->nodeSprite = new QCanvasSprite(qcpa, qtw->Canvas());
  animInfo->nodeSprite->show();
  QCanvasPixmap* qcp = qcpa->image(0);
  animInfo->pixelSizeX = qcp->width();
  animInfo->pixelSizeY = qcp->height();
  DEBUG0((cout << "Image width " << qcp->width()
          << " height " << qcp->height() << endl));
  animInfo->placeImage = true;
  animInfo->nodeSprite->setZ(-1); // Place behind all other objects
  return true;
}
#endif
