// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: args.cc 460 2006-01-31 20:48:39Z riley $



// Helper classes for command line argument processing
// George F. Riley, Georgia Tech, Winter 2002

#include <iostream>
#include <cstdlib>

#include "args.h"

using namespace std;

// Arg static member variables
ArgVec_t    Arg::args;      // The expected arguments list
StringVec_t Arg::nonEquiv;  // The non-equivalenced argument list

Arg::Arg(const string& n, long& v, long d)
  : argName(n), argType(Int)
{
  v = d; // Set default value
  pArg = (void*)&v;
  args.push_back(*this);
}

Arg::Arg(const string& n, long& v)
  : argName(n), argType(Int)
{
  pArg = (void*)&v;
  args.push_back(*this);
}

Arg::Arg(const string& n, unsigned long& v, unsigned long d)
  : argName(n), argType(Long)
{
  v = d; // Set default value
  pArg = (void*)&v;
  args.push_back(*this);
}

Arg::Arg(const string& n, unsigned long& v)
  : argName(n), argType(Long)
{
  pArg = (void*)&v;
  args.push_back(*this);
}

Arg::Arg(const string& n, double& v, double d)
  : argName(n), argType(Double)
{
  v = d; // Set default value
  pArg = (void*)&v;
  args.push_back(*this);
}

Arg::Arg(const string& n, double& v)
  : argName(n), argType(Double)
{
  pArg = (void*)&v;
  args.push_back(*this);
}

Arg::Arg(const string& n, string& v, const string& d)
  : argName(n), argType(String)
{
  v = d; // Set default value
  pArg = (void*)&v;
  args.push_back(*this);
}

Arg::Arg(const string& n, string& v)
  : argName(n), argType(String)
{
  pArg = (void*)&v;
  args.push_back(*this);
}

Arg::Arg(const string& n, bool& v, bool d)
  : argName(n), argType(Bool)
{
  v = d; // Set default value
  pArg = (void*)&v;
  args.push_back(*this);
}

Arg::Arg(const string& n, bool& v)
  : argName(n), argType(Bool)
{
  pArg = (void*)&v;
  args.push_back(*this);
}

// Arg static methods
bool Arg::ProcessArgs(int argc, char** argv)
{
  for (int i = 1; i < argc; ++i)
    { // Process each argument
      string s(argv[i]);
      string::size_type e = s.find_first_of('=');
      if (e == string::npos)
        { // No equal sign, add to non-equiv list
          nonEquiv.push_back(s);
        }
      else
        {
          if ((e+1) == s.length())
            { // Nothing after the = sign
              cout << "Missing equivalence for parameter " << s << endl;
              exit(1);
            }
          string f = s.substr(0, e);
          string v = s.substr(e+1, string::npos);
          ArgVec_t::iterator j = args.begin();
          bool found = false;
          for(; j != args.end() && !found; ++j)
            {
              //cout << "checking " << j->argName << " vs " << f << endl;
              if (f == j->argName)
                { // Found it
                  switch (j->argType) {
                    case Int :
                      {
                        long* pI = (long*)j->pArg;
                        *pI = atol(v.c_str());
                        found = true;
                      }
                      break;
                    case Long :
                      {
                        unsigned long* pU = (unsigned long*)j->pArg;
                        *pU = atol(v.c_str());
                        found = true;
                      }
                      break;
                    case Double :
                      {
                        double* pD = (double*)j->pArg;
                        *pD = atof(v.c_str());
                        found = true;
                      }
                      break;
                    case String :
                      {
                        string* pS = (string*)j->pArg;
                        *pS = v;
                        found = true;
                      }
                      break;
                    case Bool :
                      {
                        bool* pB = (bool*)j->pArg;
                        if (v == "true") *pB = true;
                        else if (v == "false") *pB = false;
                        else cout << "Invalid true/false value for arg "
                                  << j->argName << endl;
                        found = true;
                      }
                      break;
                    }
                }
            }
          if (!found)
            {
              cout << "Unknown equivalence parameter " << s << endl;
              exit(1);
            }
        }
    }
  return true;
}

StringVec_t::size_type Arg::NumberNonEquiv()
{
  return nonEquiv.size();
}

string Arg::NonEquiv(StringVec_t::size_type i)
{
  if (NumberNonEquiv() < i)
    {
      cout << "No non-equivalent arg index " << i << endl;
      exit(1);
    }
  return nonEquiv[i];
}

bool Arg::Specified(const string& v)
{ // True if specified string v was a non-equivalenced argument
  for (StringVec_t::size_type i = 0; i < NumberNonEquiv(); ++i)
    {
      if (NonEquiv(i) == v) return true;
    }
  return false;
}

#ifdef MAIN
int main(int argc, char** argv)
{
  unsigned long q1Limit;
  signed   long testI;
  double        testD;
  string        testS;

  Arg("q1", q1Limit, 1);
  Arg("qi", testI, 2);
  Arg("qd", testD, 5.0);
  Arg("qs", testS, "This is a test");

  Arg::ProcessArgs(argc, argv);
  cout << "q1L " << q1Limit
       << " testI " << testI
       << " testD " << testD
       << " testS " << testS << endl;
  StringVec_t::size_type nneq = Arg::NumberNonEquiv();
  cout << "Number non-equiv " << nneq << endl;
  for (StringVec_t::size_type i = 0; i < nneq; ++i)
    {
      cout << "nneq " << i << " is " << Arg::NonEquiv(i) << endl;
    }
}
#endif
