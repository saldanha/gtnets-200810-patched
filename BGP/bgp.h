/****************************************************************************/
/*  File:            bgp.h                                                  */
/*  Author:          Sunitha Beeram                                         */
/*  Email:           sunithab@ece.gatech.edu                                */
/*  Documentation:   TBD                                                    */
/*                                                                          */
/*  Version:         1.0beta                                                */
/*                                                                          */
/*  Copyright (c) 2005 Georgia Institute of Technology                      */
/*  This program is free software; you can redistribute it and/or           */
/*  modify it under the terms of the GNU General Public License             */
/*  as published by the Free Software Foundation; either version 2          */
/*  of the License, or (at your option) any later version.                  */
/*                                                                          */
/*  This program is distributed in the hope that it will be useful,         */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of          */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           */
/*  GNU General Public License for more details.                            */
/*                                                                          */
/****************************************************************************/

#ifndef BGP_H
#define BGP_H

#include "../SRC/ipaddr.h"
#include "../SRC/application.h"
#include "../SRC/tcp.h"
#include "routemsg.h"
#include "../SRC/event.h"

#include <vector>
#include <list>
#include <regex.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <assert.h>

using namespace std;

class BGPPeer;
class BGPEvent;
class BGPTimer;
class BGP;

//BGP Peer Connection

typedef list<RouteMsg>  RMsgList_t;   // Queue of pending messages

class BGPPeer {
 public:
  BGPPeer();

 public:
  TCP*   tcp; // TCP endpoint for peer
  //evaluate if this field is necessary
  bool   connected; // True if connected, false if pending
  int       m_rxbytes; // Pending received bytes from this peer
  RMsgList_t m_msgs;    // Queue of pending msgs from this peer
};

typedef vector<BGPPeer> PeerVec_t;
  
//define BGPEvent class with BGPEvent_t and appropriate constructs for *master*,thread_to_be_fetched and other variables used in BGP::Timeout of bgp++

class BGPEvent : public Event {
 public:
typedef enum { MAIN,FETCH1,FETCH2,EXECUTE,COMMAND} BGPEvent_t; //mapped from bgp++ bgpresched events
  BGPEvent(){};
  BGPEvent(Event_t ev):Event(ev),argv(NULL),thread_to_be_fetched(NULL){};

  public:
	char *argv;
	struct thread* thread_to_be_fetched;

};

class BGPTimer : public Handler {
 public:
  BGPTimer(BGP* a) : Handler(){ agent = a;};
  void Schedule(double delay, Event* e);
  void expire(Event* e);
  void Cancel(Event* e);
  void Handle(Event* e, Time_t t);
  double expire_;
 protected:
  BGP* agent;

};


/* definitions found in zebra .c and .h files*/

/****** sockunion.h*********/

union sockunion
{
  struct sockaddr sa;
  struct sockaddr_in sin;
};

enum connect_result
  {
    connect_error,
    connect_success,
    connect_in_progress
  };

/* Sockunion address string length.  Same as INET6_ADDRSTRLEN. */
#define SU_ADDRSTRLEN 46
#define sockunion_family(X)  (X)->sa.sa_family

#define TIME_BUF 27

/*****zebra.h******/

/* Zebra message types. */
#define ZEBRA_INTERFACE_ADD                1
#define ZEBRA_INTERFACE_DELETE             2
#define ZEBRA_INTERFACE_ADDRESS_ADD        3
#define ZEBRA_INTERFACE_ADDRESS_DELETE     4
#define ZEBRA_INTERFACE_UP                 5
#define ZEBRA_INTERFACE_DOWN               6
#define ZEBRA_IPV4_ROUTE_ADD               7
#define ZEBRA_IPV4_ROUTE_DELETE            8
#define ZEBRA_IPV6_ROUTE_ADD               9
#define ZEBRA_IPV6_ROUTE_DELETE           10
#define ZEBRA_REDISTRIBUTE_ADD            11
#define ZEBRA_REDISTRIBUTE_DELETE         12
#define ZEBRA_REDISTRIBUTE_DEFAULT_ADD    13
#define ZEBRA_REDISTRIBUTE_DEFAULT_DELETE 14
#define ZEBRA_IPV4_NEXTHOP_LOOKUP         15
#define ZEBRA_IPV6_NEXTHOP_LOOKUP         16
#define ZEBRA_IPV4_IMPORT_LOOKUP          17
#define ZEBRA_IPV6_IMPORT_LOOKUP          18
#define ZEBRA_MESSAGE_MAX                 19

/* Zebra route's types. */
#define ZEBRA_ROUTE_SYSTEM               0
#define ZEBRA_ROUTE_KERNEL               1
#define ZEBRA_ROUTE_CONNECT              2
#define ZEBRA_ROUTE_STATIC               3
#define ZEBRA_ROUTE_RIP                  4
#define ZEBRA_ROUTE_RIPNG                5
#define ZEBRA_ROUTE_OSPF                 6
#define ZEBRA_ROUTE_OSPF6                7
#define ZEBRA_ROUTE_BGP                  8
#define ZEBRA_ROUTE_MAX                  9

/* Zebra's family types. */
#define ZEBRA_FAMILY_IPV4                1
#define ZEBRA_FAMILY_IPV6                2
#define ZEBRA_FAMILY_MAX                 3

/* Error codes of zebra. */
#define ZEBRA_ERR_RTEXIST               -1
#define ZEBRA_ERR_RTUNREACH             -2
#define ZEBRA_ERR_EPERM                 -3
#define ZEBRA_ERR_RTNOEXIST             -4

/* Zebra message flags */
#define ZEBRA_FLAG_INTERNAL           0x01
#define ZEBRA_FLAG_SELFROUTE          0x02
#define ZEBRA_FLAG_BLACKHOLE          0x04
#define ZEBRA_FLAG_IBGP               0x08
#define ZEBRA_FLAG_SELECTED           0x10
#define ZEBRA_FLAG_CHANGED            0x20
#define ZEBRA_FLAG_STATIC             0x40

/* Zebra nexthop flags. */
#define ZEBRA_NEXTHOP_IFINDEX            1
#define ZEBRA_NEXTHOP_IFNAME             2
#define ZEBRA_NEXTHOP_IPV4               3
#define ZEBRA_NEXTHOP_IPV4_IFINDEX       4
#define ZEBRA_NEXTHOP_IPV4_IFNAME        5
#define ZEBRA_NEXTHOP_IPV6               6
#define ZEBRA_NEXTHOP_IPV6_IFINDEX       7
#define ZEBRA_NEXTHOP_IPV6_IFNAME        8
#define ZEBRA_NEXTHOP_BLACKHOLE          9 

#ifndef INADDR_LOOPBACK
#define	INADDR_LOOPBACK	0x7f000001	/* Internet address 127.0.0.1.  */
#endif

/* Address family numbers from RFC1700. */
#define AFI_IP                    1
#define AFI_IP6                   2
#define AFI_MAX                   3

/* Subsequent Address Family Identifier. */
#define SAFI_UNICAST              1
#define SAFI_MULTICAST            2
#define SAFI_UNICAST_MULTICAST    3
#define SAFI_MPLS_VPN             4
#define SAFI_MAX                  5

/* Filter direction.  */
#define FILTER_IN                 0
#define FILTER_OUT                1
#define FILTER_MAX                2

/* Default Administrative Distance of each protocol. */
#define ZEBRA_KERNEL_DISTANCE_DEFAULT      0
#define ZEBRA_CONNECT_DISTANCE_DEFAULT     0
#define ZEBRA_STATIC_DISTANCE_DEFAULT      1
#define ZEBRA_RIP_DISTANCE_DEFAULT       120
#define ZEBRA_RIPNG_DISTANCE_DEFAULT     120
#define ZEBRA_OSPF_DISTANCE_DEFAULT      110
#define ZEBRA_OSPF6_DISTANCE_DEFAULT     110
#define ZEBRA_IBGP_DISTANCE_DEFAULT      200
#define ZEBRA_EBGP_DISTANCE_DEFAULT       20

/* Flag manipulation macros. */
#define CHECK_FLAG(V,F)      ((V) & (F))
#define SET_FLAG(V,F)        (V) = (V) | (F)
#define UNSET_FLAG(V,F)      (V) = (V) & ~(F)

/* AFI and SAFI type. */
typedef u_int16_t afi_t;
typedef u_char safi_t;

/* Zebra types. */
typedef u_int16_t zebra_size_t;
typedef u_int8_t zebra_command_t;

/* FIFO -- first in first out structure and macros.  */
struct fifo
{
  struct fifo *next;
  struct fifo *prev;
};

#define FIFO_INIT(F)                                  \
  do {                                                \
    struct fifo *Xfifo = (struct fifo *)(F);          \
    Xfifo->next = Xfifo->prev = Xfifo;                \
  } while (0)

#define FIFO_ADD(F,N)                                 \
  do {                                                \
    struct fifo *Xfifo = (struct fifo *)(F);          \
    struct fifo *Xnode = (struct fifo *)(N);          \
    Xnode->next = Xfifo;                              \
    Xnode->prev = Xfifo->prev;                        \
    Xfifo->prev = Xfifo->prev->next = Xnode;          \
  } while (0)

#define FIFO_DEL(N)                                   \
  do {                                                \
    struct fifo *Xnode = (struct fifo *)(N);          \
    Xnode->prev->next = Xnode->next;                  \
    Xnode->next->prev = Xnode->prev;                  \
  } while (0)

#define FIFO_HEAD(F)                                  \
  ((((struct fifo *)(F))->next == (struct fifo *)(F)) \
  ? NULL : (F)->next)

#define FIFO_EMPTY(F)                                 \
  (((struct fifo *)(F))->next == (struct fifo *)(F))

#define FIFO_TOP(F)                                   \
  (FIFO_EMPTY(F) ? NULL : ((struct fifo *)(F))->next)

/*bgp_advertise.h*/

/* BGP advertise FIFO.  */
struct bgp_advertise_fifo
{
  struct bgp_advertise *next;
  struct bgp_advertise *prev;
};

/* BGP advertise attribute.  */
struct bgp_advertise_attr
{
  /* Head of advertisement pointer. */
  struct bgp_advertise *adv;

  /* Reference counter.  */
  unsigned long refcnt;

  /* Attribute pointer to be announced.  */
  struct attr *attr;
};

/* BGP adjacency out.  */
struct bgp_adj_out
{
  /* Lined list pointer.  */
  struct bgp_adj_out *next;
  struct bgp_adj_out *prev;

  /* Advertised peer.  */
  struct peer *peer;

  /* Advertised attribute.  */
  struct attr *attr;

  /* Advertisement information.  */
  struct bgp_advertise *adv;
};

/* BGP adjacency in. */
struct bgp_adj_in
{
  /* Linked list pointer.  */
  struct bgp_adj_in *next;
  struct bgp_adj_in *prev;

  /* Received peer.  */
  struct peer *peer;

  /* Received attribute.  */
  struct attr *attr;
};

/* BGP advertisement list.  */
struct bgp_synchronize
{
  struct bgp_advertise_fifo update;
  struct bgp_advertise_fifo withdraw;
  struct bgp_advertise_fifo withdraw_low;
};

/* BGP adjacency linked list.  */
#define BGP_INFO_ADD(N,A,TYPE)                        \
  do {                                                \
    (A)->prev = NULL;                                 \
    (A)->next = (N)->TYPE;                            \
    if ((N)->TYPE)                                    \
      (N)->TYPE->prev = (A);                          \
    (N)->TYPE = (A);                                  \
  } while (0)

#define BGP_INFO_DEL(N,A,TYPE)                        \
  do {                                                \
    if ((A)->next)                                    \
      (A)->next->prev = (A)->prev;                    \
    if ((A)->prev)                                    \
      (A)->prev->next = (A)->next;                    \
    else                                              \
      (N)->TYPE = (A)->next;                          \
  } while (0)

#define BGP_ADJ_IN_ADD(N,A)    BGP_INFO_ADD(N,A,adj_in)
#define BGP_ADJ_IN_DEL(N,A)    BGP_INFO_DEL(N,A,adj_in)
#define BGP_ADJ_OUT_ADD(N,A)   BGP_INFO_ADD(N,A,adj_out)
#define BGP_ADJ_OUT_DEL(N,A)   BGP_INFO_DEL(N,A,adj_out)

/*bgp_aspath.h*/

/* AS path segment type.  */
#define AS_SET                       1
#define AS_SEQUENCE                  2
#define AS_CONFED_SEQUENCE           3
#define AS_CONFED_SET                4

/* Private AS range defined in RFC2270.  */
#define BGP_PRIVATE_AS_MIN       64512
#define BGP_PRIVATE_AS_MAX       65535

/* AS path may be include some AsSegments.  */
struct aspath 
{
  /* Reference count to this aspath.  */
  unsigned long refcnt;

  /* Rawdata length.  */
  int length;

  /* AS count.  */
  int count;

  /* Rawdata.  */
  caddr_t data;

  /* String expression of AS path.  This string is used by vty output
     and AS path regular expression match.  */
  char *str;
};

#define ASPATH_STR_DEFAULT_LEN 32

/******* zebra_zclient.h********/

/* For input/output buffer to zebra. */
#define ZEBRA_MAX_PACKET_SIZ          4096

/* Zebra header size. */
#define ZEBRA_HEADER_SIZE                3

/* Zebra API message flag. */
#define ZAPI_MESSAGE_NEXTHOP  0x01
#define ZAPI_MESSAGE_IFINDEX  0x02
#define ZAPI_MESSAGE_DISTANCE 0x04
#define ZAPI_MESSAGE_METRIC   0x08

/***** if.h *********/

#define INTERFACE_NAMSIZ      20
#define INTERFACE_HWADDR_MAX  20

/* Internal If indexes start at 0xFFFFFFFF and go down to 1 greater
   than this */
#define IFINDEX_INTERNBASE 0x80000000

#ifdef HAVE_PROC_NET_DEV
struct if_stats
{
  unsigned long rx_packets;   /* total packets received       */
  unsigned long tx_packets;   /* total packets transmitted    */
  unsigned long rx_bytes;     /* total bytes received         */
  unsigned long tx_bytes;     /* total bytes transmitted      */
  unsigned long rx_errors;    /* bad packets received         */
  unsigned long tx_errors;    /* packet transmit problems     */
  unsigned long rx_dropped;   /* no space in linux buffers    */
  unsigned long tx_dropped;   /* no space available in linux  */
  unsigned long rx_multicast; /* multicast packets received   */
  unsigned long rx_compressed;
  unsigned long tx_compressed;
  unsigned long collisions;

  /* detailed rx_errors: */
  unsigned long rx_length_errors;
  unsigned long rx_over_errors;       /* receiver ring buff overflow  */
  unsigned long rx_crc_errors;        /* recved pkt with crc error    */
  unsigned long rx_frame_errors;      /* recv'd frame alignment error */
  unsigned long rx_fifo_errors;       /* recv'r fifo overrun          */
  unsigned long rx_missed_errors;     /* receiver missed packet     */
  /* detailed tx_errors */
  unsigned long tx_aborted_errors;
  unsigned long tx_carrier_errors;
  unsigned long tx_fifo_errors;
  unsigned long tx_heartbeat_errors;
  unsigned long tx_window_errors;
};
#endif /* HAVE_PROC_NET_DEV */

/* Interface structure */
struct interface 
{
  /* Interface name. */
  char name[INTERFACE_NAMSIZ + 1];

  /* Interface index. */
  unsigned int ifindex;

  /* Zebra internal interface status */
  u_char status;
#define ZEBRA_INTERFACE_ACTIVE     (1 << 0)
#define ZEBRA_INTERFACE_SUB        (1 << 1)
  
  /* Interface flags. */
  unsigned long flags;

  /* Interface metric */
  int metric;

  /* Interface MTU. */
  int mtu;

  /* Hardware address. */
#ifdef HAVE_SOCKADDR_DL
  struct sockaddr_dl sdl;
#else
  unsigned short hw_type;
  u_char hw_addr[INTERFACE_HWADDR_MAX];
  int hw_addr_len;
#endif /* HAVE_SOCKADDR_DL */

  /* interface bandwidth, kbits */
  unsigned int bandwidth;
  
  /* description of the interface. */
  char *desc;			

  /* Distribute list. */
  void *distribute_in;
  void *distribute_out;

  /* Connected address list. */
  struct llist* connected;

  /* Daemon specific interface data pointer. */
  void *info;

  /* Statistics fileds. */
#ifdef HAVE_PROC_NET_DEV
  struct if_stats stats;
#endif /* HAVE_PROC_NET_DEV */  
#ifdef HAVE_NET_RT_IFLIST
  struct if_data stats;
#endif /* HAVE_NET_RT_IFLIST */
};

/* Connected address structure. */
struct connected
{
  /* Attached interface. */
  struct interface *ifp;

  /* Flags for configuration. */
  u_char conf;
#define ZEBRA_IFC_REAL         (1 << 0)
#define ZEBRA_IFC_CONFIGURED   (1 << 1)

  /* Flags for connected address. */
  u_char flags;
#define ZEBRA_IFA_SECONDARY   (1 << 0)

  /* Address of connected network. */
  struct prefix *address;
  struct prefix *destination;

  /* Label for Linux 2.2.X and upper. */
  char *label;
};

/* Interface hook sort. */
#define IF_NEW_HOOK   0
#define IF_DELETE_HOOK 1

/* There are some interface flags which are only supported by some
   operating system. */

#ifndef IFF_NOTRAILERS
#define IFF_NOTRAILERS 0x0
#endif /* IFF_NOTRAILERS */
#ifndef IFF_OACTIVE
#define IFF_OACTIVE 0x0
#endif /* IFF_OACTIVE */
#ifndef IFF_SIMPLEX
#define IFF_SIMPLEX 0x0
#endif /* IFF_SIMPLEX */
#ifndef IFF_LINK0
#define IFF_LINK0 0x0
#endif /* IFF_LINK0 */
#ifndef IFF_LINK1
#define IFF_LINK1 0x0
#endif /* IFF_LINK1 */
#ifndef IFF_LINK2
#define IFF_LINK2 0x0
#endif /* IFF_LINK2 */


/****** bgp_nexthop.h******/


#define BGP_SCAN_INTERVAL_DEFAULT   60
#define BGP_IMPORT_INTERVAL_DEFAULT 15

/* BGP nexthop cache value structure. */
struct bgp_nexthop_cache
{
  /* This nexthop exists in IGP. */
  u_char valid;

  /* Nexthop is changed. */
  u_char changed;

  /* Nexthop is changed. */
  u_char metricchanged;

  /* IGP route's metric. */
  u_int32_t metric;

  /* Nexthop number and nexthop linked list.*/
  u_char nexthop_num;
  struct nexthop *nexthop;
};




/*****from bgpd.h******/

/* Typedef BGP specific types.  */
typedef u_int16_t as_t;
typedef u_long bgp_size_t;

/* BGP master for system wide configurations and variables.  */
struct bgp_master
{
  /* BGP instance list.  */
  struct llist *bgp;

  /* BGP thread master.  */
  struct thread_master *master;

  /* BGP start time.  */
  time_t start_time;

  /* Various BGP global configuration.  */
  u_char options;
#define BGP_OPT_NO_FIB                   (1 << 0)
#define BGP_OPT_MULTIPLE_INSTANCE        (1 << 1)
#define BGP_OPT_CONFIG_CISCO             (1 << 2)
};

/* BGP instance structure.  */
struct bgp 
{
  /* AS number of this BGP instance.  */
  as_t as;

  /* Name of this BGP instance.  */
  char *name;
  /* Self peer.  */
  struct peer *peer_self;

  /* BGP peer. */
  struct llist *peer;

  /* BGP peer group.  */
  struct llist *group;

  /* BGP peer-conf */
	struct llist *peer_conf;

  /* BGP configuration.  */
  u_int16_t config;
#define BGP_CONFIG_ROUTER_ID              (1 << 0)
#define BGP_CONFIG_CLUSTER_ID             (1 << 1)
#define BGP_CONFIG_CONFEDERATION          (1 << 2)
#define BGP_CONFIG_DEFAULT_LOCAL_PREF     (1 << 3)

  /*BGP identifier */
  struct in_addr id;
  
  /*Local sockunion*/
  union sockunion su;

  /* BGP router identifier.  */
  struct in_addr router_id;

  /* BGP route reflector cluster ID.  */
  struct in_addr cluster_id;

  /* BGP confederation information.  */
  as_t confed_id;
  as_t *confed_peers;
  int confed_peers_cnt;

  /* BGP flags. */
  u_int16_t flags;
#define BGP_FLAG_ALWAYS_COMPARE_MED       (1 << 0)
#define BGP_FLAG_DETERMINISTIC_MED        (1 << 1)
#define BGP_FLAG_MED_MISSING_AS_WORST     (1 << 2)
#define BGP_FLAG_MED_CONFED               (1 << 3)
#define BGP_FLAG_NO_DEFAULT_IPV4          (1 << 4)
#define BGP_FLAG_NO_CLIENT_TO_CLIENT      (1 << 5)
#define BGP_FLAG_ENFORCE_FIRST_AS         (1 << 6)
#define BGP_FLAG_COMPARE_ROUTER_ID        (1 << 7)
#define BGP_FLAG_ASPATH_IGNORE            (1 << 8)
#define BGP_FLAG_IMPORT_CHECK             (1 << 9)
#define BGP_FLAG_NO_FAST_EXT_FAILOVER     (1 << 10)
#define BGP_FLAG_LOG_NEIGHBOR_CHANGES     (1 << 11)

  /* BGP Per AF flags */
  u_int16_t af_flags[AFI_MAX][SAFI_MAX];
#define BGP_CONFIG_DAMPENING              (1 << 0)

  /* Static route configuration.  */
  struct bgp_table *route[AFI_MAX][SAFI_MAX];

  /* Aggregate address configuration.  */
  struct bgp_table *aggregate[AFI_MAX][SAFI_MAX];

  /* BGP routing information base.  */
  struct bgp_table *rib[AFI_MAX][SAFI_MAX];

  /* BGP redistribute configuration. */
  u_char redist[AFI_MAX][ZEBRA_ROUTE_MAX];

  /* BGP redistribute metric configuration. */
  u_char redist_metric_flag[AFI_MAX][ZEBRA_ROUTE_MAX];
  u_int32_t redist_metric[AFI_MAX][ZEBRA_ROUTE_MAX];

  /* BGP redistribute route-map.  */
  struct
  {
    char *name;
    struct route_map *map;
  } rmap[AFI_MAX][ZEBRA_ROUTE_MAX];

  /* BGP distance configuration.  */
  u_char distance_ebgp;
  u_char distance_ibgp;
  u_char distance_local;
  
  /* BGP default local-preference.  */
  u_int32_t default_local_pref;

  /* BGP default timer.  */
  u_int32_t default_holdtime;
  u_int32_t default_keepalive;
};

/* BGP peer-group support. */
struct peer_group
{
  /* Name of the peer-group. */
  char *name;

  /* Pointer to BGP.  */
  struct bgp *bgp;
  
  /* Peer-group client list. */
  struct llist *peer;

  /* Peer-group config */
  struct peer *conf;
};

/* BGP Notify message format. */
struct bgp_notify 
{
  u_char code;
  u_char subcode;
  char *data;
  bgp_size_t length;
};

/* Next hop self address. */
struct bgp_nexthop
{
  struct interface *ifp;
  struct in_addr v4;
#ifdef HAVE_IPV6
  struct in6_addr v6_global;
  struct in6_addr v6_local;
#endif /* HAVE_IPV6 */  
};

/* BGP router distinguisher value.  */
#define BGP_RD_SIZE                8

struct bgp_rd
{
  u_char val[BGP_RD_SIZE];
};

/* BGP filter structure. */
struct bgp_filter
{
  /* Distribute-list.  */
  struct 
  {
    char *name;
    struct access_list *alist;
  } dlist[FILTER_MAX];

  /* Prefix-list.  */
  struct
  {
    char *name;
    struct prefix_list *plist;
  } plist[FILTER_MAX];

  /* Filter-list.  */
  struct
  {
    char *name;
    struct as_list *aslist;
  } aslist[FILTER_MAX];

  /* Route-map.  */
  struct
  {
    char *name;
    struct route_map *map;
  } map[FILTER_MAX];

  /* Unsuppress-map.  */
  struct
  {
    char *name;
    struct route_map *map;
  } usmap;
};

/* BGP peer configuration. */
struct peer_conf
{
  /* Pointer to BGP structure. */
  struct bgp *bgp;

  /* Pointer to peer. */
  struct peer *peer;

  /* Address Family Configuration. */
  u_char afc[AFI_MAX][SAFI_MAX];

  /* Prefix count. */
  unsigned long pcount[AFI_MAX][SAFI_MAX];

  /* Max prefix count. */
  unsigned long pmax[AFI_MAX][SAFI_MAX];
  u_char pmax_warning[AFI_MAX][SAFI_MAX];

  /* Filter structure. */
  struct bgp_filter filter[AFI_MAX][SAFI_MAX];
};

#ifdef BGP_MRAI
struct bgp_routeadv_list {
  struct bgp_routeadv_list *next;
  struct bgp_routeadv_list *prev;
  struct thread *t;
};
#endif


typedef list<pair<struct prefix,double> >    Prefix2Timestamp_t;

/* default-originate route-map.  */
struct default_rmap
{
   char *name;
   struct route_map *map;
} ;


/* BGP neighbor structure. */
struct peer
{
    
    /*pointer to BGP instance*/
    
    BGP* Bgp;

    /* BGP structure.  */
    struct bgp *bgp;
    
    /* BGP peer group.  */
    struct peer_group *group;
    u_char af_group[AFI_MAX][SAFI_MAX];
    
    /* Peer's remote AS number. */
    as_t as;			
    
    /* Peer's local AS number. */
    as_t local_as;
    
    /* Peer's Change local AS number. */
    as_t change_local_as;
    
    /* Remote router ID. */
    struct in_addr remote_id;
    
    /* Local router ID. */
    struct in_addr local_id;
    
    /* Packet receive and send buffer. */
    struct stream *ibuf;
    struct stream_fifo *obuf;
    struct stream *work;
    
    /* Status of the peer. */
    int status;
    int ostatus;
    
    /* Peer information */
    TCP* listenAgent;			/* File descriptor */
    int ttl;			/* TTL of TCP connection to the peer. */
    char *desc;			/* Description of the peer. */
    unsigned short port;          /* Destination port for peer */
    char *host;			/* Printable address of the peer. */
    union sockunion su;		/* Sockunion address of the peer. */
    time_t uptime;		/* Last Up/Down time */
    time_t readtime;		/* Last read time */
    time_t resettime;		/* Last reset time */
    
    unsigned int ifindex;		/* ifindex of the BGP connection. */
    char *ifname;			/* bind interface name. */
    char *update_if;
    union sockunion *update_source;
    struct zlog *log;
    u_char version;		/* Peer BGP version. */
    
    union sockunion *su_local;	/* Sockunion of local address.  */
    union sockunion *su_remote;	/* Sockunion of remote address.  */
    int shared_network;		/* Is this peer shared same network. */
    struct bgp_nexthop nexthop;	/* Nexthop */
    
    /* Peer address family configuration. */
    u_char afc[AFI_MAX][SAFI_MAX];
    u_char afc_nego[AFI_MAX][SAFI_MAX];
    u_char afc_adv[AFI_MAX][SAFI_MAX];
    u_char afc_recv[AFI_MAX][SAFI_MAX];
    
    /* Capability Flags.*/
    u_char cap;
#define PEER_CAP_REFRESH_ADV                (1 << 0) /* refresh advertised */
#define PEER_CAP_REFRESH_OLD_RCV            (1 << 1) /* refresh old received */
#define PEER_CAP_REFRESH_NEW_RCV            (1 << 2) /* refresh rfc received */
#define PEER_CAP_DYNAMIC_ADV                (1 << 3) /* dynamic advertised */
#define PEER_CAP_DYNAMIC_RCV                (1 << 4) /* dynamic received */

    /* Capability Flags.*/
    u_int16_t af_cap[AFI_MAX][SAFI_MAX];
#define PEER_CAP_ORF_PREFIX_SM_ADV          (1 << 0) /* send-mode advertised */
#define PEER_CAP_ORF_PREFIX_RM_ADV          (1 << 1) /* receive-mode advertised */
#define PEER_CAP_ORF_PREFIX_SM_RCV          (1 << 2) /* send-mode received */
#define PEER_CAP_ORF_PREFIX_RM_RCV          (1 << 3) /* receive-mode received */
#define PEER_CAP_ORF_PREFIX_SM_OLD_RCV      (1 << 4) /* send-mode received */
#define PEER_CAP_ORF_PREFIX_RM_OLD_RCV      (1 << 5) /* receive-mode received */

    /* Global configuration flags. */
    u_int32_t flags;
#define PEER_FLAG_PASSIVE                   (1 << 0) /* passive mode */
#define PEER_FLAG_SHUTDOWN                  (1 << 1) /* shutdown */
#define PEER_FLAG_DONT_CAPABILITY           (1 << 2) /* dont-capability */
#define PEER_FLAG_OVERRIDE_CAPABILITY       (1 << 3) /* override-capability */
#define PEER_FLAG_STRICT_CAP_MATCH          (1 << 4) /* strict-match */
#define PEER_FLAG_NO_ROUTE_REFRESH_CAP      (1 << 5) /* route-refresh */
#define PEER_FLAG_DYNAMIC_CAPABILITY        (1 << 6) /* dynamic capability */
#define PEER_FLAG_ENFORCE_MULTIHOP          (1 << 7) /* enforce-multihop */
#define PEER_FLAG_LOCAL_AS_NO_PREPEND       (1 << 8) /* local-as no-prepend */

    /* Per AF configuration flags. */
    u_int32_t af_flags[AFI_MAX][SAFI_MAX];
#define PEER_FLAG_SEND_COMMUNITY            (1 << 0) /* send-community */
#define PEER_FLAG_SEND_EXT_COMMUNITY        (1 << 1) /* send-community ext. */
#define PEER_FLAG_NEXTHOP_SELF              (1 << 2) /* next-hop-self */
#define PEER_FLAG_REFLECTOR_CLIENT          (1 << 3) /* reflector-client */
#define PEER_FLAG_RSERVER_CLIENT            (1 << 4) /* route-server-client */
#define PEER_FLAG_SOFT_RECONFIG             (1 << 5) /* soft-reconfiguration */
#define PEER_FLAG_AS_PATH_UNCHANGED         (1 << 6) /* transparent-as */
#define PEER_FLAG_NEXTHOP_UNCHANGED         (1 << 7) /* transparent-next-hop */
#define PEER_FLAG_MED_UNCHANGED             (1 << 8) /* transparent-next-hop */
#define PEER_FLAG_DEFAULT_ORIGINATE         (1 << 9) /* default-originate */
#define PEER_FLAG_REMOVE_PRIVATE_AS         (1 << 10) /* remove-private-as */
#define PEER_FLAG_ALLOWAS_IN                (1 << 11) /* set allowas-in */
#define PEER_FLAG_ORF_PREFIX_SM             (1 << 12) /* orf capability send-mode */
#define PEER_FLAG_ORF_PREFIX_RM             (1 << 13) /* orf capability receive-mode */
#define PEER_FLAG_MAX_PREFIX                (1 << 14) /* maximum prefix */
#define PEER_FLAG_MAX_PREFIX_WARNING        (1 << 15) /* maximum prefix warning-only */

    /* default-originate route-map.  */
	struct default_rmap default_rmap[AFI_MAX][SAFI_MAX];
    
    /* Peer status flags. */
    u_int16_t sflags;
#define PEER_STATUS_ACCEPT_PEER	      (1 << 0) /* accept peer */
#define PEER_STATUS_PREFIX_OVERFLOW   (1 << 1) /* prefix-overflow */
#define PEER_STATUS_CAPABILITY_OPEN   (1 << 2) /* capability open send */
#define PEER_STATUS_HAVE_ACCEPT       (1 << 3) /* accept peer's parent */
#define PEER_STATUS_GROUP             (1 << 4) /* peer-group conf */

    /* Peer status af flags. */
    u_int16_t af_sflags[AFI_MAX][SAFI_MAX];
#define PEER_STATUS_ORF_PREFIX_SEND   (1 << 0) /* prefix-list send peer */
#define PEER_STATUS_ORF_WAIT_REFRESH  (1 << 1) /* wait refresh received peer */
#define PEER_STATUS_DEFAULT_ORIGINATE (1 << 2) /* default-originate peer */
#define PEER_STATUS_PREFIX_THRESHOLD  (1 << 3) /* exceed prefix-threshold */
#define PEER_STATUS_PREFIX_LIMIT      (1 << 4) /* exceed prefix-limit */

    /* Default attribute value for the peer. */
    u_int32_t config;
#define PEER_CONFIG_WEIGHT            (1 << 0) /* Default weight. */
#define PEER_CONFIG_TIMER             (1 << 1) /* keepalive & holdtime */
#define PEER_CONFIG_CONNECT           (1 << 2) /* connect */
#define PEER_CONFIG_ROUTEADV          (1 << 3) /* route advertise */
    u_int32_t weight;
    u_int32_t holdtime;
    u_int32_t keepalive;
    u_int32_t Connect;
    u_int32_t routeadv;
    
    /* Timer values. */
    u_int32_t v_start;
    u_int32_t v_connect;
    u_int32_t v_holdtime;
    u_int32_t v_keepalive;
    u_int32_t v_asorig;
    u_int32_t v_routeadv;
    
    /* Threads. */
    struct thread *t_read;
    struct thread *t_write;
    struct thread *t_start;
    struct thread *t_connect;
    struct thread *t_holdtime;
    struct thread *t_keepalive;
    struct thread *t_asorig;

	//pending timer pointer
	//used for per peer MRAI
    struct thread *t_routeadv;
    
    /* MRAI implementation*/
    
    // List of pending MRAI timers, for per prefix MRAI
    // there are more than one pending MRAI timers
    struct bgp_routeadv_list *t_routeadv_list;
    
    //per prefix list of last update timestamps
    Prefix2Timestamp_t  update_stamps;
    
    //List of supressed advertisements
    struct bgp_advertise *top_adv;
    
    /* Statistics field */
    u_int32_t open_in;		/* Open message input count */
    u_int32_t open_out;		/* Open message output count */
    u_int32_t update_in;		/* Update message input count */
    u_int32_t update_out;		/* Update message ouput count */
    Time_t update_time;		/* Update message received time. */
    u_int32_t keepalive_in;	/* Keepalive input count */
    u_int32_t keepalive_out;	/* Keepalive output count */
    u_int32_t notify_in;		/* Notify input count */
    u_int32_t notify_out;		/* Notify output count */
    u_int32_t refresh_in;		/* Route Refresh input count */
    u_int32_t refresh_out;	/* Route Refresh output count */
    u_int32_t dynamic_cap_in;	/* Dynamic Capability input count.  */
    u_int32_t dynamic_cap_out;	/* Dynamic Capability output count.  */
    
    /* BGP state count */
    u_int32_t established;	/* Established */
    u_int32_t dropped;		/* Dropped */

	/* Adj-RIBs-In.  */
	struct bgp_table *adj_in[AFI_MAX][SAFI_MAX];
	struct bgp_table *adj_out[AFI_MAX][SAFI_MAX];

    
    /* Syncronization list and time.  */
    struct bgp_synchronize *sync[AFI_MAX][SAFI_MAX];
    Time_t synctime;

    /* Send prefix count. */
    unsigned long scount[AFI_MAX][SAFI_MAX];
    
    /* Announcement attribute hash.  */
    struct hash *hash[AFI_MAX][SAFI_MAX];

	/* Linked peer configuration. */
	struct llist *conf;
    
    /* Notify data. */
    struct bgp_notify notify;
    
    /* Whole packet size to be read. */
    unsigned long packet_size;
    
    /* Filter structure. */
    struct bgp_filter filter[AFI_MAX][SAFI_MAX];
    
    /* ORF Prefix-list */
    struct prefix_list *orf_plist[AFI_MAX][SAFI_MAX];
    
    /* Prefix count. */
    unsigned long pcount[AFI_MAX][SAFI_MAX];
    
  /* Max prefix count. */
    unsigned long pmax[AFI_MAX][SAFI_MAX];
    u_char pmax_threshold[AFI_MAX][SAFI_MAX];
#define MAXIMUM_PREFIX_THRESHOLD_DEFAULT 75
    
    /* allowas-in. */
    char allowas_in[AFI_MAX][SAFI_MAX];
    
    /* peer reset cause */
    char last_reset;
#define PEER_DOWN_RID_CHANGE             1 /* bgp router-id command */
#define PEER_DOWN_REMOTE_AS_CHANGE       2 /* neighbor remote-as command */
#define PEER_DOWN_LOCAL_AS_CHANGE        3 /* neighbor local-as command */
#define PEER_DOWN_CLID_CHANGE            4 /* bgp cluster-id command */
#define PEER_DOWN_CONFED_ID_CHANGE       5 /* bgp confederation identifier command */
#define PEER_DOWN_CONFED_PEER_CHANGE     6 /* bgp confederation peer command */
#define PEER_DOWN_RR_CLIENT_CHANGE       7 /* neighbor route-reflector-client command */
#define PEER_DOWN_RS_CLIENT_CHANGE       8 /* neighbor route-server-client command */
#define PEER_DOWN_UPDATE_SOURCE_CHANGE   9 /* neighbor update-source command */
#define PEER_DOWN_AF_ACTIVATE           10 /* neighbor activate command */
#define PEER_DOWN_USER_SHUTDOWN         11 /* neighbor shutdown command */
#define PEER_DOWN_USER_RESET            12 /* clear ip bgp command */
#define PEER_DOWN_NOTIFY_RECEIVED       13 /* notification received */
#define PEER_DOWN_NOTIFY_SEND           14 /* notification send */
#define PEER_DOWN_CLOSE_SESSION         15 /* tcp session close */
#define PEER_DOWN_NEIGHBOR_DELETE       16 /* neghbor delete */
#define PEER_DOWN_RMAP_BIND             17 /* neghbor peer-group command */
#define PEER_DOWN_RMAP_UNBIND           18 /* no neighbor peer-group command */
#define PEER_DOWN_CAPABILITY_CHANGE     19 /* neighbor capability command */
#define PEER_DOWN_PASSIVE_CHANGE        20 /* neighbor passive command */
#define PEER_DOWN_MULTIHOP_CHANGE       21 /* neighbor multihop command */

  /* The kind of route-map Flags.*/
  u_char rmap_type;
#define PEER_RMAP_TYPE_IN             (1 << 0) /* neighbor route-map in */
#define PEER_RMAP_TYPE_OUT            (1 << 1) /* neighbor route-map out */
#define PEER_RMAP_TYPE_NETWORK        (1 << 2) /* network route-map */
#define PEER_RMAP_TYPE_REDISTRIBUTE   (1 << 3) /* redistribute route-map */
#define PEER_RMAP_TYPE_DEFAULT        (1 << 4) /* default-originate route-map */
#define PEER_RMAP_TYPE_NOSET          (1 << 5) /* not allow to set commands */
};

/* This structure's member directly points incoming packet data
   stream. */
struct bgp_nlri
{
  /* AFI.  */
  afi_t afi;

  /* SAFI.  */
  safi_t safi;

  /* Pointer to NLRI byte stream.  */
  u_char *nlri;

  /* Length of whole NLRI.  */
  bgp_size_t length;
};

/* BGP versions.  */
#define BGP_VERSION_4		                 4
#define BGP_VERSION_MP_4_DRAFT_00               40

/* Default BGP port number.  */
#define BGP_PORT_DEFAULT                       179

/* BGP message header and packet size.  */
#define BGP_MARKER_SIZE		                16
#define BGP_HEADER_SIZE		                19
#define BGP_MAX_PACKET_SIZE                   4096

/* BGP minimum message size.  */
#define BGP_MSG_OPEN_MIN_SIZE                   (BGP_HEADER_SIZE + 10)
#define BGP_MSG_UPDATE_MIN_SIZE                 (BGP_HEADER_SIZE + 4)
#define BGP_MSG_NOTIFY_MIN_SIZE                 (BGP_HEADER_SIZE + 2)
#define BGP_MSG_KEEPALIVE_MIN_SIZE              (BGP_HEADER_SIZE + 0)
#define BGP_MSG_ROUTE_REFRESH_MIN_SIZE          (BGP_HEADER_SIZE + 4)
#define BGP_MSG_CAPABILITY_MIN_SIZE             (BGP_HEADER_SIZE + 3)

/* BGP message types.  */
#define	BGP_MSG_OPEN		                 1
#define	BGP_MSG_UPDATE		                 2
#define	BGP_MSG_NOTIFY		                 3
#define	BGP_MSG_KEEPALIVE	                 4
#define BGP_MSG_ROUTE_REFRESH_NEW                5
#define BGP_MSG_CAPABILITY                       6
#define BGP_MSG_ROUTE_REFRESH_OLD              128

/* BGP open optional parameter.  */
#define BGP_OPEN_OPT_AUTH                        1
#define BGP_OPEN_OPT_CAP                         2

/* BGP4 attribute type codes.  */
#define BGP_ATTR_ORIGIN                          1
#define BGP_ATTR_AS_PATH                         2
#define BGP_ATTR_NEXT_HOP                        3
#define BGP_ATTR_MULTI_EXIT_DISC                 4
#define BGP_ATTR_LOCAL_PREF                      5
#define BGP_ATTR_ATOMIC_AGGREGATE                6
#define BGP_ATTR_AGGREGATOR                      7
#define BGP_ATTR_COMMUNITIES                     8
#define BGP_ATTR_ORIGINATOR_ID                   9
#define BGP_ATTR_CLUSTER_LIST                   10
#define BGP_ATTR_DPA                            11
#define BGP_ATTR_ADVERTISER                     12
#define BGP_ATTR_RCID_PATH                      13
#define BGP_ATTR_MP_REACH_NLRI                  14
#define BGP_ATTR_MP_UNREACH_NLRI                15
#define BGP_ATTR_EXT_COMMUNITIES                16

/* BGP update origin.  */
#define BGP_ORIGIN_IGP                           0
#define BGP_ORIGIN_EGP                           1
#define BGP_ORIGIN_INCOMPLETE                    2

/* BGP notify message codes.  */
#define BGP_NOTIFY_HEADER_ERR                    1
#define BGP_NOTIFY_OPEN_ERR                      2
#define BGP_NOTIFY_UPDATE_ERR                    3
#define BGP_NOTIFY_HOLD_ERR                      4
#define BGP_NOTIFY_FSM_ERR                       5
#define BGP_NOTIFY_CEASE                         6
#define BGP_NOTIFY_CAPABILITY_ERR                7
#define BGP_NOTIFY_MAX	                         8

/* BGP_NOTIFY_HEADER_ERR sub codes.  */
#define BGP_NOTIFY_HEADER_NOT_SYNC               1
#define BGP_NOTIFY_HEADER_BAD_MESLEN             2
#define BGP_NOTIFY_HEADER_BAD_MESTYPE            3
#define BGP_NOTIFY_HEADER_MAX                    4

/* BGP_NOTIFY_OPEN_ERR sub codes.  */
#define BGP_NOTIFY_OPEN_UNSUP_VERSION            1
#define BGP_NOTIFY_OPEN_BAD_PEER_AS              2
#define BGP_NOTIFY_OPEN_BAD_BGP_IDENT            3
#define BGP_NOTIFY_OPEN_UNSUP_PARAM              4
#define BGP_NOTIFY_OPEN_AUTH_FAILURE             5
#define BGP_NOTIFY_OPEN_UNACEP_HOLDTIME          6
#define BGP_NOTIFY_OPEN_UNSUP_CAPBL              7
#define BGP_NOTIFY_OPEN_MAX                      8

/* BGP_NOTIFY_UPDATE_ERR sub codes.  */
#define BGP_NOTIFY_UPDATE_MAL_ATTR               1
#define BGP_NOTIFY_UPDATE_UNREC_ATTR             2
#define BGP_NOTIFY_UPDATE_MISS_ATTR              3
#define BGP_NOTIFY_UPDATE_ATTR_FLAG_ERR          4
#define BGP_NOTIFY_UPDATE_ATTR_LENG_ERR          5
#define BGP_NOTIFY_UPDATE_INVAL_ORIGIN           6
#define BGP_NOTIFY_UPDATE_AS_ROUTE_LOOP          7
#define BGP_NOTIFY_UPDATE_INVAL_NEXT_HOP         8
#define BGP_NOTIFY_UPDATE_OPT_ATTR_ERR           9
#define BGP_NOTIFY_UPDATE_INVAL_NETWORK         10
#define BGP_NOTIFY_UPDATE_MAL_AS_PATH           11
#define BGP_NOTIFY_UPDATE_MAX                   12

/* BGP_NOTIFY_CEASE sub codes (draft-ietf-idr-cease-subcode-03).  */
#define BGP_NOTIFY_CEASE_MAX_PREFIX              1
#define BGP_NOTIFY_CEASE_ADMIN_SHUTDOWN          2
#define BGP_NOTIFY_CEASE_PEER_UNCONFIG           3
#define BGP_NOTIFY_CEASE_ADMIN_RESET             4
#define BGP_NOTIFY_CEASE_CONNECT_REJECT          5
#define BGP_NOTIFY_CEASE_CONFIG_CHANGE           6
#define BGP_NOTIFY_CEASE_CONNECT_COLLISION       7
#define BGP_NOTIFY_CEASE_MAX                     8

/* BGP_NOTIFY_CAPABILITY_ERR sub codes (draft-ietf-idr-dynamic-cap-02). */
#define BGP_NOTIFY_CAPABILITY_INVALID_ACTION     1
#define BGP_NOTIFY_CAPABILITY_INVALID_LENGTH     2
#define BGP_NOTIFY_CAPABILITY_MALFORMED_CODE     3
#define BGP_NOTIFY_CAPABILITY_MAX                4

/* BGP finite state machine status.  */
#define Idle                                     1
#define connect                                  2 /*Originally Connect, made connect to avoid name collision with TCP::Connect*/
#define Active                                   3
#define OpenSent                                 4
#define OpenConfirm                              5
#define Established                              6
#define BGP_STATUS_MAX                           7

/* BGP finite state machine events.  */
#define BGP_Start                                1
#define BGP_Stop                                 2
#define TCP_connection_open                      3
#define TCP_connection_closed                    4
#define TCP_connection_open_failed               5
#define TCP_fatal_error                          6
#define ConnectRetry_timer_expired               7
#define Hold_Timer_expired                       8
#define KeepAlive_timer_expired                  9
#define Receive_OPEN_message                    10
#define Receive_KEEPALIVE_message               11
#define Receive_UPDATE_message                  12
#define Receive_NOTIFICATION_message            13
#define BGP_EVENTS_MAX                          14

/* BGP timers default value.  */
#define BGP_INIT_START_TIMER                     5
#define BGP_ERROR_START_TIMER                   30
#define BGP_DEFAULT_HOLDTIME                   180
#define BGP_DEFAULT_KEEPALIVE                   60 
#define BGP_DEFAULT_ASORIGINATE                 15
#define BGP_DEFAULT_EBGP_ROUTEADV               30
#define BGP_DEFAULT_IBGP_ROUTEADV                5
#define BGP_CLEAR_CONNECT_RETRY                 20
#define BGP_DEFAULT_CONNECT_RETRY              120

/* BGP default local preference.  */
#define BGP_DEFAULT_LOCAL_PREF                 100

/* SAFI which used in open capability negotiation.  */
#define BGP_SAFI_VPNV4                         128
#define BGP_SAFI_VPNV6                         129

/* Max TTL value.  */
#define TTL_MAX                                255

/* BGP uptime string length.  */
#define BGP_UPTIME_LEN 25

/* Default configuration settings for bgpd.  */
#define BGP_VTY_PORT                          2605
#define BGP_VTYSH_PATH                "/tmp/.bgpd"
#define BGP_DEFAULT_CONFIG             "bgpd.conf"

/* Check AS path loop when we send NLRI.  */
/* #define BGP_SEND_ASPATH_CHECK */

/* IBGP/EBGP identifier.  We also have a CONFED peer, which is to say,
   a peer who's AS is part of our Confederation.  */
enum
{
  BGP_PEER_IBGP,
  BGP_PEER_EBGP,
  BGP_PEER_INTERNAL,
  BGP_PEER_CONFED
};

/* Flag for peer_clear_soft().  */
enum bgp_clear_type
{
  BGP_CLEAR_SOFT_NONE,
  BGP_CLEAR_SOFT_OUT,
  BGP_CLEAR_SOFT_IN,
  BGP_CLEAR_SOFT_BOTH,
  BGP_CLEAR_SOFT_IN_ORF_PREFIX
};

/* Macros. */
#define BGP_INPUT(P)         ((P)->ibuf)
#define BGP_INPUT_PNT(P)     (STREAM_PNT(BGP_INPUT(P)))

/* Macro to check BGP information is alive or not.  */
#define BGP_INFO_HOLDDOWN(BI)                         \
  (! CHECK_FLAG ((BI)->flags, BGP_INFO_VALID)         \
   || CHECK_FLAG ((BI)->flags, BGP_INFO_HISTORY)      \
   || CHECK_FLAG ((BI)->flags, BGP_INFO_DAMPED))

/* Count prefix size from mask length */
#define PSIZE(a) (((a) + 7) / (8))

/* BGP error codes.  */
#define BGP_SUCCESS                               0
#define BGP_ERR_INVALID_VALUE                    -1
#define BGP_ERR_INVALID_FLAG                     -2
#define BGP_ERR_INVALID_AS                       -3
#define BGP_ERR_INVALID_BGP                      -4
#define BGP_ERR_PEER_GROUP_MEMBER                -5
#define BGP_ERR_MULTIPLE_INSTANCE_USED           -6
#define BGP_ERR_PEER_GROUP_MEMBER_EXISTS         -7
#define BGP_ERR_PEER_BELONGS_TO_GROUP            -8
#define BGP_ERR_PEER_GROUP_AF_UNCONFIGURED       -9
#define BGP_ERR_PEER_GROUP_NO_REMOTE_AS         -10
#define BGP_ERR_PEER_GROUP_CANT_CHANGE          -11
#define BGP_ERR_PEER_GROUP_MISMATCH             -12
#define BGP_ERR_PEER_GROUP_PEER_TYPE_DIFFERENT  -13
#define BGP_ERR_MULTIPLE_INSTANCE_NOT_SET       -14
#define BGP_ERR_AS_MISMATCH                     -15
#define BGP_ERR_PEER_INACTIVE                   -16
#define BGP_ERR_INVALID_FOR_PEER_GROUP_MEMBER   -17
#define BGP_ERR_PEER_GROUP_HAS_THE_FLAG         -18
#define BGP_ERR_PEER_FLAG_CONFLICT              -19
#define BGP_ERR_PEER_GROUP_SHUTDOWN             -20
#define BGP_ERR_PEER_FILTER_CONFLICT            -21
#define BGP_ERR_NOT_INTERNAL_PEER               -22
#define BGP_ERR_REMOVE_PRIVATE_AS               -23
#define BGP_ERR_AF_UNCONFIGURED                 -24
#define BGP_ERR_SOFT_RECONFIG_UNCONFIGURED      -25
#define BGP_ERR_INSTANCE_MISMATCH               -26
#define BGP_ERR_LOCAL_AS_ALLOWED_ONLY_FOR_EBGP  -27
#define BGP_ERR_CANNOT_HAVE_LOCAL_AS_SAME_AS    -28
#define BGP_ERR_MAX                             -29

/********bgpd.c*********/

/* peer_flag_change_type. */
enum peer_change_type
{
  peer_change_none,
  peer_change_reset,
  peer_change_reset_in,
  peer_change_reset_out,
};

struct peer_flag_action
{
  /* Peer's flag.  */
  u_int32_t flag;

  /* This flag can be set for peer-group member.  */
  u_char not_for_member;

  /* Action when the flag is changed.  */
  enum peer_change_type type;

  /* Peer down cause */
  u_char peer_down;
};

/**********bgp_aspath.c*************/

/* Attr. Flags and Attr. Type Code. */
#define AS_HEADER_SIZE        2	 

/* Two octet is used for AS value. */
#define AS_VALUE_SIZE         sizeof (as_t)

/* AS segment octet length. */
#define ASSEGMENT_LEN(X)  ((X)->length * AS_VALUE_SIZE + AS_HEADER_SIZE)

/* To fetch and store as segment value. */
struct assegment
{
  u_char type;
  u_char length;
  as_t asval[1];
};

enum as_token
  {
    as_token_asval,
    as_token_set_start,
    as_token_set_end,
    as_token_confed_start,
    as_token_confed_end,
    as_token_unknown
  };

/*bgp_community.h*/

/* Communities attribute.  */
struct community 
{
  /* Reference count of communities value.  */
  unsigned long refcnt;

  /* Communities value size.  */
  int size;

  /* Communities value.  */
  u_int32_t *val;

  /* String of community attribute.  This sring is used by vty output
     and expanded community-list for regular expression match.  */
  char *str;
};

/* Well-known communities value.  */
#define COMMUNITY_INTERNET              0x0
#define COMMUNITY_NO_EXPORT             0xFFFFFF01
#define COMMUNITY_NO_ADVERTISE          0xFFFFFF02
#define COMMUNITY_NO_EXPORT_SUBCONFED   0xFFFFFF03
#define COMMUNITY_LOCAL_AS              0xFFFFFF03

/* Macros of community attribute.  */
#define com_length(X)    ((X)->size * 4)
#define com_lastval(X)   ((X)->val + (X)->size - 1)
#define com_nthval(X,n)  ((X)->val + (n))

/*bgp_ecommunity.h*/

/* High-order octet of the Extended Communities type field.  */
#define ECOMMUNITY_ENCODE_AS                0x00
#define ECOMMUNITY_ENCODE_IP                0x01

/* Low-order octet of the Extended Communityes type field.  */
#define ECOMMUNITY_ROUTE_TARGET             0x02
#define ECOMMUNITY_SITE_ORIGIN              0x03

/* Extended communities attribute string format.  */
#define ECOMMUNITY_FORMAT_ROUTE_MAP            0
#define ECOMMUNITY_FORMAT_COMMUNITY_LIST       1
#define ECOMMUNITY_FORMAT_DISPLAY              2

/* Extended Communities value is eight octet long.  */
#define ECOMMUNITY_SIZE                        8

/* Extended Communities attribute.  */
struct ecommunity
{
  /* Reference counter.  */
  unsigned long refcnt;

  /* Size of Extended Communities attribute.  */
  int size;

  /* Extended Communities value.  */
  u_char *val;

  /* Human readable format string.  */
  char *str;
};

/* Extended community value is eight octet.  */
struct ecommunity_val
{
  char val[ECOMMUNITY_SIZE];
};

#define ecom_length(X)    ((X)->size * ECOMMUNITY_SIZE)

/*bgp_attr.h*/

/* Simple bit mapping. */
#define BITMAP_NBBY 8

#define SET_BITMAP(MAP, NUM) \
        SET_FLAG (MAP[(NUM) / BITMAP_NBBY], 1 << ((NUM) % BITMAP_NBBY))

#define CHECK_BITMAP(MAP, NUM) \
        CHECK_FLAG (MAP[(NUM) / BITMAP_NBBY], 1 << ((NUM) % BITMAP_NBBY))

/* BGP Attribute type range. */
#define BGP_ATTR_TYPE_RANGE     256
#define BGP_ATTR_BITMAP_SIZE    (BGP_ATTR_TYPE_RANGE / BITMAP_NBBY)

/* BGP Attribute flags. */
#define BGP_ATTR_FLAG_OPTIONAL  0x80	/* Attribute is optional. */
#define BGP_ATTR_FLAG_TRANS     0x40	/* Attribute is transitive. */
#define BGP_ATTR_FLAG_PARTIAL   0x20	/* Attribute is partial. */
#define BGP_ATTR_FLAG_EXTLEN    0x10	/* Extended length flag. */

/* BGP attribute header must bigger than 2. */
#define BGP_ATTR_MIN_LEN        2       /* Attribute flag and type. */

/* BGP attribute structure. */
struct attr
{
  /* Reference count of this attribute. */
  unsigned long refcnt;

  /* Flag of attribute is set or not. */
  u_int32_t flag;

  /* Attributes. */
  u_char origin;
  struct in_addr nexthop;
  u_int32_t med;
  u_int32_t local_pref;
  as_t aggregator_as;
  struct in_addr aggregator_addr;
  u_int32_t weight;
  struct in_addr originator_id;
  struct cluster_list *cluster;

  u_char mp_nexthop_len;
#ifdef HAVE_IPV6
  struct in6_addr mp_nexthop_global;
  struct in6_addr mp_nexthop_local;
#endif /* HAVE_IPV6 */
  struct in_addr mp_nexthop_global_in;
  struct in_addr mp_nexthop_local_in;

  /* AS Path structure */
  struct aspath *aspath;

  /* Community structure */
  struct community *community;	

  /* Extended Communities attribute. */
  struct ecommunity *ecommunity;

  /* Unknown transitive attribute. */
  struct transit *transit;
};

#ifdef BGP_MRAI

struct bgp_advertise {
  struct bgp_advertise *next;
  struct bgp_advertise *prev;

  struct peer_conf *conf;
  struct prefix *p;
  struct attr attribute;
  afi_t afi;
  safi_t safi;
  struct peer *from;
  struct prefix_rd *prd;
  u_char *tag;

  /* FIFO for advertisement.  */
  struct bgp_advertise_fifo fifo;
  /* Prefix information.  */
  struct bgp_node *rn;
  /* Reference pointer.  */
  struct bgp_adj_out *adj;
  /* Advertisement attribute.  */
  struct bgp_advertise_attr *baa;
  /* BGP info.  */
  struct bgp_info *binfo;

};

struct bgp_mrai_info {
  struct peer *peer;
  struct prefix *p;
  struct bgp_info *bi;
};

#else
struct bgp_advertise
{
  /* FIFO for advertisement.  */
  struct bgp_advertise_fifo fifo;

  /* Link list for same attribute advertise.  */
  struct bgp_advertise *next;
  struct bgp_advertise *prev;

  /* Prefix information.  */
  struct bgp_node *rn;

  /* Reference pointer.  */
  struct bgp_adj_out *adj;

  /* Advertisement attribute.  */
  struct bgp_advertise_attr *baa;

  /* BGP info.  */
  struct bgp_info *binfo;
};
#endif



/* Router Reflector related structure. */
struct cluster_list
{
  unsigned long refcnt;
  int length;
  struct in_addr *list;
};

/* Unknown transit attribute. */
struct transit
{
  unsigned long refcnt;
  int length;
  u_char *val;
};

#define ATTR_FLAG_BIT(X)  (1 << ((X) - 1))


/*bgp_clist.h*/

/* Community-list deny and permit.  */
#define COMMUNITY_DENY                 0
#define COMMUNITY_PERMIT               1

/* Number and string based community-list name.  */
#define COMMUNITY_LIST_STRING          0
#define COMMUNITY_LIST_NUMBER          1

/* Community-list entry types.  */
#define COMMUNITY_LIST_STANDARD        0 /* Standard community-list.  */
#define COMMUNITY_LIST_EXPANDED        1 /* Expanded community-list.  */
#define COMMUNITY_LIST_AUTO            2 /* Automatically detected.  */
#define EXTCOMMUNITY_LIST_STANDARD     3 /* Standard extcommunity-list.  */
#define EXTCOMMUNITY_LIST_EXPANDED     4 /* Expanded extcommunity-list.  */
#define EXTCOMMUNITY_LIST_AUTO         5 /* Automatically detected.  */

/* Community-list.  */
struct community_list
{
  /* Name of the community-list.  */
  char *name;

  /* String or number.  */
  int sort;

  /* Link to upper list.  */
  struct community_list_list *parent;

  /* Linked list for other community-list.  */
  struct community_list *next;
  struct community_list *prev;

  /* Community-list entry in this community-list.  */
  struct community_entry *head;
  struct community_entry *tail;
};

/* Each entry in community-list.  */
struct community_entry
{
  struct community_entry *next;
  struct community_entry *prev;

  /* Permit or deny.  */
  u_char direct;

  /* Standard or expanded.  */
  u_char style;

  /* Any match.  */
  u_char any;

  /* Community structure.  */
  union
  {
    struct community *com;
    struct ecommunity *ecom;
  } u;

  /* Configuration string.  */
  char *config;

  /* Expanded community-list regular expression.  */
  regex_t *reg;
};

/* Linked list of community-list.  */
struct community_list_list
{
  struct community_list *head;
  struct community_list *tail;
};

/* Master structure of community-list and extcommunity-list.  */
struct community_list_master
{
  struct community_list_list num;
  struct community_list_list str;
};

/* Community-list handler.  community_list_init() returns this
   structure as handler.  */
struct community_list_handler
{
  /* Community-list.  */
  struct community_list_master community_list;

  /* Exteded community-list.  */
  struct community_list_master extcommunity_list;
};

/* Error code of community-list.  */
#define COMMUNITY_LIST_ERR_CANT_FIND_LIST        -1
#define COMMUNITY_LIST_ERR_MALFORMED_VAL         -2
#define COMMUNITY_LIST_ERR_STANDARD_CONFLICT     -3
#define COMMUNITY_LIST_ERR_EXPANDED_CONFLICT     -4


/*bgp_community.c*/
/* Community token enum. */
enum community_token
{
  community_token_val,
  community_token_no_export,
  community_token_no_advertise,
  community_token_local_as,
  community_token_unknown
};

/*bgp_ecommunity.c*/
/* Extended Communities token enum. */
enum ecommunity_token
{
  ecommunity_token_rt,
  ecommunity_token_soo,
  ecommunity_token_val,
  ecommunity_token_unknown
};

/*bgp_filter.h*/

enum as_filter_type
{
  AS_FILTER_DENY,
  AS_FILTER_PERMIT
};

/*bgp_filter.c*/

enum as_list_type
{
  ACCESS_TYPE_STRING,
  ACCESS_TYPE_NUMBER
};


/* List of AS filter list. */
struct as_list_list
{
  struct as_list *head;
  struct as_list *tail;
};

/* AS path filter master. */
struct as_list_master
{
  /* List of access_list which name is number. */
  struct as_list_list num;

  /* List of access_list which name is string. */
  struct as_list_list str;

  /* Hook function which is executed when new access_list is added. */
  void (BGP::*add_hook) ();

  /* Hook function which is executed when access_list is deleted. */
  void (BGP::*delete_hook) ();
};

/* Element of AS path filter. */
struct as_filter
{
  struct as_filter *next;
  struct as_filter *prev;

  enum as_filter_type type;

  regex_t *reg;
  char *reg_str;
};

/* AS path filter list. */
struct as_list
{
  char *name;

  enum as_list_type type;

  struct as_list *next;
  struct as_list *prev;

  struct as_filter *head;
  struct as_filter *tail;
};
/* ip as-path access-list 10 permit AS1. */

/*bgp_open.h*/

/* MP Capability information. */
struct capability_mp
{
  u_int16_t afi;
  u_char reserved;
  u_char safi;
};

/* BGP open message capability. */
struct capability
{
  u_char code;
  u_char length;
  struct capability_mp mpc;
};

/* Multiprotocol Extensions capabilities. */
#define CAPABILITY_CODE_MP              1
#define CAPABILITY_CODE_MP_LEN          4

/* Route refresh capabilities. */
#define CAPABILITY_CODE_REFRESH         2
#define CAPABILITY_CODE_REFRESH_OLD   128
#define CAPABILITY_CODE_REFRESH_LEN     0

/* Cooperative Route Filtering Capability.  */
#define CAPABILITY_CODE_ORF             3 
#define CAPABILITY_CODE_ORF_OLD       130

/* ORF Type.  */
#define ORF_TYPE_PREFIX                64 
#define ORF_TYPE_PREFIX_OLD           128

/* ORF Mode.  */
#define ORF_MODE_RECEIVE                1 
#define ORF_MODE_SEND                   2 
#define ORF_MODE_BOTH                   3 

/* Dynamic capability.  */
#define CAPABILITY_CODE_DYNAMIC        66
#define CAPABILITY_CODE_DYNAMIC_LEN     0

/* Capability Message Action.  */
#define CAPABILITY_ACTION_SET           0
#define CAPABILITY_ACTION_UNSET         1

/********prefix.h**********/

/* IPv4 and IPv6 unified prefix structure. */
struct prefix
{
  u_char family;
  u_char prefixlen;
  union 
  {
    u_char prefix;
    struct in_addr prefix4;
#ifdef HAVE_IPV6
    struct in6_addr prefix6;
#endif /* HAVE_IPV6 */
    struct 
    {
      struct in_addr id;
      struct in_addr adv_router;
    } lp;
    u_char val[8];
  } u __attribute__ ((aligned (8)));
};

/* IPv4 prefix structure. */
struct prefix_ipv4
{
  u_char family;
  u_char prefixlen;
  struct in_addr prefix __attribute__ ((aligned (8)));
};

/* IPv6 prefix structure. */
#ifdef HAVE_IPV6
struct prefix_ipv6
{
  u_char family;
  u_char prefixlen;
  struct in6_addr prefix __attribute__ ((aligned (8)));
};
#endif /* HAVE_IPV6 */

struct prefix_ls
{
  u_char family;
  u_char prefixlen;
  struct in_addr id __attribute__ ((aligned (8)));
  struct in_addr adv_router;
};

/* Prefix for routing distinguisher. */
struct prefix_rd
{
  u_char family;
  u_char prefixlen;
  u_char val[8] __attribute__ ((aligned (8)));
};

#ifndef INET_ADDRSTRLEN
#define INET_ADDRSTRLEN 16
#endif /* INET_ADDRSTRLEN */

#ifndef INET6_ADDRSTRLEN
#define INET6_ADDRSTRLEN 46
#endif /* INET6_ADDRSTRLEN */

#ifndef INET6_BUFSIZ
#define INET6_BUFSIZ 51
#endif /* INET6_BUFSIZ */

/* Max bit/byte length of IPv4 address. */
#define IPV4_MAX_BYTELEN    4
#define IPV4_MAX_BITLEN    32
#define IPV4_MAX_PREFIXLEN 32
#define IPV4_ADDR_CMP(D,S)   memcmp ((D), (S), IPV4_MAX_BYTELEN)
#define IPV4_ADDR_SAME(D,S)  (memcmp ((D), (S), IPV4_MAX_BYTELEN) == 0)
#define IPV4_ADDR_COPY(D,S)  memcpy ((D), (S), IPV4_MAX_BYTELEN)

#define IPV4_NET0(a)    ((((u_int32_t) (a)) & 0xff000000) == 0x00000000)
#define IPV4_NET127(a)  ((((u_int32_t) (a)) & 0xff000000) == 0x7f000000)

/* Max bit/byte length of IPv6 address. */
#define IPV6_MAX_BYTELEN    16
#define IPV6_MAX_BITLEN    128
#define IPV6_MAX_PREFIXLEN 128
#define IPV6_ADDR_CMP(D,S)   memcmp ((D), (S), IPV6_MAX_BYTELEN)
#define IPV6_ADDR_SAME(D,S)  (memcmp ((D), (S), IPV6_MAX_BYTELEN) == 0)
#define IPV6_ADDR_COPY(D,S)  memcpy ((D), (S), IPV6_MAX_BYTELEN)

/* Count prefix size from mask length */
#define PSIZE(a) (((a) + 7) / (8))

/* Prefix's family member. */
#define PREFIX_FAMILY(p)  ((p)->family)

/**********bgp_table.h***********/

struct bgp_table
{
  struct bgp_node *top;
};

struct bgp_node
{
  struct prefix p;

  struct bgp_table *table;
  struct bgp_node *parent;
  struct bgp_node *link[2];
#define l_left   link[0]
#define l_right  link[1]

  unsigned int lock;

  void *info;

  struct bgp_adj_out *adj_out;

  struct bgp_adj_in *adj_in;

  void *aggregate;

  struct bgp_node *prn;
};

/********bgp_vty.c**********/

/* BGP clear sort. */
enum clear_sort
  {
    clear_all,
    clear_peer,
    clear_group,
    clear_external,
    clear_as
  };


/* Show BGP peer's information. */
enum show_type
  {
    show_all,
    show_peer
  };


/**********bgp_damp.h************/
/* Structure maintained on a per-route basis. */
struct bgp_damp_info
{
  /* Doubly linked list.  This information must be linked to
     reuse_list or no_reuse_list.  */
  struct bgp_damp_info *next;
  struct bgp_damp_info *prev;

  /* Figure-of-merit.  */
  unsigned int penalty;

  /* Number of flapping.  */
  int flap;
	
  /* First flap time  */
  time_t start_time;
 
  /* Last time penalty was updated.  */
  Time_t t_updated;

  /* Time of route start to be suppressed.  */
  Time_t suppress_time;

  /* Back reference to bgp_info. */
  struct bgp_info *binfo;

  /* Back reference to bgp_node. */
  struct bgp_node *rn;

  /* Current index in the reuse_list. */
  int index;

  /* Last time message type. */
  u_char lastrecord;

#define BGP_RECORD_UPDATE	1
#define BGP_RECORD_WITHDRAW	2

  afi_t afi;
  safi_t safi;
};

/* Specified parameter set configuration. */
struct bgp_damp_config
{
  /* Value over which routes suppressed.  */
  unsigned int suppress_value;

  /* Value below which suppressed routes reused.  */
  unsigned int reuse_limit;    

  /* Max time a route can be suppressed.  */
  unsigned int max_suppress_time;      

  /* Time during which accumulated penalty reduces by half.  */
  unsigned int half_life; 

  /* Non-configurable parameters but fixed at implementation time.
   * To change this values, init_bgp_damp() should be modified.
   */
  int tmax;		  /* Max time previous instability retained */
  int reuse_list_size;		/* Number of reuse lists */
  int reuse_index_size;		/* Size of reuse index array */

  /* Non-configurable parameters.  Most of these are calculated from
   * the configurable parameters above.
   */
  unsigned int ceiling;		/* Max value a penalty can attain */
  int decay_rate_per_tick;	/* Calculated from half-life */
  int decay_array_size;		/* Calculated using config parameters */
  double scale_factor;
  int reuse_scale_factor; 
         
  /* Decay array per-set based. */ 
  double *decay_array;	

  /* Reuse index array per-set based. */ 
  int *reuse_index;

  /* Reuse list array per-set based. */  
  struct bgp_damp_info **reuse_list;
  int reuse_offset;
        
  /* All dampening information which is not on reuse list.  */
  struct bgp_damp_info *no_reuse_list;

  /* Reuse timer thread per-set base. */
  struct thread* t_reuse;
};

#define BGP_DAMP_NONE           0
#define BGP_DAMP_USED		1
#define BGP_DAMP_SUPPRESSED	2

/* Time granularity for reuse lists */
#define DELTA_REUSE	          10

/* Time granularity for decay arrays */
#define DELTA_T 	           5

#define DEFAULT_PENALTY         1000

#define DEFAULT_HALF_LIFE         15
#define DEFAULT_REUSE 	       	 750
#define DEFAULT_SUPPRESS 	2000

#define REUSE_LIST_SIZE          256
#define REUSE_ARRAY_SIZE        1024


/* Utility macro to add and delete BGP dampening information to no
   used list.  */
#define BGP_DAMP_LIST_ADD(N,A)  BGP_INFO_ADD(N,A,no_reuse_list)
#define BGP_DAMP_LIST_DEL(N,A)  BGP_INFO_DEL(N,A,no_reuse_list)

#define BGP_UPTIME_LEN 25


/**********bgp_debug.h***********/
/* sort of packet direction */
#define DUMP_ON        1
#define DUMP_SEND      2
#define DUMP_RECV      4

/* for dump_update */
#define DUMP_WITHDRAW  8
#define DUMP_NLRI     16

/* dump detail */
#define DUMP_DETAIL   32


#define	NLRI	 1
#define	WITHDRAW 2
#define	NO_OPT	 3
#define	SEND	 4
#define	RECV	 5
#define	DETAIL	 6

#define BGP_DEBUG_FSM                 0x01
#define BGP_DEBUG_EVENTS              0x01
#define BGP_DEBUG_PACKET              0x01
#define BGP_DEBUG_FILTER              0x01
#define BGP_DEBUG_KEEPALIVE           0x01
#define BGP_DEBUG_UPDATE_IN           0x01
#define BGP_DEBUG_UPDATE_OUT          0x02
#define BGP_DEBUG_NORMAL              0x01

#define BGP_DEBUG_PACKET_SEND         0x01
#define BGP_DEBUG_PACKET_SEND_DETAIL  0x02

#define BGP_DEBUG_PACKET_RECV         0x01
#define BGP_DEBUG_PACKET_RECV_DETAIL  0x02

#define CONF_DEBUG_ON(a, b) (conf_bgp_debug_ ## a |= (BGP_DEBUG_ ## b))
#define CONF_DEBUG_OFF(a, b)    (conf_bgp_debug_ ## a &= ~(BGP_DEBUG_ ## b))

#define TERM_DEBUG_ON(a, b) (term_bgp_debug_ ## a |= (BGP_DEBUG_ ## b))
#define TERM_DEBUG_OFF(a, b)    (term_bgp_debug_ ## a &= ~(BGP_DEBUG_ ## b))

#define DEBUG_ON(a, b) \
    do { \
    CONF_DEBUG_ON(a, b); \
    TERM_DEBUG_ON(a, b); \
    } while (0)
#define DEBUG_OFF(a, b) \
    do { \
    CONF_DEBUG_OFF(a, b); \
    TERM_DEBUG_OFF(a, b); \
    } while (0)

#define BGP_DEBUG(a, b)     (term_bgp_debug_ ## a & BGP_DEBUG_ ## b)
#define CONF_BGP_DEBUG(a, b)    (conf_bgp_debug_ ## a & BGP_DEBUG_ ## b)


/*****bgp_dump.h******/

#define MSG_PROTOCOL_BGP4MP  16
/* subtype value */
#define BGP4MP_STATE_CHANGE   0
#define BGP4MP_MESSAGE        1
#define BGP4MP_ENTRY          2
#define BGP4MP_SNAPSHOT       3

#define BGP_DUMP_HEADER_SIZE 12


/*****bgp_dump.c*******/

enum bgp_dump_type
{
  BGP_DUMP_ALL,
  BGP_DUMP_UPDATES,
  BGP_DUMP_ROUTES
};

enum MRT_MSG_TYPES {
   MSG_NULL,
   MSG_START,                   /* sender is starting up */
   MSG_DIE,                     /* receiver should shut down */
   MSG_I_AM_DEAD,               /* sender is shutting down */
   MSG_PEER_DOWN,               /* sender's peer is down */
   MSG_PROTOCOL_BGP,            /* msg is a BGP packet */
   MSG_PROTOCOL_RIP,            /* msg is a RIP packet */
   MSG_PROTOCOL_IDRP,           /* msg is an IDRP packet */
   MSG_PROTOCOL_RIPNG,          /* msg is a RIPNG packet */
   MSG_PROTOCOL_BGP4PLUS,       /* msg is a BGP4+ packet */
   MSG_PROTOCOL_BGP4PLUS_01,    /* msg is a BGP4+ (draft 01) packet */
   MSG_PROTOCOL_OSPF,           /* msg is an OSPF packet */
   MSG_TABLE_DUMP               /* routing table dump */
};

struct bgp_dump
{
  enum bgp_dump_type type;

  char *filename;

  FILE *fp;

  unsigned int interval;

  char *interval_str;

  struct thread *t_interval;
};


/******bgp_route.c******/
#define BGP_SHOW_HEADER "   Network          Next Hop            Metric LocPrf Weight Path%s"
#define BGP_SHOW_DAMP_HEADER "   Network          From             Reuse    Path%s"
#define BGP_SHOW_FLAP_HEADER "   Network          From            Flaps Duration Reuse    Path%s"

enum bgp_show_type
{
  bgp_show_type_normal,
  bgp_show_type_regexp,
  bgp_show_type_prefix_list,
  bgp_show_type_filter_list,
  bgp_show_type_route_map,
  bgp_show_type_neighbor,
  bgp_show_type_cidr_only,
  bgp_show_type_prefix_longer,
  bgp_show_type_community_all,
  bgp_show_type_community,
  bgp_show_type_community_exact,
  bgp_show_type_community_list,
  bgp_show_type_community_list_exact,
  bgp_show_type_flap_statistics,
  bgp_show_type_flap_address,
  bgp_show_type_flap_prefix,
  bgp_show_type_flap_cidr_only,
  bgp_show_type_flap_regexp,
  bgp_show_type_flap_filter_list,
  bgp_show_type_flap_prefix_list,
  bgp_show_type_flap_prefix_longer,
  bgp_show_type_flap_route_map,
  bgp_show_type_flap_neighbor,
  bgp_show_type_dampend_paths,
  bgp_show_type_damp_neighbor
};

/*****bgp_route.h*****/

struct bgp_info
{
  /* For linked list. */
  struct bgp_info *next;
  struct bgp_info *prev;

  /* BGP route type.  This can be static, RIP, OSPF, BGP etc.  */
  u_char type;

  /* When above type is BGP.  This sub type specify BGP sub type
     information.  */
  u_char sub_type;
#define BGP_ROUTE_NORMAL       0
#define BGP_ROUTE_STATIC       1
#define BGP_ROUTE_AGGREGATE    2
#define BGP_ROUTE_REDISTRIBUTE 3 

  /* BGP information status.  */
  u_char flags;
#define BGP_INFO_IGP_CHANGED    (1 << 0)
#define BGP_INFO_DAMPED         (1 << 1)
#define BGP_INFO_HISTORY        (1 << 2)
#define BGP_INFO_SELECTED       (1 << 3)
#define BGP_INFO_VALID          (1 << 4)
#define BGP_INFO_ATTR_CHANGED   (1 << 5)
#define BGP_INFO_DMED_CHECK     (1 << 6)
#define BGP_INFO_DMED_SELECTED  (1 << 7)

  /* Peer structure.  */
  struct peer *peer;

  /* Attribute structure.  */
  struct attr *attr;

  /* This route is suppressed with aggregation.  */
  int suppress;
  
  /* Nexthop reachability check.  */
  u_int32_t igpmetric;

  /* Uptime.  */
  time_t uptime;

  /* Pointer to dampening structure.  */
  struct bgp_damp_info *damp_info;

  /* MPLS label.  */
  u_char tag[3];
};

/* BGP static route configuration. */
struct bgp_static
{
  /* Backdoor configuration.  */
  int backdoor;

  /* Import check status.  */
  u_char valid;

  /* IGP metric. */
  u_int32_t igpmetric;

  /* IGP nexthop. */
  struct in_addr igpnexthop;

  /* BGP redistribute route-map.  */
  struct
  {
    char *name;
    struct route_map *map;
  } rmap;

  /* MPLS label.  */
  u_char tag[3];
};

#define DISTRIBUTE_IN_NAME(F)   ((F)->dlist[FILTER_IN].name)
#define DISTRIBUTE_IN(F)        ((F)->dlist[FILTER_IN].alist)
#define DISTRIBUTE_OUT_NAME(F)  ((F)->dlist[FILTER_OUT].name)
#define DISTRIBUTE_OUT(F)       ((F)->dlist[FILTER_OUT].alist)

#define PREFIX_LIST_IN_NAME(F)  ((F)->plist[FILTER_IN].name)
#define PREFIX_LIST_IN(F)       ((F)->plist[FILTER_IN].plist)
#define PREFIX_LIST_OUT_NAME(F) ((F)->plist[FILTER_OUT].name)
#define PREFIX_LIST_OUT(F)      ((F)->plist[FILTER_OUT].plist)

#define FILTER_LIST_IN_NAME(F)  ((F)->aslist[FILTER_IN].name)
#define FILTER_LIST_IN(F)       ((F)->aslist[FILTER_IN].aslist)
#define FILTER_LIST_OUT_NAME(F) ((F)->aslist[FILTER_OUT].name)
#define FILTER_LIST_OUT(F)      ((F)->aslist[FILTER_OUT].aslist)

#define ROUTE_MAP_IN_NAME(F)    ((F)->map[FILTER_IN].name)
#define ROUTE_MAP_IN(F)         ((F)->map[FILTER_IN].map)
#define ROUTE_MAP_OUT_NAME(F)   ((F)->map[FILTER_OUT].name)
#define ROUTE_MAP_OUT(F)        ((F)->map[FILTER_OUT].map)

#define UNSUPPRESS_MAP_NAME(F)  ((F)->usmap.name)
#define UNSUPPRESS_MAP(F)       ((F)->usmap.map)

/*bgp_fsm.h*/

/* Macro for BGP read, write and timer thread.  */
#define BGP_READ_ON(T,F,V)   THREAD_READ_ON(master,T,F,peer,V)
#define BGP_READ_OFF(X)      THREAD_READ_OFF(X)

/* Macro for BGP write add */
#define BGP_WRITE_ON(T,F) \
do { \
  if (!(T)) \
    (T) = thread_add_ready (master, (F), peer); \
} while (0)


#define BGP_WRITE_OFF(X)     THREAD_WRITE_OFF(X)

#define BGP_TIMER_ON(T,F,V)  THREAD_TIMER_ON(master,T,F,peer,V)
#define BGP_TIMER_OFF(X)     THREAD_TIMER_OFF(X)

#define BGP_EVENT_ADD(P,E) \
    thread_add_event (master, &BGP::bgp_event, (P), (E))

#define BGP_EVENT_DELETE(P) \
    thread_cancel_event (master, (P))

struct fsm_struct {
  int (BGP::*func) (struct peer*);
  int next_state;
};


/******bgp_packet.h******/
#define BGP_NLRI_LENGTH       1
#define BGP_TOTAL_ATTR_LEN    2
#define BGP_UNFEASIBLE_LEN    2
#define BGP_WRITE_PACKET_MAX 10

/* When to refresh */
#define REFRESH_IMMEDIATE 1
#define REFRESH_DEFER     2 

/* ORF Common part flag */
#define ORF_COMMON_PART_ADD        0x00 
#define ORF_COMMON_PART_REMOVE     0x80 
#define ORF_COMMON_PART_REMOVE_ALL 0xC0 
#define ORF_COMMON_PART_PERMIT     0x00 
#define ORF_COMMON_PART_DENY       0x20 


/******Zebra/lib**********/

/*****buffer.h******/
/* Buffer master. */
struct buffer
{
  /* Data list. */
  struct buffer_data *head;
  struct buffer_data *tail;

  /* Current allocated data. */
  unsigned long alloc;

  /* Total length of buffer. */
  unsigned long size;

  /* For allocation. */
  struct buffer_data *unused_head;
  struct buffer_data *unused_tail;

  /* Current total length of this buffer. */
  unsigned long length;
};

/* Data container. */
struct buffer_data
{
  struct buffer *parent;
  struct buffer_data *next;
  struct buffer_data *prev;

  /* Acctual data stream. */
  unsigned char *data;

  /* Current pointer. */
  unsigned long cp;

  /* Start pointer. */
  unsigned long sp;
};

/*command.c*/

enum match_type 
{
  no_match,
  extend_match,
  ipv4_prefix_match,
  ipv4_match,
  ipv6_prefix_match,
  ipv6_match,
  range_match,
  vararg_match,
  partly_match,
  exact_match 
};

/***** vty.h ********/

#define VTY_BUFSIZ 512
#define VTY_MAXHIST 20

enum Type
{
  VTY_TERM,
  VTY_FILE,
  VTY_SHELL,
  VTY_SHELL_SERV
};

enum Status
{ 
  VTY_NORMAL,
  VTY_CLOSE,
  VTY_MORE,
  VTY_MORELINE,
  VTY_START,
  VTY_CONTINUE
};

/* VTY struct. */
struct vty 
{
  /* File descripter of this vty. */
  int fd;

  /* Is this vty connect to file or not */
  Type type;

  /* Node status of this vty */
  int node;

  /* What address is this vty comming from. */
  char *address;

  /* Privilege level of this vty. */
  int privilege;

  /* Failure count */
  int fail;

  /* Output buffer. */
  struct buffer *obuf;

  /* Command input buffer */
  char *buf;

  /* Command cursor point */
  int cp;

  /* Command length */
  int length;

  /* Command max length. */
  int max;

  /* Histry of command */
  char *hist[VTY_MAXHIST];

  /* History lookup current point */
  int hp;

  /* History insert end point */
  int hindex;

  /* For current referencing point of interface, route-map,
     access-list etc... */
  void *index;

  /* For multiple level index treatment such as key chain and key. */
  void *index_sub;

  /* For escape character. */
  unsigned char escape;

  /* Current vty status. */
  Status status;

  /* IAC handling */
  unsigned char iac;

  /* IAC SB handling */
  unsigned char iac_sb_in_progress;
  struct buffer *sb_buffer;

  /* Window width/height. */
  int width;
  int height;

  int scroll_one;

  /* Configure lines. */
  int lines;

  /* Current executing function pointer. */
  int (BGP::*func) (struct vty *, void *arg);

  /* Terminal monitor. */
  int monitor;

  /* In configure mode. */
  int config;

  /* Read and write thread. */
  struct thread *t_read;
  struct thread *t_write;

  /* Timeout seconds and thread. */
  unsigned long v_timeout;
  struct thread *t_timeout;

  /* Thread output function. */
  struct thread *t_output;

  /* Output data pointer. */
  int (BGP::*output_func) (struct vty *, int);
  void (BGP::*output_clean) (struct vty *);
  void *output_rn;
  unsigned long output_count;
  int output_type;
  void *output_arg;
};

/* Integrated configuration file. */
#define INTEGRATE_DEFAULT_CONFIG "Zebra.conf"

/* Small macro to determine newline is newline only or linefeed needed. */
#define VTY_NEWLINE  ("\n")

/* Default time out value */
#define VTY_TIMEOUT_DEFAULT 600

/* Vty read buffer size. */
#define VTY_READ_BUFSIZ 512

/* Directory separator. */
#ifndef DIRECTORY_SEP
#define DIRECTORY_SEP '/'
#endif /* DIRECTORY_SEP */

#ifndef IS_DIRECTORY_SEP
#define IS_DIRECTORY_SEP(c) ((c) == DIRECTORY_SEP)
#endif

/* GCC have printf type attribute check.  */
#ifdef __GNUC__
#define PRINTF_ATTRIBUTE(a,b) __attribute__ ((__format__ (__printf__, a, b)))
#else
#define PRINTF_ATTRIBUTE(a,b)
#endif /* __GNUC__ */

/* Utility macro to convert VTY argument to unsigned integer.  */
#define VTY_GET_INTEGER(NAME,V,STR)                              \
{                                                                \
  char *endptr = NULL;                                           \
  (V) = strtoul ((STR), &endptr, 10);                            \
  if (*endptr != '\0')			                         \
    {                                                            \
      vty_out (vty, "%% Invalid %s value%s", NAME, VTY_NEWLINE); \
      return CMD_WARNING;                                        \
    }                                                            \
}

#define VTY_GET_INTEGER_RANGE(NAME,V,STR,MIN,MAX)                \
{                                                                \
  char *endptr = NULL;                                           \
  (V) = strtoul ((STR), &endptr, 10);                            \
  if (*endptr != '\0'                                            \
      || (V) < (MIN) || (V) > (MAX))                             \
    {                                                            \
      vty_out (vty, "%% Invalid %s value%s", NAME, VTY_NEWLINE); \
      return CMD_WARNING;                                        \
    }                                                            \
}


/*command.h*/

/* Host configuration variable */
struct host
{
  /* Host name of this router. */
  char *name;

  /* Password for vty interface. */
  char *password;
  char *password_encrypt;

  /* Enable password */
  char *enable;
  char *enable_encrypt;

  /* System wide terminal lines. */
  int lines;

  /* Log filename. */
  char *logfile;

  /* Log stdout. */
  u_char log_stdout;

  /* Log syslog. */
  u_char log_syslog;
  
  /* config file name of this host */
  char *config;

  /* Flags for services */
  int advanced;
  int encrypt;

  /* Banner configuration. */
  char *motd;
};

/* There are some command levels which called from command node. */
enum node_type 
{
  AUTH_NODE,			/* Authentication mode of vty interface. */
  VIEW_NODE,			/* View node. Default mode of vty interface. */
  AUTH_ENABLE_NODE,		/* Authentication mode for change enable. */
  ENABLE_NODE,			/* Enable node. */
  CONFIG_NODE,			/* Config node. Default mode of config file. */
  DEBUG_NODE,			/* Debug node. */
  AAA_NODE,			/* AAA node. */
  KEYCHAIN_NODE,		/* Key-chain node. */
  KEYCHAIN_KEY_NODE,		/* Key-chain key node. */
  INTERFACE_NODE,		/* Interface mode node. */
  ZEBRA_NODE,			/* zebra connection node. */
  TABLE_NODE,			/* rtm_table selection node. */
  RIP_NODE,			/* RIP protocol mode node. */ 
  RIPNG_NODE,			/* RIPng protocol mode node. */
  BGP_NODE,			/* BGP protocol mode which includes BGP4+ */
  BGP_VPNV4_NODE,		/* BGP MPLS-VPN PE exchange. */
  BGP_IPV4_NODE,		/* BGP IPv4 unicast address family.  */
  BGP_IPV4M_NODE,		/* BGP IPv4 multicast address family.  */
  BGP_IPV6_NODE,		/* BGP IPv6 address family */
  OSPF_NODE,			/* OSPF protocol mode */
  OSPF6_NODE,			/* OSPF protocol for IPv6 mode */
  MASC_NODE,			/* MASC for multicast.  */
  IRDP_NODE,			/* ICMP Router Discovery Protocol mode. */ 
  IP_NODE,			/* Static ip route node. */
  ACCESS_NODE,			/* Access list node. */
  PREFIX_NODE,			/* Prefix list node. */
  ACCESS_IPV6_NODE,		/* Access list node. */
  PREFIX_IPV6_NODE,		/* Prefix list node. */
  AS_LIST_NODE,			/* AS list node. */
  COMMUNITY_LIST_NODE,		/* Community list node. */
  RMAP_NODE,			/* Route map node. */
  SMUX_NODE,			/* SNMP configuration node. */
  DUMP_NODE,			/* Packet dump node. */
  FORWARDING_NODE,		/* IP forwarding node. */
  VTY_NODE			/* Vty node. */
};

/* Node which has some commands and prompt string and configuration
   function pointer . */
struct cmd_node 
{
  /* Node index. */
  enum node_type node;		

  /* Prompt character at vty interface. */
  char *prompt;			

  /* Is this node's configuration goes to vtysh ? */
  int vtysh;
  
  /* Node's configuration write function */
  int (BGP::*func) (struct vty *);

  /* Vector of this node's command list. */
  struct _vector* cmd_vector;	
};

/* Structure of command element. */
struct cmd_element 
{
  char *string;			/* Command specification by string. */
  int (BGP::*func) (struct cmd_element *, struct vty *, int, char **);
  char *doc;			/* Documentation of this command. */
  int daemon;                   /* Daemon to which this command belong. */
  struct _vector* strvec;		/* Pointing out each description vector. */
  unsigned int cmdsize;			/* Command index count. */
  char *config;			/* Configuration string */
  struct _vector* subconfig;		/* Sub configuration string */
};

/* Command description structure. */
struct desc
{
  char *cmd;			/* Command string. */
  char *str;			/* Command's description. */
};

/* Return value of the commands. */
#define CMD_SUCCESS              0
#define CMD_WARNING              1
#define CMD_ERR_NO_MATCH         2
#define CMD_ERR_AMBIGUOUS        3
#define CMD_ERR_INCOMPLETE       4
#define CMD_ERR_EXEED_ARGC_MAX   5
#define CMD_ERR_NOTHING_TODO     6
#define CMD_COMPLETE_FULL_MATCH  7
#define CMD_COMPLETE_MATCH       8
#define CMD_COMPLETE_LIST_MATCH  9
#define CMD_SUCCESS_DAEMON      10

/* Argc max counts. */
#define CMD_ARGC_MAX   25

/* Turn off these macros when uisng cpp with extract.pl */
#ifndef VTYSH_EXTRACT_PL  

/* DEFUN for vty command interafce. Little bit hacky ;-). */
#define DEFUN(funcname, cmdname, cmdstr, helpstr) \
  int BGP::funcname \
	(struct cmd_element * self, struct vty *vty, int argc, char **argv)


#define DEFUNST(funcname, cmdname, cmdstr, helpstr) \
  struct cmd_element BGP::cmdname = \
  { \
    cmdstr, \
    &BGP::funcname, \
    helpstr \
  };


/* DEFUN_NOSH for commands that vtysh should ignore */
#define DEFUN_NOSH(funcname, cmdname, cmdstr, helpstr) \
  DEFUN(funcname, cmdname, cmdstr, helpstr)\
  struct cmd_element BGP::cmdname = \
  { \
    cmdstr, \
    &BGP::funcname, \
    helpstr \
  }; \
  int funcname \
  (struct cmd_element *self, struct vty *vty, int argc, char **argv)

/* DEFSH for vtysh. */
#define DEFSH(daemon, cmdname, cmdstr, helpstr) \
  struct cmd_element cmdname = \
  { \
    cmdstr, \
    NULL, \
    helpstr, \
    daemon \
  }; \

/* DEFUN + DEFSH */
#define DEFUNSH(daemon, funcname, cmdname, cmdstr, helpstr) \
  int funcname (struct cmd_element *, struct vty *, int, char **); \
  struct cmd_element cmdname = \
  { \
    cmdstr, \
    funcname, \
    helpstr, \
    daemon \
  }; \
  int funcname \
  (struct cmd_element *self, struct vty *vty, int argc, char **argv)

/* ALIAS macro which define existing command's alias. */
#define ALIAS(funcname, cmdname, cmdstr, helpstr) \
  struct cmd_element BGP::cmdname = \
  { \
    cmdstr, \
    &BGP::funcname, \
    helpstr \
  };

#endif /* VTYSH_EXTRACT_PL */

/* Some macroes */
#define CMD_OPTION(S)   ((S[0]) == '[')
#define CMD_VARIABLE(S) (((S[0]) >= 'A' && (S[0]) <= 'Z') || ((S[0]) == '<'))
#define CMD_VARARG(S)   ((S[0]) == '.')
#define CMD_RANGE(S)	((S[0] == '<'))

#define CMD_IPV4(S)	   ((strcmp ((S), "A.B.C.D") == 0))
#define CMD_IPV4_PREFIX(S) ((strcmp ((S), "A.B.C.D/M") == 0))
#define CMD_IPV6(S)        ((strcmp ((S), "X:X::X:X") == 0))
#define CMD_IPV6_PREFIX(S) ((strcmp ((S), "X:X::X:X/M") == 0))

/* Common descriptions. */
#define SHOW_STR "Show running system information\n"
#define IP_STR "IP information\n"
#define IPV6_STR "IPv6 information\n"
#define NO_STR "Negate a command or set its defaults\n"
#define CLEAR_STR "Reset functions\n"
#define RIP_STR "RIP information\n"
#define BGP_STR "BGP information\n"
#define OSPF_STR "OSPF information\n"
#define NEIGHBOR_STR "Specify neighbor router\n"
#define DEBUG_STR "Debugging functions (see also 'undebug')\n"
#define UNDEBUG_STR "Disable debugging functions (see also 'debug')\n"
#define ROUTER_STR "Enable a routing process\n"
#define AS_STR "AS number\n"
#define MBGP_STR "MBGP information\n"
#define MATCH_STR "Match values from routing table\n"
#define SET_STR "Set values in destination routing protocol\n"
#define OUT_STR "Filter outgoing routing updates\n"
#define IN_STR  "Filter incoming routing updates\n"
#define V4NOTATION_STR "specify by IPv4 address notation(e.g. 0.0.0.0)\n"
#define OSPF6_NUMBER_STR "Specify by number\n"
#define INTERFACE_STR "Interface infomation\n"
#define IFNAME_STR "Interface name(e.g. ep0)\n"
#define IP6_STR "IPv6 Information\n"
#define OSPF6_STR "Open Shortest Path First (OSPF) for IPv6\n"
#define OSPF6_ROUTER_STR "Enable a routing process\n"
#define OSPF6_INSTANCE_STR "<1-65535> Instance ID\n"
#define SECONDS_STR "<1-65535> Seconds\n"
#define ROUTE_STR "Routing Table\n"
#define PREFIX_LIST_STR "Build a prefix list\n"
#define OSPF6_DUMP_TYPE_LIST \
"(neighbor|interface|area|lsa|zebra|config|dbex|spf|route|lsdb|redistribute|hook|asbr|prefix|abr)"

#define CONF_BACKUP_EXT ".sav"

/* IPv4 only machine should not accept IPv6 address for peer's IP
   address.  So we replace VTY command string like below. */
#ifdef HAVE_IPV6
#define NEIGHBOR_CMD       "neighbor (A.B.C.D|X:X::X:X) "
#define NO_NEIGHBOR_CMD    "no neighbor (A.B.C.D|X:X::X:X) "
#define NEIGHBOR_ADDR_STR  "Neighbor address\nIPv6 address\n"
#define NEIGHBOR_CMD2      "neighbor (A.B.C.D|X:X::X:X|WORD) "
#define NO_NEIGHBOR_CMD2   "no neighbor (A.B.C.D|X:X::X:X|WORD) "
#define NEIGHBOR_ADDR_STR2 "Neighbor address\nNeighbor IPv6 address\nNeighbor tag\n"
#else
#define NEIGHBOR_CMD       "neighbor A.B.C.D "
#define NO_NEIGHBOR_CMD    "no neighbor A.B.C.D "
#define NEIGHBOR_ADDR_STR  "Neighbor address\n"
#define NEIGHBOR_CMD2      "neighbor (A.B.C.D|WORD) "
#define NO_NEIGHBOR_CMD2   "no neighbor (A.B.C.D|WORD) "
#define NEIGHBOR_ADDR_STR2 "Neighbor address\nNeighbor tag\n"
#endif /* HAVE_IPV6 */

/*distribute.h*/
/* Disctirubte list types. */
enum distribute_type
{
  DISTRIBUTE_IN,
  DISTRIBUTE_OUT,
  DISTRIBUTE_MAX
};

struct distribute
{
  /* Name of the interface. */
  char *ifname;

  /* Filter name of `in' and `out' */
  char *list[DISTRIBUTE_MAX];

  /* prefix-list name of `in' and `out' */
  char *prefix[DISTRIBUTE_MAX];
};


/*filter.h*/

enum filter_type 
{
  FILTER_DENY,
  FILTER_PERMIT,
  FILTER_DYNAMIC
};

/*filter.c*/
/* List of access_list. */
struct access_list_list
{
  struct access_list *head;
  struct access_list *tail;
};

/* Master structure of access_list. */
struct access_master
{
  /* List of access_list which name is number. */
  struct access_list_list num;

  /* List of access_list which name is string. */
  struct access_list_list str;

  /* Hook function which is executed when new access_list is added. */
  void (BGP::*add_hook) (struct access_list*);

  /* Hook function which is executed when access_list is deleted. */
  void (BGP::*delete_hook) (struct access_list*);
};


/*enum access_type
{
  ACCESS_TYPE_STRING,
  ACCESS_TYPE_NUMBER
};*/ 


/* Access list */

struct access_list
{
  char *name;
  char *remark;

  struct access_master *master;

  as_list_type type;

  struct access_list *next;
  struct access_list *prev;

  struct filter *head;
  struct filter *tail;
};

/*hash.h*/


/* Default hash table size.  */ 
#define HASHTABSIZE     1024

struct hash_backet
{
  /* Linked list.  */
  struct hash_backet *next;

  /* Hash key. */
  unsigned int key;

  /* Data.  */
  void *data;
};

struct hash
{
  /* Hash backet. */
  struct hash_backet **index;

  /* Hash table size. */
  unsigned int size;

  /* Key make function. */
  unsigned int (BGP::*hash_key) (void*);

  /* Data compare function. */
  int (BGP::*hash_cmp) (void*,void*);

  /* Backet alloc. */
  unsigned long count;
};


/*linklist.h*/

#define nextnode(X) ((X) = (X)->next)
#define listhead(X) ((X)->head)
#define listcount(X) ((X)->count)
#define list_isempty(X) ((X)->head == NULL && (X)->tail == NULL)
#define getdata(X) ((X)->data)

struct listnode 
{
  struct listnode *next;
  struct listnode *prev;
  void *data;
};

struct llist 
{
  struct listnode *head;
  struct listnode *tail;
  unsigned int count;
  int (BGP::*cmp) (void *val1, void *val2);
  void (BGP::*del) (void *val);
};

/* List iteration macro. */
#define LIST_LOOP(L,V,N) \
  for ((N) = (L)->head; (N); (N) = (N)->next) \
    if (((V) = ((N)->data) != NULL)

/* List node add macro.  */
#define LISTNODE_ADD(L,N) \
  do { \
    (N)->prev = (L)->tail; \
    if ((L)->head == NULL) \
      (L)->head = (N); \
    else \
      (L)->tail->next = (N); \
    (L)->tail = (N); \
  } while (0)

/* List node delete macro.  */
#define LISTNODE_DELETE(L,N) \
  do { \
    if ((N)->prev) \
      (N)->prev->next = (N)->next; \
    else \
      (L)->head = (N)->next; \
    if ((N)->next) \
      (N)->next->prev = (N)->prev; \
    else \
      (L)->tail = (N)->prev; \
  } while (0)



/*log.h*/

#define ZLOG_NOLOG              0x00
#define ZLOG_FILE		0x01
#define ZLOG_SYSLOG		0x02
#define ZLOG_STDOUT             0x04
#define ZLOG_STDERR             0x08

#define ZLOG_NOLOG_INDEX        0
#define ZLOG_FILE_INDEX         1
#define ZLOG_SYSLOG_INDEX       2
#define ZLOG_STDOUT_INDEX       3
#define ZLOG_STDERR_INDEX       4
#define ZLOG_MAX_INDEX          5

typedef enum 
{
  ZLOG_NONE,
  ZLOG_DEFAULT,
  ZLOG_ZEBRA,
  ZLOG_RIP,
  ZLOG_BGP,
  ZLOG_OSPF,
  ZLOG_RIPNG,  
  ZLOG_OSPF6,
  ZLOG_MASC
} zlog_proto_t;

struct zlog 
{
  const char *ident;
  zlog_proto_t protocol;
  int flags;
  FILE *fp;
  char *filename;
  int syslog;
  int stat;
  int connected;
  int maskpri;		/* as per syslog setlogmask */
  int priority;		/* as per syslog priority */
  int facility;		/* as per syslog facility */
  int record_priority;
};

/* Message structure. */
struct message
{
  int key;
  char *str;
};

/* For hackey massage lookup and check */
#define LOOKUP(x, y) mes_lookup(x, x ## _max, y)


/**memory.h**/
/* #define MEMORY_LOG */

/* For tagging memory, below is the type of the memory. */
enum
{
  MTYPE_TMP = 1,
  MTYPE_STRVEC,
  MTYPE_VECTOR,
  MTYPE_VECTOR_INDEX,
  MTYPE_LINK_LIST,
  MTYPE_LINK_NODE,
  MTYPE_THREAD,
  MTYPE_THREAD_MASTER,
  MTYPE_VTY,
  MTYPE_VTY_HIST,
  MTYPE_VTY_OUT_BUF,
  MTYPE_IF,
  MTYPE_CONNECTED,
  MTYPE_AS_SEG,
  MTYPE_AS_STR,
  MTYPE_AS_PATH,
  MTYPE_CLUSTER,
  MTYPE_CLUSTER_VAL,
  MTYPE_ATTR,
  MTYPE_TRANSIT,
  MTYPE_TRANSIT_VAL,
  MTYPE_BUFFER,
  MTYPE_BUFFER_DATA,
  MTYPE_STREAM,
  MTYPE_STREAM_DATA,
  MTYPE_STREAM_FIFO,
  MTYPE_PREFIX,
  MTYPE_PREFIX_IPV4,
  MTYPE_PREFIX_IPV6,
  MTYPE_HASH,
  MTYPE_HASH_INDEX,
  MTYPE_HASH_BACKET,
  MTYPE_RIPNG_ROUTE,
  MTYPE_RIPNG_AGGREGATE,
  MTYPE_ROUTE_TABLE,
  MTYPE_ROUTE_NODE,
  MTYPE_ACCESS_LIST,
  MTYPE_ACCESS_LIST_STR,
  MTYPE_ACCESS_FILTER,
  MTYPE_PREFIX_LIST,
  MTYPE_PREFIX_LIST_STR,
  MTYPE_PREFIX_LIST_ENTRY,
  MTYPE_ROUTE_MAP,
  MTYPE_ROUTE_MAP_NAME,
  MTYPE_ROUTE_MAP_INDEX,
  MTYPE_ROUTE_MAP_RULE,
  MTYPE_ROUTE_MAP_RULE_STR,
  MTYPE_ROUTE_MAP_COMPILED,

  MTYPE_RIB,
  MTYPE_DISTRIBUTE,
  MTYPE_ZLOG,
  MTYPE_ZCLIENT,
  MTYPE_NEXTHOP,
  MTYPE_RTADV_PREFIX,
  MTYPE_IF_RMAP,
  MTYPE_SOCKUNION,
  MTYPE_STATIC_IPV4,
  MTYPE_STATIC_IPV6,

  MTYPE_DESC,
  MTYPE_OSPF_TOP,
  MTYPE_OSPF_AREA,
  MTYPE_OSPF_AREA_RANGE,
  MTYPE_OSPF_NETWORK,
  MTYPE_OSPF_NEIGHBOR_STATIC,
  MTYPE_OSPF_IF,
  MTYPE_OSPF_NEIGHBOR,
  MTYPE_OSPF_ROUTE,
  MTYPE_OSPF_TMP,
  MTYPE_OSPF_LSA,
  MTYPE_OSPF_LSA_DATA,
  MTYPE_OSPF_LSDB,
  MTYPE_OSPF_PACKET,
  MTYPE_OSPF_FIFO,
  MTYPE_OSPF_VERTEX,
  MTYPE_OSPF_NEXTHOP,
  MTYPE_OSPF_PATH,
  MTYPE_OSPF_VL_DATA,
  MTYPE_OSPF_CRYPT_KEY,
  MTYPE_OSPF_EXTERNAL_INFO,
  MTYPE_OSPF_MESSAGE,
  MTYPE_OSPF_DISTANCE,
  MTYPE_OSPF_IF_INFO,
  MTYPE_OSPF_IF_PARAMS,

  MTYPE_OSPF6_TOP,
  MTYPE_OSPF6_AREA,
  MTYPE_OSPF6_IF,
  MTYPE_OSPF6_NEIGHBOR,
  MTYPE_OSPF6_ROUTE,
  MTYPE_OSPF6_PREFIX,
  MTYPE_OSPF6_MESSAGE,
  MTYPE_OSPF6_LSA,
  MTYPE_OSPF6_LSA_SUMMARY,
  MTYPE_OSPF6_LSDB,
  MTYPE_OSPF6_VERTEX,
  MTYPE_OSPF6_SPFTREE,
  MTYPE_OSPF6_NEXTHOP,
  MTYPE_OSPF6_EXTERNAL_INFO,
  MTYPE_OSPF6_OTHER,

  MTYPE_BGP,
  MTYPE_PEER_CONF,
  MTYPE_BGP_PEER,
  MTYPE_PEER_GROUP,
  MTYPE_PEER_DESC,
  MTYPE_PEER_UPDATE_SOURCE,
  MTYPE_BGP_STATIC,
  MTYPE_BGP_AGGREGATE,
  MTYPE_BGP_CONFED_LIST,
  MTYPE_BGP_NEXTHOP_CACHE,
  MTYPE_BGP_DAMP_INFO,
  MTYPE_BGP_DAMP_ARRAY,
  MTYPE_BGP_ANNOUNCE,
  MTYPE_BGP_ATTR_QUEUE,
  MTYPE_BGP_ROUTE_QUEUE,
  MTYPE_BGP_DISTANCE,
  MTYPE_BGP_ROUTE,
  MTYPE_BGP_TABLE,
  MTYPE_BGP_NODE,
  MTYPE_BGP_MRAI_INFO,
  MTYPE_BGP_ROUTEADV_LIST,
  MTYPE_BGP_ADVERTISE_ATTR,
  MTYPE_BGP_ADVERTISE,
  MTYPE_BGP_ADJ_IN,
  MTYPE_BGP_ADJ_OUT,
  MTYPE_BGP_REGEXP,
  MTYPE_AS_FILTER,
  MTYPE_AS_FILTER_STR,
  MTYPE_AS_LIST,

  MTYPE_COMMUNITY,
  MTYPE_COMMUNITY_VAL,
  MTYPE_COMMUNITY_STR,

  MTYPE_ECOMMUNITY,
  MTYPE_ECOMMUNITY_VAL,
  MTYPE_ECOMMUNITY_STR,

  /* community-list and extcommunity-list.  */
  MTYPE_COMMUNITY_LIST_HANDLER,
  MTYPE_COMMUNITY_LIST,
  MTYPE_COMMUNITY_LIST_NAME,
  MTYPE_COMMUNITY_LIST_ENTRY,
  MTYPE_COMMUNITY_LIST_CONFIG,

  MTYPE_RIP,
  MTYPE_RIP_INTERFACE,
  MTYPE_RIP_DISTANCE,
  MTYPE_RIP_OFFSET_LIST,
  MTYPE_RIP_INFO,
  MTYPE_RIP_PEER,
  MTYPE_KEYCHAIN,
  MTYPE_KEY,

  MTYPE_VTYSH_CONFIG,
  MTYPE_VTYSH_CONFIG_LINE,

  MTYPE_VRF,
  MTYPE_VRF_NAME,

  MTYPE_MAX
};

#ifdef MEMORY_LOG
#define XMALLOC(mtype, size) \
  mtype_zmalloc (__FILE__, __LINE__, (mtype), (size))
#define XCALLOC(mtype, size) \
  mtype_zcalloc (__FILE__, __LINE__, (mtype), (size))
#define XREALLOC(mtype, ptr, size)  \
  mtype_zrealloc (__FILE__, __LINE__, (mtype), (ptr), (size))
#define XFREE(mtype, ptr) \
  mtype_zfree (__FILE__, __LINE__, (mtype), (ptr))
#define XSTRDUP(mtype, str) \
  mtype_zstrdup (__FILE__, __LINE__, (mtype), (str))
#else
#define XMALLOC(mtype, size)       zmalloc ((mtype), (size))
#define XCALLOC(mtype, size)       zcalloc ((mtype), (size))
#define XREALLOC(mtype, ptr, size) zrealloc ((mtype), (ptr), (size))
#define XFREE(mtype, ptr)          zfree ((mtype), (ptr))
#define XSTRDUP(mtype, str)        zstrdup ((mtype), (str))
#endif /* MEMORY_LOG */

/**memory.c**/

/* For pretty printng of memory allocate information. */
struct memory_list
{
  int index;
  char *format;
};

/*plist.c*/

/* List of struct prefix_list. */
struct prefix_list_list
{
  struct prefix_list *head;
  struct prefix_list *tail;
};


/* Master structure of prefix_list. */
struct prefix_master
{
  /* List of prefix_list which name is number. */
  struct prefix_list_list num;

  /* List of prefix_list which name is string. */
  struct prefix_list_list str;

  /* Whether sequential number is used. */
  int seqnum;

  /* The latest update. */
  struct prefix_list *recent;

  /* Hook function which is executed when new prefix_list is added. */
  void (BGP::*add_hook) ();

  /* Hook function which is executed when prefix_list is deleted. */
  void (BGP::*delete_hook) ();
};


enum display_type
{
  normal_display,
  summary_display,
  detail_display,
  sequential_display,
  longer_display,
  first_match_display
};

/*plist.h*/

#define AFI_ORF_PREFIX 65535

enum prefix_list_type 
{
  PREFIX_DENY,
  PREFIX_PERMIT,
};

enum prefix_name_type
{
  PREFIX_TYPE_STRING,
  PREFIX_TYPE_NUMBER
};

struct prefix_list
{
  char *name;
  char *desc;

  struct prefix_master *master;

  enum prefix_name_type type;

  int count;
  int rangecount;

  struct prefix_list_entry *head;
  struct prefix_list_entry *tail;

  struct prefix_list *next;
  struct prefix_list *prev;
};

struct orf_prefix
{
  u_int32_t seq;
  u_char ge;
  u_char le;
  struct prefix p;
};


/*routemap.h*/

/* Route map's type. */
enum route_map_type
{
  RMAP_PERMIT,
  RMAP_DENY,
  RMAP_ANY
};

typedef enum 
{
  RMAP_MATCH,
  RMAP_DENYMATCH,
  RMAP_NOMATCH,
  RMAP_ERROR,
  RMAP_OKAY
} route_map_result_t;

typedef enum
{
  RMAP_RIP,
  RMAP_RIPNG,
  RMAP_OSPF,
  RMAP_OSPF6,
  RMAP_BGP
} route_map_object_t;

typedef enum
{
  RMAP_EXIT,
  RMAP_GOTO,
  RMAP_NEXT
} route_map_end_t;

typedef enum
{
  RMAP_EVENT_SET_ADDED,
  RMAP_EVENT_SET_DELETED,
  RMAP_EVENT_SET_REPLACED,
  RMAP_EVENT_MATCH_ADDED,
  RMAP_EVENT_MATCH_DELETED,
  RMAP_EVENT_MATCH_REPLACED,
  RMAP_EVENT_INDEX_ADDED,
  RMAP_EVENT_INDEX_DELETED
} route_map_event_t;


/* Route map rule. This rule has both `match' rule and `set' rule. */
struct route_map_rule
{
  /* Rule type. */
  struct route_map_rule_cmd *cmd;

  /* For pretty printing. */
  char *rule_str;

  /* Pre-compiled match rule. */
  void *value;

  /* Linked list. */
  struct route_map_rule *next;
  struct route_map_rule *prev;
};

/* Making route map list. */
struct route_map_list
{
  struct route_map *head;
  struct route_map *tail;

  void (BGP::*add_hook) (char *);
  void (BGP::*delete_hook) (char *);
  void (BGP::*event_hook) (route_map_event_t, char *); 
};
/* Route map rule structure for matching and setting. */
struct route_map_rule_cmd
{
  /* Route map rule name (e.g. as-path, metric) */
  char *str;

  /* Function for value set or match. */
  route_map_result_t (BGP::*func_apply)(void *, struct prefix *, 
				   route_map_object_t, void *);

  /* Compile argument and return result as void *. */
  void *(BGP::*func_compile)(char *);

  /* Free allocated value by func_compile (). */
  void (BGP::*func_free)(void *);
};

/* Route map apply error. */
enum
{
  /* Route map rule is missing. */
  RMAP_RULE_MISSING = 1,

  /* Route map rule can't compile */
  RMAP_COMPILE_ERROR
};

/* Route map rule list. */
struct route_map_rule_list
{
  struct route_map_rule *head;
  struct route_map_rule *tail;
};

/* Route map index structure. */
struct route_map_index
{
  struct route_map *mmap;

  /* Preference of this route map rule. */
  int pref;

  /* Route map type permit or deny. */
  enum route_map_type type;			

  /* Do we follow old rules, or hop forward? */
  route_map_end_t exitpolicy;

  /* If we're using "GOTO", to where do we go? */
  int nextpref;

  /* Matching rule list. */
  struct route_map_rule_list match_list;
  struct route_map_rule_list set_list;

  /* Make linked list. */
  struct route_map_index *next;
  struct route_map_index *prev;
};

/* Route map list structure. */
struct route_map
{
  /* Name of route map. */
  char *name;

  /* Route map's rule. */
  struct route_map_index *head;
  struct route_map_index *tail;

  /* Make linked list. */
  struct route_map *next;
  struct route_map *prev;
};

/*stream.h*/

/* Stream buffer. */
struct stream
{
  struct stream *next;

  unsigned char *data;
  
  /* Put pointer. */
  unsigned long putp;

  /* Get pointer. */
  unsigned long getp;

  /* End of pointer. */
  unsigned long endp;

  /* Data size. */
  unsigned long size;
};

/* First in first out queue structure. */
struct stream_fifo
{
  unsigned long count;

  struct stream *head;
  struct stream *tail;
};

/* Utility macros. */
#define STREAM_PNT(S)   ((S)->data + (S)->getp)
#define STREAM_SIZE(S)  ((S)->size)
#define STREAM_REMAIN(S) ((S)->size - (S)->putp)
#define STREAM_DATA(S)  ((S)->data)


/***table.c****/

/* Routing table top structure. */
struct route_table
{
  struct route_node *top;
};

/* Each routing entry. */
struct route_node
{
  /* Actual prefix of this radix. */
  struct prefix p;

  /* Tree link. */
  struct route_table *table;
  struct route_node *parent;
  struct route_node *link[2];
#define l_left   link[0]
#define l_right  link[1]

  /* Lock of this radix */
  unsigned int lock;

  /* Each node of route. */
  void *info;

  /* Aggregation. */
  void *aggregate;
};


/*****thread.c*****/

#ifdef HAVE_RUSAGE
#define RUSAGE_T        struct rusage
#define GETRUSAGE(X)    getrusage (RUSAGE_SELF, X);
#else
#define RUSAGE_T        struct timeval
#define GETRUSAGE(X)    gettimeofday (X, NULL);
#endif /* HAVE_RUSAGE */

/* Linked list of thread. */
struct thread_list
{
  struct thread *head;
  struct thread *tail;
  int count;
};

/* Master of the theads. */
struct thread_master
{
  struct thread_list read;
  struct thread_list write;
  struct thread_list timer;
  struct thread_list event;
  struct thread_list ready;
  struct thread_list unuse;
  fd_set readfd;
  fd_set writefd;
  fd_set exceptfd;
  unsigned long alloc;
};

/* Thread itself. */
struct thread
{
  unsigned char type;		/* thread type */
  struct thread *next;		/* next pointer of the thread */
  struct thread *prev;		/* previous pointer of the thread */
  struct thread_master *master;	/* pointer to the struct thread_master. */
  int (BGP::*func) (struct thread *); /* event function */
  void *arg;			/* event argument */
  union {
    int val;			/* second argument of the event. */
    TCP *listenAgent;			/* file descriptor in case of read/write. */
    time_t time;	/* rest of time sands value. */
  } u;
  RUSAGE_T ru;			/* Indepth usage info.  */
};

/* Thread types. */
#define THREAD_READ           0
#define THREAD_WRITE          1
#define THREAD_TIMER          2
#define THREAD_EVENT          3
#define THREAD_READY          4
#define THREAD_UNUSED         5

/* Thread yield time.  */
#define THREAD_YIELD_TIME_SLOT     100 * 1000L /* 100ms */

/* Macros. */
#define THREAD_ARG(X) ((X)->arg)
#define THREAD_FD(X)  ((X)->u.fd)
#define THREAD_VAL(X) ((X)->u.val)

#define THREAD_READ_ON(master,thread,func,arg,sock) \
  do { \
    if (! thread) \
      thread = thread_add_read (master, func, arg, sock); \
  } while (0)

#define THREAD_WRITE_ON(master,thread,func,arg,sock) \
  do { \
    if (! thread) \
      thread = thread_add_write (master,func, arg, sock); \
  } while (0)

#define THREAD_TIMER_ON(master,thread,func,arg,time) \
  do { \
    if (! thread) \
      thread = thread_add_timer (master,func, arg, time); \
  } while (0)

#define THREAD_OFF(thread) \
  do { \
    if (thread) \
      { \
        thread_cancel (thread); \
        thread = NULL; \
      } \
  } while (0)

#define THREAD_READ_OFF(thread)  THREAD_OFF(thread)
#define THREAD_WRITE_OFF(thread)  THREAD_OFF(thread)
#define THREAD_TIMER_OFF(thread)  THREAD_OFF(thread)


/*****vector.h*******/
/* struct for vector */
struct _vector 
{
  unsigned int max;		/* max number of used slot */
  unsigned int alloced;		/* number of allocated slot */
  void **index;			/* index to data */
};

#define VECTOR_MIN_SIZE 1

/* (Sometimes) usefull macros.  This macro convert index expression to
 array expression. */
#define vector_slot(V,I)  ((V)->index[(I)])
#define vector_max(V) ((V)->max)

/*****vty.c*******/

/* Vty events */
enum event 
  {
    VTY_SERV,
    VTY_READ,
    VTY_WRITE,
    VTY_TIMEOUT_RESET,
#ifdef VTYSH
    VTYSH_SERV,
    VTYSH_READ
#endif /* VTYSH */
  };


/* Timeout types */
#define TYPE_MAIN    0
#define TYPE_FETCH1  1
#define TYPE_FETCH2  2
#define TYPE_EXECUTE 3

#define SET_TIMEOUT_TYPE_MAIN     type=TYPE_MAIN
#define SET_TIMEOUT_TYPE_FETCH1   type=TYPE_FETCH1
#define SET_TIMEOUT_TYPE_FETCH2   type=TYPE_FETCH2
#define SET_TIMEOUT_TYPE_EXECUTE  type=TYPE_EXECUTE


/*duplet that identifies an agent */
struct agent_index {
  u_int32_t addr;
  u_int32_t port;
};


typedef vector<pair<IPAddr_t,BGP*> > IpBgp_t;
typedef vector<pair<agent_index,pair<int,RMsgList_t> > >        Agent2MsgListMap_t ;
typedef vector<pair<agent_index,union sockunion > >             Agent2Su_t ;
typedef vector< pair < union sockunion ,BGP* > >                Su2BgpMap_t ;
typedef pair<string,string>                                     IpAddrMaskPair_t;
typedef list<IpAddrMaskPair_t>                                  InterfaceList_t; 
typedef list<pair<string,InterfaceList_t > >                    String2List_t ;
struct InterruptInfo
{
  TCP *IntAgent;
  Data *intmsg;
};

#define BGP_VERSION 1.0

/*extern "C"  void Qsort(void *base, size_t nmemb, size_t size,
                  int(*compar)(const void *, const void *))
	{
		qsort(base,nmemb,size,compar);
	}
*/
extern "C"
{
  int community_compare(const void*,const void *);
  int cmp_node(const void*,const void*);
  int cmp_desc(const void*,const void*);
}

#define EXTCOMMUNITY_LIST_STR "Add a extended community list entry\n"
#define EXTCOMMUNITY_VAL_STR  "Extended community attribute in 'rt aa:nn_or_IPaddr:nn' OR 'soo aa:nn_or_IPaddr:nn' format\n"
#define COMMUNITY_LIST_STR "Add a community list entry\n"
#define COMMUNITY_VAL_STR  "Community number in aa:nn format or internet|local-AS|no-advertise|no-export\n"

class MessageBuffer
{
 public:
    MessageBuffer():ReceivedMsg(0),partialRead(false),dataAvailable(false)
        {}
    MessageBuffer(Data* data,bool partial, bool avl):ReceivedMsg(data),partialRead(partial),dataAvailable(avl)
    {}
    Data* ReceivedMsg;
    bool partialRead; // indicates if receivedmsg has partial data => need to append data
    bool dataAvailable; //indicates if data is available to read
};

typedef std::pair<TCP*,MessageBuffer*> TCPMsgBuf_pair;
typedef std::map<TCP*,MessageBuffer*> TCPMsgBuf;

class BGP : public Application {

  /*******************************************************************************
                            member functions added for BGP class
  ********************************************************************************/
 public:
  BGP(double);
  virtual Application* Copy() const;
  void bgp_main();	
  void timeout(Event *e);
  void AttachNode(Node*);
  void SetTrace(Trace::TraceStatus);
  void StartAt(double);
  void StopAt(double);
  void ConnectionComplete(L4Protocol *);
  void ServConnectionComplete(L4Protocol *);
  bool ConnectionFromPeer(L4Protocol*, IPAddr_t,PortId_t);
  void Receive(Packet *p,L4Protocol*,Seq_t);
  void Closed(L4Protocol*);
  void config_file(char*);
  int sendMessage(Data,struct peer*);
  virtual int command(int argc,const char** argv);
  void recv(int nbytes,TCP* from);
  void bgp_interrupt(BGP*,TCP*,Data*);
  BGP* findBgp(IPAddr_t);
  int bgp_accept(struct thread*);
 // bool bgp_accept(IPAddr_t *,TCP*);
  void dump_peer_list();
  // void bgp_randomize();
  void bgp_serv_sock_family();
  ~BGP(){

	//delete all the data alloced in the ctr?
    closezlog(zlog_default);
  };

  /**********************************************************************************
                               member variables for the class
  **********************************************************************************/

  Node* myNode;
  static bool enter_bgp_construct;         /*Flag turn on when first BGP instance enters constructor */
  static bool enter_bgp_main;              /*Flag turn on when first BGP instance enters main bgp_func */
  static int instance_cnt;                 /*# of BGP instances */

  static bool dont_reuse;                   /*Used to check the memory savings of the mem reusing scheme */
  static double last_update_time_;
  static FILE*  use_log_fp_;
  static char*  use_log_file_;
  char new_line[INET_ADDRSTRLEN + 3];   /* Hack, to append BGP identifier (ip addr string)
                       when one global file for logging is used*/


  
#define NO_WORKLOAD_MODEL         0
#define UNIFORM_WORKLOAD_MODEL     1
#define TIME_SAMPLE_WORKLOAD_MODEL  2 

#ifdef HAVE_PERFCTR
  double workload(struct perfctr_sum_ctrs *);
  static int perfcnt_init;
  static struct perfctr_info info;
#else 
  double workload();
#endif /* HAVE_PERFCTR */

  int workload_model;                  /* type of workload model */
  static double uniform_max;
  static double uniform_min;
  static bool default_randomize; 
  static Uniform*  rng;  /* uniform random number generator 
					  used by uniform workload model*/
  static int rnd;                      /* rng initialization flag */

  static IpBgp_t IpBgp;

  BGPTimer* timer;                     /* timer used for BGP++ task scheduling */
  static time_t start_time ;           /* Global Start Time of Simulation  */
  int type ;                           /* type of timeout */
  bool debug_on;                        /* print debug information */           
  bool enable_routeadv;                /* enable per peer route adv rate limiting */  
  bool enable_routeadv_prefix;          /* enable per prefix route adv rate limiting */ 
#define MRAI_DISABLE     0
#define MRAI_PER_PEER    1 
#define MRAI_PER_PREFIX  2
  int mrai_type;                       /* MRAI type    */  
  bool ssld;                           /* sender side loop detection on */
  double finish_time;                  /* finish time of simulation */
  struct thread *thread_to_be_fetched; /* holds the thread that is fetched 
					  after the virtual select unblocks */
#define MAX_CONFIG_LENGTH  512

  char bgp_config_file[MAX_CONFIG_LENGTH]; /* bgpd configuration file */
  TCP* ServAgent;                          /* Local listening tcp agent  */ 
  TCP* InterruptAgent ;                    /* Agent, analogous to file pointer, that cause  
					      the most recently virtual select unblocking */

  TCPMsgBuf MsgBuffer; /* Each TCP Agent supported by BGP needs its own message buffer*/
  list<struct InterruptInfo> InterruptQueue ;  /* Queue were interrupts, i.e. msg arrivals,
					      are temporary stored if the bgp daemon is busy when the 
					      interrupt occurs; as soon as the bgpd finishes with 
					      the work in progress, it checks the Interrupt queue, before 
					      entering the virtual select()					    
					   */
  struct llist* dummy_peer_list;



  /************************************************************************************
                              static data members from zebra
  *************************************************************************************/

  /*aspath.c*/
 
  static struct as_list_master as_list_master;


  /*bgp_vty.c*/

  static struct cmd_node bgp_node;
  static struct cmd_node bgp_ipv4_unicast_node;
  static struct cmd_node bgp_ipv4_multicast_node;
  static struct cmd_node bgp_ipv6_unicast_node;
  static struct cmd_node bgp_vpnv4_node;
  static struct cmd_node community_list_node;
  /* These are found as DEFUNs in a lot of zebra .c files. Co-locating them here*/
  
  static struct cmd_element ip_community_list_cmd;
  static struct cmd_element no_ip_community_list_cmd;
  static struct cmd_element no_ip_community_list_all_cmd;
  static struct cmd_element no_ip_community_list_name_all_cmd;
  static struct cmd_element debug_bgp_fsm_cmd;
  static struct cmd_element no_debug_bgp_fsm_cmd;
  static struct cmd_element undebug_bgp_fsm_cmd;
  static struct cmd_element debug_bgp_events_cmd;
  static struct cmd_element no_debug_bgp_events_cmd;
  static struct cmd_element undebug_bgp_events_cmd;
  static struct cmd_element debug_bgp_filter_cmd;
  static struct cmd_element no_debug_bgp_filter_cmd;
  static struct cmd_element undebug_bgp_filter_cmd;
  static struct cmd_element debug_bgp_keepalive_cmd;
  static struct cmd_element no_debug_bgp_keepalive_cmd;
  static struct cmd_element undebug_bgp_keepalive_cmd;
  static struct cmd_element debug_bgp_update_cmd;
  static struct cmd_element debug_bgp_update_direct_cmd;
  static struct cmd_element no_debug_bgp_update_cmd;
  static struct cmd_element undebug_bgp_update_cmd;
  static struct cmd_element debug_bgp_normal_cmd;
  static struct cmd_element no_debug_bgp_normal_cmd;
  static struct cmd_element undebug_bgp_normal_cmd;
  static struct cmd_element no_debug_bgp_all_cmd;
  static struct cmd_element undebug_bgp_all_cmd;
  static struct cmd_element show_debugging_bgp_cmd;
  static struct cmd_element dump_bgp_all_cmd;
  static struct cmd_element dump_bgp_all_interval_cmd;
  static struct cmd_element no_dump_bgp_all_cmd;
  static struct cmd_element dump_bgp_updates_cmd;
  static struct cmd_element dump_bgp_updates_interval_cmd;
  static struct cmd_element no_dump_bgp_updates_cmd;
  static struct cmd_element dump_bgp_routes_interval_cmd;
  static struct cmd_element no_dump_bgp_routes_cmd;
  static struct cmd_element ip_as_path_cmd;
  static struct cmd_element no_ip_as_path_cmd;
  static struct cmd_element no_ip_as_path_all_cmd;
  static struct cmd_element bgp_network_cmd;
  static struct cmd_element bgp_network_mask_cmd;
  static struct cmd_element bgp_network_mask_natural_cmd;
  static struct cmd_element no_bgp_network_cmd;
  static struct cmd_element no_bgp_network_mask_cmd;
  static struct cmd_element no_bgp_network_mask_natural_cmd;
  static struct cmd_element aggregate_address_cmd;
  static struct cmd_element aggregate_address_mask_cmd;
  static struct cmd_element aggregate_address_summary_only_cmd;
  static struct cmd_element aggregate_address_mask_summary_only_cmd;
  static struct cmd_element aggregate_address_as_set_cmd;
  static struct cmd_element aggregate_address_mask_as_set_cmd;
  static struct cmd_element aggregate_address_as_set_summary_cmd;
  static struct cmd_element aggregate_address_summary_as_set_cmd;
  static struct cmd_element aggregate_address_mask_as_set_summary_cmd;
  static struct cmd_element aggregate_address_mask_summary_as_set_cmd;
  static struct cmd_element no_aggregate_address_cmd;
  static struct cmd_element no_aggregate_address_summary_only_cmd;
  static struct cmd_element no_aggregate_address_as_set_cmd;
  static struct cmd_element no_aggregate_address_as_set_summary_cmd;
  static struct cmd_element no_aggregate_address_summary_as_set_cmd;
  static struct cmd_element no_aggregate_address_mask_cmd;
  static struct cmd_element no_aggregate_address_mask_summary_only_cmd;
  static struct cmd_element no_aggregate_address_mask_as_set_cmd;
  static struct cmd_element no_aggregate_address_mask_as_set_summary_cmd;
  static struct cmd_element no_aggregate_address_mask_summary_as_set_cmd;
  static struct cmd_element show_ip_bgp_cmd;
  static struct cmd_element show_ip_bgp_route_cmd;
  static struct cmd_element show_ip_bgp_prefix_cmd;
  static struct cmd_element show_ip_bgp_regexp_cmd;
  static struct cmd_element show_ip_bgp_prefix_list_cmd;
  static struct cmd_element show_ip_bgp_filter_list_cmd;
  static struct cmd_element show_ip_bgp_cidr_only_cmd;
  static struct cmd_element show_ip_bgp_community_all_cmd;
  static struct cmd_element show_ip_bgp_community_cmd;
  static struct cmd_element show_ip_bgp_community2_cmd;
  static struct cmd_element show_ip_bgp_community3_cmd;
  static struct cmd_element show_ip_bgp_community4_cmd;
  static struct cmd_element show_ip_bgp_community_exact_cmd;
  static struct cmd_element show_ip_bgp_community2_exact_cmd;
  static struct cmd_element show_ip_bgp_community3_exact_cmd;
  static struct cmd_element show_ip_bgp_community4_exact_cmd;
  static struct cmd_element show_ip_bgp_community_list_cmd;
  static struct cmd_element show_ip_bgp_community_list_exact_cmd;
  static struct cmd_element show_ip_bgp_prefix_longer_cmd;
  static struct cmd_element show_ip_bgp_neighbor_advertised_route_cmd;
  static struct cmd_element show_ip_bgp_neighbor_received_routes_cmd;
  static struct cmd_element show_ip_bgp_neighbor_routes_cmd;
  static struct cmd_element bgp_distance_cmd;
  static struct cmd_element no_bgp_distance_cmd;
  static struct cmd_element no_bgp_distance2_cmd;
  static struct cmd_element bgp_distance_source_cmd;
  static struct cmd_element no_bgp_distance_source_cmd;
  static struct cmd_element bgp_distance_source_access_list_cmd;
  static struct cmd_element no_bgp_distance_source_access_list_cmd;
  static struct cmd_element bgp_damp_set_cmd;
  static struct cmd_element bgp_damp_set2_cmd;
  static struct cmd_element bgp_damp_set3_cmd;
  static struct cmd_element bgp_damp_unset_cmd;
  static struct cmd_element bgp_damp_unset2_cmd;
  static struct cmd_element match_ip_address_cmd;
  static struct cmd_element no_match_ip_address_cmd;
  static struct cmd_element no_match_ip_address_val_cmd;
  static struct cmd_element match_ip_next_hop_cmd;
  static struct cmd_element no_match_ip_next_hop_cmd;
  static struct cmd_element no_match_ip_next_hop_val_cmd;
  static struct cmd_element match_ip_address_prefix_list_cmd;
  static struct cmd_element no_match_ip_address_prefix_list_cmd;
  static struct cmd_element no_match_ip_address_prefix_list_val_cmd;
  static struct cmd_element match_ip_next_hop_prefix_list_cmd;
  static struct cmd_element no_match_ip_next_hop_prefix_list_cmd;
  static struct cmd_element no_match_ip_next_hop_prefix_list_val_cmd;
  static struct cmd_element match_metric_cmd;
  static struct cmd_element no_match_metric_cmd;
  static struct cmd_element no_match_metric_val_cmd;
  static struct cmd_element match_community_cmd;
  static struct cmd_element no_match_community_cmd;
  static struct cmd_element no_match_community_val_cmd;
  static struct cmd_element match_aspath_cmd;
  static struct cmd_element no_match_aspath_cmd;
  static struct cmd_element no_match_aspath_val_cmd;
  static struct cmd_element set_ip_nexthop_cmd;
  static struct cmd_element no_set_ip_nexthop_cmd;
  static struct cmd_element no_set_ip_nexthop_val_cmd;
  static struct cmd_element set_metric_cmd;
  static struct cmd_element no_set_metric_cmd;
  static struct cmd_element no_set_metric_val_cmd;
  static struct cmd_element set_local_pref_cmd;
  static struct cmd_element no_set_local_pref_cmd;
  static struct cmd_element no_set_local_pref_val_cmd;
  static struct cmd_element set_weight_cmd;
  static struct cmd_element no_set_weight_cmd;
  static struct cmd_element no_set_weight_val_cmd;
  static struct cmd_element set_aspath_prepend_cmd;
  static struct cmd_element no_set_aspath_prepend_cmd;
  static struct cmd_element no_set_aspath_prepend_val_cmd;
  static struct cmd_element set_community_cmd;
  static struct cmd_element set_community_none_cmd;
  static struct cmd_element no_set_community_cmd;
  static struct cmd_element no_set_community_val_cmd;
  static struct cmd_element no_set_community_none_cmd;
  static struct cmd_element set_community_additive_cmd;
  static struct cmd_element no_set_community_additive_cmd;
  static struct cmd_element no_set_community_additive_val_cmd;
  static struct cmd_element set_community_delete_cmd;
  static struct cmd_element no_set_community_delete_cmd;
  static struct cmd_element no_set_community_delete_val_cmd;
  static struct cmd_element set_ecommunity_rt_cmd;
  static struct cmd_element no_set_ecommunity_rt_cmd;
  static struct cmd_element no_set_ecommunity_rt_val_cmd;
  static struct cmd_element set_ecommunity_soo_cmd;
  static struct cmd_element no_set_ecommunity_soo_cmd;
  static struct cmd_element no_set_ecommunity_soo_val_cmd;
  static struct cmd_element set_origin_cmd;
  static struct cmd_element no_set_origin_cmd;
  static struct cmd_element no_set_origin_val_cmd;
  static struct cmd_element set_atomic_aggregate_cmd;
  static struct cmd_element no_set_atomic_aggregate_cmd;
  static struct cmd_element set_aggregator_as_cmd;
  static struct cmd_element no_set_aggregator_as_cmd;
  static struct cmd_element no_set_aggregator_as_val_cmd;
  static struct cmd_element set_originator_id_cmd;
  static struct cmd_element no_set_originator_id_cmd;
  static struct cmd_element no_set_originator_id_val_cmd;
  static struct cmd_element bgp_cluster_id_cmd;
  static struct cmd_element neighbor_transparent_nexthop_cmd;
  static struct cmd_element show_startup_config_cmd;
  static struct cmd_element config_log_file_cmd;
  static struct cmd_element no_config_log_file_cmd;
  static struct cmd_element access_list_cmd;
  static struct cmd_element access_list_exact_cmd;
  static struct cmd_element no_access_list_cmd;
  static struct cmd_element no_access_list_exact_cmd;
  static struct cmd_element no_access_list_all_cmd;
  static struct cmd_element access_list_remark_cmd;
  static struct cmd_element no_access_list_remark_cmd;
  static struct cmd_element no_access_list_remark_arg_cmd;
  static struct cmd_element show_memory_all_cmd;
  static struct cmd_element show_memory_cmd;
  static struct cmd_element show_memory_lib_cmd;
  static struct cmd_element show_memory_bgp_cmd;
  static struct cmd_element ip_prefix_list_cmd;
  static struct cmd_element ip_prefix_list_ge_cmd;
  static struct cmd_element ip_prefix_list_ge_le_cmd;
  static struct cmd_element ip_prefix_list_le_cmd;
  static struct cmd_element ip_prefix_list_le_ge_cmd;
  static struct cmd_element ip_prefix_list_seq_cmd;
  static struct cmd_element ip_prefix_list_seq_ge_cmd;
  static struct cmd_element ip_prefix_list_seq_ge_le_cmd;
  static struct cmd_element ip_prefix_list_seq_le_cmd;
  static struct cmd_element ip_prefix_list_seq_le_ge_cmd;
  static struct cmd_element no_ip_prefix_list_cmd;
  static struct cmd_element no_ip_prefix_list_prefix_cmd;
  static struct cmd_element no_ip_prefix_list_ge_cmd;
  static struct cmd_element no_ip_prefix_list_ge_le_cmd;
  static struct cmd_element no_ip_prefix_list_le_cmd;
  static struct cmd_element no_ip_prefix_list_le_ge_cmd;
  static struct cmd_element no_ip_prefix_list_seq_cmd;
  static struct cmd_element no_ip_prefix_list_seq_ge_cmd;
  static struct cmd_element no_ip_prefix_list_seq_ge_le_cmd;
  static struct cmd_element no_ip_prefix_list_seq_le_cmd;
  static struct cmd_element no_ip_prefix_list_seq_le_ge_cmd;
  static struct cmd_element ip_prefix_list_sequence_number_cmd;
  static struct cmd_element no_ip_prefix_list_sequence_number_cmd;
  static struct cmd_element ip_prefix_list_description_cmd;
  static struct cmd_element no_ip_prefix_list_description_cmd;
  static struct cmd_element no_ip_prefix_list_description_arg_cmd;
  static struct cmd_element show_ip_prefix_list_cmd;
  static struct cmd_element show_ip_prefix_list_name_cmd;
  static struct cmd_element show_ip_prefix_list_name_seq_cmd;
  static struct cmd_element show_ip_prefix_list_prefix_cmd;
  static struct cmd_element show_ip_prefix_list_prefix_longer_cmd;
  static struct cmd_element show_ip_prefix_list_prefix_first_match_cmd;
  static struct cmd_element show_ip_prefix_list_summary_cmd;
  static struct cmd_element show_ip_prefix_list_summary_name_cmd;
  static struct cmd_element show_ip_prefix_list_detail_cmd;
  static struct cmd_element show_ip_prefix_list_detail_name_cmd;
  static struct cmd_element clear_ip_prefix_list_cmd;
  static struct cmd_element clear_ip_prefix_list_name_cmd;
  static struct cmd_element clear_ip_prefix_list_name_prefix_cmd;
  static struct cmd_element route_map_cmd;
  static struct cmd_element no_route_map_all_cmd;
  static struct cmd_element no_route_map_cmd;
  static struct cmd_element rmap_onmatch_next_cmd;
  static struct cmd_element no_rmap_onmatch_next_cmd;
  static struct cmd_element rmap_onmatch_goto_cmd;
  static struct cmd_element no_rmap_onmatch_goto_cmd;
  static struct cmd_element bgp_network_route_map_cmd;
  static struct cmd_element bgp_network_mask_route_map_cmd;
  static struct cmd_element bgp_network_mask_natural_route_map_cmd;
  static struct cmd_element bgp_network_backdoor_cmd;
  static struct cmd_element bgp_network_mask_backdoor_cmd;
  static struct cmd_element bgp_network_mask_natural_backdoor_cmd;
  static struct cmd_element show_ip_bgp_ipv4_cmd;
  static struct cmd_element show_ip_bgp_ipv4_route_cmd;
  static struct cmd_element show_ip_bgp_vpnv4_all_route_cmd;
  //static struct cmd_element show_ip_bgp_vpnv4_rd_route_cmd;
  static struct cmd_element show_ip_bgp_ipv4_prefix_cmd;
  static struct cmd_element show_ip_bgp_vpnv4_all_prefix_cmd;
  //static struct cmd_element show_ip_bgp_vpnv4_rd_prefix_cmd;
  static struct cmd_element show_ip_bgp_view_cmd;
  static struct cmd_element show_ip_bgp_view_route_cmd;
  static struct cmd_element show_ip_bgp_view_prefix_cmd;
  static struct cmd_element show_ip_bgp_ipv4_regexp_cmd;
  static struct cmd_element show_ip_bgp_ipv4_prefix_list_cmd;
  static struct cmd_element show_ip_bgp_ipv4_filter_list_cmd;
  static struct cmd_element show_ip_bgp_route_map_cmd;
  static struct cmd_element show_ip_bgp_ipv4_route_map_cmd;
  static struct cmd_element show_ip_bgp_ipv4_cidr_only_cmd;
  static struct cmd_element show_ip_bgp_ipv4_community_all_cmd;
  static struct cmd_element show_ip_bgp_ipv4_community_cmd;
  static struct cmd_element show_ip_bgp_ipv4_community_exact_cmd;
  static struct cmd_element show_ip_bgp_ipv4_community_list_cmd;
  static struct cmd_element show_ip_bgp_ipv4_community_list_exact_cmd;
  static struct cmd_element show_ip_bgp_ipv4_prefix_longer_cmd;
  static struct cmd_element show_ip_bgp_ipv4_neighbor_advertised_route_cmd;
  static struct cmd_element show_ip_bgp_ipv4_neighbor_received_routes_cmd;
  static struct cmd_element show_ip_bgp_ipv4_neighbor_routes_cmd;
  static struct cmd_element show_ip_bgp_neighbor_received_prefix_filter_cmd;
  static struct cmd_element show_ip_bgp_ipv4_neighbor_received_prefix_filter_cmd;
  static struct cmd_element show_ip_bgp_dampened_paths_cmd;
  static struct cmd_element show_ip_bgp_flap_statistics_cmd;
  static struct cmd_element show_ip_bgp_flap_address_cmd;
  static struct cmd_element show_ip_bgp_flap_prefix_cmd;
  static struct cmd_element show_ip_bgp_flap_cidr_only_cmd;
  static struct cmd_element show_ip_bgp_flap_regexp_cmd;
  static struct cmd_element show_ip_bgp_flap_filter_list_cmd;
  static struct cmd_element show_ip_bgp_flap_prefix_list_cmd;
  static struct cmd_element show_ip_bgp_flap_prefix_longer_cmd;
  static struct cmd_element show_ip_bgp_flap_route_map_cmd;
  static struct cmd_element show_ip_bgp_neighbor_flap_cmd;
  static struct cmd_element show_ip_bgp_neighbor_damp_cmd;
  static struct cmd_element clear_ip_bgp_dampening_cmd;
  static struct cmd_element clear_ip_bgp_dampening_prefix_cmd;
  static struct cmd_element clear_ip_bgp_dampening_address_cmd;
  static struct cmd_element clear_ip_bgp_dampening_address_mask_cmd;
  static struct cmd_element match_community_exact_cmd;  
  static struct cmd_element match_ecommunity_cmd;  
  static struct cmd_element no_match_ecommunity_cmd;  
  static struct cmd_element match_origin_cmd;  
  static struct cmd_element no_match_origin_cmd;  
  static struct cmd_element set_vpnv4_nexthop_cmd;  
  static struct cmd_element no_set_vpnv4_nexthop_cmd;  
  static struct cmd_element show_ip_as_path_access_list_cmd;  
  static struct cmd_element show_ip_as_path_access_list_all_cmd;  
  static struct cmd_element dump_bgp_routes_cmd;
  static struct cmd_element show_ip_bgp_scan_cmd;
  static struct cmd_element bgp_scan_time_cmd;
  static struct cmd_element no_bgp_scan_time_cmd;
  static struct cmd_element no_bgp_scan_time_val_cmd;

  static struct cmd_element no_bgp_network_route_map_cmd;
  static struct cmd_element show_ip_bgp_ipv4_community2_cmd;
  static struct cmd_element show_ip_bgp_ipv4_community3_cmd;
  static struct cmd_element show_ip_bgp_ipv4_community4_cmd;
  static struct cmd_element show_ip_bgp_ipv4_community2_exact_cmd;
  static struct cmd_element show_ip_bgp_ipv4_community3_exact_cmd;
  static struct cmd_element show_ip_bgp_ipv4_community4_exact_cmd;

  static struct cmd_element no_bgp_network_mask_route_map_cmd;
  static struct cmd_element no_bgp_network_mask_natural_route_map_cmd;
  static struct cmd_element no_bgp_network_backdoor_cmd;
  static struct cmd_element no_bgp_network_mask_backdoor_cmd;
  static struct cmd_element no_bgp_network_mask_natural_backdoor_cmd;
  static struct cmd_element show_bgp_ipv6_route_map_cmd;

  static struct cmd_element no_match_community_exact_cmd;
  static struct cmd_element no_match_ecommunity_val_cmd;
  static struct cmd_element no_match_origin_val_cmd;
  static struct cmd_element set_metric_addsub_cmd;
  static struct cmd_element config_quit_cmd;
  static struct cmd_element config_write_cmd;
  static struct cmd_element config_write_memory_cmd;
  static struct cmd_element copy_runningconfig_startupconfig_cmd;
  static struct cmd_element show_running_config_cmd;
  static struct cmd_element password_text_cmd;
  static struct cmd_element enable_password_text_cmd;
  static struct cmd_element no_config_log_syslog_facility_cmd;
  static struct cmd_element bgp_multiple_instance_cmd;
  static struct cmd_element no_bgp_multiple_instance_cmd;
  static struct cmd_element bgp_config_type_cmd;
  static struct cmd_element no_bgp_config_type_cmd;
  static struct cmd_element no_synchronization_cmd;
  static struct cmd_element no_auto_summary_cmd;
  static struct cmd_element router_bgp_cmd;
  static struct cmd_element router_bgp_view_cmd;
  static struct cmd_element no_router_bgp_cmd;
  static struct cmd_element no_router_bgp_view_cmd;
  static struct cmd_element bgp_router_id_cmd;
  static struct cmd_element no_bgp_router_id_cmd;
  static struct cmd_element no_bgp_router_id_val_cmd;
  static struct cmd_element bgp_cluster_id32_cmd;
  static struct cmd_element no_bgp_cluster_id_cmd;
  static struct cmd_element no_bgp_cluster_id_arg_cmd;
  static struct cmd_element bgp_confederation_identifier_cmd;
  static struct cmd_element no_bgp_confederation_identifier_cmd;
  static struct cmd_element no_bgp_confederation_identifier_arg_cmd;
  static struct cmd_element bgp_confederation_peers_cmd;
  static struct cmd_element no_bgp_confederation_peers_cmd;
  static struct cmd_element bgp_timers_cmd;
  static struct cmd_element no_bgp_timers_cmd;
  static struct cmd_element no_bgp_timers_arg_cmd;
  static struct cmd_element bgp_client_to_client_reflection_cmd;
  static struct cmd_element no_bgp_client_to_client_reflection_cmd;
  static struct cmd_element bgp_always_compare_med_cmd;
  static struct cmd_element no_bgp_always_compare_med_cmd;
  static struct cmd_element bgp_deterministic_med_cmd;
  static struct cmd_element no_bgp_deterministic_med_cmd;
  static struct cmd_element bgp_fast_external_failover_cmd;
  static struct cmd_element no_bgp_fast_external_failover_cmd;
  static struct cmd_element bgp_enforce_first_as_cmd;
  static struct cmd_element no_bgp_enforce_first_as_cmd;
  static struct cmd_element bgp_bestpath_compare_router_id_cmd;
  static struct cmd_element no_bgp_bestpath_compare_router_id_cmd;
  static struct cmd_element bgp_bestpath_aspath_ignore_cmd;
  static struct cmd_element no_bgp_bestpath_aspath_ignore_cmd;
  static struct cmd_element bgp_log_neighbor_changes_cmd;
  static struct cmd_element no_bgp_log_neighbor_changes_cmd;
  static struct cmd_element bgp_bestpath_med_cmd;
  static struct cmd_element bgp_bestpath_med2_cmd;
  static struct cmd_element bgp_bestpath_med3_cmd;
  static struct cmd_element no_bgp_bestpath_med_cmd;
  static struct cmd_element no_bgp_bestpath_med2_cmd;
  static struct cmd_element no_bgp_bestpath_med3_cmd;
  static struct cmd_element no_bgp_default_ipv4_unicast_cmd;
  static struct cmd_element bgp_default_ipv4_unicast_cmd;
  static struct cmd_element bgp_network_import_check_cmd;
  static struct cmd_element no_bgp_network_import_check_cmd;
  static struct cmd_element bgp_default_local_preference_cmd;
  static struct cmd_element no_bgp_default_local_preference_cmd;
  static struct cmd_element no_bgp_default_local_preference_val_cmd;
  static struct cmd_element neighbor_remote_as_cmd;
  static struct cmd_element neighbor_peer_group_cmd;
  static struct cmd_element no_neighbor_cmd;
  static struct cmd_element no_neighbor_remote_as_cmd;
  static struct cmd_element no_neighbor_peer_group_cmd;
  static struct cmd_element no_neighbor_peer_group_remote_as_cmd;
  static struct cmd_element neighbor_local_as_cmd;
  static struct cmd_element neighbor_local_as_no_prepend_cmd;
  static struct cmd_element no_neighbor_local_as_cmd;
  static struct cmd_element no_neighbor_local_as_val_cmd;
  static struct cmd_element no_neighbor_local_as_val2_cmd;
  static struct cmd_element neighbor_activate_cmd;
  static struct cmd_element no_neighbor_activate_cmd;
  static struct cmd_element neighbor_set_peer_group_cmd;
  static struct cmd_element no_neighbor_set_peer_group_cmd;
  static struct cmd_element neighbor_passive_cmd;
  static struct cmd_element no_neighbor_passive_cmd;
  static struct cmd_element neighbor_shutdown_cmd;
  static struct cmd_element no_neighbor_shutdown_cmd;
  static struct cmd_element neighbor_capability_route_refresh_cmd;
  static struct cmd_element no_neighbor_capability_route_refresh_cmd;
  static struct cmd_element neighbor_capability_dynamic_cmd;
  static struct cmd_element no_neighbor_capability_dynamic_cmd;
  static struct cmd_element neighbor_dont_capability_negotiate_cmd;
  static struct cmd_element no_neighbor_dont_capability_negotiate_cmd;
  static struct cmd_element neighbor_capability_orf_prefix_cmd;
  static struct cmd_element no_neighbor_capability_orf_prefix_cmd;
  static struct cmd_element neighbor_nexthop_self_cmd;
  static struct cmd_element no_neighbor_nexthop_self_cmd;
  static struct cmd_element neighbor_remove_private_as_cmd;
  static struct cmd_element no_neighbor_remove_private_as_cmd;
  static struct cmd_element neighbor_send_community_cmd;
  static struct cmd_element no_neighbor_send_community_cmd;
  static struct cmd_element neighbor_send_community_type_cmd;
  static struct cmd_element no_neighbor_send_community_type_cmd;
  static struct cmd_element neighbor_soft_reconfiguration_cmd;
  static struct cmd_element no_neighbor_soft_reconfiguration_cmd;
  static struct cmd_element neighbor_route_reflector_client_cmd;
  static struct cmd_element no_neighbor_route_reflector_client_cmd;
  static struct cmd_element neighbor_route_server_client_cmd;
  static struct cmd_element no_neighbor_route_server_client_cmd;
  static struct cmd_element neighbor_attr_unchanged_cmd;
  static struct cmd_element neighbor_attr_unchanged1_cmd;
  static struct cmd_element neighbor_attr_unchanged2_cmd;
  static struct cmd_element neighbor_attr_unchanged3_cmd;
  static struct cmd_element neighbor_attr_unchanged4_cmd;
  static struct cmd_element neighbor_attr_unchanged5_cmd;
  static struct cmd_element neighbor_attr_unchanged6_cmd;
  static struct cmd_element neighbor_attr_unchanged7_cmd;
  static struct cmd_element neighbor_attr_unchanged8_cmd;
  static struct cmd_element neighbor_attr_unchanged9_cmd;
  static struct cmd_element neighbor_attr_unchanged10_cmd;
  static struct cmd_element no_neighbor_attr_unchanged_cmd;
  static struct cmd_element no_neighbor_attr_unchanged1_cmd;
  static struct cmd_element no_neighbor_attr_unchanged2_cmd;
  static struct cmd_element no_neighbor_attr_unchanged3_cmd;
  static struct cmd_element no_neighbor_attr_unchanged4_cmd;
  static struct cmd_element no_neighbor_attr_unchanged5_cmd;
  static struct cmd_element no_neighbor_attr_unchanged6_cmd;
  static struct cmd_element no_neighbor_attr_unchanged7_cmd;
  static struct cmd_element no_neighbor_attr_unchanged8_cmd;
  static struct cmd_element no_neighbor_attr_unchanged9_cmd;
  static struct cmd_element no_neighbor_attr_unchanged10_cmd;
  static struct cmd_element neighbor_transparent_as_cmd;
  static struct cmd_element neighbor_ebgp_multihop_cmd;
  static struct cmd_element neighbor_ebgp_multihop_ttl_cmd;
  static struct cmd_element no_neighbor_ebgp_multihop_cmd;
  static struct cmd_element no_neighbor_ebgp_multihop_ttl_cmd;
  static struct cmd_element neighbor_enforce_multihop_cmd;
  static struct cmd_element no_neighbor_enforce_multihop_cmd;
  static struct cmd_element neighbor_description_cmd;
  static struct cmd_element no_neighbor_description_cmd;
  static struct cmd_element no_neighbor_description_val_cmd;
  static struct cmd_element neighbor_update_source_cmd;
  static struct cmd_element no_neighbor_update_source_cmd;
  static struct cmd_element neighbor_default_originate_cmd;
  static struct cmd_element neighbor_default_originate_rmap_cmd;
  static struct cmd_element no_neighbor_default_originate_cmd;
  static struct cmd_element no_neighbor_default_originate_rmap_cmd;
  static struct cmd_element neighbor_port_cmd;
  static struct cmd_element no_neighbor_port_cmd;
  static struct cmd_element no_neighbor_port_val_cmd;
  static struct cmd_element neighbor_weight_cmd;
  static struct cmd_element no_neighbor_weight_cmd;
  static struct cmd_element no_neighbor_weight_val_cmd;
  static struct cmd_element neighbor_override_capability_cmd;
  static struct cmd_element no_neighbor_override_capability_cmd;
  static struct cmd_element neighbor_strict_capability_cmd;
  static struct cmd_element no_neighbor_strict_capability_cmd;
  static struct cmd_element neighbor_timers_cmd;
  static struct cmd_element no_neighbor_timers_cmd;
  static struct cmd_element neighbor_timers_connect_cmd;
  static struct cmd_element no_neighbor_timers_connect_cmd;
  static struct cmd_element no_neighbor_timers_connect_val_cmd;
  static struct cmd_element neighbor_advertise_interval_cmd;
  static struct cmd_element no_neighbor_advertise_interval_cmd;
  static struct cmd_element no_neighbor_advertise_interval_val_cmd;
  static struct cmd_element neighbor_version_cmd;
  static struct cmd_element no_neighbor_version_cmd;
  static struct cmd_element neighbor_interface_cmd;
  static struct cmd_element no_neighbor_interface_cmd;
  static struct cmd_element neighbor_distribute_list_cmd;
  static struct cmd_element no_neighbor_distribute_list_cmd;
  static struct cmd_element neighbor_prefix_list_cmd;
  static struct cmd_element no_neighbor_prefix_list_cmd;
  static struct cmd_element neighbor_filter_list_cmd;
  static struct cmd_element no_neighbor_filter_list_cmd;
  static struct cmd_element neighbor_route_map_cmd;
  static struct cmd_element no_neighbor_route_map_cmd;
  static struct cmd_element neighbor_unsuppress_map_cmd;
  static struct cmd_element no_neighbor_unsuppress_map_cmd;
  static struct cmd_element neighbor_maximum_prefix_cmd;
  static struct cmd_element neighbor_maximum_prefix_threshold_cmd;
  static struct cmd_element neighbor_maximum_prefix_warning_cmd;
  static struct cmd_element neighbor_maximum_prefix_threshold_warning_cmd;
  static struct cmd_element no_neighbor_maximum_prefix_cmd;
  static struct cmd_element no_neighbor_maximum_prefix_val_cmd;
  static struct cmd_element no_neighbor_maximum_prefix_val2_cmd;
  static struct cmd_element no_neighbor_maximum_prefix_val3_cmd;
  static struct cmd_element neighbor_allowas_in_cmd;
  static struct cmd_element neighbor_allowas_in_arg_cmd;
  static struct cmd_element no_neighbor_allowas_in_cmd;
  static struct cmd_element address_family_ipv4_cmd;
  static struct cmd_element address_family_ipv4_safi_cmd;
  static struct cmd_element address_family_ipv6_unicast_cmd;
  static struct cmd_element address_family_ipv6_cmd;
  static struct cmd_element address_family_vpnv4_cmd;
  static struct cmd_element address_family_vpnv4_unicast_cmd;
  static struct cmd_element exit_address_family_cmd;
  static struct cmd_element clear_ip_bgp_all_cmd;
  static struct cmd_element clear_bgp_all_cmd;
  static struct cmd_element clear_bgp_ipv6_all_cmd;
  static struct cmd_element clear_ip_bgp_instance_all_cmd;
  static struct cmd_element clear_bgp_instance_all_cmd;
  static struct cmd_element clear_ip_bgp_peer_cmd;
  static struct cmd_element clear_bgp_peer_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_cmd;
  static struct cmd_element clear_bgp_peer_group_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_group_cmd;
  static struct cmd_element clear_ip_bgp_external_cmd;
  static struct cmd_element clear_bgp_external_cmd;
  static struct cmd_element clear_bgp_ipv6_external_cmd;
  static struct cmd_element clear_ip_bgp_as_cmd;
  static struct cmd_element clear_bgp_as_cmd;
  static struct cmd_element clear_bgp_ipv6_as_cmd;
  static struct cmd_element clear_ip_bgp_all_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_all_out_cmd;
  static struct cmd_element clear_ip_bgp_instance_all_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_all_ipv4_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_all_ipv4_out_cmd;
  static struct cmd_element clear_ip_bgp_instance_all_ipv4_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_all_vpnv4_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_all_vpnv4_out_cmd;
  static struct cmd_element clear_bgp_all_soft_out_cmd;
  static struct cmd_element clear_bgp_instance_all_soft_out_cmd;
  static struct cmd_element clear_bgp_all_out_cmd;
  static struct cmd_element clear_bgp_ipv6_all_soft_out_cmd;
  static struct cmd_element clear_bgp_ipv6_all_out_cmd;
  static struct cmd_element clear_ip_bgp_peer_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_peer_out_cmd;
  static struct cmd_element clear_ip_bgp_peer_ipv4_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_peer_ipv4_out_cmd;
  static struct cmd_element clear_ip_bgp_peer_vpnv4_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_peer_vpnv4_out_cmd;
  static struct cmd_element clear_bgp_peer_soft_out_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_soft_out_cmd;
  static struct cmd_element clear_bgp_peer_out_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_out_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_out_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_ipv4_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_ipv4_out_cmd;
  static struct cmd_element clear_bgp_peer_group_soft_out_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_group_soft_out_cmd;
  static struct cmd_element clear_bgp_peer_group_out_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_group_out_cmd;
  static struct cmd_element clear_ip_bgp_external_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_external_out_cmd;
  static struct cmd_element clear_ip_bgp_external_ipv4_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_external_ipv4_out_cmd;
  static struct cmd_element clear_bgp_external_soft_out_cmd;
  static struct cmd_element clear_bgp_ipv6_external_soft_out_cmd;
  static struct cmd_element clear_bgp_external_out_cmd;
  static struct cmd_element clear_bgp_ipv6_external_out_cmd;
  static struct cmd_element clear_ip_bgp_as_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_as_out_cmd;
  static struct cmd_element clear_ip_bgp_as_ipv4_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_as_ipv4_out_cmd;
  static struct cmd_element clear_ip_bgp_as_vpnv4_soft_out_cmd;
  static struct cmd_element clear_ip_bgp_as_vpnv4_out_cmd;
  static struct cmd_element clear_bgp_as_soft_out_cmd;
  static struct cmd_element clear_bgp_ipv6_as_soft_out_cmd;
  static struct cmd_element clear_bgp_as_out_cmd;
  static struct cmd_element clear_bgp_ipv6_as_out_cmd;
  static struct cmd_element clear_ip_bgp_all_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_instance_all_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_all_in_cmd;
  static struct cmd_element clear_ip_bgp_all_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_instance_all_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_all_ipv4_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_all_ipv4_in_cmd;
  static struct cmd_element clear_ip_bgp_instance_all_ipv4_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_all_ipv4_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_instance_all_ipv4_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_all_vpnv4_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_all_vpnv4_in_cmd;
  static struct cmd_element clear_bgp_all_soft_in_cmd;
  static struct cmd_element clear_bgp_instance_all_soft_in_cmd;
  static struct cmd_element clear_bgp_ipv6_all_soft_in_cmd;
  static struct cmd_element clear_bgp_all_in_cmd;
  static struct cmd_element clear_bgp_ipv6_all_in_cmd;
  static struct cmd_element clear_bgp_all_in_prefix_filter_cmd;
  static struct cmd_element clear_bgp_ipv6_all_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_peer_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_peer_in_cmd;
  static struct cmd_element clear_ip_bgp_peer_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_peer_ipv4_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_peer_ipv4_in_cmd;
  static struct cmd_element clear_ip_bgp_peer_ipv4_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_peer_vpnv4_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_peer_vpnv4_in_cmd;
  static struct cmd_element clear_bgp_peer_soft_in_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_soft_in_cmd;
  static struct cmd_element clear_bgp_peer_in_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_in_cmd;
  static struct cmd_element clear_bgp_peer_in_prefix_filter_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_in_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_ipv4_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_ipv4_in_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_ipv4_in_prefix_filter_cmd;
  static struct cmd_element clear_bgp_peer_group_soft_in_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_group_soft_in_cmd;
  static struct cmd_element clear_bgp_peer_group_in_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_group_in_cmd;
  static struct cmd_element clear_bgp_peer_group_in_prefix_filter_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_group_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_external_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_external_in_cmd;
  static struct cmd_element clear_ip_bgp_external_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_external_ipv4_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_external_ipv4_in_cmd;
  static struct cmd_element clear_ip_bgp_external_ipv4_in_prefix_filter_cmd;
  static struct cmd_element clear_bgp_external_soft_in_cmd;
  static struct cmd_element clear_bgp_ipv6_external_soft_in_cmd;
  static struct cmd_element clear_bgp_external_in_cmd;
  static struct cmd_element clear_bgp_ipv6_external_in_cmd;
  static struct cmd_element clear_bgp_external_in_prefix_filter_cmd;
  static struct cmd_element clear_bgp_ipv6_external_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_as_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_as_in_cmd;
  static struct cmd_element clear_ip_bgp_as_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_as_ipv4_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_as_ipv4_in_cmd;
  static struct cmd_element clear_ip_bgp_as_ipv4_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_as_vpnv4_soft_in_cmd;
  static struct cmd_element clear_ip_bgp_as_vpnv4_in_cmd;
  static struct cmd_element clear_bgp_as_soft_in_cmd;
  static struct cmd_element clear_bgp_ipv6_as_soft_in_cmd;
  static struct cmd_element clear_bgp_as_in_cmd;
  static struct cmd_element clear_bgp_ipv6_as_in_cmd;
  static struct cmd_element clear_bgp_as_in_prefix_filter_cmd;
  static struct cmd_element clear_bgp_ipv6_as_in_prefix_filter_cmd;
  static struct cmd_element clear_ip_bgp_all_soft_cmd;
  static struct cmd_element clear_ip_bgp_instance_all_soft_cmd;
  static struct cmd_element clear_ip_bgp_all_ipv4_soft_cmd;
  static struct cmd_element clear_ip_bgp_instance_all_ipv4_soft_cmd;
  static struct cmd_element clear_ip_bgp_all_vpnv4_soft_cmd;
  static struct cmd_element clear_bgp_all_soft_cmd;
  static struct cmd_element clear_bgp_instance_all_soft_cmd;
  static struct cmd_element clear_bgp_ipv6_all_soft_cmd;
  static struct cmd_element clear_ip_bgp_peer_soft_cmd;
  static struct cmd_element clear_ip_bgp_peer_ipv4_soft_cmd;
  static struct cmd_element clear_ip_bgp_peer_vpnv4_soft_cmd;
  static struct cmd_element clear_bgp_peer_soft_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_soft_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_soft_cmd;
  static struct cmd_element clear_ip_bgp_peer_group_ipv4_soft_cmd;
  static struct cmd_element clear_bgp_peer_group_soft_cmd;
  static struct cmd_element clear_bgp_ipv6_peer_group_soft_cmd;
  static struct cmd_element clear_ip_bgp_external_soft_cmd;
  static struct cmd_element clear_ip_bgp_external_ipv4_soft_cmd;
  static struct cmd_element clear_bgp_external_soft_cmd;
  static struct cmd_element clear_bgp_ipv6_external_soft_cmd;
  static struct cmd_element clear_ip_bgp_as_soft_cmd;
  static struct cmd_element clear_ip_bgp_as_ipv4_soft_cmd;
  static struct cmd_element clear_ip_bgp_as_vpnv4_soft_cmd;
  static struct cmd_element clear_bgp_as_soft_cmd;
  static struct cmd_element clear_bgp_ipv6_as_soft_cmd;
  static struct cmd_element show_ip_bgp_summary_cmd;
  static struct cmd_element show_ip_bgp_instance_summary_cmd;
  static struct cmd_element show_ip_bgp_ipv4_summary_cmd;
  static struct cmd_element show_ip_bgp_instance_ipv4_summary_cmd;
  static struct cmd_element show_ip_bgp_vpnv4_all_summary_cmd;
  static struct cmd_element show_bgp_summary_cmd;
  static struct cmd_element show_bgp_instance_summary_cmd;
  static struct cmd_element show_bgp_ipv6_summary_cmd;
  static struct cmd_element show_bgp_instance_ipv6_summary_cmd;
  static struct cmd_element show_ipv6_bgp_summary_cmd;
  static struct cmd_element show_ipv6_mbgp_summary_cmd;
  static struct cmd_element show_ip_bgp_neighbors_cmd;
  static struct cmd_element show_ip_bgp_ipv4_neighbors_cmd;
  static struct cmd_element show_ip_bgp_vpnv4_all_neighbors_cmd;
  static struct cmd_element show_ip_bgp_vpnv4_rd_neighbors_cmd;
  static struct cmd_element show_bgp_neighbors_cmd;
  static struct cmd_element show_bgp_ipv6_neighbors_cmd;
  static struct cmd_element show_ip_bgp_neighbors_peer_cmd;
  static struct cmd_element show_ip_bgp_ipv4_neighbors_peer_cmd;
  static struct cmd_element show_ip_bgp_vpnv4_all_neighbors_peer_cmd;
  static struct cmd_element show_ip_bgp_vpnv4_rd_neighbors_peer_cmd;
  static struct cmd_element show_bgp_neighbors_peer_cmd;
  static struct cmd_element show_bgp_ipv6_neighbors_peer_cmd;
  static struct cmd_element show_ip_bgp_instance_neighbors_cmd;
  static struct cmd_element show_ip_bgp_instance_neighbors_peer_cmd;
  static struct cmd_element show_ip_bgp_paths_cmd;
  static struct cmd_element show_ip_bgp_ipv4_paths_cmd;
  static struct cmd_element show_ip_bgp_community_info_cmd;
  static struct cmd_element show_ip_bgp_attr_info_cmd;
  static struct cmd_element bgp_redistribute_ipv4_cmd;
  static struct cmd_element bgp_redistribute_ipv4_rmap_cmd;
  static struct cmd_element bgp_redistribute_ipv4_metric_cmd;
  static struct cmd_element bgp_redistribute_ipv4_rmap_metric_cmd;
  static struct cmd_element bgp_redistribute_ipv4_metric_rmap_cmd;
  static struct cmd_element no_bgp_redistribute_ipv4_cmd;
  static struct cmd_element no_bgp_redistribute_ipv4_rmap_cmd;
  static struct cmd_element no_bgp_redistribute_ipv4_metric_cmd;
  static struct cmd_element no_bgp_redistribute_ipv4_rmap_metric_cmd;
  static struct cmd_element no_bgp_redistribute_ipv4_metric_rmap_cmd;
  static struct cmd_element bgp_redistribute_ipv6_cmd;
  static struct cmd_element bgp_redistribute_ipv6_rmap_cmd;
  static struct cmd_element bgp_redistribute_ipv6_metric_cmd;
  static struct cmd_element bgp_redistribute_ipv6_rmap_metric_cmd;
  static struct cmd_element bgp_redistribute_ipv6_metric_rmap_cmd;
  static struct cmd_element no_bgp_redistribute_ipv6_cmd;
  static struct cmd_element no_bgp_redistribute_ipv6_rmap_cmd;
  static struct cmd_element no_bgp_redistribute_ipv6_metric_cmd;
  static struct cmd_element no_bgp_redistribute_ipv6_rmap_metric_cmd;
  static struct cmd_element no_bgp_redistribute_ipv6_metric_rmap_cmd;
  static struct cmd_element ip_community_list_standard_cmd;
  static struct cmd_element ip_community_list_standard2_cmd;
  static struct cmd_element ip_community_list_expanded_cmd;
  static struct cmd_element ip_community_list_name_standard_cmd;
  static struct cmd_element ip_community_list_name_standard2_cmd;
  static struct cmd_element ip_community_list_name_expanded_cmd;
  static struct cmd_element no_ip_community_list_standard_cmd;
  static struct cmd_element no_ip_community_list_expanded_cmd;
  static struct cmd_element no_ip_community_list_name_standard_cmd;
  static struct cmd_element no_ip_community_list_name_expanded_cmd;
  static struct cmd_element show_ip_community_list_cmd;
  static struct cmd_element show_ip_community_list_arg_cmd;
  static struct cmd_element ip_extcommunity_list_standard_cmd;
  static struct cmd_element ip_extcommunity_list_standard2_cmd;
  static struct cmd_element ip_extcommunity_list_expanded_cmd;
  static struct cmd_element ip_extcommunity_list_name_standard_cmd;
  static struct cmd_element ip_extcommunity_list_name_standard2_cmd;
  static struct cmd_element ip_extcommunity_list_name_expanded_cmd;
  static struct cmd_element no_ip_extcommunity_list_all_cmd;
  static struct cmd_element no_ip_extcommunity_list_name_all_cmd;
  static struct cmd_element no_ip_extcommunity_list_standard_cmd;
  static struct cmd_element no_ip_extcommunity_list_expanded_cmd;
  static struct cmd_element no_ip_extcommunity_list_name_standard_cmd;
  static struct cmd_element no_ip_extcommunity_list_name_expanded_cmd;
  static struct cmd_element show_ip_extcommunity_list_cmd;
  static struct cmd_element show_ip_extcommunity_list_arg_cmd;

  static struct cmd_element show_ip_access_list_cmd;
  static struct cmd_element show_ip_access_list_name_cmd;
  static struct cmd_element access_list_any_cmd;
  static struct cmd_element no_access_list_any_cmd;
  static struct cmd_element access_list_standard_cmd;
  static struct cmd_element access_list_standard_nomask_cmd;
  static struct cmd_element access_list_standard_host_cmd;
  static struct cmd_element access_list_standard_any_cmd;
  static struct cmd_element no_access_list_standard_cmd;
  static struct cmd_element no_access_list_standard_nomask_cmd;
  static struct cmd_element no_access_list_standard_host_cmd;
  static struct cmd_element no_access_list_standard_any_cmd;
  static struct cmd_element access_list_extended_cmd;
  static struct cmd_element access_list_extended_any_mask_cmd;
  static struct cmd_element access_list_extended_mask_any_cmd;
  static struct cmd_element access_list_extended_any_any_cmd;
  static struct cmd_element access_list_extended_host_mask_cmd;
  static struct cmd_element access_list_extended_mask_host_cmd;
  static struct cmd_element access_list_extended_host_host_cmd;
  static struct cmd_element access_list_extended_any_host_cmd;
  static struct cmd_element access_list_extended_host_any_cmd;
  static struct cmd_element no_access_list_extended_cmd;
  static struct cmd_element no_access_list_extended_any_mask_cmd;
  static struct cmd_element no_access_list_extended_mask_any_cmd;
  static struct cmd_element no_access_list_extended_any_any_cmd;
  static struct cmd_element no_access_list_extended_host_mask_cmd;
  static struct cmd_element no_access_list_extended_mask_host_cmd;
  static struct cmd_element no_access_list_extended_host_host_cmd;
  static struct cmd_element no_access_list_extended_any_host_cmd;
  static struct cmd_element no_access_list_extended_host_any_cmd;

  //static struct cmd_element ;
  
  
  static struct route_map_rule_cmd route_match_ip_address_cmd ; 
  static struct route_map_rule_cmd route_match_ip_next_hop_cmd ;    
  static struct route_map_rule_cmd route_match_ip_address_prefix_list_cmd ;
  static struct route_map_rule_cmd route_match_ip_next_hop_prefix_list_cmd ;
  static struct route_map_rule_cmd route_match_metric_cmd ; 
  static struct route_map_rule_cmd route_match_aspath_cmd ; 
  static struct route_map_rule_cmd route_match_community_cmd ;
  static struct route_map_rule_cmd route_set_aggregator_as_cmd ; 
  static struct route_map_rule_cmd route_set_aspath_prepend_cmd ; 
  static struct route_map_rule_cmd route_set_atomic_aggregate_cmd ; 
  static struct route_map_rule_cmd route_set_community_additive_cmd ; 
  static struct route_map_rule_cmd route_set_community_cmd ; 
  static struct route_map_rule_cmd route_set_community_delete_cmd ; 
  static struct route_map_rule_cmd route_set_ecommunity_rt_cmd; 
  static struct route_map_rule_cmd route_set_ecommunity_soo_cmd ;  
  static struct route_map_rule_cmd route_set_ip_nexthop_cmd ; 
  static struct route_map_rule_cmd route_set_local_pref_cmd ; 
  static struct route_map_rule_cmd route_set_metric_cmd ; 
  static struct route_map_rule_cmd route_set_origin_cmd ; 
  static struct route_map_rule_cmd route_set_originator_id_cmd; 
  static struct route_map_rule_cmd route_set_weight_cmd;
  static struct route_map_rule_cmd route_match_ecommunity_cmd;
  static struct route_map_rule_cmd route_match_origin_cmd;
  static struct route_map_rule_cmd route_set_vpnv4_nexthop_cmd;

  static char* progname;
  static struct message attr_str[16]; /*bgp_attr.c*/


  //bgpd.c

  static struct peer_flag_action peer_flag_action_list[] ;


  //vty.c

  static struct _vector* vtyvec;
 
  //following are from bgp_debug.c

  /* messages for BGP-4 status */
  static struct message bgp_status_msg[7] ;
  static int bgp_status_msg_max  ;

  /* BGP message type string. */
  static char *bgp_type_str[7] ;

  /* message for BGP-4 Notify */
  static struct message bgp_notify_msg[8] ;
  static int bgp_notify_msg_max  ;

  static struct message bgp_notify_head_msg[4] ;
  static int bgp_notify_head_msg_max ;

  static struct message bgp_notify_open_msg[8] ;
  static int bgp_notify_open_msg_max  ;

  static struct message bgp_notify_update_msg[12]; 
  static int bgp_notify_update_msg_max  ;

  /* Origin strings. */
  static char *bgp_origin_str[3] ;
  static char *bgp_origin_long_str[3] ;

  /* Finite State Machine structures from bgp_fsm.c */
  static struct fsm_struct FSM [BGP_STATUS_MAX - 1][BGP_EVENTS_MAX - 1] ;
  static char *bgp_event_str[14] ;
  static char *peer_down_str[];
  /* bgpd.c*/
  
  /* BGP process wide configuration.  */
  static struct bgp_master bgp_master;
  
  /* BGP process wide configuration pointer to export.  */
  static struct bgp_master *bm;
  
  /* BGP community-list.  */
  static struct community_list_handler *bgp_clist;
  
  /*from command.c*/
  
  /* Command vector which includes some level of command lists. Normally
     each daemon maintains each own cmdvec. */
  static struct _vector* cmdvec;
  static int cmdvec_init;

  static struct cmd_node config_node ;

  /*from log.c*/

  static char *zlog_proto_names[10];
  static char *zlog_priority[9];

  /*memory.c*/
  static struct message mstr[6] ; 
#ifdef MEMORY_LOG
  struct
  {
    char *name;
    unsigned long alloc;
    unsigned long t_malloc;
    unsigned long c_malloc;
    unsigned long t_calloc;
    unsigned long c_calloc;
    unsigned long t_realloc;
    unsigned long t_free;
    unsigned long c_strdup;
  } mstat [MTYPE_MAX];
#else
  struct
  {
    char *name;
    unsigned long alloc;
  } mstat [MTYPE_MAX];
#endif /* MTPYE_LOG */

  static struct memory_list memory_list_lib[28];
  static struct memory_list memory_list_bgp[43] ;
  static struct memory_list memory_list_separator[2];

  /*prefix.c*/
  
  static u_char maskbit[9] ;
  /*routemap.c*/

  /* Vector for route match rules. */
  static struct cmd_node rmap_node;
  static struct _vector* route_match_vec;
  /* Master list of route map. */
  struct route_map_list route_map_master ;
  /* Vector for route set rules. */
  static struct _vector* route_set_vec;

  /* BGP node structure. */
  static struct cmd_node bgp_dump_node ;

  /*vty.c*/

  static char* vty_cwd;
  static unsigned long vty_timeout_val;
  /* Configure lock. */
  static int vty_config;

  /*************************************************************************
                     per-instance variables from zebra
  *************************************************************************/
  
  /*filter.c*/
  struct access_master access_master_ipv4;

  /*prefix.c*/
  struct prefix_master prefix_master_ipv4;


  struct thread_master* master;

  //top level structure of aspath
  struct hash *aspath; /* defined in bgp_aspath.c*/
  
  struct hash *cluster_hash; /*bgp_attr.c*/
  
  //unknown transit attribute
  struct hash *transit_hash; /*bgp_attr.c*/

  //attribute hash routines
  struct hash *attrhash; /*bgp_attr.c*/

  struct community_list_master community_list_master; /*bgp_clist.c*/

  //Hash of community attribute
  struct hash *comhash; /*bgp_community.c*/

  //from bgp_damp.c

  /* Global variable to access damping configuration */
  struct bgp_damp_config bgp_damp_cfg;
  int reuse_array_offset ;
  struct thread *bgp_reuse_thread ;
  struct bgp_damp_config *prev_bgp_damp_cfg ;

  //from bgp_debug.c

  unsigned long conf_bgp_debug_fsm;
  unsigned long conf_bgp_debug_events;
  unsigned long conf_bgp_debug_packet;
  unsigned long conf_bgp_debug_filter;
  unsigned long conf_bgp_debug_keepalive;
  unsigned long conf_bgp_debug_update;
  unsigned long conf_bgp_debug_normal;

  unsigned long term_bgp_debug_fsm;
  unsigned long term_bgp_debug_events;
  unsigned long term_bgp_debug_packet;
  unsigned long term_bgp_debug_filter;
  unsigned long term_bgp_debug_keepalive;
  unsigned long term_bgp_debug_update;
  unsigned long term_bgp_debug_normal;

  //from bgp_route.c
  /*Static announcement peer*/
  struct peer *peer_self;
  struct bgp_table *bgp_distance_table; /*Not used in the simulation*/
  

  //from bgp_dump.c


    /* BGP packet dump output buffer. */
  struct stream *bgp_dump_obuf;

  /* BGP dump strucuture for 'dump bgp all' */
  struct bgp_dump bgp_dump_all;

  /* BGP dump structure for 'dump bgp updates' */
  struct bgp_dump bgp_dump_updates;

  /* BGP dump structure for 'dump bgp routes' */
  struct bgp_dump bgp_dump_routes;

  /* Dump whole BGP table is very heavy process.  */
  struct thread *t_bgp_dump_routes;

  //from bgp_ecommunity

  /* Hash of community attribute. */
  struct hash *ecomhash;

  /* ip as-path access-list 10 permit AS1. */
  //struct as_list_master as_list_master  ; //bgp_filter.c
  
  /*from bgpd.c*/
  struct llist* bgp_list;
  struct llist* peer_list;

  /*command.c*/
  struct host host;

  /*from log.c*/
  struct zlog *zlog_default ;
  
  /*distribute.c*/
  /* Hash of distribute list. */
  struct hash *disthash;
  struct llist *iflist;

  /***********************************************************************
                         member functions from zebra
  ***********************************************************************/

  /*bgp_aspath.c*/
  struct aspath * aspath_new ();
  void aspath_free (struct aspath *aspath);
  void aspath_unintern (struct aspath *aspath);
  char aspath_delimiter_char (u_char type, u_char which);
  char * aspath_make_str_count (struct aspath *as);
  struct aspath * aspath_intern (struct aspath *aspath);
  struct aspath * aspath_dup (struct aspath *aspath);
  void * aspath_hash_alloc (void *arg);
  struct aspath * aspath_parse (caddr_t pnt, int length);
  struct aspath * aspath_aggregate_segment_copy (struct aspath *aspath, struct assegment *seg,int i);
  struct assegment * aspath_aggregate_as_set_add (struct aspath *aspath, struct assegment *asset,as_t as);
  struct aspath *  aspath_aggregate (struct aspath *as1, struct aspath *as2);
  int aspath_firstas_check (struct aspath *aspath, as_t asno);
  int aspath_loop_check (struct aspath *aspath, as_t asno);
  int aspath_private_as_check (struct aspath *aspath);
  struct aspath * aspath_merge (struct aspath *as1, struct aspath *as2);
  struct aspath *aspath_prepend (struct aspath *as1, struct aspath *as2);
  struct aspath * aspath_add_one_as (struct aspath *aspath, as_t asno, u_char type);
  struct aspath * aspath_add_seq (struct aspath *aspath, as_t asno);
  int aspath_cmp_left (struct aspath *aspath1, struct aspath *aspath2);
  int aspath_cmp_left_confed (struct aspath *aspath1, struct aspath *aspath2);
  struct aspath * aspath_delete_confed_seq (struct aspath *aspath);
  struct aspath * aspath_add_confed_seq (struct aspath *aspath, as_t asno);
  void aspath_as_add (struct aspath *as, as_t asno);
  void aspath_segment_add (struct aspath *as, int type);
  struct aspath * aspath_empty();
  struct aspath * aspath_empty_get();
  unsigned long aspath_count();
  char * aspath_gettoken (char *buf, enum as_token *token, u_short *asno);
  struct aspath * aspath_str2aspath (char *str);
  unsigned int  aspath_key_make (void *aspath);
  int aspath_cmp (void *as1, void *as2);
  void aspath_init();
  const char * aspath_print (struct aspath *as);
  void aspath_print_vty (struct vty *vty, struct aspath *as);
  void aspath_show_all_iterator (struct hash_backet *backet, void *vty);
  void aspath_print_all_vty (struct vty *vty);

  /*bgp_attr.c*/
  
  void * cluster_hash_alloc (void *val);
  struct cluster_list * cluster_parse (caddr_t pnt, int length);
  int cluster_loop_check (struct cluster_list *cluster, struct in_addr originator);
  unsigned int cluster_hash_key_make (void *cluster);
  int cluster_hash_cmp (void *cluster1, void *cluster2);
  void cluster_free (struct cluster_list *cluster);
  struct cluster_list * cluster_dup (struct cluster_list *cluster);
  struct cluster_list * cluster_intern (struct cluster_list *cluster);
  void cluster_unintern (struct cluster_list *cluster);
  void cluster_init ();
  void transit_free (struct transit *transit);
  void * transit_hash_alloc (void *transit);
  struct transit * transit_intern (struct transit *transit);
  void transit_unintern (struct transit *transit);
  unsigned int transit_hash_key_make (void *transit);
  int transit_hash_cmp (void *transit1, void *transit2);
  void transit_init();
  unsigned int attrhash_key_make (void *attr);
  int attrhash_cmp (void *attr1, void *attr2);
  void attrhash_init();
  void attr_show_all_iterator (struct hash_backet *backet, void *vty);
  void attr_show_all (struct vty *vty);
  void * bgp_attr_hash_alloc (void *val);
  struct attr * bgp_attr_intern (struct attr *attr);
  struct attr * bgp_attr_default_set (struct attr *attr, u_char origin);
  struct attr * bgp_attr_default_intern (u_char origin);
  struct attr * bgp_attr_aggregate_intern (struct bgp *bgp, u_char origin,struct aspath *aspath,struct community *community, int as_set);
  void bgp_attr_unintern (struct attr *attr);
  void bgp_attr_flush (struct attr *attr);
  int bgp_attr_origin (struct peer *peer, bgp_size_t length,struct attr *attr, u_char flag, u_char *startp);
  int bgp_attr_aspath (struct peer *peer, bgp_size_t length,struct attr *attr, u_char flag, u_char *startp);
  int bgp_attr_nexthop (struct peer *peer, bgp_size_t length,struct attr *attr, u_char flag, u_char *startp);
  int bgp_attr_med (struct peer *peer, bgp_size_t length,struct attr *attr, u_char flag, u_char *startp);
  int bgp_attr_local_pref (struct peer *peer, bgp_size_t length,struct attr *attr, u_char flag);
  int bgp_attr_atomic (struct peer *peer, bgp_size_t length,struct attr *attr, u_char flag);
  int bgp_attr_aggregator (struct peer *peer, bgp_size_t length,struct attr *attr, u_char flag);
  int bgp_attr_community (struct peer *peer, bgp_size_t length,struct attr *attr, u_char flag);
  int bgp_attr_originator_id (struct peer *peer, bgp_size_t length,struct attr *attr, u_char flag);
  int bgp_attr_cluster_list (struct peer *peer, bgp_size_t length,struct attr *attr, u_char flag);
  int bgp_mp_reach_parse (struct peer *peer, bgp_size_t length, struct attr *attr,struct bgp_nlri *mp_update);
  int bgp_mp_unreach_parse (struct peer *peer, int length,struct bgp_nlri *mp_withdraw);
  int bgp_attr_ext_communities (struct peer *peer, bgp_size_t length,struct attr *attr, u_char flag);
  int bgp_attr_unknown (struct peer *peer, struct attr *attr, u_char flag,u_char type, bgp_size_t length, u_char *startp);
  int bgp_attr_parse (struct peer *peer, struct attr *attr, bgp_size_t size,struct bgp_nlri *mp_update, struct bgp_nlri *mp_withdraw);
  int bgp_attr_check (struct peer *peer, struct attr *attr);
  bgp_size_t bgp_packet_attribute (struct bgp *bgp, struct peer *peer,struct stream *s, struct attr *attr, struct prefix *p,afi_t afi, safi_t safi, struct peer *from,struct prefix_rd *prd, u_char *tag);
  bgp_size_t bgp_packet_withdraw (struct peer *peer, struct stream *s, struct prefix *p,afi_t afi, safi_t safi, struct prefix_rd *prd,u_char *tag);
  void bgp_attr_init();
  void bgp_dump_routes_attr (struct stream *s, struct attr *attr);

  /*bgp_advertise.c*/


  struct bgp_advertise * bgp_advertise_new();
  struct bgp_advertise_attr * baa_new ();
  void baa_free (struct bgp_advertise_attr *baa);
  void * baa_hash_alloc (void *ref);
  unsigned int baa_hash_key (void *baa);
  int baa_hash_cmp(void *baa1,void* baa2);
  void bgp_advertise_free (struct bgp_advertise *adv);
  void bgp_advertise_add (struct bgp_advertise_attr *baa,struct bgp_advertise *adv);
  void bgp_advertise_delete (struct bgp_advertise_attr *baa,struct bgp_advertise *adv);
  struct bgp_advertise_attr * bgp_advertise_intern (struct hash *hash, struct attr *attr);
  void bgp_advertise_unintern (struct hash *hash, struct bgp_advertise_attr *baa);
  void bgp_adj_out_free (struct bgp_adj_out *adj);
  
  int bgp_adj_out_lookup (struct peer *peer, struct prefix *p,afi_t afi, safi_t safi, struct bgp_node *rn);
  void bgp_adj_in_set (struct bgp_node *rn, struct peer *peer, struct attr *attr);
  struct bgp_advertise * bgp_advertise_clean (struct peer *peer, struct bgp_adj_out *adj,afi_t afi, safi_t safi);
  void bgp_adj_out_set (struct bgp_node *rn, struct peer *peer, struct prefix *p,struct attr *attr, afi_t afi, safi_t safi,struct bgp_info *binfo);
  void bgp_adj_out_unset (struct bgp_node *rn, struct peer *peer, struct prefix *p,afi_t afi, safi_t safi);
  void bgp_adj_out_remove (struct bgp_node *rn, struct bgp_adj_out *adj,struct peer *peer, afi_t afi, safi_t safi);
  void bgp_adj_in_remove (struct bgp_node *rn, struct bgp_adj_in *bai);
  void bgp_adj_in_unset (struct bgp_node *rn, struct peer *peer);
  void bgp_sync_init (struct peer *peer);
  void bgp_sync_delete (struct peer *peer);

  /*bgp_clist.c*/
  struct community_list_master * community_list_master_lookup (struct community_list_handler *ch, int style);
  struct community_entry * community_entry_new ();
  void community_entry_free (struct community_entry *entry);
  struct community_list * community_list_new ();
  void community_list_free (struct community_list *list);
  struct community_list * community_list_insert (struct community_list_handler *ch,char *name, int style);
  struct community_list * community_list_lookup (struct community_list_handler *ch,char *name, int style);
  struct community_list * community_list_get (struct community_list_handler *ch, char *name, int style);
  void community_list_delete (struct community_list *list);
  int community_list_empty_p (struct community_list *list);
  void community_list_entry_add (struct community_list *list,struct community_entry *entry);
  void community_list_entry_delete (struct community_list *list,struct community_entry *entry, int style);
  struct community_entry * community_list_entry_lookup (struct community_list *list, void *arg,int direct);
  int community_regexp_match (struct community *com, regex_t *reg);
  int ecommunity_regexp_match (struct ecommunity *ecom, regex_t *reg);
  int community_list_match (struct community *com, struct community_list *list);
  int ecommunity_list_match (struct ecommunity *ecom, struct community_list *list);
  int community_list_exact_match (struct community *com, struct community_list *list);
  int comval_regexp_match (u_int32_t comval, regex_t *reg);
  struct community * community_list_match_delete (struct community *com,struct community_list *clist);
  int community_list_dup_check (struct community_list *list,struct community_entry *New);
  int community_list_set (struct community_list_handler *ch,char *name, char *str, int direct, int style);
  int community_list_unset (struct community_list_handler *ch,char *name, char *str, int direct, int style);
  int extcommunity_list_set (struct community_list_handler *ch,char *name, char *str, int direct, int style);
  int extcommunity_list_unset (struct community_list_handler *ch,char *name, char *str, int direct, int style);
  struct community_list_handler * community_list_init ();
  void community_list_terminate (struct community_list_handler *ch);

  /*bgp_community.c*/
  
  struct community * community_new ();
  void community_free (struct community *com);
  void community_add_val (struct community *com, u_int32_t val);
  void community_del_val (struct community *com, u_int32_t *val);
  struct community * community_delete (struct community *com1, struct community *com2);
 // int community_compare (const void *a1, const void *a2);
  int community_include (struct community *com, u_int32_t val);
  u_int32_t community_val_get (struct community *com, int i);
  struct community * community_uniq_sort (struct community *com);
  char * community_com2str  (struct community *com);
  struct community * community_intern (struct community *com);
  void community_unintern (struct community *com);
  struct community * community_parse (char *pnt, u_short length);
  struct community * community_dup (struct community *com);
  char * community_str (struct community *com);
  unsigned int community_hash_make (void *com);
  int community_match (struct community *com1, struct community *com2);
  int community_cmp (void *c1, void *com2);
  struct community * community_merge (struct community *com1, struct community *com2);
  char * community_gettoken (char *buf, enum community_token *token, u_int32_t *val);
  struct community * community_str2com (char *str);
  unsigned long community_count ();
  struct hash * community_hash ();
  void community_init ();


  /*bgp_ecommunity.c*/
  struct ecommunity * ecommunity_new ();
  void ecommunity_free (struct ecommunity *ecom);
  int ecommunity_add_val (struct ecommunity *ecom, struct ecommunity_val *eval);
  struct ecommunity * ecommunity_uniq_sort (struct ecommunity *ecom);
  struct ecommunity * ecommunity_parse (char *pnt, u_short length);
  struct ecommunity * ecommunity_dup (struct ecommunity *ecom);
  char * ecommunity_str (struct ecommunity *ecom);
  struct ecommunity * ecommunity_merge (struct ecommunity *ecom1, struct ecommunity *ecom2);
  struct ecommunity * ecommunity_intern (struct ecommunity *ecom);
  void ecommunity_unintern (struct ecommunity *ecom);
  unsigned int ecommunity_hash_make (void *ecom);
  int ecommunity_cmp (void *ecom1, void *ecom2);
  void ecommunity_init ();
  char * ecommunity_gettoken (char *str, struct ecommunity_val *eval,enum ecommunity_token *token);
  struct ecommunity * ecommunity_str2com (char *str, int type, int keyword_included);
  char * ecommunity_ecom2str (struct ecommunity *ecom, int format);
  int ecommunity_match (struct ecommunity *ecom1, struct ecommunity *ecom2);
  
  /*bgp_filter.c*/
  struct as_filter * as_filter_new ();
  void as_filter_free (struct as_filter *asfilter);
  struct as_filter *  as_filter_make (regex_t *reg, char *reg_str, enum as_filter_type type);
  struct as_filter * as_filter_lookup (struct as_list *aslist, char *reg_str,enum as_filter_type type);
  void as_list_filter_add (struct as_list *aslist, struct as_filter *asfilter);
  struct as_list * as_list_lookup (char *name);
  struct as_list * as_list_new ();
  void as_list_free (struct as_list *aslist);
  struct as_list * as_list_insert (char *name);
  struct as_list * as_list_get (char *name);
  char * filter_type_str (enum as_filter_type type);
  void  as_list_delete (struct as_list *aslist);
  int as_list_empty (struct as_list *aslist);
  void as_list_filter_delete (struct as_list *aslist, struct as_filter *asfilter);
  int as_filter_match (struct as_filter *asfilter, struct aspath *aspath);
  enum as_filter_type as_list_apply (struct as_list *aslist, void *object);
  void as_list_add_hook (void (BGP::*) ());
  void as_list_delete_hook (void (BGP::*) ());
  int as_list_dup_check (struct as_list *aslist, struct as_filter *New);
  void as_list_show (struct vty *vty, struct as_list *aslist);
  void as_list_show_all (struct vty *vty);
  int config_write_as_list (struct vty *vty);
  void bgp_filter_init ();

  
  int ip_as_path(struct cmd_element *,struct vty *,int,char **); 
  int no_ip_as_path(struct cmd_element *,struct vty *,int,char **);
  int no_ip_as_path_all(struct cmd_element *,struct vty *,int,char **);
  int show_ip_as_path_access_list(struct cmd_element *,struct vty *,int,char **);
  int show_ip_as_path_access_list_all(struct cmd_element *,struct vty *,int,char **);
  
  /*bgp_network.c*/
  int bgp_connect (struct peer *peer);
  void bgp_getsockname (struct peer *peer);

  /*bgp_open.c*/
  void bgp_capability_vty_out (struct vty *vty, struct peer *peer);
  int bgp_capability_mp (struct peer *peer, struct capability *cap);
  void bgp_capability_orf_not_support (struct peer *peer, afi_t afi, safi_t safi,u_char type, u_char mode);
  int bgp_capability_orf (struct peer *peer, struct capability *cap,u_char *pnt);
  int bgp_capability_parse (struct peer *peer, u_char *pnt, u_char length,u_char **error);
  int bgp_auth_parse (struct peer *peer, u_char *pnt, size_t length);
  int strict_capability_same (struct peer *peer);
  int bgp_open_option_parse (struct peer *peer, u_char length, int *capability);
  void bgp_open_capability_orf (struct stream *s, struct peer *peer,afi_t afi, safi_t safi, u_char code);
  void bgp_open_capability (struct stream *s, struct peer *peer);
  
  /*bgp_regex.c*/

  regex_t *  bgp_regcomp (char *regstr);
  int bgp_regexec (regex_t *regex, struct aspath *aspath);
  void bgp_regex_free (regex_t *regex);

  /*bgp_routemap.c*/

  route_map_result_t route_match_ip_address (void *, struct prefix *,route_map_object_t , void *);
  void *route_match_ip_address_compile (char *);
  void route_match_ip_address_free (void *);
  route_map_result_t route_match_ip_next_hop (void *rule, struct prefix *,route_map_object_t , void *);
  void * route_match_ip_next_hop_compile (char *);
  void route_match_ip_next_hop_free (void *);
  route_map_result_t route_match_ip_address_prefix_list (void *, struct prefix *,route_map_object_t , void *);
  void * route_match_ip_address_prefix_list_compile (char *);
  void route_match_ip_address_prefix_list_free (void *);
  route_map_result_t route_match_ip_next_hop_prefix_list (void *, struct prefix *,route_map_object_t , void *);
  void * route_match_ip_next_hop_prefix_list_compile (char *);
  void route_match_ip_next_hop_prefix_list_free (void *);
  route_map_result_t route_match_metric (void *, struct prefix *,route_map_object_t , void *);
  void * route_match_metric_compile (char *);
  void route_match_metric_free (void *);
  route_map_result_t route_match_aspath (void *, struct prefix *,route_map_object_t , void *);
  void * route_match_aspath_compile (char *);
  int route_match_aspath (void *, struct prefix *, void *);
  void route_match_aspath_free (void *);
  route_map_result_t route_match_community (void *, struct prefix *, route_map_object_t , void *);
  void * route_match_community_compile (char *);
  void route_match_community_free (void *);
  route_map_result_t route_match_ecommunity (void *, struct prefix *,route_map_object_t , void *);
  void * route_match_ecommunity_compile (char *);
  void route_match_ecommunity_free (void *);
  route_map_result_t route_match_origin (void *, struct prefix *,route_map_object_t, void *);
  void * route_match_origin_compile (char *);
  void route_match_origin_free (void *);
  route_map_result_t route_set_ip_nexthop (void *, struct prefix *,route_map_object_t , void *);
  void * route_set_ip_nexthop_compile (char *);
  void route_set_ip_nexthop_free (void *);
  route_map_result_t route_set_local_pref (void *, struct prefix *,route_map_object_t , void *);
  void * route_set_local_pref_compile (char *);
  void route_set_local_pref_free (void *);
  route_map_result_t route_set_weight (void *, struct prefix *, route_map_object_t ,void *);
  void * route_set_weight_compile (char *);
  void route_set_weight_free (void *);
  route_map_result_t route_set_metric (void *, struct prefix *,route_map_object_t , void *);
  void * route_set_metric_compile (char *);
  void route_set_metric_free (void *);
  route_map_result_t route_set_aspath_prepend (void *, struct prefix *, route_map_object_t , void *);
  void * route_set_aspath_prepend_compile (char *);
  void route_set_aspath_prepend_free (void *);
  route_map_result_t route_set_community (void *, struct prefix *,route_map_object_t , void *);
  void * route_set_community_compile (char *);
  void route_set_community_free (void *);
  route_map_result_t route_set_community_additive(void *,struct prefix *,route_map_object_t,void *);
  void *route_set_community_additive_compile(char *);
  void route_set_community_additive_free(void *);
  route_map_result_t route_set_community_delete (void *, struct prefix *,route_map_object_t, void *);
  void * route_set_community_delete_compile (char *);
  void route_set_community_delete_free (void *);
  route_map_result_t route_set_ecommunity_rt (void *, struct prefix *,route_map_object_t , void *);
  void * route_set_ecommunity_rt_compile (char *);
  void route_set_ecommunity_rt_free (void *);
  route_map_result_t route_set_ecommunity_soo (void *, struct prefix *,route_map_object_t , void *);
  void * route_set_ecommunity_soo_compile (char *);
  void route_set_ecommunity_soo_free (void *);
  route_map_result_t route_set_origin (void *, struct prefix *, route_map_object_t , void *);
  void * route_set_origin_compile (char *);
  void route_set_origin_free (void *);
  route_map_result_t route_set_atomic_aggregate (void *, struct prefix *,route_map_object_t , void *);
  void * route_set_atomic_aggregate_compile (char *);
  void route_set_atomic_aggregate_free (void *);
  route_map_result_t route_set_aggregator_as (void *, struct prefix *,route_map_object_t , void *);
  void * route_set_aggregator_as_compile (char *);
  void route_set_aggregator_as_free (void *);
  route_map_result_t route_match_ipv6_address (void *, struct prefix *,route_map_object_t , void *);
  void * route_match_ipv6_address_compile (char *);
  void route_match_ipv6_address_free (void *);
  route_map_result_t route_match_ipv6_next_hop (void *, struct prefix *, route_map_object_t , void *);
  void * route_match_ipv6_next_hop_compile (char *);
  void route_match_ipv6_next_hop_free (void *);
  route_map_result_t route_match_ipv6_address_prefix_list (void *, struct prefix *,route_map_object_t , void *);
  void * route_match_ipv6_address_prefix_list_compile (char *);
  void route_match_ipv6_address_prefix_list_free (void *);
  route_map_result_t route_set_ipv6_nexthop_global (void *, struct prefix *,route_map_object_t , void *);
  void * route_set_ipv6_nexthop_global_compile (char *);
  void route_set_ipv6_nexthop_global_free (void *);
  route_map_result_t route_set_ipv6_nexthop_local (void *, struct prefix *, route_map_object_t , void *);
  void * route_set_ipv6_nexthop_local_compile (char *);
  void route_set_ipv6_nexthop_local_free (void *);
  route_map_result_t route_set_vpnv4_nexthop (void *, struct prefix *,route_map_object_t , void *);
  void * route_set_vpnv4_nexthop_compile (char *);
  void route_set_vpnv4_nexthop_free (void *);
  route_map_result_t  route_set_originator_id (void *, struct prefix *, route_map_object_t , void *);
  void * route_set_originator_id_compile (char *);
  void route_set_originator_id_free (void *);
  int bgp_route_match_add (struct vty *, struct route_map_index *, char *, char *);
  int bgp_route_match_delete (struct vty *, struct route_map_index *,char *, char *);
  int bgp_route_set_add (struct vty *, struct route_map_index *, char *, char *);
  int bgp_route_set_delete (struct vty *, struct route_map_index *, char *, char *);
  void bgp_route_map_update (char *);
  void bgp_route_map_init ();

  
  int match_ip_address(struct cmd_element *,struct vty *,int,char **); 
  int no_match_ip_address(struct cmd_element *,struct vty *,int,char **); 
  int match_ip_next_hop(struct cmd_element *,struct vty *,int,char **); 
  int no_match_ip_next_hop(struct cmd_element *,struct vty *,int,char **);
  int match_ip_address_prefix_list(struct cmd_element *,struct vty *,int,char **); 
  int no_match_ip_address_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int match_ip_next_hop_prefix_list(struct cmd_element *,struct vty *,int,char **); 
  int no_match_ip_next_hop_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int match_metric(struct cmd_element *,struct vty *,int,char **); 
  int no_match_metric(struct cmd_element *,struct vty *,int,char **);
  int match_community(struct cmd_element *,struct vty *,int,char **); 
  int match_community_exact(struct cmd_element *,struct vty *,int,char **); 
  int no_match_community(struct cmd_element *,struct vty *,int,char **);
  int match_ecommunity(struct cmd_element *,struct vty *,int,char **); 
  int no_match_ecommunity(struct cmd_element *,struct vty *,int,char **);
  int match_aspath(struct cmd_element *,struct vty *,int,char **);
  int no_match_aspath(struct cmd_element *,struct vty *,int,char **);
  int match_origin(struct cmd_element *,struct vty *,int,char **);
  int no_match_origin(struct cmd_element *,struct vty *,int,char **);
  int set_ip_nexthop(struct cmd_element *,struct vty *,int,char **);
  int no_set_ip_nexthop(struct cmd_element *,struct vty *,int,char **);
  int set_metric(struct cmd_element *,struct vty *,int,char **);
  int no_set_metric(struct cmd_element *,struct vty *,int,char **);
  int set_local_pref(struct cmd_element *,struct vty *,int,char **);
  int no_set_local_pref(struct cmd_element *,struct vty *,int,char **);
  int set_weight(struct cmd_element *,struct vty *,int,char **);
  int no_set_weight(struct cmd_element *,struct vty *,int,char **);
  int set_aspath_prepend(struct cmd_element *,struct vty *,int,char **);
  int no_set_aspath_prepend(struct cmd_element *,struct vty *,int,char **);
  int set_community(struct cmd_element *,struct vty *,int,char **);
  int set_community_none(struct cmd_element *,struct vty *,int,char **);
  int no_set_community(struct cmd_element *,struct vty *,int,char **);
  int set_community_delete(struct cmd_element *,struct vty *,int,char **);
  int no_set_community_delete(struct cmd_element *,struct vty *,int,char **);
  int set_community_additive(struct cmd_element *,struct vty *,int,char **);
  int no_set_community_additive(struct cmd_element *,struct vty *,int,char **);
  int set_ecommunity_rt(struct cmd_element *,struct vty *,int,char **);
  int no_set_ecommunity_rt(struct cmd_element *,struct vty *,int,char **);
  int set_ecommunity_soo(struct cmd_element *,struct vty *,int,char **);
  int no_set_ecommunity_soo(struct cmd_element *,struct vty *,int,char **);
  int set_origin(struct cmd_element *,struct vty *,int,char **);
  int no_set_origin(struct cmd_element *,struct vty *,int,char **);
  int set_atomic_aggregate(struct cmd_element *,struct vty *,int,char **);
  int no_set_atomic_aggregate(struct cmd_element *,struct vty *,int,char **);
  int set_aggregator_as(struct cmd_element *,struct vty *,int,char **);
  int no_set_aggregator_as(struct cmd_element *,struct vty *,int,char **);
  int match_ipv6_address(struct cmd_element *,struct vty *,int,char **); 
  int no_match_ipv6_address(struct cmd_element *,struct vty *,int,char **); 
  int match_ipv6_next_hop(struct cmd_element *,struct vty *,int,char **); 
  int no_match_ipv6_next_hop(struct cmd_element *,struct vty *,int,char **);
  int match_ipv6_address_prefix_list(struct cmd_element *,struct vty *,int,char **); 
  int no_match_ipv6_address_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int set_ipv6_nexthop_global(struct cmd_element *,struct vty *,int,char **);
  int no_set_ipv6_nexthop_global(struct cmd_element *,struct vty *,int,char **);
  int set_ipv6_nexthop_local(struct cmd_element *,struct vty *,int,char **);
  int no_set_ipv6_nexthop_local(struct cmd_element *,struct vty *,int,char **);
  int set_vpnv4_nexthop(struct cmd_element *,struct vty *,int,char **);
  int no_set_vpnv4_nexthop(struct cmd_element *,struct vty *,int,char **);
  int set_originator_id(struct cmd_element *,struct vty *,int,char **);
  int no_set_originator_id(struct cmd_element *,struct vty *,int,char **);
  
  /*bgpd.h*/
  
  int bgp_option_set (int flag);
  int bgp_option_unset (int flag);
  int bgp_option_check (int flag);
  int bgp_flag_set (struct bgp *bgp, int flag);
  int bgp_flag_unset (struct bgp *bgp, int flag);
  int bgp_flag_check (struct bgp *bgp, int flag);
  void bgp_config_set (struct bgp *bgp, int config);
  void bgp_config_unset (struct bgp *bgp, int config);
  int bgp_config_check (struct bgp *bgp, int config);
  int bgp_router_id_set (struct bgp *bgp, struct in_addr *id);
  int bgp_router_id_unset (struct bgp *bgp);
  int bgp_cluster_id_set (struct bgp *bgp, struct in_addr *cluster_id);
  int bgp_cluster_id_unset (struct bgp *bgp);
  int bgp_timers_set (struct bgp *bgp, u_int32_t keepalive, u_int32_t holdtime);
  int bgp_timers_unset (struct bgp *bgp);
  int bgp_confederation_id_set (struct bgp *bgp, as_t as);
  int bgp_confederation_id_unset (struct bgp *bgp);
  int bgp_confederation_peers_check (struct bgp *bgp, as_t as);
  int bgp_confederation_peers_add (struct bgp *bgp, as_t as);
  int bgp_confederation_peers_remove (struct bgp *bgp, as_t as);
  int bgp_default_local_preference_set (struct bgp *bgp, u_int32_t local_pref);
  int bgp_default_local_preference_unset (struct bgp *bgp);
  int peer_cmp (void *p1,void *p2);
  int peer_af_flag_check (struct peer *peer, afi_t afi, safi_t safi, u_int32_t flag);
  void peer_af_flag_reset (struct peer *peer, afi_t afi, safi_t safi);
  void peer_global_config_reset (struct peer *peer);
  int peer_list_cmp(struct peer*,struct peer*);
  int peer_conf_cmp(void*,void*);
  struct peer_conf* peer_conf_new();
  void peer_conf_free(struct peer_conf*);
  void peer_conf_delete(struct peer_conf*);
  int peer_sort (struct peer *peer);
  struct peer * peer_new ();
  struct peer * peer_create (union sockunion *su, struct bgp *bgp, as_t local_as, as_t remote_as, afi_t afi, safi_t safi);
  struct peer * peer_create_accept (struct bgp *bgp);
  void peer_as_change (struct peer *peer, as_t as);
  int peer_remote_as (struct bgp *bgp, union sockunion *su, as_t *as,afi_t afi, safi_t safi);
  int peer_activate (struct peer *peer, afi_t afi, safi_t safi);
  int peer_deactivate (struct peer *peer, afi_t afi, safi_t safi);
  int peer_delete (struct peer *peer);
  int peer_group_cmp (void *g1, void *g2);
  int peer_group_active (struct peer *peer);
  struct peer_group * peer_group_new ();
  void peer_group_free (struct peer_group *group);
  struct peer_group * peer_group_lookup (struct bgp *bgp, char *name);
  struct peer_group * peer_group_get (struct bgp *bgp, char *name);
  void peer_group2peer_config_copy (struct peer_group *group, struct peer *peer,afi_t afi, safi_t safi);
  int peer_group_remote_as (struct bgp *bgp, char *group_name, as_t *as);
  int peer_group_delete (struct peer_group *group);
  int peer_group_remote_as_delete (struct peer_group *group);
  int peer_group_bind (struct bgp *bgp, union sockunion *su, struct peer_group *group, afi_t afi, safi_t safi, as_t *as);
  int peer_group_unbind (struct bgp *bgp, struct peer *peer,struct peer_group *group, afi_t afi, safi_t safi);
  struct bgp * bgp_create (as_t *as, char *name);
  struct bgp_master * bgp_get_master ();
  struct bgp * bgp_get_default ();
  struct bgp * bgp_lookup (as_t as, char *name);
  struct bgp * bgp_lookup_by_name (char *name);
  int bgp_get (struct bgp **bgp_val, as_t *as, char *name);
  int bgp_delete (struct bgp *bgp);
  struct peer * peer_lookup (struct bgp *bgp, union sockunion *su);
  struct peer * peer_lookup_by_su(union sockunion *su);
  struct peer * peer_lookup_with_open (union sockunion *su, as_t remote_as,struct in_addr *remote_id, int *as);
  int peer_active (struct peer *peer);
  int peer_active_nego (struct peer *peer);
  void peer_change_action (struct peer *peer, afi_t afi, safi_t safi,enum peer_change_type type);
  int peer_flag_action_set (struct peer_flag_action *action_list, int size,struct peer_flag_action *action, u_int32_t flag);
  void peer_flag_modify_action (struct peer *peer, u_int32_t flag);
  int peer_flag_modify (struct peer *peer, u_int32_t flag, int set);
  int peer_flag_set (struct peer *peer, u_int32_t flag);
  int peer_flag_unset (struct peer *peer, u_int32_t flag);
  int peer_is_group_member (struct peer *peer, afi_t afi, safi_t safi);
  int peer_af_flag_modify (struct peer *peer, afi_t afi, safi_t safi, u_int32_t flag,int set);
  int peer_af_flag_set (struct peer *peer, afi_t afi, safi_t safi, u_int32_t flag);
  int peer_af_flag_unset (struct peer *peer, afi_t afi, safi_t safi, u_int32_t flag);
  int peer_ebgp_multihop_set (struct peer *peer, int ttl);
  int peer_ebgp_multihop_unset (struct peer *peer);
  int peer_description_set (struct peer *peer, char *desc);
  int peer_description_unset (struct peer *peer);
  int  peer_update_source_if_set (struct peer *peer, char *ifname);
  int peer_update_source_addr_set (struct peer *peer, union sockunion *su);
  int peer_update_source_unset (struct peer *peer);
  int peer_default_originate_set (struct peer *peer, afi_t afi, safi_t safi,char *rmap);
  int peer_default_originate_unset (struct peer *peer, afi_t afi, safi_t safi);
  int peer_port_set (struct peer *peer, u_int16_t port);
  int peer_port_unset (struct peer *peer);
  int peer_weight_set (struct peer *peer, u_int16_t weight);
  int peer_weight_unset (struct peer *peer);
  int peer_timers_set (struct peer *peer, u_int32_t keepalive, u_int32_t holdtime);
  int peer_timers_unset (struct peer *peer);
  int peer_timers_connect_set (struct peer *peer, u_int32_t );
  int peer_timers_connect_unset (struct peer *peer);
  int peer_advertise_interval_set (struct peer *peer, u_int32_t routeadv);
  int peer_advertise_interval_unset (struct peer *peer);
  int peer_version_set (struct peer *peer, int version);
  int peer_version_unset (struct peer *peer);
  int peer_interface_set (struct peer *peer, char *str);
  int peer_interface_unset (struct peer *peer);
  int peer_allowas_in_set (struct peer *peer, afi_t afi, safi_t safi, int allow_num);
  int peer_allowas_in_unset (struct peer *peer, afi_t afi, safi_t safi);
  int peer_local_as_set (struct peer *peer, as_t as, int no_prepend);
  int peer_local_as_unset (struct peer *peer);
  int peer_distribute_set (struct peer *peer, afi_t afi, safi_t safi, int direct,char *name);
  int peer_distribute_unset (struct peer *peer, afi_t afi, safi_t safi, int direct);
  void peer_distribute_update (struct access_list *access);
  int peer_prefix_list_set (struct peer *peer, afi_t afi, safi_t safi, int direct,char *name);
  int peer_prefix_list_unset (struct peer *peer, afi_t afi, safi_t safi, int direct);
  void peer_prefix_list_update ();
  int peer_aslist_set (struct peer *peer, afi_t afi, safi_t safi, int direct,char *name);
  int peer_aslist_unset (struct peer *peer,afi_t afi, safi_t safi, int direct);
  void peer_aslist_update ();
  int peer_route_map_set (struct peer *peer, afi_t afi, safi_t safi, int direct,char *name);
  int peer_route_map_unset (struct peer *peer, afi_t afi, safi_t safi, int direct);
  int peer_unsuppress_map_set (struct peer *peer, afi_t afi, safi_t safi, char *name);
  int peer_unsuppress_map_unset (struct peer *peer, afi_t afi, safi_t safi);
  int peer_maximum_prefix_set (struct peer *peer, afi_t afi, safi_t safi,u_int32_t max, u_char threshold, int warning);
  int peer_maximum_prefix_unset (struct peer *peer, afi_t afi, safi_t safi);
  int peer_clear (struct peer *peer);
  int peer_clear_soft (struct peer *peer, afi_t afi, safi_t safi,enum bgp_clear_type stype);
  char * peer_uptime (time_t uptime2, char *buf, size_t len);
  struct peer_conf *peer_conf_lookup (struct bgp *bgp, union sockunion *su, int afi);
  struct peer_conf *peer_conf_lookup_vty (struct vty *vty,char *ip_str,int afi);
  struct peer_conf *peer_conf_lookup_existing (struct bgp *bgp, union sockunion *su);
  void bgp_config_write_filter (struct vty *vty, struct peer *peer,afi_t afi, safi_t safi);
  void bgp_config_write_peer (struct vty *vty, struct bgp *bgp,struct peer *peer, afi_t afi, safi_t safi);
  void bgp_config_write_family_header (struct vty *vty, afi_t afi, safi_t safi,int *write);
  int bgp_config_write_family (struct vty *vty, struct bgp *bgp, afi_t afi,safi_t safi);
  int bgp_config_write (struct vty *vty);
  void bgp_master_init ();
  void bgp_init ();
  

  /*bgp_damp.h*/

  int bgp_reuse_index (int penalty);
  void bgp_reuse_list_add (struct bgp_damp_info *bdi);
  void bgp_reuse_list_delete (struct bgp_damp_info *bdi);
  int bgp_damp_decay (Time_t tdiff, int penalty);
  int bgp_reuse_timer (struct thread *t);
  int bgp_damp_withdraw (struct bgp_info *binfo, struct bgp_node *rn,afi_t afi, safi_t safi, int attr_change);
  int bgp_damp_update (struct bgp_info *binfo, struct bgp_node *rn,afi_t afi, safi_t safi);
  int bgp_damp_scan (struct bgp_info *binfo, afi_t afi, safi_t safi);
  void bgp_damp_info_free (struct bgp_damp_info *bdi, int withdraw);
  void bgp_damp_parameter_set (int hlife, int reuse, int sup, int maxsup);
  int bgp_damp_enable (struct bgp *bgp, afi_t afi, safi_t safi, int half,unsigned int reuse, unsigned int suppress,unsigned int max);
  void bgp_damp_config_clean (struct bgp_damp_config *damp);
  void bgp_damp_info_clean ();
  int bgp_damp_disable (struct bgp *bgp, afi_t afi, safi_t safi);
  int bgp_config_write_damp (struct vty *vty);
  char * bgp_get_reuse_time (unsigned int penalty, char *buf, size_t len);
  void bgp_damp_info_vty (struct vty *vty, struct bgp_info *binfo) ;
  char * bgp_damp_reuse_time_vty (struct vty *vty, struct bgp_info *binfo);
  

  /*bgp_debug.c*/

  void bgp_dump_attr (struct peer *peer, struct attr *attr, char *buf, size_t size);
  void bgp_notify_print(struct peer *peer, struct bgp_notify *bgp_notify, char *direct);
  int debug (unsigned int option);
  int bgp_config_write_debug (struct vty *vty);
  void bgp_debug_init ();
  int debug_bgp_fsm(struct cmd_element *,struct vty *,int,char **); 
  int no_debug_bgp_fsm(struct cmd_element *,struct vty *,int,char **); 
  int debug_bgp_events(struct cmd_element *,struct vty *,int,char **); 
  int no_debug_bgp_events(struct cmd_element *,struct vty *,int,char **); 
  int debug_bgp_filter(struct cmd_element *,struct vty *,int,char **); 
  int no_debug_bgp_filter(struct cmd_element *,struct vty *,int,char **); 
  int no_debug_bgp_keepalive(struct cmd_element *,struct vty *,int,char **); 
  int debug_bgp_keepalive(struct cmd_element *,struct vty *,int,char **); 
  int debug_bgp_update(struct cmd_element *,struct vty *,int,char **); 
  int debug_bgp_update_direct(struct cmd_element *,struct vty *,int,char **); 
  int no_debug_bgp_update(struct cmd_element *,struct vty *,int,char **); 
  int debug_bgp_normal(struct cmd_element *,struct vty *,int,char **); 
  int no_debug_bgp_normal(struct cmd_element *,struct vty *,int,char **); 
  int no_debug_bgp_all(struct cmd_element *,struct vty *,int,char **); 
  int show_debugging_bgp(struct cmd_element *,struct vty *,int,char **); 

  /*bgp_dump.c*/

  FILE * bgp_dump_open_file (struct bgp_dump *bgp_dump);
  int bgp_dump_interval_add (struct bgp_dump *bgp_dump, int interval);
  void bgp_dump_header (struct stream *obuf, int type, int subtype);
  void bgp_dump_set_size (struct stream *s, int type);
  void bgp_dump_routes_entry (struct prefix *p, struct bgp_info *info, int afi,int type, unsigned int seq);
  void bgp_dump_routes_func (int afi);
  int bgp_dump_interval_func (struct thread *t);
  void bgp_dump_common (struct stream *obuf, struct peer *peer);
  void bgp_dump_state (struct peer *peer, int status_old, int status_new);
  void bgp_dump_packet_func (struct bgp_dump *bgp_dump, struct peer *peer,struct stream *packet);
  void bgp_dump_packet (struct peer *peer, int type, struct stream *packet);
  unsigned int bgp_dump_parse_time (char *str);
  int bgp_dump_set (struct vty *vty, struct bgp_dump *bgp_dump, int type,char *path, char *interval_str);
  int bgp_dump_unset (struct vty *vty, struct bgp_dump *bgp_dump);
  int config_write_bgp_dump (struct vty *vty);
  void bgp_dump_init ();


  int dump_bgp_all(struct cmd_element *,struct vty *,int,char **);
  int dump_bgp_all_interval(struct cmd_element *,struct vty *,int,char **);
  int no_dump_bgp_all(struct cmd_element *,struct vty *,int,char **);
  int dump_bgp_updates(struct cmd_element *,struct vty *,int,char **);
  int dump_bgp_updates_interval(struct cmd_element *,struct vty *,int,char **);
  int no_dump_bgp_updates(struct cmd_element *,struct vty *,int,char **);
  int dump_bgp_routes(struct cmd_element *,struct vty *,int,char **);
  int dump_bgp_routes_interval(struct cmd_element *,struct vty *,int,char **);
  int no_dump_bgp_routes(struct cmd_element *,struct vty *,int,char **);
  
  
  /*bgp_route.c*/


  struct bgp_node * bgp_afi_node_get (struct bgp *bgp, afi_t afi, safi_t safi, struct prefix *p,struct prefix_rd *prd);
  struct bgp_info * bgp_info_new ();
  void bgp_info_free (struct bgp_info *binfo);
  void bgp_info_add (struct bgp_node *rn, struct bgp_info *ri);
  void bgp_info_delete (struct bgp_node *rn, struct bgp_info *ri);
  u_int32_t bgp_med_value (struct attr *attr, struct bgp *bgp);
  int bgp_info_cmp (struct bgp *bgp, struct bgp_info *New, struct bgp_info *exist);
  enum filter_type bgp_input_filter (struct peer *peer, struct prefix *p, struct attr *attr,afi_t afi, safi_t safi);
  enum filter_type bgp_output_filter (struct peer *peer, struct prefix *p, struct attr *attr,afi_t afi, safi_t safi);
  int bgp_community_filter (struct peer *peer, struct attr *attr);
  int bgp_cluster_filter (struct peer *peer, struct attr *attr);
  int bgp_input_modifier (struct peer *peer, struct prefix *p, struct attr *attr,afi_t afi, safi_t safi);
  int bgp_adj_set(struct bgp_table *table, struct prefix *p, struct attr *attr,
                  struct prefix_rd *prd, safi_t safi);
  int bgp_adj_unset (struct bgp_table *table, struct prefix *p,
                    struct prefix_rd *prd, safi_t safi);
  int bgp_adj_lookup (struct bgp_table *table, struct prefix *p,
                     struct prefix_rd *prd, safi_t safi);
  void bgp_adj_clear (struct bgp_table *table, safi_t safi);
  int bgp_announce_check (struct bgp_info *ri, struct peer *peer, struct prefix *p,struct attr *attr, afi_t afi, safi_t safi);
#ifdef BGP_MRAI
  void bgp_refresh_rib (struct peer_conf *conf, afi_t afi, safi_t safi);
  void bgp_announce_table (struct peer *peer);
  void bgp_announce_rib (struct peer_conf *conf, afi_t afi, safi_t safi);
  void bgp_refresh_table (struct peer *peer, afi_t afi, safi_t safi);
  int bgp_process (struct bgp *bgp, struct bgp_node *rn, afi_t afi, safi_t safi,
                  struct bgp_info *del, struct prefix_rd *prd, u_char *tag);
#endif

#ifndef BGP_MRAI
  int bgp_process (struct bgp *bgp, struct bgp_node *rn, afi_t afi, safi_t safi);
#endif
  int bgp_maximum_prefix_overflow (struct peer *peer, afi_t afi, safi_t safi, int always);
  void bgp_rib_remove (struct bgp_node *rn, struct bgp_info *ri, struct peer *peer,afi_t afi, safi_t safi);
  void bgp_rib_withdraw (struct bgp_node *rn, struct bgp_info *ri, struct peer *peer,afi_t afi, safi_t safi, int force);
  /* from bgp_nexthop.c*/
int bgp_nexthop_self (afi_t afi, struct attr *attr);

  int bgp_update (struct peer *peer, struct prefix *p, struct attr *attr,afi_t afi, safi_t safi, int type, int sub_type,struct prefix_rd *prd, u_char *tag, int soft_reconfig);
  int bgp_withdraw (struct peer *peer, struct prefix *p, struct attr *attr,int afi, int safi, int type, int sub_type, struct prefix_rd *prd,u_char *tag);
  void bgp_default_originate (struct peer *peer, afi_t afi, safi_t safi, int withdraw);
  void bgp_announce_table (struct peer *peer, afi_t afi, safi_t safi,struct bgp_table *table);
  void bgp_announce_route (struct peer *peer, afi_t afi, safi_t safi);
  void bgp_announce_route_all (struct peer *peer);
  void bgp_soft_reconfig_table (struct peer *peer, afi_t afi, safi_t safi,struct bgp_table *table);
  void bgp_soft_reconfig_in (struct peer *peer, afi_t afi, safi_t safi);
  void bgp_route_clear_with_afi (struct peer *peer, struct bgp *bgp, afi_t afi,
                               safi_t safi);
  void bgp_route_clear (struct peer *peer);
  void bgp_clear_route_table (struct peer *peer, afi_t afi, safi_t safi,struct bgp_table *table);
  void bgp_clear_route (struct peer *peer, afi_t afi, safi_t safi);
  void bgp_clear_route_all (struct peer *peer);
  void bgp_clear_adj_in (struct peer *peer, afi_t afi, safi_t safi);
  void bgp_terminate ();
  void bgp_reset ();
  int bgp_nlri_parse (struct peer *peer, struct attr *attr, struct bgp_nlri *packet);
  int bgp_nlri_sanity_check (struct peer *peer, int afi, u_char *pnt,bgp_size_t length);
  struct bgp_static * bgp_static_new ();
  void bgp_static_free (struct bgp_static *bgp_static);
  void bgp_static_update (struct bgp *bgp, struct prefix *p,struct bgp_static *bgp_static, afi_t afi, safi_t safi);
  void bgp_static_update_vpnv4 (struct bgp *bgp, struct prefix *p, u_int16_t afi,u_char safi, struct prefix_rd *prd, u_char *tag);
  void bgp_static_withdraw (struct bgp *bgp, struct prefix *p, afi_t afi,safi_t safi);
  void bgp_static_withdraw_vpnv4 (struct bgp *bgp, struct prefix *p, u_int16_t afi,u_char safi, struct prefix_rd *prd, u_char *tag);
  int bgp_static_set (struct vty *vty, struct bgp *bgp, char *ip_str, u_int16_t afi,u_char safi, char *rmap, int backdoor);
  int bgp_static_unset (struct vty *vty, struct bgp *bgp, char *ip_str,u_int16_t afi, u_char safi);
  void bgp_static_delete (struct bgp *bgp);
  int bgp_static_set_vpnv4 (struct vty *vty, char *ip_str, char *rd_str,char *tag_str);
  int bgp_static_unset_vpnv4 (struct vty *vty, char *ip_str, char *rd_str,char *tag_str);
  struct bgp_aggregate * bgp_aggregate_new ();
  void bgp_aggregate_free (struct bgp_aggregate *aggregate);
  void bgp_aggregate_route (struct bgp *bgp, struct prefix *p, struct bgp_info *rinew,afi_t afi, safi_t safi, struct bgp_info *del,struct bgp_aggregate *aggregate);
  void bgp_aggregate_increment (struct bgp *bgp, struct prefix *p,struct bgp_info *ri, afi_t afi, safi_t safi);
  void bgp_aggregate_delete (struct bgp *, struct prefix *, afi_t, safi_t,struct bgp_aggregate *);
  void bgp_aggregate_decrement (struct bgp *bgp, struct prefix *p,struct bgp_info *del, afi_t afi, safi_t safi);
  void bgp_aggregate_add (struct bgp *bgp, struct prefix *p, afi_t afi, safi_t safi,struct bgp_aggregate *aggregate);
  int bgp_aggregate_set (struct vty *vty, char *prefix_str, afi_t afi, safi_t safi,u_char summary_only, u_char as_set);
  int bgp_aggregate_unset (struct vty *vty, char *prefix_str, afi_t afi, safi_t safi);
  void bgp_redistribute_add (struct prefix *p, struct in_addr *nexthop,u_int32_t metric, u_char type);
  void bgp_redistribute_delete (struct prefix *p, u_char type);
  void bgp_redistribute_withdraw (struct bgp *bgp, afi_t afi, int type);
  void route_vty_out_route (struct prefix *p, struct vty *vty);
  int vty_calc_line (struct vty *vty, unsigned long length);
  int route_vty_out (struct vty *vty, struct prefix *p,struct bgp_info *binfo, int display, safi_t safi);
  void route_vty_out_tmp (struct vty *vty, struct prefix *p,struct attr *attr, safi_t safi);
  int damp_route_vty_out (struct vty *vty, struct prefix *p,struct bgp_info *binfo, int display, safi_t safi);
  int flap_route_vty_out (struct vty *vty, struct prefix *p,struct bgp_info *binfo, int display, safi_t safi);
  void route_vty_out_detail (struct vty *vty, struct bgp *bgp, struct prefix *p,struct bgp_info *binfo, afi_t afi, safi_t safi);
  int bgp_show_callback (struct vty *vty, int unlock);
  int bgp_show (struct vty *vty, char *view_name, afi_t afi, safi_t safi,enum bgp_show_type type);
  int bgp_show_route (struct vty *vty, char *view_name, char *ip_str,afi_t afi, safi_t safi, struct prefix_rd *prd,int prefix_check);
  void bgp_show_regexp_clean (struct vty *vty);
  int bgp_show_regexp (struct vty *vty, int argc, char **argv, afi_t afi,safi_t safi, enum bgp_show_type type);
  int bgp_show_prefix_list (struct vty *vty, char *prefix_list_str, afi_t afi,safi_t safi, enum bgp_show_type type);
  int bgp_show_filter_list (struct vty *vty, char *filter, afi_t afi,safi_t safi, enum bgp_show_type type);
  int bgp_show_route_map (struct vty *vty, char *rmap_str, afi_t afi,safi_t safi, enum bgp_show_type type);
  int bgp_show_community (struct vty *vty, int argc, char **argv, int exact,u_int16_t afi, u_char safi);
  int bgp_show_community_list (struct vty *vty, char *com, int exact,u_int16_t afi, u_char safi);
  void bgp_show_prefix_longer_clean (struct vty *vty);
  int bgp_show_prefix_longer (struct vty *vty, char *prefix, afi_t afi,safi_t safi, enum bgp_show_type type);
  void show_adj_route (struct vty *vty, struct peer *peer, afi_t afi, safi_t safi,int in);
  int peer_adj_routes (struct vty *vty, char *ip_str, afi_t afi, safi_t safi, int in);
  void bgp_show_neighbor_route_clean (struct vty *vty);
  int bgp_show_neighbor_route (struct vty *vty, char *ip_str, afi_t afi,safi_t safi, enum bgp_show_type type);
  struct bgp_distance * bgp_distance_new ();
  void bgp_distance_free (struct bgp_distance *bdistance);
  int bgp_distance_set (struct vty *vty, char *distance_str, char *ip_str,char *access_list_str);
  int bgp_distance_unset (struct vty *vty, char *distance_str, char *ip_str,char *access_list_str);
  void bgp_distance_reset ();
  u_char bgp_distance_apply (struct prefix *p, struct bgp_info *rinfo, struct bgp *bgp);
  int bgp_clear_damp_route (struct vty *vty, char *view_name, char *ip_str,afi_t afi, safi_t safi, struct prefix_rd *prd,int prefix_check);
  int bgp_config_write_network (struct vty *vty, struct bgp *bgp,afi_t afi, safi_t safi, int *write);
  int bgp_config_write_distance (struct vty *vty, struct bgp *bgp);
  void bgp_route_init ();
 
  int bgp_network(struct cmd_element *,struct vty *,int,char **); 
  int bgp_network_route_map(struct cmd_element *,struct vty *,int,char **); 
  int bgp_network_backdoor(struct cmd_element *,struct vty *,int,char **); 
  int bgp_network_mask(struct cmd_element *,struct vty *,int,char **); 
  int bgp_network_mask_route_map(struct cmd_element *,struct vty *,int,char **); 
  int bgp_network_mask_backdoor(struct cmd_element *,struct vty *,int,char **); 
  int bgp_network_mask_natural(struct cmd_element *,struct vty *,int,char **); 
  int bgp_network_mask_natural_route_map(struct cmd_element *,struct vty *,int,char **); 
  int bgp_network_mask_natural_backdoor(struct cmd_element *,struct vty *,int,char **); 
  int no_bgp_network(struct cmd_element *,struct vty *,int,char **); 
  int no_bgp_network_mask(struct cmd_element *,struct vty *,int,char **); 
  int no_bgp_network_mask_natural(struct cmd_element *,struct vty *,int,char **); 
  int ipv6_bgp_netowrk(struct cmd_element *,struct vty *,int,char **); 
  int ipv6_bgp_network_route_map(struct cmd_element *,struct vty *,int,char **); 
  int no_ipv6_bgp_network(struct cmd_element *,struct vty *,int,char **); 
  int aggregate_address(struct cmd_element *,struct vty *,int,char **); 
  int aggregate_address_mask(struct cmd_element *,struct vty *,int,char **); 
  int aggregate_address_summary_only(struct cmd_element *,struct vty *,int,char **); 
  int aggregate_address_mask_summary_only(struct cmd_element *,struct vty *,int,char **); 
  int aggregate_address_as_set(struct cmd_element *,struct vty *,int,char **); 

  int aggregate_address_mask_as_set(struct cmd_element *,struct vty *,int,char **);
  int aggregate_address_as_set_summary(struct cmd_element *,struct vty *,int,char **);
  int aggregate_address_mask_as_set_summary(struct cmd_element *,struct vty *,int,char **);
  int no_aggregate_address(struct cmd_element *,struct vty *,int,char **);
  int no_aggregate_address_mask(struct cmd_element *,struct vty *,int,char **);
  int ipv6_aggregate_address(struct cmd_element *,struct vty *,int,char **);
  int ipv6_aggregate_address_summary_only(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_aggregate_address(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_aggregate_address_summary_only(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_route(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_route(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_vpnv4_all_route(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_vpnv4_rd_route(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_prefix(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_prefix(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_vpnv4_all_prefix(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_vpnv4_rd_prefix(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_view(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_view_route(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_view_prefix(struct cmd_element *,struct vty *,int,char **);
  int show_bgp(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_bgp(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_bgp_route(struct cmd_element *,struct vty *,int,char **);
  int show_bgp_prefix(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_bgp_prefix(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_mbgp(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_mbgp_route(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_mbgp_prefix(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_regexp(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_flap_regexp(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_ipv4_regexp(struct cmd_element *,struct vty *,int,char **); 
  int show_bgp_regexp(struct cmd_element *,struct vty *,int,char **); 
  int show_ipv6_bgp_regexp(struct cmd_element *,struct vty *,int,char **); 
  int show_ipv6_mbgp_regexp(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_prefix_list(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_flap_prefix_list(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_ipv4_prefix_list(struct cmd_element *,struct vty *,int,char **); 
  int show_bgp_prefix_list(struct cmd_element *,struct vty *,int,char **); 
  int show_ipv6_bgp_prefix_list(struct cmd_element *,struct vty *,int,char **); 
  int show_ipv6_mbgp_prefix_list(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_filter_list(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_flap_filter_list(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_ipv4_filter_list(struct cmd_element *,struct vty *,int,char **); 
  int show_bgp_filter_list(struct cmd_element *,struct vty *,int,char **); 
  int show_ipv6_bgp_filter_list(struct cmd_element *,struct vty *,int,char **); 
  int show_ipv6_mbgp_filter_list(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_route_map(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_flap_route_map(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_ipv4_route_map(struct cmd_element *,struct vty *,int,char **); 
  int show_bgp_route_map(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_cidr_only(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_flap_cidr_only(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_cidr_only(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_community_all(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_community_all(struct cmd_element *,struct vty *,int,char **);
  int show_bgp_community_all(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_bgp_community_all(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_mbgp_community_all(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_community(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_community(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_community_exact(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_community_exact(struct cmd_element *,struct vty *,int,char **);
  int show_bgp_community(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_bgp_community(struct cmd_element *,struct vty *,int,char **);
  int show_bgp_community_exact(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_bgp_community_exact(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_mbgp_community(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_mbgp_community_exact(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_community_list(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_community_list(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_community_list_exact(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_community_list_exact(struct cmd_element *,struct vty *,int,char **);
  int show_bgp_community_list(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_bgp_community_list(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_mbgp_community_list(struct cmd_element *,struct vty *,int,char **);
  int show_bgp_community_list_exact(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_bgp_community_list_exact(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_mbgp_community_list_exact(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_prefix_longer(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_flap_prefix_longer(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_prefix_longer(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_flap_address(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_flap_prefix(struct cmd_element *,struct vty *,int,char **);
  int show_bgp_prefix_longer(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_bgp_prefix_longer(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_mbgp_prefix_longer(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_neighbor_advertised_route(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_neighbor_advertised_route(struct cmd_element *,struct vty *,int,char **);
  int show_bgp_neighbor_advertised_route(struct cmd_element *,struct vty *,int,char **);
  int ipv6_bgp_neighbor_advertised_route(struct cmd_element *,struct vty *,int,char **);
  int ipv6_mbgp_neighbor_advertised_route(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_neighbor_received_routes(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_neighbor_received_routes(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_neighbor_received_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_neighbor_received_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int show_bgp_neighbor_received_routes(struct cmd_element *,struct vty *,int,char **);
  int show_bgp_neighbor_received_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int ipv6_bgp_neighbor_received_routes(struct cmd_element *,struct vty *,int,char **);
  int ipv6_mbgp_neighbor_received_routes(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_neighbor_routes(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_neighbor_flap(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_neighbor_damp(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_neighbor_routes(struct cmd_element *,struct vty *,int,char **);
  int show_bgp_neighbor_routes(struct cmd_element *,struct vty *,int,char **);
  int ipv6_bgp_neighbor_routes(struct cmd_element *,struct vty *,int,char **);
  int ipv6_mbgp_neighbor_routes(struct cmd_element *,struct vty *,int,char **);
  int bgp_distance(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_distance(struct cmd_element *,struct vty *,int,char **);
  int bgp_distance_source(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_distance_source(struct cmd_element *,struct vty *,int,char **);
  int bgp_distance_source_access_list(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_distance_source_access_list(struct cmd_element *,struct vty *,int,char **);
  int bgp_damp_set(struct cmd_element *,struct vty *,int,char **);
  int bgp_damp_unset(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_dampened_paths(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_flap_statistics(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_dampening(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_dampening_prefix(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_dampening_address(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_dampening_address_mask(struct cmd_element *,struct vty *,int,char **);
  
  /*bgp_fsm.c*/
  
  int bgp_start_jitter (int time);
  void bgp_timer_set (struct peer *peer);
  int bgp_start_timer (struct thread *thread);
  int bgp_connect_timer (struct thread *thread);
  int bgp_holdtime_timer (struct thread *thread);
  int bgp_keepalive_timer (struct thread *thread);
  int bgp_routeadv_timer (struct thread *thread);
  void bgp_uptime_reset (struct peer *peer);
  int bgp_stop (struct peer *peer);
  int bgp_stop_with_error (struct peer *peer);
  int bgp_connect_success (struct peer *peer);
  int bgp_connect_fail (struct peer *peer);
  int bgp_start (struct peer *peer);
  int bgp_reconnect (struct peer *peer);
  int bgp_fsm_open (struct peer *peer);
  void bgp_fsm_change_status (struct peer *peer, int status);
  int bgp_fsm_keepalive_expire (struct peer *peer);
  int bgp_fsm_holdtime_expire (struct peer *peer);
  int bgp_establish (struct peer *peer);
  int bgp_fsm_keepalive (struct peer *peer);
  int bgp_fsm_update (struct peer *peer);
  int bgp_ignore (struct peer *peer);
  int bgp_event (struct thread *thread);


  /*bgp_packet.c*/

  int peer_writen(struct peer* peer,char *ptr,int nbytes);
  int bgp_packet_set_marker (struct stream *s, u_char type);
  int bgp_packet_set_size (struct stream *s);
  void bgp_packet_add (struct peer *peer, struct stream *s);
  void bgp_packet_delete (struct peer *peer);
  struct stream * bgp_packet_dup (struct stream *s);
  void bgp_connect_check (struct peer *peer);
  struct stream * bgp_update_packet (struct peer *peer, afi_t afi, safi_t safi);
  struct stream * bgp_withdraw_packet (struct peer *peer, afi_t afi, safi_t safi);
  void bgp_update_send (struct peer *peer, struct prefix*,struct attr *attr,afi_t afi, safi_t safi, struct peer *from);
  void bgp_withdraw_send (struct peer *peer,struct prefix*, afi_t afi, safi_t safi);
  struct stream * bgp_write_packet (struct peer *peer);
  int bgp_write_proceed (struct peer *peer);
  int bgp_write (struct thread *thread);
  int bgp_write_notify (struct peer *peer);
  void bgp_keepalive_send (struct peer *peer);
  void bgp_open_send (struct peer *peer);
  void bgp_notify_send_with_data (struct peer *peer, u_char code, u_char sub_code,u_char *data, size_t datalen);
  void bgp_notify_send (struct peer *peer, u_char code, u_char sub_code);
  void bgp_route_refresh_send (struct peer *peer, afi_t afi, safi_t safi,u_char orf_type, u_char when_to_refresh, int remove);
  void bgp_capability_send (struct peer *peer, afi_t afi, safi_t safi,int capability_code, int action);
  int bgp_collision_detect (struct peer *New, struct in_addr remote_id);
  int bgp_open_receive (struct peer *peer, bgp_size_t size);
  int bgp_update_receive (struct peer *peer, bgp_size_t size);
  void bgp_notify_receive (struct peer *peer, bgp_size_t size);
  void bgp_keepalive_receive (struct peer *peer, bgp_size_t size);
  void bgp_route_refresh_receive (struct peer *peer, bgp_size_t size);
  int bgp_capability_msg_parse (struct peer *peer, u_char *pnt, bgp_size_t length);
  void bgp_capability_receive (struct peer *peer, bgp_size_t size);
  int bgp_read_packet (struct peer *peer,Data*);
  int bgp_marker_all_one (struct stream *s, int length);
  int bgp_read (struct thread *thread);
  char *afi2str (afi_t afi);
  char *safi2str (safi_t safi);
  
/*****bgp_table.c******/

  struct bgp_table * bgp_table_init (void);
  void bgp_table_finish (struct bgp_table *rt);
  void bgp_node_free (struct bgp_node *node);
  void bgp_table_free(struct bgp_table*);
  void route_common (struct prefix *n, struct prefix *p, struct prefix *New);
  int check_bit (u_char *prefix, u_char prefixlen);
  void set_link (struct bgp_node *node, struct bgp_node *New);
  struct bgp_node *bgp_lock_node (struct bgp_node *node);
  void bgp_unlock_node (struct bgp_node *node);
  struct bgp_node *bgp_node_match (struct bgp_table *table, struct prefix *p);
  struct bgp_node *bgp_node_match_ipv4 (struct bgp_table *table, struct in_addr *addr);
  struct bgp_node *bgp_node_lookup (struct bgp_table *table, struct prefix *p);
  struct bgp_node *bgp_node_get (struct bgp_table *table, struct prefix *p);
  void bgp_node_delete (struct bgp_node *node);
  struct bgp_node * bgp_table_top (struct bgp_table *table);
  struct bgp_node *bgp_route_next (struct bgp_node *node);
  struct bgp_node *bgp_route_next_until (struct bgp_node *node, struct bgp_node *limit);
  struct bgp_node *bgp_node_create ();
  struct bgp_node *bgp_node_set (struct bgp_table *table, struct prefix *prefix);
  
  

  /**** Zebra library functions ****/
  
  /*routemap.c*/
  void route_map_index_delete (struct route_map_index *, int);
  struct route_map * route_map_new (char *name);
  struct route_map * route_map_add (char *name);
  void route_map_delete (struct route_map *mmap);
  struct route_map * route_map_lookup_by_name (char *name);
  struct route_map * route_map_get (char *name);
  char * route_map_type_str (enum route_map_type type);
  int route_map_empty (struct route_map *mmap);
  void route_map_print ();
  struct route_map_index * route_map_index_new ();
  struct route_map_index * route_map_index_lookup (struct route_map *map, enum route_map_type type,int pref);
  struct route_map_index * route_map_index_add (struct route_map *mmap, enum route_map_type type,int pref);
  struct route_map_index * route_map_index_get (struct route_map *map, enum route_map_type type,int pref);
  struct route_map_rule * route_map_rule_new ();
  void route_map_install_match (struct route_map_rule_cmd *cmd);
  void route_map_install_set (struct route_map_rule_cmd *cmd);
  struct route_map_rule_cmd * route_map_lookup_match (char *name);
  struct route_map_rule_cmd * route_map_lookup_set (char *name);
  void route_map_rule_add (struct route_map_rule_list *list,struct route_map_rule *rule);
  void route_map_rule_delete (struct route_map_rule_list *list,struct route_map_rule *rule);
  int rulecmp (char *dst, char *src);
  int route_map_add_match (struct route_map_index *index, char *match_name,char *match_arg);
  int route_map_delete_match (struct route_map_index *index, char *match_name,char *match_arg);
  int route_map_add_set (struct route_map_index *index, char *set_name,char *set_arg);
  int route_map_delete_set (struct route_map_index *index, char *set_name,char *set_arg);
  route_map_result_t route_map_apply_index (struct route_map_index *index, struct prefix *prefix,route_map_object_t type, void *object);
  route_map_result_t route_map_apply (struct route_map *map, struct prefix *prefix,route_map_object_t type, void *object);
  void route_map_add_hook (void (BGP::*func) (char *));
  void route_map_delete_hook (void (BGP::*func) (char *));
  void route_map_event_hook (void (BGP::*func) (route_map_event_t, char *));
  void route_map_init ();
  int route_map_config_write (struct vty *vty);
  void route_map_init_vty ();
  
  
  int route_map(struct cmd_element *,struct vty *,int,char **);
  int no_route_map_all(struct cmd_element *,struct vty *,int,char **);
  int no_route_map(struct cmd_element *,struct vty *,int,char **);
  int rmap_onmatch_next(struct cmd_element *,struct vty *,int,char **);
  int no_rmap_onmatch_next(struct cmd_element *,struct vty *,int,char **);
  int rmap_onmatch_goto(struct cmd_element *,struct vty *,int,char **);
  int no_rmap_onmatch_goto(struct cmd_element *,struct vty *,int,char **);
  
 
  /*buffer.c*/
  
  struct buffer_data * buffer_data_new (size_t size);
  void buffer_data_free (struct buffer_data *d);
  struct buffer * buffer_new (size_t size);
  void buffer_free (struct buffer *b);
  char * buffer_getstr (struct buffer *b);
  int buffer_empty (struct buffer *b);
  void buffer_reset (struct buffer *b);
  void buffer_add (struct buffer *b);
  int buffer_write (struct buffer *b, u_char *ptr, size_t size);
  int buffer_putc (struct buffer *b, u_char c);
  int buffer_putw (struct buffer *b, u_short c);
  int buffer_putstr (struct buffer *b, u_char *c);
  void buffer_flush (struct buffer *b, int fd, size_t size);
  int buffer_flush_all (struct buffer *b, int fd);
  int buffer_flush_vty_all (struct buffer *b, int fd, int erase_flag,int no_more_flag);
  int buffer_flush_vty (struct buffer *b, int fd, unsigned int size,int erase_flag, int no_more_flag);
  int buffer_flush_window (struct buffer *b, int fd, int width, int height,int erase, int no_more);
  

  /*command.c*/

  char * argv_concat (char **argv, int argc, int shift);
  void install_node (struct cmd_node *node,int (BGP::*func) (struct vty *));
//  int cmp_node (const void *p, const void *q);
//  int cmp_desc (const void *p, const void *q);
  void sort_node ();
  struct _vector* cmd_make_strvec (char *string);
  void cmd_free_strvec (struct _vector* v);
  char * cmd_desc_str (char **string);
  struct _vector* cmd_make_descvec (char *string, char *descstr);
  int cmd_cmdsize (struct _vector *strvec);
  char * cmd_prompt (enum node_type node);
  void install_element (enum node_type ntype, struct cmd_element *cmd);
  void to64(char *s, long v, int n);
//  char *zencrypt (char *passwd);
  char *syslog_facility_print (int facility);
  int config_write_host (struct vty *vty);
  struct _vector* cmd_node_vector (struct _vector* v, enum node_type ntype);
  int cmd_filter_by_symbol (char *command, char *symbol);
  enum match_type cmd_ipv4_match (char *str);
  enum match_type cmd_ipv4_prefix_match (char *str);
  enum match_type cmd_ipv6_match (char *str);
  enum match_type cmd_ipv6_prefix_match (char *str);
  int cmd_range_match (char *range, char *str);
  enum match_type cmd_filter_by_completion (char *command,struct _vector* v, unsigned int index);
  enum match_type cmd_filter_by_string (char *command, struct _vector* v,unsigned int index);
  int is_cmd_ambiguous (char *command, struct _vector* v, int index, enum match_type type);
  char * cmd_entry_function (char *src, char *dst);
  char * cmd_entry_function_desc (char *src, char *dst);
  int cmd_unique_string (struct _vector* v, char *str);
  int desc_unique_string (struct _vector* v, char *str);
  struct _vector* cmd_describe_command (struct _vector* vline, struct vty *vty, int *status);
  int cmd_lcd (char **matched);
  char ** cmd_complete_command (struct _vector* vline, struct vty *vty, int *status);
  int cmd_execute_command (struct _vector* vline, struct vty *vty, struct cmd_element **cmd);
  int cmd_execute_command_strict (struct _vector* vline, struct vty *vty,struct cmd_element **cmd);
  int config_from_file (struct vty *vty, FILE *fp);
  void host_config_set (char *filename);
  void install_default (enum node_type node);
  void cmd_init(int );


  int config_terminal(struct cmd_element *,struct vty *,int,char **);
  int enable(struct cmd_element *,struct vty *,int,char **); 
  int disable(struct cmd_element *,struct vty *,int,char **); 
  int config_exit(struct cmd_element *,struct vty *,int,char **);
  int config_end(struct cmd_element *,struct vty *,int,char **);
  int show_version(struct cmd_element *,struct vty *,int,char **);
  int config_help(struct cmd_element *,struct vty *,int,char **);
  int config_list(struct cmd_element *,struct vty *,int,char **);
  int config_write_file(struct cmd_element *,struct vty *,int,char **); 
  int config_write_terminal(struct cmd_element *,struct vty *,int,char **);
  int show_startup_config(struct cmd_element *,struct vty *,int,char **);
  int config_hostname(struct cmd_element *,struct vty *,int,char **); 
  int config_no_hostname(struct cmd_element *,struct vty *,int,char **); 
  //int config_password(struct cmd_element *,struct vty *,int,char **); 
  int config_enable_password(struct cmd_element *,struct vty *,int,char**);
  int no_config_enable_password(struct cmd_element *,struct vty *,int,char **); 
  int service_password_encrypt(struct cmd_element *,struct vty *,int,char **);
  int no_service_password_encrypt(struct cmd_element *,struct vty *,int,char **);
  int config_terminal_length(struct cmd_element *,struct vty *,int,char **); 
  int config_terminal_no_length(struct cmd_element *,struct vty *,int,char **);
  int service_terminal_length(struct cmd_element *,struct vty *,int,char **);
  int no_service_terminal_length(struct cmd_element *,struct vty *,int,char **);
  int config_log_stdout(struct cmd_element *,struct vty *,int,char **);
  int no_config_log_stdout(struct cmd_element *,struct vty *,int,char **);
  int config_log_file(struct cmd_element *,struct vty *,int,char **);
  int no_config_log_file(struct cmd_element *,struct vty *,int,char **);
  int config_log_syslog(struct cmd_element *,struct vty *,int,char **);
  int config_log_syslog_facility(struct cmd_element *,struct vty *,int,char **);
  int no_config_log_syslog(struct cmd_element *,struct vty *,int,char **);
  int config_log_trap(struct cmd_element *,struct vty *,int,char **);
  int no_config_log_trap(struct cmd_element *,struct vty *,int,char **);
  int config_log_record_priority(struct cmd_element *,struct vty *,int,char **);
  int no_config_log_record_priority(struct cmd_element *,struct vty *,int,char **);
  int banner_motd_default(struct cmd_element *,struct vty *,int,char **);
  int no_banner_motd(struct cmd_element *,struct vty *,int,char **);
  
  /*Distribute.c*/
  
  struct distribute * distribute_new ();
  void distribute_free (void *dist);
  struct distribute * distribute_lookup (char *ifname);
  void distribute_list_add_hook (void (BGP::*func) (struct distribute *));
  void distribute_list_delete_hook (void (BGP::*func) (struct distribute *));
  void * distribute_hash_alloc (void *arg);
  struct distribute * distribute_get (char *ifname);
  unsigned int distribute_hash_make (void *dist);
  int distribute_cmp (void *dist1, void *dist2);
  struct distribute * distribute_list_set (char *ifname, enum distribute_type type, char *alist_name);
  int distribute_list_unset (char *ifname, enum distribute_type type,char *alist_name);
  struct distribute * distribute_list_prefix_set (char *ifname, enum distribute_type type,char *plist_name);
  int distribute_list_prefix_unset (char *ifname, enum distribute_type type,char *plist_name);
  int config_show_distribute (struct vty *vty);
  int config_write_distribute (struct vty *vty);
  void distribute_list_reset ();
  void distribute_list_init (int node);

  /*filter.c*/

  struct access_master * access_master_get (afi_t afi);
  struct filter * filter_new ();
  void filter_free (struct filter *filter);
  char * filter_type_str (struct filter *filter);
  int filter_match_cisco (struct filter *mfilter, struct prefix *p);
  int filter_match_zebra (struct filter *mfilter, struct prefix *p);
  struct access_list * access_list_new ();
  void access_list_free (struct access_list *access);
  void access_list_delete (struct access_list *access);
  struct access_list * access_list_insert (afi_t afi, char *name);
  struct access_list * access_list_lookup (afi_t afi, char *name);
  struct access_list * access_list_get (afi_t afi, char *name);
  enum filter_type access_list_apply (struct access_list *access, void *object);
  void access_list_add_hook (void (BGP::*func) (struct access_list *access));
  void access_list_delete_hook (void (BGP::*func) (struct access_list *access));
  void access_list_filter_add (struct access_list *access, struct filter *filter);
  int access_list_empty (struct access_list *access);
  void access_list_filter_delete (struct access_list *access, struct filter *filter);
  struct filter * filter_lookup_cisco (struct access_list *access, struct filter *mnew);
  struct filter * filter_lookup_zebra (struct access_list *access, struct filter *mnew);
  int vty_access_list_remark_unset (struct vty *vty, afi_t afi, char *name);
  int filter_set_cisco (struct vty *vty, char *name_str, char *type_str,char *addr_str, char *addr_mask_str,char *mask_str, char *mask_mask_str,int extended, int set);
  int filter_set_zebra (struct vty *vty, char *name_str, char *type_str,afi_t afi, char *prefix_str, int exact, int set);
  int filter_show (struct vty *vty, char *name, afi_t afi);
  void config_write_access_cisco (struct vty *vty, struct filter *mfilter);
  void config_write_access_zebra (struct vty *vty, struct filter *mfilter);
  int config_write_access (struct vty *vty, afi_t afi);
  int config_write_access_ipv4 (struct vty *vty);
  void access_list_reset_ipv4 ();
  void access_list_init_ipv4 ();
  int config_write_access_ipv6 (struct vty *vty);
  void access_list_reset_ipv6 ();
  void access_list_init_ipv6 ();
  void access_list_reset ();
  void access_list_init ();


  int access_list_standard(struct cmd_element *,struct vty *,int,char **);
  int access_list_standard_nomask(struct cmd_element *,struct vty *,int,char **);
  int access_list_standard_host(struct cmd_element *,struct vty *,int,char **);
  int access_list_standard_any(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_standard(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_standard_nomask(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_standard_host(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_standard_any(struct cmd_element *,struct vty *,int,char **);
  int access_list_extended(struct cmd_element *,struct vty *,int,char **);
  int access_list_extended_mask_any(struct cmd_element *,struct vty *,int,char **);
  int access_list_extended_any_mask(struct cmd_element *,struct vty *,int,char **);
  int access_list_extended_any_any(struct cmd_element *,struct vty *,int,char **);
  int access_list_extended_mask_host(struct cmd_element *,struct vty *,int,char **);
  int access_list_extended_host_mask(struct cmd_element *,struct vty *,int,char **);
  int access_list_extended_host_host(struct cmd_element *,struct vty *,int,char **);
  int access_list_extended_any_host(struct cmd_element *,struct vty *,int,char **);
  int access_list_extended_host_any(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_extended(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_extended_mask_any(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_extended_any_mask(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_extended_any_any(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_extended_mask_host(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_extended_host_mask(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_extended_host_host(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_extended_any_host(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_extended_host_any(struct cmd_element *,struct vty *,int,char **);
  int access_list(struct cmd_element *,struct vty *,int,char **);
  int access_list_exact(struct cmd_element *,struct vty *,int,char **);
  int access_list_any(struct cmd_element *,struct vty *,int,char **);
  int no_access_list(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_exact(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_any(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_all(struct cmd_element *,struct vty *,int,char **);
  int access_list_remark(struct cmd_element *,struct vty *,int,char **);
  int no_access_list_remark(struct cmd_element *,struct vty *,int,char **);
  int ipv6_access_list(struct cmd_element *,struct vty *,int,char **);
  int ipv6_access_list_exact(struct cmd_element *,struct vty *,int,char **);
  int ipv6_access_list_any(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_access_list(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_access_list_exact(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_access_list_any(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_access_list_all(struct cmd_element *,struct vty *,int,char **);
  int ipv6_access_list_remark(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_access_list_remark(struct cmd_element *,struct vty *,int,char **);
  int show_ip_access_list(struct cmd_element *,struct vty *,int,char **);
  int show_ip_access_list_name(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_access_list(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_access_list_name(struct cmd_element *,struct vty *,int,char **);
  
  /*memory.c*/
  
  void zerror (const char *fname, int type, size_t size);
  void * zmalloc (int type, size_t size);
  void * zcalloc (int type, size_t size);
  void * zrealloc (int type, void *ptr, size_t size);
  void zfree (int type, void *ptr);
  char * zstrdup (int type, char *str);
  void mtype_log (char *func, void *memory, const char *file, int line, int type);
  void * mtype_zmalloc (const char *file, int line, int type, size_t size);
  void * mtype_zcalloc (const char *file, int line, int type, size_t size);
  void * mtype_zrealloc (const char *file, int line, int type, void *ptr, size_t size);
  void  mtype_zfree (const char *file, int line, int type, void *ptr);
  char * mtype_zstrdup (const char *file, int line, int type, char *str);
  void alloc_inc (int type);
  void alloc_dec (int type);
  void show_memory_vty(struct vty *vty, struct memory_list *llist);
  void memory_init ();
  int show_memory_all(struct cmd_element*,struct vty*,int, char **);
  int show_memory_lib(struct cmd_element *,struct vty *,int,char **);
  int show_memory_bgp(struct cmd_element *,struct vty *,int,char **);
 
  /*hash.c*/

  struct hash * hash_create (unsigned int (BGP::*hash_key) (void*), int (BGP::*hash_cmp) (void *,void*));
  void * hash_alloc_intern (void *arg);
  void * hash_get (struct hash *hash, void *data, void * (BGP::*alloc_func) (void*));
  void * hash_lookup (struct hash *hash, void *data);
  void * hash_release (struct hash *hash, void *data);
  void hash_iterate (struct hash *hash,void (BGP::*func) (struct hash_backet *, void *), void *arg);
  void hash_clean (struct hash *hash, void (BGP::*free_func) (void *));
  void hash_free (struct hash *hash);

  /*linklist.c*/

  struct llist * list_new ();
  void list_free (struct llist *l);
  struct listnode * listnode_new ();
  void listnode_free (struct listnode *node);
  void listnode_add (struct llist *, void *val);
  void listnode_add_sort (struct llist *, void *val);
  void listnode_add_after (struct llist *, struct listnode *pp, void *val);
  void listnode_delete (struct llist *, void *val);
  void * listnode_head (struct llist *);
  void list_delete_all_node (struct llist *);
  void list_delete (struct llist *);
  struct listnode * listnode_lookup (struct llist *, void *data);
  void list_delete_node (struct llist*,struct listnode* node);
  void list_add_node_prev (struct llist *, struct listnode *current, void *val);
  void list_add_node_next (struct llist* , struct listnode *current, void *val);
  void list_add_list (struct llist *l, struct llist *m);

  /*log.c*/

  void time_print (FILE *fp);
  void vzlog (struct zlog *zl, int priority, const char *format, va_list *args);
  void zlog (struct zlog *zl, int priority, const char *format, ...);
  void zlog_err (const char *format, ...);
  void zlog_warn (const char *format, ...);
  void zlog_info (const char *format, ...);
  void zlog_notice (const char *format, ...);
  void zlog_debug (const char *format, ...);
  void plog_err (struct zlog *zl, const char *format, ...);
  void plog_warn (struct zlog *zl, const char *format, ...);
  void plog_info (struct zlog *zl, const char *format, ...);
  void plog_notice (struct zlog *zl, const char *format, ...);
  void plog_debug (struct zlog *zl, const char *format, ...);
  struct zlog * openzlog (const char *progname, int flags, zlog_proto_t protocol,int syslog_flags, int syslog_facility);
  void closezlog (struct zlog *zl);
  void zlog_set_flag (struct zlog *zl, int flags);
  void zlog_reset_flag (struct zlog *zl, int flags);
  int zlog_set_file (struct zlog *zl, int flags, char *filename);
  int zlog_reset_file (struct zlog *zl);
  int zlog_rotate (struct zlog *zl);
  void zlog_save_cwd ();
  char * zlog_get_cwd ();
  void zlog_free_cwd ();
  char * lookup (struct message *mes, int key);
  char * mes_lookup (struct message *meslist, int max, int index);



  /*network.c*/
  
  int readn (int fd, char *ptr, int nbytes);
  int writen(int fd, char *ptr, int nbytes);
  
  
  /*plist.c*/

  struct prefix_master * prefix_master_get (afi_t afi);
  struct prefix_list * prefix_list_lookup (afi_t afi, char *name);
  struct prefix_list * prefix_list_new ();
  void prefix_list_free (struct prefix_list *plist);
  struct prefix_list_entry * prefix_list_entry_new ();
  void prefix_list_entry_free (struct prefix_list_entry *pentry);
  struct prefix_list * prefix_list_insert (afi_t afi, char *name);
  struct prefix_list * prefix_list_get (afi_t afi, char *name);
  void prefix_list_delete (struct prefix_list *plist);
  struct prefix_list_entry * prefix_list_entry_make (struct prefix *prefix, enum prefix_list_type type,int seq, int le, int ge, int any);
  void prefix_list_add_hook (void (BGP::*func) ());
  void prefix_list_delete_hook (void (BGP::*func) ());
  int prefix_new_seq_get (struct prefix_list *plist);
  struct prefix_list_entry * prefix_seq_check (struct prefix_list *plist, int seq);
  struct prefix_list_entry * prefix_list_entry_lookup (struct prefix_list *plist, struct prefix *prefix,enum prefix_list_type type, int seq, int le, int ge);
  void prefix_list_entry_delete (struct prefix_list *plist,struct prefix_list_entry *pentry ,int update_list);
  void prefix_list_entry_add (struct prefix_list *plist,struct prefix_list_entry *pentry);
  char * prefix_list_type_str (struct prefix_list_entry *pentry);
  int prefix_list_entry_match (struct prefix_list_entry *pentry, struct prefix *p);
  enum prefix_list_type prefix_list_apply (struct prefix_list *plist, void *object);
  void prefix_list_print (struct prefix_list *plist);
  struct prefix_list_entry * prefix_entry_dup_check (struct prefix_list *plist,struct prefix_list_entry *New);
  int vty_invalid_prefix_range (struct vty *vty, char *prefix);
  int vty_prefix_list_install (struct vty *vty, afi_t afi,char *name, char *seq, char *typestr,char *prefix, char *ge, char *le);
  int vty_prefix_list_uninstall (struct vty *vty, afi_t afi,char *name, char *seq, char *typestr,char *prefix, char *ge, char *le);
  int vty_prefix_list_desc_unset (struct vty *vty, afi_t afi, char *name);
  void vty_show_prefix_entry (struct vty *vty, afi_t afi, struct prefix_list *plist,struct prefix_master *master, enum display_type dtype,int seqnum);
  int vty_show_prefix_list (struct vty *vty, afi_t afi, char *name,char *seq, enum display_type dtype);
  int vty_show_prefix_list_prefix (struct vty *vty, afi_t afi, char *name,char *prefix, enum display_type type);
  int vty_clear_prefix_list (struct vty *vty, afi_t afi, char *name, char *prefix);
  int config_write_prefix_afi (afi_t afi, struct vty *vty);
  struct stream * prefix_bgp_orf_entry (struct stream *s, struct prefix_list *plist,u_char init_flag, u_char permit_flag, u_char deny_flag);
  int prefix_bgp_orf_set (char *name, afi_t afi, struct orf_prefix *orfp,int permit, int set);
  void prefix_bgp_orf_remove_all (char *name);
  int prefix_bgp_show_prefix_list (struct vty *vty, afi_t afi, char *name);
  void prefix_list_reset_orf ();
  int config_write_prefix_ipv4 (struct vty *vty);
  void prefix_list_reset_ipv4 ();
  void prefix_list_init_ipv4 ();
  int config_write_prefix_ipv6 (struct vty *vty);
  void prefix_list_reset_ipv6 ();
  void prefix_list_init_ipv6 ();
  void prefix_list_init ();
  void prefix_list_reset ();


  int ip_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int ip_prefix_list_ge(struct cmd_element *,struct vty *,int,char **);
  int ip_prefix_list_ge_le(struct cmd_element *,struct vty *,int,char **);
  int ip_prefix_list_le(struct cmd_element *,struct vty *,int,char **);
  int ip_prefix_list_le_ge(struct cmd_element *,struct vty *,int,char **);
  int ip_prefix_list_seq(struct cmd_element *,struct vty *,int,char **);
  int ip_prefix_list_seq_ge(struct cmd_element *,struct vty *,int,char **);
  int ip_prefix_list_seq_ge_le(struct cmd_element *,struct vty *,int,char **);
  int ip_prefix_list_seq_le(struct cmd_element *,struct vty *,int,char **);
  int ip_prefix_list_seq_le_ge(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list_prefix(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list_ge(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list_ge_le(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list_le(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list_le_ge(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list_seq(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list_seq_ge(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list_seq_ge_le(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list_seq_le(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list_seq_le_ge(struct cmd_element *,struct vty *,int,char **);
  int ip_prefix_list_sequence_number(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list_sequence_number(struct cmd_element *,struct vty *,int,char **);
  int ip_prefix_list_description(struct cmd_element *,struct vty *,int,char **);
  int no_ip_prefix_list_description(struct cmd_element *,struct vty *,int,char **);
  int show_ip_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int show_ip_prefix_list_name(struct cmd_element *,struct vty *,int,char **);
  int show_ip_prefix_list_name_seq(struct cmd_element *,struct vty *,int,char **);
  int show_ip_prefix_list_prefix(struct cmd_element *,struct vty *,int,char **);
  int show_ip_prefix_list_prefix_longer(struct cmd_element *,struct vty *,int,char **);
  int show_ip_prefix_list_prefix_first_match(struct cmd_element *,struct vty *,int,char **);
  int show_ip_prefix_list_summary(struct cmd_element *,struct vty *,int,char **);
  int show_ip_prefix_list_summary_name(struct cmd_element *,struct vty *,int,char **);
  int show_ip_prefix_list_detail(struct cmd_element *,struct vty *,int,char **);
  int show_ip_prefix_list_detail_name(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_prefix_list_name(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_prefix_list_name_prefix(struct cmd_element *,struct vty *,int,char **);
  int ipv6_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int ipv6_prefix_list_ge(struct cmd_element *,struct vty *,int,char **);
  int ipv6_prefix_list_ge_le(struct cmd_element *,struct vty *,int,char **);
  int ipv6_prefix_list_le(struct cmd_element *,struct vty *,int,char **);
  int ipv6_prefix_list_le_ge(struct cmd_element *,struct vty *,int,char **);
  int ipv6_prefix_list_seq(struct cmd_element *,struct vty *,int,char **);
  int ipv6_prefix_list_seq_ge(struct cmd_element *,struct vty *,int,char **);
  int ipv6_prefix_list_seq_ge_le(struct cmd_element *,struct vty *,int,char **);
  int ipv6_prefix_list_seq_le(struct cmd_element *,struct vty *,int,char **);
  int ipv6_prefix_list_seq_le_ge(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list_prefix(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list_ge(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list_ge_le(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list_le(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list_le_ge(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list_seq(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list_seq_ge(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list_seq_ge_le(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list_seq_le(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list_seq_le_ge(struct cmd_element *,struct vty *,int,char **);
  int ipv6_prefix_list_sequence_number(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list_sequence_number(struct cmd_element *,struct vty *,int,char **);
  int ipv6_prefix_list_description(struct cmd_element *,struct vty *,int,char **);
  int no_ipv6_prefix_list_description(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_prefix_list_name(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_prefix_list_name_seq(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_prefix_list_prefix(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_prefix_list_prefix_longer(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_prefix_list_prefix_first_match(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_prefix_list_summary(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_prefix_list_summary_name(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_prefix_list_detail(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_prefix_list_detail_name(struct cmd_element *,struct vty *,int,char **);
  int clear_ipv6_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int clear_ipv6_prefix_list_name(struct cmd_element *,struct vty *,int,char **);
  int clear_ipv6_prefix_list_name_prefix(struct cmd_element *,struct vty *,int,char **);
  
  

  /*prefix.c*/

  int afi2family (int afi);
  int family2afi (int family);
  int prefix_match (struct prefix *n, struct prefix *p);
  void prefix_copy (struct prefix *dest, struct prefix *src);
  int prefix_same (struct prefix *p1, struct prefix *p2);
  int prefix_cmp (struct prefix *p1, struct prefix *p2);
  char * prefix_family_str (struct prefix *p);
  struct prefix_ipv4 * prefix_ipv4_new ();
  void prefix_ipv4_free (struct prefix_ipv4 *p);
  int str2prefix_ipv4 (char *str, struct prefix_ipv4 *p);
  void masklen2ip (int masklen, struct in_addr *netmask);
  u_char ip_masklen (struct in_addr netmask);
  void apply_mask_ipv4 (struct prefix_ipv4 *p);
  int prefix_ipv4_any (struct prefix_ipv4 *p);
  struct prefix_ipv6 * prefix_ipv6_new ();
  void prefix_ipv6_free (struct prefix_ipv6 *p);
  int str2prefix_ipv6 (char *str, struct prefix_ipv6 *p);
  int ip6_masklen (struct in6_addr netmask);
  void  masklen2ip6 (int masklen, struct in6_addr *netmask);
  void apply_mask_ipv6 (struct prefix_ipv6 *p);
  void str2in6_addr (char *str, struct in6_addr *addr);
  void apply_mask (struct prefix *p);
  struct prefix * sockunion2prefix (union sockunion *dest,union sockunion *mask);
  struct prefix * sockunion2hostprefix (union sockunion *su);
  int prefix_blen (struct prefix *p);
  int str2prefix (char *str, struct prefix *p);
  int prefix2str (struct prefix *p, char *str, int size);
  struct prefix * prefix_new ();
  void prefix_free (struct prefix *p);
  int all_digit (char *str);
  void apply_classful_mask_ipv4 (struct prefix_ipv4 *p);
  int netmask_str2prefix_str (char *net_str, char *mask_str, char *prefix_str);
  

  /*sockunion.c*/

  int inet_aton (const char *cp, struct in_addr *inaddr);
  int inet_pton (int family, const char *strptr, void *addrptr);
  const char * inet_ntop (int family, const void *addrptr, char *strptr, size_t len);
  const char * inet_sutop (union sockunion *su, char *str);
  int str2sockunion (char *str, union sockunion *su);
  const char * sockunion2str (union sockunion *su, char *buf, size_t len);
  union sockunion * sockunion_str2su (char *str);
  char * sockunion_su2str (union sockunion *su);
  int sockunion_sizeof (union sockunion *su);
  int sockunion_same (union sockunion *su1, union sockunion *su2);
  int in6addr_cmp (struct in6_addr *addr1, struct in6_addr *addr2);
  int sockunion_cmp (union sockunion *su1, union sockunion *su2);
  union sockunion * sockunion_dup (union sockunion *su);
  void sockunion_free (union sockunion *su);


  /*stream*/

  struct stream * stream_new (size_t size);
  void stream_free (struct stream *s);
  unsigned long stream_get_getp (struct stream *s);
  unsigned long stream_get_putp (struct stream *s);
  unsigned long stream_get_endp (struct stream *s);
  unsigned long stream_get_size (struct stream *s);
  void stream_set_getp (struct stream *s, unsigned long pos);
  void stream_set_putp (struct stream *s, unsigned long pos);
  void stream_forward (struct stream *s, int size);
  void stream_get (void *dst, struct stream *s, size_t size);
  u_char stream_getc (struct stream *s);
  u_char stream_getc_from (struct stream *s, unsigned long from);
  u_int16_t stream_getw (struct stream *s);
  u_int16_t stream_getw_from (struct stream *s, unsigned long from);
  u_int32_t stream_getl (struct stream *s);
  u_int32_t stream_get_ipv4 (struct stream *s);
  void stream_put (struct stream *s, void *src, size_t size);
  int stream_putc (struct stream *s, u_char c);
  int stream_putw (struct stream *s, u_int16_t w);
  int stream_putl (struct stream *s, u_int32_t l);
  int stream_putc_at (struct stream *s, unsigned long putp, u_char c);
  int stream_putw_at (struct stream *s, unsigned long putp, u_int16_t w);
  int stream_putl_at (struct stream *s, unsigned long putp, u_int32_t l);
  int stream_put_ipv4 (struct stream *s, u_int32_t l);
  int stream_put_in_addr (struct stream *s, struct in_addr *addr);
  int stream_put_prefix (struct stream *s, struct prefix *p);
  int stream_read (struct stream *s, int fd, size_t size);
  int stream_read_unblock (struct stream *s, size_t size,Data*);
  int stream_write (struct stream *s, u_char *ptr, size_t size);
  u_char * stream_pnt (struct stream *s);
  int stream_empty (struct stream *s);
  void stream_reset (struct stream *s);
  int stream_flush (struct stream *s, int fd);
  struct stream_fifo * stream_fifo_new ();
  void stream_fifo_push (struct stream_fifo *fifo, struct stream *s);
  struct stream * stream_fifo_pop (struct stream_fifo *fifo);
  struct stream * stream_fifo_head (struct stream_fifo *fifo);
  void stream_fifo_clean (struct stream_fifo *fifo);
  void stream_fifo_free (struct stream_fifo *fifo);

  
  /****table.c****/

  struct bgp_table * route_table_init (void);
  void route_table_finish (struct bgp_table *rt);
  struct bgp_node * route_node_new ();
  struct bgp_node * route_node_set (struct bgp_table *table, struct prefix *prefix);
  void route_node_free (struct bgp_node *node);
  void route_table_free (struct bgp_table *rt);
  void set_link (struct route_node *node, struct route_node *New);
  struct bgp_node * route_lock_node (struct bgp_node *node);
  void route_unlock_node (struct bgp_node *node);
  void route_dump_node (struct bgp_table *t);
  struct bgp_node * route_node_match (struct bgp_table *table, struct prefix *p);
  struct bgp_node * route_node_match_ipv4 (struct bgp_table *table, struct in_addr *addr);
  struct bgp_node * route_node_match_ipv6 (struct bgp_table *table, struct in6_addr *addr);
  struct bgp_node * route_node_lookup (struct bgp_table *table, struct prefix *p);
  struct bgp_node * route_node_get (struct bgp_table *table, struct prefix *p);
  void route_node_delete (struct bgp_node *node);
  struct bgp_node * route_top (struct bgp_table *table);
  struct bgp_node * route_next (struct bgp_node *node);
  struct bgp_node * route_next_until (struct bgp_node *node, struct bgp_node *limit);
  
  /******thread.c******/

  Time_t thread_timer_sub (Time_t a, Time_t b);
  unsigned long thread_timer_remain_second(struct thread*);
  int thread_timer_cmp (Time_t a, Time_t b);
  void thread_list_debug (struct thread_list *list);
  void thread_master_debug (struct thread_master *m);
  struct thread_master * thread_master_create ();
  void thread_list_add (struct thread_list *list, struct thread *thread);
  void thread_list_add_before (struct thread_list *list,struct thread *point,struct thread *thread);
  struct thread * thread_list_delete (struct thread_list *list, struct thread *thread);
  void thread_add_unuse (struct thread_master *m, struct thread *thread);
  void thread_list_free (struct thread_master *m, struct thread_list *list);
  void thread_master_free (struct thread_master *m);
  struct thread * thread_trim_head (struct thread_list *list);
  int thread_empty (struct thread_list *list);
  struct thread * thread_new(struct thread_master *m);
  struct thread * thread_get (struct thread_master *m, u_char type,int (BGP::*func) (struct thread *), void *arg);
  struct thread * thread_add_read (struct thread_master *m,int (BGP::*func) (struct thread *), void *arg, TCP* );
  struct thread * thread_add_ready (struct thread_master *m,int (BGP::*func) (struct thread *), void *arg);
  struct thread * thread_add_timer (struct thread_master *m,int (BGP::*func) (struct thread *), void *arg, long timer);
  struct thread * thread_add_event (struct thread_master *m,int (BGP::*func) (struct thread *), void *arg, int val);
  void thread_cancel (struct thread *thread);
  void thread_cancel_event (struct thread_master *m, void *arg);
  struct thread * thread_run (struct thread_master *m, struct thread *thread,struct thread *fetch);
  void thread_call (struct thread *thread);
  struct thread * thread_execute (struct thread_master *m,int (BGP::*func)(struct thread *),void *arg,int val);
  void thread_fetch_part1(struct thread_master *);
  void thread_fetch_part2(struct thread_master *,TCP*); 
  struct thread_master * thread_make_master();

  /*****vector.c*****/
  struct _vector* vector_init (unsigned int size);
  void vector_only_wrapper_free (struct _vector* v);
  void vector_only_index_free (void *index);
  void vector_free (struct _vector* v);
  struct _vector* vector_copy (struct _vector* v);
  void vector_ensure (struct _vector* v, unsigned int num);
  int vector_empty_slot (struct _vector* v);
  int vector_set (struct _vector* v, void *val);
  int vector_set_index (struct _vector* v, unsigned int i, void *val);
  void * vector_lookup (struct _vector* v, unsigned int i);
  void * vector_lookup_ensure (struct _vector* v, unsigned int i);
  void vector_unset (struct _vector* v, unsigned int i);
  unsigned int vector_count (struct _vector* v);

  /***vty.c****/
  int vty_out (struct vty *vty, const char *format, ...);
  void vty_log (const char *proto_str, const char *format, va_list va);
  int vty_shell (struct vty *vty);
  int vty_shell_serv (struct vty *vty);
  int vty_log_out (struct vty *vty, const char *proto_str, const char *format,va_list va);
  int time_str (char *buf);
  struct vty* vty_new();
  void vty_close(struct vty*);
  void vty_read_config();
  void vty_read_file(FILE *confp);

#ifdef BGP_MRAI
  int bgp_advertise_insert(struct peer *peer,struct bgp_advertise *adv);
  void bgp_advertise_remove( struct peer *peer,struct bgp_advertise *rmv);
  void bgp_advertise_remove_by_prefix (struct peer *peer,struct prefix *p);
  void routeadv_list_add(struct peer *peer, struct thread *t);
  void routeadv_list_remove(struct peer *peer, struct thread *t);
  bool routeadv_list_search(struct peer *peer, struct prefix *p) ;
  int bgp_update_send_check (struct bgp_info *ri, struct peer_conf *conf, struct peer *peer,
                struct prefix *p, struct attr *attr, afi_t afi, safi_t safi,
                struct peer *from, struct prefix_rd *prd, u_char *tag); 
  int bgp_withdraw_send_check(struct peer* peer,struct prefix *p);
#endif  

  /*****bgp_vty.c*****/

  afi_t bgp_node_afi(struct vty *vty);
  safi_t bgp_node_safi(struct vty *vty);
  //int  peer_address_self_check (union sockunion *su);
  struct peer * peer_lookup_vty (struct vty *vty, char *ip_str);
  struct peer * peer_and_group_lookup_vty (struct vty *vty, char *peer_str);
  int bgp_vty_return (struct vty *vty, int ret);
  struct peer_conf* peer_conf_create(int,int,struct peer*);
  void peer_conf_active(int,int,struct peer_conf*);
  void peer_conf_deactive(int,int,struct peer_conf*);
  int peer_remote_as_vty (struct vty *vty, char *peer_str, char *as_str, afi_t afi,safi_t safi);
  int peer_flag_modify_vty (struct vty *vty, char *ip_str, u_int16_t flag, int set);
  int peer_flag_set_vty (struct vty *vty, char *ip_str, u_int16_t flag);
  int peer_af_flag_modify_vty (struct vty *vty, char *peer_str, afi_t afi,safi_t safi, u_int16_t flag, int set);
  int peer_af_flag_set_vty (struct vty *vty, char *peer_str, afi_t afi,safi_t safi, u_int16_t flag);
  int peer_af_flag_unset_vty (struct vty *vty, char *peer_str, afi_t afi,safi_t safi, u_int16_t flag);
  int peer_ebgp_multihop_set_vty (struct vty *vty, char *ip_str, char *ttl_str); 
  int peer_ebgp_multihop_unset_vty (struct vty *vty, char *ip_str) ;
  int peer_update_source_vty (struct vty *vty, char *peer_str, char *source_str); 
  int peer_default_originate_set_vty (struct vty *vty, char *peer_str, afi_t afi,safi_t safi, char *rmap, int set);
  int peer_port_vty (struct vty *vty, char *ip_str, int afi, char *port_str);
  int peer_advertise_interval_vty (struct vty *vty, char *ip_str, char *time_str,int set) ; 
#ifdef BGP_MRAI
  void bgp_update_add_timestamp( struct peer* peer, struct prefix *p);
  void bgp_mrai_timers_off(struct peer *peer);
  void bgp_cancel_supressed_update_by_prefix(struct peer *peer,struct prefix* p);
  void bgp_cancel_timer_by_prefix(struct peer *peer,struct prefix* p);
  void aspath_delete(struct aspath *aspath);
#endif
  int peer_version_vty (struct vty *vty, char *ip_str, char *str);
  int peer_interface_vty (struct vty *vty, char *ip_str, char *str);
  int peer_distribute_set_vty (struct vty *vty, char *ip_str, afi_t afi, safi_t safi,char *name_str, char *direct_str); 
  int peer_distribute_unset_vty (struct vty *vty, char *ip_str, afi_t afi,safi_t safi, char *direct_str);
  int peer_prefix_list_set_vty (struct vty *vty, char *ip_str, afi_t afi,safi_t safi, char *name_str, char *direct_str);
  int peer_prefix_list_unset_vty (struct vty *vty, char *ip_str, afi_t afi,safi_t safi, char *direct_str);
  int peer_aslist_set_vty (struct vty *vty, char *ip_str, afi_t afi, safi_t safi,char *name_str, char *direct_str);
  int peer_aslist_unset_vty (struct vty *vty, char *ip_str, afi_t afi, safi_t safi,char *direct_str);
  int peer_route_map_set_vty (struct vty *vty, char *ip_str, afi_t afi, safi_t safi,char *name_str, char *direct_str);
  int peer_route_map_unset_vty (struct vty *vty, char *ip_str, afi_t afi,safi_t safi, char *direct_str); 
  void community_list_show (struct vty *vty, struct community_list *llist);
  int peer_weight_set_vty (struct vty *vty, char *ip_str, char *weight_str);
  int peer_weight_unset_vty (struct vty *vty, char *ip_str);
  int peer_timers_set_vty (struct vty *vty, char *ip_str, char *keep_str,char *hold_str);
  int peer_timers_unset_vty (struct vty *vty, char *ip_str);
  int peer_timers_connect_set_vty (struct vty *vty, char *ip_str, char *time_str);
  int peer_timers_connect_unset_vty (struct vty *vty, char *ip_str);
  int extcommunity_list_set_vty (struct vty *vty, int argc, char **argv, int style, int reject_all_digit_name);
  int extcommunity_list_unset_all_vty (struct vty *vty, char *name);
  
  int bgp_multiple_instance_func(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_multiple_instance(struct cmd_element *,struct vty *,int,char **);
  int bgp_config_type(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_config_type(struct cmd_element *,struct vty *,int,char **);
  int no_synchronization(struct cmd_element *,struct vty *,int,char **);
  int no_auto_summary(struct cmd_element *,struct vty *,int,char **);
  int router_bgp(struct cmd_element *,struct vty *,int,char **); 
  int no_router_bgp(struct cmd_element *,struct vty *,int,char **);
  int bgp_router_id(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_router_id(struct cmd_element *,struct vty *,int,char **);
  int bgp_cluster_id(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_cluster_id(struct cmd_element *,struct vty *,int,char **);
  int bgp_confederation_identifier(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_confederation_identifier(struct cmd_element *,struct vty *,int,char **);
  int bgp_confederation_peers(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_confederation_peers(struct cmd_element *,struct vty *,int,char **);
  int bgp_timers(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_timers(struct cmd_element *,struct vty *,int,char **);
  int bgp_client_to_client_reflection(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_client_to_client_reflection(struct cmd_element *,struct vty *,int,char **);
  int bgp_always_compare_med(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_always_compare_med(struct cmd_element *,struct vty *,int,char **);
  int bgp_deterministic_med(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_deterministic_med(struct cmd_element *,struct vty *,int,char **);
  int bgp_fast_external_failover(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_fast_external_failover(struct cmd_element *,struct vty *,int,char **);
  int bgp_enforce_first_as(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_enforce_first_as(struct cmd_element *,struct vty *,int,char **);
  int bgp_bestpath_compare_router_id(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_bestpath_compare_router_id(struct cmd_element *,struct vty *,int,char **);
  int bgp_bestpath_aspath_ignore(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_bestpath_aspath_ignore(struct cmd_element *,struct vty *,int,char **);
  int bgp_log_neighbor_changes(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_log_neighbor_changes(struct cmd_element *,struct vty *,int,char **);
  int bgp_bestpath_med(struct cmd_element *,struct vty *,int,char **);
  int bgp_bestpath_med2(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_bestpath_med(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_bestpath_med2(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_default_ipv4_unicast(struct cmd_element *,struct vty *,int,char **);
  int bgp_default_ipv4_unicast(struct cmd_element *,struct vty *,int,char **);
  int bgp_network_import_check(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_network_import_check(struct cmd_element *,struct vty *,int,char **);
  int bgp_default_local_preference(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_default_local_preference(struct cmd_element *,struct vty *,int,char **);
  int neighbor_remote_as(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_remote_as(struct cmd_element *,struct vty *,int,char **);
  int neighbor_peer_group(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_peer_group(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_peer_group_remote_as(struct cmd_element *,struct vty *,int,char **);
  int neighbor_local_as(struct cmd_element *,struct vty *,int,char **);
  int neighbor_local_as_no_prepend(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_local_as(struct cmd_element *,struct vty *,int,char **);
  int neighbor_activate(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_activate(struct cmd_element *,struct vty *,int,char **);
  int neighbor_set_peer_group(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_set_peer_group(struct cmd_element *,struct vty *,int,char **);
  int neighbor_passive(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_passive(struct cmd_element *,struct vty *,int,char **);
  int neighbor_shutdown(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_shutdown(struct cmd_element *,struct vty *,int,char **);
  int neighbor_capability_route_refresh(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_capability_route_refresh(struct cmd_element *,struct vty *,int,char **);
  int neighbor_capability_dynamic(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_capability_dynamic(struct cmd_element *,struct vty *,int,char **);
  int neighbor_dont_capability_negotiate(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_dont_capability_negotiate(struct cmd_element *,struct vty *,int,char **);
  int neighbor_capability_orf_prefix(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_capability_orf_prefix(struct cmd_element *,struct vty *,int,char **);
  int neighbor_nexthop_self(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_nexthop_self(struct cmd_element *,struct vty *,int,char **);
  int neighbor_remove_private_as(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_remove_private_as(struct cmd_element *,struct vty *,int,char **);
  int neighbor_send_community(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_send_community(struct cmd_element *,struct vty *,int,char **);
  int neighbor_send_community_type(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_send_community_type(struct cmd_element *,struct vty *,int,char **);
  int neighbor_soft_reconfiguration(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_soft_reconfiguration(struct cmd_element *,struct vty *,int,char **);
  int neighbor_route_reflector_client(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_route_reflector_client(struct cmd_element *,struct vty *,int,char **);
  int neighbor_route_server_client(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_route_server_client(struct cmd_element *,struct vty *,int,char **);
  int neighbor_attr_unchanged(struct cmd_element *,struct vty *,int,char **);
  int neighbor_attr_unchanged1(struct cmd_element *,struct vty *,int,char **);
  int neighbor_attr_unchanged2(struct cmd_element *,struct vty *,int,char **);
  int neighbor_attr_unchanged3(struct cmd_element *,struct vty *,int,char **);
  int neighbor_attr_unchanged4(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_attr_unchanged(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_attr_unchanged1(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_attr_unchanged2(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_attr_unchanged3(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_attr_unchanged4(struct cmd_element *,struct vty *,int,char **);
  int neighbor_transparent_as(struct cmd_element *,struct vty *,int,char **);
  int neighbor_transparent_nexthop(struct cmd_element *,struct vty *,int,char **);
  int neighbor_ebgp_multihop(struct cmd_element *,struct vty *,int,char **);
  int neighbor_ebgp_multihop_ttl(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_ebgp_multihop(struct cmd_element *,struct vty *,int,char **);
  int neighbor_enforce_multihop(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_enforce_multihop(struct cmd_element *,struct vty *,int,char **);
  int neighbor_description(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_description(struct cmd_element *,struct vty *,int,char **);
  int neighbor_update_source(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_update_source(struct cmd_element *,struct vty *,int,char **);
  int neighbor_default_originate(struct cmd_element *,struct vty *,int,char **);
  int neighbor_default_originate_rmap(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_default_originate(struct cmd_element *,struct vty *,int,char **);
  int neighbor_port(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_port(struct cmd_element *,struct vty *,int,char **);
  int neighbor_weight(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_weight(struct cmd_element *,struct vty *,int,char **);
  int neighbor_override_capability(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_override_capability(struct cmd_element *,struct vty *,int,char **);
  int neighbor_strict_capability(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_strict_capability(struct cmd_element *,struct vty *,int,char **);
  int neighbor_timers(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_timers(struct cmd_element *,struct vty *,int,char **);
  int neighbor_timers_connect(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_timers_connect(struct cmd_element *,struct vty *,int,char **);
  int neighbor_advertise_interval(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_advertise_interval(struct cmd_element *,struct vty *,int,char **);
  int neighbor_version(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_version(struct cmd_element *,struct vty *,int,char **);
  int neighbor_interface(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_interface(struct cmd_element *,struct vty *,int,char **);
  int neighbor_distribute_list(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_distribute_list(struct cmd_element *,struct vty *,int,char **);
  int neighbor_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_prefix_list(struct cmd_element *,struct vty *,int,char **);
  int neighbor_filter_list(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_filter_list(struct cmd_element *,struct vty *,int,char **);
  int neighbor_route_map(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_route_map(struct cmd_element *,struct vty *,int,char **);
  int neighbor_unsuppress_map(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_unsuppress_map(struct cmd_element *,struct vty *,int,char **);
  int neighbor_maximum_prefix(struct cmd_element *,struct vty *,int,char **);
  int neighbor_maximum_prefix_threshold(struct cmd_element *,struct vty *,int,char **);
  int neighbor_maximum_prefix_warning(struct cmd_element *,struct vty *,int,char **);
  int neighbor_maximum_prefix_threshold_warning(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_maximum_prefix(struct cmd_element *,struct vty *,int,char **);
  int neighbor_allowas_in(struct cmd_element *,struct vty *,int,char **);
  int no_neighbor_allowas_in(struct cmd_element *,struct vty *,int,char **);
  int address_family_ipv4(struct cmd_element *,struct vty *,int,char **);
  int address_family_ipv4_safi(struct cmd_element *,struct vty *,int,char **);
  int address_family_ipv6_unicast(struct cmd_element *,struct vty *,int,char **);
  int address_family_vpnv4(struct cmd_element *,struct vty *,int,char **);
  int exit_address_family(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_all(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_group(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_external(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_as(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_all_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_all_ipv4_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_instance_all_ipv4_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_all_vpnv4_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_all_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_ipv4_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_vpnv4_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_peer_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_group_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_group_ipv4_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_peer_group_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_external_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_external_ipv4_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_external_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_as_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_as_ipv4_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_as_vpnv4_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_as_soft_out(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_all_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_all_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_all_ipv4_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_instance_all_ipv4_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_all_ipv4_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_instance_all_ipv4_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_all_vpnv4_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_all_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_all_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_ipv4_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_ipv4_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_vpnv4_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_peer_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_peer_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_group_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_group_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_group_ipv4_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_group_ipv4_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_peer_group_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_peer_group_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_external_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_external_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_external_ipv4_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_external_ipv4_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_external_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_external_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_as_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_as_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_as_ipv4_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_as_ipv4_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_as_vpnv4_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_as_soft_in(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_as_in_prefix_filter(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_all_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_all_ipv4_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_instance_all_ipv4_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_all_vpnv4_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_all_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_ipv4_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_vpnv4_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_peer_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_group_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_peer_group_ipv4_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_peer_group_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_external_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_external_ipv4_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_external_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_as_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_as_ipv4_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_ip_bgp_as_vpnv4_soft(struct cmd_element *,struct vty *,int,char **);
  int clear_bgp_as_soft(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_summary(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_instance_summary(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_ipv4_summary(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_instance_ipv4_summary(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_vpnv4_all_summary(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_vpnv4_rd_summary(struct cmd_element *,struct vty *,int,char **);
  int show_bgp_summary(struct cmd_element *,struct vty *,int,char **); 
  int show_bgp_instance_summary(struct cmd_element *,struct vty *,int,char **);
  int show_ipv6_bgp_summary(struct cmd_element *,struct vty *,int,char **); 
  int show_ipv6_mbgp_summary(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_neighbors(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_neighbors_peer(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_instance_neighbors(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_instance_neighbors_peer(struct cmd_element *,struct vty *,int,char **);
  int show_ip_bgp_paths(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_ipv4_paths(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_community_info(struct cmd_element *,struct vty *,int,char **); 
  int show_ip_bgp_attr_info(struct cmd_element *,struct vty *,int,char **); 
  int bgp_redistribute_ipv4(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_ipv4_rmap(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_ipv4_metric(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_ipv4_rmap_metric(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_ipv4_metric_rmap(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_redistribute_ipv4(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_redistribute_ipv4_rmap(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_redistribute_ipv4_metric(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_redistribute_ipv4_rmap_metric(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_ipv6(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_ipv6_rmap(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_ipv6_metric(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_ipv6_rmap_metric(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_ipv6_metric_rmap(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_redistribute_ipv6(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_redistribute_ipv6_rmap(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_redistribute_ipv6_metric(struct cmd_element *,struct vty *,int,char **);
  int no_bgp_redistribute_ipv6_rmap_metric(struct cmd_element *,struct vty *,int,char **);
  int ip_community_list(struct cmd_element *,struct vty *,int,char **);
  int ip_community_list_standard(struct cmd_element *,struct vty *,int,char **);
  int ip_community_list_expanded(struct cmd_element *,struct vty *,int,char **);
  int ip_community_list_name_standard(struct cmd_element *,struct vty *,int,char **);
  int ip_community_list_name_expanded(struct cmd_element *,struct vty *,int,char **);
  int no_ip_community_list_all(struct cmd_element *,struct vty *,int,char **);
  int no_ip_community_list_name_all(struct cmd_element *,struct vty *,int,char **);
  int no_ip_community_list(struct cmd_element *,struct vty *,int,char **);
  int no_ip_community_list_standard(struct cmd_element *,struct vty *,int,char **);
  int no_ip_community_list_expanded(struct cmd_element *,struct vty *,int,char **);
  int no_ip_community_list_name_standard(struct cmd_element *,struct vty *,int,char **);
  int no_ip_community_list_name_expanded(struct cmd_element *,struct vty *,int,char **);
  int show_ip_community_list(struct cmd_element *,struct vty *,int,char **);
  int show_ip_community_list_arg(struct cmd_element *,struct vty *,int,char **);
  int ip_extcommunity_list_standard(struct cmd_element *,struct vty *,int,char **);
  int ip_extcommunity_list_expanded(struct cmd_element *,struct vty *,int,char **);
  int ip_extcommunity_list_name_standard(struct cmd_element *,struct vty *,int,char **);
  int ip_extcommunity_list_name_expanded(struct cmd_element *,struct vty *,int,char **);
  int no_ip_extcommunity_list_all(struct cmd_element *,struct vty *,int,char **);
  int no_ip_extcommunity_list_name_all(struct cmd_element *,struct vty *,int,char **);
  int no_ip_extcommunity_list_standard(struct cmd_element *,struct vty *,int,char **);
  int no_ip_extcommunity_list_expanded(struct cmd_element *,struct vty *,int,char **);
  int no_ip_extcommunity_list_name_standard(struct cmd_element *,struct vty *,int,char **);
  int no_ip_extcommunity_list_name_expanded(struct cmd_element *,struct vty *,int,char **);
  int show_ip_extcommunity_list(struct cmd_element *,struct vty *,int,char **);
  int show_ip_extcommunity_list_arg(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_set (struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_rmap_set(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_metric_set(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_unset(struct cmd_element *,struct vty *,int,char **);
  int bgp_redisribute_routemap_unset(struct cmd_element *,struct vty *,int,char **);
  int bgp_redistribute_metric_unset(struct cmd_element *,struct vty *,int,char **);
  
  
  int peer_unsuppress_map_set_vty (struct vty *vty, char *ip_str, afi_t afi,safi_t safi, char *name_str);
  int peer_unsuppress_map_unset_vty (struct vty *vty, char *ip_str, afi_t afi,safi_t safi); 
  int peer_maximum_prefix_set_vty (struct vty *vty, char *ip_str, afi_t afi,safi_t safi, char *num_str, char *threshold_str,int warning);
  int peer_maximum_prefix_unset_vty (struct vty *vty, char *ip_str, afi_t afi,safi_t safi);
  void bgp_clear_vty_error (struct vty *vty, struct peer *peer, afi_t afi,safi_t safi, int error);
  int bgp_clear (struct vty *vty, struct bgp *bgp,  afi_t afi, safi_t safi,enum clear_sort sort,enum bgp_clear_type stype, char *arg);
  int bgp_clear_vty (struct vty *vty, char *name, afi_t afi, safi_t safi,enum clear_sort sort, enum bgp_clear_type stype, char *arg);
  int bgp_show_summary (struct vty *vty, struct bgp *bgp, int afi, int safi);
  int bgp_show_summary_vty (struct vty *vty, char *name, afi_t afi, safi_t safi);
  void bgp_show_peer_afi_orf_cap (struct vty *vty, struct peer *p,afi_t afi, safi_t safi,u_int16_t adv_smcap, u_int16_t adv_rmcap,u_int16_t rcv_smcap, u_int16_t rcv_rmcap);
  void bgp_show_peer_afi (struct vty *vty, struct peer *p, afi_t afi, safi_t safi);
  void bgp_show_peer (struct vty *vty, struct peer *p);
  int bgp_show_neighbor (struct vty *vty, struct bgp *bgp,enum show_type type, union sockunion *su);
  int bgp_show_neighbor_vty (struct vty *vty, char *name, enum show_type type,char *ip_str);
  void community_show_all_iterator (struct hash_backet *backet, void *vty);
  int bgp_str2route_type (int afi, char *str);
  int bgp_config_write_redistribute (struct vty *vty, struct bgp *bgp, afi_t afi,safi_t safi, int *write);
  void bgp_vty_init ();
  int peer_flag_unset_vty(struct vty*,char*,short unsigned int);
  char *community_direct_str (int direct);
  void community_list_perror (struct vty *vty, int ret);
  int community_list_set_vty (struct vty *vty, int argc, char **argv, int style,int reject_all_digit_name);
  int community_list_unset_all_vty (struct vty *vty, char *name);
  int community_list_unset_vty (struct vty *vty, int argc, char **argv, int style);
  int extcommunity_list_unset_vty (struct vty *vty, int argc, char **argv, int style);
  void extcommunity_list_show (struct vty *vty, struct community_list *list);
  char * community_list_config_str (struct community_entry *entry);
  int community_list_config_write (struct vty *vty);
  void community_list_vty ();
};

#endif
