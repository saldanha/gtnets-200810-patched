// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: node-interconnect.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - Supercomputer Interconnect Node class
// George F. Riley.  Georgia Tech, Spring 2002

#include <iostream>
#include <sstream>

#include "debug.h"
#include "node-interconnect.h"
#include "supercomputer-interconnect.h"
#include "interface-interconnect.h"
#include "interface-interconnect-remote.h"
#include "application-mpi.h"
#include "linkp2p.h"
#include "mpi.h"
#include "distributed-simulator.h"


using namespace std;

MPIMap_t  NodeInterconnect::mpiGridMapping;
GridMap_t NodeInterconnect::locNodeMapping;

NodeInterconnect::NodeInterconnect(
    Count_t x, Count_t y, Count_t z)
    : Node(DistributedSimulator::systemId), loc(GridLocation(x, y, z)),
      mpiDemux(nil),
      neighbors(6, (InterfaceInterconnect*)nil)
{
  mpiDemux = new MPI(this);
  locNodeMapping[loc] = this;
  SetLocation(x, y); // ! THis is a hack for animation
}

NodeInterconnect::~NodeInterconnect()
{ // NOthing to do for the moment
}

  
// Get list of neighbors
void NodeInterconnect::Neighbors(NodeWeightVec_t& nwv, bool)
{
  for (Count_t i = 0; i < neighbors.size(); ++i)
    {
      if (neighbors[i])
        { // Interface exists, see if neighbor exists
          InterfaceInterconnect* ni = neighbors[i];
          if (ni->peer)
            { // Peer exists, not remote link
              nwv.push_back(NodeIfWeight(ni->peer->GetNode(), ni, 1));
            }
        }
    }
}
          
  
// Get interface to the specified node
Interface* NodeInterconnect::GetIfByNode(Node* n)
{
  for (Count_t i = 0; i < neighbors.size(); ++i)
    {
      if (neighbors[i])
        { // Interface exists, check if neighbor exists
          if (neighbors[i]->peer)
            {
              if (neighbors[i]->peer->GetNode() == n)
                {
                  return (Interface*)neighbors[i];
                }
            }
        }
    }
  return nil; // Not found
}

void     NodeInterconnect::AddMPId(MPIApplication* app)
{  // Add an MPI process identifier to this node
  // FIrst get an MPI process Id
  app->myId = mpiDemux->NextId();
  DEBUG0((cout << "Inserting mpid " << app->MPId()
          << " at gridloc " << GetGridLocation() << endl));
  mpiGridMapping.insert(MPIMap_t::value_type(app->MPId(), GetGridLocation()));
  // Also insert in the mpi demux
  mpiDemux->AddMPIApp(app);
}

InterfaceInterconnect* NodeInterconnect::Route(
    const GridLocation& dst, GridLocation& nextHop) const
{
  // Find the correct interface to route to the specified destination
  if (dst == GetGridLocation()) return nil; // At destination
  GridLocation diff = dst - loc;
  GridLocation aD(abs(diff.x), abs(diff.y), abs(diff.z));
  GridLocation offs;
  // This algo chooses the direction (x, y, z) based on farthest
  // one first.  If ties, x first, then y, then z
  if ((aD.x >= aD.y) && (aD.x >= aD.z) && diff.x)
    {
      offs.x = (diff.x < 0) ? -1 : +1;
    }
  else if ((aD.y >= aD.x) && (aD.y >= aD.z) && diff.y)
    {
      offs.y = (diff.y < 0) ? -1 : +1;
    }
  else if ((aD.z >= aD.x) && (aD.z >= aD.y) && diff.z)
    {
      offs.z = (diff.z < 0) ? -1 : +1;
    }
  #ifdef OLD_WAY  
  if      (diff.x > 0) offs.x = +1;
  else if (diff.x < 0) offs.x = -1;
  else if (diff.y > 0) offs.y = +1;
  else if (diff.y < 0) offs.y = -1;
  else if (diff.z > 0) offs.z = +1;
  else if (diff.z < 0) offs.z = -1;
#endif
  nextHop = loc + offs;
  Count_t ind = LocToOffset(offs);
  return neighbors[ind];
}

InterfaceInterconnect* NodeInterconnect::Route(
    MPId_t dst, GridLocation& ultimateTarget, GridLocation& nextHop) const
{
  bool valid;
  ultimateTarget = SupercomputerInterconnect::instance->GetLocation(dst,valid);
  if (!valid) return nil;
  return Route(ultimateTarget, nextHop);  // Find correct interface
}


void NodeInterconnect::AddNeighbor(const GridLocation& nOffset)
{
  Count_t ni = LocToOffset(nOffset);
  if (neighbors[ni])
    {
      cout << "HuH?  Node " << GetGridLocation()
           << " already has a neighbor at " << nOffset
           << endl;
      return;
    }
  GridLocation nbLoc = GetGridLocation() + nOffset;
  NodeInterconnect* nn = GetNode(nbLoc);
  GridLocation nbOffset(nOffset);
  nbOffset.x = -nbOffset.x;
  nbOffset.y = -nbOffset.y;
  nbOffset.z = -nbOffset.z;
  Count_t nbi = LocToOffset(nbOffset);
  if (nn->neighbors[nbi])
    {
      cout << "HuH?  Node " << nn->GetGridLocation()
           << " already has a neighbor at " << nbLoc
           << endl;
      return;
    }
  InterfaceInterconnect* li = new InterfaceInterconnect(this);
  InterfaceInterconnect* ri = new InterfaceInterconnect(nn);
  ((Linkp2p*)li->GetLink())->SetPeer(ri);
  ((Linkp2p*)ri->GetLink())->SetPeer(li);
  
  neighbors[ni] = li;
  nn->neighbors[nbi] = ri;
  ri->peer = li;
  li->peer = ri;
}

// Add Remote Neighbor
void NodeInterconnect::AddRemoteNeighbor(const GridLocation& nOffset)
{
  Count_t ni = LocToOffset(nOffset);
  if (neighbors[ni])
    {
      cout << "HuH?  Node " << GetGridLocation()
           << " already has a neighbor at " << nOffset
           << endl;
      return;
    }
  RemoteInterfaceInterconnect* li = new RemoteInterfaceInterconnect(this);
  neighbors[ni] = li;
  li->peerLocation = GetGridLocation() + nOffset;
  // Create the multicast groups.  We create the local->remote
  // and subscribe to remote->local
  GridLocation peer = li->peerLocation;
  GridLocation local = GetGridLocation();
  ostringstream osstr;
  cout << "Adding remote iface from " << local << " to " << peer << endl;
  osstr << local.x << "-" << local.y << "-" << local.z << "-"
        << peer.x  << "-" << peer.y  << "-" << peer.z;
  DistributedSimulator::rti_BCast.push_back(
      InitGroup(osstr.str().c_str(), (LinkRTI*)li));
  //cout << "RTI TX Group " << osstr.str() << endl;
  ostringstream osstr1;
  osstr1 << peer.x  << "-" << peer.y  << "-" << peer.z << "-"
         << local.x << "-" << local.y << "-" << local.z;
  DistributedSimulator::rti_MGroup.push_back(
      MGroup(osstr1.str().c_str(), (Interface*)li));
  //cout << "RTI RX Group " << osstr1.str() << endl;
}
  
const GridLocation&  NodeInterconnect::GetGridLocation() const
{
  return loc;
}

// Static Methods
GridLocation NodeInterconnect::GetGridLocation(MPId_t mpi)
{
  MPIMap_t::iterator i = mpiGridMapping.find(mpi);
  if (i != mpiGridMapping.end()) return i->second;
  return GridLocation();
}

NodeInterconnect* NodeInterconnect::GetNode(const GridLocation& loc)
{
  GridMap_t::iterator i = locNodeMapping.find(loc);
  if (i == locNodeMapping.end()) return nil; // Not found
  return i->second;
}

Count_t NodeInterconnect::LocToOffset(const GridLocation& loc)
{
  if (loc.x < 0) return 0;
  if (loc.x > 0) return 1;
  if (loc.y < 0) return 2;
  if (loc.y > 0) return 3;
  if (loc.z < 0) return 4;
  if (loc.z > 0) return 5;
  cout << "HuH?  Invalid location offset " << loc << endl;
  return 0;
}


