// GENERAL PUBLIC LICENSE AGREEMENT
//
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
//
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
//
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
//
// 1.  This License allows you to:
//
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
//
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//
//
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
//
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
//
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
//
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
//
//
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
//
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
//
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
//
//
// 3.  Export Law Assurance.
//
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//
// 4.  Termination.
//
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
//
//
// 5.  Disclaimer of Warranties and Limitation on Liability.
//
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
//
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
//
// GTRC shall have no obligation for support or maintenance of Program.
//
// 6.  Copyright Notice.
//
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
//
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
//
// Said copyright notice shall read as follows:
//
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: routing-dsr.cc 437 2005-10-28 19:45:14Z dheeraj $



// Uncomment below to enable debug level 0
#define DEBUG_MASK 0x01

#include "debug.h"
#include "routing-dsr.h"
#include "node.h"
#include "l2proto802.11.h"
#include "queue.h"

#define MYIP	((string)IPAddr(node->GetIPAddr()))

using namespace std;

// Default setting
bool RoutingDSR::enableL2Notify = true;
bool RoutingDSR::enableRingSearch = true;
Word_t RoutingDSR::defaultSendBufSize = SEND_BUF_SIZE;
Time_t RoutingDSR::defaultBufCheck = 1.0;
Time_t RoutingDSR::defaultSendBufPeriod = 0;  // 30 ms
Word_t RoutingDSR::defaultPCacheSize = 30;    // primary
Word_t RoutingDSR::defaultSCacheSize = 64;    // secondary

/*
  ------------------------------------------------------
  RouteCache
  ------------------------------------------------------
*/

void RouteCache::AddRoute(const SourceRoute_t& rt)
{
  // check if already has the same route in cache
  RtCache_t::iterator i;
  for (i = cache.begin(); i != cache.end(); ++i) {
    SourceRoute_t::size_type sz
        = i->route.size() < rt.size() ? i->route.size() : rt.size();
    SourceRoute_t::size_type j;
    for (j = 0; j < sz; ++j)
      if (rt[j] != i->route[j]) break;
    if (j == sz) break;
  }

  if (i == cache.end()) {  // no same route in cache
    if (cache.size() >= cacheSize) {
      //cout << "killing route\n";
      cache.pop_front();
    }
    cache.push_back(RtCacheEntry(rt));
  }
  else if (rt.size() > i->route.size())  // new route contains the cached route
    for (SourceRoute_t::size_type k = i->route.size(); k < rt.size(); ++k)
      i->route.push_back(rt[k]);
}

//#define REMOVE_NEW_WAY

void RouteCache::RemoveRoute(Node* node, IPAddr_t ip1, IPAddr_t ip2)
{
  // search route cache for path including error link ip1-ip2
  RtCache_t::iterator i = cache.begin();
  while (i != cache.end())
    {
      if (ip1 == node->GetIPAddr()) {
        // search for next hop == ip2
        if (i->route[0] == ip2) {
          DEBUG0(cout << MYIP << ": removing route -> ");
          DEBUG0(RoutingDSR::PrintRoute(i->route));
          cache.erase(i++);
        }
        else ++i;
      }
      else {
        // search for link ip1-ip2
#ifdef REMOVE_NEW_WAY
        bool erased = false;
#endif
        SourceRoute_t::size_type j;
        for (j = 0; j < i->route.size()-1; ++j)
          if (i->route[j] == ip1 && i->route[j+1] == ip2) {
            DEBUG0(cout << MYIP << ": removing route -> ");
            DEBUG0(RoutingDSR::PrintRoute(i->route));
            SourceRoute_t::size_type sz = i->route.size();
            for (SourceRoute_t::size_type k = 0; k < sz-j-1; ++k)
              i->route.pop_back();
#ifdef REMOVE_NEW_WAY
            SourceRoute_t temp = i->route;
            cache.erase(i++);
            AddRoute(temp);
            erased = true;
#endif
            break;
          }
#ifdef REMOVE_NEW_WAY
        if (!erased) ++i;
#else
        if (j == i->route.size()-1) ++i;
#endif
      }
    } //while
}

bool RouteCache::FindRoute(IPAddr_t dst, RtCache_t::iterator& ii,
                           SourceRoute_t::size_type& jj)
{
  unsigned int min_hop = MAX_SR_LEN;
  ii = cache.end();
  for (RtCache_t::iterator i = cache.begin(); i != cache.end(); ++i)
    for (SourceRoute_t::size_type j = 0; j < i->route.size(); ++j)
      if (dst == i->route[j]) {  // found a route
        if (j+1 >= min_hop) break;
        min_hop = j+1;
        ii = i;
        jj = j;
        break;
      }
  return ii != cache.end();
}

void RouteCache::Maintain()
{
  for (RtCache_t::iterator i=cache.begin(); i!=cache.end(); )
    {
      i->lt--;
      if (i->lt <= 0) cache.erase(i++);
      else ++i;
    }
}


/*
  ------------------------------------------------------
  RoutingDSR
  ------------------------------------------------------
*/

RoutingDSR::RoutingDSR()
{
  seqID = 0;
  evSendBuf = nil;
  evBufCheck = nil;
  evRtCache = nil;
  urvJitter = new Uniform(0, BroadcastJitter);
  rreqQue.reserve(5);
  errorOption = nil;
  timeErrorOpt = 0;
  primaryCache.cacheSize = defaultPCacheSize;
  secondaryCache.cacheSize = defaultSCacheSize;
}

RoutingDSR::~RoutingDSR()
{
  if (evSendBuf) delete evSendBuf;
  if (evBufCheck) delete evBufCheck;
  if (evRtCache) delete evRtCache;
  if (urvJitter) delete urvJitter;
  if (errorOption) delete errorOption;
}

void RoutingDSR::DataRequest(Node* n, Packet* p, void* v)
{
  IPV4ReqInfo* ipInfo = (IPV4ReqInfo*)v;
  SourceRouteOption* srOpt = GetSourceRoute(ipInfo->dst, true);
  if (srOpt) {
    if (defaultSendBufPeriod > 0) {
      // store this pkt if there is another pending pkt for the same dst
      //if (!sendBuffer.empty()) {
      if (FindSendBuffer(ipInfo->dst)) {
        delete srOpt;
        AddIPHeader(p, ipInfo->dst, ipInfo->ttl, ipInfo->l4proto);
        BufferPacket(p);
        return;
      }
    }
    // Source route option
    DSROptionsHeader* dsrhdr = new DSROptionsHeader();
    dsrhdr->options.push_back(srOpt);

    DEBUG(1, cout << MYIP << ": Sending packet to " << (string)ipInfo->dst
          << " using route: ");
    DEBUG(1, PrintRoute(srOpt->route));

    // Unicast
    L3Transmit(n, p, ipInfo->dst, ipInfo->ttl, ipInfo->l4proto, dsrhdr);
  }
  else {
    // Route Discovery
    DEBUG0(cout << MYIP << ": No route to " << (string)ipInfo->dst
           << " in route cache\n");

    AddIPHeader(p, ipInfo->dst, ipInfo->ttl, ipInfo->l4proto);
    BufferPacket(p);

    // send RREQ
    if (!IsRequestPending(ipInfo->dst))
      SendRouteRequest(ipInfo->dst, 1, NonpropRequestTimeout);
  }
}

void RoutingDSR::DataSend(Packet* p, SourceRouteOption* sr)
{
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
  if (!sr) {
    sr = GetSourceRoute(iphdr->dst, true);
    if (!sr) return;
  }

  // Source route option
  DSROptionsHeader* dsrhdr;
  SourceRouteOption* srOpt;
  if (iphdr->options.empty()) {
    srOpt = sr;
    dsrhdr = new DSROptionsHeader();
    dsrhdr->options.push_back(srOpt);
    iphdr->options.push_back(dsrhdr);
  }
  else {  // salvaged packet
    dsrhdr = (DSROptionsHeader*)iphdr->options[0];
    srOpt = (SourceRouteOption*)dsrhdr->options[0];
    for (SourceRoute_t::iterator i=sr->route.begin(); i!=sr->route.end(); ++i)
      srOpt->route.push_back(*i);
    delete sr;
  }
  iphdr->totalLength = (Word_t)p->SizeUpdate();

  DEBUG(1, cout << MYIP << " Sending packet to "
        << IPAddr::ToDotted(iphdr->dst) << " using route: ");
  DEBUG(1, PrintRoute(srOpt->route));

  // Unicast
  RoutingEntry re = srOpt->LookupNextHop(node);  // get the next hop
  Unicast(re, p);
}

// Add L3 header and transmit
void RoutingDSR::L3Transmit(Node* n, Packet* p, IPAddr_t dst, Count_t ttl,
			    Proto_t l4proto, DSROptionsHeader* dsrhdr)
{
  IPV4Header* iphdr = AddIPHeader(p, dst, ttl, l4proto);
  iphdr->options.push_back(dsrhdr);
  iphdr->totalLength = (Word_t)p->SizeUpdate();

  if (dst == IPAddrBroadcast)
    // Broadcast
    {
      Broadcast(n->Interfaces()[0], p);
    }
  else {
    // Unicast
    DSROptions* dsr_opt = (DSROptions*)dsrhdr->options[0];
    if (dsr_opt->type != SOURCEROUTE_ID) {
      DEBUG0(cout << "DSR option type is wrong\n");
      delete p;
      return;
    }
    // Process DSR option
    SourceRouteOption* sr = (SourceRouteOption*)dsr_opt;
    RoutingEntry re = sr->LookupNextHop(n);  // get the next hop
    Unicast(re, p);
  }
}

void RoutingDSR::Broadcast(Interface* i, Packet* p)
{
  // assumes all broadcast is control packet
  MACAddr dst;
  dst.SetBroadcast();
  i->Send(p, dst, IPV4::Instance()->Proto());
}

void RoutingDSR::Unicast(RoutingEntry& re, Packet* p)
{
  // register L2 xmit failure handler
  if (enableL2Notify)
#ifndef WIN32
    ((L2Proto802_11*)re.interface->GetL2Proto())->xmitFailHandler = this;
  re.interface->Send(p, re.nexthop, IPV4::Instance()->Proto());
#else
    ((L2Proto802_11*)re.interface_->GetL2Proto())->xmitFailHandler = this;
  re.interface_->Send(p, re.nexthop, IPV4::Instance()->Proto());
#endif
}

SourceRouteOption* RoutingDSR::GetSourceRoute(IPAddr_t dst, bool move)
{
  RtCache_t::iterator pi, si;
  SourceRoute_t::size_type ps, ss;
  bool p = primaryCache.FindRoute(dst, pi, ps);
  bool s = secondaryCache.FindRoute(dst, si, ss);

  SourceRoute_t rt;
  if (p && s) {
    if (ps <= ss) {
      for (SourceRoute_t::size_type k = 0; k <= ps; ++k)
        rt.push_back(pi->route[k]);
      return new SourceRouteOption(rt);
    }
    for (SourceRoute_t::size_type k = 0; k <= ss; ++k)
      rt.push_back(si->route[k]);
    if (move) {
      // move to the primary cache
      primaryCache.AddRoute(si->route);
      secondaryCache.cache.erase(si);
    }
    return new SourceRouteOption(rt);
  }
  if (p) {
    for (SourceRoute_t::size_type k = 0; k <= ps; ++k)
      rt.push_back(pi->route[k]);
    return new SourceRouteOption(rt);
  }
  if (s) {
    for (SourceRoute_t::size_type k = 0; k <= ss; ++k)
      rt.push_back(si->route[k]);
    if (move) {
      // move to the primary cache
      primaryCache.AddRoute(si->route);
      secondaryCache.cache.erase(si);
    }
    return new SourceRouteOption(rt);
  }
  return nil;
}

void RoutingDSR::SendRouteRequest(IPAddr dst, Count_t ttl, Time_t timeout,
                                  bool prop)
{
  DEBUG0(cout << IPAddr::ToDotted(node->GetIPAddr()) << " Sending RREQ to "
	 << (string)IPAddr(dst) << " with ttl: " << ttl << " timeout: "
	 << timeout << endl);

  Packet* p = new Packet;
  DSROptionsHeader* dsrhdr = new DSROptionsHeader();
  RouteRequestOption* rreqOption
      = new RouteRequestOption(GetSeqID(), dst, node->GetIPAddr());
  dsrhdr->options.push_back(rreqOption);
  if (errorOption) {
    // if has route error option to be piggybacked on route request
    // check if it's stale
    if (Simulator::Now()-timeErrorOpt > MAX_ERR_HOLD) {
      delete errorOption;
      errorOption = nil;
    }
    else
      dsrhdr->options.push_back(errorOption->Copy());
  }

  // Broadcast
  L3Transmit(node, p, IPAddrBroadcast, ttl, Proto(), dsrhdr);

  AddRequest(rreqOption, ttl, timeout, prop);    // add to request table
}

bool RoutingDSR::IsRecentRequest(RouteRequestOption* rreq)
{
  // search request table for <src, dst, id>
  for (RequestTable_t::iterator i=rreqTable.begin(); i!=rreqTable.end(); ++i)
    {
      if (!i->used) continue;
      //if (i->src == rreq->src && i->dst == rreq->dst && i->id == rreq->seqID)
      if (i->src == rreq->src && i->id >= rreq->seqID)
        return true;
    }
  // not found, add to request table
  AddRequest(rreq, 0, 0);
  return false;
}

void RoutingDSR::AddRequest(RouteRequestOption* rreq, Count_t ttl, Time_t to,
                            bool prop)
{
  // get available slot in rreqTable
  RequestTable_t::size_type i;
  for (i = 0; i < rreqTable.size(); ++i)
    if (!rreqTable[i].used) break;
  if (i == rreqTable.size()) {  // no available one
    if (i < RequestTableSize)
      rreqTable.push_back(RequestEntry());  // add one
    else {
      // table is full, find LRU one
      Time_t oldest = Simulator::Now();
      for (RequestTable_t::size_type k = 0; k < rreqTable.size(); ++k) {
        if (rreqTable[k].timer) continue;
        if (rreqTable[k].timestamp < oldest) {
          oldest = rreqTable[k].timestamp;
          i = k;
        }
      }
    }
  }

  // store entry
  rreqTable[i] = RequestEntry(rreq->src, rreq->dst, rreq->seqID,
                              (Byte_t)ttl, to, Simulator::Now(), nil);

  if (ttl) {
    rreqTable[i].prop = prop;
    rreqTable[i].timer = new DSREvent(DSREvent::REQUEST_TIMEOUT, i);

    // schedule timer
    ScheduleTimer(DSREvent::REQUEST_TIMEOUT, rreqTable[i].timer, to);
  }
}

void RoutingDSR::AddSourceRoute(const SourceRoute_t& rt, bool primary)
{
  if (primary)
    primaryCache.AddRoute(rt);
  else
    secondaryCache.AddRoute(rt);
  if (!evRtCache) ScheduleTimer(DSREvent::ROUTE_CACHE, evRtCache, 1);
}

bool RoutingDSR::IsRequestPending(IPAddr_t dst)
{
  RequestTable_t::iterator i;
  for (i = rreqTable.begin(); i != rreqTable.end(); ++i) {
    if (!i->used) continue;
    if (i->src == node->GetIPAddr() && i->dst == dst) return true;
  }
  return false;
}

void RoutingDSR::BufferPacket(Packet* p)
{
  if (sendBuffer.size() >= defaultSendBufSize) {
    DEBUG0(cout << "send buffer full oldest dropped\n");
    SendBuf_t::iterator i = sendBuffer.begin();
    delete i->p;  // L3 drop
    sendBuffer.pop_front();
  }

  // Store packet
  sendBuffer.push_back(SendBufEntry(p, Simulator::Now()));
  if (!evBufCheck)
    ScheduleTimer(DSREvent::BUF_CHECK, evBufCheck, defaultBufCheck);
}

void RoutingDSR::FlushBuffer(IPAddr_t dst)
{
  DEBUG0(cout << MYIP << ": Flushing Buffer for destination "
	 << (string)IPAddr(dst) << endl);

  SendBuf_t::iterator i = sendBuffer.begin();
  while (i != sendBuffer.end())
    {
      IPV4Header* iphdr = (IPV4Header*)i->p->PeekPDU();
      if (dst == iphdr->dst)
        {
          DataSend(i->p);
          sendBuffer.erase(i++);
          continue;
        }
      ++i;
    }
}

// called when rrep arrives
void RoutingDSR::FlushBufferPeriod(IPAddr_t dst)
{
  SendBuf_t::iterator i;
  for (i = sendBuffer.begin(); i != sendBuffer.end(); ++i)
    {
      IPV4Header* iphdr = (IPV4Header*)i->p->PeekPDU();
      if (dst == iphdr->dst) break;
    }
  if (i == sendBuffer.end()) return;  // not found

  DataSend(i->p);
  sendBuffer.erase(i);
  if (!evSendBuf && !sendBuffer.empty())
    ScheduleTimer(DSREvent::SENDBUF, evSendBuf, defaultSendBufPeriod);
}

// called when timeout occurs
void RoutingDSR::FlushBufferPeriod()
{
  vector<IPAddr_t> dVec;
  bool needSchedule = false;

  SendBuf_t::iterator i = sendBuffer.begin();
  while (i != sendBuffer.end()) {
    IPV4Header* iphdr = (IPV4Header*)i->p->PeekPDU();
    vector<IPAddr_t>::iterator p = find(dVec.begin(), dVec.end(), iphdr->dst);
    if (p != dVec.end()) { ++i; continue; }  // already processed
    dVec.push_back(iphdr->dst);
    SourceRouteOption* sr = GetSourceRoute(iphdr->dst, true);
    if (sr) {
      DataSend(i->p, sr);
      sendBuffer.erase(i++);
      needSchedule = true;
    }
    else {
      if (!IsRequestPending(iphdr->dst))
        SendRouteRequest(iphdr->dst, 1, NonpropRequestTimeout);
      ++i;
    }
  }
  if (needSchedule)
    ScheduleTimer(DSREvent::SENDBUF, evSendBuf, defaultSendBufPeriod);
}

void RoutingDSR::CheckSendBuffer()
{
  Time_t now = Simulator::Now();
  vector<IPAddr_t> dVec;
  vector<IPAddr_t>::iterator p;
  for (SendBuf_t::iterator i=sendBuffer.begin(); i!=sendBuffer.end(); )
    {
      if (now - i->t > SendBufferTimeout) {
        delete i->p;  // L3 drop
        sendBuffer.erase(i++);
        continue;
      }
      IPV4Header* iphdr = (IPV4Header*)i->p->PeekPDU();
      p = find(dVec.begin(), dVec.end(), iphdr->dst);
      if (p != dVec.end()) { ++i; continue; }  // already processed
      dVec.push_back(iphdr->dst);
      SourceRouteOption* sr = GetSourceRoute(iphdr->dst);
      if (sr) delete sr;
      else if (!IsRequestPending(iphdr->dst)) {
        SendRouteRequest(iphdr->dst, 1, NonpropRequestTimeout);
        /*
        if (iphdr->options.empty())
          SendRouteRequest(iphdr->dst, 1, NonpropRequestTimeout);
        else {  // salvaged packet
          delete i->p;
          sendBuffer.erase(i++);
          continue;
        }
        */
      }
      ++i;
    }
}

bool RoutingDSR::FindSendBuffer(IPAddr_t dst)
{
  for (SendBuf_t::iterator i=sendBuffer.begin(); i!=sendBuffer.end(); ++i)
    {
      IPV4Header* iph = (IPV4Header*)i->p->PeekPDU();
      if (dst == iph->dst) return true;
    }
  return false;
}

void RoutingDSR::RemoveRoute(IPAddr_t ip1, IPAddr_t ip2)
{
  primaryCache.RemoveRoute(node, ip1, ip2);
  secondaryCache.RemoveRoute(node, ip1, ip2);
}

void RoutingDSR::Timeout(TimerEvent* pev)
{
  DSREvent* ev = (DSREvent*)pev;

  switch (ev->event) {
    case DSREvent::REQUEST_TIMEOUT: {
      int i = ev->h;
      delete ev;
      IPAddr_t dst = rreqTable[i].dst;
      DEBUG0(cout << MYIP << " Route Discovery to " << (string)IPAddr(dst)
             << " timed out\n");
      rreqTable[i].timer = nil;  // free timer
      rreqTable[i].used = 0;
      //if (!rreqTable[i].prop) break;  // nonpropagating rreq

      if (enableRingSearch) {
        // Expanding ring search
        if (rreqTable[i].ttl < MAX_SR_LEN) {
          Time_t to =
              rreqTable[i].ttl == 1 ? RequestPeriod : rreqTable[i].timeout*2;
          if (to > MaxRequestPeriod) to = MaxRequestPeriod;
          Byte_t ttl = rreqTable[i].ttl * 2;
          if (ttl > MAX_SR_LEN) ttl = MAX_SR_LEN;
          SendRouteRequest(rreqTable[i].dst, ttl, to);
        }
      }
      else {
        if (rreqTable[i].ttl == 1)
          SendRouteRequest(rreqTable[i].dst, MAX_SR_LEN, MaxRequestPeriod);
      }
      break;
    }

    case DSREvent::BROADCAST_JITTER: {
      Packet* p = rreqQue[ev->h].p;
      rreqQue[ev->h].timer = nil;  // free timer
      delete ev;
      IPV4Header* iph = (IPV4Header*)p->PeekPDU();
      if (IPAddrBroadcast == iph->dst)  // rreq
        Broadcast(node->Interfaces()[0], p);
      else {  // rrep
        DSROptionsHeader* dh = (DSROptionsHeader*)iph->options[0];
        SourceRouteOption* sr = (SourceRouteOption*)dh->options[0];
        RoutingEntry re = sr->LookupNextHop(node);
        Unicast(re, p); // Sending Ctrl Packet
      }
      break;
    }

    case DSREvent::BUF_CHECK:
      CheckSendBuffer();
      if (!sendBuffer.empty())
        ScheduleTimer(DSREvent::BUF_CHECK, evBufCheck, defaultBufCheck);
      else {
        delete ev;
        evBufCheck = nil;
      }
      break;

    case DSREvent::SENDBUF:
      delete ev;
      evSendBuf = nil;
      // send out the oldest packet in the SendBuffer
      if (!sendBuffer.empty()) FlushBufferPeriod();
      break;

    case DSREvent::ROUTE_CACHE:
      primaryCache.Maintain();
      secondaryCache.Maintain();
      if (primaryCache.cache.empty() && secondaryCache.cache.empty()) {
        delete ev;
        evRtCache = nil;
      }
      else
        ScheduleTimer(DSREvent::ROUTE_CACHE, evRtCache, 1);
      break;

    default:
      break;
  } //switch
}

void RoutingDSR::ScheduleTimer(Event_t ev, DSREvent*& tev, Time_t when)
{
  Time_t now = Simulator::Now();
  if (tev) {
    if (tev->Time() == now+when) return;
    CancelTimer(tev);
  }
  if (!tev) {
    tev = new DSREvent(ev);
  }

  timer.Schedule(tev, when, this);
}

void RoutingDSR::CancelTimer(DSREvent*& ev, bool delTimer)
{
  if (!ev) return;

  timer.Cancel(ev);
  if (delTimer) {
    delete ev;
    ev = nil;
  }
}

void RoutingDSR::CancelRequestTimer(IPAddr_t dst)
{
  // search request table for dst
  RequestTable_t::iterator i;
  for (i = rreqTable.begin(); i != rreqTable.end(); ++i)
    {
      if (!i->used) continue;
      if (i->src==node->GetIPAddr() && i->dst == dst) {
        CancelTimer(i->timer, true);
        i->used = 0;
        return;
      }
    }
}

bool RoutingDSR::DataIndication(Interface* i, Packet* p)
{
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
  DSROptionsHeader* dsrhdr = (DSROptionsHeader*)iphdr->options[0];
  DSROptions* dsrOption = (DSROptions*)dsrhdr->options[0];
  Node* n = i->GetNode();

  switch (dsrOption->type)
    {
      case ROUTEREQUEST_ID:
        return ProcessRouteRequest(n, p);

      case SOURCEROUTE_ID:
        return ProcessSourceRoute(n, p, dsrhdr);

      default:
        DEBUG0(cout << IPAddr::ToDotted(node->GetIPAddr())
               << " Unknown DSR option type in packet"
               << p->uid << endl);
        delete p;
        break;
    }
  return true;
}

bool RoutingDSR::ProcessRouteRequest(Node* n, Packet* p)
{
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
  DSROptionsHeader* dsrhdr = (DSROptionsHeader*)iphdr->options[0];
  RouteRequestOption* rreqOpt = (RouteRequestOption*)dsrhdr->options[0];

  // 1. check target address field in rreq option
  if (n->GetIPAddr() == rreqOpt->dst) {
    // rreq reached destination, send reply
    DEBUG0(cout << MYIP << ": RREQ reached target from src "
           << (string)iphdr->src << endl);

    SourceRoute_t foreRoute = rreqOpt->routeRecord;
    foreRoute.push_back(rreqOpt->dst);
    SourceRoute_t revRoute = ReverseRoute(rreqOpt->routeRecord);
    revRoute.push_back(iphdr->src);

    // cache source route to rreq originator
    AddSourceRoute(revRoute, false);  // secondary cache

    //SourceRouteOption* replySROption = GetSourceRoute(iphdr->src);

    // rrep packet
    Packet* rp = new Packet();
    DSROptionsHeader* hdr = new DSROptionsHeader();
    SourceRouteOption* srOpt = new SourceRouteOption(revRoute);
    RouteReplyOption* rrepOpt = new RouteReplyOption(foreRoute, rreqOpt->seqID);
    hdr->options.push_back(srOpt);
    hdr->options.push_back(rrepOpt);

    // unicast
    L3Transmit(n, rp, iphdr->src, 255, Proto(), hdr);

    /*
    // add jitter instead of immediate reply
    IPV4Header* iph = AddIPHeader(rp, iphdr->src, 255, Proto());
    iph->options.push_back(hdr);
    iph->totalLength = rp->SizeUpdate();
    AddBroadcastJitter(rp, urvJitter->Value());
    */

    delete p;
    return true;
  }

  // 2. check if own ip appears in the address list of rreq option
  if (rreqOpt->IsIPAdded(n->GetIPAddr())) {
    DEBUG(1, cout << MYIP << ": Routing Loop in RREQ to "
	  << (string)rreqOpt->dst << ", dropping packet: " << p->uid << endl);
    delete p;
    return true;
  }

  // 3. blacklist check, not supported currently
  // 4.

  // 5. check if this RREQ option is recently received
  if (IsRecentRequest(rreqOpt)) {
    DEBUG(1, cout << MYIP << ": Dropping Recently Forwarded RREQ to "
	  << (string)rreqOpt->dst << ", dropping packet: " << p->uid << endl);
    delete p;
    return true;
  }

  // process piggybacked route error option if present
  if (dsrhdr->options.size() > 1) {
    DSROptions* dsrOpt2 = (DSROptions*)dsrhdr->options[1];
    if (dsrOpt2->type == ROUTEERROR_ID) {
      RouteErrorOption* rerrOpt = (RouteErrorOption*)dsrOpt2;
      //RemoveRoute(rerrOpt->errorSrc, rerrOpt->info);
      //RemoveRoute(rerrOpt->info, rerrOpt->errorSrc);
      ProcessRouteError(rerrOpt);
    }
  }

  // 6. further process rreq option
  // 6-0. cache source route to rreq originator
  //SourceRoute_t revRoute = ReverseRoute(rreqOpt->routeRecord);
  //revRoute.push_back(iphdr->src);
  //AddSourceRoute(revRoute, false);  // secondary cache

  // 6-1. add entry for this rreq option in id fifo cache
  // this was done by step 5

  // 6-2. append own ip to address list in rreq option
  rreqOpt->AddHop(n->GetIPAddr());

  // 6-3. search own route cache for route to target of rreq option
  // if found, return cached route reply
  SourceRouteOption* sr_opt = GetSourceRoute(rreqOpt->dst);
  if (sr_opt) {
    SourceRoute_t rt = sr_opt->route;
    delete sr_opt;
    // found in cache, return route reply
    // check possible loop
    SourceRoute_t& rec = rreqOpt->routeRecord;
    //Oct-27-05: Gurashish
   //Bug: The src of route request is not included in the routeRecord
   //     So if the route in cache to the dst contains the src the cycle
   //     will not be detected.
   //Hack: Adding Src to the routeRecord, for cycle detection
   //	   Since rest of code doesnt expect src to be in the routeRecord
   //	   So we remove the src later.
    rec.push_back(iphdr->src);

    for (SourceRoute_t::size_type k = 0; k < rt.size(); ++k)
      {
        SourceRoute_t::iterator it = find(rec.begin(), rec.end(), rt[k]);
        if (it != rec.end()) {  // found
          // do not reply, forward the rreq
          // check ttl to determine to drop or forward rreq option
          if (!iphdr->ttl) {
            // ttl expired, drop rreq
            DEBUG(1, cout << MYIP << ": RREQ TTL expired, dropping packet "
                  << p->uid << endl);
            delete p;
            return true;
          }
	//Undo the SRC insertion
	rec.pop_back();

          iphdr->totalLength = (Word_t)p->SizeUpdate();
          // link re-broadcast with delay of random period [0, BroadcastJitter]
          Time_t jitter = urvJitter->Value();
          DEBUG(1, cout << MYIP << ": Forwarding RREQ to "
                << (string)rreqOpt->dst << " using broadcast jitter "
                << jitter << endl);
          if (!jitter)
            Broadcast(n->Interfaces()[0], p);
          else
            AddBroadcastJitter(p, jitter);
          return true;
        }
      } //for

   //Undo the SRC insertion
    rec.pop_back();

    // maximum route length check
    if (rreqOpt->routeRecord.size() + rt.size() > MAX_SR_LEN) {
      delete p;
      return true;
    }

    // form reverse source route
    SourceRoute_t temp = rreqOpt->routeRecord;
    temp.pop_back();
    SourceRoute_t revRoute = ReverseRoute(temp);
    revRoute.push_back(iphdr->src);

    // form forward source route
    SourceRoute_t forRoute = rreqOpt->routeRecord;
    for (SourceRoute_t::size_type k = 0; k < rt.size(); ++k)
      forRoute.push_back(rt[k]);
    
    // rrep packet
    Packet* rp = new Packet();
    DSROptionsHeader* hdr = new DSROptionsHeader();
    SourceRouteOption* srOpt = new SourceRouteOption(revRoute);
    RouteReplyOption* rrepOpt = new RouteReplyOption(forRoute, rreqOpt->seqID);
    hdr->options.push_back(srOpt);
    hdr->options.push_back(rrepOpt);

    // unicast
    L3Transmit(n, rp, iphdr->src, 255, Proto(), hdr);

    delete p;
    return true;
  } //if (sr_opt)

  // check ttl to determine to drop or forward rreq option
  if (!iphdr->ttl) {
    // ttl expired, drop rreq
    DEBUG(1, cout << MYIP << ": RREQ TTL expired, dropping packet "
          << p->uid << endl);
    delete p;
    return true;
  }

  // 6-4. link re-broadcast with delay of random period [0, BroadcastJitter]
  iphdr->totalLength = (Word_t)p->SizeUpdate();
  Time_t jitter = urvJitter->Value();
  DEBUG(1, cout << MYIP << " Forwarding RREQ to " << (string)rreqOpt->dst
        << " using broadcast jitter " << jitter << endl);
  if (!jitter)
    Broadcast(n->Interfaces()[0], p);
  else
    AddBroadcastJitter(p, jitter);
  return true;
}

bool RoutingDSR::ProcessSourceRoute(Node* n, Packet* p, DSROptionsHeader* hdr)
{
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
  SourceRouteOption* srOpt = (SourceRouteOption*)hdr->options[0];

  // check for automatic route shortening
  // not implemented

  if (!n->LocalIP(iphdr->dst)) {
    // need to forward

    // process snooped packet before forwarding
    ProcessSnoopedPacket(n, p, hdr);

    // check ttl in ip header
    if (!iphdr->ttl) {
      delete p;
      return true;
    }

    // unicast to next hop
    RoutingEntry re = srOpt->LookupNextHop(n);
    Unicast(re, p);
    return true;
  }

  // destined for this node
  if (hdr->options.size() == 1) {
    // no more dsr options, carry L4 data
    return false;
  }

  // more dsr options
  DSROptions* dsrOpt2 = (DSROptions*)hdr->options[1];
  switch (dsrOpt2->type) {
    case ROUTEREPLY_ID: {
      RouteReplyOption* rrepOpt = (RouteReplyOption*)dsrOpt2;

      // cancel route request timer and delete from request table
      CancelRequestTimer(rrepOpt->GetRouteDst());

      // cache route to target
      AddSourceRoute(rrepOpt->GetRoute());  // primary cache

      // print discovered route
      DEBUG0(cout << MYIP << ": Discovered Route time "
             << Simulator::Now() << " -> ");
      DEBUG0(PrintRoute(rrepOpt->GetRoute()));

      // handle pending packets
      if (defaultSendBufPeriod <= 0)
        FlushBuffer(rrepOpt->GetRouteDst());  // immediatley
      else
        FlushBufferPeriod(rrepOpt->GetRouteDst());  // periodically

      break;
    }

    case ROUTEERROR_ID: {
      RouteErrorOption* rerrOpt = (RouteErrorOption*)dsrOpt2;
      DEBUG0(cout << MYIP << ": Received RERR from " << (string)iphdr->src
             << " in link: " << (string)IPAddr(rerrOpt->errorSrc)
             << " -> " << (string)IPAddr(rerrOpt->info) << endl);
      ProcessRouteError(rerrOpt);
      break;
    }

    default:
      DEBUG0(cout << "ERROR::Something went wrong "
             << "option after source route has wrong number\n");
      break;
  } //switch (dsrOpt2->type)
  delete p;
  return true;
}

void RoutingDSR::ProcessSnoopedPacket(Node* n, Packet* p, DSROptionsHeader* hdr)
{
  // this function assumes at least one DSROption
  // the first option can be either a Source Route or Route Request
  if (hdr->options.size() <= 1) return;    // nothing to process
  DSROptions* dsrOpt = (DSROptions*)hdr->options[1];  // secondary option

  switch(dsrOpt->type) {
    case ROUTEREPLY_ID: {
      RouteReplyOption* rrepOpt = (RouteReplyOption*)dsrOpt;

      // cache route to dst in rrep option
      SourceRoute_t::size_type k = 0;
      for (; k < rrepOpt->recordedRoute.size(); ++k)
        if (rrepOpt->recordedRoute[k] == n->GetIPAddr()) break;
      SourceRoute_t rt;
      SourceRoute_t::size_type i;
      for (i=k+1; i<rrepOpt->recordedRoute.size(); ++i)
        rt.push_back(rrepOpt->recordedRoute[i]);
      if (!rt.empty())
        AddSourceRoute(rt, false);  // secondary cache

      // cache route to src in rrep option
      SourceRoute_t rt2;
      if (k > 0) {
        for (i=k-1; i>0; --i)
          rt2.push_back(rrepOpt->recordedRoute[i]);
        rt2.push_back(rrepOpt->recordedRoute[i]);
      }
      IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
      rt2.push_back(iphdr->dst);
      AddSourceRoute(rt2, false);  // secondary cache
/*
      // print discovered route
      DEBUG0(cout << MYIP << ": Discovered Route time "
             << Simulator::Now() << " -> ");
      DEBUG0(PrintRoute(rrepOpt->GetRoute()));

      // handle pending packets for RREP SRC
      if (defaultSendBufPeriod <= 0)
        FlushBuffer(rrepOpt->GetRouteDst());  // immediatley
      else
        FlushBufferPeriod(rrepOpt->GetRouteDst());  // periodically

      // handle pending packets for RREP DST
      if (defaultSendBufPeriod <= 0)
        FlushBuffer(rrepOpt->GetRouteDst());  // immediatley
      else
        FlushBufferPeriod(rrepOpt->GetRouteDst());  // periodically
*/


      break;
    }

    case ROUTEERROR_ID: {
      RouteErrorOption* rerrOpt = (RouteErrorOption*)dsrOpt;
      //RemoveRoute(rerrOpt->errorSrc, rerrOpt->info);
      //RemoveRoute(rerrOpt->info, rerrOpt->errorSrc);
      ProcessRouteError(rerrOpt);
      break;
    }

    default:
      DEBUG0(cout << MYIP << " Not processing DSROption of type: "
             << dsrOpt->type << endl);
  }
}

void RoutingDSR::AddBroadcastJitter(Packet* p, Time_t jitter)
{
  // Find available slot in rreqQue
  RReqQue_t::size_type i;
  for (i = 0; i < rreqQue.size(); ++i)
    if (!rreqQue[i].timer) break;
  if (i == rreqQue.size()) rreqQue.push_back(RReqQueItem());

  // Store RREQ
  rreqQue[i].p = p;
  rreqQue[i].timer = new DSREvent(DSREvent::BROADCAST_JITTER, i);
  //DEBUG0(cout << "# of delayed RREQs " << rreqQue.size() << endl);

  // Schedule timer
  ScheduleTimer(DSREvent::BROADCAST_JITTER, rreqQue[i].timer, jitter);
}

void RoutingDSR::FormErrorPacket(Node* n, Packet* p)
{
  // assumes that p was passed up by L2 error notification
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
  DSROptionsHeader* dsrhdr = (DSROptionsHeader*)iphdr->options[0];
  SourceRouteOption* srOpt = (SourceRouteOption*)dsrhdr->options[0];
  IPAddr_t target = !srOpt->salvage ? iphdr->src.ip : srOpt->route[0];
  RouteErrorOption* rerrOpt =
      new RouteErrorOption(NODE_UNREACHABLE, n->GetIPAddr(), target);
  rerrOpt->info = srOpt->route[srOpt->hopIndex-1];

  ProcessRouteError(rerrOpt);

  if (n->GetIPAddr() == target) {
    // the packet is originated by this node
    delete rerrOpt;
  }
  else {
    // return error info to target
    // use reverse route of err packet
    SourceRoute_t rt = srOpt->route;
    rt.erase(rt.begin()+srOpt->hopIndex-2, rt.end());
    rt = ReverseRoute(rt);
    if (!srOpt->salvage) rt.push_back(iphdr->src);
    AddSourceRoute(rt, false);  // secondary cache
    SourceRouteOption* errsrOpt = new SourceRouteOption(rt);

    // RERR packet
    Packet* ep = new Packet();
    DSROptionsHeader* dsrh = new DSROptionsHeader();
    dsrh->options.push_back(errsrOpt);
    dsrh->options.push_back(rerrOpt);

    // Unicast
    L3Transmit(n, ep, errsrOpt->GetDst(), 255, Proto(), dsrh);
  }

  // salvage the packet if possible
  // do not salvage RREP packet
  if (dsrhdr->options.size() > 1) {
    DSROptions* opt = (DSROptions*)dsrhdr->options[1];
    if (opt->type == ROUTEREPLY_ID) {
      delete p;
      return;
    }
  }
  if (++srOpt->salvage > MAX_SALVAGE_COUNT) { delete p; return; }

  srOpt->hopIndex = 1;
  srOpt->route.clear();
  srOpt->route.push_back(n->GetIPAddr());  // salvaging node
  SourceRouteOption* sr = GetSourceRoute(iphdr->dst);
  if (sr) {
    for (SourceRoute_t::iterator i=sr->route.begin(); i!=sr->route.end(); ++i)
      srOpt->route.push_back(*i);
    delete sr;
    iphdr->totalLength = (Word_t)p->SizeUpdate();
    RoutingEntry re = srOpt->LookupNextHop(n);
    Unicast(re, p);
  }
  else {
    /*
    // nonpropagating RREQ
    BufferPacket(p);
    if (!IsRequestPending(iphdr->dst))
      SendRouteRequest(iphdr->dst, 1, NonpropRequestTimeout, false);
    */
    delete p;
  }
}

void RoutingDSR::ProcessRouteError(RouteErrorOption* rerr)
{
  // remove routes including error link
  RemoveRoute(rerr->errorSrc, rerr->info);
  RemoveRoute(rerr->info, rerr->errorSrc);

  // save route error option for later piggyback on route request
  if (errorOption) delete errorOption;
  errorOption = (RouteErrorOption*)rerr->Copy();
  timeErrorOpt = Simulator::Now();
}

// static methods
SourceRoute_t RoutingDSR::ReverseRoute(SourceRoute_t& route)
{
  SourceRoute_t reverseRoute;
  SourceRoute_t::reverse_iterator ri;
  for(ri = route.rbegin(); ri != route.rend(); ++ri)
    reverseRoute.push_back(*ri);
  return reverseRoute;
}

void RoutingDSR::PrintRoute(const SourceRoute_t& route)
{
  for (SourceRoute_t::size_type i = 0; i < route.size(); ++i)
    if (i != 0) {
      cout << "->";
      if (ROUTE_DOTTED)
        cout << (string)IPAddr(route[i]);
      else
        cout << (route[i] & ROUTE_MASK);
    }
    else {
      if (ROUTE_DOTTED)
        cout << (string)IPAddr(route[i]);
      else
        cout << (route[i] & ROUTE_MASK);
    }
  cout << endl;
}

IPV4Header*
RoutingDSR::AddIPHeader(Packet* p, IPAddr_t dst, Count_t ttl, Proto_t l4proto)
{
  IPV4Header* iphdr = new IPV4Header();
  iphdr->src = node->GetIPAddr();
  iphdr->dst = dst;
  iphdr->ttl = (Byte_t)ttl;
  iphdr->protocol = (Byte_t)l4proto;
  p->PushPDU(iphdr);
  return iphdr;
}

void RoutingDSR::Notify(void* v)
{
  // handle transmit failure notification from L2
  Packet* p = (Packet*) v;
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
  if (IPAddrBroadcast == iphdr->dst) {
    // don't process broadcast packet
    delete p;
    return;
  }

  // Clear pkts over broken link from ifq
  vector<IPAddr_t> sVec;
  vector<Packet*> pVec;
  IPV4Header* iph = (IPV4Header*)p->PeekPDU();
  sVec.push_back(iph->src);
  pVec.push_back(p);

  // identify dst mac addr
  //CHANGED GURASHISH
  //09/05/05
  //The MAC and LLC PDUs are not available at this layer
  //This code is a hack, which doesnt work everytime
  //DATA_frame *df = (DATA_frame*)p->PeekPDU(-2);
  //MACAddr dst = df->addr1;
  //Using ARP/GTNetS-support functions to resolve MAC
  //Address
  MACAddr dst=node->Interfaces()[0]->IPToMac(iph->src);

  // get the pointer to the ifq
  Queue* ifq = node->Interfaces()[0]->GetQueue();

  // search the ifq for dst
  while ((p = ifq->DequeOneDstMac(dst)))
    {
      iph = (IPV4Header*)p->PeekPDU(2);
      vector<IPAddr_t>::iterator j = find(sVec.begin(), sVec.end(), iph->src);
      if (j == sVec.end()) {  // not found
        sVec.push_back(iph->src);
        p->PopPDU();  // remove L2 header
        p->PopPDU();  // remove LLCSNAP header
        pVec.push_back(p);
      }
      else {
        delete p;  // drop
        Stats::pktsDropped++;
      }
    }

  //FormErrorPacket(node, p);
  for (vector<Packet*>::size_type k = 0; k < pVec.size(); ++k)
    FormErrorPacket(node, pVec[k]);
}
