// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: eval.c 92 2004-09-14 18:18:43Z dheeraj $



/* 
 * 
 * eval.c -- evaluation routines.
 *
 */

#include <stdio.h>
#include <malloc.h>
#include "gb_graph.h"
#include "gb_dijk.h"
#include "eval.h"
#include <assert.h>

int finddegdist(Graph *g, int** degdist)
{
    Vertex *v;
    Arc *a;
    int max = 0, *deg, idx, i;

    deg = (int *)malloc((g->n)*sizeof(int));
    for (v = g->vertices,i=0; i<g->n; i++,v++) {
	deg[i] = 0;
	for (a = v->arcs; a; a = a->next)
		deg[i]++;
	if (deg[i] > max) max = deg[i];
    }
    *degdist = (int *)calloc(max+1,sizeof(int));
    for (v = g->vertices,i=0; i < g->n; i++, v++) {
	(*degdist)[deg[i]]++;
    }
    return max;
}

#define first(g)  g->vertices
#define BIGINT 0x7fffffff
#define MAXBINS	100
#define BINCONST  10		/* number of bins in pathlen dist = n*BINCONST */

static int *depth;
static int min, max;
static float avg;

void
dopaths(Graph *g, enum Field f0, enum Field f1, int *rmin, int *rmax, float *ravg)
{
    Vertex *u, *v;
    Arc *a;
    int idx, i,j, sum;

    depth = (int *)calloc(g->n,sizeof(int)); 

    /* note that bins really should be calculated using scale 	*/
    /*	 e.g., bins = c*scale, where c is some constant		*/
    /* scale can be extracted from g->id, but I don't want 	*/
    /*	 to bother with that right now				*/
    /*
     *    bins = BINCONST*g->n;
     *    pathlendist = (int *)calloc(bins+1,sizeof(int)); 
     *    maxpathlen = 0;
     */

    max = 0;
    min = BIGINT;
    sum = 0;
    for (v = first(g),i=0; i< g->n; i++,v++) {
        twofield_sptree(g,v,f0,f1);	/* sets mdist for each node */
        depth[i] = 0;	/* paranoia */
        for (u = first(g),j=0; j<g->n; j++,u++) { 
	    if (u->mdist > depth[i]) depth[i] = u->mdist;

	    /* if (u->dist > maxpathlen) maxpathlen = u->dist; */
	    /* pathlendist[bins] counts all lengths >= bins */
	    /* if (u->dist > bins) pathlendist[bins]++; */
	    /* else pathlendist[u->dist]++; */
	} 
	sum += depth[i];
	if (depth[i] < min) min = depth[i];
        if (depth[i] > max) max = depth[i];
    }
    avg = (float)sum/g->n;
    *rmax = max;
    *rmin = min;
    *ravg = avg;
}

/* convert depth data into a distribution */
/* assumes dopaths has already been called, and that min, max, depth */
/*     have valid data */
void
dodepthdist(Graph *g, int** ddist)
{
int num, i;

  num = max - min + 1;
  *ddist = (int *)calloc(num,sizeof(int));
  for (i = 0; i < g->n; i++) 
    (*ddist)[depth[i] - min]++;
}


#define rank z.I
#define parent y.V
#define untagged x.A
#define link w.V
#define min v.V

#define idx(g,v) v-g->vertices

/* count bicomponents. */
/* verbose=0 means no printouts, just return the number. */
int bicomp(Graph *g,int verbose)
{
    register Vertex *v;
    long            nn;
    Vertex          dummy;
    Vertex         *active_stack;
    Vertex         *vv;
    Vertex         *artic_pt = NULL;

    int count = 0;	

    for (v = g->vertices; v < g->vertices + g->n; v++) {
	v->rank = 0;
	v->untagged = v->arcs;
        v->link = NULL;
    }
    nn = 0;
    active_stack = NULL;
    dummy.rank = 0;

    for (vv = g->vertices; vv < g->vertices + g->n; vv++)
	if (vv->rank == 0) {
	    v = vv;
	    v->parent = &dummy;
	    v->rank = ++nn;
	    v->link = active_stack;
	    active_stack = v;
	    v->min = v->parent;
	    do {
		register Vertex *u;
		register Arc   *a = v->untagged;
		if (a) {
		    u = a->tip;
		    v->untagged = a->next;
		    if (u->rank) {
			if (u->rank < v->min->rank)
			    v->min = u;
		    } else {
			u->parent = v;
			v = u;
			v->rank = ++nn;
			v->link = active_stack;
			active_stack = v;
			v->min = v->parent;
		    }
		} else {
		    u = v->parent;
		    if (v->min == u)	/* 19: */
			if (u == &dummy) {
			    if (verbose) {
			    if (artic_pt)
				printf(" and %d (this ends a connected 	component of the graph)\n", idx(g, artic_pt));
			    else
				printf("Isolated vertex %d\n", idx(g, v));
			    }
			    active_stack = artic_pt = NULL;
			} else {
			    register Vertex *t;

			    if (artic_pt && verbose)
				printf(" and articulation point %d\n", 
				idx(g, artic_pt));
			    t = active_stack;
			    active_stack = v->link;
			    if (verbose) printf("Bicomponent %d", idx(g,v));
			    count++;
			    if (verbose) {
			    if (t == v)
				putchar('\n');
			    else {
				printf(" also includes:\n");
				while (t != v) {
				    printf(" %d (from %d; ..to %d)\n",
				       idx(g,t),idx(g,t->parent),idx(g,t->min));
				    t = t->link;
				}
			    }
			    }
			    artic_pt = u;
			}
		    else if (v->min->rank < u->min->rank)
			u->min = v->min;
		    v = u;
		}
	    }

	    while (v != &dummy);
	}
    return count;
}


void twofield_sptree(g,u,f0,f1)
Graph*g;
Vertex*u;
enum Field f0,f1;
{
	Vertex *v, *t;
	Arc *r;
  	int newdist;

	for (t = g->vertices; t < g->vertices + g->n; t++) {
		t->backlink = NULL;
		t->dist = BIGINT;
		t->mdist = 0;
	}
	u->backlink = u;
	u->dist = 0;
	u->mdist = 0;
	t = u;
	(*init_queue)(0L);	/* queue contains only unknown v's */
	while (t != NULL) {
	/* invariant: u is known => u->mdist and u->dist are correct */
	    for (r = t->arcs; r; r=r->next) {
		v = r->tip;
		/* newdist = t->dist + r->len; */
		switch (f0) {
		case Len: newdist = t->dist + r->len; break;
		case A: newdist = t->dist + r->a.I; break;
		case B: newdist = t->dist + r->b.I; break;
		case Hops: newdist = t->dist + 1;
		}
		
		if (v->backlink) { /* v was seen but not known */
		    if (newdist < v->dist) {
			v->backlink = t;
			/* v->hops = t->hops + 1; */
			switch (f1) {
			case Len: v->mdist = t->mdist + r->len; break;
			case A: v->mdist = t->mdist + r->a.I; break;
			case B: v->mdist = t->mdist + r->b.I; break;
			case Hops: v->mdist = t->mdist + 1;
			}
			(*requeue)(v,newdist);
		    }
	   	}
		else { /* newly seen */
		    v->backlink = t;
		    /* v->hops = t->hops + 1; */
		    switch (f1) {
			case Len: v->mdist = t->mdist + r->len; break;
			case A: v->mdist = t->mdist + r->a.I; break;
			case B: v->mdist = t->mdist + r->b.I; break;
			case Hops: v->mdist = t->mdist + 1;
			}
		    (*enqueue)(v,newdist);
		}
	    }
	    t = (*del_min)();
        }

}


