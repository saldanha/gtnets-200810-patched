// Create the Topology for the NMS Scalability Demonstrations per David Nicol
// George F. Riley.  Georgia Tech, Fall 2002

#include <ctype.h>
#include <iostream>
#include <iomanip>
#include <cstring>

//#define DEBUG_MASK 0x06
#include "debug.h"
#include "simulator.h"
#include "distributed-simulator.h"
#include "scheduler.h"
#include "application-tcpsend.h"
#include "application-tcpserver.h"
#include "duplexlink.h"
#include "queue.h"
#include "droptail.h"
#include "ratetimeparse.h"
#include "ipv4.h"
#include "tcp-tahoe.h"
#include "node.h"
#include "routing-nixvector.h"
#include "routing-static.h"
#include "ethernet.h"
#include "mask.h"
#include "rng.h"
#include "rtt-estimator.h"
#include "globalstats.h"
#include "udp.h"
#include "application-onoff.h"
#include "average-min-max.h"
#include "link-rti.h"
#include "timerbuckets.h"
#include "application-webbrowser.h"
#include "validation.h"

// Define a class for each network
// IP Addresses are assigned as follows:
// For each campus network, use "0.0.0.0" plus network number+1 * "0.1.0.0"
// Remote links to "next" campus is x.x+1.1.1
// Remote link to "prior" campus is x.(x+1-2).1.2
// Servers are x.x.0.[3..6] (four servers)
// Leaf networks (ethernets) are x.x.(l+2),[1..42]

using namespace std;

// Progress hook
static void Progress(Time_t now)
{
  double phSec = 0;
  if (Simulator::instance->RunTime() > 0)
    {
      phSec = Stats::pktsTransmitted / Simulator::instance->RunTime();
    }
  cout << "Prog " << Simulator::Now() 
       << " ph/Sec " << phSec
       << " aBr " << setw(3) << WebBrowser::activeBrowsers
       << " aCn " << setw(3) << WebBrowser::activeConnections
       << " sT " << setw(4) << WebBrowser::startedThisPeriod
       << " cT " << setw(4) << WebBrowser::completedThisPeriod
       << " cCn " << WebBrowser::completedConnections
       << endl;
  WebBrowser::startedThisPeriod = 0;
  WebBrowser::completedThisPeriod = 0;
}

Count_t  netCount = 20;
Count_t  totalNets; // netCount times number of federates
Count_t  baseNet;   // Starting network id (for distributed version)
bool     singleNet = false;
bool     singleflow = false;
bool     scaledDown = false;
AverageMinMax* averageMinMax = nil; // Statistics
bool     distributed = false;
Count_t  totalFlows = 0;
IPAddr_t netMult = IPAddr("0.1.0.0"); // Convert network number to baseip
class CampusLan;
typedef vector<CampusLan*> CLanVec_t;

// Web browsers stop making requests at IDLE_TIME
// Simulation finishes at STOP_TIME
#define IDLE_TIME  5400.0
//#define STOP_TIME  6000
#define STOP_TIME 500

class NMSNetwork {
public:
  NMSNetwork(IPAddr_t);
  Node* Gateway(); // Return the gateway
  Node* GetServer(Count_t);                 // Get specified server
  Node* GetNode(Count_t, Count_t, Count_t); // Get node from campus/enet/node
public:
  NodeVec_t net0;
  NodeVec_t net1;
  NodeVec_t net2;
  NodeVec_t net3;
  NodeVec_t servers;
  CLanVec_t campuses;
};

typedef vector<NMSNetwork*> NetVec_t;
typedef vector<TCPSend*>    TCPSendVec_t;
typedef vector<Interface*>  IFVec_t;

TCPSendVec_t tcpSends;
IFVec_t      interfaces;

class CampusLan {
public:
  CampusLan(Node*, IPAddr_t);      // Specifies gateway and IPAddress base
  Node* GetNode(Count_t, Count_t); // Get specified node from specified enet
  Ethernet* GetENet(Count_t);      // Get specified ethernet network
public:
  Node* lanRouter;         // The lan router object
  vector<Ethernet*> enets; // The ethernet objects
};

// NMSNetwork methods
NMSNetwork::NMSNetwork(IPAddr_t baseIP)
{
  // Number of routers in each subnet
#define N0Count 3     
#define N1Count 2
#define N2Count 7
#define N3Count 4
#define ServerCount 4

  // Create point-to-point link template to use
  Linkp2p l2Gb(Rate("2Gb"), Time("5ms")); // Link to use, 2Gb

  // Create Net 0
  for (int i = 0; i < N0Count; ++i)
    { // Three routers and three links
      net0.push_back(new Node());
      if (i) net0[i-1]->AddDuplexLink(net0[i], l2Gb);
    }
  net0[N0Count-1]->AddDuplexLink(net0[0], l2Gb);
  
  // Create net 1
  for (int i = 0; i < N1Count ; ++i)
    { // Two routers and one link
      net1.push_back(new Node());
      if(i)net1[i-1]->AddDuplexLink(net1[i], l2Gb);
    }
  // Create the servers on net 1
  for (int i = 0; i < ServerCount ; ++i)
    { // net1 servers
      servers.push_back(new Node());
      servers[i]->SetIPAddr(baseIP + i + 3);
      DEBUG(2,(cout << "Created server IPAddr "
              << (string)IPAddr(baseIP + i + 3)
              << endl));
      if (i < ServerCount / 2)
        servers[i]->AddDuplexLink(net1[0], l2Gb);
      else
        servers[i]->AddDuplexLink(net1[1], l2Gb);
      // Create a TCP server application
      TCPServer* s = (TCPServer*)servers[i]->AddApplication(
          TCPServer());
      s->BindAndListen(80);
      s->CloseOnEmpty();
      s->DeleteOnComplete();
      L4Protocol* p = s->GetL4();
      ((TCP*)p)->isServer = true;
      p->SetTrace(Trace::ENABLED);
    }
  
  // Connect net0 to net 1
  DuplexLink dl01(net0[2], l2Gb, net1[0], l2Gb);

  // Create the lone routers R4 and R5
  Node* R4 = new Node();
  Node* R5 = new Node();
  net0[0]->AddDuplexLink(R4, l2Gb);
  net0[1]->AddDuplexLink(R5, l2Gb);
  R4->AddDuplexLink(R5, l2Gb);

  // Create net 2
  for (int i = 0; i < N2Count; ++i)
    { // Seven routers and seven links
      net2.push_back(new Node());
    }
  // Router connectivity in net 2
  net2[0]->AddDuplexLink(net2[1], l2Gb);
  net2[1]->AddDuplexLink(net2[3], l2Gb);
  net2[2]->AddDuplexLink(net2[3], l2Gb);
  net2[0]->AddDuplexLink(net2[2], l2Gb);
  net2[2]->AddDuplexLink(net2[4], l2Gb);
  net2[3]->AddDuplexLink(net2[5], l2Gb);
  net2[5]->AddDuplexLink(net2[6], l2Gb);

  // Connectivity to R4
  net2[0]->AddDuplexLink(R4, l2Gb);
  net2[1]->AddDuplexLink(R4, l2Gb);

  IPAddr_t campusBaseIP = baseIP + IPAddr("0.0.2.1");
  IPAddr_t mult = IPAddr("0.0.4.0");
  // Create the net2 campus lans
  campuses.push_back(new CampusLan(net2[2], campusBaseIP + 0 * mult));
  campuses.push_back(new CampusLan(net2[3], campusBaseIP + 1 * mult));
  campuses.push_back(new CampusLan(net2[4], campusBaseIP + 2 * mult));
  campuses.push_back(new CampusLan(net2[5], campusBaseIP + 3 * mult));
  campuses.push_back(new CampusLan(net2[6], campusBaseIP + 4 * mult));
  campuses.push_back(new CampusLan(net2[6], campusBaseIP + 5 * mult));
  campuses.push_back(new CampusLan(net2[6], campusBaseIP + 6 * mult));
  
  // Create net 3
  for (int i = 0; i < N3Count; ++i)
    { // Seven routers and seven links
      net3.push_back(new Node());
    }
  net3[0]->AddDuplexLink(net3[1], l2Gb);
  net3[1]->AddDuplexLink(net3[2], l2Gb);
  net3[1]->AddDuplexLink(net3[3], l2Gb);
  net3[2]->AddDuplexLink(net3[3], l2Gb);

  // Connect to R5
  net3[0]->AddDuplexLink(R5, l2Gb);
  net3[1]->AddDuplexLink(R5, l2Gb);

  // Create the net3 campus lans
  campuses.push_back(new CampusLan(net3[0], campusBaseIP + 7 * mult));
  campuses.push_back(new CampusLan(net3[0], campusBaseIP + 8 * mult));
  campuses.push_back(new CampusLan(net3[2], campusBaseIP + 9 * mult));
  campuses.push_back(new CampusLan(net3[3], campusBaseIP + 10* mult));
  campuses.push_back(new CampusLan(net3[3], campusBaseIP + 11* mult));
}

Node* NMSNetwork::Gateway()
{ // Return the gateway (network 0, node 0)
  return net0[0];
}

Node* NMSNetwork::GetServer(Count_t i)
{ // Return specified server
  if (i >= servers.size()) return nil; // Out of range
  return servers[i];
}

Node* NMSNetwork::GetNode(Count_t cl, Count_t en, Count_t i)
{ // Get node from specified campus lan, ethernet, index
  if (cl >= campuses.size()) return nil;  // Out of range
  return campuses[cl]->GetNode(en, i);    // Get node from specified CampusLan
}

// Define a class for the CampusLans
CampusLan::CampusLan(Node* g, IPAddr_t ip)
{
  // Create a 1Gb link to use
  Linkp2p l1Gb(Rate("1Gb"), Time("5ms")); // Link to use, 1Gb

  lanRouter = new Node();
  g->AddDuplexLink(lanRouter, l1Gb);

  // Create the four ethernets with 10, 10, 10, and 12 nodes respectively
  if (scaledDown)
    {
      enets.push_back(new Ethernet(2, lanRouter,
                                   ip + 0x000, Mask(24),
                                   0, 
                                   Rate("100Mb"), Time("1ms")));
      enets.push_back(new Ethernet(2, lanRouter,
                                   ip + 0x100, Mask(24),
                                   0, 
                                   Rate("100Mb"), Time("1ms")));
      enets.push_back(new Ethernet(2, lanRouter,
                                   ip + 0x200, Mask(24),
                                   0, 
                                   Rate("100Mb"), Time("1ms")));
      enets.push_back(new Ethernet(2, lanRouter,
                                   ip + 0x300, Mask(24),
                                   0, 
                                   Rate("100Mb"), Time("1ms")));
    }
  else
    {
      DEBUG(1,(cout << "Creating enet, IP " 
               << (string)IPAddr(ip + 0x000) << endl));
      enets.push_back(new Ethernet(10, lanRouter,
                                   ip + 0x000, Mask(24),
                                   0, 
                                   Rate("100Mb"), Time("1ms")));
      DEBUG(1,(cout << "Creating enet, IP "
               << (string)IPAddr(ip + 0x100) << endl));
      enets.push_back(new Ethernet(10, lanRouter,
                                   ip + 0x100, Mask(24),
                                   0, 
                                   Rate("100Mb"), Time("1ms")));
      DEBUG(1,(cout << "Creating enet, IP " <<
               (string)IPAddr(ip + 0x200) << endl));
      enets.push_back(new Ethernet(10, lanRouter,
                                   ip + 0x200, Mask(24),
                                   0, 
                                   Rate("100Mb"), Time("1ms")));
      DEBUG(1,(cout << "Creating enet, IP "
               << (string)IPAddr(ip + 0x300) << endl));
      enets.push_back(new Ethernet(12, lanRouter,
                                   ip + 0x300, Mask(24),
                                   0, 
                                   Rate("100Mb"), Time("1ms")));
    }

  // Reduce queue sizes on leafs to reduce memory consumption
  //enets[0]->SetLeafQLimit(2000);
  //enets[1]->SetLeafQLimit(2000);
  //enets[2]->SetLeafQLimit(2000);
  //enets[3]->SetLeafQLimit(2000);

  // Connect the ethernet gateways to the lanRouter
  enets[0]->Gateway()->AddDuplexLink(lanRouter, l1Gb);
  enets[1]->Gateway()->AddDuplexLink(lanRouter, l1Gb);
  enets[2]->Gateway()->AddDuplexLink(lanRouter, l1Gb);
  enets[3]->Gateway()->AddDuplexLink(lanRouter, l1Gb);
}

Node* CampusLan::GetNode(Count_t lan, Count_t which)
{ // Set specified node from specified enet
  if (lan >= enets.size()) return nil; // Out of range
  return enets[lan]->GetNode(which);   // Get the specified node
}

Ethernet* CampusLan::GetENet(Count_t eni)
{ // Get the specified ethernet from a campus lan
  if (eni >= enets.size()) return nil; // Out of range
  return enets[eni];
}

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  bool            useNER = true;
  bool            seqEvents = false;
  bool            timerBuckets = false;
  string          rlinkDelayString("200ms");
  Count_t    myId;
  Count_t    nSys;

  Queue::Default(DropTail()); // Set default to drop tail
  if (argc > 1) netCount = atol(argv[1]);
  if (argc > 2)
    {
      // First convert to lower case
      int l = strlen(argv[2]);
      for (int i = 0; i < l; ++i) argv[2][i] = tolower(argv[2][i]);
      string arg(argv[2]);
      if (arg.find("single") != string::npos) singleNet = true;
      if (arg.find("scale") != string::npos) scaledDown = true;
      if (arg.find("ner") != string::npos) useNER = true;
      if (arg.find("tar") != string::npos) useNER = false;
      if (arg.find("dist") != string::npos) distributed = true;
      if (arg.find("link") != string::npos) seqEvents = true;
      if (arg.find("bucket") != string::npos) timerBuckets = true;
    }
  if (distributed && (argc < 5))
    {
      cout << "Distributed Usage: nmsweb nCampus keyword myid totsys" 
           << endl;
      exit(1);
    }
  if (singleNet) netCount = 1;
  if (!netCount)
    {
      cout << "Invalid Network Count " << argv[2] << endl;
      exit(1);
    }
  if (distributed)
    {
      myId = atol(argv[3]);
      nSys = atol(argv[4]);
      totalNets = netCount * nSys;
      baseNet = netCount * myId;
    }
  else
    {
      totalNets = netCount;
      baseNet = 0;
    }
  NetVec_t Networks;
  IPAddr_t baseIp = IPAddr("0.0.0.0") + netMult * baseNet;

  // Define the simulator object
  Simulator* s;
  if (distributed)
    {
      s =  new DistributedSimulator(myId);
      if (useNER)
        {
          cout << "Using NER time advance" << endl;
          // Specify NextEventReqest Time advance
          ((DistributedSimulator*)s)->UseNER();
        }
      cout << "Using lookahead of " << rlinkDelayString << endl;
    }
  else
    {
      s = new Simulator();
    }
  //s->CleanupOnExit(); // For debugging memory leaks
  // Specify NixVector routing
  //Routing::SetRouting(new RoutingNixVector());
  Routing::SetRouting(new RoutingStatic());  // Static routing for testing

  // Create the trace file
  //Trace* gs = Trace::Instance();
  //gs->IPDotted(true);
  //gs->Open("nmsweb.txt");
  // Enable all l3 tracing
  IPV4::Instance()->SetTrace(Trace::ENABLED);
  //L2Proto802_3::Instance()->SetTrace(Trace::ENABLED);

  // Set slightly higher initial rtt estimate
  RTTEstimator::InitialEstimate(3.0);

  // Set default segment size for TCP and UDP connections
  //TCP::DefaultSegSize(1460);  // Plus 40 for tcp/ip hdr, plus 14 for l2=1514
  TCP::DefaultSegSize(1000);    // Plus 40 for tcp/ip hdr, plus 14 for l2=1514
  UDP::DefaultPacketSize(1000);
  TCP::DefaultAdvWin(20000);    // Matches NS default window size
  //TCP::DefaultAdvWin(10000);    // just testing
  //TCP::DefaultAdvWin(40000);    // just testing
  TCP::UseTimerBuckets(timerBuckets);   // Use bucketed timers (efficient)
  TCP::Default().SetTrace(Trace::ENABLED); // Enable trace on default tcp
  Link::UseSeqEvents(seqEvents);     // Bypass central event list (efficient)
  OnOffApplication::DefaultSize(1000);

  // Set default queue limit
  Queue::DefaultLength(50000);

  // Create 20 copies of the network
  for (Count_t i = 0; i < netCount; ++i)
    {
      IPAddr_t subnetIP = baseIp + netMult * (i+1);
      DEBUG(1,(cout << "Creating network subnetIP " <<
               (string)IPAddr(subnetIP) << endl));
      Networks.push_back(new NMSNetwork(subnetIP));
      DEBUG0((cout << "Created network, node count " << Node::nextId << endl));
    }
  cout << "Created " << netCount
       << " networks, node count " << Node::nextId << endl;
  // Connect the gateways
  Time_t linkDelay = Time("200ms");
  //Time_t linkDelay = Time("5ms");
  cout << "Using link delay of " << linkDelay << " secs" << endl;
  Linkp2p l2Gb(Rate("2Gb"), linkDelay); // Link to use, 2Gb
  // Create the ring
  if (netCount > 1)
    {
      for (Count_t i = 0; i < netCount; ++i)
        {
          Count_t  nextNet = baseNet + i + 1;
          if (nextNet == totalNets) nextNet = 0;
          Count_t priorNet = baseNet + i - 1;
          if (baseNet == 0 && i == 0) priorNet = totalNets - 1;
          IPAddr_t thisIP   = netMult * (baseNet + i + 1);
          IPAddr_t nextRIP  = thisIP + IPAddr("0.0.1.1");
          IPAddr_t nextIP   = netMult * (nextNet + 1);
          IPAddr_t priorIP  = netMult * (priorNet + 1);
          IPAddr_t priorRIP = priorIP + IPAddr("0.0.1.2");
          if (distributed)
            {
              if (i == (netCount-1))
                { // Need a remote link to next system, zeroth network
                  Interface* iface = Networks[i]->Gateway()->AddRemoteLink(
                                                  nextRIP, Mask(24),
                                                  Rate("2Gb"), linkDelay);
                  // Notify of remote ip's reachable via this link  
                  DEBUG(1,(cout << "Next RemoteteLink IP is "
                           << (string)IPAddr(nextRIP) << endl));
                  DEBUG(1,(cout << "Adding remote ip "
                           << (string)IPAddr(nextIP) << endl));
                  iface->AddRemoteIP(nextIP, Mask(16));
                  interfaces.push_back(iface);
                }
              else
                { // Need normal link to next subnet
                  Interface* iface = Networks[i]->Gateway()->AddDuplexLink(
                               Networks[(i+1)%netCount]->Gateway(), l2Gb);
                  interfaces.push_back(iface);
                }
              if (!i)
                { // Need a remote link to prior system, zero'th network
                  if (netCount > 1)
                    { // but not if only one network per federate
                      Interface* iface = Networks[i]->Gateway()->AddRemoteLink(
                                                 priorRIP, Mask(24),
                                                 Rate("2Gb"), linkDelay);
                      DEBUG(1,(cout << "Prior RemoteteLink IP is "
                               << (string)IPAddr(priorRIP) << endl));
                      // Notify of remote ip's reachable via this link
                      iface->AddRemoteIP(priorIP, Mask(16));
                      DEBUG(1,(cout << "Adding remote ip "
                               << (string)IPAddr(priorIP) << endl));
                      interfaces.push_back(iface);
                    }
                }
            }
          else
            {
              Interface* iface = Networks[i]->Gateway()->AddDuplexLink(
                           Networks[(i+1)%netCount]->Gateway(), l2Gb);
              interfaces.push_back(iface);
            }
        }
      // Create the N+4 chords
      for (Count_t i = 0; i < netCount; i+=4)
        { // ! need to account for distributed here also, but not really
          // since the chords are not really used
          Networks[i]->Gateway()->AddDuplexLink(
                                  Networks[(i+4)%netCount]->Gateway(), l2Gb);
        }
      // Create the N+10 chords
      for (Count_t i = 0; i < netCount/2; i+=2)
        {
          Networks[i]->Gateway()->AddDuplexLink(
                                  Networks[(i+10)%netCount]->Gateway(), l2Gb);
        }
    }

  Uniform  startRng(0, 100.0); // Random starting times
  NetVec_t::size_type nnet = Networks.size();
  if (singleNet) nnet = 1;
  for (NetVec_t::size_type ni = 0; ni < nnet; ++ni)
    { // for each network
      IPAddrVec_t servers; // IP Addresses of possible servers
      Count_t  nextNet = baseNet + ni + 1;
      if (nextNet == totalNets) nextNet = 0;
      Count_t priorNet = baseNet + ni - 1;
      if (baseNet == 0 && ni == 0) priorNet = totalNets - 1;
      IPAddr_t nextIP   = netMult * (nextNet + 1);
      IPAddr_t priorIP  = netMult * (priorNet + 1);
      cout << "Next " << ni << " nip " << (string)IPAddr(nextIP)
           << " pip " << (string)IPAddr(priorIP) << endl;
      for (Count_t k = 0; k < 4; ++k)
        { // Add the next and prior net servers
          servers.push_back(nextIP + k + 3);
          cout << "Added ip " << (string)IPAddr(nextIP + k + 3)
               << " new size " << servers.size() << endl;
          if (nextIP != priorIP)
            { // next == prior == this if one network only
              servers.push_back(priorIP + k + 3);
              cout << "Added ip " << (string)IPAddr(priorIP + k + 3)
                   << " new size " << servers.size() << endl;
            }
        }
      CLanVec_t& clan = Networks[ni]->campuses; // Get the campus vec
      for (CLanVec_t::size_type ci = 0; ci < clan.size(); ++ci)
        { // for each campus network
          for (Count_t ei = 0; ei < 4; ++ei)
            { // each ethernet
              Count_t pc = clan[ci]->GetENet(ei)->PeerCount();
              for (Count_t eni = 1; eni <= pc; ++eni)
                { // Get nodes from the ethernet except 0 (gateway)
                  // Get my node      
                  Node* mn = Networks[ni]->GetNode(ci, ei, eni);
                  if (!mn)
                    { 
                      cout << "HuH?  Null my node" << endl;
                      exit(1);
                    }
                  TCPTahoe t(mn);// TCP agent to copy for each browser
                  t.SetTrace(Trace::ENABLED);
                  WebBrowser* wb = (WebBrowser*)mn->AddApplication(
                      WebBrowser(servers, Uniform(0, servers.size()), t));
                  wb->ThinkTimeBound(600.0);
                  wb->IdleTime(IDLE_TIME);
                  wb->Start(startRng.Value());
                }
            }
        }
    }
  cout << "Created " << netCount << " copies of the network" << endl;
  cout << "Total nodes created " << Node::GetNodes().size() << endl;
  cout << "Total flows created " << totalFlows << endl;
  cout << "Memory usage before run " 
       << s->ReportMemoryUsageMB() << "MB" << endl;
  //s->Progress(0.1);
  s->ProgressHook(Progress);
  s->Progress(1.0);
  s->StopAt(STOP_TIME);
  s->Run();
  Stats::Print(); // Print the statistics
  cout << "Memory usage after run " 
       << s->ReportMemoryUsageMB() << "MB" << endl;
  cout << "Total events processed "
       << Scheduler::Instance()->TotalEventsProcessed() << endl;
  cout << "Total pkt-hops " << Stats::pktsTransmitted << endl;
  cout << "Setup time " << s->SetupTime() << endl;
  cout << "Route time " << s->RouteTime() << endl;
  cout << "Run time "   << s->RunTime() << endl;
  cout << "Total time "   << s->TotalTime() << endl;
  cout << "Pkt-hops/sec " << (double)Stats::pktsTransmitted / s->TotalTime()
       << endl;
  cout << "NUpdates " << LinkRTI::nUpdates
       << " NReflect " << LinkRTI::nReflects 
       << " IReflect " << LinkRTI::ignoredReflects << endl;
#ifdef VERBOSE_DEBUG
  s->PrintStats();
#endif
  delete s;       // Delete before printing stats so we have cleaned up
  if (averageMinMax)
    {
      if (averageMinMax->Samples())
        { // At least one sample
          averageMinMax->Log(cout);
        }
    }
  TimerBucket::Instance()->DBDump();
#ifdef VERBOSE_DEBUG
  // debug...dump state of all sending apps
  for (TCPSendVec_t::size_type i = 0; i < tcpSends.size(); ++i)
    {
      tcpSends[i]->DBDump();
    }
  // for debug, dump length of interface event queues
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      Interface* iface = interfaces[i];
      cout << "Interface " << (string)IPAddr(iface->GetIPAddr())
           << " pending size " << iface->evList.size() << endl;
    }
#endif
}


