// Verify averages for MAH Http random distributions
// George F. Riley, Georgia Tech, Winter 2002

#include "common-defs.h"
#include "average-min-max.h"
#include "http-distributions.h"
#include "validation.h"  

using namespace std;

HttpPrimaryRequest*   primaryRequest;
HttpSecondaryRequest* secondaryRequest;
HttpPrimaryReply*     primaryReply;
HttpSecondaryReply*   secondaryReply;
HttpFilesPerPage*     filesPerPage;
HttpConsecutivePages* consecutivePages;
HttpThinkTime*        thinkTime;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  if (Validation::sameSeed)
    Random::GlobalSeed(12345, 54321, 77777, 88888, 1234567, 765431);

  primaryRequest   = new HttpPrimaryRequest();
  secondaryRequest = new HttpSecondaryRequest();
  primaryReply     = new HttpPrimaryReply();
  secondaryReply   = new HttpSecondaryReply();
  filesPerPage     = new HttpFilesPerPage();
  consecutivePages = new HttpConsecutivePages();
  thinkTime        = new HttpThinkTime();
  
  AverageMinMax amm;

  for (Count_t i = 0; i < 500000; ++i)
    {
      IRandom_t irv = filesPerPage->IntValue();
      if (!irv) irv++;
      amm.Record(irv);
    }
  cout << "FilesPerPage ";
  amm.Log(cout);
  amm.Reset();

  for (Count_t i = 0; i < 100000; ++i)
    {
      amm.Record(primaryRequest->IntValue());
    }
  cout << "PrimaryRequest ";
  amm.Log(cout);
  amm.Reset();

  for (Count_t i = 0; i < 100000; ++i)
    {
      amm.Record(secondaryRequest->IntValue());
    }
  cout << "SecondaryRequest ";
  amm.Log(cout);
  amm.Reset();

  for (Count_t i = 0; i < 100000; ++i)
    {
      amm.Record(primaryReply->IntValue());
    }
  cout << "PrimaryReply ";
  amm.Log(cout);
  amm.Reset();

  for (Count_t i = 0; i < 100000; ++i)
    {
      amm.Record(secondaryReply->IntValue());
    }
  cout << "SecondaryReply ";
  amm.Log(cout);
  amm.Reset();

  for (Count_t i = 0; i < 100000; ++i)
    {
      amm.Record(consecutivePages->IntValue());
    }
  cout << "ConsecutivePages ";
  amm.Log(cout);
  amm.Reset();

  for (Count_t i = 0; i < 100000; ++i)
    {
      Random_t tt = thinkTime->Value();
      if (tt > 600.0) tt = 600.0;
      amm.Record(tt);
    }
  cout << "ThinkTime ";
  amm.Log(cout);
  amm.Reset();
}


