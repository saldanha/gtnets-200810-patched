// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//     
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


// Georgia Tech Network Simulator - Bluetooth BNEP class
// George F. Riley, Georgia Tech, Spring 2004

#include <string.h>
#include <assert.h>

//#include "bluetus.h"
#include "bnep.h"
#include "l2cap.h"
#include "lmp.h"
#include "simulator.h"

using namespace std;
//string for debug purpose
static const char *pPacketType[] = {
  "BNEP_GENERAL_ETHERNET","BNEP_CONTROL",
  "BNEP_COMPRESSED_ETHERNET", 
  "BNEP_COMPRESSED_ETHERNET_SOURCE_ONLY",
     "BNEP_COMPRESSED_ETHERNET_DST_ONLY"};
static const char *pControlType[] = {
  "BNEP_CONTROL_COMMAND_NOT_UNDERSTOOD",
  "BNEP_SETUP_CONNECTION_REQUEST_MSG",
  "BNEP_SETUP_CONNECTION_RESPONSE_MSG",
  "BNEP_FILTER_NET_TYPE_SET_MSG",
  "BNEP_FILTER_NET_TYPE_RESPONSE_MSG",
  "BNEP_FILTER_MULTI_ADDR_SET_MSG",
  "BNEP_FILTER_MULTI_ADDR_RESPONSE_MSG"};
//
//class BNEP 
//
//Spec 1.0

///////////////////////////////////////////////////
//
//Note: BNEP_XX *pPacket = new uChar[usLen]; may 
//      cause memory leak because of delete pPacket
//      will not delete some memory?
//
////////////////////////////////////////////////////

//constructor
Bnep::Bnep(L2cap *pL2cap) {
  ucState = CLOSED_BNEP;
  pL2CAP = pL2cap;
  pL2CAP->pUpperProtocol = this;
  ucUUIDSize = 16;
  pDstUUID = new uChar[ucUUIDSize]; //128 bits
  pSrcUUID = new uChar[ucUUIDSize]; //128 bits
   
  memset(pDstUUID, 0, ucUUIDSize);
  memset(pSrcUUID, 0, ucUUIDSize );
  usLocalCID = 0;
  connected = false;
}

//copy constructor
/*
Bnep::Copy() const {
  return new Bnep(*this);
}
*/

Bnep::~Bnep() {
  if(pDstUUID!=NULL)
    delete []pDstUUID;
  if(pSrcUUID!=NULL)
    delete []pSrcUUID;
}

//Send Setup Connection request.
void
Bnep::SetupConnReq(uChar ucUUIDSize, uChar *pDstUUID,
      uChar *pSrcUUID) {
  
  uShort usLength = sizeof(BNEP_CONTROL_Hdr) + sizeof(uChar)
      + 2 * ucUUIDSize;
  char  *pTemp = new char[usLength];
  BNEP_SCReqM_Packet *pPacket = (BNEP_SCReqM_Packet *)pTemp;//new char[usLength];
  
  pPacket->CtrlHdr.ucType_E = BNEP_CONTROL; //E flag must be "0"
  pPacket->CtrlHdr.ucCtrlType = BNEP_SETUP_CONNECTION_REQUEST_MSG;
  pPacket->ucUUIDSize = ucUUIDSize;
  
  memcpy(pPacket->ucPayload, pDstUUID, ucUUIDSize);
  memcpy(pPacket->ucPayload + ucUUIDSize, pSrcUUID, ucUUIDSize);
  

  pL2CAP->DataRequest(usLength,
          usLocalCID, //TODO: should be a real localCID
          (void *)pPacket);
  ucState = HALF_OPEN_BNEP; //after sending Connection Request.

  delete []pTemp; //release the memory since we made a copy in L2cap.
}

uShort
Bnep::RecvConnRsp(BNEPPacket *pPkt) {
  BNEP_RSP_Packet *pPacket = (BNEP_RSP_Packet *)pPkt;
  assert(pPacket->CtrlHdr.ucCtrlType == 
          BNEP_SETUP_CONNECTION_RESPONSE_MSG);
  switch(pPacket->usResponse) {
    case 0x0000:
      cout << "Operation Successful.\n";
      //do what you want.
      if(ucState==HALF_OPEN_BNEP) //indicate I ever sent SetupConnectReq
        ucState = CONNECTED_BNEP; //The connection is established
      connected = true;
      //call application
      pApplication->StartApp();  
      break;
    case 0x0001:
      cout << "Operation Failed: Invalid Dst Service UUID \n";
      //do what you want.
      break;
    case 0x0002:
      cout << "Operation Failed: Invalid Src Service UUID \n";
      //do what you want.
      break;
    case 0x0003:
      cout << "Operation Failed: Invalid Service UUID size. \n";
      //do what you want.
      break;
    case 0x0004:
      cout << "Connection not allowed.\n";
      //do what you want
      break;
    default:
      cout <<" Reserved for future.\n";
      break;
  }
  return SUCCESS;
}  

//Receive a connection request from remote
uShort
Bnep::RecvConnReq(BNEPPacket *pPkt) {
  uShort usResponse = 0x0000; //default to success
  BNEP_SCReqM_Packet *pPacket = (BNEP_SCReqM_Packet *)pPkt;
  char *pDstID = new char[pPacket->ucUUIDSize];
  char *pSrcID = new char[pPacket->ucUUIDSize];

  memcpy(pDstID, &(pPacket->ucPayload), pPacket->ucUUIDSize);
  memcpy(pSrcID, ((char *)&(pPacket->ucPayload)) + (pPacket->ucUUIDSize), 
          pPacket->ucUUIDSize);

  //
  //accept it or reject it?
  //
  if(pPacket->ucUUIDSize!=ucUUIDSize)
    usResponse = 0x0003;
  else if(memcmp(pDstID, this->pDstUUID, pPacket->ucUUIDSize))
    usResponse = 0x0001;
  else if(memcmp(pSrcID, this->pSrcUUID, pPacket->ucUUIDSize)) 
    usResponse = 0x0002;
  else
    usResponse = 0x0000; //Accept it

  //Send response
  SendRsp(false, BNEP_SETUP_CONNECTION_RESPONSE_MSG, 0, 
          NULL, usResponse); 
  if(usResponse==0x0000){
    ucState = CONNECTED_BNEP;
    connected = true;
  }
    
  delete []pDstID;
  delete []pSrcID;
  
  //free the packet
  //delete pPkt;

  return SUCCESS;

}

//Generally send response
uShort
Bnep::SendRsp(bool bE, uChar ucCtrlType, uShort usExtLen,
        void *pExt, uShort usResponse) {
  uShort usLength = sizeof(BNEP_CONTROL_Hdr) + usExtLen + sizeof(uShort);
  char *pTemp = new char[usLength];
  BNEP_RSP_Packet *pPacket = (BNEP_RSP_Packet *)pTemp;//new char[usLength];
  uChar ucType_E = BNEP_CONTROL;
  
  if(bE) {
    assert(usExtLen!=0);
    assert(pExt!=NULL);
    
    ucType_E = ucType_E|(0x80);
    pPacket->CtrlHdr.ucType_E = ucType_E;
    pPacket->CtrlHdr.ucCtrlType = ucCtrlType;

    memcpy(pPacket+sizeof(BNEP_CONTROL_Hdr), pExt, usExtLen);
    memcpy(pPacket+usLength -sizeof(uShort), &usResponse, sizeof(uShort));
    
  }else{
    pPacket->CtrlHdr.ucType_E = ucType_E;
    pPacket->CtrlHdr.ucCtrlType = ucCtrlType;
    pPacket->usResponse = usResponse;
  }
  
  pL2CAP->DataRequest(usLength, 
          usLocalCID, //TODO: should be a real one.
          (void *)pPacket);
  delete []pTemp; //deallocate the memory
  return SUCCESS;
}

//Send filter net type set control message
uShort
Bnep::SendFilterNetSet(bool bE, uShort usListLength, void *pExt, 
        void *NetStartEnd) {
    
  uShort usLen = 0;
  uShort usLenExt = 0;
  bool bExtension = false;
  BNEP_EXTENSION_Hdr *pExten = (BNEP_EXTENSION_Hdr *)pExt;
  uChar *pPacket = NULL;
  BNEP_CONTROL_Hdr *pCtrlHdr;

  assert(usListLength%4==0);
  if(bE) {
    //compute the extension length;
    do {
      usLenExt = usLenExt + pExten->ucLength;
      bExtension = pExten->ucType_E;
    }while(bExtension);
  
    usLen = usLenExt + usListLength + sizeof(usListLength);
    pPacket = new uChar[usLen + sizeof(BNEP_CONTROL_Hdr)];
    pCtrlHdr = (BNEP_CONTROL_Hdr *)pPacket;
    pCtrlHdr->ucType_E = BNEP_CONTROL;
    assert(pExt!=NULL);
    pCtrlHdr->ucType_E = BNEP_CONTROL|0x80;
    pCtrlHdr->ucCtrlType = BNEP_FILTER_NET_TYPE_SET_MSG;
    memcpy(pPacket + sizeof(BNEP_CONTROL_Hdr), pExt, usLenExt);
    memcpy(pPacket + sizeof(BNEP_CONTROL_Hdr)+usLenExt,&usListLength,
            sizeof(usListLength));
    memcpy(pPacket + sizeof(BNEP_CONTROL_Hdr)+usLenExt + sizeof(usListLength),
            NetStartEnd, usListLength);
    pL2CAP->DataRequest(usLen+ sizeof(BNEP_CONTROL_Hdr), 
            usLocalCID, //TODO: should be a real one.
            (void *)pPacket);
    
  }else {
    pPacket = new uChar[usListLength + sizeof(BNEP_CONTROL_Hdr)+ 
        sizeof(usListLength)];
    BNEP_FNTSM_Packet *pFNTSMPkt = (BNEP_FNTSM_Packet *)pPacket;
    pFNTSMPkt->CtrlHdr.ucType_E = BNEP_CONTROL;
    pFNTSMPkt->CtrlHdr.ucCtrlType = BNEP_FILTER_NET_TYPE_SET_MSG;
    pFNTSMPkt->usLength = usListLength;

    pFNTSMPkt->pStartEnd = (ProtocolStartEnd *)(pPacket + 
            sizeof(BNEP_CONTROL_Hdr)+sizeof(usListLength));
    memcpy(pFNTSMPkt->pStartEnd, NetStartEnd, usListLength);
    pL2CAP->DataRequest(usListLength+ sizeof(BNEP_CONTROL_Hdr) + 
            sizeof(usListLength),
            usLocalCID, //TODO: should be a real one
            (void *)pFNTSMPkt);
  }
  
  if(pPacket!=NULL)
    delete []pPacket;
     
  return SUCCESS;  
}

//Receive filter network protocol type set message
uShort
Bnep::RecvReqFilterNetSet(BNEPPacket *pPkt) {
  BNEP_CONTROL_Hdr *pCtrlHdr = (BNEP_CONTROL_Hdr *)pPkt;
  assert(pCtrlHdr->ucCtrlType == BNEP_FILTER_NET_TYPE_SET_MSG);
  ProtocolStartEnd *pRange; 

  bool bE = (pCtrlHdr->ucType_E & 0x80)>>7; //get the highest LSB
  uShort usReason = 0x0000; //accept default.

  if(bE) {
    BNEP_EXTENSION_Hdr *pExten =(BNEP_EXTENSION_Hdr *) 
        ((uChar *)pPkt + sizeof(BNEP_CONTROL_Hdr));
    //TODO:unfinished yet!
                pExten = nil; // Get around compiler warning, remove later
  }else {
    uShort usListLength;

    BNEP_FNTSM_Packet *pPacket = (BNEP_FNTSM_Packet *)pPkt;
    usListLength = pPacket->usLength;
    pRange = (ProtocolStartEnd *)((char *)pPacket + sizeof(BNEP_CONTROL_Hdr)
           + sizeof(pPacket->usLength)) ;
    //pRange = pPacket->pStartEnd;
    assert(usListLength%4==0);
    
    while(usListLength!=0) {
      //test code begins
      cout<<"Protocol Range starts "<<pRange->usTypeRangeStart 
        <<" ends "<<pRange->usTypeRangeEnd <<endl;
      //end of test code
      if((pRange->usTypeRangeStart)>(pRange->usTypeRangeEnd)) {
        cout<<"Warning, the received FilterNet invalid"<<endl;
        usReason = 0x0002; //Invalid Networking Protocol
        break;
      }
      pRange = (ProtocolStartEnd *)((uChar *)pRange + 
              sizeof(ProtocolStartEnd));
      //pStartEnd =(uChar *)(pPacket->pStartEnd) + sizeof(ProtocolStartEnd);
      usListLength = usListLength - sizeof(ProtocolStartEnd);
    }//!while(usListLength)

  }//!if(bE)

  //continue to process it.
  //
  //Accept it or reject it?
  SendRsp(false, BNEP_FILTER_NET_TYPE_RESPONSE_MSG, 0, 
          NULL, usReason);

  return SUCCESS;
}

//Receive filter net type response message
uShort
Bnep::RecvRspFilterNet(BNEPPacket *pPkt) {
  BNEP_RSP_Packet *pPacket = (BNEP_RSP_Packet *)pPkt;
  assert(pPacket->CtrlHdr.ucCtrlType == 
          BNEP_FILTER_NET_TYPE_RESPONSE_MSG);
  switch(pPacket->usResponse) {
    case 0x0000:
      cout << "Operation Successful.\n";
      //do what you want.
      //TODO: store those stuffs in "list" of BNEP
      //\\ Test code starts here. REMOVED shortly after\\//
      {
        MultiAddrStartEnd multiStartEnd;
        multiStartEnd.AddrStart = 0x4321;
        multiStartEnd.AddrEnd = 0x9867;
        uShort usLen = sizeof(multiStartEnd);
        SendFilterMultiAddrSet(false, usLen, (void *)NULL, (void *)&multiStartEnd);
      }
      //unfinished yet.
      //End of the test code
      break;
    case 0x0001:
      cout << "Unsupported Request \n";
      //do what you want.
      break;
    case 0x0002:
      cout << "Operation Failed: Invalid Networking protocol Type Range \n";
      //do what you want.
      break;
    case 0x0003:
      cout << "Operation Failed: Too many filters. \n";
      //do what you want.
      break;
    case 0x0004:
      cout << "Operation Failed: Unable to fulfill" 
          <<"request due to security reasons.\n";
      //do what you want
      break;
    default:
      cout <<" Reserved for future.\n";
      break;
  }
  return SUCCESS;
}

//Send filter multicast address set control message
//This function can be merged together with
// SendFilterNetSet.....
uShort
Bnep::SendFilterMultiAddrSet(bool bE, uShort usListLength, void *pExt,
        void*MultiAddrStartEnd) {
  uShort usLen = 0;
  uShort usLenExt = 0;
  bool bExtension = false;
  BNEP_EXTENSION_Hdr *pExten = (BNEP_EXTENSION_Hdr *)pExt;
  uChar *pPacket = NULL;
  BNEP_CONTROL_Hdr *pCtrlHdr;

  assert(usListLength%12==0);
  if(bE) {
    //compute the extension length;
    do {
      usLenExt = usLenExt + pExten->ucLength;
      bExtension = pExten->ucType_E;
    }while(bExtension);
  
    usLen = usLenExt + usListLength + sizeof(usListLength);
    pPacket = new uChar[usLen + sizeof(BNEP_CONTROL_Hdr)];
    pCtrlHdr = (BNEP_CONTROL_Hdr *)pPacket;
    pCtrlHdr->ucType_E = BNEP_CONTROL;
    assert(pExt!=NULL);
    pCtrlHdr->ucType_E = BNEP_CONTROL|0x80;
    pCtrlHdr->ucCtrlType = BNEP_FILTER_MULTI_ADDR_SET_MSG;
    memcpy(pPacket + sizeof(BNEP_CONTROL_Hdr), pExt, usLenExt);
    memcpy(pPacket + sizeof(BNEP_CONTROL_Hdr)+usLenExt,&usListLength,
            sizeof(usListLength));
    memcpy(pPacket + sizeof(BNEP_CONTROL_Hdr)+usLenExt + sizeof(usListLength),
            MultiAddrStartEnd, usListLength);
    pL2CAP->DataRequest(usLen + sizeof(BNEP_CONTROL_Hdr),
                usLocalCID, //TODO: should be a real one.
                (void *)pPacket);
    
  }else {
    pPacket = new uChar[usListLength + sizeof(BNEP_CONTROL_Hdr)+ 
        sizeof(usListLength)];
    BNEP_FNTSM_Packet *pFNTSMPkt = (BNEP_FNTSM_Packet *)pPacket;
    pFNTSMPkt->CtrlHdr.ucType_E = BNEP_CONTROL;
    pFNTSMPkt->CtrlHdr.ucCtrlType = BNEP_FILTER_MULTI_ADDR_SET_MSG;
    pFNTSMPkt->usLength = usListLength;

    pFNTSMPkt->pStartEnd = (ProtocolStartEnd *)(pPacket + 
            sizeof(BNEP_CONTROL_Hdr)+ sizeof(usListLength));
    memcpy(pFNTSMPkt->pStartEnd, MultiAddrStartEnd, usListLength);
    pL2CAP->DataRequest(usListLength + sizeof(BNEP_CONTROL_Hdr) +
            sizeof(usListLength),
            usLocalCID, //TODO: should be a real one.
            (void *)pPacket);
  }
  
  if(pPacket!=NULL)
    delete []pPacket;
     
  return SUCCESS;  
}

//Receive request of filter multicast
uShort
Bnep::RecvReqFilterMultiAddrSet(BNEPPacket *pPkt) {
  
  BNEP_CONTROL_Hdr *pCtrlHdr = (BNEP_CONTROL_Hdr *)pPkt;
  assert(pCtrlHdr->ucCtrlType == BNEP_FILTER_MULTI_ADDR_SET_MSG);
  MultiAddrStartEnd *pRange; 

  bool bE = (pCtrlHdr->ucType_E & 0x80)>>7; //get the highest LSB
  uShort usReason = 0x0000; //accept default.

  if(bE) {
    BNEP_EXTENSION_Hdr *pExten = (BNEP_EXTENSION_Hdr *)
        ((uChar *)pPkt + sizeof(BNEP_CONTROL_Hdr));
    //TODO:.....
                pExten = nil; // Remove compiler warning (remove later)
  }else {
    uShort usListLength;

    BNEP_FMASM_Packet *pPacket = (BNEP_FMASM_Packet *)pPkt;
          //(BNEP_FNTSM_Packet *)pPkt;
    usListLength = pPacket->usLength;
    pRange = (MultiAddrStartEnd *)((char *)pPkt + sizeof(BNEP_CONTROL_Hdr)
        + sizeof(uShort));
    //pRange = pPacket->pAddrStartEnd;//pStartEnd;

    assert(usListLength%12==0);
    
    while(usListLength!=0) {
      pRange =(MultiAddrStartEnd *)((uChar *)pRange + 
              sizeof(MultiAddrStartEnd));
      usListLength = usListLength - sizeof(MultiAddrStartEnd);
    }//!while(usListLength)

  }//!if(bE)

  //continue to process it.
  //
  //Accept it or reject it?
  SendRsp(0, BNEP_FILTER_MULTI_ADDR_RESPONSE_MSG, 
          0, NULL, usReason);
  //reject it
  //SendRsp(0, BNEP_FILTER_MULTI_ADDR_RESPONSE_MSG, 
  //        0, NULL, 0x0001); //or 2....etc
  

  return SUCCESS;
}

//Receive response of filter multicast address setting
uShort
Bnep::RecvRspFilterMultiAddr(BNEPPacket *pPkt) {
  BNEP_RSP_Packet *pPacket = (BNEP_RSP_Packet *)pPkt;
  assert(pPacket->CtrlHdr.ucCtrlType == 
          BNEP_FILTER_MULTI_ADDR_RESPONSE_MSG);
  switch(pPacket->usResponse) {
    case 0x0000:
      cout << "Operation Successful.\n";
      //TODO: enable the filter setting now.
      //do what you want.
      break;
    case 0x0001:
      cout << "Unsupported Request \n";
      //do what you want.
      break;
    case 0x0002:
      cout << "Operation Failed: Invalid multicast address \n";
      //do what you want.
      break;
    case 0x0003:
      cout << "Operation Failed: Too many filters. \n";
      //do what you want.
      break;
    case 0x0004:
      cout << "Operation Failed: Unable to fulfill" 
          <<"request due to security reasons.\n";
      //do what you want
      break;
    default:
      cout <<" Reserved for future.\n";
      break;
  }
  return SUCCESS;
}

//Applicaion send
NCount_t
Bnep::Send(Size_t s) {
  uChar* pData = new uChar[s];
  memset(pData, 0, s);
  DataRequest(false, 0x800, 0, NULL, s, pData);
  delete pData;
  return s;
}

//upper layer(ip) request to send packet
//General version:including src addr and dst addr
// It SHALL be used to carry ethernet packets to and from Bluetooth networks.
// Src/Dst may be an IEEE ethernet address.
uShort 
Bnep::DataRequest(bool bE, BdAddr Src, BdAddr Dst, uShort usProtocolType,
        uShort usLenExt, void *pExt, uShort usLenData,
        void *pData /* extension,  payload */) {
  if(ucState!=CONNECTED_BNEP) {
    cout << "Connection is not established yet." <<endl;
    return BT_ERROR;
  }
  //Send an ethernet packet.
  BNEP_ETHERNET_Hdr *pBEtherHdr = NULL;
  uChar *pTemp =   (new uChar[pBEtherHdr->HdrSize() + usLenExt  + usLenData]);
  pBEtherHdr = (BNEP_ETHERNET_Hdr *)pTemp;

  pBEtherHdr->ucType_E = BNEP_GENERAL_ETHERNET | (bE<<7);
  memcpy(&(pBEtherHdr->Dst), &Dst, sizeof(BdAddr));
  memcpy(&(pBEtherHdr->Src), &Src, sizeof(BdAddr));
  pBEtherHdr->usProtocolType = usProtocolType;

  if(bE) {
    assert(pExt!=NULL);
    assert(usLenExt!=0);
    memcpy(&(pBEtherHdr->ucPayload),pExt, usLenExt);
    if(pData!=NULL)
#ifndef WIN32
      memcpy(&(pBEtherHdr->ucPayload) + usLenExt, pData, usLenData);
#else
	memcpy((pBEtherHdr->ucPayload) + usLenExt, pData, usLenData);
#endif
  }else {
    if(pData!=NULL)
      memcpy(&(pBEtherHdr->ucPayload), pData, usLenData);
  }
  
  pL2CAP->DataRequest(pBEtherHdr->HdrSize() + usLenExt + usLenData,
          usLocalCID, //TODO: should be a real one.
          (void *)pBEtherHdr);
  if(pTemp != NULL)
    delete []pTemp;

  return SUCCESS;
}

//upper layer(ip) request to send packet
//Compressed version: no src and dst address
uShort 
Bnep::DataRequest(bool bE, uShort usProtocolType, uShort usLenExt,
        void *pExt, uShort usLenData, void *pData) {
  if(ucState!=CONNECTED_BNEP) {
    cout << "Connection is not established yet." <<endl;
    return BT_ERROR;
  }
  //Send an ethernet packet.
  BNEP_COMPRESSED_ETHERNET_Hdr *pBEtherHdr = NULL;
  uChar *pTemp = new uChar[pBEtherHdr->HdrSize() + usLenExt + 
      usLenData];
  pBEtherHdr = (BNEP_COMPRESSED_ETHERNET_Hdr *)pTemp;

  pBEtherHdr->ucType_E = BNEP_COMPRESSED_ETHERNET | (bE<<7);
  pBEtherHdr->usProtocolType = usProtocolType;

  if(bE) {
    assert(pExt!=NULL);
    assert(usLenExt!=0);
    memcpy(&(pBEtherHdr->ucPayload),pExt, usLenExt);
    if(pData!=NULL)
#ifndef WIN32
      memcpy(&(pBEtherHdr->ucPayload) + usLenExt, pData, usLenData);
#else
	memcpy((pBEtherHdr->ucPayload) + usLenExt, pData, usLenData);
#endif
     
  }else {
    if(pData!=NULL)
      memcpy(&(pBEtherHdr->ucPayload), pData, usLenData);
  }
  
  pL2CAP->DataRequest(pBEtherHdr->HdrSize() + usLenExt +
          usLenData,
          usLocalCID, //TODO: should be a real one!
          (void *)pBEtherHdr);
  delete []pTemp;

  return SUCCESS;  
}

//upper layer(ip) request to send packet
//
//Half-compressed version:
//
//The function can be used for Dst-only and Src-only compressed
//ethernet packet although BNEP_COMPRESSED_ETHERNET_SRC_ONLY_Hdr is used.
// 
// ucType can be BNEP_COMPRESSED_ETHERNET_SOURCE_ONLY or
//               BNEP_COMPRESSED_ETHERNET_DST_ONLY
uShort
Bnep::DataRequest(bool bE, BdAddr Src, uShort usProtocolType, uShort usLenExt,
        void *pExt, uShort usLenData, void *pData, uChar ucType) {
  if(ucState!=CONNECTED_BNEP) {
    cout << "Connection is not established yet." <<endl;
    return BT_ERROR;
  }
  //Send an ethernet packet.
  //
  BNEP_COMPRESSED_ETHERNET_SRC_ONLY_Hdr *pBEtherHdr = NULL;
  uChar *pTemp = new uChar[pBEtherHdr->HdrSize() + usLenExt + usLenData];
  pBEtherHdr = (BNEP_COMPRESSED_ETHERNET_SRC_ONLY_Hdr *)pTemp;

  pBEtherHdr->ucType_E = /*BNEP_COMPRESSED_ETHERNET_SOURCE_ONLY*/ ucType | (bE<<7);

  memcpy(&(pBEtherHdr->Src), &Src, sizeof(BdAddr));
  
  pBEtherHdr->usProtocolType = usProtocolType;

  if(bE) {
    assert(pExt!=NULL);
    assert(usLenExt!=0);
    memcpy(&(pBEtherHdr->ucPayload),pExt, usLenExt);
    if(pData!=NULL)

      
#ifndef WIN32
      memcpy(&(pBEtherHdr->ucPayload) + usLenExt, pData, usLenData);
#else
	memcpy((pBEtherHdr->ucPayload) + usLenExt, pData, usLenData);
#endif

  }else {
    if(pData!=NULL)
      memcpy(&(pBEtherHdr->ucPayload), pData, usLenData);
  }
  
  pL2CAP->DataRequest(pBEtherHdr->HdrSize() + usLenExt + usLenData,
          usLocalCID, //TODO: should be a real one.
          (void *)pBEtherHdr);
  delete []pTemp;

  return SUCCESS;  
  
}

//lower layer indicate incoming data
//TODO:
//    Fill the related handler to Ethernet packet
//                   Compressed ethernet packet
//                half-compressed ethernet packet
uShort
Bnep::DataIndication(void *pPacketIn, uShort usCID) {
  cout<<"BNep::DataIndication? "<<endl;
  BNEPPacket *pPacket = (BNEPPacket *)pPacketIn; 
  //XXX don't free the Packet in Bnep. I will take care in L2cap
  uChar ucType = (pPacket->ucType_E) & 0x7F;

  cout<<"Received a BNEP packet, TYPE = "
      <<pPacketType[ucType]<<endl;
  //temp code
  //XXX in future, pass the usCID to RecvControlPacket
  usLocalCID = usCID;
  //*******************
  //exclusively for test only!
  //*******************  
  
  switch(ucType) {
    case BNEP_GENERAL_ETHERNET:
      ;// receive Ethernet packet
      ;//Pass to IP layer.
      break;
    case BNEP_CONTROL:
      RecvControlPacket(pPacket);//
      break;
    case BNEP_COMPRESSED_ETHERNET:
      // receive compressed ethernet packet
      cout<<"Received a COMPRESSED ETHERNET packet"<<endl;
      //by xzhang on 05/09/2004
      break;
    case BNEP_COMPRESSED_ETHERNET_SOURCE_ONLY:
      ;// receive half-compressed packet
      ;//pass to ip layer.
      break;
    case BNEP_COMPRESSED_ETHERNET_DST_ONLY:
      ;// receive half-compressed packet
      ;//pass to ip layer.
      break;
    default:
      cout <<" Unsupported packet type" <<endl;
      break;
  }

  return SUCCESS;
}

// dispatch the incoming control packet to appropriate handler.
//
void
Bnep::RecvControlPacket(BNEPPacket *pPacket) {
  BNEP_CONTROL_Hdr *pHdr = (BNEP_CONTROL_Hdr *)pPacket;
  cout<<"Received a BNEP control packet, TYPE = "
      <<pControlType[pHdr->ucCtrlType]<<endl;
  switch(pHdr->ucCtrlType) {
    case BNEP_CONTROL_COMMAND_NOT_UNDERSTOOD:
      {
      BNEP_CNU_Packet *pCmdUnknown = (BNEP_CNU_Packet *)pPacket;
      cout <<"Command type" << pCmdUnknown->ucUCType 
          <<" is NOT understood" <<endl;
      }
      break;
    case BNEP_SETUP_CONNECTION_REQUEST_MSG:
      RecvConnReq(pPacket);
      break;
    case BNEP_SETUP_CONNECTION_RESPONSE_MSG:
      RecvConnRsp(pPacket);
      break;
    case BNEP_FILTER_NET_TYPE_SET_MSG:
      RecvReqFilterNetSet(pPacket);
      break;
    case BNEP_FILTER_NET_TYPE_RESPONSE_MSG:
      RecvRspFilterNet(pPacket);
      break;
    case BNEP_FILTER_MULTI_ADDR_SET_MSG:
      RecvReqFilterMultiAddrSet(pPacket);
      break;
    case BNEP_FILTER_MULTI_ADDR_RESPONSE_MSG:
      RecvRspFilterMultiAddr(pPacket);
      break;
    default:
      cout <<"Unsupported control type" <<endl;
  }
  return;
}

//Indicates a connection request has been received
uShort 
Bnep::L2CA_ConnectInd(uShort usPSM, uChar ucIdentifier,
        uShort usLocalChannelID) {
  //TODO: save the localChannelID and the remote ChannelID.
  //this layer has to check this!!
  BdAddr tmpAddr = {{0x0000, 0x0000, 0x0000}};
  if(usPSM != this->PSM()) {
    pL2CAP->L2CA_ConnectRsp(tmpAddr, //TODO: XXX Should be remote BdAddr??
            ucIdentifier, 
            usLocalChannelID, 0x0002//PSM not supported
            ,0x0000); //no further information.
    return SUCCESS;
  }
  
  //TODO: make other choice. e.g.
  cout<<"BNEP::L2CA_ConnectInd" <<endl;//by xzhang on 03/19/2004
   
  pL2CAP->L2CA_ConnectRsp(tmpAddr, //TODO: XXX should be remote BdAddr??
          ucIdentifier, usLocalChannelID,
          0x0000,/* success*/
          0x0000/* no further information*/);
  return SUCCESS;
}

//Schedule timer for request(conn, cfg, and disconn)
void 
Bnep::ScheduleTimer(Event_t event,  
        BNEPTimerEvent*& ev, Time_t t) {
  assert(ev==NULL);
  ev = new BNEPTimerEvent(event);
  cout << "scheduleing a timer for ucIdentifier locally.\n";
  timer.Schedule(ev, t, this);
}

//Cancel the timer because of getting response.
void 
Bnep::CancelTimer(BNEPTimerEvent*& event, 
        bool delTimer) {
  if(event!=NULL) {
    timer.Cancel(event);
    if (delTimer) { // Delete it
      delete event;
      event = nil;
    }
  }
  return;
}

void 
Bnep::Timeout(TimerEvent *pEvent) {
  BNEPTimerEvent *pTimerEvent = (BNEPTimerEvent *)pEvent;
  switch(pTimerEvent->event) {
    case BNEPTimerEvent::CONNREQ_TIMEOUT:
      ;
      break;
    case BNEPTimerEvent::FILTERREQ_TIMEOUT:
      ;
      break;
    default:
      ;
      break;
  }
  delete pTimerEvent;//consume the event.
  pEvent = NULL;
  return;
}

//Indicates "my" connection request was accepted, step further.
uShort 
Bnep::L2CA_ConnectCfm(uShort usLocalChannelID) {
  //Pass the "good" news to upper layer.
  
  cout<<"BNEP:L2CA_ConnectCfm" <<endl;
   
  //\\begin test code\\//
  uShort usIncomingMTU = 341;
  uShort usOutFlushTO = 0xFFFF;//default value
  uShort usLinkTO = 0; //unused now
  
  cout <<"Local Channel ID = "<< usLocalChannelID<<endl;
  pL2CAP->L2CA_ConfigReq(usLocalChannelID, usIncomingMTU, 
        (void*)NULL, usOutFlushTO, usLinkTO, 
        (uShort *)NULL,(void *)NULL,(uShort *)NULL);
  //end of test code
  
  //pL2CAP->L2CA_ConfigReq(usLocalChannelID, XXXX,XXXX...);
  return SUCCESS;
}

//Indicates "my" request was rejected. :((
//          Or timeout of request without getting response.
uShort 
Bnep::L2CA_ConnectCfmNeg(uShort usLocalChannelID){
  //pass the "bad" news to upper layer.
  
  cout<<"BNEP:L2CA_ConnectCfmNeg" <<endl;
  
  //do nothing since L2capSignalChannel will do something.
  return SUCCESS;
}

//Indicates a configure request received from remote device.
uShort 
Bnep::L2CA_ConfigInd(uShort usLocalChannelID, uChar ucIdentifier){
  cout<<"BNEP:L2CA_ConfigInd" <<endl;
  //\\test code starts \\//
  uShort usOutMTU = 341; 
  pL2CAP->L2CA_ConfigRsp(usLocalChannelID, usOutMTU, ucIdentifier,
        (void *)NULL, SUCCESS);
  //end of test code
  return SUCCESS;
}

//Indicates a configure request was accepted following the receipt
//          of a configuration response from the remote device
uShort 
Bnep::L2CA_ConfigCfm(uShort usLocalChannelID) {
  //parameters are accepted by remote device.
  cout<<"BNEP:L2CA_ConfigCfm" <<endl;
  //\\test code starts\\//
  //disconnect the channel now.
  //pL2CAP->L2CA_DisconnectReq(usLocalChannelID);  
  //
  //commented out so that I could test bnep self.
  //store this, and use later.
  usLocalCID = usLocalChannelID;
  SetupConnReq(16, this->pDstUUID, this->pSrcUUID);
  return SUCCESS;
}

//Indicates a configure request has negative confirm response. 
//          Or an RTX timer expires.
uShort 
Bnep::L2CA_ConfigCfmNeg(uShort usLocalChannelID) {
  cout<<"BNEP:L2CA_ConfigCfmNeg" <<endl;
  //TODO: Renegotiate the parameters?
  // e.g. adjust parameters again.
  //pL2CAP->L2CA_ConfigReq(....);
  return SUCCESS;
}

uShort
Bnep::L2CA_ConnectPnd(uShort usCID) {
  return SUCCESS;
}

//Indicates a disconnection request has been received from a remote
//device or the remote device has been disconnected because it fails
//to respond to a signalling request
uShort 
Bnep::L2CA_DisconnectInd(uShort usLocalChannelID, 
        uChar ucIdentifier) {
  cout<<"BNEP:L2CA_DisconnectInd" <<endl;
  //TODO: clear what it should in this layer.
  //\\test code starts \\//
  pL2CAP->L2CA_DisconnectRsp(usLocalChannelID, ucIdentifier);
  //end of test code 
  return SUCCESS;
}

//Indicates a disconnection request has been processed by remote
//device following receipt of a disconnection response from remote.
uShort 
Bnep::L2CA_DisconnectCfm() {
  cout<<"BNEP:L2CA_DisconnectCfm" <<endl;
  //TODO: clear what it should in this layer
  return SUCCESS;
}

//
//L2CA_TimeOutInd
//

//
//L2CA_QoSViolationInd
//

//test method for L2CAP
// 
void
Bnep::TestL2CAP(BdAddr dst) {
  //continously send data to dst
  uShort usChannelId = 0;
  uShort usStatus = 0;

  assert(pL2CAP!=NULL);
  cout<<"TestL2CAP: Send L2CA_ConnectReq."<<endl;
  pL2CAP->L2CA_ConnectReq(PSM(), dst, (uShort *)&usChannelId,&usStatus);
  
  cout<<"Channel ID = "<<usChannelId <<endl;
  return;
  
}

//this segment is also test code.
void
Bnep::Start(Time_t t, BdAddr dst)
{ //check active/sniff before send out
  if(pL2CAP->pLMP->IsSniff() && pL2CAP->pLMP->IsSleep()) {
    cout<<"In sniff mode sleep state."<<endl;
  return;
  }
  else {
  // Schedule the Start event
    BnepEvent* startEvent = new BnepEvent(BnepEvent::STARTB);
    Scheduler::Schedule(startEvent, t , this);
    this->dst = dst;
  }
}

void
Bnep::Stop(Time_t t)
{ // Schedule the Stop Event
  BnepEvent* stopEvent = new BnepEvent(BnepEvent::STOPB);
  Scheduler::Schedule(stopEvent, t, this);
}

void
Bnep::Handle(Event* e, Time_t)
{
  BnepEvent* ev = (BnepEvent*)e;
  switch(ev->event) {
    case BnepEvent::STARTB :
      TestL2CAP(dst); // Call the application startup code
      break;
    case BnepEvent::STOPB :
      ;//StopApp();  // Call the application shutdown code
      break;
  case BnepEvent::STARTBM:
    SetupConnReq(16, this->pDstUUID, this->pSrcUUID);
    break;
  }
  delete ev;
}
