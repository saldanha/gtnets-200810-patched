// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-voip.h 330 2005-03-23 19:05:51Z riley $



// Georgia Tech Network Simulator - VOIP application class
// Young J. Lee.  Georgia Tech, Fall 2004

// Define a voip application class

#ifndef __VOIP_APPLICATION_H
#define __VOIP_APPLICATION_H

#include "application-onoff.h"
#include "rng.h"

// Define the start/stop events
//Doc:ClassXRef
class AppVOIPEvent : public Event {
public:
  typedef enum { START_CALL = 1000, STOP_CALL, NEXT_SAMPLE } AppVOIPEvent_t;
  AppVOIPEvent() { }
  AppVOIPEvent(Event_t ev) : Event(ev) { }
};

class AIFF;  // Audio Format reading/writing

// Define a callback for custom codecs.
// Custom codecs return the count of bytes used in the char* buffer
// Inputs are the 32 bit sample buffer, the 8 bit sample buffer,
// length of both buffers, and bool "encode".  If encode is true
// convert 32 bit to 8 bit, if false convert 8 bit to 32 bit.
typedef Count_t (*CodecCallback_t)(long*, char*, Count_t, bool);

//Doc:ClassXRef
class VOIPApplication : public OnOffApplication {
  //Doc:Class Defines an application that uses the VOIP traffic generator
  //Doc:Class model.
public:
  typedef enum {
      CODEC_ULAW,
      CODEC_ALAW,
      CODEC_CUSTOM
  } Codec_t;
  
  //Doc:Method
  VOIPApplication(IPAddr_t, PortId_t,
                  const Random& on = Exponential(0.352),
                  const Random& off = Exponential(0.650),
                  Rate_t r = 64000, // Rate when on
                  Size_t s = 80);   // packet size
    //Doc:Desc Constructor for On-Off VOIP application.
    //Doc:Arg1 \IPA\ for remote endpoint
    //Doc:Arg2 Port number for remote endpoint
    //Doc:Arg3 Random generator for "On" time.
    //Doc:Arg4 Random generator for "Off" time
    //Doc:Arg5 Data rate when "on"
    //Doc:Arg6 Packet size.

  //Doc:Method
  VOIPApplication(IPAddr_t, PortId_t, const std::string&, Size_t s = 80);
    //Doc:Desc Constructor for VOIP app with AIFF input file
    //Doc:Arg1 \IPA\ for remote endpoint
    //Doc:Arg2 Port number for remote endpoint
    //Doc:Arg3 File name for AIFF input file.
    //Doc:Arg4 Packet size

  //Doc:Method
  void CallDuration(const Random&); // random var for call duration (seconds)
    //Doc:Desc Specify the duration of an on-off VOIP app.
    //Doc:Desc The On-Off app will 
    //Doc:Desc stop after the specified amount of time.
    //Doc:Desc If none specified,
    //Doc:Desc the call continues infinitely.
    //Doc:Arg1 Random generator for call duration time.

  //Doc:Method
  void WaitDuration(const Random&); // random var for wait duration (seconds)
    //Doc:Desc Specify the time between calls for on-off VOIP app.
    //Doc:Desc If not specified, the app stops after one call.
    //Doc:Arg1 Random generator for waiting time between calls.

  // Need a handler for duration events
  void Handle(Event*, Time_t);
  virtual void StartApp();      // Called at time specified by Start
  virtual void StopApp();       // Called at time specified by Stop
  virtual Application* Copy() const;// Make a copy of the application
  virtual void Receive(Packet*, L4Protocol*, Seq_t = 0);   // Data received
  // Specify audio file input and output
  //Doc:Method
  bool CallFile(const std::string&);      // Use specified file for audio in
    //Doc:Desc Specify file name for the AIFF audio file.
    //Doc:Desc THis is specified on the constructor, so this
    //Doc:Desc method is not generally used.
    //Doc:Arg1 File name for AIFF file.
    //Doc:Return True if file opened successfully.

  //Doc:Method
  bool RecordingFile(const std::string&); // Record on specified file
    //Doc:Desc Specify the file to write received voice data.
    //Doc:Arg1 Recording file name.
    //Doc:Return True if file created successfully.

  //Doc:Method
  void SetSampleRate(Count_t r) {sampleRate = r;} // Set audio file sample rate
    //Doc:Desc Specify sampling rate for reading/writing the AIFF voice files.
    //Doc:Desc If not specified, default is 8k samples/sec
    //Doc:Arg1 Sample rate desired.

  //Doc:Method
  void UseSilenceSupression(double = 1000.0);
    //Doc:Desc   Specifies if silence supression is used.  This is not static,
    //Doc:Desc since VOIP apps can work correctly if one end uses
    //Doc:Desc supression and the other end does not.
    //Doc:Arg1 Silence threshold.  This is the value of the
    //Doc:Arg1 sound intensity calculation below which the block is
    //Doc:Arg1 considered silence.  Default value is 1,000.

  // Specify codec.
  // Note these are STATIC, meaning all instances use same codec
  //Doc:Method
  static void SetCodec(Codec_t);
    //Doc:Desc Specify coder/decoder to use.  Built-in codes are
    //Doc:Desc CODEC_ULAW and CODEC_ALAW. Specifying CODEC_CUSTOM
    //Doc:Desc requires use for a custom codec callback (specified below).
    //Doc:Desc This method is static, meaning all VOIP apps must use
    //Doc:Desc same codec.
    //Doc:Arg1 Codec to use.

  //Doc:Method
  static void SetCustomCodecCallback(CodecCallback_t);
    //Doc:Desc Specify custom codec callback to use.  Changes
    //Doc:Desc the codec to CODEC_CUSTOM.
    //Doc:Arg1 Callback pointer.

private:
  void SendNextSample();
public:
  Random*       callDuration;     // Random variable for duration of the all
  Random*       waitDuration;     // Wait time between calls
  AppVOIPEvent* pendingSSEvent;   // Scheduled start or stop event 
  AppVOIPEvent* pendingDataEvent; // Scheduled send data event
  std::string   callFileName;     // Name of AIFF file for call data
  std::string   recFileName;      // Name of AIFF file for recording rx data
  AIFF*         callFile;         // If a call audio file specified
  AIFF*         recordFile;       // If a recording file desired 
  Count_t       sampleRate;       // Audio sampling rate (default 8k)
  Count_t       sampleIndex;      // Starting sample number for next sample
  Count_t       nextRxIndex;      // Next expected received index
  long*         longBuf;          // Audio samples, 
  char*         buf;              // Holds the read/write data after compress
  bool          silenceSupression;// True if silence supression enabled
  double        ssThreshold;      // Silence supression sound intensity thresh
  bool          verbose;          // debug
  bool          verbose1;         // debug
public:
  static Count_t activeCalls;     // Debug..number of active calls
  static Codec_t codec;           // Codec to use
  static CodecCallback_t codecCb; // Callback for custom codec
};

#endif
