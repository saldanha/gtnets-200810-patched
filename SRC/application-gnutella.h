// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-gnutella.h 185 2004-11-05 16:40:27Z riley $



// Implementation of the Gnutella Peer-to-Peer Protocol
// George F. Riley, Georgia Tech, Spring 2003

#ifndef __gnutella_h__

#include <set>
#include <list>

#include "common-defs.h"
#include "ipaddr.h"
#include "application.h"
#include "tcp.h"

// Define type for IPAddr_t set
// Used for defining the IPAddresses of the well known GCache list

typedef std::set<IPAddr_t> IPAddrSet_t;

//Doc:ClassXRef
class GnuPeerInfo { // Information about active peers
public:
  GnuPeerInfo() : tcp(nil), ipPort(0,0), connected(false) { }
  GnuPeerInfo(TCP* t, IPPort_t i)
    : tcp(t), ipPort(i), connected(false) { }
public:
  TCP*     tcp;       // TCP Endpoint for this peer
  IPPort_t ipPort;    // IP and Port
  bool     connected; // True if connected, false if pending
};

typedef std::list<GnuPeerInfo> PeerList_t; // List of peers

//Doc:ClassXRef
class Gnutella : public Application {
  //Doc:Class Class {\tt Gnutella} provides a model of the behavior 
  //Doc:Class of {\em Gnutella} peer--to--peer applications.  When started,
  //Doc:Class a {\tt Gnutella} application will randomly select a
  //Doc:Class {\em GCache} server (from the list of servers specified
  //Doc:Class with the static {\tt AddGCache} method) and requests 
  //Doc:Class a list of potential peers from the GCache.  The application
  //Doc:Class also posts it's own \IPA\ to the GCache.  At the same
  //Doc:Class time, each {\tt Gnutella} application opens a listening
  //Doc:Class {\tt TCP} endpoint to accept connections from peers.
  //Doc:Class Once a list of potential clients is know, the application
  //Doc:Class selects one of the potential  peers randomly and tries
  //Doc:Class to connect.  It will continue  querying the GCache servers
  //Doc:Class and attempting peer connections until it has successfully
  //Doc:Class connected with the number of desired peers (this limit
  //Doc:Class is specified by static variable {\tt outPeerCount}.
  //Doc:Class The applications also enforce a limit on the number of
  //Doc:Class allowable accepted connections, specified by static
  //Doc:Class variable {\tt inPeerCount}.

  //Doc:Class Once a connection is established, the {\em Ping} and
  //Doc:Class {\em Query} messages are sent (Note: With this release
  //Doc:Class this functionality is not implemented.

public:
  //Doc:Method
  Gnutella();
    //Doc:Desc Constructor for {\tt Gnutella} objects.  Uses
    //Doc:Desc the default {\tt TCP} type for all connections.

  //Doc:Method
  Gnutella(const TCP&);
    //Doc:Desc Constructor, specifying  a reference to the type of {\tt TCP}
    //Doc:Desc protocol to use.
    //Doc:Arg1 Reference to any subclass of {\tt TCP}.

  //Doc:Method
  Gnutella(const Gnutella&);
    //Doc:Arg1     //Doc:Desc Copy constructor.
    //Doc:Arg1 {\tt Gnutella} object to copy.

  virtual ~Gnutella();
public:
  // Connect to a peer, IP, Port, and timeout
  void Connect(IPAddr_t, PortId_t, Time_t);
  // Send Ping Request
  void Ping();
  // Send Query
  void Query();
  // Set port number
  //Doc:Method
  void SetPort(PortId_t p) { gnuPort = p;}
    //Doc:Desc Set the port number to bind to for incoming connections.
    //Doc:Arg1 Port number for incoming connections

  // Upcalls
  // Overridden calls from application base class
  virtual void Receive(Packet*,L4Protocol*,Seq_t = 0);   // Data received
  virtual void Sent(Count_t, L4Protocol*);     // Data has been sent
  virtual void CloseRequest(L4Protocol*);      // Close request from peer
  virtual void Closed(L4Protocol*);            // Connection has closed
  virtual void ConnectionComplete(L4Protocol*);// Connection request succeeded
  virtual void ConnectionFailed(L4Protocol*,bool);// Connection request failed
  // Listener has cn rq from peer 
  virtual bool ConnectionFromPeer(L4Protocol*, IPAddr_t, PortId_t);
  virtual void StartApp();           // Called at time specified by Start
  virtual void StopApp();            // Called at time specified by Stop
  virtual void AttachNode(Node*);    // Note which node attached to
  virtual Application* Copy() const; // Make a copy of the application
  void    DBDump();                  // Print debug info
  // Private Methods
  //Doc:Method
  virtual void GCacheConnect();      // Initiate a gcache connection
    //Doc:Desc Initiate a connection to a randomly selected GCache server.

  //Doc:Method
  virtual void AddPossiblePeer(const IPPort_t&);
    //Doc:Desc Add a peer (\IPA\ and port) to list of possible peers.
    //Doc:Arg1 Reference to a pair with \IPA\ and port of possible peer.

  //Doc:Method
  virtual void ConnectToPeer();      // Try another connection
    //Doc:Desc Attempt a connection to a randomly selected peer.

  // Find a peer in a peer list
  PeerList_t::iterator FindPeerInfo(PeerList_t&, TCP*);
  // Static Methods

  //Doc:Method
  static void DefaultPort(PortId_t); // Set the default port
    //Doc:Desc Specify the default port (incoming connections) for all
    //Doc:Desc future {\tt Gnutella} applications.
    //Doc:Arg1 Port to bind to.

  // Members
  Node*    node;                     // Node we are attached to
  TCP*     gcacheTCPh;               // TCP gcache hostfile requests
  TCP*     gcacheTCPu;               // TCP gcache ip update requests
  TCP*     listenTCP;                // TCP to use for incomming connections
  bool     gcacheActive;             // GCache request is active
  IPAddr_t gcacheIP;                 // IP Address of current GCache
  PortId_t gnuPort;                  // Port to use for incoming connections
  Count_t  outConnectionLimit;       // Number of desired output connections
  Count_t  inConnectionLimit;        // Number of allowed input connections
  Count_t  transferLimit;            // Number of allowed transfer connections
  PeerList_t outConnections;         // Connections we initiated
  PeerList_t inConnections;          // Connections we accepted
  PeerList_t transferConnections;    // Connections for file transfers
  Count_t    goodOutConnections;     // Number of out connections succeeded
  Count_t    failedOutConnections;   // Out connections that failed
  Count_t    goodInConnections;      // In connections we accepted
  Count_t    rejectedInConnections;  // In connections we rejected
  Count_t    gcacheRequests;         // Number successful gcache requests
  Count_t    gcacheFailed;           // Number of gcache failures
  std::deque<IPPort_t> pendingHosts; // List of hosts we can try to connect to

  // Static methods
  //Doc:Method
  static  void AddGCache(IPAddr_t);  // Add a gcache to the list
    //Doc:Desc Specify the \IPA\ of a potential GCache to query and post.
    //Doc:Arg1 \IPA\ to add.

  // Static members
  //Doc:Member
  static IPAddrSet_t gcacheHosts;    // IPAddress of list of GCache's
    //Doc:Desc List of all potential GCache servers (\IPA{s})

  //Doc:Member
  static PortId_t    defaultPort;    // Port number for incoming connections
    //Doc:Desc Default port number to bind for incoming connections.

  //Doc:Member
  static Count_t     outPeerCount;   // Limit on desired out connections
    //Doc:Desc Limit on the number of connections that this {\tt Gnutella}
    //Doc:Desc application maintains to other {\tt Gnutella} applications.

  //Doc:Member
  static Count_t     inPeerCount;    // Limit on desired in connections
    //Doc:Desc Limit on the number of accepted incoming connections.

  //Doc:Member
  static Count_t     transferCount;  // Limit on simulataneous tranfers
    //Doc:Desc Limit on the number of simultaneous file transfers.
    //Doc:Desc (Note: Not presently implemented)
};

#endif
