// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


// Georgia Tech Network Simulator - Bluetooth BNEP class
// George F. Riley, Georgia Tech, Spring 2004

#ifndef __bnep_h
#define __bnep_h

//
// Bluetooth Network Encapsulation Protocol 
// (BNEP) 1.0 
//
#include "timer.h"
#include "bluetus.h"
#include "l2cap.h"
#include "application-blue.h"
#include "common-defs.h"

//define the BNEP Type values
#define BNEP_GENERAL_ETHERNET					0x00
#define BNEP_CONTROL							0x01
#define BNEP_COMPRESSED_ETHERNET				0x02
#define BNEP_COMPRESSED_ETHERNET_SOURCE_ONLY	0x03
#define BNEP_COMPRESSED_ETHERNET_DST_ONLY		0x04

//define BNEP control type value
#define BNEP_CONTROL_COMMAND_NOT_UNDERSTOOD		0x00
#define BNEP_SETUP_CONNECTION_REQUEST_MSG		0x01
#define BNEP_SETUP_CONNECTION_RESPONSE_MSG		0x02
#define BNEP_FILTER_NET_TYPE_SET_MSG			0x03
#define BNEP_FILTER_NET_TYPE_RESPONSE_MSG		0x04
#define BNEP_FILTER_MULTI_ADDR_SET_MSG			0x05
#define BNEP_FILTER_MULTI_ADDR_RESPONSE_MSG		0x06

class BlueApplication;

struct BNEPPacket {
	uChar ucType_E;
		//7 bits Type followed by E flag
	uChar ucPayload[0];
};

//BNEP General Ethernet Packet header.
struct BNEP_ETHERNET_Hdr {
	uChar  ucType_E;
	BdAddr Dst;
	BdAddr Src;
	uShort usProtocolType; /* Networking Protocol Type*/
	uChar ucPayload[0];

	uShort HdrSize() {
		return sizeof(uChar) + 2*sizeof(BdAddr) + sizeof(uShort);
	}
	//extension header or Payload
};

///////////////////////////////
// BNEP control message format
///////////////////////////////
//
//BNEP Control Packet header
struct BNEP_CONTROL_Hdr {
	uChar ucType_E;
	uChar ucCtrlType;
};

struct BNEP_CNU_Packet { //Command Not Understand == "CNU"
	BNEP_CONTROL_Hdr CtrlHdr;
	uChar ucUCType; //Unkown Control Type
};

//response format for all kinds of request:
struct BNEP_RSP_Packet {
	BNEP_CONTROL_Hdr CtrlHdr;
	//extension may exist here.
	uShort usResponse;
};
	
struct BNEP_SCReqM_Packet { //Setup_Connection_Request_Msg == "SCRM"
	BNEP_CONTROL_Hdr CtrlHdr;
	//here may contains extension header based on flag "E".
	uChar ucUUIDSize; //ucUUIDSize = N

	uChar ucPayload[0]; //contains DstUUID and SrcUUID
	//uChar *pDstUUID;  //0---N
	//uChar *pSrcUUID;  //0---N
};

struct ProtocolStartEnd {
	uShort usTypeRangeStart;
	uShort usTypeRangeEnd;
};
struct BNEP_FNTSM_Packet { //Filter_Net_Type_Set_Msg = "FNTSM"
	BNEP_CONTROL_Hdr CtrlHdr;
	uShort usLength;
	ProtocolStartEnd *pStartEnd;
};

struct MultiAddrStartEnd {
	//MACAddr AddrStart;
	//MACAddr AddrEnd;
	uShort AddrStart;
	uShort AddrEnd;
};
struct BNEP_FMASM_Packet { //Filer_Multi_Addr_Set_Msg = "FMASM"
	BNEP_CONTROL_Hdr CtrlHdr;
	//optionally here contains extension
	uShort usLength;
	MultiAddrStartEnd *pAddrStartEnd;
};


//BNEP Compressed Ethernet Packet header
struct BNEP_COMPRESSED_ETHERNET_Hdr {
	uChar ucType_E;
	uShort usProtocolType; /* Networking Protocol Type*/

	uShort HdrSize() {
		return sizeof(uChar) + sizeof(uShort);
	}

	//extension header or Payload.
	uChar ucPayload[0];	
};

//BNEP Compressed Ethernet source only packet header
struct BNEP_COMPRESSED_ETHERNET_SRC_ONLY_Hdr {
	uChar ucType_E;
	BdAddr Src;
	uShort usProtocolType;
	
	uShort HdrSize() {
		return sizeof(uChar) + sizeof(BdAddr) + sizeof(uShort);
	}
	//extension header or Payload
	uChar ucPayload[0];	
};

//BNEP Compressed Ethernet Dest only packet header
struct BNEP_COMPRESSED_ETHERNET_DST_ONLY_Hdr {
	uChar ucType_E;
	BdAddr Dst;
	uShort usProtocolType;

	uShort HdrSize() {
		return sizeof(uChar) + sizeof(BdAddr) + sizeof(uShort);
	}
	//extension header or Payload
	uChar ucPayload[0];	
};

////////////////////
//Extension header
////////////////////

//BNEP Extension header
struct BNEP_EXTENSION_Hdr{
	uChar ucType_E;
	uChar ucLength;
	//extension payload
};

#define BNEP_EXTENSION_CONTROL	0x00

//BNEP Extension control packet header
struct BNEP_EXTENSION_CONTROL_Hdr {
	uChar ucType_E;
	uChar ucLength;
	uChar ucCtlType;
	//control packet based on Control Type.
};

//Timer events
class BNEPTimerEvent : public TimerEvent {
    public:
    // the types of the BNEP timer events
          typedef enum {CONNREQ_TIMEOUT, FILTERREQ_TIMEOUT
                         } BNEPTimerEvent_t;
    public:
           BNEPTimerEvent(Event_t ev) 
                 : TimerEvent(ev) { } 
	       virtual ~BNEPTimerEvent() {}
};

class L2cap;

// Define the start/stop events
class BnepEvent : public Event {
public:
  typedef enum { STARTB, STOPB, STARTBM } BnepEvent_t;
  BnepEvent() { }
  BnepEvent(Event_t ev) : Event(ev) { }
};

class Bnep : public TimerHandler , public Handler{
	public:
		//constructor
		Bnep(L2cap *pL2cap);
		//copy constructor
		//Bnep* Copy() const;
		//destructor.
		virtual ~Bnep();
		uShort PSM() {
			return 0x000F; //BNEP;
		}
	public:
		//test method for lower layer, it will continuing sending data
		BdAddr dst;
		void TestL2CAP(BdAddr dst);
		void Start(Time_t t, BdAddr dst);
		void Stop(Time_t t);
		void Handle(Event *e, Time_t);
		//end of test

  		//Connection management
  		void AttachApplication(BlueApplication* a) { pApplication = a;}
		
		void SetupConnReq(uChar ucUUIDSize, uChar *pDstUUID,
						uChar *pSrcUUID);
		uShort RecvConnRsp(BNEPPacket *pPacket);
        uShort RecvConnReq(BNEPPacket *pPkt);
		uShort SendRsp(bool bE, uChar ucCtrlType, uShort usExtLen, 
						void * pExt, uShort usResponse);
		
		void RecvControlPacket(BNEPPacket *pPacket);		          
		
		uShort SendFilterNetSet(bool bE, uShort usListLength, void * pExt, 
						void* NetStartEnd);
		uShort RecvReqFilterNetSet(BNEPPacket *pPkt);
		uShort RecvRspFilterNet(BNEPPacket *pPkt);
		uShort SendFilterMultiAddrSet(bool bE, uShort usListLength, 
						void * pExt,  void* MultiAddrStartEnd);
		uShort RecvReqFilterMultiAddrSet(BNEPPacket *pPkt);
		uShort RecvRspFilterMultiAddr(BNEPPacket *pPkt);
																	   
		
	public:	
		uShort DataIndication(void *pPacket, uShort usCID); //channel id
        //General version, src and dst address are required
		uShort DataRequest(bool bE, BdAddr Src, BdAddr Dst, uShort 
						usProtocolType, uShort usLenExt, 
						void * pExt, uShort usLenData, void * pData);
		//compressed version, no address information is required
		uShort DataRequest(bool bE, uShort usProtocolType, uShort 
						usLenExt, void * pExt, uShort usLenData, 
						void * pData);
		//compressed version, either src or dst is required.
		uShort DataRequest(bool bE, BdAddr Src, uShort usProtocolType,
					   uShort usLenExt, void * pExt, uShort usLenData,
					   void * pData, uChar ucType);
		//application send
		NCount_t Send(Size_t s);
		bool IsConn() {return connected;}
		
	public:
		//Signalling Interface to L2CAP layer.
		uShort L2CA_ConnectInd(uShort usPSM, uChar ucIdentifier,
					uShort usLocalChannelID) ;

		uShort L2CA_ConnectCfm(uShort usLocalChannelID) ;

		uShort L2CA_ConnectCfmNeg(uShort usLocalChannelID);
		uShort L2CA_ConnectPnd(uShort usCID) ;

		uShort L2CA_ConfigInd(uShort usLocalChannelID, uChar ucIdentifier);

		uShort L2CA_ConfigCfm(uShort usLocalChannelID) ;

		uShort L2CA_ConfigCfmNeg(uShort usLocalChannelID) ;

		uShort L2CA_DisconnectInd(uShort usLocalChannelID, 
				uChar ucIdentifier) ;

		uShort L2CA_DisconnectCfm() ;
		
		L2cap *pL2CAP;	//change to public for stat output in application
				
	private:
		Timer timer;
		BNEPTimerEvent *pConnReqTimeout;
		BNEPTimerEvent *pFilterReqTimeout; //only one of them exist.

		//Timer handle methods
		virtual void Timeout(TimerEvent *);
		//Timer management
		void ScheduleTimer(Event_t, BNEPTimerEvent*&, Time_t);
		void CancelTimer(BNEPTimerEvent*&, bool delTimer = false);
		void CancelAllTimers();

	private:
		
#define CLOSED_BNEP		0x00
#define HALF_OPEN_BNEP	0x01 //set after sending SetupConnReq		
#define CONNECTED_BNEP	0x02		
		uChar ucState; 
		//before getting Setup_Connection response successfully
		//usState = CLOSED;
		//
		uChar ucUUIDSize;
		uChar *pSrcUUID;
		uChar *pDstUUID;
		
		uShort usLocalCID;//get this value after you establish L2CAP conn

		BlueApplication* pApplication;	//Application object
		bool connected;

		//Add a net type filter list
		
		//Add a multicast address filter list.

};

#endif
