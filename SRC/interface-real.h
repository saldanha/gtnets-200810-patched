// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: interface-real.h 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - Interface class
// George F. Riley.  Georgia Tech, Spring 2002

// Define class to model network link interfaces
// Implements the Layer 2 protocols

#ifndef __interface_real_h__
#define __interface_real_h__

#include "common-defs.h"
#include "simulator.h"
#include "protocol.h"
#include "packet.h"
#include "handler.h"
#include "ipaddr.h"
#include "notifier.h"
#include "macaddr.h"
#include "node.h"
#include "link.h"
#include "link-real.h"
#include "eventcq.h"
#include "interface.h"
#include "arp.h"
#include "worm-containment.h"

class L2Proto;
class Node;
class Queue;
class LinkEvent;
class NotifyHandler;

#define CLASS_C_MASK 0xffffff00
#define CLASS_B_MASK 0xffff0000
#define CLASS_A_MASK 0xff000000
#define MASK_NONE    0x00000000

//Doc:ClassXRef
class IpMaskCount { // IP Address, mask, and hop count
public:
  IpMaskCount(IPAddr_t i, Mask_t m, Count_t c = 0) 
      : ip(i), mask(m), hopCount(c) { }
public:
  IPAddr_t ip;
  Mask_t   mask;
  Count_t  hopCount;
};

typedef std::vector<IpMaskCount> IPMaskCountVec_t;

//Doc:ClassXRef
class InterfaceReal : public Interface
{
  //Doc:Class Interface objects are assigned to nodes, and have
  //Doc:Class corresponding link objects to represent the communications
  //Doc:Class links.  Interfaces also have queues  for queuing packets
  //Doc:Class when a transmit is requested when the link is busy,
  //Doc:Class and an associated layer 2 protocol objects
  //Doc:Class Interfaces are typically not allocated directly by
  //Doc:Class a \GTNS\ simulation program, but rather are created
  //Doc:Class indirectly by {\tt AddDuplexLink} and {\tt AddSimplexLink}
  //Doc:Class methods of {\tt Node} objects.

public:
  //Doc:Method
  InterfaceReal(const L2Proto& l2 = L2Proto802_3(),
		IPAddr_t i   = IPADDR_NONE,
		Mask_t   m   = MASK_ALL,
		MACAddr  mac = MACAddr::NONE,
		bool bootstrap = false);
    //Doc:Desc Constructs a new real interface object.
    //Doc:Arg1 The layer 2 protocol type to associate with this interface.
    //Doc:Arg2 The \IPA\ to assign to this interface.
    //Doc:Arg3 The address mask to assign to this interface.
    //Doc:Arg4 The {\tt MAC} address to assign to this interface.

  virtual ~InterfaceReal();

  void SetARPTimeout(Time_t t){ arptimeout = t;}


  //Doc:Method
  bool Send(Packet*, IPAddr_t, int); // Enque and transmit
    //Doc:Desc Send a packet on this interface.
    //Doc:Arg1 Packet to send.
    //Doc:Arg2 \IPA\ of destination
    //Doc:Arg3 LLC/SNAP type
    //Doc:Return True if packet sent or enqued, false if dropped.

 //Doc:Method
  bool Xmit(Packet*, IPAddr_t, int); // Enque and transmit
    //Doc:Desc Send a packet on this interface.
    //Doc:Arg1 Packet to send.
    //Doc:Arg2 \IPA\ of destination
    //Doc:Arg3 LLC/SNAP type
    //Doc:Return True if packet sent or enqued, false if dropped.

  //Doc:Method
  bool Send(Packet*, const MACAddr&, int); // Enque and transmit
    //Doc:Desc Send a packet on this interface.
    //Doc:Arg1 Packet to send.
    //Doc:Arg2 MAC address of destination
    //Doc:Arg3 LLC/SNAP type
    //Doc:Return True if packet sent or enqued, false if dropped.

  //Doc:Method
  bool Send(Packet*, Word_t); // Enque and transmit
    //Doc:Desc Send a broadcast packet on this interface.
    //Doc:Arg1 Packet to send.
    //Doc:Arg2 MAC address of destination
    //Doc:Arg3 LLC/SNAP type
    //Doc:Return True if packet sent or enqued, false if dropped.

  //Doc:Method
  bool EnquePacket(Packet*);
    //Doc:Desc Enque this packet for later transmission
    //Doc:Return True if successfully enqueued, false if dropped.

  // Handler
  virtual void Handle(Event*, Time_t);
  // Notifier
  virtual void Notify(void*);
  
  // Interface Methods
  //Doc:Method
  Count_t      PeerCount() const;          // Number of peers
    //Doc:Desc Determine how many peer nodes  are associated with this 
    //Doc:Desc interface.
    //Doc:Return Count of peers.

  //Doc:Method
  IPAddr       PeerIP(int npeer) const;    // Get peer IP addr if known
    //Doc:Desc Get the \IPA\ of the specified peer, by peer index.
    //Doc:Arg1 The peer index of the desired peer.
    //Doc:Return The \IPA\ of the specified peer.
    //Doc:Return Returns {\tt IPADDR_NONE} if the peer does not exist.

  //Doc:Method
  IPAddr       NodePeerIP(Node*) const;    // Get peer IP addr if known
    //Doc:Desc Get the \IPA\ of the peer's interface directly connected
    //Doc:Desc to this interface.
    //Doc:Arg1 Node pointer for the desired peer.
    //Doc:Return \IPA\ of the specified peer.
    //Doc:Return Returns {\tt IPADDR_NONE} if the peer does not exist.

  //Doc:Method
  bool         NodeIsPeer(Node*) const;    // Find out if node is a peer
    //Doc:Desc Determine if the specified node is a directly connected
    //Doc:Desc peer on this interface.
    //Doc:Arg1 Node pointer of the desired peer.
    //Doc:Return True if the node is a directly connected peer on this
    //Doc:Return interface.

  //Doc:Method
  Count_t      NodePeerIndex(Node*) const; // Get peer index for node
    //Doc:Desc Get the peer index for the specified node.
    //Doc:Arg1 Node pointer for the desired peer.
    //Doc:Return The peer index of the specified node.

  //Doc:Method
  void             SetNode(Node*);
    //Doc:Desc Set the  node pointer to which this interface is associated.
    //Doc:Arg1 Pointer to the node this interface is associated with.

#ifdef MOVED_TO_BASIC
  //Doc:Method
  Link*        GetLink() const;            // Return the link pointer
    //Doc:Desc Get the link object associated with this interface.
    //Doc:Return A pointer to the associated link object.

  //Doc:Method
  void         SetLink(Link*);
    //Doc:Desc Set the associated link to the specified link pointer.
    //Doc:Arg1 Link pointer to set.
#endif

  //Doc:Method
  Queue*       GetQueue() const;           // Return the queue pointer
    //Doc:Desc Get the packet queue associated with this interface.
    //Doc:Return A pointer to the associated packet queue.

  //Doc:Method
  void         SetQueue(const Queue& q);          // Set a new queue object
    //Doc:Desc Set a new packet queue object.
    //Doc:Arg1 The type of packet queue  object to use.  This object
    //Doc:Arg1 is copied to this interface, so an anonymous temporary
    //Doc:Arg1 object can be used, or the same object can be passed
    //Doc:Arg1 to multiple interfaces.

  bool         QueueDetailed() const;             // True if queue is detailed

  //Doc:Method
  L2Proto*     GetL2Proto() const { return pL2Proto;} // Layer 2 proto object
    //Doc:Desc Get the layer 2 protocol object associated with this interface.
    //Doc:Return A pointer to the layer 2 protocol object associated with this 
    //Doc:Return interface.

  //Doc:Method
  void         SetL2Proto(const L2Proto& l2);
    //Doc:Desc Set a new layer 2 protocol object for this interface.
    //Doc:Return The new layer 2 protocol object to use.

  //Doc:Method
  void         AddNotify(NotifyHandler*,void*);   // Add a notify client
    //Doc:Desc Add a notification client.  The client is notified when
    //Doc:Desc the associated link is free.
    //Doc:Arg1 A pointer to the notify client.
    //Doc:Arg2 A blind pointer to pass back to the notify client.

  //Doc:Method
  void         CancelNotify(NotifyHandler*);      // Cancel pending notif.
    //Doc:Desc Cancel an existing notification request.
    //Doc:Arg1 A pointer to the notifi client to cancel.

  //Doc:Method
  MACAddr      IPToMac(IPAddr_t) const;           // Return mac addr for ip
    //Doc:Desc Determine the {\em MAC} address of the interface
    //Doc:Desc associated with a peer, specified by \IPA.
    //Doc:Arg1 The \IPA\ to search for.
    //Doc:Return The {\em MAC} address of the remote interface associated
    //Doc:Return with the specified \IPA.

  //Doc:Method
  Count_t      NeighborCount(Node*) const;        // Number of rtg neighbors
    //Doc:Desc Get the count of directly connected neighbors on this interface.
    //Doc:Arg1 Node querying neighbors (significant for Ethernet links)
    //Doc:Return Count of directly connect neighbors.

  // Get neighbor list    
  //Doc:Method
  void         Neighbors(NodeWeightVec_t&, bool forceAll = false);
    //Doc:Desc Get a list of directly connected neighbors on this interface.
    //Doc:Arg1 An output parameter, with a vector of nodes and associated
    //Doc:Arg1 link weights directly connected on this link.
    //Doc:Arg2 ForceAll indicates that all neighbors  should be reported.
    //Doc:Arg2 Some link types (ie. {\tt Ethernet} sometimes report
    //Doc:Arg2 zero neighbors, for efficiency in route computations.

  //Doc:Method
  IPAddr_t     DefaultPeer();              // Get default peer from link
    //Doc:Desc Get the \IPA\ of the default peer.
    //Doc:Return The \IPA\ of the default peer.

  //Doc:Method
  bool         IsLocalIP(IPAddr_t) const;  // True if IP is on this i/f
    //Doc:Desc Determine if the specified \IPA\ is a node directly connected
    //Doc:Desc to this interface.
    //Doc:Arg1 \IPA\ to check.
    //Doc:Return True if directly connected \IPA, false if not.

  //Doc:Method
  Mult_t       TrafficIntensity() const;   // TotBytesSent/LineSpeed
    //Doc:Desc Return the traffic intensity on this interface.  Calculated
    //Doc:Desc as the total bytes sent per second divided by the link
    //Doc:Desc bandwidth.
    //Doc:Return The traffic  intensity for this interface.

  // Used by distributed simulation
  //Doc:Method
  void         AddRemoteIP(IPAddr_t, Mask_t, Count_t = 0);// Add to rmt IP list
    //Doc:Desc Add an \IPA\ and mask to the list of the \IPA{s} that
    //Doc:Desc can be reached via the remote link associated with this
    //Doc:Desc interface.
    //Doc:Desc Useful only for distributed simulations.
    //Doc:Arg1 The \IPA\ to add.
    //Doc:Arg2 The address mask to add.
    //Doc:Arg3 Hop count to the destination set

  //Doc:Method
  void         HandleLLCSNAP(Packet*, bool);
    //Doc:Desc Process the LLCSnap header on a wireless packet.
    //Doc:Arg1 Pointer with packet with LLCSnap header to process.
    //Doc:Arg2 True if "First bit RX" event
  //Doc:Method
  virtual bool     IsWireless() const { return false;}// True if wireless interface
  //Doc:Desc Test if this interface is wireless 
  //Doc:Return Always false for InterfaceReal nodes
  
  //Doc:Method
  virtual bool     IsReal() const { return true;}     // True if wireless interface
  //Doc:Desc Test if this interface is real 
  //Doc:Return Always true for InterfaceReal interfaces
  
  virtual bool     IsEthernet() const {return false;}               // True if ethernet interface
  
  void SetWormContainment(WormContainment*);
  WormContainment* GetWormContainment() { return wormcontainment;}

  void UseARP(bool);  // use the ARP protocol

protected:
  // PacketRxStart is called by the event handler for the real interface
  // before calling the l2 data indication.  This
  // which allows the wired and wireless interface implementations
  // to react differently.
  virtual void        PacketRxStart(LinkEvent*);
  // PacketRxDone is called by the event handler for the real interface
  // after calling the l2 data indication.
  virtual void        PacketRxEnd(bool, Size_t);

  // Make all data items private, since we need to access via
  // methods, to facilitate real/ghost interface implementation

private:
  //Link*            pLink;           // Associated link (moved to basic)
  Queue*           pQueue;          // Associated Queue
  NList_t          notifications;   // Deque of pending notifications
  LinkEvent*       notifEvent;      // Pending notification event
  DCount_t         totBytesSent;    // Track total bytes requested to send
  L2Proto*         pL2Proto;        // Associated layer 2 protocol
  IPAddr_t         defaultPeer;     // Used to cache defaultPeer IPAddr
  bool             usearp;          // use ARP protocol
  ARP*             arpmodule;       // The ARP module
  Time_t           arptimeout;      // timeout for the arp entries
  WormContainment* wormcontainment; // The worm containment module

 private:
  // The following is for the distibuted version.  List IP addresses 
  // (with masks) that are reached via this interface.  The Ghost Node
  // approach (when implemented) will obviate the need for this
  IPMaskCountVec_t remoteIPList;// List if remote IP/Masks reached via this i/f
private:
  void        CallNotification(bool); // Notify l4 proto of free buffer space
public:
  static Count_t count;    // debug
};

#endif
