using namespace std;

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#include <iostream>
#include <fstream>

#include "debug.h"
#include "simulator.h"
#include "node.h"
#include "duplexlink.h"
#include "application-cbr.h"
#include "ratetimeparse.h"
#include "trace.h"
#include "udp.h"
#include "validation.h"  

#include "application-chord.h"

#define SIMTIME 200

Chord *ch0a, *ch1a, *ch0b, *ch1b;

void print_finger_tables(Time_t t) {
	ch0a->PrintFingerTable();
	ch1a->PrintFingerTable();
	ch0b->PrintFingerTable();
	ch1b->PrintFingerTable();
	
	cout << Simulator::Now() << " Incorrect Fingers:" << Chord::GetStats()->CountIncorrectFingers() << endl;
	
	if(t > SIMTIME - 2) {
		// Need a better hook to do this
		// (The ChordStats destructor is never called, so the log has
		// to be close like this)
		cout << "Closing Log" << endl;
		Chord::GetStats()->CloseLog();
		
		// Print out all identifiers registered in the stats module
		Chord::GetStats()->PrintNodeIds();
	}
}

int main(int argc, char** argv) {
	Validation::Init(argc, argv);
	Simulator s;

	s.AnimationUpdateInterval(Time("10us"));
	s.StartAnimation(0, true);
	Chord::UseVis(true);
	
	ChordId::SetBitLength(5); // 5-bit identifiers
	
	// Open the trace file
	Trace* gs = Trace::Instance();
	gs->Open("test-chord.txt");
	gs->IPDotted(true);

	Node* n0 = new Node();
	n0->SetLocation(0,0);
	Node* n1 = new Node();
	n1->SetLocation(4,0);
	Node* pn = n0; // Prior
	Interface* pi = pn->AddInterface(L2Proto802_3(), 
                        	  0xc0010001, Mask(24), MACAddr::Allocate());
	Link::DefaultRate(Rate("10Mb")); // Set to 10Megabits /sec 
	//Link::DefaultRate(Rate("1Mb"));  // Set to 1Megabits /sec 
	// Create 3 intermediate nodes
	for (int i = 0; i < 3; ++i) {
		Node* nn = new Node();
		nn->SetLocation(i + 1, 0);
		//nn->SetTrace(Trace::DISABLED);  // Trace nothing on this node
		Interface* ni = nn->AddInterface();
		DuplexLink(pi, ni);
		pi = nn->AddInterface(); // add right side interface
	}
	
	Interface* ni = n1->AddInterface(L2Proto802_3(),
                        	  0xc0020001, Mask(24), MACAddr::Allocate());
	DuplexLink(pi, ni);

	// Increase detail of L3 trace messages
	IPV4::Instance()->DetailOn(IPV4::TOTALLENGTH);
	IPV4::Instance()->SetTrace(Trace::ENABLED);

	IPV4::Instance()->route_locally = true;

	Chord::GetStats()->OpenLog("chordtest.log");
	
	// Generate Chord objects
	ch0a = new Chord();
	ch0a->AttachNode(n0);
	
	ch1a = new Chord();
	ch1a->AttachNode(n1);

	ch0b = new Chord();
	ch0b->SetPort(12345);
	ch0b->AttachNode(n0);
	
	ch1b = new Chord();
	ch1b->SetPort(34567);
	ch1b->AttachNode(n1);
	
	n0->SetTrace(Trace::ENABLED); // Trace information to/from this protocol
	n1->SetTrace(Trace::ENABLED); // Trace information to/from this protocol

	ch0a->Start(0);
	ch1a->Start(0);
	ch0b->Start(0);
	ch1b->Start(0);

	ch0a->Create(2);
	ch1a->Join(3, IPAddr("192.1.0.1"), CHORD_STD_PORT);
	ch0b->Join(5, IPAddr("192.1.0.1"), CHORD_STD_PORT);
	ch1b->Join(30, IPAddr("192.1.0.1"), 12345);
	
	ch0b->Die(100);
	
	s.Progress(1);
	s.ProgressHook(print_finger_tables);

	s.StopAt(SIMTIME);

	// Start the simulation
	s.Run();
}
