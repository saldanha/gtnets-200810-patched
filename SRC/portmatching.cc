/*

* GTNetS provides a portable C++ class library for network simulations
*
* Copyright (C) 2003 George F. Riley
*
* No guarantees or warranties or anything are provided or implied in any way
* whatsoever.  Use this program at your own risk.  Permission to use this
* program for any purpose is given, as long as the copyright is kept intact.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

* Permission is hereby granted for any non-commercial use of this code

*/

// Georgia Tech Network Simulator - PortMatching class
// Mohamed Abd Elhafez.  Georgia Tech, Spring 2004

// Implements the port matching algorithm for worm containment


#include <iostream>

#include "debug.h"
#include "portmatching.h"
#include "l2proto802.3.h"
#include "node.h"
#include "droppdu.h"
#include "hex.h"
#include "udp.h"

using namespace std;

#define SENSITIVITY 0.4
#define TIMEOUT 2;
#define ALPHA 0.125
#define DELTA 3
#define QTIME 500


PortMatching::PortMatching():qtime(QTIME), starttime(0)
{
  cout << "Creating port matching module" << endl;
  incoming.clear();
  outgoing.clear();
  Scheduler::Schedule(new DEWPEvent(DEWPEvent::PROCESS_QUEUE), SENSITIVITY, this);
}


void PortMatching::AddIP( SPortMap_t::iterator j, IPAddr_t ip)
{
  bool found = false;
  IPNPair_t p = j->second;
  
  // search for the ip address
  for (unsigned int i = 0; i < p.first.size(); ++i)
    {
      if (ip == p.first[i])
	found = true;
    }
  
  if(!found)
    {
      //cout << "adding new IP " << IPAddr::ToDotted(ip) << endl;
      p.first.push_back(ip);
    }
  suspicious[j->first] = p;
}


bool PortMatching::IsInfected( PortId_t port )
{
  bool found = false;
   
  // cout << "checking infected for port #" << port << endl;
  // search for the ip address
  for (unsigned int i = 0; i < infected.size(); ++i)
    {
      if(port == infected[i].port)
	{
	  found = true;
	  break;
	}
    }
  
  return found;
}


void PortMatching::CheckInfected( SPortMap_t::iterator j )
{
  IPNPair_t p = j->second;
  

  if (Simulator::Now() >= starttime)
    {
      if(j->first == 80 ) return;
      //      if (p.second < 1) p.second = 1;

      if (p.first.size() > p.second * ( 1 + DELTA) )
	{
	  infected.push_back(PortTime(j->first, Simulator::Now() + qtime));
	  cout << "N = " << p.first.size() << " N' = " << p.second << endl;
	  cout << "port " << j->first << "Is infected" << endl;
  	}

  //  cout << "N = " << p.first.size() << " N' = " << p.second << endl;
    }

  p.second = ALPHA * p.second + (1 - ALPHA) * p.first.size();

  p.first.clear();
  suspicious[j->first] = p;
}


bool PortMatching::IsOutIface(Interface* iface)
{
  bool found = false;
  //  cout << "iface = " << iface << " outif = " << outIfs[0] << endl;
  for (unsigned int i = 0; i < outIfs.size(); ++i)
    {
      if (outIfs[i] == iface)
	found = true;
    }
  return found;
}


void PortMatching::ProcessOutPacket( Packet* p, IPAddr_t i,  int type, Interface* iface)
{
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
  IPAddr_t destip = iphdr->dst;
  PortId_t destp;
  
  if(iphdr->protocol == TCP_PROTOCOL)
    {
      TCPHeader* tcphdr = (TCPHeader*)p->PeekPDU(1);
      if(tcphdr->flags != TCP::SYN)
	{
	  //cout << "PortMatching:: Outgoing not SYN packet" << endl;
	  iface->Xmit(p, i, type);
	  return;
	}
      destp = tcphdr->destPort;
      if (destp == 80)
	{
	   iface->Xmit(p, i, type);
	   return;
	}
    }
  else if (iphdr->protocol == UDP_PROTOCOL)
    {
      UDPHeader* udphdr = (UDPHeader*)p->PeekPDU(1);
      destp = udphdr->destPort;
      //cout << "Outgoing UDP pkt " << " On port " << destp << endl;
    }
  
  if (IsInfected(destp))
    {
      // drop packet 
      //cout << "infected port #" << destp << " dropping packet " << p->uid <<endl;
      
      DropPDU d("L3-VT", p);
      iface->GetNode()->TracePDU(nil, &d);
      DEBUG(0,(cout << "Packet Dropped " << endl));
      Stats::pktsDropped++;
      delete p;
      
      return;
    }

  SPortMap_t::iterator j = suspicious.find(destp);
  if(j != suspicious.end())
    {
      AddIP(j, destip);
      iface->Xmit(p, i, type);
      return;
    }

  if(IsOutIface(iface))
    {
      // outgoing packet
        
      // add to outgoing list
      outgoing[destp] = Simulator::Now() + TIMEOUT;
      
      // Check match with incoming list
      PortMap_t::iterator k = incoming.find(destp);
      if(k != incoming.end())
	{
	  // add to the suspicious list
	  suspicious[destp] = IPNPair_t(IPVec_t(), 1);
	}
    }
  else
    {
      // incoming packet

      // add to incominging list
      incoming[destp] = Simulator::Now() + TIMEOUT;
      
      // Check match with outgoing list
      PortMap_t::iterator k = outgoing.find(destp);
      if(k != outgoing.end())
	{
	  // add to the suspicious list
	  suspicious[destp] = IPNPair_t(IPVec_t(), 0);
	}
    }
  iface->Xmit(p, i, type);
}



// void PortMatching::ProcessInPacket( Packet* p, Interface* iface)
// {
//   //cout << "PortMatching:: Processing in packet " << endl;
//   IPV4Header* iphdr = (IPV4Header*)p->PeekPDU(2);
//   IPAddr_t destip = iphdr->dst;
//   PortId_t destp;
//   if(iphdr->protocol == TCP_PROTOCOL)
//     {
//       TCPHeader* tcphdr = (TCPHeader*)p->PeekPDU(3);
//       if(tcphdr->flags != TCP::SYN)
// 	{
// 	  //  cout << "PortMatching:: Incoming not SYN packet" << endl;
// 	  iface->GetL2Proto()->DataIndication(p);
// 	  return;
// 	}
//       destp = tcphdr->destPort;
//     }
//   else if (iphdr->protocol == UDP_PROTOCOL)
//     {
//       UDPHeader* udphdr = (UDPHeader*)p->PeekPDU(3);
//       destp = udphdr->destPort;
//       //cout << "Incoming UDP pkt " << " On port " << destp << endl;
//     }
  
//  if (IsInfected(destp))
//   {
//     // drop packet 
//     cout << "infected port #" << destp << " dropping packet " << p->uid <<endl;
    
//     DropPDU d("L3-VT", p);
//     iface->GetNode()->TracePDU(nil, &d);
//     DEBUG(0,(cout << "Packet Dropped " << endl));
//     Stats::pktsDropped++;
//     delete p;
    
//     return;
//   }

//   SPortMap_t::iterator j = suspicious.find(destp);
//   if(j != suspicious.end())
//     {
//       AddIP(j, destip);
//     }
//   else
//     {
//       // add to incominging list
//       incoming[destp] = Simulator::Now() + TIMEOUT;
      
//       // Check match with outgoing list
//       PortMap_t::iterator k = outgoing.find(destp);
//       if(k != outgoing.end())
// 	{
// 	  // add to the suspicious list
// 	  suspicious[destp] = IPNPair_t(IPVec_t(), 0);
// 	}
//     }

//  //  cout << "Outgoing List contents" << endl;
// //   for (PortMap_t::iterator n = outgoing.begin(); n != outgoing.end(); n++)
// //     {
// //       cout << "Port: " << n->first << " T: " << n->second << endl;; 
// //     }
// //   cout << "Incoming List contents" << endl;
// //   for (PortMap_t::iterator n = incoming.begin(); n != incoming.end(); n++)
// //     {
// //       cout << "Port: " << n->first << " T: " << n->second << endl;; 
// //     }
  
//   iface->GetL2Proto()->DataIndication(p);
// }

  
void PortMatching::Handle(Event* e, Time_t t)
{
  DEWPEvent* te = (DEWPEvent*)e;
  //  IPV4Header* iphdr ;

  switch(te->event) {
  case DEWPEvent::PROCESS_QUEUE :
    // loop through port lists delete old entries
    for (PortMap_t::iterator j = incoming.begin(); j != incoming.end(); ++j)
      {
	if(j->second <= Simulator::Now())
	  {
	    incoming.erase(j);
	  
	  }
      }

    for (PortMap_t::iterator j = outgoing.begin(); j != outgoing.end(); ++j)
      {
	if(j->second <= Simulator::Now())
	  {
	    outgoing.erase(j);
	  
	  }
      }

     for (PortVec_t::iterator j = infected.begin(); j != infected.end(); ++j)
      {
	if((*j).time <= Simulator::Now())
	  {
	    infected.erase(j);
	    --j;
	  }
      }

    for (SPortMap_t::iterator j = suspicious.begin(); j != suspicious.end(); ++j)
      {
	CheckInfected(j);
      }
    // Schedule the next event
    //if(!incoming.empty() || !outgoing.empty())
    //  {
	Scheduler::Schedule(new DEWPEvent(DEWPEvent::PROCESS_QUEUE), SENSITIVITY, this);
	// }
    break;
  default:
    break;
  }
  delete te;
}


