// Test animation of wired networks
// George F. Riley, Georgia Tech, Fall 2002

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "dumbbell.h"
#include "tcp-tahoe.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "validation.h"
#include "queue.h"

#ifdef HAVE_QT
#include <qnamespace.h>
#endif

#define N_LEAF 5

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  Count_t nl = N_LEAF;
  if (argc > 1)
    {
      nl = atol(argv[1]);
    }

  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);
  
  //Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  //tr->Open("testanim.txt");
  TCP::LogFlagsText(true);       // Log TCP flags in text mode
  Linkp2p lk(Rate("10Mb"), Time("20ms"));
  Dumbbell b(nl, nl, 0.1,
             IPAddr("192.168.1.1"), IPAddr("192.168.2.1"), lk);
  // Specify the bounding box
  b.BoundingBox(Location(0,0), Location(10,10));
  // Animate the queue on the bottleneck link
  Queue* q = b.LeftQueue();
  q->Animate(true);
  
  // Create the clients and servers
  for (Count_t i = 0; i < b.LeftCount(); ++i)
    {
      Node* l = b.Left(i);
      Node* r = b.Right(i);
      // Create a tcp server on right and tcp source on left
      cout << "Testanim adding app" << endl;
      TCPServer* tcpApp =
        (TCPServer*)r->AddApplication(TCPServer());
      tcpApp->BindAndListen(r, 80);
      tcpApp->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
      cout << "Testanim adding tcpsend " << endl;
      TCPSend* ts = (TCPSend*)l->AddApplication(
          TCPSend(r->GetIPAddr(), 80,
                  Constant(1000000), TCPTahoe()));
      if (ts)
        { // Application added
#ifdef HAVE_QT
          if (i == 0)
            { // Debug..set packets to blue for this flow
              ts->GetL4()->SetColor(Qt::blue);
            }
#endif
          ts->SetTrace(Trace::ENABLED);
          ts->Start(0.0);
        }
    }
  
  // For fun, change router nodes to squares
  b.Left()->Shape(Node::SQUARE);
  b.Right()->Shape(Node::SQUARE);
  
  // Also for fun, add two more nodes with no locations to test animaton
  Node* nl1 = new Node();
  nl1->SetIPAddr(IPAddr("192.168.0.1"));
  Node* nl2 = new Node();
  nl2->SetIPAddr(IPAddr("192.168.3.1"));
  //nl1->SetLocation(-2, 5);
  //nl2->SetLocation(12,5);
  
  nl1->AddDuplexLink(b.Left(nl / 2));
  nl2->AddDuplexLink(b.Right(nl / 2));
  TCPServer* tcpApp =
    (TCPServer*)nl2->AddApplication(TCPServer());
  tcpApp->BindAndListen(nl2, 80);
  tcpApp->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
  TCPSend* ts = (TCPSend*)nl1->AddApplication(
      TCPSend(nl2->GetIPAddr(),80,
              Constant(1000000)));
  ts->Start(0.0);
  
  // Specify animation
  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Progress(1);
  s.StopAt(100);
  s.Run();
}
