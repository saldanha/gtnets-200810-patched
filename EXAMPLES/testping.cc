// Test the PING application with an animation of dumbbell with stars.
// George F. Riley, Georgia Tech, Summer 2004

#include <iostream>

#include "debug.h"
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "dumbbell.h"
#include "star.h"
#include "application-ping.h"
#include "application-tcpsend.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "l2proto802.3.h"
#include "validation.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

#define N_LEAF 5

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  Count_t nl = N_LEAF; // Number leaf nodes for dumbbell
  Count_t ns = N_LEAF; // Number leaf nodes for each star
  
  if (argc > 1) nl = atol(argv[1]);
  if (argc > 2) ns = atol(argv[2]);

  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);

  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->Open("testping.txt");
  ICMP::Enable(); // Enable  ICMP processing for this simulation
  ICMP::Instance()->SetTrace(Trace::ENABLED);
  IPV4::Instance()->SetTrace(Trace::ENABLED);
  L2Proto802_3::GlobalSetTrace(Trace::ENABLED);

  // Set 2 pings per second
  PingApplication::DefaultPingRate(2);
  
  Linkp2p lk(Rate("1Mb"), Time("10ms"));
  Dumbbell b(nl, nl, 1.0,
             IPAddr("192.168.0.1"), IPAddr("192.169.0.1"), lk);
  // Specify the bounding box
  b.BoundingBox(Location(0,0), Location(10,10));
  
  Uniform startRng(0, 0.1); // Random number generator for start times
  // Create the Star networks and assign clients/servers
  Angle_t adderL = -M_PI / (b.LeftCount() + 1.0);
  Angle_t adderR = M_PI / (b.RightCount() + 1.0);
  Angle_t thetaL = -M_PI_2 + adderL;
  Angle_t thetaR = -M_PI_2 + adderR;

#ifdef HAVE_QT
  b.Left()->Color(Qt::blue);
  b.Right()->Color(Qt::blue);
#endif
  for (Count_t i = 0; i < b.LeftCount(); ++i)
    {
      Node* l = b.Left(i);
      Node* r = b.Right(i);
#ifdef HAVE_QT
      l->Color(Qt::blue);
      r->Color(Qt::blue);
#endif
      // Create a star networks with this each leaf as hub
      Star* sl = new Star(ns,l,lk, IPAddr("192.168.1.1")+IPAddr("0.0.1.0")*i);
      Star* sr = new Star(ns,r,lk, IPAddr("192.169.1.1")+IPAddr("0.0.1.0")*i);
      
      sl->BoundingBox(Location(0,0), Location(2,2), thetaL, M_PI / 2);
      sr->BoundingBox(Location(0,0), Location(2,2), thetaR, M_PI / 2);

      thetaL += adderL;
      thetaR += adderR;
      
      // Add a PING application at each right side leaf
      for (Count_t j = 0; j < sr->LeafCount(); ++j)
        {
          Node* rl = sr->GetLeaf(j); // Right leaf
          PingApplication* pa = (PingApplication*)rl->AddApplication(
              PingApplication(sl->GetLeaf(j)->GetIPAddr()));
          pa->Start(startRng.Value());
        }
    }

  // Specify animation
  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  
  s.Progress(1);
  s.StopAt(10);
  s.Run();
  tr->Close();
}
