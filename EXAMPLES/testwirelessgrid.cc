// Test program for Wireless Grid object
// George F. Riley.  Georgia Tech, Fall 2002

#include <iostream>
#include <stdlib.h>

#include "simulator.h"
#include "wireless-grid-rectangular.h"
#include "wireless-grid-polar.h"
#include "ipaddr.h"
#include "rng.h"
#include "node.h"
#include "mobility-random-waypoint.h"
#include "ratetimeparse.h"
#include "application-cbr.h"
#include "validation.h"  

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  Count_t  nNodes = 500;
  bool     useUni = false;
  
  if (argc > 1) nNodes = atol(argv[1]);
  if (argc > 2)  useUni = true;
  IPAddr_t firstIP = IPAddr("192.168.0.1");
  Location l(500,500);
  WirelessGridPolar g(l,
                      Constant(nNodes),
                      Exponential(250, 500),
                      firstIP,
                      Uniform(0.0, 360.0));
  g.AddMobility(RandomWaypoint(g, Uniform(0,0.2), Uniform(100,200)));
  Uniform startRNG(0, 0.1);
  Uniform peerRNG(0,  nNodes);
  
  for (Count_t i = 0; i < g.Size(); ++i)
    {
      Node* n = g.GetNode(i);
      n->SetRadioRange(l.X() / 2);
      CBRApplication* cbr;
      if (useUni)
        {
          cbr = (CBRApplication*)n->AddApplication(
              CBRApplication(firstIP + peerRNG.IntValue(), 1000, 1000));
        }
      else
        {
          cbr = (CBRApplication*)n->AddApplication(
              CBRApplication(IPAddr("255.255.255.255"), 1000, 1000));
        }
      cbr->Start(startRNG.Value());
    }
  
  if (!Validation::noAnimation)
    {
      s.AnimateWirelessTx(true);
      s.StartAnimation(0, true);
      s.PauseAnimation(100);
      s.AnimationUpdateInterval(Time("1ms")); // 1ms initial update rate
    }
  s.StopAt(1000);
  s.Run();
}
