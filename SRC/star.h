// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: star.h 92 2004-09-14 18:18:43Z dheeraj $



// Georgia Tech Network Simulator - Star topology
// George F. Riley.  Georgia Tech, Fall 2002

// Define a star topology using point-to-point links

#ifndef __star_h__
#define __star_h__

#include <math.h>

#include "common-defs.h"

class Node;
class Linkp2p;
class Queue;
class Location;

//Doc:ClassXRef
class Star {
  //Doc:Class Define a star topology using point--to--point links
public:
  //Doc:Method
  Star(Count_t, IPAddr_t = IPADDR_NONE, SystemId_t = 0);// Number of nodes
    //Doc:Desc Create a star topology with the specified number of leaf nodes.
    //Doc:Desc Uses the default point--to--point link object.
    //Doc:Arg1 Number of leaf nodes desired.
    //Doc:Arg2 Starting \IPA\ of leaf nodes.
    //Doc:Arg3 Responsible system ID for distributed simulation

  //Doc:Method
  Star(Count_t, const Linkp2p&, IPAddr_t = IPADDR_NONE, SystemId_t = 0);
    //Doc:Desc Create a star topology with the specified number of leaf nodes,
    //Doc:Desc and the specified point--to--point link object.
    //Doc:Arg1 Number of leaf nodes desired.
    //Doc:Arg2 Link object to use for leaf to core links.
    //Doc:Arg3 Starting \IPA\ of leaf nodes.
    //Doc:Arg4 Responsible system ID for distributed simulation

  //Doc:Method
  Star(Count_t, Node*, IPAddr_t = IPADDR_NONE, SystemId_t = 0);
    //Doc:Desc Create a star topology with the specified number of leaf nodes.
    //Doc:Desc and an existing node for the core.
    //Doc:Desc Uses the default point--to--point link object.
    //Doc:Arg1 Number of leaf nodes desired.
    //Doc:Arg2 Node pointer for core node.
    //Doc:Arg3 Starting \IPA\ of leaf nodes.
    //Doc:Arg4 Responsible system ID for distributed simulation

  //Doc:Method
  Star(Count_t, Node*, const Linkp2p&, IPAddr_t = IPADDR_NONE, SystemId_t = 0);
    //Doc:Desc Create a star topology with the specified number of leaf nodes.
    //Doc:Desc and an existing node for the core.
    //Doc:Desc Uses the specified point--to--point link object for leaf links.
    //Doc:Arg1 Number of leaf nodes desired.
    //Doc:Arg2 Node pointer for core node.
    //Doc:Arg3 Link object to use for leaf to core links.
    //Doc:Arg4 Starting \IPA\ of leaf nodes.
    //Doc:Arg5 Responsible system ID for distributed simulation

  //Doc:Method
  Node* GetHub();                     // Get the hub node
    //Doc:Desc Get a pointer to the hub node in the star.
    //Doc:Return Node pointer to hub node.

  //Doc:Method
  Node* GetLeaf(Count_t);             // Get specified leaf
    //Doc:Desc Get a pointer to a leaf node.
    //Doc:Arg1 Leaf node index to get.
    //Doc:Return Pointer to specified leaf node.

  //Doc:Method
  Linkp2p* HubLink(Count_t);          // Get link from hub to leaf router
    //Doc:Desc Get a pointer to a hub to leaf link.
    //Doc:Arg1 Leaf node index.
    //Doc:Return Pointer to point--to--point link from hub to specified leaf

  //Doc:Method
  Linkp2p* LeafLink(Count_t);         // Get link from leaf to hub
    //Doc:Desc Get a pointer to a leaf to hub link.
    //Doc:Arg1 Leaf node index.
    //Doc:Return Pointer to point--to--point link from specified leaf to hub.

  //Doc:Method
  Queue*   HubQueue(Count_t);         // Get queue from hub to leaf
    //Doc:Desc Get a pointer to a hub to leaf queue.
    //Doc:Arg1 Leaf node index.
    //Doc:Return Pointer to queue from hub to specified leaf

  //Doc:Method
  Queue*   LeafQueue(Count_t);        // Get queue from leaf to hub
    //Doc:Desc Get a pointer to a leaf to hub queue.
    //Doc:Arg1 Leaf node index.
    //Doc:Return Pointer to queue from specified leaf to hub

  //Doc:Method
  Count_t  LeafCount() { return leafCount;}
    //Doc:Desc Get a count  of the number of leaf nodes.
    //Doc:Return Leaf node count.

  //Doc:Method
  void BoundingBox(const Location&, const Location&,
      Angle_t = 0, Angle_t = 2 * M_PI);
    //Doc:Desc Define a bounding box for this star object. 
    //Doc:Desc The bounding box
    //Doc:Desc is used to assign node locations, which are used during
    //Doc:Desc the simulation animation.
    //Doc:Desc If the hub node has an existing location, then this bounding box
    //Doc:Desc is relative to that node.  Otherwise, the hub is placed in the
    //Doc:Desc center of the box, with all leaf nodes extending radially from 
    //Doc:Desc the hub with length of half the bounding box size
    //Doc:Desc (minus a small fudge factor).
    //Doc:Arg1 Lower left corner of bounding box.
    //Doc:Arg2 Upper right corner of bounding box.
    //Doc:Arg3 The angle that the radial links are centered around.
    //Doc:Arg4 The total size of the arc for the radial links

public:
  NodeId_t   first;      // First leaf node id
  NodeId_t   last;       // Just log first/last+1 entries
  Node*      hub;        // Points to hub node
  Count_t    leafCount;  // Number of leaf nodes
private:
  void ConstructorHelper(Count_t, Node*, 
                         IPAddr_t, const Linkp2p&, SystemId_t);
};

#endif


