// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


// Georgia Tech Network Simulator - Bluetooth LMP class
// George F. Riley, Georgia Tech, Spring 2004

#ifndef __lmp_h
#define __lmp_h

#include <assert.h>
#include "timer.h"
#include "bluetus.h"
#include "baseband.h"

/////////////////////////////////////////////////////////////////
//
//LMP v1.1
//
/////////////////////////////////////////////////////////////////

#define OPCODE_ACCEPTED		3
#define OPCODE_NOT_ACCEPTED	4
#define OPCODE_CLKOFFSET_REQ 5
#define OPCODE_CLKOFFSET_RSP 6
#define OPCODE_DETACH		 7
#define OPCODE_AU_RAND		11
#define OPCODE_SRESPONSE	12 //authentication response

#define OPCODE_QOS			41
#define OPCODE_QOS_REQ		42

#define OPCODE_MAX_SLOT		45
#define OPCODE_MAX_SLOTREQ	46
#define OPCODE_HOST_CONNECTIONREQ	51
#define OPCODE_SETUP_COMPLETE	49
#define OPCODE_SUPERVISION_TIMEOUT	55

//Sniff
#define OPCODE_SNIFF_REQ	23
#define OPCODE_UNSNIFF_REQ	24
//Hold
#define OPCODE_HOLD_REQ	21
#define OPCODE_HOLD	20
//Park
#define OPCODE_PARK_REQ	25
//define the error reasons
#define AUTHENTICATION_FAILURE	0x05
#define	KEY_MISSING		0x06
#define UNSUPPORTED_PARAMETER_VALUE	0x20

class LMPPacket {
	public:
		LMPPacket( bool bTransactionId, uChar ucOpCode);
		~LMPPacket();
		
	public:
		uChar GetHdrLength() {
			return sizeof(ucOpCodeTransId);
		}
		uChar GetOpCode() {
			return ucOpCodeTransId>>1;
		}
		
	public:
		uChar ucOpCodeTransId; //0--7  TransactionId  OpCode 
		uChar Content[1]; //actually start of data here
};

//sniff struct
struct SniffReq {
	uChar ucFlag;
	uShort usDsniff;
	uShort usTsniff;
	uShort usSniffAttemp;
	uShort usSniffTo;
};

//hold struct
struct HoldReq {
	uShort	usSlotStart;
	uShort	usSlotCount;
};

//park struct
struct ParkReq {
	uChar	ucFlag;
	uShort	usDb;
	uShort	usTb;
	uChar	ucNb;
	uChar	ucDeltaB;
	uChar	ucPMAddr;
	uChar	ucARAddr;
	uChar	ucNbsleep;
	uChar	ucDbsleep;
	uChar	ucDaccess;
	uChar	ucTaccess;
	uChar	ucNaccslots;
	uChar	ucNpoll;
	uChar	ucMaccess;
};

//Timer events
class LMPEvent : public TimerEvent {
	public:
		// the types of the LMP timer events
		typedef enum { HELLO_TIMEOUT, SEND_SNIFF, SNIFF_TIMEOUT, SNIFFATTEMP_TIMEOUT,
				SEND_HOLD, HOLD_TIMEOUT, WAKEUP_TIMEOUT, SEND_PARK, PARK_TIMEOUT, 
				PARK_BEACON_TIMEOUT, PARK_ACCESS_TIMEOUT
				}LMPTimeout_t;
	public:
		LMPEvent(Event_t ev) : TimerEvent(ev) { }
		virtual ~LMPEvent() {}
};


class LMP : public TimerHandler {
	public:
		//constructor
		LMP(BaseBand *pBand=NULL, uChar ucMaxSlot=1, uChar ucMaxSlotAvailable = 1, 
						uShort usTpoll=6){ //default value.
			this->ucMaxSlot = ucMaxSlot;
			this->ucMaxSlotAvailable = ucMaxSlotAvailable;
			this->usTpoll = usTpoll;

			pBaseBand = pBand;
			assert(pBaseBand!=NULL);
			if(pBaseBand->GetRole() == MASTER)
				bTransactionId = 0;
			else
				bTransactionId = 1;

			//create a link key
			pLinkKey = new uChar[16];
			//for sniff
			bSniffEnabled = false;
			isSniff = true;
			//for hold
			isHold = false;
			ulHoldSlot = 0;
			//for park
			bParkEnabled = false;
			isBeacon = false;
			isAccess = false;
			
  			HelloTimeout = NULL; // TODO:timer timeout
 	 		SniffTimeout = NULL;
  			SniffAttempTimeout = NULL;
  			SendSniffTimeout = NULL;
  			SendHoldTimeout = NULL;
  			HoldTimeout = NULL;
  			WakeupTimeout = NULL;
  			SendParkTimeout = NULL;
 	 		ParkTimeout = NULL;
  			ParkBeaconTimeout = NULL;
  			ParkAccessTimeout = NULL;
		}
		//destructor
		virtual ~LMP() {
			delete []pLinkKey;
		}
		
	public:
		/*
		void AttachL2cap(L2cap *pL2cap) {
			if((pL2CAP==NULL)&&(pL2cap!=NULL))
				pL2CAP = pL2cap;
			else
				cout<<"L2cap already attached or parameter invalid"<<endl;
			 //called by baseband when installing L2CAP.
		}
		*/
		void SendAccepted(bool bTranID, uChar ucOpCode);
		void RecvAccepted(LMPPacket *pPacket);
		void SendNotAccepted(bool bTranID, uChar ucOpCode, 
						uChar ucReason);
		void RecvNotAccepted(LMPPacket *pPacket);
		void SendLMPAuRand(uChar ucLen, bool bTranId, uChar *pRandom);
		void RecvAuRand(LMPPacket *pPacket, uChar ucAllLen);
		void RecvRsp(LMPPacket *pPacket, uChar ucAllLen);
		void CalRsp(uChar ucLen, uChar *pRandom, BdAddr LocalAddr, 
						uChar *pLinkKey, uLong& ulRsp);
		void ClockOffsetReq();
		void RecvClockOffsetReq(LMPPacket *pPacket);
		void RecvClockOffsetRsp(LMPPacket *pPacket);
		void SendDetach(bool bTranId, uChar ucReason);
		void RecvDetach(LMPPacket *pPacket);
		void QoSHelper(bool bTranId, uShort usTpoll, uChar ucNbc, 
						uChar opcode);
		void SendQoS();
		void RecvQoS(LMPPacket *pPacket, uChar ucAllLen);
		void SendQoSReq(uShort usLocalTpoll, uChar ucLocalNbc);
		void RecvQoSReq(LMPPacket *pPacket, uChar ucAllLen);
		void MaxSlot();
		void RecvMaxSlot(LMPPacket *pPacket, uChar ucAllLen);
		void SendMaxSlotReq(uChar MaxSlot);
		void RecvMaxSlotReq(LMPPacket *pPacket, uChar ucAllLen);
		void SendSupervisionTO(uShort usTimeout);
		void RecvSupervisionTO(LMPPacket *pPacket);
		void HostConnectionReq();
		void RecvHostConnectionReq(LMPPacket *pPacket);
		void RecvSetupComplete(LMPPacket *pPacket);
		void DataRequest(uChar ucOpCode, bool bTranId, uChar ucLen,
						uChar *pContent);
		void DataIndication(uChar ucFlow, uShort usLen, uChar *pData);
		//void DataIndication(LMPPacket *pPacket);
		
		//Sniff methods
		void SetSniff(Time_t, uShort, uShort, uShort);
		void InitSniffPara(uShort);
		void SendSniffReq(uShort usTsniff, uShort usSniffAttemp, 
					uShort usSniffTo);
		void RecvSniffReq(LMPPacket *pPacket);
		void SendUnsniffReq();
		void RecvUnsniffReq(LMPPacket *pPacket);
		//Hold methods
		void SetHold(Time_t, uShort, uShort, const Random&);
		void SendHoldReq(uShort, uShort);
		void RecvHoldReq(LMPPacket *pPacket);
		//Park methods
		void SetPark(Time_t tSend, uShort usTb, uChar ucNb, uChar ucDeltaB, 
				uChar ucNbsleep, uChar ucDaccess, uChar ucTaccess, 
				uChar ucNaccslots, uChar ucNpoll, uChar ucMaccess);
		void InitParkPara(uShort, uChar);
		void AllocPmArAddr();
		void SendParkReq(uShort usTb, uChar ucNb, uChar ucDeltaB, 
				uChar ucNbsleep, uChar ucDaccess, uChar ucTaccess, 
				uChar ucNaccslots, uChar ucNpoll, uChar ucMaccess);
		void RecvParkReq(LMPPacket *pPacket);

	public:
  		// Timers
  		Timer  timer;           // Timer for various LMP timer events
  		LMPEvent*  HelloTimeout;   // TODO:timer timeout
  		LMPEvent*  SniffTimeout;
  		LMPEvent*  SniffAttempTimeout;
  		LMPEvent*  SendSniffTimeout;
  		LMPEvent*  SendHoldTimeout;
  		LMPEvent*  HoldTimeout;
  		LMPEvent*  WakeupTimeout;
  		LMPEvent*  SendParkTimeout;
  		LMPEvent*  ParkTimeout;
  		LMPEvent*  ParkBeaconTimeout;
  		LMPEvent*  ParkAccessTimeout;

		uLong ulSniffClk;

	  // Timer management
		virtual void Timeout(TimerEvent *);
  		void ScheduleTimer(Event_t, LMPEvent*&, Time_t);
  		void CancelTimer(LMPEvent*&, bool delTimer = false); 
  		void CancelAllTimers();

		uShort GetSniffTo();
		uShort GetSniffAttemp();
		bool SniffEnabled() {return bSniffEnabled;}
		bool IsSniff() {return isSniff;}
		bool IsSleep() {return isSleep;}
		bool IsHold() {return isHold;}
		bool isBeacon;	//for access in BaseBand
		//bool IsBeacon() {return isBeacon;}
		bool IsAccess() {return isAccess;}

		uShort GetTpoll() {return usTpoll;}
		
	public:
		 
		BaseBand *pBaseBand;

		BdAddr GetPeerAddr() {
			return RemoteAddr;
		}
		
		void SetPeerAddr(BdAddr addr) {
			RemoteAddr = addr; 
		}
		uChar  GetMaxSlot(){
			return ucMaxSlot;
		}
	private:
		L2cap *pL2CAP; //point to L2cap protocol object 

		BdAddr	RemoteAddr; //currently we are working on.
		bool bTransactionId; //transcation id for initializtion.
		
		uChar *pLinkKey; //128 bits
		uShort usClockOffset; //??belong here?? depending on 
							  //the role of the device....
		uShort usTpoll; //poll interval
		uChar ucNbc;	//the number of repetitions for broadcast packets.

		uShort usTpollReq; //parameter requested.
		uChar ucNbcReq;    //parameter requested.

		uChar ucMaxSlotAvailable; //for remote device
		uChar ucMaxSlot; //I request and set when I got from remote.
		
		uChar ucMaxSlotReq; //parameter requested.

		uShort usSupervisionTimeout;

		//sniff parameters
		uChar ucFlag;	//ucFlag and usDsniff are shared with park
		uShort usDsniff;
		uShort usTsniff;
		uShort usSniffAttemp;
		uShort usSniffTo;
		bool bSniffEnabled;
		bool isSniff;
		bool isSleep;
		//hold parameters
		uShort usSlotStart;
		uShort usSlotCount;
		bool isHold;
		Random* onTime;
		Time_t onInterval;
		uLong ulHoldSlot;	//stat for number of hold slots.
		//park parameters
		bool bParkEnabled;
		bool isAccess;
		uChar	ucParkCount;
		uShort	usTb;
		uChar	ucNb;
		uChar	ucDeltaB;
		uChar	ucPMAddr;
		uChar	ucARAddr;
		uChar	ucNbsleep;
		uChar	ucDbsleep;
		uChar	ucDaccess;
		uChar	ucTaccess;
		uChar	ucNaccslots;
		uChar	ucNpoll;
		uChar	ucMaccess;
		char	cPMAddrList[256];
		char	cARAddrList[256];
};

#endif
