//
// Sample test program to instantiate a BGP router
// Sunitha Beeram, Georgia Tech, Spring 2005
//

#include <iostream>
#include <stdio.h>
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "bgp.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "interface.h"
#include "routing.h"

using namespace std;

int main(int argc,char ** argv)
{
    Simulator Sim;
    char command[]="show ip bgp";
    char command1[]="no network 192.175.48.1 mask 255.255.255.255";
    Node  *node[9];
    BGP *bgpRouter[9];
    int StopTime = 400;	
    Linkp2p lk(Rate("500Kb"), Time("20ms"));
    Node::DefaultShape(Node::CIRCLE);
    
    Trace* tr= Trace::Instance();
    tr->Open("anycast.txt");
    IPV4::Instance()->SetTrace(Trace::ENABLED);
    
    Uniform startRng(0,0.1);
    
    for (int i=0; i < 9; ++i)
    {
        node[i]= new Node();
        bgpRouter[i]= new BGP(0);
        bgpRouter[i]->AttachNode(node[i]);
    }
    
    
    /*AS1*/
    
    bgpRouter[0]->config_file("./AS1/bgpd1.conf");
    bgpRouter[1]->config_file("./AS1/bgpd2.conf");
    bgpRouter[2]->config_file("./AS1/bgpd3.conf");
    
    node[2]->AddDuplexLink(node[0],lk,IPAddr("192.38.0.2"),Mask(30),
                           IPAddr("192.38.0.1"),Mask(30));
    
    
    node[2]->AddDuplexLink(node[1],lk,IPAddr("192.38.0.5"),Mask(30),
                           IPAddr("192.38.0.6"),Mask(30));
    
    node[1]->AddDuplexLink(node[0],lk,IPAddr("192.38.0.9"),Mask(30),
                           IPAddr("192.38.0.10"),Mask(30));
    
    /*AS2*/
    
    bgpRouter[3]->config_file("./AS2/bgpd1.conf");
    bgpRouter[4]->config_file("./AS2/bgpd2.conf");
    bgpRouter[5]->config_file("./AS2/bgpd3.conf");
    
    node[3]->AddDuplexLink(node[4],lk,IPAddr("192.38.1.1"),Mask(30),
                           IPAddr("192.38.1.2"),Mask(30));
    
    node[3]->AddDuplexLink(node[2],lk,IPAddr("192.38.1.5"),Mask(30),
                           IPAddr("192.38.1.6")),Mask(30);
    
    
    node[5]->AddDuplexLink(node[4],lk,IPAddr("192.38.1.9"),Mask(30),
                           IPAddr("192.38.1.10"),Mask(30));
    
    node[5]->AddDuplexLink(node[3],lk,IPAddr("192.38.1.11"),Mask(30),
                           IPAddr("192.38.1.12"),Mask(30));
    
    
    /*AS3*/
    
    bgpRouter[6]->config_file("./AS3/bgpd1.conf");
    bgpRouter[7]->config_file("./AS3/bgpd2.conf");
    bgpRouter[8]->config_file("./AS3/bgpd3.conf");
    
    
    node[6]->AddDuplexLink(node[5],lk,IPAddr("192.38.2.1"),Mask(30),
                           IPAddr("192.38.2.2"),Mask(30));
    
    node[6]->AddDuplexLink(node[7],lk,IPAddr("192.38.2.5"),Mask(30),
                           IPAddr("192.38.2.6"),Mask(30));
    
    
    node[8]->AddDuplexLink(node[6],lk,IPAddr("192.38.2.9"),Mask(30),
                           IPAddr("192.38.2.10"),Mask(30));
    
    node[8]->AddDuplexLink(node[7],lk,IPAddr("192.38.2.11"),Mask(30),
                           IPAddr("192.38.2.12"),Mask(30));
    
    /*schedule an event to check the bgp routing tables*/
    
    for (int i =0; i < 9 ; ++i)
    {
        
        bgpRouter[i]->StartAt(0.1);
        bgpRouter[i]->StopAt(StopTime);

        BGPEvent *ev = new BGPEvent(BGPEvent::COMMAND);
        ev->argv = (char*)malloc(sizeof(char)*strlen(command)+1);
        strcpy(ev->argv,command);
        bgpRouter[i]->timer->Schedule(StopTime/2-1,ev);
        
        ev = new BGPEvent(BGPEvent::COMMAND);
        ev->argv = (char*)malloc(sizeof(char)*strlen(command)+1);
        strcpy(ev->argv,command);
        bgpRouter[i]->timer->Schedule(StopTime-1,ev);
    }
    
    BGPEvent *ev = new BGPEvent(BGPEvent::COMMAND);
    ev->argv = (char*)malloc(sizeof(char)*strlen(command1)+1);
    strcpy(ev->argv,command1);
    bgpRouter[4]->timer->Schedule(StopTime/2,ev);
    
    
    Sim.StopAt(StopTime);
    cout << "Starting Simulation" << endl;
    Sim.Run();
    cout << "Simulation complete" << endl;
    
}

