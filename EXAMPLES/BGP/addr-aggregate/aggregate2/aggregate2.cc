// Sample test program to instantiate a BGP router
// Sunitha Beeram, Georgia Tech, Spring 2005

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "bgp.h"


using namespace std;

int main(int argc,char ** argv)
{
  Simulator Sim;

  int StopTime=100;


  /* If stop time is specified as cli arg*/

  if ( argc > 1)
  {
	StopTime= atoi(argv[1]);
	
	if (StopTime < 100)
		StopTime =100;
  }


  Node::DefaultShape(Node::CIRCLE);
  //Linkp2p lk(Rate("1.5Mb"), Time("1ms"));
  Linkp2p lk(Rate("500Kb"), Time("20ms"));
  Uniform startRng(0,0.1);

  /*First BGP Router*/
  Node* node1 = new Node();
  BGP* bgpRouter1 = new BGP(0);

  bgpRouter1->AttachNode(node1);
  bgpRouter1->config_file("./bgpd1.conf");

  /*Second BGP Router*/
  Node *node2 = new Node();
  BGP* bgpRouter2 = new BGP(0);
  
  bgpRouter2->AttachNode(node2);
  bgpRouter2->config_file("./bgpd2.conf");
  node1->AddDuplexLink(node2,lk);

  /*Third BGP Router*/
  
  Node *node3 = new Node();
  BGP* bgpRouter3 = new BGP(0);
  
  bgpRouter3->AttachNode(node3);
  bgpRouter3->config_file("./bgpd3.conf");
  node2->AddDuplexLink(node3,lk);


  /*Fourth BGP Router*/ 
  Node *node4 = new Node();
  BGP* bgpRouter4 = new BGP(0);
  
  bgpRouter4->AttachNode(node4);
  bgpRouter4->config_file("./bgpd4.conf");
  node2->AddDuplexLink(node4,lk);

  char command[] ="show ip bgp";

  BGPEvent *ev1 = new BGPEvent(BGPEvent::COMMAND);
  ev1->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev1->argv,command);
  bgpRouter1->timer->Schedule(StopTime-1,ev1);

  BGPEvent *ev2 = new BGPEvent(BGPEvent::COMMAND);
  ev2->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev2->argv,command);
  bgpRouter2->timer->Schedule(StopTime-1,ev2);

  BGPEvent *ev3 = new BGPEvent(BGPEvent::COMMAND);
  ev3->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev3->argv,command);
  bgpRouter3->timer->Schedule(StopTime-1,ev3);

  BGPEvent *ev4 = new BGPEvent(BGPEvent::COMMAND);
  ev4->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev4->argv,command);
  bgpRouter4->timer->Schedule(StopTime-1,ev4);

  bgpRouter1->StartAt(0.1);
  bgpRouter2->StartAt(0.1);
  bgpRouter3->StartAt(0.1);
  bgpRouter4->StartAt(0.1);
  bgpRouter1->StopAt(StopTime);
  bgpRouter2->StopAt(StopTime);
  bgpRouter3->StopAt(StopTime);
  bgpRouter4->StopAt(StopTime);

  Sim.StopAt(StopTime);
  Sim.Run();
}
 
