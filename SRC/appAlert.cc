// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: appAlert.cc 92 2004-09-14 18:18:43Z dheeraj $



//*********************************************************************
//
// AUTHOR:    George Papageorgiou <gpapag@eng.umd.edu>
//
// FILE:      AppAlert.cc
//
// DATE:      October 27, 2003
//
// PURPOSE:   A new application that implements the alert protocol
//           for the dynamic firewall.
//
//*********************************************************************


// Modified for GTNetS by George Riley, Georgia Tech
// Spring 2004

#include <iostream>
#ifdef HAVE_QT
#include <qnamespace.h>
#endif


#include "appAlert.h"
#include "droptail-filter.h"

using namespace std;

// Constructor

AppAlert::AppAlert() : l4Proto(nil), localNode(nil), running(1),
                       alertCode(ALERT_OK), server(false), reg(true),
                       serverIP(IPADDR_NONE), dtfQueue(nil)
{
}

AppAlert::AppAlert(const AppAlert& c)
    : l4Proto(nil), localNode(nil),
      running(c.running), alertCode(c.alertCode),
      attackerId(c.attackerId), server(c.server), reg(c.reg),
      serverIP(IPADDR_NONE), dtfQueue(nil)
{
}


void AppAlert::Timeout(TimerEvent* ev)
{
  //SendAlertPacket(IPADDR_NONE, NO_PORT);
  // delete ev??
}


#ifdef FIX_THIS
// OTcl command interpreter

int AppAlert::command(int argc, const char*const* argv)  {
  Tcl& tcl = Tcl::instance();

  if (argc == 3)  {
       if (strcmp(argv[1], "send-alert") == 0)  {
               alertCode = atoi((char *) argv[2]);
               SendAlertPacket(IPADDR_NONE, NO_PORT);
               return(TCL_OK);
       }  else if  (strcmp(argv[1], "set-server") == 0)  {
               server = atoi((char *) argv[2]);
               return(TCL_OK);
       }  else if  (strcmp(argv[1], "attach-agent") == 0) {
               agent_ = (Agent*) TclObject::lookup(argv[2]);
               udp_Alert_Agent_ = (UdpAlertAgent *) TclObject::lookup(argv[2]);
               if (agent_ == 0) {
                       tcl.resultf("no such agent %s", argv[2]);
                       return(TCL_ERROR);
               }

               // Make sure the underlying agent supports alerts
               if(udp_Alert_Agent_->supportalertApp()) {
                       udp_Alert_Agent_->enablealertApp();
               } else {
                       tcl.resultf("agent \"%s\" does not support Alert Application", argv[2]);
                       return(TCL_ERROR);
               }

               agent_->attachApp(this);
               return(TCL_OK);

       }
  }  else if (argc == 4)  {
       if (strcmp((char *) argv[1], (char *) "send-alert") == 0)  {
               alertCode   =  atoi((char *) argv[2]);
               attackerId  =  atoi((char *) argv[3]);
               SendAlertPacket(IPADDR_NONE, NO_PORT);
               return(TCL_OK);
       }
  }
  return (Application::command(argc, argv));
}
#endif


// Executed when the application is started

void AppAlert::StartApp()
{
  Init();
}


// Executed when the application is terminated

void AppAlert::StopApp()
{
  running  = 0;

}



// Initialize the application

void AppAlert::Init()
{

  running    = 1;                            // redundant, see constructor
  alertCode  = ALERT_OK;                     // redundant, see constructor

  // Create the UDP proto if doesn't exist
  if (!l4Proto)
    {
      l4Proto = new UDP();
      l4Proto->PacketSize(1024);
      l4Proto->Attach(localNode);
      if (reg)
        l4Proto->Bind(AlertPort);     // Bind to well-known port
      else
        l4Proto->Bind(NO_PORT);       // Random port

      l4Proto->AttachApplication(this);
#ifdef HAVE_QT
      l4Proto->SetColor(Qt::green); // Use green for alert packets
#endif
    }

  // if you are a client, register yourself to the server
  if (!server && reg)
    {
      SendAlertPacket(ALERT_OK, IPADDR_NONE, IPADDR_NONE, NO_PORT);
    }

}



// Send application data packet

void AppAlert::SendAlertPacket(IPAddr_t destid, PortId_t destport)
{
  if (running && l4Proto)
    {
      // Create a new AlertPDU and send to specified
      AlertHeader* hdr = new AlertHeader();
      hdr->srcIP = IPADDR_NONE;
      if (localNode) hdr->srcIP = localNode->GetIPAddr();
      hdr->srcPort = l4Proto->localPort;
      hdr->alertCode = alertCode;
      hdr->attackerId = attackerId;
      hdr->nbytes    = 900;    // Size of alert packet (NOT UDP packet size)
      hdr->time      = Simulator::Now();      // Current time

      DEBUG0((cout << "Inside send: at " << (string)IPAddr(hdr->srcIP)
              << " alertcode " << alertCode
              << " attackerid " << attackerId
              << " serverIP " << (string)IPAddr(serverIP)
              << endl));

      if (destid == IPADDR_NONE)
        { // Send to connected peer
          l4Proto->Send(*hdr);
        }
      else
        { // Send to specified peer
          l4Proto->SendTo(*hdr, destid, destport);
        }
    }
}

void AppAlert::SendAlertPacket(int code, IPAddr_t id,
                               IPAddr_t destid, PortId_t destport)
{
  alertCode = code;
  attackerId = id;
  SendAlertPacket(destid, destport);
}


void AppAlert::Connect(IPAddr_t peerIP, PortId_t peerPort)
{
  if (!l4Proto)
    {
      l4Proto = new UDP();
      l4Proto->PacketSize(1024);
      l4Proto->Attach(localNode);
      if (reg)
        l4Proto->Bind(AlertPort);     // Bind to well-known port
      else
        l4Proto->Bind(NO_PORT);       // Random port
      l4Proto->AttachApplication(this);
#ifdef HAVE_QT
      l4Proto->SetColor(Qt::green); // Use green for alert packets
#endif
    }
  serverIP = peerIP;
  l4Proto->Connect(peerIP, peerPort);
}

void AppAlert::AttachNode(Node* n)
{
  localNode = n;
}

Application* AppAlert::Copy() const
{
  return new AppAlert(*this);
}

// Receive message from underlying agent

void AppAlert::Receive(Packet* p, L4Protocol* l4, Seq_t seq)
{
  PDU* pdu = p->PopPDU(); // Get the data pdu
  AlertHeader* alert = nil;

  if (pdu->Proto() == 1001)
    {  // Found an alert PDU
      alert = (AlertHeader*)pdu;
    }

  if (alert)
    {
      alertCode        =  alert->alertCode;
      attackerId       =  alert->attackerId;

      DEBUG0((cout << "Inside receive at "
              << (string)IPAddr(localNode->GetIPAddr())
              << " alertcode " << alertCode
              << " attackerId " << attackerId
              << endl));

       // Finite State Machine for the server

       if (server)
          {
            ServerRxAlert(alert);
          }

        else
          {
            ClientRxAlert(alert);
          }
       // Finite State Machine for the client


#ifdef CODE_LATER
       if (!server)  {
          // client is notified that sth wrong is happening
          // start more defense mechanisms

          if (alertCode == ALERT_OK)  {
               int   thisId;                   // id of this instnace
               int   range;                    // no of hosts
               char  tmpStr[200];              // dummy string variable

               Tcl& tcl = Tcl::instance();

               sprintf(tmpStr, "set Sizeofsubnets");
               tcl.eval(tmpStr);
               range = (int) atoi((char *) tcl.result());

               thisId = agent_->addr();

               for (int i = 1; i < range; i++)  {
                       sprintf(tmpStr, "[$ns link $n(%d) $n(%d)] queue", thisId, thisId + i);
                       tcl.eval(tmpStr);
                       sprintf(tmpStr, "%s set DDOS_on_ 1", (char *) tcl.result());
                       tcl.eval(tmpStr);

                       sprintf(tmpStr, "[$ns link $n(%d) $n(%d)] queue", thisId + i, thisId);
                       tcl.eval(tmpStr);
                       sprintf(tmpStr, "%s set DDOS_on_ 1", (char *) tcl.result());
                       tcl.eval(tmpStr);
               }

          }  else if (alertCode == ALERT_IPSPF)  {
          }  else if (alertCode == ALERT_TCPFL)  {
          }  else if (alertCode == ALERT_UDPFL)  {

               int   thisId;                   // id of this instnace
               int   range;                    // no of hosts
               int   corR;                     // id of the corresponding rtr
               char  tmpStr[200];              // dummy string variable

               Tcl& tcl = Tcl::instance();

               thisId = agent_->addr();

               sprintf(tmpStr, "set Sizeofsubnets");
               tcl.eval(tmpStr);
               range = (int) atoi((char *) tcl.result());


               if ((thisId < attackerId) && (attackerId < thisId + range))  {

                       sprintf(tmpStr, "set corR_(%d)", thisId);
                       tcl.eval(tmpStr);
                       corR = (int) atoi((char *) tcl.result());
                       sprintf(tmpStr, "[$ns link $n(%d) $n(%d)] queue", thisId, corR);
                       tcl.eval(tmpStr);
                       sprintf(tmpStr, "%s set-attack-id %d", (char *) tcl.result(), attackerId);
                       tcl.eval(tmpStr);

                       sprintf(tmpStr, "$ns at %f  \"$ns trace-annotate \\\"At time=%f: Filtering UDP-flooding attack at router %d\\\"\"", Scheduler::instance().clock() + 0.000001, Scheduler::instance().clock(), thisId);
                       tcl.eval(tmpStr);
               }
          }
       }
#endif
    }
}

void AppAlert::ServerRxAlert(AlertHeader* alert)
{
  IPAddr_t attackIP = IPADDR_NONE;

  switch(alert->alertCode) {
    case ALERT_OK:
      // if this is the server and alertCode is ALERT_OK, then
      // someone is trying to register with me
      // Add to list of targets
      targetList.push_back(TargetAddrPort(alert->srcIP, alert->srcPort));
      break;
    case ALERT_IPSPF:
      DEBUG0((cout << "appAlert got IPSpoofing alert" << endl));
      attackIP = alert->attackerId;
      break;
    case ALERT_TCPFL:
      DEBUG0((cout << "appAlert got TCP Flood alert..not implemented yet"
              << endl));
      attackIP = alert->attackerId;
      break;
    case ALERT_UDPFL:
      DEBUG0((cout << "appAlert got UDP Flood alert" << endl));
      attackIP = alert->attackerId;
      break;
  }
  // Send a response packet to all clients
  for (TargetVec_t::size_type i = 0; i < targetList.size(); ++i)
    {
      TargetAddrPort& tap = targetList[i];
      SendAlertPacket(alert->alertCode, attackIP,  tap.ip, tap.port);
    }
}


void AppAlert::ClientRxAlert(AlertHeader* alert)
{
  switch (alert->alertCode){
  case ALERT_OK:
    break;
  case ALERT_IPSPF:
    DEBUG0((cout << "AppAlert::ClientRxAlert IPSpoof" << endl));
    break;
  case ALERT_TCPFL:
    break;
  case ALERT_UDPFL:
    DEBUG0((cout << "AppAlert::ClientRxAlert UDPFlood dtfQ " << dtfQueue
            << endl));
    if (dtfQueue) dtfQueue->filterIP = alert->attackerId;
    break;
  }
}



