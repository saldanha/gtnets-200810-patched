# $Id: rticheck.mk 148 2004-10-15 14:35:12Z dheeraj $


ifndef _RTI_CHECK_
_RTI_CHECK_	:= DONE

ifdef RTIDIR
       CFLAGS    +=   -DHAVE_RTI
       RTI_INCL  +=   -I$(RTIDIR) -I$(RTIDIR)/fdkcompat
       RTI_LIBS  +=   -L$(RTIDIR)/fdkcompat -lbrti  -L$(RTIDIR) -lsynk
else
ifeq ($(findstring libsynk, $(wildcard $(HOME)/*)), libsynk)
ifeq ($(findstring fm.h, $(wildcard $(HOME)/libsynk/*)), fm.h)
       RTIDIR     =   $(HOME)/libsynk
       CFLAGS    +=   -DHAVE_RTI
       RTI_INCL  +=   -I$(RTIDIR) -I$(RTIDIR)/fdkcompat
       RTI_LIBS  +=   -L$(RTIDIR)/fdkcompat -lbrti  -L$(RTIDIR) -lsynk
endif
endif
endif

# this target is for debugging
#checkrti:
#	echo $(CFLAGS) ; \
#	echo $(RTI_INCL) ; \
#	echo $(RTI_LIBS) ;

endif
