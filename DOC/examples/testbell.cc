// Test the dumbbell topology stock object.
// George F. Riley.  Georgia Tech, Fall 2002

#include "simulator.h"
#include "dumbbell.h"
#include "linkp2p.h"
#include "ratetimeparse.h"
#include "tcp-tahoe.h"
#include "application-tcpsend.h"
#include "application-tcpserver.h"
#include "globalstats.h"
#include "droptail.h"
#include "ipv4.h"

using namespace std;


int main()
{
  Simulator s;

  Queue::Default(DropTail()); // Set default to non-detailed drop tail
  Trace* gs = Trace::Instance();
  gs->IPDotted(true);
  gs->Open("testbell.txt");
  TCP::LogFlagsText(true);    // Log flags in text mode
  // Enable all l3 tracing
  IPV4::Instance()->SetTrace(Trace::ENABLED);

  Linkp2p l;
  l.Bandwidth(Rate("100Mb"));
  Dumbbell b(10, 10, 0.1, IPAddr("192.168.1.0"), IPAddr("192.168.2.0"), l);
  for (Count_t i = 0; i < b.LeftCount(); ++i)
    {
      cout << "Left  node " << i 
           << " ipaddr " << (string)IPAddr(b.Left(i)->GetIPAddr()) << endl;
    }
  for (Count_t i = 0; i < b.RightCount(); ++i)
    {
      cout << "Right node " << i 
           << " ipaddr " << (string)IPAddr(b.Right(i)->GetIPAddr()) << endl;
      // Add a tcp server on right side
      Node* n = b.Right(i);
      TCPServer* server = (TCPServer*)n->AddApplication(TCPServer());
      server->BindAndListen(n, 40);
    }
  TCPSend* app = (TCPSend*)b.Left(0)->AddApplication(
           TCPSend(TCPTahoe(), b.Right(0)->GetIPAddr(), 40, Constant(100000)));
  app->l4Proto->SetTrace(Trace::ENABLED);
  app->Start(0.1);
  s.Run();
  Stats::Print(); // Print the statistics
  cout << "Memory usage after run " 
       << s.ReportMemoryUsageMB() << "MB" << endl;
  cout << "Total events processed " << s.TotalEventsProcessed() << endl;
  cout << "Setup time " << s.SetupTime() << endl;
  cout << "Route time " << s.RouteTime() << endl;
  cout << "Run time "   << s.RunTime() << endl;
  gs->Close();
}
