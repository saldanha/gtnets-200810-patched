// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: droptail-filter.h 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - DropTail Queue class with spoof filtering
// George F. Riley.  Georgia Tech, Spring 2004

#ifndef __droptailfilter_h__
#define __droptailfilter_h__

#include <vector>
#include <map>

#include "droptail.h"
#include "udp.h"
#include "appAlert.h"
#include "timer.h"
#include "node.h"

class QColor;

// From UMd Code
#define BOUNDC          4    // max number of links per router
#define BOUNDR          30   // Number of refresh rates for computing historic 
		             // estimate...  BOUNDR<LOADSIZE
#define LOADSIZE        100
#define FALSEALARMDELAY 20
// End UMd

// Added by GFR to support UMd Code
typedef std::vector<int>        LoadVec_t;
typedef std::map<IPAddr_t, int> ThroughputMap_t;

class DropTailFilter : public DropTail, public TimerHandler {
public:
  DropTailFilter();
  DropTailFilter(Count_t); // Constructor with size
  DropTailFilter(const DropTailFilter&); // Copy constructor
  virtual ~DropTailFilter();
  Queue*  Copy() const;       // Make a copy
  bool Enque(Packet*); // Return true if enqued, false if dropped
  bool CheckSpoofedSource(Packet*); // Check for spoofed source address
  void SetRateAlgorithm(bool);

  // From TimerHandler
  void Timeout(TimerEvent*);

  // Specific to DropTailFilter
  void Connect(IPAddr_t, PortId_t); // Connection to server
  void ValidIP(IPAddr_t, Mask_t);   // Specify valid source IP's
private:
  void ConstructorHelper();
  void RateAlgorithm(Packet* p);
  void DropPacket(Packet* p, const char*);
  double ComputeStatistic();
public:
  UDP*      l4Proto;    // UDP proto to use to contact server
  IPAddr_t  validIP;    // Valid IP's allowed on link (for spoof detection)
  Mask_t    validMask;  // Mask for above
  IPAddr_t  filterIP;   // IPAddress of mis-behaving host to always drop
  AppAlert* appAlert;   // Alert application for notifying others of storms
  bool      needAlert;  // True if need an alert on next spoofed pkt
  bool      rateAlgorithm; // true if need to run the rx-rate algorithm
  Timer     timer;      // Timer to re-enable alerts
  QColor*   normalColor;// Original color of node for restoration
  // Stuff for attack detection from UMd
  // Added by gpapag May 6, 2003 for multiple link detection
  int   fromNode_;            // the node that sends to the link
  int   toNode_;              // the node that receives from the link
  int   color_;               // the color of the queue
  // Added by gpapag for IP spoofing
  int  IPSpoofingChecked;
  // Added by gpapag for UDP flooding
  IPAddr_t UDPfloodingIP;
  bool     UDPchecked;
  //int  posG;
  //jjs DDOS-----------------
  bool DDOS_on;
  int  LL;
  LoadVec_t Load;
  double LastUpDate;
  int  Rate[100];
  int  L_index; 
  //ALVARO DDOS----------------------------------------------
  double y[BOUNDR][BOUNDC];	// where r is the bound for r
  int gamma;	                // which link are we computing
  double meany[BOUNDC];	        // mean of y. 10 is a bound on the 
  // number of outgoing links
  double covy[BOUNDC];	      //uncorrelated covariance matrix->vector
  int r;                //length of time window to compute the statistic
  int c;	                        // number of links at router
  double historicy[BOUNDR][BOUNDC];
  double nonparamstat;
  double YellowThres;
  double RedThres;
  
  //ALVARO DDOS----------------------------------------------
  // jjs Engress Filtering-----------------------------------
  
  ThroughputMap_t ThroughputPerSource; // rate of each source after alarm
  IPAddr_t MaxRateSourceIpAddr;       // block traffic from the source ip addr
  int TotalArriveAfterAlarm;     // no. of packet arrive > some fix 
                                 // integer (ex. 2000) -->begin to block 
                                 // after alarm
  bool UnderAttack;
  // jjs  Engress Filtering----------------------------------
  // Added from Jia-Shiang's code for tcp-flooding
#ifdef CHECK_SYN_FLOOD
  // Not presently used
  int       Firewall_;
  int       Monitor_Attacker[1000];
  SYNTimer *Monitor_Timer[1000];
#endif
  // The tcl scripts that run this code in ns2 define several global variables
public:
  static int Size;
  static int Degree;
  static int Subnets;
  static int SizeOfSubnets;
};

#endif
  
