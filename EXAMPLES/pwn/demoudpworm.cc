// USES WormUDP class as demo

#include <iostream>
#include <stdio.h>
#include <string>
#include <sys/time.h>

#define DEBUG_MASK 0x01

#include "debug.h"
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "args.h"
#include "validation.h"


#include "wormudp.h"
#include "wormhelper.h"

#ifdef HAVE_QT
#include <qnamespace.h>
#endif

#define SHOWANIMATION true

using namespace std;

FILE *tracefile, *infofile;


// Normally you should create a new class like this, so you can have many worms
// in one simulation, if you want. Use the SetSignature method to set the 
// signature of different worms.
class VSlammerWorm : public WormUDP {
};

// Progress hook
static void Progress(Time_t now)
{
  cout << Simulator::Now() << " " << VSlammerWorm::TotalInfected()<<endl; 
}

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;

  // Set node shape to a circle for animation

  Node::DefaultShape(Node::CIRCLE);

  VSlammerWorm::SetScanRate(50);
  VSlammerWorm::SetBaseIP(IPAddr("192.168.0.0"));
  VSlammerWorm::SetTargetVector
    (WTVUniform(AddressRangeOfRandomTreeNetworks(4,4,4,0,0)));
  VSlammerWorm::SetInfectionPort(1434);
  VSlammerWorm::SetPayloadLength(376);

  //Linkp2p lk(Rate("10Mb"), Time("20ms"));
  //Linkp2p hlk(Rate("10Mb"), Time("20ms"));
  //Linkp2p blk(Rate("100Mb"), Time("20ms"));  

  Linkp2p lk(Rate("1Mb"), Time("20ms"));
  Linkp2p hlk(Rate("1Mb"), Time("20ms"));
  Linkp2p blk(Rate("10Mb"), Time("20ms"));  


  CreateRandomTreeNetworksWithWorms(4, 4, 4, lk, 0, 0, hlk, 0.85,blk,
				    IPAddr("192.168.0.0"),VSlammerWorm());

  s.ProgressHook(Progress);

  if (SHOWANIMATION && !Validation::noAnimation) {
    s.StartAnimation(0, true);
    s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
  }

  s.Progress(Time("50ms"));
  s.StopAt(Time("5s"));
  s.Run();
}
