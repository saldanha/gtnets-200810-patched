// Test program for Ethernet LAN object, broadcasts
// George F. Riley.  Georgia Tech, Spring 2002

#include "common-defs.h"
#include "mask.h"
#include "simulator.h"
#include "ethernet.h"
#include "timer.h"
#include "validation.h"

using namespace std;

class MyTimerEvent : public TimerEvent {
public:
  MyTimerEvent(Interface* i) : iFace(i) { }
public:
  Interface* iFace; // Interface to use for broadcast
};

class MyTimer : public Timer {
public:
  virtual void Timeout(TimerEvent*);  // Called when timer expires
};

void MyTimer::Timeout(TimerEvent* ev)
{
  MyTimerEvent* mt = (MyTimerEvent*)ev;
  Time_t now = Simulator::Now();
  cout << "Hello from MyTimer::Timeout time " << now << endl;
  // Broadcast a packet on the specified interface
  Packet* p = new Packet();
  IPV4Header* ipHdr = new IPV4Header();
  ipHdr->dst = IPADDR_NONE;
  ipHdr->src = mt->iFace->GetIPAddr();
  ipHdr->ttl = 1;
  p->PushPDU(ipHdr);
  
  MACAddr dst;
  dst.SetBroadcast();
  mt->iFace->Send(p, dst, 0x01);
}

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Count_t enetSize = 10;
  if (argc > 1) enetSize = atol(argv[1]);

  // Create the Simulator object       
  Simulator s; 

 // Open the trace file
  Trace* gs = Trace::Instance();
  gs->Open("testenetbc.txt");
  // L2Proto802_3::Instance()->SetTrace(Trace::ENABLED);// Enable all l2 tracing
  Ethernet  e1(enetSize, IPAddr("208.201.0.1"), Mask(16));
  e1.RxOwnBroadcast(true);             // Set nodes to receive their own bcasts
  MyTimer myTimer;
  for (Count_t i = 0; i < enetSize; ++i)
    { // Schedule the broadcasts one second apart
      myTimer.Schedule(new MyTimerEvent(e1.GetIf(i)), 1.0 * i);
    }
  IPV4::Instance();                    // Insure IPV4 is in the proto graph
  s.StopAt(enetSize * 10.0);           // Stop after all broadcasts
  s.Run();                             // Run the simulation
}
