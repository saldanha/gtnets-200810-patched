/*
 * Generic address class
 */

#ifndef _addr_h_
#define _addr_h_

#include <string>

class Addr {
public:
      Addr() {}
      typedef enum { AF_INET, AF_INET6, AF_LINK } Addrfamily_t;
      Addr(Addrfamily_t af_):af(af_){}
      virtual bool operator ==(const Addr&) const = 0;
      virtual ~Addr() {}
public:
      Addrfamily_t af;
};
#endif
