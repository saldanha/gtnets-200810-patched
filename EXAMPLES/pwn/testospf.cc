
// A simple test application for OSPF
// Di-Shi Sun, Georgia Tech, Fall 2004

#include "simulator.h"
#include "node.h"
#include "linkp2p.h"
#include "ethernet.h"
#include "ratetimeparse.h"
#include "application-ospf.h"
#include "validation.h"

using namespace std;

int main (int argc, char** argv)
{
  Validation::Init (argc, argv);

  Simulator s;

  Node::DefaultShape(Node::CIRCLE);

  Node* node1 = new Node ();
  node1->SetLocation (0, 1);
  Node* node2 = new Node ();
  node2->SetLocation (1, 1);
  Node* node3 = new Node ();
  node3->SetLocation (1, 0);
  Node* node4 = new Node ();
  node4->SetLocation (2, 1);
  Node* node5 = new Node ();
  node5->SetLocation (2, 0);
  Node* node6 = new Node ();
  node6->SetLocation (3, 1);
 
  Linkp2p link12 (Rate ("0.05Mb"), Time ("10ms"));
  Linkp2p link13 (Rate ("0.05Mb"), Time ("10ms"));
  Linkp2p link23 (Rate ("0.05Mb"), Time ("10ms"));
  Linkp2p link24 (Rate ("0.05Mb"), Time ("10ms"));
  Linkp2p link35 (Rate ("0.05Mb"), Time ("10ms"));
  Linkp2p link45 (Rate ("0.05Mb"), Time ("10ms"));
  Linkp2p link46 (Rate ("0.05Mb"), Time ("10ms"));
  Linkp2p link56 (Rate ("0.05Mb"), Time ("10ms"));

  node1->AddDuplexLink (node2, link12, IPAddr ("192.168.12.1"), Mask (32), IPAddr ("192.168.12.2"), Mask (32)); 
  node1->AddDuplexLink (node3, link13, IPAddr ("192.168.13.1"), Mask (32), IPAddr ("192.168.13.3"), Mask (32)); 
  node2->AddDuplexLink (node3, link23, IPAddr ("192.168.23.2"), Mask (32), IPAddr ("192.168.23.3"), Mask (32)); 
  node2->AddDuplexLink (node4, link24, IPAddr ("192.168.24.2"), Mask (32), IPAddr ("192.168.24.4"), Mask (32)); 
  node3->AddDuplexLink (node5, link35, IPAddr ("192.168.35.3"), Mask (32), IPAddr ("192.168.35.5"), Mask (32)); 
  node4->AddDuplexLink (node5, link45, IPAddr ("192.168.45.4"), Mask (32), IPAddr ("192.168.45.5"), Mask (32)); 
  node4->AddDuplexLink (node6, link46, IPAddr ("192.168.46.4"), Mask (32), IPAddr ("192.168.46.6"), Mask (32)); 
  node5->AddDuplexLink (node6, link56, IPAddr ("192.168.56.5"), Mask (32), IPAddr ("192.168.56.6"), Mask (32)); 
  
  OSPFApplication* router1 = (OSPFApplication*)node1->AddApplication (OSPFApplication (IPAddr ("10.0.0.1")));
  OSPFArea* area1 = router1->AddArea (IPAddr ("0.0.0.1"));
  router1->AddInterface (area1, OSPFP2P, IPAddr ("192.168.12.1"), Mask (32), 1, 3);
  router1->AddInterface (area1, OSPFP2P, IPAddr ("192.168.13.1"), Mask (32), 1, 6);
  router1->Start (0.0);
  
  OSPFApplication* router2 = (OSPFApplication*)node2->AddApplication (OSPFApplication (IPAddr ("10.0.0.2")));
  OSPFArea* area2 = router2->AddArea (IPAddr ("0.0.0.1"));
  router2->AddInterface (area2, OSPFP2P, IPAddr ("192.168.12.2"), Mask (32), 1, 3);
  router2->AddInterface (area2, OSPFP2P, IPAddr ("192.168.23.2"), Mask (32), 1, 3);
  router2->AddInterface (area2, OSPFP2P, IPAddr ("192.168.24.2"), Mask (32), 1, 1);
  router2->Start (1.0);

  OSPFApplication* router3 = (OSPFApplication*)node3->AddApplication (OSPFApplication (IPAddr ("10.0.0.3")));
  OSPFArea* area3 = router3->AddArea (IPAddr ("0.0.0.1"));
  router3->AddInterface (area3, OSPFP2P, IPAddr ("192.168.13.3"), Mask (32), 1, 6);
  router3->AddInterface (area3, OSPFP2P, IPAddr ("192.168.23.3"), Mask (32), 1, 3);
  router3->AddInterface (area3, OSPFP2P, IPAddr ("192.168.35.3"), Mask (32), 1, 1);
  router3->Start (2.0);

  OSPFApplication* router4 = (OSPFApplication*)node4->AddApplication (OSPFApplication (IPAddr ("10.0.0.4")));
  OSPFArea* area4 = router4->AddArea (IPAddr ("0.0.0.1"));
  router4->AddInterface (area4, OSPFP2P, IPAddr ("192.168.24.4"), Mask (32), 1, 1);
  router4->AddInterface (area4, OSPFP2P, IPAddr ("192.168.45.4"), Mask (32), 1, 3);
  router4->AddInterface (area4, OSPFP2P, IPAddr ("192.168.46.4"), Mask (32), 1, 13);
  router4->Start (3.0);

  OSPFApplication* router5 = (OSPFApplication*)node5->AddApplication (OSPFApplication (IPAddr ("10.0.0.5")));
  OSPFArea* area5 = router5->AddArea (IPAddr ("0.0.0.1"));
  router5->AddInterface (area5, OSPFP2P, IPAddr ("192.168.35.5"), Mask (32), 1, 1);
  router5->AddInterface (area5, OSPFP2P, IPAddr ("192.168.45.5"), Mask (32), 1, 3);
  router5->AddInterface (area5, OSPFP2P, IPAddr ("192.168.56.5"), Mask (32), 1, 10);
  router5->Start (4.0);

  OSPFApplication* router6 = (OSPFApplication*)node6->AddApplication (OSPFApplication (IPAddr ("10.0.0.6")));
  OSPFArea* area6 = router6->AddArea (IPAddr ("0.0.0.1"));
  router6->AddInterface (area6, OSPFP2P, IPAddr ("192.168.46.6"), Mask (32), 1, 13);
  router6->AddInterface (area6, OSPFP2P, IPAddr ("192.168.56.6"), Mask (32), 1, 10);
  router6->Start (5.0);

  if (!Validation::noAnimation) {
    s.StartAnimation (0, true);
    s.AnimationUpdateInterval (Time ("100us"));
  }
  
  s.StopAt (30.0);
  s.Run ();
}

