// Test animation of with "Node Selected" callback
// George F. Riley, Georgia Tech, Summer 2004

#include <iostream>
#include <sstream>
#include <string>

#include "debug.h"
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "dumbbell.h"
#include "star.h"
#include "tcp-tahoe.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "l2proto802.3.h"
#include "validation.h"
#include "qtwindow.h"
#include "qtchildwindow.h"

#ifdef HAVE_QT
#include <qcolor.h>
#include <qcanvas.h>
#endif

#define N_LEAF 5

using namespace std;

class TextEvent : public  Event 
{
public:
  TextEvent(QCanvasText* ct) 
      : t(ct) {}
public:
  QCanvasText* t;
};

class TextHandler : public Handler
{
public:
  void Handle(Event*, Time_t);
};

void TextHandler::Handle(Event* e, Time_t)
{
  TextEvent* te = (TextEvent*)e;
  // Just delete the text event and update the canvas
  delete te->t;
  Simulator::instance->GetQTWindow()->Canvas()->update();
  // and delete the event as we don't need it anymore
  delete e;
}

// Define the texthandler object
TextHandler textHandler;

// Class for supercomputer links
class SCLink // : public linkp2p
{
public:
  SCLink();
  SCLink(Count_t linkNum,
         Count_t x0, Count_t y0,
         Count_t w0, Mult_t sinTheta0, Mult_t cosTheta0);
public:
  Count_t x; // x and y translation coordinates
  Count_t y;
  Count_t w; // Width. (Height is dependent on filts size, q size, etc)
  Mult_t  sinTheta; // x and y rotations
  Mult_t  cosTheta;
};

SCLink::SCLink() 
    : x(0), y(0), w(0), sinTheta(0), cosTheta(0)
{
}

SCLink::SCLink(Count_t linkNum,
               Count_t x0, Count_t y0,
               Count_t w0, Mult_t sinTheta0, Mult_t cosTheta0)
{ //
}

// SuperComputer router node animation
class SCNodeAnim : public  QTChildWindow
{
public:
  SCNodeAnim(const string&);
  virtual void Update(); // Update display data
  void DrawIQueueH(Count_t, Count_t);
  void DrawIQueueV(Count_t, Count_t);
  void DrawOQueueH(Count_t, Count_t);
  void DrawOQueueV(Count_t, Count_t);
public:
  QCanvasRectangle* outline;
  std::vector<QCanvasRectangle*> inQueue;
  std::vector<QCanvasRectangle*> outQueue;
  std::vector<QCanvasLine*>      inLinks;
public:
  // Global parameter settings
  static Count_t dimensions; // xy(2) or xyz(3)
  static Count_t inputQSize; // Packets
  static Count_t outputQSize;// Flits
  static Count_t flitsPerPacket;
  static Count_t bytesPerFlit;
  static Count_t w;
  static Count_t h;
};

Count_t SCNodeAnim::dimensions = 2; // xy(2) or xyz(3)
Count_t SCNodeAnim::inputQSize = 8; // Packets
Count_t SCNodeAnim::outputQSize = 4;// Flits
Count_t SCNodeAnim::flitsPerPacket = 16;
Count_t SCNodeAnim::bytesPerFlit = 8;
Count_t SCNodeAnim::w = 256;        // Window width and height
Count_t SCNodeAnim::h = 256;
 
SCNodeAnim::SCNodeAnim(const string& cap)
    : QTChildWindow(w, h, cap), outline(nil)
{
  // Need parameter for this
  dimensions = 2;
  inputQSize = 8;
  outputQSize = 4;
  flitsPerPacket = 16;
  bytesPerFlit = 8;
}

void SCNodeAnim::Update()
{
  QTChildWindow::Update();
  if (!outline)
    { // first call, create the objects
      Count_t x = width/8;
      Count_t y = height/8;
      Count_t w = width/4 * 3;
      Count_t h = height/4 * 3;
      outline = new QCanvasRectangle(x, y, w, h, canvas);
      outline->setPen(Qt::black);
      outline->show();
      // Links
      Count_t w2 = width/2;
      Count_t h2 = height/2;
      inLinks.push_back(new QCanvasLine(canvas));
      inLinks.back()->setPoints(0, h2, x, h2);
      inLinks.push_back(new QCanvasLine(canvas));
      inLinks.back()->setPoints(x + w, h2, width, h2);
      inLinks.push_back(new QCanvasLine(canvas));
      inLinks.back()->setPoints(w2, 0, w2, y);
      inLinks.push_back(new QCanvasLine(canvas));
      inLinks.back()->setPoints(w2, y + h, w2, height);
      // show and setPen
      for (vector<QCanvasLine*>::size_type i = 0; i < inLinks.size(); ++i)
        {
          inLinks[i]->show();
          inLinks[i]->setPen(Qt::black);
        }
      // Input and output queues
      Count_t w16 = w/16;
      Count_t h16 = h/16;
      //Count_t w32 = w/32;
      Count_t h32 = h/32;
      
      // Bottom input queue
      DrawIQueueH(y + h, y + h - h16);
      DrawOQueueH(y + h - h16 - h32, y + h - h32 - 2 * h16);
      // Top input queue
      DrawIQueueH(y, y + h16);
      // Left input queue
      DrawIQueueV(x, x + w16);
      // Right input queue
      DrawIQueueV(x + w, x + w - w16);
    }
  canvas->update();
}

void SCNodeAnim::DrawIQueueH(Count_t y0, Count_t y1)
{ // horizontal
  Mult_t dx = (Mult_t)(width/2)/inputQSize;
  Mult_t fx = width / 4;    // first
  Mult_t lx = fx + dx;  // last
  
  for (Count_t i = 0; i < inputQSize; ++i)
    {
      QCanvasRectangle* r = new QCanvasRectangle(
          (Count_t)fx, y0, (Count_t)(lx-fx), y1 - y0, canvas);
      r->setPen(Qt::black);
      r->show();
      // Advance x
      fx = lx;
      lx = fx + dx;
    }
}

void SCNodeAnim::DrawIQueueV(Count_t x0, Count_t x1)
{ // vertical
  Mult_t dy = (Mult_t)(height/2)/inputQSize;
  Mult_t fy = height / 4; // first
  Mult_t ly = fy + dy;    // last
  
  for (Count_t i = 0; i < inputQSize; ++i)
    {
      QCanvasRectangle* r = new QCanvasRectangle(
          x0, (Count_t)fy, x1 - x0, (Count_t)(ly-fy), canvas);
      r->setPen(Qt::black);
      r->show();
      // Advance x
      fy = ly;
      ly = fy + dy;
    }
}

void SCNodeAnim::DrawOQueueH(Count_t y0, Count_t y1)
{ // horizontal
  Count_t nFlits = outputQSize * (2 * dimensions - 1);
  nFlits += 2 * dimensions - 2; // Extra spacing between each oQueue
  
  Mult_t dx = (Mult_t)(width/2)/nFlits;
  Mult_t fx = width / 4;// first
  Mult_t lx = fx + dx;  // last
  
  for (Count_t j = 0; j < 2 * dimensions - 1; ++j)
    {
      for (Count_t i = 0; i < outputQSize; ++i)
        {
          QCanvasRectangle* r = new QCanvasRectangle(
              (Count_t)fx, y0, (Count_t)(lx-fx), y1 - y0, canvas);
          r->setPen(Qt::black);
          r->show();
          // Advance x
          fx = lx;
          lx = fx + dx;
        }
      // Add some extra space between queues
      fx += dx;
      lx += dx;
    }
}

      
  
void NodeSelectedChildWindow(Node* n)
{ // Test detailed child window
  string nn;
  string* nodeName = (string*)n->UserInformation();
  
  if (nodeName)
    {
      nn = *nodeName;
    }
  else
    {
      ostringstream osstr;
      osstr << "Node " << n->Id();
      nn = osstr.str();
    }
  SCNodeAnim* qtcw = new SCNodeAnim(nn);
  qtcw->Update();
}

  
void NodeSelected(Node* n)
{
  cout << "Hello from NodeSelected, node " << n->Id() << endl;
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  if (!qtw) return; // Can't happen
  QCanvas* canvas = qtw->Canvas();
  string* nodeName = (string*)n->UserInformation();
  QCanvasText* qct;
  if (nodeName)
    {
      cout << "name is " << *nodeName << endl;
      qct = new QCanvasText(nodeName->c_str(), canvas);
    }
  else
    {
      ostringstream osstr;
      osstr << "Node " << n->Id() << "\nSecondLine";
      qct = new QCanvasText(osstr.str(), canvas);
    }
  
  // Place the text item near the node on the canvas
  QPoint p = qtw->LocationToPixels(n->GetLocation());
  qct->move(p.x() + 10, p.y() - 10);
  qct->show();
  // Schedule an event 1 sec in future to remove it
  TextEvent* te = new TextEvent(qct);
  Scheduler::Schedule(te, 1.0, textHandler);
  // Update the canvas
  canvas->update();
  qtw->ProcessEvents();
}

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  Count_t nl = N_LEAF; // Number leaf nodes for dumbbell
  Count_t ns = N_LEAF; // Number leaf nodes for each star
  
  if (argc > 1) nl = atol(argv[1]);
  if (argc > 2) ns = atol(argv[2]);

  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);

  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->Open("testdb-star.txt");
  IPV4::Instance()->SetTrace(Trace::ENABLED);
  L2Proto802_3::GlobalSetTrace(Trace::ENABLED);
  
  TCP::LogFlagsText(true);       // Log TCP flags in text mode
  Linkp2p lk(Rate("10Mb"), Time("10ms"));
  Dumbbell b(nl, nl, 0.1,
             IPAddr("192.168.0.1"), IPAddr("192.169.0.1"), lk);
  // Specify the bounding box
  b.BoundingBox(Location(0,0), Location(10,10));
  
  Uniform startRng(0, 0.1); // Random number generator for start times
  // Create the Star networks and assign clients/servers
  Angle_t adderL = -M_PI / (b.LeftCount() + 1.0);
  Angle_t adderR = M_PI / (b.RightCount() + 1.0);
  Angle_t thetaL = -M_PI_2 + adderL;
  Angle_t thetaR = -M_PI_2 + adderR;

#ifdef HAVE_QT
  b.Left()->Color(Qt::blue);
  b.Right()->Color(Qt::blue);
#endif
  for (Count_t i = 0; i < b.LeftCount(); ++i)
    {
      Node* l = b.Left(i);
      Node* r = b.Right(i);
      // Create a star networks with this each leaf as hub
      Star* sl = new Star(ns,l,lk, IPAddr("192.168.1.1")+IPAddr("0.0.1.0")*i);
      Star* sr = new Star(ns,r,lk, IPAddr("192.169.1.1")+IPAddr("0.0.1.0")*i);
      
      sl->BoundingBox(Location(0,0), Location(2,2), thetaL, M_PI / 2);
      sr->BoundingBox(Location(0,0), Location(2,2), thetaR, M_PI / 2);

#ifdef HAVE_QT
      l->Color(Qt::blue);
      r->Color(Qt::blue);
#endif
      thetaL += adderL;
      thetaR += adderR;
      
      // Add a tcp server at each left side node and client at right side
      for (Count_t j = 0; j < sl->LeafCount(); ++j)
        {
          Node* ll = sl->GetLeaf(j); // Left leaf
          ostringstream lns;
          lns << "Left Star " << i << endl << " leaf " << j;
          ll->UserInformation(new string(lns.str()));
          string* tests = (string*)ll->UserInformation();
          cout << "star " << i << " leaf " << j
               << " id " << ll->Id()
               << " name " << lns.str() 
               << " ui " << *tests
               << endl;
          TCPServer* tcpApp =
            (TCPServer*)ll->AddApplication(TCPServer());
          tcpApp->BindAndListen(ll, 80);
          tcpApp->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
#ifdef HAVE_QT
          ll->Color(Qt::yellow);
#endif          
          Node* rl = sr->GetLeaf(j); // Right leaf
          TCPSend* ts = (TCPSend*)rl->AddApplication(
              TCPSend(ll->GetIPAddr(),80,
                      Constant(100000)));
          ostringstream rns;
          rns << "Right Star " << i << endl << " leaf " << j;
          rl->UserInformation(new string(rns.str()));
#ifdef HAVE_QT
          rl->Color(Qt::green);
          if (j == 0)
            { // Debug..set packets to blue for this flow
              ts->GetL4()->SetColor(Qt::blue);
            }
#endif
          ts->SetTrace(Trace::ENABLED);
          ts->Start(startRng.Value());
        }
    }

  // Specify animation
  if (!Validation::noAnimation)
    {
      s.EnableAnimationRecorder(true);
      s.RecorderMPEGSpeedup(40.0); // Record at 40x Real Time
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
      // Specify the "NodeSelected" callback
      //s.NodeSelectedCallback(NodeSelected);
      s.NodeSelectedCallback(NodeSelectedChildWindow);
    }
  
  s.Progress(1);
  s.StopAt(100);
  s.Run();
  tr->Close();
}
