// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-webbrowser.cc 500 2006-07-20 21:37:22Z rsimpson $



// Georgia Tech Network Simulator - Web Browser Application Class
// George F. Riley.  Georgia Tech, Fall 2002

// Define a Web Browser Application class
// Based on the Web Browsing models by Charles R. Simpson, Jr., Dheeraj Reddy, George Riley, PADS 2006

// Uncomment below to enable debug 
//#define DEBUG_MASK 0x02
#include "debug.h"
#include "application-webbrowser.h"
#include "tcp.h"
#include "simulator.h"
#include "globalstats.h"

//#include "http-distributions.h"	// Use this include for models by Bruce Mah, Infocom 1997
#include "netihttp-distributions.h"		// models by Charles R. Simpson, Jr., Dheeraj Reddy, George Riley, PADS 2006

using namespace std;

// Number of simulataneous connections
#define DEFAULT_CONNECTIONS 2

// Static member variables
/*
// These are the Empirical Distriubtions described by Bruce Mah in Infocom 1997
HttpPrimaryRequest*   WebBrowser::primaryRequest;
HttpSecondaryRequest* WebBrowser::secondaryRequest;
HttpPrimaryReply*     WebBrowser::primaryReply;
HttpSecondaryReply*   WebBrowser::secondaryReply;
HttpFilesPerPage*     WebBrowser::filesPerPage;
HttpConsecutivePages* WebBrowser::consecutivePages;
HttpThinkTime*        WebBrowser::thinkTime;
*/
// These are the NETI@home Empirical Distributions described in PADS 2006
NETIHttpRequest*          WebBrowser::request;
NETIHttpReply*            WebBrowser::reply;
NETIHttpConsecutivePages* WebBrowser::consecutivePages;
NETIHttpDiffThinkTime*    WebBrowser::diffThinkTime;
NETIHttpSameThinkTime*    WebBrowser::sameThinkTime;


Count_t               WebBrowser::defaultConcurrentConn = DEFAULT_CONNECTIONS;
Count_t               WebBrowser::activeBrowsers;
Count_t               WebBrowser::activeConnections;
Count_t               WebBrowser::completedConnections;
Count_t               WebBrowser::failedConnections;
Count_t               WebBrowser::startedThisPeriod;
Count_t               WebBrowser::completedThisPeriod;
Time_t                WebBrowser::totRespTime;
DCount_t              WebBrowser::totRespSize;
Count_t               WebBrowser::totObjectsPerPage;
Count_t               WebBrowser::totBrowserSessions;
Time_t                WebBrowser::largestRespTime;
Count_t               WebBrowser::largestRespSize;
Time_t                WebBrowser::totWaitTime;
Time_t                WebBrowser::largestWaitTime;
Count_t               WebBrowser::waitCount;
DCount_t              WebBrowser::totReqBytes;   // Total size of requests
DCount_t              WebBrowser::totRespBytes;  // Total size of responses
Count_t               WebBrowser::cleanConnections;
Count_t               WebBrowser::dirtyConnections;
Count_t               WebBrowser::clientTimeouts;
Count_t               WebBrowser::serverTimeouts;

// Browser Connection methods
BrowserConnection::BrowserConnection() 
    : t(nil), start(0),
      rqSize(0), rsSize(0), rxSize(0), cnCount(0),
      active(false), logged(false), closed(false),
      first(true)
{
}

BrowserConnection::BrowserConnection(const TCP& tcp) 
  : t((TCP*)tcp.Copy()), start(0),
    rqSize(0), rsSize(0), rxSize(0), cnCount(0),
    active(false), logged(false), closed(false)
{
}


// Constructor
WebBrowser::WebBrowser(const IPAddrVec_t& s, // List of servers
                       const Random& ss,     // Random to select server
                       const TCP& t)         // TCP Variant to use
    : pendingEvent(nil), node(nil), tcp((TCP*)(t.Copy())),
      stats(nil), dbstats(nil),
      servers(s),
      selectServer(ss.Copy()),
      concurrentConn(defaultConcurrentConn),
      serverIP(IPADDR_NONE), consecutive(0),
      nObjects(0), nObjectsRq(0), nObjectsRx(0),
      thinkTimeBound(INFINITE_TIME), idleTime(INFINITE_TIME),
      verbose(false)
{
#ifdef MOVED_TO_START
  for (Connections_t::size_type i = 0; i < connections.size(); ++i)
    {
      connections[i].t = (TCP*)t.Copy(); // Load the tcp endpoints 
      connections[i].t->AttachApplication(this); // Notify of application
      DEBUG(4,(cout << "WebBrowser tcp " << connections[i].t << endl));
    }
#endif
}

WebBrowser::WebBrowser(const WebBrowser& c)
    : Application(c), pendingEvent(nil), node(c.node),
      tcp((TCP*)(c.tcp->Copy())),
      stats(c.stats),
      dbstats(c.dbstats),
      servers(c.servers),
      selectServer(c.selectServer->Copy()),
      concurrentConn(c.concurrentConn),
      serverIP(c.serverIP), consecutive(c.consecutive),
      nObjects(c.nObjects),
      nObjectsRq(c.nObjectsRq), nObjectsRx(c.nObjectsRx),
      thinkTimeBound(c.thinkTimeBound), idleTime(c.idleTime),
      verbose(c.verbose)
{ // Copy constructor
#ifdef MOVED_TO_START
  for (Connections_t::size_type i = 0; i < connections.size(); ++i)
    { // Load the tcp endpoints 
      if (connections[i].t)
        connections[i].t = (TCP*)connections[i].t->Copy();
    }
#endif
}

WebBrowser::~WebBrowser()
{ // Destructor
  // Delete the select server RNG
  if (selectServer) {
    delete selectServer;
    selectServer=nil;
  }
  if (tcp) {
    delete tcp;
    tcp=nil;
  }
  // Delete the tcp connections
  for (ConnectionVec_t::size_type i = 0; i < connections.size(); ++i)
    { // Load the tcp endpoints 
      if (connections[i].t) {
        delete connections[i].t;
        connections[i].t=nil;
      }
    }
}

// Handler Methods
void WebBrowser::Handle(Event* e, Time_t t)
{
  AppWebBrowserEvent* ev = (AppWebBrowserEvent*)e;
  switch (ev->event) {
    case AppWebBrowserEvent::REQUEST_PAGE :
      DEBUG0((cout << "Request page Event" << endl));
      if (Simulator::Now() < idleTime)
        {
          RequestWebPage();  // Request the next web page
        }
      pendingEvent = nil;
      delete e;
      return;
    default :
      break;
  }
  Application::Handle(e, t);  // May be application event
}

  // Upcalls from L4 protocol
void WebBrowser::Receive(Packet* p,L4Protocol* pr,Seq_t s)
{ // Data received, see if we have all
  BrowserConnection* c = FindConnection(pr);
  if (!c)
    {
      cout << "HuH?  WebBrowser::Receive, can't find connection"
           << endl;
      return;
    }
  Data* d = (Data*)p->PopPDU();  // Get the data pdu
  if (c->rxSize != s) 
    { // debug testing
      cout << "HuH?  TCP " << c->t
           << " seq/rx mismatch, seq " << s
           << " rxSize " << c->rxSize << endl;
      exit(1);
    }
  c->rxSize += d->Size();        // Count bytes received
  DEBUG(2,(cout << "AppWB, RX " << d->Size() 
          << " tot " << c->rxSize
          << " exp " << c->rsSize << endl));
  delete p; // Delete the packet
}

void WebBrowser::CloseRequest(L4Protocol* pr)
{ // Close request from peer
  BrowserConnection* c = FindConnection(pr);
  DEBUG0((cout << "WebBrowser::CloseRequest() " << this << endl));
  pr->Close(); // Close in response
  Time_t now = Simulator::Now();
  if (stats)
    { // Log response time in statistics object
      stats->Record(now - c->start);
      totRespSize += c->rxSize;
      totRespTime += (now - c->start);
      if (c->rxSize > largestRespSize)
        largestRespSize=c->rxSize;
      if ((now - c->start) > largestRespTime)
        largestRespTime = now - c->start;
      DEBUG(2,(cout << "Size " << c->rxSize << " resp " << now - c->start 
               << " toc " << c->t->TimeoutCount()
               << " tod " << c->t->TimeoutDelay()));
      TCP* pp = c->t->Peer();
      if (pp)
        {
          DEBUG(2,(cout << " ptoc " << pp->TimeoutCount()
                   << " ptod " << pp->TimeoutDelay()));
        }
      DEBUG(2,(cout << endl));
    }
  if (verbose)
    {
      cout << "rqSize " << c->rqSize
           << " rxSize " << c->rxSize
           << " rxTime " << now - c->start
           << " ";
      Statistics* st = c->t->GetRTTEstimator()->GetPrivateStatistics();
      if (st) st->Log(cout);
    }
  c->logged = true;
  DEBUG(1,(cout << "AppWB, CloseReq, Time " << now
       << " logging resp time " << now - c->start
       << " objsize " << c->rsSize
       << " rxSize " << c->rxSize
           << endl));
  // More debug
  TCP* tc = c->t->Peer();
  if (tc)
    {
      if (tc->TimeoutCount() == 0 && tc->RetransmitCount() == 0)
        {
          cleanConnections++;
        }
      else
        {
          dirtyConnections++;
        }
      serverTimeouts += tc->TimeoutCount();
    }
  clientTimeouts += c->t->TimeoutCount();
}

void WebBrowser::Closed(L4Protocol* pr)
{ // Connection has closed
  DEBUG0((cout << "WebBrowser::Closed() " << this << endl));
  BrowserConnection* c = FindConnection(pr);
  // debug
  if (dbstats)
    {
      TCP* tc = c->t->Peer();
      if (tc) dbstats->Record(tc->TimeoutCount());
    }
  if (!c->logged)
    {
      Time_t now = Simulator::Now();
      if (stats)
        { // Log response time in histogram
          stats->Record(now - c->start);
          totRespSize += c->rxSize;
          totRespTime += (now - c->start);
          if (c->rxSize > largestRespSize)
            largestRespSize = c->rxSize;
          if ((now - c->start) > largestRespTime)
            largestRespTime = now - c->start;
          DEBUG(2,(cout << "Size " << c->rxSize << " resp " << now - c->start 
                   << " toc " << c->t->TimeoutCount()
                   << " tod " << c->t->TimeoutDelay()));
          TCP* pp = c->t->Peer();
          if (pp)
            {
              DEBUG(2,(cout << " ptoc " << pp->TimeoutCount()
                       << " ptod " << pp->TimeoutDelay()));
            }
          DEBUG(2,(cout << endl));
        }
      if (verbose)
        {
          cout << "rqSize " << c->rqSize
               << " rxSize " << c->rxSize
               << " rxTime " << now - c->start
               << " ";
          Statistics* st = c->t->GetRTTEstimator()->GetPrivateStatistics();
          if (st) st->Log(cout);
        }
      c->logged = true;
      // More debug
      TCP* tc = c->t->Peer();
      if (tc)
        {
          if (tc->TimeoutCount() == 0 && tc->RetransmitCount() == 0)
            {
              cleanConnections++;
            }
          else
            {
              dirtyConnections++;
              serverTimeouts += tc->TimeoutCount();
            }
        }
      clientTimeouts += c->t->TimeoutCount();
      // end debug
    }
  if (c->active)
    { // Finshed, see if we need another
      DEBUG(2,(cout << "AppWB, Closed "
               << " tot " << c->rxSize
               << "  exp " << c->rsSize << endl));
      if (c->rxSize != c->rsSize)
        { // ! HuH?
          cout << "WebBrowser " << this << " debug exit" << endl;
          cout << "AppWB, " << this << " Closed, time " << Simulator::Now()
               << " TCP " << c->t
               << " tot " << c->rxSize
               << " exp " << c->rsSize << endl;
          cout << "MyIP " << (string)IPAddr(c->t->localNode->GetIPAddr())
               << " myport " << c->t->localPort << endl;
        }
      ObjectComplete(c);
    }
}

void WebBrowser::ConnectionComplete(L4Protocol* pr)
{ // Connection request succeeded
  BrowserConnection* c = FindConnection(pr);
  if (!c)
    {
      cout << "HuH?  WebBrowser::ConnectionComplete, can't find connection"
           << endl;
      return;
    }
  DEBUG(1,(cout << "WebBrowser::ConnectionComplete, "
           << " TCP " << c->t
           << " req " << c->rqSize
           << " resp " << c->rsSize << endl));
  Data d(c->rqSize);           // Request size
  d.msgSize = c->rqSize;       // Request size
  d.responseSize = c->rsSize;  // Response size
  c->t->Send(d);               // Send the request
}

void WebBrowser::ConnectionFailed(L4Protocol* pr, bool aborted)
{ // Connection request failed
  BrowserConnection* c = FindConnection(pr);
  failedConnections++;
  if (!c)
    {
      cout << "HuH?  WebBrowser::ConnectionFailed, can't find connection"
           << endl;
      return;
    }
  if (c->cnCount < CONNECT_RETRY)
    { // Try again
      c->cnCount++;
      cout << "WebBrowser::ConnectionFailed, TCP " << c->t 
               << " retrying try " << c->cnCount
               << " of " << CONNECT_RETRY 
               << " time " << Simulator::Now()
               << endl;
      c->t->Reset(); // Reset the connection
      c->t->Connect(serverIP, HTTP_PORT);
    }
  else
    {
      cout << "WebBrowser::ConnectionFailed, TCP " << c->t 
               << " giving up" 
               << " time " << Simulator::Now()
               << endl;

      if (stats)
	{ // Log response time in statistics object
	  stats->Record(INFINITE_TIME);
	}      
      c->active = false;
      ObjectComplete(c);
    }
}

// Application Methods
void WebBrowser::StartApp()    // Called at time specified by Start
{
  DEBUG0((cout << "Starting browser " << this 
          << " time " << Simulator::Now() << endl));
  StopApp();                   // Insure no pending event
  if (connections.size() == 0)
    { // Need to create the connections list
      for (ConnectionVec_t::size_type i = 0; i < concurrentConn; ++i)
        {
          connections.push_back(BrowserConnection(*tcp));
          BrowserConnection& c = connections.back();
          c.t->AttachApplication(this);
        }
    }
  InitializeRandom();
  RequestWebPage();            // Request the next web page    
}

void WebBrowser::StopApp()     // Called at time specified by Stop
{
  if (pendingEvent)
    { // Cancel the pending start event
      Scheduler::Cancel(pendingEvent);
      delete pendingEvent;
      pendingEvent = nil;
    }
}

void WebBrowser::AttachNode(Node* n)
{ // Note attached node
  node = n;
  tcp->Attach(n);
}

Application* WebBrowser::Copy() const
{ // Make a copy of this application
  return new WebBrowser(*this);
}

void WebBrowser::ConcurrentConnections(Count_t c)
{ // Set number of concurrent connections
  concurrentConn = c;
}

void WebBrowser::DBDump()
{
  for (ConnectionVec_t::size_type i = 0; i < connections.size(); ++i)
    {
      if (connections[i].active)
        {
          connections[i].t->DBDump(); // Dump active tcp connection
          cout << "       rsSize " << connections[i].rsSize
               << " rxSize " << connections[i].rxSize << endl;
        }
    }
}

// Private Methods

BrowserConnection* WebBrowser::FindConnection(L4Protocol* l4)
{
  for (ConnectionVec_t::size_type i = 0; i < connections.size(); ++i)
    {
      if (connections[i].t == l4) return &connections[i]; // Found it
    }
  return nil; // Not found
}

void WebBrowser::RequestWebPage()
{ // Request the next web page
  if (servers.size() == 0)
    {
      cout << "WebBrowser::RequestWebPage, no servers!" << endl;
      return;
    }
  activeBrowsers++;
  if (!consecutive)
    { // Time to select a new server
      if (!consecutivePages) InitializeRandom();
      consecutive = consecutivePages->IntValue();
      IRandom_t ss = selectServer->IntValue(); // Choose a server
      if (ss >= servers.size())
        { 
          cout << "WebBrowser::RequestWebPage, bad server selector" << endl;
          ss = 0;
        }
      serverIP = servers[ss];
    }
  //nObjects = filesPerPage->IntValue();
  nObjects = 1;
  if (!nObjects) nObjects++; // Zero not allowed
  totObjectsPerPage += nObjects;
  totBrowserSessions++;
  nObjectsRq = 0;
  nObjectsRx = 0;
  DEBUG0((cout << "WebBrowser::RWP, nObj " << nObjects
          << "conn.size() " << connections.size()
          << endl));
  for (ConnectionVec_t::size_type i = 0; i < connections.size(); ++i)
    {
      RequestOneFile(&connections[i]);
      if (nObjectsRq == nObjects) break; // Requested all
    }
}

void WebBrowser::RequestOneFile(BrowserConnection* c)
{
  c->t->Reset(); // Reset the tcp endpoint
  c->start = Simulator::Now(); // Note the start time
  //if (!nObjectsRq)
/*
  if (nObjectsRq < 2) // The Jeffay experiment had a bug, primary twice
    { // Primary
      c->rqSize = primaryRequest->IntValue();  // Request size
      c->rsSize = primaryReply->IntValue();    // Response size
    }
  else
    { // Secondary
      c->rqSize = secondaryRequest->IntValue();// Request size
      c->rsSize = secondaryReply->IntValue();  // Response size
    }
*/
  c->rqSize = request->IntValue();  // Request size
  c->rsSize = reply->IntValue();    // Response size

  DEBUG0((cout << "Browser " << this << " rqSize " << c->rqSize
          << " rsSize " << c->rsSize << endl));
  if (!c->rqSize) c->rqSize++;
  if (!c->rsSize) c->rsSize++;        // Insure non-zero for sizes
  c->rxSize = 0;                      // Clear received count   
  nObjectsRq++;                       // Note this request
  c->t->Bind();                       // Bind to available local port
  c->t->FlowId(++TCP::nextFlowId);    // Unique id for debugging
  DEBUG(1,(cout << "AppWB Connecting to server IP " << (string)IPAddr(serverIP)
          << " time " << Simulator::Now()
          << endl));
  
  c->t->Connect(serverIP, HTTP_PORT); // Connect to the server
  c->active = true;
  c->logged = true;
  c->cnCount = 0;                     // Connection Count
  activeConnections++;
  startedThisPeriod++;
  totReqBytes  += c->rqSize;
  totRespBytes += c->rsSize;
  DEBUG(2,(cout << "AppWB, RequestSize " << c->rqSize
           << " ReplySize " << c->rsSize << endl));
  DEBUG(2,(cout << "AppWB, " << this << " Rq " << c->rqSize
           << " Rs " << c->rsSize 
           << " ip " << (string)IPAddr(c->t->localNode->GetIPAddr())
           << " port " << c->t->localPort << endl));
  DEBUG0((cout << "WebBrowser::ROF, obj " << nObjectsRq <<
          " of " << nObjects << endl));
}

void WebBrowser::ObjectComplete(BrowserConnection* c)
{
  Time_t st;
  // Remove binding
  c->t->Unbind(c->t->ProtocolNumber(), c->t->Port());
  c->active = false;
  nObjectsRx++; // Count received objects
  activeConnections--;
  completedConnections++;
  completedThisPeriod++;
  if (nObjectsRq < nObjects)
    { // Still pending objects
      RequestOneFile(c);
    }
  if (nObjectsRx == nObjects)
    { // All done, get the sleep time and schedule wakeup event
      activeBrowsers--;
      consecutive--;
      if (Simulator::Now() < idleTime)
        { // Not time to idle
/*
          if (!consecutive)
            { // Try to match a bug in the UNC jeffay experiment
              // They had a bug that did not sleep when choosing
              // a new server
              RequestWebPage();
            }
          else
            {
              Time_t st = thinkTime->Value();
              DEBUG0((cout << "Browser " << this << "tt " << st << endl));
              if (st > thinkTimeBound)
                {
                  DEBUG0((cout << "Bounding TT " << st 
                          << " to " << thinkTimeBound << endl));
                  st = thinkTimeBound;
                }
              pendingEvent = 
                new AppWebBrowserEvent(AppWebBrowserEvent::REQUEST_PAGE);
              // Schedule again    
              DEBUG(1,(cout << "Sleeping for " << st << " secs" << endl));
              Scheduler::Schedule(pendingEvent, st, this);
              // Stats for debugging
              totWaitTime += st;
              if (st > largestWaitTime) largestWaitTime = st;
              waitCount++;
            }
*/
          if (!consecutive)
            { // New Server
               st = diffThinkTime->Value();
            }
          else
            { // Same Server
               st = sameThinkTime->Value();
            }
          DEBUG0((cout << "Browser " << this << "tt " << st << endl));
          if (st > thinkTimeBound)
            {
              DEBUG0((cout << "Bounding TT " << st 
                      << " to " << thinkTimeBound << endl));
              st = thinkTimeBound;
            }
          pendingEvent = 
            new AppWebBrowserEvent(AppWebBrowserEvent::REQUEST_PAGE);
          // Schedule again    
          DEBUG(1,(cout << "Sleeping for " << st << " secs" << endl));
          Scheduler::Schedule(pendingEvent, st, this);
          // Stats for debugging
          totWaitTime += st;
          if (st > largestWaitTime) largestWaitTime = st;
          waitCount++;
        }
    }
}

// Static Methods
void WebBrowser::InitializeRandom()
{
/*
  if (primaryRequest) return; // Already intialized
  primaryRequest   = new HttpPrimaryRequest();
  secondaryRequest = new HttpSecondaryRequest();
  primaryReply     = new HttpPrimaryReply();
  secondaryReply   = new HttpSecondaryReply();
  filesPerPage     = new HttpFilesPerPage();
  consecutivePages = new HttpConsecutivePages();
  thinkTime        = new HttpThinkTime();
*/
  if (request) return; // Already initialized
  request          = new NETIHttpRequest();
  reply            = new NETIHttpReply();
  consecutivePages = new NETIHttpConsecutivePages();
  diffThinkTime    = new NETIHttpDiffThinkTime();
  sameThinkTime    = new NETIHttpSameThinkTime();
}

