// Test nix-vector creation and extension
// George F. Riley, Georgia Tech, Summer 2003

#include "simulator.h"
#include "routing-nixvector.h"
#include "validation.h"  

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  NixVectorOption* pnv = new NixVectorOption();
  for (int i = 0; i < 72; ++i)
    {
      cout << "Adding bit number " << i << endl;
      pnv->Add(1, 1);
    }
  s.Run();
}
