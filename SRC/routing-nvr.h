/* -*- mode: c++ -*- */

// GENERAL PUBLIC LICENSE AGREEMENT
//
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
//
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
//
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
//
// 1.  This License allows you to:
//
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
//
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//
//
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
//
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
//
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
//
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
//
//
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
//
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
//
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
//
//
// 3.  Export Law Assurance.
//
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//
// 4.  Termination.
//
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
//
//
// 5.  Disclaimer of Warranties and Limitation on Liability.
//
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
//
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
//
// GTRC shall have no obligation for support or maintenance of Program.
//
// 6.  Copyright Notice.
//
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
//
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
//
// Said copyright notice shall read as follows:
//
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


#ifndef __ROUTING_NVR_H
#define __ROUTING_NVR_H

#include <string.h>
#include "common-defs.h"
#include "routing.h"
#include "nixvector.h"
#include "ipv4.h"
#include "timer.h"
#include "notifier.h"

#define TTL_INIT	3
#define TTL_INC		2
#define TTL_THRESH	7
#define TTL_MAX		16

#define PROTO_NVR	0x8FF

class Uniform;

class NVREvent : public TimerEvent {
public:
  enum NVREvent_t { NV_REQ=1, NV_REQ_DELAY, NV_SENDBUF, NV_SENDBUF_TO };

public:
  NVREvent(Event_t ev, int i=0) : TimerEvent(ev), h(i) {}
  virtual ~NVREvent() {}

public:
  int  h;    // index of associated item in a table
};

class NVROption : public IPV4Options {
public:
  enum { Number=25 };
  enum NVROpt_t {
      NC_REQ=1, NC_REP, NC_REQ_SYM, NC_REP_SYM, NV_ROT, NV_ROT_LITE,
      NV_REP, NV_ROT_FAIL
  };

public:
  NVROption() : IPV4Options(Number), type(0) {}
  NVROption(Byte_t t) : IPV4Options(Number), type(t) {}
  virtual Size_t Size() const = 0;
  virtual PDU* Copy() const = 0;

public:
  Byte_t type;    // NVR option type
};

// NVRLiteHeader
class NVRLiteHeader : public NVROption {
public:
  NVRLiteHeader() : NVROption(NV_ROT_LITE) {}
  NVRLiteHeader(Byte_t t) : NVROption(t) {}
  NVRLiteHeader(IPAddr_t o, Byte_t q, Byte_t p, const CNixVector& n)
      : NVROption(NV_ROT_LITE), pidOrg(o), pidQnum(q), pidPnum(p), nvRoute(n) {}
  virtual ~NVRLiteHeader() {}

  virtual Size_t Size() const;
  virtual PDU*   Copy() const { return new NVRLiteHeader(*this); }

public:
  IPAddr      pidOrg;     // origin
  Byte_t      pidQnum;    // NVREQ #
  Byte_t      pidPnum;    // NVREP #
  CNixVector  nvRoute;    // route
  CNixVector  rnvRoute;
};

// NV Request Option, Symmetric
// NCSymReqHeader
class NCSymReqHeader : public NVRLiteHeader {
public :
  NCSymReqHeader() : NVRLiteHeader(NC_REQ_SYM) {}
  virtual ~NCSymReqHeader() {}

  Size_t  Size() const;
  PDU*    Copy() const { return new NCSymReqHeader(*this); }

public:
  Byte_t      id;
  IPAddr      src;
  IPAddr      dst;
  Byte_t      mid;
  CMetricVec  mv;
  MACAddr     from_inf;
  CNixVector  rnv;        // NV back to src
};

// NV Reply Option, Symmetric
// NCSymRepHeader
class NCSymRepHeader : public NVRLiteHeader {
public :
  NCSymRepHeader() : NVRLiteHeader(NC_REP_SYM) {}
  virtual ~NCSymRepHeader() {}

  Size_t  Size() const;
  PDU*    Copy() const { return new NCSymRepHeader(*this); }

public:
  IPAddr      src;
  IPAddr      dst;
  Byte_t      mid;
  CMetricVec  mv;
  CMetricVec  cmv;
  MACAddr     from_inf;
  CNixVector  nv;         // requested NV
};

// NVRFailHeader
// NV Route Failure Option
class NVRFailHeader : public NVRLiteHeader {
public:
  NVRFailHeader() : NVRLiteHeader(NV_ROT_FAIL) {}
  NVRFailHeader(IPAddr_t dst, IPAddr_t org, IPAddr_t end);
  NVRFailHeader(IPAddr_t dst, IPAddr_t org, IPAddr_t end, const CRouteInfo* ri);
  virtual ~NVRFailHeader() {}

  Size_t Size() const;
  PDU* Copy() const { return new NVRFailHeader(*this); }

public:
  IPAddr      dst;        // target
  IPAddr      org;        // origin
  IPAddr      end;        // error reporting node
};

// RoutingNVR Class

typedef std::list<SendBufItem> SendBuf_t;

class RoutingNVR : public Routing, public TimerHandler, public NotifyHandler {
public:
  RoutingNVR();
  virtual ~RoutingNVR();

  void  Default(RoutingEntry) {}         // Specify default route
  RoutingEntry GetDefault() { return RoutingEntry(); }
  void  Add(IPAddr_t, Count_t, Interface*, IPAddr_t) {}  // Add a routing entry
  RoutingEntry Lookup(Node*, IPAddr_t) { return RoutingEntry(); }
  RoutingEntry LookupFromPDU(PDU*) { return RoutingEntry(); }
  Routing* Clone() { return new RoutingNVR(); }
  RType_t  Type() { return NVR; }
  void  InitializeRoutes(Node*);         // Initialization
  bool  NeedInit() { return true; }      // True if init needed
  Size_t Size() const { return 0; }

  CRouteInfo* GetRouteInfo(IPAddr_t);
  CRouteInfo* GetRouteInfo(const PathId&, bool=false);
  Byte_t GetNCReqID() { return ++nvQnum; }
  MACAddr Lookup(CNixVector&, bool=false, bool=false);
  bool  IsBetter(int i, Byte_t hop)  { return NVFIB.IsBetter(i,hop); }
  bool  IsWorse(int i, Byte_t hop)   { return NVFIB.IsWorse(i,hop); }
  bool  IsShorter(int i, Word_t nvl) { return NVFIB.IsShorter(i,nvl); }

  void  DataRequest(Node*, Packet*, void*);
  bool  DataIndication(Interface*, Packet*);
  void  L3Transmit(Packet*, IPAddr_t, Count_t, Proto_t, NVROption*);
  virtual Proto_t Proto()   { return PROTO_NVR; }
  void  Notify(void*);    // L2 xmit failure notification handler

private:
  void  DataSend(Node*, Packet*);
  bool  DoNVRotLite(Interface*, Packet*, NVROption*);
  void  DoNCSymReq(Interface*, Packet*, NVROption*);
  void  DoNCSymRep(Interface*, Packet*, NVROption*);
  void  DoNVRotFail(Interface*, Packet*, NVROption*);
  void  DoL2Fail(Interface*, Packet*);
  void  ReturnNVRotFail(const PathId&, bool, const CNixVector&);
  NCSymRepHeader* CreateNCSymRep(NCSymReqHeader*, MACAddr&, CRouteInfo* ri=nil);
  void  Broadcast(Interface*, Packet*);
  void  Unicast(Interface*, Packet*, MACAddr&, bool=false);
  bool  ConstructNV(CNixVector&, CNixVector&, MACAddr&);
  void  SendNVRequest(Node*, IPAddr, Byte_t, Time_t, Byte_t retry_cnt=0);
  void  PrintRoute(CNixVector&);
  void  AddNCDelay(Packet*, Time_t);
  void  UpdateNVTime(const PathId&);    // update NV timestamp
  IPV4Header* AddIPHeader(Packet*, IPAddr_t, Count_t, Proto_t);

  // Timer
  void  ScheduleTimer(Event_t, NVREvent*&, Time_t);
  void  CancelTimer(NVREvent*&, bool=false);
  void  Timeout(TimerEvent*);    // timer event handler

  // NV request list
  bool  AddNVReq(IPAddr_t, Byte_t, Byte_t, Byte_t, Time_t);
  int   FindNVReq(IPAddr_t);

  // Send buffer
  void  SendBufferEnque(Packet*, IPAddr_t);
  SendBuf_t::iterator SendBufferFind(IPAddr_t);
  void  SendBufferCheck();
  void  SendBufferClear(IPAddr_t);
  void  ProcessPending(Node*, IPAddr_t);
  void  ProcessPendingPeriod(Node*, IPAddr_t);
  void  ProcessPendingPeriod();

public:
  static bool    enableL2Notify;
  static bool    enableRingSearch;
  static bool    enableDelayedNVReq;
  static bool    enableOnlyDstReply;
  static bool    enableNCUnicast;
  static bool    enablePnumFilter;
  static bool    enableNVCheck;
  static bool    enableBlackList;
  static Word_t  defaultNVReqListSize;
  static Byte_t  defaultNVReqRetry;
  static Word_t  defaultSendBufSize;
  static Time_t  defaultSendBufPeriod;
  static Time_t  defaultSendBufTimeout;
  static Time_t  defaultRTT0;

private:
  typedef std::vector<CNCQueItem>  NCQue_t;
  typedef std::vector<NVReqItem>   NVReqList_t;

  //Uniform*    nvreqSlot;     // NV request random time slot
  Uniform*    nvreqJitter;   // NV request jitter in a time slot
  Byte_t      nvQnum;        // NVREQ #
  Byte_t      nvPnum;        // NVREP #
  Timer       timer;
  NVREvent*   evSendBuf;     // SendBuffer period
  NVREvent*   evSendBufTimeout;

  CNCList     NCList;        // NC list
  CReplyList  NRList;        // Reply list
  CNVFIB      NVFIB;         // NV-FIB
  CNbrTable   NbrTable;      // Neighbor table
  NCQue_t     nvreqQue;      // Que for delayed NV request
  NVReqList_t nvreqList;     // NV request list
  SendBuf_t   sendBuffer;    // Buffer for pending packets
  BlackList   blackList;     // Black list
};

#endif
