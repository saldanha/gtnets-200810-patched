// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: link16.h 394 2005-08-18 15:36:13Z riley $



// Georgia Tech Network Simulator - Military Link16 Link class
// George F. Riley.  Georgia Tech, Summer 2005

#ifndef __link16_h__
#define __link16_h__

#include "wlan.h"
#include "ratetimeparse.h"

//Doc:ClassXRef
class Link16 : public WirelessLink {
//Doc:Class Class {\tt Link16} defines a military Link16 type link.

public:
  //Doc:Method
  Link16(Count_t,             // Node count
         IPAddr_t,            // IP addr start
         Rate_t r = Rate("1Mb"));
  //Doc:Desc This is the default constructor for this link. It constructs
  //Doc:Desc a default Link16 of a given number of nodes.
  //Doc:Arg1 Number of nodes in the LAN
  //Doc:Arg2 The IP Address of the first node in a given mask
  //Doc:Arg3 The Rate of the link.
  
  ~Link16(); // Destructor

  // Methods from Link
  //Doc:Method
  Link*      Copy() const ;     // Make a copy of this link
  //Doc:Desc This method makes a copy of itself and returns a pointer to
  //Doc:Desc the copy
  //Doc:Return Pointer to the new {\tt Link} object

  //Doc:Method
  bool       Transmit(Packet*, Interface*, Node*);
  //Doc:Desc Transmit a packet.
  //Doc:Arg1 The packet to transmit.
  //Doc:Arg2 A pointer to the local (transmitting) interface
  //Doc:Arg3 A pointer to the local (transmitting) node.
  //Doc:Return True if the transmit succeeded, false if packet was dropped.

  //Doc:Method
  virtual   bool     Busy() const;
    //Doc:Desc Determine if the link is  currently busy transmitting a packet.
    //Doc:Return True if link busy, false if not.

  //Doc:Method
  virtual Interface* GetNewInterface(IPAddr_t = IPADDR_NONE,
                                     Mask_t = MASK_ALL) const;
    //Doc:Desc Link16 TDM requires a Link16 interface type.
    //Doc:Arg1 IP Address for this interface.
    //Doc:Arg2 Address mask for this interface
    //Doc:Return New interface of type InterfaceLink16


  //Doc:Method
  void BoundingBox(const Location&, const Location&);
  // Arranges the nodes in a circle, centered in the bounding box
  //Doc:Arg1 Lower left corner of bounding box.
  //Doc:Arg2 Upper right corner of bounding box.

  //Doc:Method
  void SetRadioRange(Meters_t);
    //Doc:Desc Set the transmission range of every transmitter
    //Doc:Arg1     Range (meters);

  // Most of the remaining link.h methods are inherited from linkp2p
public:
  Count_t slotSize;  // Size of TDM slot (bits)
  Count_t nSlots;    // Number of TDM slots per cycle
  Time_t  slotTime;  // Time per slot
public:
  static Count_t DefaultSlotSize(){return defaultSlotSize;}
  static void    DefaultSlotSize(Count_t dfs) {defaultSlotSize = dfs;}
  static Count_t DefaultSlotCount() {return defaultSlotCount;}
  static void    DefaultSlotCount(Count_t dfc) {defaultSlotCount = dfc;}
  static Time_t  DefaultSlotTime(){return defaultSlotTime;}
  static void    DefaultSlotTime(Time_t dft){defaultSlotTime = dft;}
private:
  static Count_t defaultSlotSize;
  static Count_t defaultSlotCount;
  static Time_t  defaultSlotTime;
};

#endif


