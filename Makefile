
############################################################################
# Invoke using make QTDIR=<where QT is installed> , if QT is installed in  #
# non-standard places OR if you want a specific qt to be included          #
# $Id: Makefile 505 2006-08-24 15:57:45Z dheeraj $
############################################################################

#.SILENT:

SHELL           := /bin/sh
OSNAME		:= $(subst -,_,$(subst .,-,$(shell uname -s)))
ARCH            := $(shell uname -m)
MKDIR		:= $(shell which mkdir)
TOPSRCDIR	:= $(shell pwd)
TOPDIR		:= $(shell pwd)
SUPPORTED_OS	:= "SunOS Linux Darwin"
VERSION		:= 2.0

ifndef CXX
CXX             := $(shell which g++)
endif

export
CXXLOCATION     := $(shell echo `(which $(CXX))`)
NOTHING :=

SUBDIRS	+= SRC BRITE BGP EXAMPLES

.PHONY:		subdirs $(SUBDIRS)

all:
#	@echo "If you are trying a fresh installation of GTNetS"
#	@echo "please refer INSTALL-README accompanied with the package"
	@echo ""
	@echo "The Relevant make Targets are :: "
	@echo ""
	@echo "make depend         /* to build dependencies **MUST** */"
	@echo "make debug          /* Library with debug syms + examples */"
	@echo "make opt            /* Optimized Library with  + examples */"
	@echo "make clean          /* Clean up the object files */"
	@echo "make cleanall       /* Bring source tree to pristine state */"

subdirs:	$(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)

depend:	subdirs prebuild_sanity

prebuild_sanity:
ifeq ($(CXXLOCATION),)
	@echo "$(CXX) not found"
else
	@echo "$(CXX) found --  OK"
endif


clean:	subdirs
	@echo "Cleaning top-level libraries"
	$(RM) -f libGTNetS*.* libBrite*.* libBgp*.*

cleanall: subdirs
	@echo "Cleaning top-level libraries"
	$(RM) -f libGTNetS*.* libBrite*.* libBgp*.*

debug:	subdirs

opt:	subdirs

version:
	@echo "GTNetS Version : $(VERSION)"

testing:
	@echo "Make variables in this Makefile"
	@echo "MAKE        = $(MAKE)"
	@echo "CXX         = $(CXX)"
	@echo "SHELL       = $(SHELL)"
	@echo "OSNAME      = $(OSNAME)"
	@echo "ARCH        = $(ARCH)"
	@echo "MKDIR	   = $(MKDIR)"
	@echo "TOPSRCDIR   = $(TOPSRCDIR)"
	@echo "TOPDIR      = $(TOPDIR)"
	@echo "SUPPORTED_OS= $(SUPPORTED_OS)"
	@echo "SUBDIRS     = $(SUBDIRS)"
	@echo "MAKECMDGOALS= $(MAKECMDGOALS)"
	@echo "RM	   = $(RM)"
#libbgp-debug.a:
#	$(MAKE) -C BGP debug

#libbgp-opt.a:
#	$(MAKE) -C BGP opt


#dbglibs: debug

#optlibs: opt

# libbrite-debug.a:
# 	$(MAKE) -C BRITE debug

# libbrite-opt.a:
# 	$(MAKE) -C BRITE opt
