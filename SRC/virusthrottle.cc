// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: virusthrottle.cc 456 2006-01-20 17:37:47Z riley $



// Georgia Tech Network Simulator - VirusThrottle class
// Mohamed Abd Elhafez.  Georgia Tech, Spring 2004

// Implements the virus throttle algorithm for worm containment


#include <iostream>

#ifdef HAVE_QT
#include "qnamespace.h"
#include <qcolor.h>
#endif

#include "debug.h"
#include "virusthrottle.h"
#include "l2proto802.3.h"
#include "node.h"
#include "droppdu.h"
#include "hex.h"

using namespace std;

#define DELAY_QUEUE_FULL 100
#define WORKING_QUEUE_FULL 5
#define SENSITIVITY 1


// // Define the callback function
// bool MyCallback(Layer_t l, Proto_t p, PacketCallbacks::Type_t t,
//                 Packet* pkt, Node* n, Interface* iface)
// {
//   cout << "Hello from callback layer " << l << " proto " << p
//        << " type " << t 
//        << " iface " << iface
//        << endl;
//   if (l == 3 && t == PacketCallbacks::TX)
//     { // For testing, drop 10% of transmitted packets at l3
//       if (1)
//         { // Make a trace entry for this drop, drop the packet, return false
//           cout << "Callback dropping packet" << endl;
//           DropPDU d("L3-CB", pkt);
//           n->TracePDU(nil, &d);
//           delete pkt;
//           return false;
//         }
//     }
//   return true;
// }


unsigned int VirusThrottle::delayqueuelimit = DELAY_QUEUE_FULL;
unsigned int VirusThrottle::workingsetsize = WORKING_QUEUE_FULL;
unsigned int VirusThrottle::blocked = 0;
unsigned int VirusThrottle::falsedetection = 0;

VirusThrottle::VirusThrottle():  conn(true), scheduled(false)
{
  cout << "creating virus throttle " << endl;
}


void VirusThrottle::ProcessOutPacket( Packet* p, IPAddr_t i,  int type, Interface* iface)
{
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();
  if(iphdr->src != iface->GetIPAddr())
    {
      iface->Xmit(p, i, type);
      return;
    }

  if(iphdr->protocol == TCP_PROTOCOL)
    {
      TCPHeader* tcphdr = (TCPHeader*)p->PeekPDU(1);
      if(tcphdr->flags != TCP::SYN)
	{
	  // cout << "VirusThrottle:: not SYN packet" << endl;
	  iface->Xmit(p, i, type);
	  return;
	}
      Delay(iphdr, p, i, type, iface);
    }
  else if(iphdr->protocol == UDP_PROTOCOL)
    {
      //cout << "VirusThrottle:: udp protocol is used " << endl;
      Delay(iphdr, p, i, type, iface);
    }
}


  void VirusThrottle::Delay(IPV4Header* iphdr, Packet* p, IPAddr_t i, int type, Interface* iface)
    {
      if(conn == false)
	{
	  //cout << "VirusThrottle:: connection is blocked" << endl;
	  //cout << "VirusThrottle queue size =  " << delayqueue.size() << endl;
	  // Dropping packet
	  //	  cout << "dropping packet " << p->uid << endl;
	  DropPDU d("L3-VT", p);
	  iface->GetNode()->TracePDU(nil, &d);
	  DEBUG(0,(cout << "Packet Dropped " << endl));
	  Stats::pktsDropped++;
	  delete p;
	  
	  return;
	}
      if( InWorkingSet(iphdr->dst) )
	{
	  iface->Xmit(p, i, type);
	  return;
	}
      if(workingset.size() < workingsetsize)
	{
	  workingset.push_back(iphdr->dst);
	  iface->Xmit(p, i, type);
	  return;
	}
       
    //   cout << "VirusThrottle:: working set Contents ------------" << endl;
//       for (int j = 0; j < workingset.size(); j++)
// 	{
// 	  cout << IPAddr::ToDotted(workingset[j]) << endl;

// 	}
      //	cout << "VirusThrottle:: Queuing packet " << p->uid << endl;
      PacketInfo pinfo = PacketInfo();
      pinfo.type = type;
      pinfo.nexthop = i;
      delayqueue.push(PacketPairInfo_t(p, pinfo));
      if(delayqueue.size() == delayqueuelimit)
	{
	  conn = false;
	  blocked++;

	    // this is plain wrong. need fixing   
#ifdef HAVE_QT 
	    // this is just a hack to find out if the node is really infected or not
	    QColor col = iface->GetNode()->Color();
	    if( col == Qt::red)
	      {
		cout << "VirusThrottle:: Worm is really infected" << endl;
	      }
	    else
	      {
		cout << "VirusThrottle:: False detection" << endl;
		falsedetection++;
	      }
	    
	    // try to turn the blocked node yellow
	    iface->GetNode()->Color(Qt::yellow);
#endif
	    return;
	  }
	// Schedule the next event
      if(!delayqueue.empty() && !scheduled)
	{
	  scheduled = true;
	  //cout << "VirusThrottle:: Scheduling process queue event" << endl;
	  //cout << "VirusThrottle queue size =  " << delayqueue.size() << endl;
	  //Scheduler::Schedule(new ThrottleEvent(ThrottleEvent::PROCESS_QUEUE, i, iface), pow((double)2,(int)delayqueue.size()) * SENSITIVITY, this);
	  Scheduler::Schedule(new ThrottleEvent(ThrottleEvent::PROCESS_QUEUE, iface), SENSITIVITY, this);
		  
	}      
    }


  
void VirusThrottle::Handle(Event* e, Time_t t)
{
  ThrottleEvent* te = (ThrottleEvent*)e;
  IPV4Header* iphdr ;

  PacketPairInfo_t pp;
  switch(te->event) {
  case ThrottleEvent::PROCESS_QUEUE :
    if(workingset.size() == workingsetsize)
      {
	//cout << "VirusThrottle working set queue size =  " << workingset.size() << endl;
	workingset.erase(workingset.begin());
      }
    if(!delayqueue.empty() && conn)
      {
	pp = delayqueue.front();
	iphdr = (IPV4Header*)pp.first->PeekPDU();
	te->iface->Xmit(pp.first, pp.second.nexthop, pp.second.type);
	workingset.push_back(iphdr->dst);
	delayqueue.pop();

	//Scheduler::Schedule(new ThrottleEvent(ThrottleEvent::PROCESS_QUEUE, te->dst, te->iface), pow((double)2, (int)delayqueue.size())*SENSITIVITY, this);
	Scheduler::Schedule(new ThrottleEvent(ThrottleEvent::PROCESS_QUEUE,  te->iface), SENSITIVITY, this);
      }
    else
      {
	scheduled = false;
      }
    break;
  default:
    break;
  }
  delete te;
}



bool VirusThrottle::InWorkingSet(IPAddr_t ipaddr)
{
  bool found = false;
  int size = workingset.size();

  for(int i = 0; i < size; i++)
    {
      if( workingset[i] == ipaddr )
	{
	  found = true;
	  break;
	}
    }
  return found;
}
