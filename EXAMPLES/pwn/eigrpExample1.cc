// Test the EIGRP protocol with animation.
// George F. Riley, Talal Jaafar, Georgia Tech, Spring 2004

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "star.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "eigrp.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "tcp-tahoe.h"
#include "routing-eigrp.h"

#ifdef HAVE_QT
#include <qnamespace.h>
#endif

#define N_NODES 7
#define N_ROUTERS 5

using namespace std;

int main(int argc, char** argv)
{
  Simulator s;
  
  // Set to eigrp routing
  Routing::SetRouting(new RoutingEIGRP());
  
  bool noAnim = true;
 
  if (argc > 1) noAnim = atol(argv[1]);

  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);
  
  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->Open("eigrpExample1.txt");
  Linkp2p lk(Rate("500Kb"), Time("20ms"));
  
  
  // Random number generator for start times
  Uniform startRng(0, 0.1);

  Node* node[N_NODES];
  EIGRP* EIGRProuter[N_ROUTERS];

  // Create the routers
  for (Count_t i = 0; i < N_ROUTERS; ++i)
    {
      Node* l = new Node(); 
      // Create an eigrp protocol at the leaf
      EIGRP* e = new EIGRP();
      //e->SetLossProbability(0.5); // For debugging reliable mcast
      e->AttachNode(l);   // binds the layer 4 protocol to this node
      e->SetTrace(Trace::ENABLED); // Trace EIGRP actions at hub
      //e->StartAt(startRng.Value());
      e->StartAt((i+1)*0.1);
      EIGRProuter[i] = e;
      node[i] = l;
    }
  
  node[5] = new Node();
  node[6] = new Node();


  // Set the node locations
  node[0]->SetLocation(4,0);
  node[1]->SetLocation(12,0);
  node[2]->SetLocation(8,4);
  node[3]->SetLocation(4,8);
  node[4]->SetLocation(12,8);
  node[5]->SetLocation(0,0);
  node[6]->SetLocation(16,8);

  // create the links between the routers
  node[0]->AddDuplexLink(node[1], lk, IPAddr("10.1.1.1"), Mask(24), IPAddr("10.1.1.2"), Mask(24));
  
  node[0]->AddDuplexLink(node[2], lk, IPAddr("10.1.2.1"), Mask(24), IPAddr("10.1.2.2"), Mask(24));
 
  node[1]->AddDuplexLink(node[2], lk, IPAddr("10.1.3.1"), Mask(24), IPAddr("10.1.3.2"), Mask(24));
  
  node[2]->AddDuplexLink(node[3], lk, IPAddr("10.1.4.1"), Mask(24), IPAddr("10.1.4.2"), Mask(24)); 
 
  node[2]->AddDuplexLink(node[4], lk, IPAddr("10.1.5.1"), Mask(24), IPAddr("10.1.5.2"), Mask(24)); 
  
  node[3]->AddDuplexLink(node[4], lk, IPAddr("10.1.6.1"), Mask(24), IPAddr("10.1.6.2"), Mask(24)); 

  node[0]->AddDuplexLink(node[5], lk, IPAddr("10.1.7.1"), Mask(24), IPAddr("10.1.7.2"), Mask(24));

  node[4]->AddDuplexLink(node[6], lk, IPAddr("10.1.8.1"), Mask(24), IPAddr("10.1.8.2"), Mask(24));

  // Set the links metrics
  EIGRProuter[0]->SetMetric(0,IPAddr("10.1.1.1"),4);  
  EIGRProuter[1]->SetMetric(0,IPAddr("10.1.1.2"),4);

  EIGRProuter[0]->SetMetric(0,IPAddr("10.1.2.1"),2);  
  EIGRProuter[2]->SetMetric(0,IPAddr("10.1.2.2"),2);

  EIGRProuter[1]->SetMetric(0,IPAddr("10.1.3.1"),1);  
  EIGRProuter[2]->SetMetric(0,IPAddr("10.1.3.2"),1);

  EIGRProuter[2]->SetMetric(0,IPAddr("10.1.4.1"),20); 
  EIGRProuter[3]->SetMetric(0,IPAddr("10.1.4.2"),20);

  EIGRProuter[2]->SetMetric(0,IPAddr("10.1.5.1"),1);  
  EIGRProuter[4]->SetMetric(0,IPAddr("10.1.5.2"),1);

  EIGRProuter[3]->SetMetric(0,IPAddr("10.1.6.1"),1);  
  EIGRProuter[4]->SetMetric(0,IPAddr("10.1.6.2"),1);
  
  
  // Metric change
  EIGRProuter[0]->SetMetric(2,IPAddr("10.1.2.1"),10);
  EIGRProuter[2]->SetMetric(2,IPAddr("10.1.2.2"),10);
  
  
  if (noAnim)
    {
      // Specify animation
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Progress(1);
  s.StopAt(5);
  cout << "Starting Simulation" << endl;
  s.Run();
  cout << "Simulation complete" << endl;
}
