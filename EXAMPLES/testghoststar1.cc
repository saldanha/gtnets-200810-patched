// Test the GhostNode distributed simulation
// Talal M. Jaafar, Georgia Tech, Fall 2003

#include <iostream>

#include "simulator.h"
#include "distributed-simulator.h"
#include "scheduler.h"
#include "node.h"
#include "node-real.h"
#include "linkp2p.h"
#include "ratetimeparse.h"
#include "routing-static.h"
#include "globalstats.h"
#include "mask.h"
#include "ipv4.h"
#include "routing-nixvector.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "tcp-tahoe.h"
#include "star.h"
#include "hex.h"
#include "validation.h"

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  if (argc < 3)
    {
      cout << "Usage: testghoststar myid nLeaf" << endl;
      exit(1);
    }
  
  unsigned long sys_id = atoi(argv[1]);
  unsigned long n_leaf = atoi(argv[2]);
  Count_t flowCount = 10;
  Count_t nbytes = 1000000;
  
  if (argc > 3) flowCount = atol(argv[3]);
  if (argc > 4) nbytes = atol(argv[4]);
  cout << "nbytes " << nbytes << endl;
  
  //DistributedSimulator s(sys_id);
  Simulator s;
  
  Routing::SetRouting(new RoutingNixVector());
  //Routing::SetRouting(new RoutingStatic());
  
  // create and enable IP packet tracing if TCP
  Trace* tr = Trace::Instance();
  tr->IPDotted(true);
 //  if(sys_id == 0)
//     tr->Open("testghoststar0.txt");
//   else if(sys_id == 1)
//     tr->Open("testghoststar1.txt");
//   else if(sys_id == 2)
//     tr->Open("testghoststar2.txt");
//   else if(sys_id == 3)
//     tr->Open("testghoststar3.txt");
//   else if(sys_id == 4)
//     tr->Open("testghoststar4.txt");
//   else if(sys_id == 5)
//     tr->Open("testghoststar5.txt");
//   else if(sys_id == 6)
//     tr->Open("testghoststar6.txt");
//   else if(sys_id == 7)
//     tr->Open("testghoststar7.txt");

  TCP::LogFlagsText(true);
  IPV4::Instance()->SetTrace(Trace::ENABLED);
  //tr->Open("testghoststar.txt");

  Linkp2p link(Rate("10Mb"), Time("10ms"));
  Linkp2p rlink(Rate("100Mb"), Time("1ms"));

  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);
  
  // Create the stars
  Star star0(n_leaf, link, IPAddr("192.168.1.1"),0);
  star0.GetHub()->SetLocation(Location(10, 10));    // Assign a location to the hub
  //star0.BoundingBox(Location(0,0), Location(10,10));// Specify the bounding box
  cout << "Created star 0" << endl;

  Star star1(n_leaf, link, IPAddr("192.170.1.1"),0);
  star1.GetHub()->SetLocation(Location(10, 10));
  //star1.BoundingBox(Location(0,0), Location(10,10));
  cout << "Created star 1" << endl;

  Star star2(n_leaf, link, IPAddr("192.172.1.1"),0);
  star2.GetHub()->SetLocation(Location(10, 10));
  //star2.BoundingBox(Location(0,0), Location(10,10));
  cout << "Created star 2" << endl;

  Star star3(n_leaf, link, IPAddr("192.174.1.1"),0);
  star3.GetHub()->SetLocation(Location(10, 10));
  //star3.BoundingBox(Location(0,0), Location(10,10));
  cout << "Created star 3" << endl;

  Star star4(n_leaf, link, IPAddr("192.176.1.1"),0);
  star4.GetHub()->SetLocation(Location(10, 10));
  //star4.BoundingBox(Location(0,0), Location(10,10));
  cout << "Created star 4" << endl;

  Star star5(n_leaf, link, IPAddr("192.178.1.1"),0);
  star5.GetHub()->SetLocation(Location(10, 10));
  //star5.BoundingBox(Location(0,0), Location(10,10));
  cout << "Created star 5" << endl;

  Star star6(n_leaf, link, IPAddr("192.180.1.1"),0);
  star6.GetHub()->SetLocation(Location(10, 10));
  //star6.BoundingBox(Location(0,0), Location(10,10));
  cout << "Created star 6" << endl;

  Star star7(n_leaf, link, IPAddr("192.182.1.1"),0);
  star7.GetHub()->SetLocation(Location(10, 10));
  //star7.BoundingBox(Location(0,0), Location(10,10));
  cout << "Created star 7" << endl;

  // Get the hubs of all the stars
  Node* hub0 = star0.GetHub();
  Node* hub1 = star1.GetHub();
  Node* hub2 = star2.GetHub();
  Node* hub3 = star3.GetHub();
  Node* hub4 = star4.GetHub();
  Node* hub5 = star5.GetHub();
  Node* hub6 = star6.GetHub();
  Node* hub7 = star7.GetHub();


  //create the remote link between the hubs of the stars
  hub0->AddDuplexLink(hub1, rlink, IPAddr("192.169.1.1"), Mask(24), IPAddr("192.169.1.2"), Mask(24));

  hub1->AddDuplexLink(hub2, rlink, IPAddr("192.171.1.1"), Mask(24), IPAddr("192.171.1.2"), Mask(24));

  hub2->AddDuplexLink(hub3, rlink, IPAddr("192.173.1.1"), Mask(24), IPAddr("192.173.1.2"), Mask(24));

  hub3->AddDuplexLink(hub4, rlink, IPAddr("192.175.1.1"), Mask(24), IPAddr("192.175.1.2"), Mask(24));

  hub4->AddDuplexLink(hub5, rlink, IPAddr("192.177.1.1"), Mask(24), IPAddr("192.177.1.2"), Mask(24));

  hub5->AddDuplexLink(hub6, rlink, IPAddr("192.179.1.1"), Mask(24), IPAddr("192.179.1.2"), Mask(24));

  hub6->AddDuplexLink(hub7, rlink, IPAddr("192.181.1.1"), Mask(24), IPAddr("192.181.1.2"), Mask(24));
  
  hub7->AddDuplexLink(hub0, rlink, IPAddr("192.183.1.1"), Mask(24), IPAddr("192.183.1.2"), Mask(24));

  // Get the leaf nodes that are to be used as Servers and Clients
  Node* s0 = star0.GetLeaf(0);
  Node* c0[flowCount];
  for(Count_t i=0; i < flowCount; i++)
    {
      c0[i] = star0.GetLeaf(i+1);
    }

  Node* c1[flowCount];
  for(Count_t i=0; i < flowCount; i++)
    {
      c1[i] = star1.GetLeaf(i+1);
    }

  Node* s4 = star4.GetLeaf(0);
  Node* c4[flowCount];
  for(Count_t i=0; i < flowCount; i++)
    {
      c4[i] = star4.GetLeaf(i+1);
    }

  Node* s7 = star7.GetLeaf(0);

  for(Count_t i=0; i < flowCount; i++)
      {
        TCPSend* ts = (TCPSend*)c0[i]->AddApplication(
            TCPSend(s4->GetIPAddr(),80, Constant(nbytes), TCPTahoe()));
        if (ts) ts->Start(0.01 * i); 
      }

  // create a TCP server and attach it to leaf node 0 of star0
  // Server for client in star4
  TCPServer* tcpApp0 = (TCPServer*)s0->AddApplication(TCPServer());
  if (tcpApp0) tcpApp0->BindAndListen(s0, 80);

  for(Count_t i=0; i < flowCount; i++)
    {
      TCPSend* ts = (TCPSend*)c1[i]->AddApplication(
          TCPSend(s4->GetIPAddr(),80, Constant(nbytes), TCPTahoe()));
      if (ts) ts->Start(0.01 * i); 
    }  

  // create a TCP server and attach it to leaf node 0 of star4
  // Server for clients in star0 and star1    
  TCPServer* tcpApp4 = (TCPServer*)s4->AddApplication(TCPServer());
  if (tcpApp4) tcpApp4->BindAndListen(s4, 80);

  // create a TCP sending application and attach it to leaf node 1, node 2, node 3 of star4   
  for(Count_t i=0; i < flowCount; i++)
    {
      TCPSend* ts = (TCPSend*)c4[i]->AddApplication(
          TCPSend(s7->GetIPAddr(),80, Constant(nbytes), TCPTahoe()));
      if(ts) ts->Start(0.01 * i);
    }

  // create a TCP server and attach it to leaf node 0 of star7
  // Server for client in star4
  TCPServer* tcpApp7 = (TCPServer*)s7->AddApplication(TCPServer());
  if (tcpApp7) tcpApp7->BindAndListen(s7, 80);

  s.Progress(1.0);
  s.StopAt(500.0);
  //s.StartAnimation(0, true);
  //s.AnimationUpdateInterval(Time("1ms")); // 1ms initial update rate
 
  s.Run();

  cout << "Used memory is: " << s.ReportMemoryUsageMB() << endl;
  Stats::Print(); // Print the statistics
  cout << "Apps started " << TCPSend::totalStarted << endl;
  cout << "Apps ended   " << TCPSend::totalEnded << endl;
  cout << "db1 " << NodeReal::count1 << endl;
  cout << "db2 " << NodeReal::count2 << endl;
  cout << "db3 " << NodeReal::count3 << endl;
  cout << "db4 " << NodeReal::count4 << endl;

  cout << "Memory usage after run " 
       << s.ReportMemoryUsageMB() << "MB" << endl;
  cout << "Total events processed "
       << Scheduler::Instance()->TotalEventsProcessed() << endl;
  cout << "Total pkt-hops " << Stats::pktsTransmitted << endl;
  cout << "Setup time " << s.SetupTime() << endl;
  cout << "Route time " << s.RouteTime() << endl;
  cout << "Run time "   << s.RunTime() << endl;
  cout << "Total time "   << s.TotalTime() << endl;

  tr->Close();
  //MyStat::Print();
  cout << "Simulation complete" << endl;
}
