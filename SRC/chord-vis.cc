/*
 * Chord Visualization
 * Author: Sven Krasser
 * $Id: chord-vis.cc 446 2005-11-15 20:48:03Z riley $
 *
 */
 
// GENERAL PUBLIC LICENSE AGREEMENT
//
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
//
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
//
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
//
// 1.  This License allows you to:
//
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
//
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//
//
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
//
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
//
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
//
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
//
//
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
//
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
//
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
//
//
// 3.  Export Law Assurance.
//
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//
// 4.  Termination.
//
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
//
//
// 5.  Disclaimer of Warranties and Limitation on Liability.
//
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
//
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
//
// GTRC shall have no obligation for support or maintenance of Program.
//
// 6.  Copyright Notice.
//
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
//
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
//
// Said copyright notice shall read as follows:
//
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved



#include "chord-vis.h"
#include "application-chord.h"
#include "simulator.h"

#include <iostream>
using namespace std;

#ifdef HAVE_QT
#include <qcanvas.h>
#endif

ChordVisData::ChordVisData() {
#ifdef HAVE_QT
	flines = new QCanvasLine*[ChordId::GetBitLength()];
#endif
}

ChordVisData::~ChordVisData() {
#ifdef HAVE_QT
	if(rect) {
		rect->hide();
		delete rect;
	}
	if(flines) {
		int i;
		for(i = 0; i < ChordId::GetBitLength(); i++) {
			flines[i]->hide();
			delete flines[i];
		}
		delete[] flines;				
	}
#endif
}

		
ChordVis::ChordVis() : QTChildWindow(), ChordStats() {
#ifdef HAVE_QT
	SetCaption(string("Chord"));
	ring = 0;
	w = width;
	h = height;
#endif
}

void ChordVis::AddChord(Chord *ch) {
	ChordStats::AddChord(ch);
#ifdef HAVE_QT
	if(!ring) { // Code for the first node to join
		ring = new QCanvasEllipse(canvas);
		ring->setSize((int)(0.8 * w), (int)(0.8 * h));
		ring->setX(w/2);
		ring->setY(h/2);
		ring->setZ(0);
		ring->setBrush(Qt::cyan);
		ring->show();
		
		// Unfortunately, Qt doesn't provide a drawing primitive for an
		// ellipse outline... But this will do the job too:
		innerring = new QCanvasEllipse(canvas);
		innerring->setSize((int)(0.8 * w) - 10, (int)(0.8 * h) - 10);
		innerring->setX(w/2);
		innerring->setY(h/2);
		innerring->setZ(1);
		innerring->setBrush(Qt::white);
		innerring->show();
		
	}
	ChordVisData *vd = new ChordVisData();
	vd->rect = new QCanvasRectangle(canvas);
	vd->rect->setX(GetX(ch->my_id));
	vd->rect->setY(GetY(ch->my_id));
	vd->rect->setZ(3);
	vd->rect->setSize(4, 4);
	vd->rect->show();
	
	int i;
	for(i = 0; i < ChordId::GetBitLength(); i++) {
		vd->flines[i] = new QCanvasLine(canvas);
		vd->flines[i]->setZ(2);
		vd->flines[i]->setPen(Qt::black);
	}

	visdata[ch] = vd;
	UpdateFingerLines(ch);
	
#endif
}

void ChordVis::UpdateFingerLines(Chord *ch) {
#ifdef HAVE_QT
	int i;
	ChordFinger f;
	ChordId cid, mid = ch->my_id;
	ChordVisData *vd = visdata[ch];
	int ax, ay, bx, by;
	
	for(i = 0; i < ChordId::GetBitLength(); i++) {
		f = ch->finger_table.GetFinger(i + 1);
		if(f.IsSet()) {
			cid = f.GetId();

			ax = GetX(mid);
			ay = GetY(mid);
			bx = GetX(cid);
			by = GetY(cid);
			
			vd->flines[i]->setPoints(ax, ay, bx, by);
			vd->flines[i]->show();
		} else {
			vd->flines[i]->hide();
		}			
	}
#endif
}

void ChordVis::RemoveChord(Chord *ch) {
#ifdef HAVE_QT
	ChordStats::RemoveChord(ch);
	delete visdata[ch];
	visdata.erase(ch);
#endif
}

double ChordVis::GetAngle(ChordId cid) {
	double angle = 0;
#ifdef HAVE_QT
	if(ChordId::GetBitLength() == 160) {
		angle = (double)cid.id[0] / 4294967296.0; // id / 2^32
	} else if(ChordId::GetBitLength() <= 31) {
		angle = (double)cid.id[4] / ((1 << ChordId::GetBitLength()));
	} else {
		cout << "Warning: Visualization is only implemented for identifier lengths 160 and 1..31." << endl;
		angle = 0.0;
	}
	angle *= 2 * 3.1415;
#endif
	return angle;
}

int ChordVis::GetX(ChordId cid) {
#ifdef HAVE_QT
	double angle = GetAngle(cid);
	return w / 2 + (int)(0.5 * 0.8 * (double)w * sin(angle));
#else
        return 0;
#endif
}

int ChordVis::GetY(ChordId cid) {
#ifdef HAVE_QT
	double angle = GetAngle(cid);
	return h / 2 - (int)(0.5 * 0.8 * (double)h * cos(angle));
#else
        return 0;
#endif
}

void ChordVis::Update()
{
#ifdef HAVE_QT
	QTChildWindow::Update();
	if(ring) {
		ring->setSize((int)(0.8 * w), (int)(0.8 * h));
		ring->setX(w/2);
		ring->setY(h/2);
		innerring->setSize((int)(0.8 * w) - 10, (int)(0.8 * h) - 10);
		innerring->setX(w/2);
		innerring->setY(h/2);
	}
	
	map<Chord*, ChordVisData*>::iterator visdata_iterator = visdata.begin();
	while(visdata_iterator != visdata.end()) {
		UpdateFingerLines(visdata_iterator->first);
		visdata_iterator++;
	}	
	
	canvas->update();
#endif
}

void ChordVis::resizeEvent(QResizeEvent* e)
{
#ifdef HAVE_QT
	w = e->size().width();
	h = e->size().height();
	
	map<Chord*, ChordVisData*>::iterator visdata_iterator = visdata.begin();
	while(visdata_iterator != visdata.end()) {
		ChordVisData *vd = (*visdata_iterator).second;
		Chord* ch = (*visdata_iterator).first;
		vd->rect->setX(GetX(ch->my_id));
		vd->rect->setY(GetY(ch->my_id));
		visdata_iterator++;
	}
	
	if(canvas && view) {
		view->resize(w + 16, h + 16);
		canvas->resize(w, h);
		canvas->update();
		view->show();
		show();
	}
#endif
}
