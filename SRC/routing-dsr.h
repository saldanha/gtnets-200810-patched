// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: routing-dsr.h 446 2005-11-15 20:48:03Z riley $



#ifndef __routing_dsr_h
#define __routing_dsr_h

#include "routing.h"
#include "ipv4.h"
#include "ipaddr.h"
#include "timer.h"
#include "notifier.h"

#define PROTO_DSR		0x8FE   // DSR protocol number

#define MAX_SR_LEN		16
#define SEND_BUF_SIZE		64
#define MAX_ERR_HOLD		1.0     // 1 sec

#define ROUTEREQUEST_ID		2
#define ROUTEREPLY_ID		3
#define ROUTEERROR_ID		4
#define SOURCEROUTE_ID		96

// ROUTEERROR_ID
#define NODE_UNREACHABLE		1
#define FLOW_STATE_NOT_SUPPORTED	2
#define OPTION_NOT_SUPPORTED		3

// Constants from IETF DSR internet draft
// draft-ietf-manet-dsr-08.txt
#define BroadcastJitter		0.01    // 10 msec
#define ROUTE_CACHE_TIMEOUT	300     // 300 sec
#define SendBufferTimeout	30      // 30 sec

#define RequestTableSize	64      // nodes
#define RequestTableIds		16      // identifiers
#define MaxRequestRexmt		16      // retransmissions
#define MaxRequestPeriod	10      // 10 sec
#define RequestPeriod		0.5     // 500 msec
#define NonpropRequestTimeout	0.03    // 30 msec

#define RexmtBufferSize		50      // packets
#define MainHoldoffTime		0.25    // 250 msec
#define MaxMaintRexmt		2       // retransmissions

#define TryPassiveAcks		1       // attempt
#define PassiveAckTimeout	0.1     // 100 msec
#define GratReplyHoldoff	1       // 1 sec

#define MAX_SALVAGE_COUNT	15      // salvages
// draft-ietf-manet-dsr-08.txt

// Sets whether IP Address in routes are printed in dotted
// notation or not, must bu un-dotted when using ROUTE_MASK
#define ROUTE_DOTTED 0
// Used when printing routes for debug and trace
// 0xFF prints only last octet of an IP Address
#define ROUTE_MASK 0xFF


class Node;
class RouteRequestOption;
class DSREvent;
class Uniform;

typedef std::vector<IPAddr_t>     SourceRoute_t;

class RtCacheEntry {
public:
  RtCacheEntry(const SourceRoute_t& r) : route(r), lt(ROUTE_CACHE_TIMEOUT) {}

public:
  SourceRoute_t  route;
  Time_t         lt;    // lifetime
};

//typedef std::list<SourceRoute_t> RtCache_t;
typedef std::list<RtCacheEntry> RtCache_t;

// RequestEntry
class RequestEntry {
public:
  RequestEntry() : src(0), dst(0), id(0), ttl(0), timeout(0), timestamp(0),
                   timer(nil), prop(0), used(0) {}
  RequestEntry(IPAddr_t s, IPAddr_t d, Word_t i, Byte_t t, Time_t to,
               Time_t ts, DSREvent* e)
      : src(s), dst(d), id(i), ttl(t), timeout(to), timestamp(ts), timer(e),
        prop(0), used(1) {}

public:
  IPAddr_t   src;
  IPAddr_t   dst;
  Word_t     id;
  Byte_t     ttl;
  Time_t     timeout;
  Time_t     timestamp;
  DSREvent*  timer;
  bool       prop;
  bool       used;
};

// RReqQueItem
class RReqQueItem {
public:
  RReqQueItem() : p(nil), timer(nil) {}
  RReqQueItem(Packet* pkt, DSREvent* e) : p(pkt), timer(e) {}

public:
  Packet*    p;
  DSREvent*  timer;
};

// SendBufEntry
class SendBufEntry {
public:
  SendBufEntry(Packet* pkt, Time_t ts) : p(pkt), t(ts) {}

public:
  Packet*  p;    // L3 packet
  Time_t   t;    // timestamp
};

// DSREvent
class DSREvent : public TimerEvent
{
public:
  typedef enum {
      REQUEST_TIMEOUT, BROADCAST_JITTER, SENDBUF, BUF_CHECK, ROUTE_CACHE
  } DSRTimeout_t;

public:
  DSREvent(Event_t ev, int i=-1) : TimerEvent(ev), h(i) {}

public:
  int      h;    // index of associated item in a table
};

// DSROptionsHeader
class DSROptionsHeader : public IPV4Options
{
public:
  DSROptionsHeader();
  DSROptionsHeader(const DSROptionsHeader& r);
  virtual ~DSROptionsHeader();
  Size_t Size() const;
  PDU* Copy() const { return new DSROptionsHeader(*this); }
  void Trace(Tfstream&, Bitmap_t);

public:
  enum { Number=26 };
  //Byte_t  nextHeader;    // replaced by IPV4Options::optionNumber
  //Byte_t  reserved;
  //Word_t  payloadLength;
  PDUVec_t  options;       // DSR options
};

// DSROptions
class DSROptions : public PDU
{
public:
  DSROptions(Byte_t t) : type(t) {}
  virtual Size_t Size() const { return 2; }
  virtual PDU* Copy() const { return new DSROptions(*this); }
  virtual void Trace(Tfstream&, Bitmap_t) {}

public:
  Byte_t  type;         // option type
  //Byte_t  dataLength;   // option length
};

// RouteRequestOption
class RouteRequestOption : public DSROptions
{
public:
  RouteRequestOption(Word_t id, IPAddr_t d, IPAddr_t s);
  Size_t Size() const;
  PDU* Copy() const { return new RouteRequestOption(*this); }

public:
  void  AddHop(IPAddr_t current) { routeRecord.push_back(current); }
  bool  IsIPAdded(IPAddr_t current);
  IPAddr_t GetSrc() { return src; }
  void Trace(Tfstream&, Bitmap_t);

public:
  Word_t  seqID;     // identification
  IPAddr  dst;       // target address
  IPAddr  src;       // originator address, not part of this option
  SourceRoute_t  routeRecord;
};

// RouteReplyOption
class RouteReplyOption : public DSROptions
{
public:
  RouteReplyOption(SourceRoute_t route, Word_t id);
  Size_t Size() const;
  PDU* Copy() const { return new RouteReplyOption(*this); }
  void Trace(Tfstream&, Bitmap_t);

public:
  SourceRoute_t& GetRoute()    { return recordedRoute; }
  IPAddr_t       GetRouteDst() { return recordedRoute.back(); }

public:
  SourceRoute_t recordedRoute;
  Word_t        seqID;
};

// RouteErrorOption
class RouteErrorOption : public DSROptions
{
public:
  RouteErrorOption(Byte_t e, IPAddr_t s, IPAddr_t d);
  virtual Size_t Size() const;
  virtual PDU* Copy() const { return new RouteErrorOption(*this); }
  void Trace(Tfstream&, Bitmap_t);

public:
  Byte_t  errorType;
  Byte_t  salvage;
  IPAddr  errorSrc;
  IPAddr  errorDst;
  Long_t  info;        // type specific information
};

// SourceRouteOption
class SourceRouteOption : public DSROptions
{
public:
  SourceRouteOption(SourceRoute_t sr);
  Size_t Size() const;
  PDU* Copy() const { return new SourceRouteOption(*this); }
  void Trace(Tfstream&, Bitmap_t);

public:
  RoutingEntry LookupNextHop(Node* current);
  IPAddr_t GetDst()  { return route.back(); }

public:
  Byte_t  salvage;    // 4 bits
  Byte_t  segsLeft;   // 6 bits
  SourceRoute_t route;
  int     hopIndex;
};

// RouteCache
class RouteCache
{
public:
  RouteCache() : cacheSize(0) {}
  virtual ~RouteCache() {}

  void  AddRoute(const SourceRoute_t&);
  void  RemoveRoute(Node*, IPAddr_t, IPAddr_t);
  bool  FindRoute(IPAddr_t, RtCache_t::iterator&, SourceRoute_t::size_type&);
  void  Maintain();

public:
  RtCache_t  cache;
  Word_t     cacheSize;
};

// RoutingDSR
class RoutingDSR : public Routing, public TimerHandler, public NotifyHandler
{
public:
  RoutingDSR();
  virtual ~RoutingDSR();
  void Default(RoutingEntry r) {}
  void Add( IPAddr_t, Count_t, Interface*, IPAddr_t) {}
  RoutingEntry Lookup(Node*, IPAddr_t) { return RoutingEntry(); }
  RoutingEntry LookupFromPDU(PDU*) { return RoutingEntry(); }
  Routing* Clone() { return new RoutingDSR(); }
  RType_t Type() { return DSR; }
  Size_t  Size() const { return 0; }
  Proto_t Proto() { return PROTO_DSR; }
  void Notify(void*);    // L2 xmit failure notification handler
  RoutingEntry GetDefault() { return RoutingEntry(); }

public:
  void DataRequest(Node*, Packet*, void*);
  bool DataIndication(Interface*, Packet*);
  void L3Transmit(Node*, Packet*, IPAddr_t, Count_t, Proto_t,
                  DSROptionsHeader*);
  void Broadcast(Interface*, Packet*);
  void Unicast(RoutingEntry&, Packet*);

  // Source Routing Functions
  SourceRouteOption* GetSourceRoute(IPAddr_t, bool=false);
  void AddSourceRoute(const SourceRoute_t&, bool=true);

  // Route Discovery Functions
  void SendRouteRequest(IPAddr, Count_t, Time_t, bool=true);
  bool IsRequestPending(IPAddr_t);
  int  GetSeqID() { return seqID++; }

private:
  // Route Discovery Functions
  bool IsRecentRequest(RouteRequestOption*);
  void AddRequest(RouteRequestOption*, Count_t, Time_t, bool=true);
  void AddBroadcastJitter(Packet*, Time_t);

  // Route Maintenance
  void RemoveRoute(IPAddr_t ip1, IPAddr_t ip2);

  // Send Buffer
  void BufferPacket(Packet* p);
  void FlushBuffer(IPAddr_t dst);
  void FlushBufferPeriod(IPAddr_t dst);
  void FlushBufferPeriod();
  void CheckSendBuffer();
  bool FindSendBuffer(IPAddr_t);

  // Timer Functions
  void Timeout(TimerEvent*);
  void ScheduleTimer(Event_t, DSREvent*&, Time_t);
  void CancelRequestTimer(IPAddr_t);
  void CancelTimer(DSREvent*&, bool=false);

  // Packet Handling
  bool ProcessRouteRequest(Node*, Packet*);
  bool ProcessSourceRoute(Node*, Packet*, DSROptionsHeader*);
  void FormErrorPacket(Node*, Packet*);
  void ProcessSnoopedPacket(Node*, Packet*, DSROptionsHeader*);
  void ProcessRouteError(RouteErrorOption*);
  void DataSend(Packet*, SourceRouteOption* sr=nil);
  IPV4Header* AddIPHeader(Packet*, IPAddr_t, Count_t, Proto_t);

public:
  static SourceRoute_t ReverseRoute(SourceRoute_t&);
  static void PrintRoute(const SourceRoute_t&);

  static bool    enableL2Notify;
  static bool    enableRingSearch;
  static Word_t  defaultSendBufSize;
  static Time_t  defaultBufCheck;
  static Time_t  defaultSendBufPeriod;
  static Word_t  defaultPCacheSize;
  static Word_t  defaultSCacheSize;

private:
  typedef std::vector<RequestEntry> RequestTable_t;
  typedef std::vector<RReqQueItem>  RReqQue_t;
  typedef std::list<SendBufEntry>   SendBuf_t;

  int            seqID;             // route request id
  RouteCache     primaryCache;      // primary path cache
  RouteCache     secondaryCache;    // secondary path cache
  RequestTable_t rreqTable;         // route request table
  RReqQue_t      rreqQue;           // delayed route request que
  SendBuf_t      sendBuffer;        // send buffer

  Timer          timer;
  Uniform*       urvJitter;
  DSREvent*      evSendBuf;
  DSREvent*      evBufCheck;
  DSREvent*      evRtCache;
  RouteErrorOption* errorOption;    // piggybacked route error option
  Time_t         timeErrorOpt;
};

#endif
