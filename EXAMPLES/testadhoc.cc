// testadhoc.cc
// Ad hoc routing protocol test, GTNetS
// Young-Jun Lee, Georgia Tech, Fall 2004

#include "simulator.h"
#include "node.h"
#include "wlan.h"
#include "ratetimeparse.h"
#include "application-cbr.h"
#include "udp.h"
#include "routing-dsr.h"
#include "routing-nvr.h"
#include "routing-aodv.h"
#include "application-aodv.h"
#include "wireless-grid-rectangular.h"
#include "priqueue.h"
#include "trace.h"

#define ANIMATION_ON

#define XWIDTH		800
#define YWIDTH		800
#define NO_NODE		50
#define NO_SRC		1

#define SIM_TIME	500

#define ROUTING_DNVR	1
#define ROUTING_DSR	2
#define ROUTING_AODV	3

using namespace std;

int main(int argc, char** argv)
{
  /* parameters setting
     1. ad hoc routing protocol type
     2. seed
  */
  int routingProto = ROUTING_DSR;
  Seed_t seed = 1;
  if (argc > 1) {
    if (!strcmp("dnvr", argv[1])) routingProto = ROUTING_DNVR;
    else if (!strcmp("aodv", argv[1])) routingProto = ROUTING_AODV;
  }
  if (argc > 2) seed = atoi(argv[2]);

  Random::GlobalSeed(seed, seed, seed, seed, seed, seed);

  Simulator s;
  //s.HasMobility(true);

  // trace file
  Trace* gs = Trace::Instance();
  gs->Open("testadhoc.txt");
  gs->IPDotted(true);

  // Increase detail of L3 trace messages
  IPV4::Instance()->DetailOn(IPV4::TOTALLENGTH);
  IPV4::Instance()->SetTrace(Trace::ENABLED);

  // routing protocol
  if (routingProto == ROUTING_DSR) {
    RoutingDSR::defaultSendBufPeriod = 0;
    Routing::SetRouting(new RoutingDSR);  // set DSR to default routing
  }
  else if (routingProto == ROUTING_DNVR) {
    Routing::SetRouting(new RoutingNVR);  // set NVR to default routing
  }

  // default setting
  Queue::Default(PriQueue());
  Queue::DefaultLimitPkts(50);

  WirelessLink wlink(NO_NODE, IPAddr("192.168.0.1"), MASK_ALL);

  Uniform urvx(0, XWIDTH), urvy(0, YWIDTH);
  WirelessGridRectangular g(Location(0,0), Constant(NO_NODE), urvx, urvy,
                            IPADDR_NONE, false);
  Node* n[NO_NODE];
  for (int i = 0; i < NO_NODE; i++) {
    n[i] = wlink.GetNode(i);
    n[i]->SetRadioRange(250);
    n[i]->SetLocation(urvx.Value(), urvy.Value());
  }

  if (routingProto == ROUTING_AODV) {
    AODVApplication* appAodv[NO_NODE];
    for(int i = 0; i < NO_NODE; i++) {
      appAodv[i] = new AODVApplication(n[i]);
      appAodv[i]->Start(0);
      appAodv[i]->Stop(SIM_TIME);
    }
  }

  Rate_t rate = (Rate_t)Rate("4.096Kb") * 4;

  Application* cbr;
  cbr = n[0]->AddApplication(
      CBRApplication(n[1]->GetIPAddr(), 1001, 1000, rate, 512, UDP()));
  cbr->Start(0);
  cbr->Stop(SIM_TIME);
  n[0]->SetTrace(Trace::ENABLED);
  n[1]->SetTrace(Trace::ENABLED);
  n[0]->Shape(Node::CIRCLE);
  n[1]->Shape(Node::CIRCLE);
  n[0]->SetLocation(0, 0);
  n[1]->SetLocation(XWIDTH, YWIDTH);

  //s.Progress(1.0);
  s.StopAt(SIM_TIME);
#ifdef ANIMATION_ON
  s.AnimateWirelessTx(true);
  s.PauseOnWirelessTx(false);
  s.StartAnimation(0, true);
  //s.StartAnimation(0, true);
  s.AnimationUpdateInterval(Time("1ms"));
  //s.PauseOnWirelessTx(true);
#endif
  s.Run();

#ifdef RANDOM_SRC
  for (int i = 0; i < NO_SRC; i++) {
    cout << "{" << sd[i].s << ", " << sd[i].d << "},";
    if( (i+1)%5 == 0 ) cout << endl;
  }
  cout << endl;
#endif

  cout << "Simulation complete" << endl;
}
