// Simple tester for TCP
// George F. Riley.  Georgia Tech, Spring 2002

#include <iostream>

#include "simulator.h"
#include "application-tcpsend.h"
#include "application-tcpserver.h"
#include "duplexlink.h"
#include "queue.h"
#include "ratetimeparse.h"
#include "tcp-tahoe.h"
#include "dumbbell.h"
#include "args.h"
#include "validation.h"  

using namespace std;

class TCPSendData : public TCPSend
{
public:
  TCPSendData(IPAddr_t, PortId_t, const Random&,
          const TCP& = TCP::Default(), 
          const Random& sleepTime = Constant(0),
          Count_t loopCount = 1);
  Application* Copy() const;  // Make a copy of the application
  virtual void SendData();    // Send the (next) data block
private:
  Count_t* intData;
  Count_t  nextTx;
};

TCPSendData::TCPSendData(IPAddr_t ip, PortId_t port, const Random& rnd,
                         const TCP& tcp, const Random& sleepTime,
                         Count_t loopCount)
    : TCPSend(ip, port, rnd, tcp, sleepTime, loopCount),
      intData(nil), nextTx(0)
{
}

Application* TCPSendData::Copy() const
{
  return new TCPSendData(*this);
}

void TCPSendData::SendData()
{
  if (!connected)
    {
      DEBUG0((cout << "TCPSend::SendData not connected, tcp " << l4Proto << endl));
      l4Proto->Connect(peerIP, peerPort);
      return;
    }
  sent = (Count_t)sizeRV->Value();  // Number of bytes to send
  Count_t intSent = sent/4;
  if (intSent == 0) intSent++; // Never 0
  sent = intSent * 4;
  // Make  a set of known data
  if (intData) delete [] intData;
  intData = new Count_t[intSent];
  for (Count_t k = 0; k < intSent; ++k)
    {
      intData[k] = k + nextTx;
    }
  nextTx += intSent;
  sentAck = 0;                 // Reset sent count
  Data d(intSent*4, (char*)intData);// Message size and data
  cout << "Hello from SendData(), sending " << intSent*4 << " bytes" 
       << " totSent " << nextTx * 4
       << endl;
  l4Proto->Send(d);            // Send the data
}

class TCPServerData : public TCPServer {
public:
  TCPServerData();
  virtual Application* Copy() const; // Make a copy of application
  virtual void Receive(Packet*,L4Protocol*,Seq_t);   // Data received
private:
  Count_t nextRx;
  Count_t totRx;
};

TCPServerData::TCPServerData()
    : TCPServer(), nextRx(0), totRx(0)
{
}

Application* TCPServerData::Copy() const
{
  return new TCPServerData(*this);
}

void TCPServerData::Receive(Packet* p, L4Protocol*, Seq_t)
{
  Data* d = (Data*)p->PopPDU();
  totRx += d->Size();
  cout << "TCPServerData::Rx, time " << Simulator::Now()
       << " size " << d->Size() 
       << " tot " << totRx
       << endl;
  Count_t* iptr= (Count_t*)d->data;
  if (!iptr)
    {
      cout << "HuH?  TCPServerData::Receive with no data" << endl;
      return;
    }
  Count_t isize = d->Size() / 4;
  for (Count_t i = 0; i < isize; ++i)
    {
      if (iptr[i] != nextRx)
        {
          cout << "HuH?  Expected " << nextRx << " got " << iptr[i] << endl;
          exit(1);
        }
      nextRx++;
    }
}
  
int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  // Create the simulator object
  Simulator s;
  Count_t nleaf = 1;
  
  Arg("nleaf", nleaf, 1);
  Arg::ProcessArgs(argc, argv);
  
  // Enable l2 tracing
  //L2Proto802_3::Instance()->SetTrace(Trace::ENABLED);
  // Disable l2 tracing
  //L2Proto802_3::Instance()->SetTrace(Trace::DISABLED);

  // Set some defaults
  //Queue::DefaultLength(200000);    // 200,000 bytes
  Link::DefaultRate(Rate("10Mb")); // 10 Mega-bits/sec
  Link::DefaultDelay(Time("10ms"));// 10 milliseconds delay

  // Create the topology, using default link
  Dumbbell b(nleaf, nleaf, 0.1, IPAddr("192.168.1.1"), IPAddr("192.168.2.1"));
  b.BoundingBox(Location(0,0), Location(1,1));
  // Set queues artificially small to insure some loss
  b.LeftQueue()->SetLimitPkts(10);
  b.RightQueue()->SetLimitPkts(10);
  
  // Restrict queue limit on bottleneck
  if (!Arg::Specified("unrestricted")) b.LeftQueue()->SetLimit(10000);

  // If "forcedloss" specified, force 5 losses at time 5.0
  if (Arg::Specified("forcedloss")) b.LeftQueue()->AddForcedLoss(5.0, 5);

  // Set bottleneck link delay a bit longer
  b.LeftLink()->Delay(Time("100ms"));
  b.RightLink()->Delay(Time("100ms"));
  
  // Create the TCP endpoints
  for (unsigned k = 0; k < nleaf; ++k)
    {
      TCPServerData* server = (TCPServerData*)b.Right(k)->
          AddApplication(TCPServerData());
      server->BindAndListen(80);
    }
  
  // Create the TCP sending applications
  Uniform startRNG(0, 0.1);
  for (unsigned k = 0; k < nleaf; ++k)
    {
      TCPSend* pApp = (TCPSend*)b.Left(k)->AddApplication(
          TCPSendData(b.Right(k)->GetIPAddr(), // Peer IP Address
                      80,                      // Peer port number
                      Uniform(100000, 1000000),// Amount of data
                      TCP::Default(),          // TCP to use
                      Uniform(0, 5.0),         // Sleeptime
                      50));                    // Loopcount
      // Parameterize the tcp endpoint
      pApp->Start(startRNG.Value());
    }

  s.StopAt(1000.0);
  if (Arg::Specified("animate") && !Validation::noAnimation)
    { // Enable animiation if specified on command line
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Run();
}

