// Test the GCache Gnutella bootstrap
// George F. Riley.  Georgia Tech, Spring 2003

#include <vector>

#include "simulator.h"
#include "scheduler.h"
#include "dumbbell.h"
#include "linkp2p.h"
#include "ratetimeparse.h"
#include "tcp-tahoe.h"
#include "application-webserver.h"
#include "application-gnutella.h"
#include "globalstats.h"
#include "droptail.h"
#include "ipv4.h"
#include "rng.h"
#include "validation.h"

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  int lnodes = 10;
  int rnodes = 10;
  vector<Gnutella*> gVec;

  if (argc > 1) lnodes = atol(argv[1]);
  if (argc > 2) rnodes = atol(argv[2]);

  Queue::Default(DropTail()); // Set default to non-detailed drop tail
  Trace* gs = Trace::Instance();
  gs->IPDotted(true);
  gs->Open("testgcache.txt");
  TCP::LogFlagsText(true);    // Log flags in text mode
  // Enable all l3 tracing
  IPV4::Instance()->SetTrace(Trace::ENABLED);

  Linkp2p l;
  l.Bandwidth(Rate("56Kb"));
  l.Delay(Time("50ms"));
  Uniform startRNG(0, 10);
  Dumbbell b(lnodes, rnodes, 1.0,
             IPAddr("192.168.1.0"), IPAddr("192.168.2.0"), l);
  b.BoundingBox(Location(0,0), Location(10,10));

  TCPTahoe tcp;
  tcp.SetTrace(Trace::ENABLED);
  for (Count_t i = 0; i < b.LeftCount(); ++i)
    { // Add a gnutella application at each left side node
      Node* n = b.Left(i);
      Gnutella* app = (Gnutella*)n->AddApplication(Gnutella(tcp));
      app->Start(startRNG.Value());
      gVec.push_back(app); // Save for debug print later
    }
  // Add a few that don't work for testing
  Gnutella::AddGCache(IPAddr("192.168.3.1"));
  Gnutella::AddGCache(IPAddr("192.168.3.2"));
  Gnutella::AddGCache(IPAddr("192.168.3.3"));
  Gnutella::AddGCache(IPAddr("192.168.3.4"));
  Gnutella::AddGCache(IPAddr("192.168.3.5"));
  for (Count_t i = 0; i < b.RightCount(); ++i)
    { // Add a GCache server at each right node
      Node* n = b.Right(i);
      // Add a gcache server on right side
      Gnutella::AddGCache(b.Right(i)->GetIPAddr());
      WebServer* app = (WebServer*)(n->AddApplication(WebServer(tcp)));
      app->EnableGCache();
    }
  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Progress(1);
  s.StopAt(100);
  s.Run();
  Stats::Print(); // Print the statistics
  cout << "Memory usage after run " 
       << s.ReportMemoryUsageMB() << "MB" << endl;
  cout << "Total events processed "
       << Scheduler::Instance()->TotalEventsProcessed() << endl;
  cout << "Setup time " << s.SetupTime() << endl;
  cout << "Route time " << s.RouteTime() << endl;
  cout << "Run time "   << s.RunTime() << endl;
  cout << endl << "Dumping Gnutella HostEnt debug info" << endl;
  for (vector<Gnutella>::size_type i = 0; i < gVec.size(); ++i)
    {
      gVec[i]->DBDump();
    }
}
