// Generated automatically by make-gtimage from HostSmall.png
// DO NOT EDIT

#include "image.h"

class HostSmallImage : public Image {
public:
  HostSmallImage(){}
  const char* Data() const { return data;}
  int Size() const { return size;}
  operator const char*() { return data;}
  static const char* data;
  static int size;
};
