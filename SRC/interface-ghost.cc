// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: interface-ghost.cc 506 2007-07-18 19:03:56Z mhafez $



// Georgia Tech Network Simulator - Ghost Interface class
// George F. Riley.  Georgia Tech, Spring 2002

// Define class to model "Ghost" network link interfaces
// Ghosts are used in distributed simulations to maintain
// connectivity information for nodes assigned to other
// processes.

#include <iostream>

//#define DEBUG_MASK 0x01
#include "debug.h"
#include "interface-ghost.h"
#include "link.h"
#include "hex.h"

using namespace std;

Count_t InterfaceGhost::count = 0;

InterfaceGhost::InterfaceGhost(
    const L2Proto& l2, IPAddr_t i,
    Mask_t m, MACAddr mac, bool bootstrap)  
  : Interface(l2, i, m, mac)
{
  count++;
}

InterfaceGhost::~InterfaceGhost()
{
}

bool InterfaceGhost::Send(Packet* p, IPAddr_t i, int type)
{ // Never called for Ghosts
  return false;
}

// Send without ARP
bool InterfaceGhost::Send(Packet* p, const MACAddr& dst, int type)
{ // Never called for Ghosts
  return false;
}

bool InterfaceGhost::Send(Packet* p, Word_t type)
{ // Never called for Ghosts
  return false;
}

// Handler methods
void InterfaceGhost::Handle(Event* e, Time_t t)
{ // Never called for Ghosts
}

void InterfaceGhost::HandleLLCSNAP(Packet*, bool)
{ // Never called for GHosts
}

// Notifier
void InterfaceGhost::Notify(void* v)
{ // Never called for  ghosts
}

Count_t  InterfaceGhost::PeerCount() const
{ // Number of peers
  return pLink->PeerCount();
}

IPAddr   InterfaceGhost::PeerIP(int npeer) const
{ // Get peer IP addr (if known)
  return pLink->PeerIP(npeer);
}

IPAddr   InterfaceGhost::NodePeerIP(Node* n) const
{ // Get peer IP addr (if known)
  return pLink->NodePeerIP(n);
}

bool     InterfaceGhost::NodeIsPeer(Node* n) const
{ // Find out if specified node is a peer
  if (!pLink) return false;
  return pLink->NodeIsPeer(n);
}

Count_t  InterfaceGhost::NodePeerIndex(Node* n) const
{ // Find out peer index (for NixVector Routing)
  return pLink->NodePeerIndex(n);
}

#ifdef MOVED_TO_BASIC
void  InterfaceGhost::SetNode(Node* n)
{
  pNode = n;
}

Link* InterfaceGhost::GetLink() const
{
  return pLink;
}

void InterfaceGhost::SetLink(Link* pl)
{
  pLink = pl;
}
#endif

Queue* InterfaceGhost::GetQueue() const
{
  return nil;
}
  
void InterfaceGhost::SetQueue(const Queue&)
{
}

bool InterfaceGhost::QueueDetailed() const
{
  return false;
}

L2Proto* InterfaceGhost::GetL2Proto() const
{
  return nil;
}
  
void InterfaceGhost::SetL2Proto(const L2Proto&)
{ // Nothing to do
}

void InterfaceGhost::AddNotify(NotifyHandler*, void*)
{ // Add a notify client
}

void InterfaceGhost::CancelNotify(NotifyHandler*)
{ // Cancel a notification
}

MACAddr InterfaceGhost::IPToMac(IPAddr_t i) const
{ // Never called for Ghosts
  return MACAddr::NONE;
}

Count_t InterfaceGhost::NeighborCount(Node* n) const
{ // Get number of routing neighbor by asking the link
  // The links do this in different ways, so we ask
  if(pLink) return pLink->NeighborCount(n);
  return 0;
}

void  InterfaceGhost::Neighbors(NodeWeightVec_t& nwv, bool forceAll)
{ // Add to neighbor list by asking the link
  if (pLink) pLink->Neighbors(nwv, this, forceAll);
}

IPAddr_t InterfaceGhost::DefaultPeer()
{
  if (pLink) return pLink->DefaultPeer(this);
  return IPADDR_NONE;
}

bool InterfaceGhost::IsLocalIP(IPAddr_t i) const
{ // Determine if specified is on this subnetwork
  DEBUG0((cout << "Checking local ip " << Hex8(i)
          << " ip " << Hex8(ip)
          << " mask " << Hex8(mask) << endl));
  if ((ip & mask) == (i & mask)) return true;
  return false;
}

Mult_t InterfaceGhost::TrafficIntensity() const
{ // Never called for Ghosts
  return 0.0;
}

// Distributed Simulation Routines
void InterfaceGhost::AddRemoteIP(IPAddr_t, Mask_t, Count_t)
{ // Never called for Ghosts
}



