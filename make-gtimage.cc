// Make a GTImage object from an existing image file
// George F. Riley, Georgia Tech, Summer 2004

#include <iostream>
#include <string>

#include <stdio.h>

using namespace std;

void WriteCommentHeader(FILE* f, const char* iname)
{
  fprintf(f, "// Generated automatically by make-gtimage from %s\n", iname);
  fprintf(f, "// DO NOT EDIT\n\n");
}

void WriteHFile(FILE* f, const string& gtn)
{
  const char* n = gtn.c_str();
  fprintf(f, "#include \"image.h\"\n\n");
  fprintf(f, "class %sImage : public Image {\n", n);
  fprintf(f, "public:\n");
  fprintf(f, "  %sImage(){}\n", n);
  fprintf(f, "  const char* Data() const { return data;}\n");
  fprintf(f, "  int Size() const { return size;}\n");
  fprintf(f, "  operator const char*() { return data;}\n");
  fprintf(f, "  static const char* data;\n", n);
  fprintf(f, "  static int size;\n", n);
  fprintf(f, "};\n");
}

void WriteCCFile(FILE* f, const string& gtn, const string& hname)
{
  fprintf(f, "#include \"%s\"\n\n", hname.c_str());
  fprintf(f, "const char* %sImage::data = \n", gtn.c_str());
}

int main(int argc, char** argv)
{
  if (argc < 2)
    {
      cout << "Usage: make-gtimage image-file-name" << endl;
      exit(1);
    }
  for (int j = 1; j < argc; ++j)
    {
      
      FILE* f = fopen(argv[j], "rb");
      if (!f)
        {
          cout << "Can't open image file " << argv[j] << endl;
          exit(2);
        }
      string gtn(argv[j]);
      // Remove leading path info
      string::size_type k = gtn.find_last_of('/');
      if (k != string::npos) gtn = gtn.substr(k+1, string::npos);
      cout << "Converting " << gtn << endl;
      // Remove trailing . and all following
      k = gtn.find_last_of('.');
      if (k != string::npos) gtn = gtn.substr(0, k);
      string gtfnh(gtn);
      gtfnh = string("image-") + gtfnh + string(".h");
      string gtfnc(gtn);
      gtfnc = string("image-") + gtfnc + string(".cc");

      // Change dots in gt name to underscores
      for (string::size_type i = 0; i < gtn.length(); ++i)
        {
          if (gtn[i] == '.') gtn[i] = '_';
        }
      FILE* h  = fopen(gtfnh.c_str(), "wb");
      FILE* cc = fopen(gtfnc.c_str(), "wb");
      WriteCommentHeader(h, argv[1]);
      WriteCommentHeader(cc, argv[1]);

      WriteHFile(h, gtn);
      WriteCCFile(cc, gtn, gtfnh);
  
      // Now write the image data
      int sz = 0;
      while(true)
        {
          unsigned char c[16];
          int cnt = fread(c, 1, sizeof(c), f);
          fprintf(cc, "\"");
          if (cnt)
            {
              for (int i = 0; i < cnt; ++i)
                {
                  fprintf(cc, "\\x%02x", c[i]);
                }
            }
          sz += cnt;
          if (cnt < sizeof(c))
            {
              fprintf(cc, "\";\n");
              break;
            }
          else
            {
              fprintf(cc, "\"\n");
            }
        }
      fprintf(cc, "\n");
      fprintf(cc, "int %sImage::size = %d;\n", gtn.c_str(), sz);
#ifdef ADD_DEBUG
      // Add the debug write stuff
      fprintf(cc, "\n#include<stdio.h>\nint main()\n{\n");
      fprintf(cc, "FILE* f = fopen(\"zzz.zzz\", \"wb\");\n");
      fprintf(cc, "fwrite(%s_dat, 1, %s_size, f);\n", gtn.c_str(), gtn.c_str());
      fprintf(cc, "fclose(f);\n}\n");
#endif
      fclose(cc);
    }
}

      
