// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: red.cc 141 2004-10-14 02:43:14Z riley $



// Georgia Tech Network Simulator - RED Queue class
// Tarik Arici (tariq@ece.gatech.edu), Aug. 2002

///////////////////////////////////////////////////////////
// These paramater values are taken from S.Floyd's simulator; tcpsim.
#define q_weight 0.002
#define min_threshold 5
#define max_threshold 15
#define queue_size 60
#define max_prob 0.1
#define mean_pkt_size 500
////////////////////////////////////////////////////////////
#include "red.h"
#include <math.h>
#include "simulator.h"
#include "rng.h"
#include "interface.h"
#include "link.h"

//#define DEBUG_MASK 0x1
#include "debug.h"

using namespace std;

REDQueue::REDQueue() : iface(nil)
{
  w_q = q_weight;
  mean_pktsize = mean_pkt_size;
  size = 0;
  min_th = min_threshold * mean_pktsize;
  max_th = max_threshold * mean_pktsize;
  SetLimit(queue_size * mean_pktsize);
  max_p = max_prob;
  bandwidth = 800000;
  count = -1;
  q_avg = 0;
  pktspersec = bandwidth / mean_pktsize;  // [(bits/sec) / (bits/pkt)]
  if (Simulator::instance != NULL)
    {
    idle_since = Simulator::instance->Now();
    }
  else
    {
    idle_since = 0.0;                  // scheduler not instantiated yet
    }
}

REDQueue::REDQueue(
   DCount_t in_w_q, Count_t in_min_th, Count_t in_max_th, 
   Count_t in_limit, DCount_t in_max_p, Count_t in_mean_pktsize) : iface(nil)
{
  DEBUG(0,(cout << "REDQueue::RedQueue" << endl));
  // set by the user
  w_q = in_w_q;
  min_th = in_min_th;
  max_th = in_max_th;
  SetLimit(in_limit);
  max_p = in_max_p;
  mean_pktsize = in_mean_pktsize;
  size = 0;
  bandwidth = 800000;                // !!!GET THIS FROM THE LINK
  count = -1;
  q_avg = 0;
  if (Simulator::instance != NULL)
    {
    idle_since = Simulator::instance->Now();
    }
  else
    {
    idle_since = 0.0;                  // scheduler not instantiated yet
    }
  // pkts per sec. is used to update average queue size when queue is idle
  pktspersec = bandwidth / mean_pktsize;  // [(bits/sec) / (bits/pkt)]


}

bool REDQueue::Enque(Packet* p)
{
  //Time_t now = Simulator::instance->Now();
  cout << "Red::Enque, this " << this
       << " length " << Length() << endl;
  DEBUG(0,(cout << "REDQueue::Enqueue" << endl));
  DEBUG(1,(cout << "w_q: "<< w_q <<"min_th: "<<min_th<<"max_th: "<< max_th
       <<"size: "<<size<<endl));
  DEBUG(1,(cout<<"max_p: "<<max_p<<"count: "<<count<<"q_avg: "<<q_avg<<"limit: "<< GetLimit()<<endl));

  UpdateAverage();
  // update average queue size
  if (size == 0 ) { // pkt arriving to an idle queue
    DEBUG(1,(cout << "packet finds queue idle" << endl));
    int num_pkts_idle;    // number of packets that would have been arrived
                          // when the queue is idle
    Time_t now = Simulator::instance->Now();
    num_pkts_idle = int((now - idle_since) * pktspersec);
    DEBUG(1,(cout << "num_pkts_idle" << num_pkts_idle << endl));
    q_avg = pow((1 - w_q),num_pkts_idle) * q_avg; 
    // pkt arriving to an idle queue will never be dropped
    pkts.push_back(p);    
    size += p->Size();  
    DEBUG(1,(cout << "size: "<<size<< "q_avg: " << q_avg
	     << "now: "<< now << endl));
    return true;
  } 

  pkts.push_back(p);    // put the packet into the queue
  size += p->Size();    // update size of the queue
  q_avg = (1 - w_q) * q_avg + w_q * size;
  // actually here always last packet is dropped !!!
  // if the queue is full drop a packet
  if (Length() > Limit()) {
    Packet* pkt_to_drop = DropPacket();
    count = 0;          // after dropping, counter from last marked 
                        //pkt must be reset
    if (pkt_to_drop == p) 
      {
	DEBUG(1,(cout << "queue is full, pkt dropped" << endl));
	return false;
      }
    else return true;
  }

  // mark/drop packet with probablity p_a
  if ((min_th <= q_avg) && (q_avg <= max_th)){
    count++;
    DEBUG(1,(cout << "count: " << count));
    Packet* pkt_to_drop = MarkPacketwProb();
    if (pkt_to_drop == p) 
      {
	DEBUG(1,(cout << "pkt marked, min < avg < max" << endl));
	return false;
      }
    else
      {
	DEBUG(1,(cout << "pkt not marked, min < avg < max" << endl));
	return true;
      }
  } else if (q_avg >= max_th){
    count++;
    Packet* pkt_to_drop = MarkPacket();
    if (pkt_to_drop == p) 
      {
	DEBUG(1,(cout << "pkt marked, max < avg" << endl));
	return false;
      }
    else 
      {
	DEBUG(1,(cout << "pkt not marked, max < avg" << endl));
	return true;
      }
  } else {
    DEBUG(1,(cout << "pkt not marked, avg < min" << endl));
    DEBUG(1,(cout << "'count' before initialisation:"<< count << endl));
    count = -1;     // when avg smaller than min_th count must be reset
    return true;
  }

}

// Mark a pkt with probability  
// This implementation drops the arriving packet 
// with probability p_a as in Floyd's 93 paper.
// Different queings may be implemented by modifying this function
Packet* REDQueue::MarkPacketwProb()
{
  DEBUG(0,(cout << "REDQueue::MarkPacketwProb" << endl));
  DCount_t p_b;        

  DCount_t p_a;        
  p_b = (max_p * (q_avg - min_th) / (max_th - min_th));
  p_a = p_b / (1 - count * p_b); 
  DEBUG(1,(cout << "mark_prob: " << p_a <<endl));
  Uniform U_rng = Uniform();          
  if (U_rng.Value() <= p_a){
    count = 0;          
    Packet* pkt = pkts.back();  
    pkts.pop_back();          
    size -= pkt->Size();   
    return pkt;
  }
  return NULL;
}

// Drops the arriving packet because queue is larger then max_th
// Different queings may be implemented by modifying this function
Packet* REDQueue::MarkPacket()
{
  DEBUG(0,(cout << "REDQueue::MarkPacket" << endl));
  count = 0;          
  Packet* pkt = pkts.back(); 
  pkts.pop_back();          
  size -= pkt->Size();      
  return pkt;
}

// Drops the arriving packet because queue is full
// Different queings may be implemented by modifying this function
Packet* REDQueue::DropPacket()
{
  DEBUG(0,(cout << "REDQueue::DropPacket" << endl));
  count = 0;         
  Packet* pkt = pkts.back(); 
  pkts.pop_back();          
  size -= pkt->Size();      
  return pkt;
}

Queue*  REDQueue::Copy() const
{
  return new REDQueue(*this);
}

Packet* REDQueue::Deque()
{
  cout << "Red::Deque, this " << this
       << " length " << Length() << endl;
  DEBUG(0,(cout << "REDQueue::Deque" << endl));
  Time_t now = Simulator::instance->Now();
  UpdateAverage();
  if (Length() == 0){
    idle_since = now;                // if queue is empty, set idle_since
    return NULL;                         // Nothing enqued
  }
  Packet* p = pkts.front();              // Get head of queue
  pkts.pop_front();                      // Remove it
  size -= p->Size();                     // Note byte count removed
  return p;                              // Return the packet
}

void REDQueue::SetInterface(Interface* i)
{ // Note the associated interface
  iface = i;
}

Rate_t REDQueue::Bandwidth()
{
  if (iface)
    { // Interface is known
      Link* l = iface->GetLink();
      if (l)
        { // Link exists
          return l->Bandwidth();
        }
    }
  return 0.0; // Not known
}
