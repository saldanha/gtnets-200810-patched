// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: droptail-ipa.h 92 2004-09-14 18:18:43Z dheeraj $



// DropTail Queue class with IPA derivatives calculation
// George F. Riley, Georgia Tech, Fall 2002

#ifndef __droptail_ipa_h__
#define __droptail_ipa_h__

#include "droptail.h"
#include "timer.h"

class IPAEvent;
class IPATimer;

//Doc:ClassXRef
class DropTailIPA : public DropTail, public Timer {
public :
  DropTailIPA();
  DropTailIPA(Count_t l);            // Specify limit
  DropTailIPA(const DropTailIPA&);   // Copy constructor
  ~DropTailIPA();                    // Destructor
  // Time functions
  virtual void Timeout(TimerEvent*); // Called when timer expires
  // DropTail functions
  bool Enque(Packet*);               // Return true if enqued, false if dropped
  Packet*  Deque();                  // Return next element (NULL if none)
  void     SetLimit(Count_t l);      // Set queue limit
  Queue*   Copy() const;             // Make a copy
  virtual void ResetStats();         // Reset the statistics to zero
  // New functions for IPA queues
  virtual  NCount_t DL1dB1();        // Get dl1db1 derivative
  virtual  Time_t   DQ1dB1();        // Get dq1db1 derivative
  virtual  Time_t   PartialDQ1();    // Get partial (from start of lbp) dQ1
  virtual  NCount_t DL2dB1();        // Get dl2db1 derivative
  virtual  Time_t   DQ2dB1();        // Get dq2db1 derivative
  virtual  void     Stage1(DropTailIPA*); // Set stage 1 peer
  virtual  void     Stage2(DropTailIPA*); // Set stage 2 peer
  virtual  Count_t  Losses();             // Return number of losses
  virtual  DCount_t Workload();           // Total buffer workload
  virtual  bool     LBP();                // True if currently in a LBPeriod
  virtual  void     LWeight(Mult_t w);    // Set loss weight
  virtual  void     QWeight(Mult_t w);    // Set queuing weight
  virtual  Mult_t   LWeight();            // Get the loss weight
  virtual  Mult_t   QWeight();            // Get the queue weight
  virtual  void     AdjustPeriod(Time_t); // Set the adjustment period
  virtual  void     AdjustStart(Time_t);  // Set the starting time for adjusts
  virtual  void     Gain(Mult_t g);       // Set the gain factor for adjusts
private:
  bool    busyPeriod; 
  bool    lossyBusyPeriod;
  Time_t  bpStart;           // Time the busy period started
  Time_t  lbpStart;          // Time the lpb started
  Count_t lossyBusyPeriods;  // Count of lossy busy periods
  Time_t  sumLossy;          // Sum of time from start of LBP to end
  bool    full;              // True if queue is full
  // The next two are used for two stage models
  DropTailIPA* stage1;       // Points to stage 1 queue from stage 2
  DropTailIPA* stage2;       // Points to stage 2 queue from stage 1
  // Statisics
  Count_t  totalLosses;      // Total packet losses
  // Information for two stage derivatives
  Count_t  tmp;              // Temporary to store for dL2/dB1
  Count_t  accumulator;      // Accumulator for dL2/dB1
  Count_t  aspCount;         // Count of active switchover points
  DCount_t dQ2;              // Secondary Q workload derivative dQ2/dB1
  Time_t   ppStart;          // Partial period start
  Count_t  ppMult;           // Partial period multiplier
  // Information used for periodic adjustments
  Mult_t   qLim;             // Floating point queue limit
  Mult_t   wL;               // Weight for loss
  Mult_t   wQ;               // Weight for queuing delay
  Time_t   lastAdjust;       // Time of last adjustment
  Time_t   adjustPeriod;     // Length of adjustment period
  Time_t   adjustStart;      // Time to start adjustments
  Mult_t   gain;             // Gain factor for adjustments
  NCount_t cumdL1;           // Cumulative dL1/dB1 at last adjustment
  Time_t   cumdQ1;           // Cumulative dQ1/dB1 at last adjustment
  NCount_t cumdL2;           // Cumulative dL2/dB1 at last adjustment
  Time_t   cumdQ2;           // Cumulative dQ2/dB1 at last adjustment
  IPATimer* adjustTimer;     // Points to currently active IPA timer
  IPAEvent* adjustEvent;     // Points to currently active IPA event
};

#endif

