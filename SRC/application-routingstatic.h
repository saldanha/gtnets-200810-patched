/*
  static routing application
 */


#ifndef __application_routingstatic_h
#define __application_routingstatic_h

#include "application-routing.h"



class ApplicationRoutingStatic: public ApplicationRouting {
 public:
	ApplicationRoutingStatic();
	~ApplicationRoutingStatic();
	virtual void StartApp();
	virtual void InitializeRoutes();
	virtual void ReInitializeRoutes();
	
	virtual void StopApp();
	virtual void DefaultRoute(rt_value_t*);
	
 public:
	unsigned int variable1;
};

#endif
