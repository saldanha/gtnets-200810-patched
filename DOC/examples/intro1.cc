// Simple GTNetS example
// George F. Riley, Georgia Tech, Winter 2002

#include "simulator.h"      // Definitions for the Simulator Object
//Doc:Label:IncludeStart
#include "node.h"           // Definitions for the Node Object
#include "linkp2p.h"        // Definitions for point-to-point link objects
#include "ratetimeparse.h"  // Definitions for Rate and Time objects
#include "application-tcpserver.h" // Definitions for TCPServer application
#include "application-tcpsend.h"   // Definitions for TCP Sending application
#include "tcp-tahoe.h"      // Definitions for TCP Tahoe
//Doc:Label:IncludeEnd

int main()
//Doc:Label:Main
{
  // Create the simulator object    
  Simulator s;
  //Doc:Label:Simulator

  // Create and enable IP packet tracing
  //Doc:Label:TraceStart
  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  //Doc:Label:TraceInstance
  tr->IPDotted(true);            // Trace IP addresses in dotted notation
  //Doc:Label:TraceDotted
  tr->Open("intro1.txt");        // Create the trace file
  //Doc:Label:TraceOpen
  TCP::LogFlagsText(true);       // Log TCP flags in text mode
  //Doc:Label:TraceFlags
  IPV4::Instance()->SetTrace(Trace::ENABLED);  // Enable IP tracing all nodes
  //Doc:Label:TraceIP
  //Doc:Label:TraceEnd    

  // Create the nodes
  //Doc:Label:NodeStart
  Node c1;    // Client node 1
  Node c2;    // Client node 2
  Node r1;    // Router node 1
  Node r2;    // Router node 2
  Node s1;    // Server node 1
  Node s2;    // Server node 2
//Doc:Label:NodeEnd

  // Create a link object template, 100Mb bandwidth, 5ms delay
  //Doc:Label:LinkStart   
  Linkp2p link(Rate("100Mb"), Time("5ms"));
  //Doc:Label:Link
  // Add the links to client and server leaf nodes
  c1.AddDuplexLink(&r1, link, IPAddr("192.168.0.1")); // c1 to r1
  //Doc:Label:LinkAddStart
  c2.AddDuplexLink(&r1, link, IPAddr("192.168.0.2")); // c2 to r1
  s1.AddDuplexLink(&r2, link, IPAddr("192.168.1.1")); // s1 to r2
  s2.AddDuplexLink(&r2, link, IPAddr("192.168.1.2")); // s2 to r2
  //Doc:Label:LinkAddEnd

  // Create a link object template, 10Mb bandwidth, 100ms delay
  //Doc:Label:LinkTwoStart
  Linkp2p r(Rate("10Mb"), Time("100ms"));
  // Add the router to router link
  r1.AddDuplexLink(&r2, r);
  //Doc:Label:LinkTwoEnd
  //Doc:Label:LinkEnd     

  // Create the TCP Servers
  //Doc:Label:TCPServerStart
  TCPServer* server1 = (TCPServer*)s1.AddApplication(TCPServer(TCPTahoe()));
  //Doc:Label:TCPServerOne
  if (server1)
    {
      server1->BindAndListen(80); // Application on s1, port 80
      //Doc:Label:TCPServerBindOne
      server1->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
      //Doc:Label:TCPServerTraceOne
    }
  
  TCPServer* server2 = (TCPServer*)s2.AddApplication(TCPServer(TCPTahoe()));
  //Doc:Label:TCPServerTwo
  if (server2)
    {
      server2->BindAndListen(80); // Application on s2, port 80
      //Doc:Label:TCPServerBindTwo
      server2->SetTrace(Trace::ENABLED); // Trace TCP actions at server2
      //Doc:Label:TCPServerTraceTwo
    }
  //Doc:Label:TCPServerEnd

  // Set random starting times for the applications
  //Doc:Label:TCPClientStartUniform
  Uniform startRv(0.0, 2.0);

  // Create the TCP Sending Applications
  //Doc:Label:TCPClientStart
  TCPSend* client1 = (TCPSend*)c1.AddApplication(
  //Doc:Label:TCPClientOne
      TCPSend(s1.GetIPAddr(), 80,
      //Doc:Label:TCPClientOneUniform
              Uniform(1000,10000),
              TCPTahoe()));
  if (client1)
    {
      // Enable TCP trace for this client
      //Doc:Label:TCPClientOneTrace
      client1->SetTrace(Trace::ENABLED);
      // Set random starting time
      //Doc:Label:TCPClientStartOne
      client1->Start(startRv.Value());
    }
  
  TCPSend* client2 = (TCPSend*)c2.AddApplication(
  //Doc:Label:TCPClientTwo
      TCPSend(s2.GetIPAddr(), 80,
  //Doc:Label:TCPClientTwoConstant
              Constant(100000),
              TCPTahoe()));
  if (client2)
    {
      // Enable TCP trace for this client
      //Doc:Label:TCPClientTwoTrace
      client2->SetTrace(Trace::ENABLED);
      // Set random starting time
      //Doc:Label:TCPClientStartTwo
      client2->Start(startRv.Value());
    }
  //Doc:Label:TCPClientEnd

  s.Progress(1.0);                // Request progress messages
  //Doc:Label:Progress
  s.StopAt(10.0);                 // Stop the simulation at time 10.0
  //Doc:Label:StopAt
  s.Run();                        // Run the simulation
  //Doc:Label:Run
  std::cout << "Simulation Complete" << std::endl;
}
