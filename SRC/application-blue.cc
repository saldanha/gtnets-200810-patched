// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//


// Georgia Tech Network Simulator - Bluetooth Application class
// George F. Riley, Georgia Tech, Spring 2004

// Define an on/off data source application class

// Uncomment below to enable debug level 0
//#define DEBUG_MASK 0x1
#include "debug.h"
#include "application-blue.h"
//#include "udp.h"
//#include "tcp.h"
#include "node.h"
#include "simulator.h"

using namespace std;

// Defaults for rate/size
Rate_t BlueApplication::defaultRate = 500000.0;
//Size_t BlueApplication::defaultSize = 512;
Size_t BlueApplication::defaultSize = 5;
Count_t BlueApplication::defaultCount = 500000;

// Constructors
BlueApplication::BlueApplication(BlueNode* n1, BlueNode* n2,
                   const  Random& ontime, 
                   Bnep* pBnep, //const  Bnep& p,
                   Rate_t r,
                   Size_t s,
				   Count_t c)
  : //bnep(p.Copy()), peerProto(nil),
    connected(false),
    onTime(ontime.Copy()), offTime(ontime.Copy()),
    cbrRate(r), pktSize(s), pendingEvent(nil), pendingBlue(nil), 
    residualBits(0), lastStartTime(0),
    maxBytes(c), totBytes(0)
{
  bnep = pBnep;
  ConstructorHelper(n1, n2);
}

BlueApplication::BlueApplication(BlueNode* n1, BlueNode* n2,
                   const  Random& ontime, 
                   const  Random& offtime,
                   Bnep* pBnep, //const  Bnep& p,
                   Rate_t r,
                   Size_t s,
				   Count_t c)
  : //bnep(p.Copy()), peerProto(nil),
    connected(false),
    onTime(ontime.Copy()), offTime(offtime.Copy()),
    cbrRate(r), pktSize(s), pendingEvent(nil), pendingBlue(nil),
    residualBits(0), lastStartTime(0),
    maxBytes(c), totBytes(0)
{
  bnep = pBnep;
  ConstructorHelper(n1, n2);
}

BlueApplication::BlueApplication(const BlueApplication& c)
  : //bnep(c.bnep->Copy()),
    //peerProto(c.peerProto->Copy()),
	peerBdAddr(c.peerBdAddr),
    connected(c.connected),
    onTime(c.onTime->Copy()), offTime(c.offTime->Copy()),
    cbrRate(c.cbrRate), pktSize(c.pktSize),
    pendingEvent(nil), pendingBlue(nil),
    residualBits(c.residualBits),
    lastStartTime(c.lastStartTime),
    maxBytes(c.maxBytes), totBytes(c.totBytes)
{
}

// Handler Methods
void BlueApplication::Handle(Event* e, Time_t t)
{
  AppBlueEvent* ev = (AppBlueEvent*)e;
  switch (ev->event) {
    case AppBlueEvent::CHECK_CONN :
      {
	  	if(bnep->IsConn())
			{
  				Time_t offInterval = offTime->Value();
  				DEBUG0((cout << "Scheduling initial off time " << offInterval << endl));
  				pendingBlue = new AppBlueEvent(AppBlueEvent::START_GEN);
  				// Schedule the start event
				Scheduler::Schedule(pendingBlue, offInterval, this);
			}
		else
			{
  				DEBUG0((cout << "Scheduling check connnection time again" << endl));
  				pendingBlue = new AppBlueEvent(AppBlueEvent::CHECK_CONN);
  				// Schedule the check event
				Scheduler::Schedule(pendingBlue, 1.0, this);
			}
        return;
      }
    case AppBlueEvent::SEND_PKT :
      {
        bnep->Send(pktSize); // Send the packet
        DEBUG0((cout << "SendBlue Pkt at time " << Simulator::Now() << endl));
        lastStartTime = Simulator::Now();
        residualBits = 0;
        totBytes += pktSize;
        ScheduleNextTx();
        return;
      }
    case AppBlueEvent::START_GEN :
      {
        DEBUG0((cout << "StartGen at " << Simulator::Now() << endl));
        lastStartTime = Simulator::Now();
        ScheduleNextTx();
        Time_t onInterval = onTime->Value();
        pendingBlue->event = AppBlueEvent::STOP_GEN;
         // Schedule the stop event
        Scheduler::Schedule(pendingBlue, onInterval, this);
        return;
      }
    case AppBlueEvent::STOP_GEN :
      {
        DEBUG0((cout << "StopGen at " << Simulator::Now() << endl));
        if (totBytes < maxBytes)
          { // Only schedule if not execeeded maxBytes
            Time_t offInterval = offTime->Value();
            pendingBlue->event = AppBlueEvent::START_GEN;
            // Schedule the start event
            Scheduler::Schedule(pendingBlue, offInterval, this);
          }
        if (pendingEvent)
          {
            // Calculate residual bits since last packet sent
            residualBits += (Size_t)(cbrRate*(Simulator::Now()-lastStartTime));
            Scheduler::Cancel(pendingEvent);
            delete pendingEvent;
            pendingEvent = nil;
          }
        return;
      }
  }
  Application::Handle(e, t);  // May be application event
}

// Application Methods
void BlueApplication::StartApp()    // Called at time specified by Start
{
  StopApp();                         // Insure no pending event
  // Schedule the start generation event
  if (!connected)
    { // Need to establish connection if Bnep
      //bnep->Start(Simulator::Now(), peerBdAddr);
      bnep->Start(1.0, peerBdAddr);
	  connected = true;
    }
  if(bnep->IsConn())
    {
  		Time_t offInterval = offTime->Value();
  		//DEBUG0((cout << "Scheduling check connection time " << endl));
  		DEBUG0((cout << "Scheduling initial off time " << offInterval << endl));
  		//pendingBlue = new AppBlueEvent(AppBlueEvent::CHECK_CONN);
  		pendingBlue = new AppBlueEvent(AppBlueEvent::START_GEN);
  		// Schedule the check event
  		Scheduler::Schedule(pendingBlue, offInterval, this);
	}
}

void BlueApplication::StopApp()     // Called at time specified by Stop
{
  if (pendingEvent)
    { // Cancel the pending transmit event
      Scheduler::Cancel(pendingEvent);
      delete pendingEvent;
      pendingEvent = nil;
      // Calculate residual bits since last packet sent
      residualBits += (Size_t)(cbrRate * (Simulator::Now() - lastStartTime));
    }
  if (pendingBlue)
    { // Cancel the pending on/off event
      Scheduler::Cancel(pendingBlue);
      delete pendingBlue;
      pendingBlue = nil;
    }
  //if(bnep->IsConn())
  	StatOutput();
}
/*
void BlueApplication::AttachNode(BlueNode* n) 
{
  if(bnep)bnep->Attach(n);
}
*/
Application* BlueApplication::Copy() const
{
  return new BlueApplication(*this);
}

// Private Methods
void BlueApplication::ConstructorHelper( BlueNode* n1, BlueNode* n2)
{
  //bnep->Attach(n1);              // Attach the protocol to local node
  //peerProto = bnep->Copy();      // Get a second protocol object for n2
  bnep->AttachApplication(this); // Attached application

  //peerProto->Attach(n2);              // Attach remote to it's node
  peerBdAddr = n2->GetBdAddr();
  //connect immediately
  //bnep->Start(Simulator::Now(), peerBdAddr);
  //connected = true;
}

void BlueApplication::ScheduleNextTx()
{
  if (totBytes < maxBytes)
    {
      DEBUG0((cout << "SNTX pktSize " << pktSize 
              << " residual " << residualBits << endl));
      Count_t bits = pktSize * 8 - residualBits;
      Time_t nextTime = bits / cbrRate; // Time till next packet
      DEBUG0((cout << "SNTX, bits " << bits 
              << " nextTime " << nextTime << endl));
      if (!pendingEvent)
        {
          pendingEvent = new AppBlueEvent(AppBlueEvent::SEND_PKT);
        }
      // Schedule the packet transmit
      Scheduler::Schedule(pendingEvent, nextTime, this);
    }
  else
    { // All done, cancel any pending events
      StopApp();
    }
}

//stat output
void BlueApplication::StatOutput()
{
  //Own stat
  double dTxPower = bnep->pL2CAP->pLowerProtocol->StatTxPower();
  double dRxPower = bnep->pL2CAP->pLowerProtocol->StatRxPower();
  double dRxAcErrPower = bnep->pL2CAP->pLowerProtocol->StatRxAcErrPower();
  double dActiveNoPower = bnep->pL2CAP->pLowerProtocol->StatActiveNoPower();
  double dHoldPower = bnep->pL2CAP->pLowerProtocol->StatHoldPower();
  cout<<"Own TxPower "<<dTxPower<<endl;
  cout<<"Own RxPower "<<dRxPower<<endl;
  cout<<"Own RxAcErrPower "<<dRxAcErrPower<<endl;
  cout<<"Own ActiveNoPower "<<dActiveNoPower<<endl;
  cout<<"Own HoldPower "<<dHoldPower<<endl;
  
  //Peer stat
  /*
  double dTxPower = bnep->pL2CAP->pLowerProtocol->StatTxPower();
  double dRxPower = bnep->pL2CAP->pLowerProtocol->StatRxPower();
  double dRxAcErrPower = bnep->pL2CAP->pLowerProtocol->StatRxAcErrPower();
  double dActiveNoPower = bnep->pL2CAP->pLowerProtocol->StatActiveNoPower();
  double dHoldPower = bnep->pL2CAP->pLowerProtocol->StatHoldPower();
  cout<<"Peer TxPower "<<dTxPower<<endl;
  cout<<"Peer RxPower "<<dRxPower<<endl;
  cout<<"Peer RxAcErrPower "<<dRxAcErrPower<<endl;
  cout<<"Peer ActiveNoPower "<<dActiveNoPower<<endl;
  cout<<"Peer HoldPower "<<dHoldPower<<endl;
  */
}
