// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: routing-nixvector.h 446 2005-11-15 20:48:03Z riley $



// Georgia Tech Network Simulator - NixVector Routing class
// George F. Riley.  Georgia Tech, Spring 2002

// NixVector routing method

#ifndef __routingnixvector_h__
#define __routingnixvector_h__

#include "routing.h"
#include "ipv4.h"
#include "node.h"
#include <climits>
// NixVector routing is described in two research papers:
//    "Stateless Routing in Network Simulations", MASCOTS 2000
//    "Efficient Routing using NixVectors", HPSR 2001
// both by George F. Riley

typedef unsigned long NixBits_t;             // Use 32 bit values for nix bits
// NIX_NONE is used to specify NONE
#if INT_MAX < LONG_MAX
	#define NIX_BPW  64
#else
	#define NIX_BPW 32
#endif

#if INT_MAX < LONG_MAX
	#define NIX_NONE (NixBits_t)0xffffffffffffffff
#else
	#define NIX_NONE (NixBits_t)0xffffffff
#endif

//Doc:ClassXRef
class NixVectorOption : public PDU {   // Nix vector for routing
//Doc:Class The class {\tt NixVectorOption } derives from the class {\tt PDU}
//Doc:Class and defines a protocol header for the nix vextor routing
//Doc:Class For Nix Vector refer to the description of class
//Doc:Class {\tt RoutingNixVector }
	
public:
  typedef enum {Number = 24} NVOpt_t;
public:
  //Doc:Method  
  NixVectorOption();
  //Doc:Desc The Default Constructor
  
  //Doc:Method    
  NixVectorOption(const NixVectorOption&);   // Copy constructor
  //Doc:Desc This constructor takes a reference to a NixVectorOption and
  //Doc:Desc instantiates itself.

  //Doc:Method    
  NixVectorOption(char*, Size_t&);           // Construct from serial buff
  //Doc:Desc This method contructs a NixVector PDU from a serialized buffer
  //Doc:Desc of a given size.
  
  //Doc:Method    
  virtual ~NixVectorOption();                // Destructor
  //Doc:Desc The desctructor
  
  //  //Doc:Method    
  NixVectorOption& operator=(const NixVectorOption& s); // Assignment operator
  //  //Doc:Desc This method overloads the assignment operator
  //  //Doc:Return Reference to the PDU

  //Doc:Method    
  Size_t   Size() const;                     // Required for all PDU's
  //Doc:Desc This method returns the size of the PDU
  //Doc:Return size of the pdu
public:

  //Doc:Method  
  PDU*      Copy() const;                     // Make a copy of this NixVector
  //Doc:Desc  This method creates a copy of this PDU
  //Doc:Return A pointer to the copy of this PDU
  
  // Serialization

  //Doc:Method    
  Size_t    SSize();                         // Size needed for serialization
  //Doc:Desc The actual size of all the PDU contents in terms of bytes for
  //Doc:Desc serialization of the contents.
  //Doc:Return the size  

  //Doc:Method    
  char*     Serialize(char*, Size_t&);       // Serialize to a buffer
  //Doc:Desc This method is used to serialize the contents of the PDU  
  //Doc:Desc into a serialized character buffer b . the size of this buffer is 
  //Doc:Desc in the size arguement
  //Doc:Arg1 This is the pointer to the character buffer
  //Doc:Arg2 a reference to the size variable
  //Doc:Return the pointer to the updated character buffer
  
  //Doc:Method    
  char*     Construct(char*, Size_t&);       // Construct from buffer
  //Doc:Desc This method is the reverse of the Serialize . It constructs the
  //Doc:Desc PDU object from the character buffer that has been received over
  //Doc:Desc the network
  //Doc:Arg1 This is the pointer to the character buffer
  //Doc:Arg2 a reference to the size variable
  //Doc:Return the pointer to the character buffer of the remaining data
  
  //Doc:Method    
  void      Add(NixBits_t, Count_t);         // Add bits
  //Doc:Desc This method adds the n bits specified by the Nix_Bits bitmap
  //Doc:Desc The bits are right justified
  //Doc:Arg1 The bitmap
  //Doc:Arg2 The number of bits
  
  //Doc:Method    
  NixBits_t Extract(Count_t);                // Extract bits
  //Doc:Desc This method extracts the the number of bits specified by the
  //Doc:Desc arguement
  //Doc:Desc The extracted bitmap
  
  //Doc:Method    
  Count_t   Remaining();                     // Remaining unused buts
  //Doc:Desc This method returns the number of remaining bits
  //Doc:Return number of bits remaining

  //Doc:Method    
  void      Reset();                         // Reset used to zero
  //Doc:Desc This method saves the used count and resets used to zero.
  
  //Doc:Method    
  void      Dump();                          // Debug...print it
  //Doc:Desc Thie method is for debugging only

public:
  // The pointer below is actually the 32 bit NV if size <= 32.
  NixBits_t* nixVector;                      // Points to the nix vector
  // Use "word" for the fields below so we get 16 bit values.
  // These will never require more than 16 bits and we save memory
  // Both these are specified in bits
  Word_t     used;                           // Used portion of this nix vector
  Word_t     size;                           // Total size
private:
  Count_t    WSize();                        // Size in words
  Mask_t     BitMask(Count_t, Count_t);      // Get a mask at pos, nbits
  void       Expand();                       // Expand malloc'ed by 1 word
  // Insert bits into a 32  bit value
  void       InsertBits(NixBits_t&, Count_t, NixBits_t, Count_t);
public:
  Count_t    BitCount(Count_t);              // Find number of bits needed
};

// Define a hash table from IPAddress to NixVector
typedef std::map<IPAddr_t, NixVectorOption> NixMap_t;

//Doc:ClassXRef
class RoutingNixVector : public Routing {
//Doc:Class The class {\tt RoutingNixVector } derives from class {\tt Routing}
//Doc:Class and defines the NixVector Routing. This is useful for simulating
//Doc:Class large topologies with comparitively lesser memory requirements.
	
public:
  //Doc:Method
  RoutingNixVector();
  //Doc:Desc Default Constructor

  //Doc:Method
  virtual ~RoutingNixVector();
  //Doc:Desc The Destructor
  
  //Doc:Method
  virtual void Default( RoutingEntry r);         // Specify default route
  //Doc:Desc This method adds the default route to a particular node.
  //Doc:Arg1 The default routing entry or the default gateway
  
  //Doc:Method  
  virtual RoutingEntry GetDefault();         // Get default route
  //Doc:Desc This method returns the default route for this node
  //Doc:return The default routing entry or the default gateway

  //Doc:Method  
  virtual void Add( IPAddr_t, Count_t,
                    Interface*, IPAddr_t);       // Add routing entry
  //Doc:Desc This method is used to add a routing entry into the routing
  //Doc:Desc table.
  //Doc:Arg1 The destination IP Address
  //Doc:Arg2 The subnet Mask of the destination IP Address
  //Doc:Arg3 The interface to use
  //Doc:Arg4 The next hop IP Address
  
  //Doc:Method  
  virtual RoutingEntry Lookup(Node*, IPAddr_t);  // Find a routing entry
  //Doc:Desc This method implements the lookup mechanism at a  node
  //Doc:Desc for a given destination IP address
  //Doc:Arg1 Node which is looking up the routing table
  //Doc:Arg2 Destination IP address
  //Doc:Return The corresponding routing entry or nil
  
  //Doc:Method    
  virtual RoutingEntry LookupFromPDU(PDU*);      // Find from PDU (NixVector)
  //Doc:Desc This method uses the PDU to look up the next route. Such a
  //Doc:Desc mechanism is used in routing protocols like nix vector routing
  //Doc:Arg1 The pointer to the PDU
  //Doc:Return The corresponding routing entry or nil
  
  //Doc:Method    
  virtual Routing* Clone();                      // Get a Clone of the object
  //Doc:Desc This method is used to make copies of the routing object.
  //Doc:Desc Such a mechanism is useful for example, in topology creation
  //Doc:Return A pointer to the cloned object
  
  //Doc:Method    
  virtual RType_t Type();                        // Find out routing type
  //Doc:Desc This method enables to query the routing object of the
  //Doc:Desc routing protocol it implements
  //Doc:Return the routing type
  
    //Doc:Method
  virtual void    ReInitializeRoutes(Node*, bool);// Re-Initialization
  //Doc:Desc This method responds to topology changes and re-computes
  //Doc:Desc routes as needed.
  //Doc:Arg1 Node pointer for which the routes are to be re-calculated
  //Doc:Arg2 True if node or interface UP, false if DOWN

//Doc:Method    
  virtual bool    NeedReInit();                  // True if re-init needed
  //Doc:Desc This method determines if re-initialization is needed 
  //Doc:Desc in response to a topology change
  //Doc:Return true if initialization is needed
  
  //Doc:Method    
  virtual Size_t  Size() const;                  // Total size (bytes) of FIB
  //Doc:Desc This method gives the size of the routing table. or the
  //Doc:Desc Forwarding Information Base
  //Doc:Desc Size of the FIB
  
  //Doc:Method    
  NixVectorOption*  GetCachedNixVector(IPAddr_t);
  //Doc:Desc Thie method locates a previously cached copy of
  //Doc:Desc the nix vector to the specified  IP address,
  //Doc:Arg1 The destination address
  //Doc:Return the pointer to the NixVectorOption PDU, or nil of none

  //Doc:Method    
  NixVectorOption* GetNixVector(Node*, IPAddr_t);// Get a NixVector to address
  //Doc:Desc Thie method returns a nix vector options PDU correcponding to
  //Doc:Desc an IP address at a specified node
  //Doc:Arg1 The node at which we need the NixVector
  //Doc:Arg2 The destination address
  //Doc:Return the pointer to the NixVectorOption PDU
  
  //Doc:Method
  bool            ValidateNixVector(Node*, IPAddr_t, NixVectorOption);
    //Doc:Desc Validate an existing nixvector.  Used to insure still valid
    //Doc:Desc after a topology change.
    //Doc:Arg1 Source node pointer.
    //Doc:Arg2 Destination IPAddress.
    //Doc:Arg3 Existing NixVector
    //Doc:Return True if valid (does not trasverse a down node  or interface)

  //Doc:Method  
  void            DBDump(Node*);                 // Print debug info
  //Doc:Desc This method is for debugging
  //Doc:Arg1 Pointer to the node at which we need debug info
  
private:
  void    BuildNixVector(Node*, Node*,
                         NodeVec_t&,
                         NixVectorOption&);      // Build the nix vector
private:
  // NixVector caches the NixVectors to each destination
  NixMap_t cache;
  RoutingEntry defaultRoute;                 // Default route
};

#endif

