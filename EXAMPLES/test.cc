#include "simulator.h"
#include "application-tcp.h"
#include "dumbbell.h"
#include "tcp-tahoe.h"
#include "validation.h"

using namespace std;

class MyApp : public TCPApplication {
public:
  MyApp(const TCP& t) : TCPApplication(t) { }
  void Receive(Packet*, L4Protocol*);
  Application* Copy() const { return new MyApp(*this);}
};

void MyApp::Receive(Packet*, L4Protocol*)
{
  cout << "Hello from myapp receive" << endl;
}

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  

  //Node* n1 = new Node();
  //Node* n2 = new Node();
  //Linkp2p l = new Linkp2p();
  //l->Bandwidth(Rate("100Mb"));

  //n1->AddDuplexLink(n2, l);

  //GTITM* gt = new GTITM(...);

  Dumbbell* b = new Dumbbell(20, 20, 0.1,
                             IPAddr("192.1.0.1"),
                             IPAddr("192.2.0.1"));
  Node* nl= b->Left(0);
  Node* nr = b->Right(0);

  // Define server and application
  TCPTahoe t(nr);
  t.Bind(80);
  t.Listen();
  MyApp* app = new MyApp(t);
  t.AttachApplication(app);

  TCPTahoe tc(nl);
  tc.Connect(nr->GetIPAddr(), 80);
  tc.Send(10000);
  //char work[100] = { ..... }
  //tc.Send(work, sizeof(work));
  s.Run();
}
