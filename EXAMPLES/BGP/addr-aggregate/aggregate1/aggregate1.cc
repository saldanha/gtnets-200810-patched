// Sample test program to instantiate a BGP router
// Sunitha Beeram, Georgia Tech, Spring 2005

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "bgp.h"


using namespace std;

int main(int argc,char ** argv)
{
  Simulator Sim;

  int StopTime=200;
  bool sum=false;

  if ( argc > 1 )
  {

	if (! strncmp(argv[1],"sum",3))
		sum=true;
	else
		{
			printf("usage: aggregate1-dbg \n");
			printf("       aggregate1-dbg sum\n\
	 -- this will cause specific updates to be supressed\n");
			exit(1);
	  }
	}

  Node::DefaultShape(Node::CIRCLE);
  //Linkp2p lk(Rate("1.5Mb"), Time("1ms"));
 Linkp2p lk(Rate("500Kb"), Time("20ms"));
  Uniform startRng(0,0.1);

  /*First BGP Router*/
  Node* node1 = new Node();
  BGP* bgpRouter1 = new BGP(0);

  bgpRouter1->AttachNode(node1);
  bgpRouter1->config_file("./bgpd1.conf");

  /*Second BGP Router*/
  Node *node2 = new Node();
  BGP* bgpRouter2 = new BGP(0);
  
  bgpRouter2->AttachNode(node2);
  bgpRouter2->config_file("./bgpd2.conf");
  node1->AddDuplexLink(node2,lk);

  /*Third BGP Router*/
  
  Node *node3 = new Node();
  BGP* bgpRouter3 = new BGP(0);
  
  bgpRouter3->AttachNode(node3);
  bgpRouter3->config_file("./bgpd3.conf");
  node2->AddDuplexLink(node3,lk);

  if ( sum )
  {

	char command1[]="aggregate-address 160.0.0.0 255.0.0.0 summary-only";
	
	BGPEvent *ev = new BGPEvent(BGPEvent::COMMAND);

	ev->argv = (char*)malloc(sizeof(char)*(strlen(command1)+1));
    strcpy(ev->argv,command1);

	bgpRouter2->timer->Schedule(1,ev);

  }
  else
  {

	char command1[]="aggregate-address 160.0.0.0 255.0.0.0";	

	BGPEvent *ev = new BGPEvent(BGPEvent::COMMAND);

	ev->argv = (char*)malloc(sizeof(char)*(strlen(command1)+1));
    strcpy(ev->argv,command1);

	bgpRouter2->timer->Schedule(1,ev);
  }	

  char command[] ="show ip bgp";

  BGPEvent *ev1 = new BGPEvent(BGPEvent::COMMAND);
  ev1->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev1->argv,command);

  bgpRouter1->timer->Schedule(StopTime-1,ev1);

  BGPEvent *ev2 = new BGPEvent(BGPEvent::COMMAND);
  ev2->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev2->argv,command);

  bgpRouter2->timer->Schedule(StopTime-1,ev2);

  BGPEvent *ev3 = new BGPEvent(BGPEvent::COMMAND);
  ev3->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev3->argv,command);

  bgpRouter3->timer->Schedule(StopTime-1,ev3);

  bgpRouter1->StartAt(0.1);
  bgpRouter2->StartAt(0.1);
  bgpRouter3->StartAt(0.1);
  bgpRouter1->StopAt(StopTime);
  bgpRouter2->StopAt(StopTime);
  bgpRouter3->StopAt(StopTime);

  Sim.StopAt(StopTime);
  Sim.Run();
}
 
