// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: itm.c 92 2004-09-14 18:18:43Z dheeraj $



/* 
 *
 * itm.c -- Driver to create graphs using geo(), geo_hier(), and transtub().
 *
 * Each argument is a file containing ONE set of specs for graph generation.
 * Such a file has the following format:
 *    <method keyword> <number of graphs> [<initial seed>]
 *    <method-dependent parameter lines>
 * Supported method names are "geo", "hier", and "ts".
 * Method-dependent parameter lines are described by the following:
 *    <geo_parms> ::=
 *        <n> <scale> <edgeprobmethod> <alpha> [<beta>] [<gamma>]
 *    <"geo" parms> ::= <geo_parms>
 *    <"hier" parms> ::=
 *          <number of levels> <edgeconnmethod> <threshold>
 *          <geo_parms>+  {one per number of levels}
 *    <"ts" parms> ::=
 *          <# stubs/trans node> <#t-s edges> <#s-s edges>
 *          <geo_parms>       {top-level parameters}
 *          <geo_parms>       {transit domain parameters}
 *          <geo_parms>       {stub domain parameters}
 *
 * Note that the stub domain "scale" parameter is ignored; a fraction
 * of the transit scale range is used.  This fraction is STUB_OFF_FACTOR,
 * defined in ts.c.
 * 
 * From the foregoing, it follows that best results will be obtained with
 *   -- a SMALL value for top-level scale parameter
 *   -- a LARGE value for transit-level scale parameter
 * and then the value of stub-level scale parameter won't matter.
 *
 * The indicated number of graphs is produced using the given parameters.
 * If the initial seed is present, it is used; otherwise, DFLTSEED is used.
 *
 * OUTPUT FILE NAMING CONVENTION:
 * The i'th graph created with the parameters from file "arg" is placed
 * in file "arg-i.gb", where the first value of i is zero.
 *
 */

/* Modified by TJ */


#include <stdio.h>
#include <sys/types.h>		/* for NBBY, number of bits/byte */
#include <stdlib.h>		/* for calloc(),atoi(),etc. */
#include <string.h>		/* for strtok() */
#include "gb_graph.h"
#include "geog.h"

#define LINE 512
#define GEO_KW	"geo"
#define HIER_KW	"hier"
#define TS_KW	"ts"
#define GEO	100
#define HIER	101
#define TS	102

char *delim = " \t\n", *nonestr = "<none>";
static char  errstr[256];


char *
get_geoparms(FILE * f, geo_parms * pp)
{
char *cp;
char inbuf[LINE];

    do {
	if (feof(f))
	    return "file ended unexpectedly";
	fgets(inbuf, LINE - 1, f);
	cp = strtok(inbuf, delim);
    } while ((cp == NULL) || (*cp == '#'));

    if ((pp->n = atoi(cp)) <= 0)
	return "bad n parameter";

    cp = strtok(0, delim);
    if (cp == NULL || (pp->scale = atoi(cp)) <= 0)
	return "bad scale parameter";

    cp = strtok(0, delim);
    if (cp == NULL) return "bad edge method parameter";
    pp->method = atoi(cp);
    if (pp->method < 1 || pp->method > 6)
			return "bad edge method parameter";

    cp = strtok(0, delim);
    if (cp == NULL) return "bad alpha parameter";
    pp->alpha = atof(cp);
    if (pp->alpha < 0.0 || pp->alpha > 1.0)
			return "bad alpha parameter";

    cp = strtok(0, delim);
    if (cp != NULL) {
	       if ((pp->beta = atof(cp)) < 0.0)
	       return "bad beta parameter";
    } else pp->beta = 0.0;

    cp = strtok(0,delim);
    if (cp != NULL) {
			 if ((pp->gamma = atof(cp)) < 0.0)
	   return "bad gamma parameter";
    } else pp->gamma = 0.0;

    return NULL;
}				/* get_parms */

char *
get_hierparms(FILE *f,
                int *nLev,
		int *ecMeth,
		int *haux,
                geo_parms *parmsbuf)
{
char inbuf[LINE];
char *cp;
int i;

    do {
	if (feof(f))
	    return "file ended unexpectedly";
	fgets(inbuf, LINE - 1, f);
	cp = strtok(inbuf, delim);
    } while ((cp == NULL) || (*cp == '#'));

    *nLev = atoi(cp);
    if (*nLev < 0 || *nLev > MAXLEVEL)
	return "bad number of levels";


    cp = strtok(0, delim);
    if (cp == NULL || (*ecMeth = atoi(cp)) < 0)
	return "bad/missing edge connection method";

    cp = strtok(0, delim);
    if (cp != NULL)
	*haux = atoi(cp);

    for (i=0; i<*nLev; i++)
	if (cp = get_geoparms(f,&parmsbuf[i]))
	    return cp;

    return NULL;
}

char *
get_tsparms(FILE *f,
                int *SpT, int *TSe, int *SSe,
                geo_parms *parmsbuf)
{
char inbuf[LINE];
char *cp;
int i;

    if (SpT==NULL ||
	SSe==NULL ||
	TSe==NULL)
	return "bad input parameter (null ptr)";

    do {
	if (feof(f))
	    return "file ended unexpectedly";
	fgets(inbuf, LINE - 1, f);
	cp = strtok(inbuf, delim);
    } while ((cp == NULL) || (*cp == '#'));

    if ((*SpT = atoi(cp)) < 0)
      return "bad stubs/trans parameter";
    if(1)printf("stubs/trans parameter is %d \n", *SpT);

    cp = strtok(0, delim);

    if (cp == NULL)
	return "missing stub-stub edge parameter";
    
    if ((*TSe = atoi(cp)) < 0)
	return "bad transit-stub edge parameter";
    if(1)printf("trans-stub edge parameter is %d \n", *TSe);

    cp = strtok(0, delim);

    if (cp == NULL)
	return "missing transit-stub edge parameter";

    if ((*SSe = atoi(cp))  < 0)
	return "bad stub-stub edge parameter";
    if(1)printf("stub-stub edge parameter is %d \n", *SSe);

    for (i=0; i<3; i++)
	if (cp = get_geoparms(f, &parmsbuf[i]))
	    return cp;

    return NULL;
}


Graph *
makegraph(char *iname)
{
FILE *infile;
Graph *G = NULL;
char inbuf[LINE],outfilename[LINE];
register char *cp;
char *method;
int count, lineno=0, numlevels, nerrors;
int i, m=0, prefixlen;
int edgeConnMeth, haux, stubsPerTrans, tsEdges, ssEdges;
long seed;
geo_parms	parmsbuf[MAXLEVEL];	/* make sure MAXLEVEL >= 3 */
int NodeSum = 0;
int EdgeSum = 0;

    if ((infile = fopen(iname, "r")) == NULL) {
        sprintf(errstr, "can't open input file %s", iname);
        die(errstr);
    }

    /* set up output file name */

    sprintf(outfilename,"%s-",iname);
    prefixlen = strlen(outfilename);
    
    do {
        fgets(inbuf, LINE - 1, infile); lineno++;
        method = strtok(inbuf, delim);
    } while (((method == NULL) || (*method == '#'))
            && !feof(infile));
		/* skip over comments  and blank lines */

    if ((cp = strtok(NULL, delim))==NULL){
      sprintf(errstr, "missing <number of graphs>");
      return NULL;
    }

    count = atoi(cp);

    if ((cp = strtok(NULL, delim))==NULL)
	seed = 0;
    else
      seed = atol(cp);
    
    if (strcmp(method,GEO_KW)==0) {
      
      if (cp = get_geoparms(infile,parmsbuf)) 
        {
          printf("cp is %s \n",cp);
          return NULL;
        }
      m = GEO;
      
    } else if (strcmp(method,HIER_KW)==0) {
      
      if (cp = get_hierparms(infile, 
                             &numlevels, &edgeConnMeth, &haux, parmsbuf))
        {
          printf("cp is %s \n",cp);
          return NULL;
        }
      m = HIER;
      
    } else if (strcmp(method,TS_KW)==0) {
      
      if (cp = get_tsparms(infile,
                           &stubsPerTrans, &tsEdges, &ssEdges, parmsbuf))
        {
          printf("cp is %s \n",cp);
          return NULL;
        }
      m = TS;
    } else {
      sprintf(errstr,"Unknown generation method %s",method);
      return NULL;
    }
    
    
    if (seed)
      gb_init_rand(seed);
    else
      gb_init_rand(DEFAULT_SEED);
    
    for (i=0; i<count; i++) {
      sprintf(outfilename+prefixlen,"%d.gb",i);
      switch(m) {
      case GEO:
        do {
          gb_recycle(G);
          G = geo(0,parmsbuf);
        } while (G != NULL && !isconnected(G));
        break;
      case HIER:
        G = geo_hier(0,numlevels,edgeConnMeth,haux,parmsbuf);
        break;
      case TS:
        G = transtub(0,stubsPerTrans,tsEdges,ssEdges,&parmsbuf[0],
                     &parmsbuf[1],&parmsbuf[2]);
        break;
      default:
        {
          printf("This can't happen!\n");
          return NULL;
        }
	
      }	/* switch */
      
      if (G==NULL) {
        sprintf(errstr,"Error creating graph %d, trouble code=%d",i,
                gb_trouble_code);
        return NULL;
      }
      
      //nerrors = save_graph(G, outfilename);	
      //if (nerrors > 0)
      //fprintf(stderr, "%s ha  d %d anomalies\n", outfilename,nerrors); */
            
      //gb_recycle(G);
    }	/* for */
    
    return G;
}

#ifdef USE_MAIN
main(int argc, char **argv)
{
    FILE           *infile;
    char	   *rv;
    char           *badinpstr = "badly formed input file %s, continuing\n";

    if (argc == 1) {
	printf("itm <spec-file0> <spec-file1> ....\n\n");
	return;
    }
    while (--argc) {

        if (rv = makegraph(*(++argv))) {
	    fprintf(stderr,"Error processing file %s: %s\n",*argv,rv);
	}

    }	/* while on argc */

    exit(0);
}
#endif
