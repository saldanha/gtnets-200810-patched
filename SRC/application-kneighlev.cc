// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-kneighlev.cc 456 2006-01-20 17:37:47Z riley $



#include "debug.h"
#include "application-kneighlev.h"
#include "udp.h"
#include "node.h"
#include "simulator.h"
#include "interface.h"
#include "debug.h"
#include "interface-wireless.h"

#include <assert.h>
#include <iostream>
#include <cstring>

using namespace std;

#ifdef USE_CISCO_AIRONET350
  // Cisco Aironet 350 Discrete Power Levels in mW 
  double KNeighLevApplication::txPower[] = {0.001, 0.005, 0.020, 
					    0.030, 0.050, 0.100};
  double KNeighLevApplication::radioRange[] = {24.0, 55.0, 109.0, 
					       134.0, 173.0, 244.0};
  double KNeighLevApplication::MAX_POWER = 0.100;
  // TL is the timeout between stepping power levels
  double KNeighLevApplication::TL[] = {0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
#endif

unsigned KNeighLevApplication::K_CONST = 0; // must be modified from simulation code

double KNeighLevApplication::GetTxPowerFromRange(Meters_t range)
{
  int i = 0;
  do
  {
    if (radioRange[i] == range)
      return txPower[i];
    else
      i++;
  } while (radioRange[i] != MAX_POWER);
  return -1.0;
}

KNeighLevApplication::KNeighLevApplication(Node *n, Time_t start)
  : node(n), iw(nil), ip(IPADDR_NONE), l4Proto(nil),
    pendingEvent(0), myTxPowerLevel(0), startTime(start),
    changedPower(false)
{
  
}

KNeighLevApplication::KNeighLevApplication(const KNeighLevApplication &c)
  : l4Proto(nil)
{
}

Application* KNeighLevApplication::Copy() const
{
  return new KNeighLevApplication(*this);
}

void KNeighLevApplication::Handle(Event* e, Time_t t)
{
  //cout << "In handle..." << endl;;
  AppKNeighLevEvent *ev = (AppKNeighLevEvent*)e;
  //cout << "\n" << IPAddr::ToDotted(ip) << "\n" 
  //     << "Nsymmetric.size(): " << Nsymmetric.size() << "." << endl;
  switch (ev->event) {
  case AppKNeighLevEvent::START:
  {
    DEBUG0((cout << "we got a start event" << endl));
    // initialize this node's power level
    SetTxPowerLevel(0);
    // send beacon message
    SendMessage(K_BEACON);
    
    //Time_t nextTime = t + TL[myTxPowerLevel]; 
    Time_t nextTime = TL[myTxPowerLevel]; 
    AppKNeighLevEvent* ev = new AppKNeighLevEvent(AppKNeighLevEvent::TIMEOUT);
    pendingEvent = ev;
    Scheduler::Schedule(ev, nextTime, this); // Schedule again
    break;
  }
  case AppKNeighLevEvent::TIMEOUT:
  {
    //cout << "Got a TIMEOUT event." << endl;
    // if node's symmetric neighbors list has at least k nodes
    if (Nsymmetric.size() >= K_CONST)
    {
      //  stop
      //cout << IPAddr::ToDotted(ip) 
      //     << ": We've reached k at time " << t << " seconds." << endl;
      pendingEvent = NULL;
      break;
    }
    // else if current node's power level is maximum
    else if (KNeighLevApplication::txPower[myTxPowerLevel] >= MAX_POWER)
    {
      //  stop
      //cout << IPAddr::ToDotted(ip) 
      //     << ": We've reached maximum power." << endl;
      pendingEvent = NULL;
      break;
    }
    // else 
    else
    {
      //cout << "We need to increment power levels and try again." << endl;
      // increment power level
      if (!changedPower)
	SetTxPowerLevel(myTxPowerLevel + 1);
      changedPower = false;
      //  send help message
      SendMessage(K_HELP);
    }

    //Time_t nextTime = t + TL[myTxPowerLevel]; // Time till next packet
    Time_t nextTime = TL[myTxPowerLevel]; 
    if (pendingEvent) delete pendingEvent;
    ev = new AppKNeighLevEvent(AppKNeighLevEvent::TIMEOUT);
    pendingEvent = ev;
    Scheduler::Schedule(ev, nextTime, this); // Schedule again
    //cout << "returning from handle..." << endl;
    return;
  }
  }
  Application::Handle(e, t);  // May be application event
  //cout << "leaving handle..." << endl;
}

// Application Methods
void KNeighLevApplication::StartApp()    // Called at time specified by Start
{
  // Copied from original constructor by GFR
  l4Proto = new UDP();
  //cout << "in KNeighLev Application constructor...\n";
  IFVec_t ifs = node->Interfaces();
  
  iw = NULL;
  for(IFVec_t::iterator i = ifs.begin(); i != ifs.end(); ++i)
    {
      DEBUG0((cout << "Node " << (string)IPAddr(node->GetIPAddr())
              << " checking iface " << (*i) << endl));
      if ((*i)->IsWireless())
	{
	  iw = (InterfaceWireless*)(*i);
          DEBUG0((cout << "Found a wireless interface" << endl));
	  break;
	}
    }
  
  if (iw == NULL)
    {
      cout << "KNeighLev: node does not have wireless interface." << endl;
      return;
    }
  
  //cout << "found the wireless interface...\n";
  l4Proto->Attach(node);
  //node->Bind(l4Proto->Proto(), K_PORT, l4Proto);
  if (l4Proto->Bind(K_PORT) == NO_PORT)   
    // Bind the local l4 protocol to available port
    {
      cout << "KNeighLev Application Local Protoocl Bind Failed!" << endl;
      return;
    }
  if (node->Bind(l4Proto->Proto(), K_PORT, l4Proto))
    {
      cout << "KNeighLev Application Local Protoocl Bind Failed!" << endl;
      return;
    }
  l4Proto->AttachApplication(this); // Attached application
  //cout << "Attached application...done with constructor.\n";

  // End of copied from constructor
  StopApp();                               // Insure no pending event
  l4Proto->Connect(IPAddrBroadcast, K_PORT);  

  AppKNeighLevEvent* ev = new AppKNeighLevEvent(AppKNeighLevEvent::START);
  pendingEvent = ev;
  Scheduler::Schedule(ev, startTime, this); // Schedule again
}

void KNeighLevApplication::StopApp()     // Called at time specified by Stop
{
  if (pendingEvent)
    { // Cancel the pending transmit event
      Scheduler::Cancel(pendingEvent);
      pendingEvent = NULL;
    }
}

void KNeighLevApplication::Receive(Packet* packet, L4Protocol *, Seq_t)
{
  Data *d = (Data *)packet->PeekPDU();
  struct KNeighLevHdr *hdr = (struct KNeighLevHdr *)(d->data);
  int level = hdr->txPowerLevel;
  IPAddr_t v = hdr->ip;
  // if message is a beacon
  if (hdr->type == K_BEACON)
  {
    //cout << IPAddr::ToDotted(ip); 
    //cout << ": Received a BEACON from " << IPAddr::ToDotted(v) 
    // << " with power level: " << KNeighLevApplication::txPower[level] 
    // << "." << endl;
    //  if v (source node) is not in the incoming neighbor list
    if (Nincoming.find(v) == Nincoming.end())
    {
      //   add v to the incoming neighbor list
      Nincoming.insert(TxPowerDataMap_t::value_type(v, level));
      //   if my power level is greater than or equal to v's
      if (myTxPowerLevel >= level)
	//    add v to the symmetric neighbor list
	Nsymmetric.insert(TxPowerDataMap_t::value_type(v, level));
    }
  }
  // if message is a help
  else if (hdr->type == K_HELP)
  {
    //cout << IPAddr::ToDotted(ip); 
    //cout << ": Received a HELP msg from " << IPAddr::ToDotted(v)
    // << " with power level: " << KNeighLevApplication::txPower[level] 
    // << "." << endl;
    //  if v (source node) is in the incoming neighbor list
    //   and not in the symmetric neighbor list
    if (Nincoming.find(v) != Nincoming.end() &&
	Nsymmetric.find(v) == Nsymmetric.end())
    {
      //    step-wise increase to v's power level
      StepWiseIncrease(myTxPowerLevel + 1, level);
    }
    //  else if v is not in the incoming neighbor list
    else if (Nincoming.find(v) == Nincoming.end())
    {
      //   add v to the incoming neighbor list
      Nincoming.insert(TxPowerDataMap_t::value_type(v, level));
      //   if my power level is less than v's
      if (myTxPowerLevel < level)
      {
	//    step-wise increase to v's power level
	StepWiseIncrease(myTxPowerLevel + 1, level);
      }
      //   else
      else
      {
	//    add v to the symmetric neighbor list
	Nsymmetric.insert(TxPowerDataMap_t::value_type(v, level));
      }
    }
  }
  else
    cout << IPAddr::ToDotted(ip) 
	 << ": Received an invalid message." << endl;

  delete packet;
}

void KNeighLevApplication::SendMessage(KMsgType_t type)
{
#ifdef CHANGED_BY_GFR
  struct KNeighLevHdr *hdr = new struct KNeighLevHdr;
  memset((void *)hdr, 0, sizeof(struct KNeighLevHdr));
  hdr->type = type;
  hdr->txPowerLevel = myTxPowerLevel;
  hdr->ip = ip;
  //cout << IPAddr::ToDotted(ip) 
  //     << ((type == K_BEACON) ? ": BEACON sent." : ": HELP sent.") << endl;
  l4Proto->SendTo((char*)hdr, hdr->size(), IPAddrBroadcast, K_PORT);
#endif
  struct KNeighLevHdr hdr;
  memset((void*)&hdr, 0, sizeof(hdr));
  hdr.type = type;
  hdr.txPowerLevel = myTxPowerLevel;
  hdr.ip = ip;
  l4Proto->SendTo((char*)&hdr, sizeof(hdr), IPAddrBroadcast, K_PORT);
}

void KNeighLevApplication::SetTxPowerLevel(int level)
{
  myTxPowerLevel = level;
  assert(iw);
  iw->setTxPower(KNeighLevApplication::txPower[myTxPowerLevel]);
  node->SetRadioRange(KNeighLevApplication::radioRange[myTxPowerLevel]);
  //cout << IPAddr::ToDotted(ip) 
  //     << ": Power set to " << iw->getTxPower() << " mW. "
  //     << "Range set to " << node->GetRadioRange() << " m." << endl;
  changedPower = true;
}

void KNeighLevApplication::StepWiseIncrease(int levI, int levF)
{
  //cout << IPAddr::ToDotted(ip) 
  //     << ": in step-wise increase..." << endl;
  //cout << "levI: " << levI << ", levF: " << levF << endl;
  for (int h = levI; h <= levF; h++)
  {
    SetTxPowerLevel(h);
    SendMessage(K_BEACON);

    // if there exists a node i in the incoming neighbor list
    //  with the same power level
    //   add it to the symmetric neighbor list
    TxPowerDataMap_t::iterator i;
    for (i = Nincoming.begin(); i != Nincoming.end(); i++)
      if (i->second == h)
	Nsymmetric.insert(*i);
  }
  //cout << "leaving step-wise increase..." << endl;
}
