// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: linkp2p.cc 460 2006-01-31 20:48:39Z riley $



// Georgia Tech Network Simulator - Point to Point Link class
// George F. Riley.  Georgia Tech, Spring 2002

#include "debug.h"
#include "linkp2p.h"
#include "simulator.h"
#include "macaddr.h"
#include "qtwindow.h"
#include "eventcq.h"

#ifdef HAVE_QT
#include <qcanvas.h>
#endif

using namespace std;

Linkp2p* Linkp2p::defaultp2p; // Default p2p link pointer
Count_t  Linkp2p::count = 0;

Linkp2p::Linkp2p()
    : LinkReal(), pPeer(nil), linkAnimation(nil), usedAnim(0)
{
  count++;
}

Linkp2p::Linkp2p(Rate_t b, Time_t d)
    : LinkReal(b, d), pPeer(nil), linkAnimation(nil), usedAnim(0)
{ 
  count++;
}

Linkp2p::Linkp2p(const Linkp2p& c) : LinkReal(c)
{
  pPeer = nil;  // Don't copy the peer or local pointers pointer
  linkAnimation = nil;
  usedAnim = 0;
  count++;
}

Linkp2p::~Linkp2p()
{
  count--;
#ifdef HAVE_QT
  delete linkAnimation;
#endif
}

Link* Linkp2p::Copy() const
{
  return new Linkp2p(*this);
}

bool Linkp2p::Transmit(Packet* p, Interface* i, Node* n)
{
  DEBUG0((cout << "Linkp2p::Transmit to peer " << GetPeer(p) << endl));
  return TransmitHelper(p, i, n, (Interface*)GetPeer(p));
}

bool Linkp2p::Transmit(Packet* p, Interface* i, Node* n, Rate_t)
{ // Transmit with rate is used on wireless links only
  return Transmit(p, i, n);
}

MACAddr Linkp2p::IPToMac(IPAddr_t)
{ // Convert peer IP to peer MAC
  // For point to point, this is just MAC address of peer
  if (pPeer) return ((Interface*)pPeer)->GetMACAddr();
  return MACAddr::NONE; // Else none
}

Count_t Linkp2p::NeighborCount(Node* n)
{ // Get number of routing neighbors (always 1 for p2p links)
  return 1;
}

void Linkp2p::Neighbors(NodeWeightVec_t& nwv, Interface* i, bool)
{ // Add to neighbor list
  // Point to point has only one peer
  // last param (forceAll) meaningless on p2p link.
  nwv.push_back(NodeIfWeight(pPeer->GetNode(), i, Weight()));
}

void Linkp2p::AllNeighbors(NodeWeightVec_t& nwv)
{ // Add to neighbor list
  // Point to point has only one peer.  Also note interface is
  // peer, not current as in Neighbors above.
  nwv.push_back(NodeIfWeight(pPeer->GetNode(), pPeer, Weight()));
}

Count_t  Linkp2p::PeerCount()       // Return number of peers
{
  return 1; // Point to point links have exactly 1 peer
}

IPAddr_t Linkp2p::PeerIP(Count_t npeer)  // Get peer IP Address (if known)
{
  if (npeer > 0)
    return IPADDR_NONE;      // Oops, value out of range
  // Return one and only peer's ip address  
  return ((Interface*)pPeer)->GetIPAddr();
}

IPAddr_t Linkp2p::NodePeerIP(Node* n)  // Get peer IP Address (if known)
{ // Find the peer's ip address for peer n on this link
  // For point to point, there is only one peer, so it must match
  Node* peerNode = pPeer->GetNode();
  if (n != peerNode)
    return IPADDR_NONE;      // Oops, not correct peer
  // Return one and only peer's ip address  
  return ((Interface*)pPeer)->GetIPAddr();
}

bool     Linkp2p::NodeIsPeer(Node* n)
{ // Return true if specified node is a peer
  Node* peerNode = pPeer->GetNode();
  return (peerNode == n);
}

Count_t  Linkp2p::NodePeerIndex(Node* n)
{
  return 0; // Point to point links always have only one peer
}

IPAddr_t Linkp2p::DefaultPeer(Interface*)
{ // Get IP Addr of default peer
  return PeerIP(0); // For p2p links, default is same as only
}

// Static methods
Linkp2p& Linkp2p::Default()
{ // Get the default link
  if (!defaultp2p) defaultp2p = new Linkp2p(); // Allocate it
  return *defaultp2p;                           // Return it
}

Linkp2p& Linkp2p::Default(const Linkp2p& l)    // Set new default
{
  if (defaultp2p) delete defaultp2p;           // Delete if already exists
  defaultp2p = (Linkp2p*)l.Copy();             // Set new one
  return *defaultp2p;
}

bool Linkp2p::ScheduleFailure(Time_t t)
{
  LinkEvent* evFail = new LinkEvent (LinkEvent::LINK_FAIL, nil);
  Scheduler::Schedule(evFail, t, pPeer);
  return true;
}


bool Linkp2p::ScheduleRestore(Time_t t)
{
  LinkEvent* evRestore = new LinkEvent (LinkEvent::LINK_RESTORE, nil);
  Scheduler::Schedule(evRestore, t, pPeer);
  return true;
}


void Linkp2p::AnimateLink()
{
#ifdef HAVE_QT
  InterfaceBasic* pLocal = GetLocalIf();
  if (!pPeer || !pLocal) return; // Can't animate without interfaces
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  if (!qtw) return;
  Node* s = pLocal->GetNode();   // Local node
  Node* d = pPeer->GetNode();    // Remote node
  QPoint fp = qtw->NodeLocation(s);
  QPoint tp = qtw->NodeLocation(d);
  if (!linkAnimation)
    {
      // Create a new canvas item
      linkAnimation = new QCanvasLine(qtw->Canvas());
    }
  
  linkAnimation->show();
  linkAnimation->setPoints(fp.x(), fp.y(), tp.x(), tp.y());
  // If interface is down in either direction color it red
  if (pLocal->IsDown() || pPeer->IsDown())
    {
      linkAnimation->setPen(Qt::red);
    }
#endif
}

void Linkp2p::ReAnimateLink()
{
}

void Linkp2p::AnimatePackets()
{
#ifdef HAVE_QT
  InterfaceBasic* pLocal = GetLocalIf();
  if (!pPeer || !pLocal) return; // Can't animate without interfaces
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  if (!qtw) return;
  // First hide any existing packets
  for (PacketVec_t::size_type i = 0; i < usedAnim; ++i)
    {
      animatedPackets[i]->hide();
    }
  usedAnim = 0; // Reset to none used
  // The destination interface has a list of pending packets
  EventCQ* pending = pPeer->GetEventCQ();
  if (!pending) return; // No event cq yet, so no packets
  while(qtw->PlaybackMode() && !pending->empty())
    { // remove any rx events n the past
      EventPair* ep = pending->front();
      if (ep->time > Simulator::Now()) break; // Received in future, keep
      pending->pop_front();       // Remove this one
      LinkEvent* evRx = (LinkEvent*)ep->event;
      DEBUG0((cout << "QTW removing evRx time " << ep->time
              << " now " << Simulator::Now() << endl));
      delete evRx->p;
      delete evRx;
    }
  DEBUG0((cout << "AMP, Time " << Simulator::Now()
          << " local " << pLocal << " peer " << pPeer
          << " pending.size() " << pending->size() << endl));
  
  if (pending->empty()) return; // No packets

  for (Count_t j = 0; j < pending->size();  ++j)
    {
      EventPair* ev = (*pending)[j];
      LinkEvent* le = (LinkEvent*)(ev->event);
      // Ignore first bit events for satellite interfaces
      if (le->event != LinkEvent::PACKET_RX) continue;
      Packet*    p = le->p;
      QColor     c(Qt::blue);
      // See if the packet has a color assigned
      if (p->IsColored())
        {
          c = QColor(p->R(), p->G(), p->B());
        }
      else
        { // Display BER pkts in red if not other color specified
          if (le->hasBitError) c = Qt::red;
        }
          
      // The timestamp in the event is last bit received.
      // We also need first bit time.
      Count_t    pSize = p->Size()*8; // Size in bits
      pSize -= pSize/10; // Shorten a bit to show breaks between packets
      Time_t     lastBitTime  = ev->time;
      Time_t     firstBitTime = ev->time - (double)(pSize)/Bandwidth();
      // Check if Pkt sent in future (playback). ignore
      if ((firstBitTime - Delay()) > Simulator::Now())
        {
          DEBUG0((cout << "Ignoring pkt sent in future" << endl));
          break;
        }

      // Check if last bit received in the past (playback), ignore
      if (lastBitTime < Simulator::Now())
        {
          DEBUG0((cout << "Ignoring pkt received in past" << endl));
          break;
        }

      if (firstBitTime < Simulator::Now()) firstBitTime = Simulator::Now();
      Time_t fbFuture = firstBitTime - Simulator::Now();
      Time_t lbFuture = lastBitTime - Simulator::Now();
      if (lbFuture > Delay()) lbFuture = Delay();
      // Compute fraction of the link delay for first and last bit times.
      // This is the "distance" along the link line for the display
      Mult_t dfb = 1.0 - fbFuture/Delay();
      Mult_t dlb = 1.0 - lbFuture/Delay();
      DEBUG0((cout << "Displaying pkt, dfb " << dfb 
              << " dlb " << dlb << endl));
      Node* s = pLocal->GetNode(); // Source node
      Node* d = pPeer->GetNode();  // Destination node
      DisplayOnePacket(s, d, dfb, dlb, c); // Display the packet
    }
#endif
}

// Private helpers
void Linkp2p::DisplayOnePacket(Node* s, Node* d,
                                Mult_t firstBit, Mult_t lastBit,
                                const QColor& c)
{
#ifdef HAVE_QT
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  QPoint sp = qtw->NodeLocation(s); // Get location of source
  QPoint dp = qtw->NodeLocation(d); // Get location of destination
  // Compute length of line connecting src/dst
  double dx = dp.x() - sp.x();
  double dy = dp.y() - sp.y();
  DEBUG(2,(cout << " sp.x " << sp.x() << " sp.y " << sp.y()
           << " dp.x " << dp.x() << " dp.y " << dp.y()
           << " dx " << dx << " dy " << dy << endl));
  double dist = sqrt(dx*dx + dy*dy);
  DEBUG(2,(cout << "Distance node " << s->Id() << " to " << d->Id() 
           << " is " << dist << endl));
  // Compute the angle of a line from src to dst
  double theta = atan2(dy, dx);
  DEBUG(2,(cout << "Angle is " << 360.0 * theta / ( 2.0 * M_PI)
           << " degrees" << endl));
  bool verbose = (theta == 0.0);
  verbose = false;
  
  double sinTheta = sin(theta);
  double cosTheta = cos(theta);
  double pxfb = sp.x() + dist * firstBit * cosTheta;
  double pyfb = sp.y() + dist * firstBit * sinTheta;
  double pxlb = sp.x() + dist * lastBit  * cosTheta;
  double pylb = sp.y() + dist * lastBit  * sinTheta;
  // Use QCanvasPolygon for packets
  QPointArray qpa(5); // Four Points for rectangle, plus closing point
  // Compute points 2 pixels left and right of specified
  // Also translate the packet so we are 4 pixels "to the right"
  // of the link line, allowing bi-directional packets to not
  // interfere with each other.
  double sinThetaM2 = sin(theta + M_PI_2);
  double cosThetaM2 = cos(theta + M_PI_2);
  double width = 2.0;

  double pxlfb = pxfb - width * sinTheta + 4 * cosThetaM2;
  double pylfb = pyfb + width * cosTheta + 4 * sinThetaM2;
  double pxrfb = pxfb + width * sinTheta + 4 * cosThetaM2;
  double pyrfb = pyfb - width * cosTheta + 4 * sinThetaM2;
  qpa.setPoint(0, (int)pxlfb, (int)pylfb);
  qpa.setPoint(1, (int)pxrfb, (int)pyrfb);
  double pxllb = pxlb - width * sinTheta + 4 * cosThetaM2;
  double pyllb = pylb + width * cosTheta + 4 * sinThetaM2;
  double pxrlb = pxlb + width * sinTheta + 4 * cosThetaM2;
  double pyrlb = pylb - width * cosTheta + 4 * sinThetaM2;
  qpa.setPoint(2, (int)pxrlb, (int)pyrlb);
  qpa.setPoint(3, (int)pxllb, (int)pyllb);
  qpa.setPoint(4, (int)pxlfb, (int)pylfb); // Close the polygon

  // Get a canvas polygon to use
  if (animatedPackets.size() == usedAnim)
    {
      animatedPackets.push_back(new QCanvasPolygon(qtw->Canvas()));
    }
  QCanvasPolygon* cp = animatedPackets[usedAnim];
  cp->setPen(Qt::black);
  cp->setBrush(c);
  cp->show();
  cp->setPoints(qpa);
  usedAnim++;
#endif
}

