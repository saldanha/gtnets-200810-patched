// Simple GTNetS example
// George F. Riley, Georgia Tech, Winter 2002

#include "simulator.h"      // Definitions for the Simulator Object
#include "node.h"           // Definitions for the Node Object
#include "linkp2p.h"        // Definitions for point-to-point link objects
#include "ratetimeparse.h"  // Definitions for Rate and Time objects
#include "application-tcpserver.h" // Definitions for TCPServer application
#include "application-tcpsend.h"   // Definitions for TCP Sending application
#include "tcp-tahoe.h"      // Definitions for TCP Tahoe

int main()
{
  // Create the simulator object    
  Simulator s;

  // Create and enable IP packet tracing
  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->IPDotted(true);            // Trace IP addresses in dotted notation
  tr->Open("intro1.txt");        // Create the trace file
  TCP::LogFlagsText(true);       // Log TCP flags in text mode
  IPV4::Instance()->SetTrace(Trace::ENABLED);  // Enable IP tracing all nodes

  // Create the nodes
  Node* c1 = new Node();    // Client node 1
  Node* c2 = new Node();    // Client node 2
  Node* r1 = new Node();    // Router node 1
  Node* r2 = new Node();    // Router node 2
  Node* s1 = new Node();    // Server node 1
  Node* s2 = new Node();    // Server node 2

  // Create a link object template, 100Mb bandwidth, 5ms delay
  Linkp2p l(Rate("100Mb"), Time("5ms"));
  // Add the links to client and server leaf nodes
  c1->AddDuplexLink(r1, l, IPAddr("192.168.0.1")); // c1 to r1
  c2->AddDuplexLink(r1, l, IPAddr("192.168.0.2")); // c2 to r1
  s1->AddDuplexLink(r2, l, IPAddr("192.168.1.1")); // s1 to r2
  s2->AddDuplexLink(r2, l, IPAddr("192.168.1.2")); // s2 to r2

  // Create a link object template, 10Mb bandwidth, 100ms delay
  Linkp2p r(Rate("10Mb"), Time("100ms"));
  // Add the router to router link
  r1->AddDuplexLink(r2, r);

  // Create the TCP Servers
  TCPServer* server1 = (TCPServer*)s1->AddApplication(TCPServer(TCPTahoe()));
  if (server1)
    {
      server1->BindAndListen(80); // Application on s1, port 80
      server1->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
    }
  
  TCPServer* server2 = (TCPServer*)s2->AddApplication(TCPServer(TCPTahoe()));
  if (server2)
    {
      server2->BindAndListen(80); // Application on s2, port 80
      server2->SetTrace(Trace::ENABLED); // Trace TCP actions at server2
    }

  // Set random starting times for the applications
  Uniform startRv(0.0, 2.0);

  // Create the TCP Sending Applications
  TCPSend* client1 = (TCPSend*)c1->AddApplication(
      TCPSend(s1->GetIPAddr(), 80,
              Uniform(1000,10000),
              TCPTahoe()));
  if (client1)
    {
      // Enable TCP trace for this client
      client1->SetTrace(Trace::ENABLED);
      // Set random starting time
      client1->Start(startRv.Value());
    }
  
  TCPSend* client2 = (TCPSend*)c2->AddApplication(
      TCPSend(s2->GetIPAddr(), 80,
              Constant(100000),
              TCPTahoe()));
  if (client2)
    {
      // Enable TCP trace for this client
      client2->SetTrace(Trace::ENABLED);
      // Set random starting time
      client2->Start(startRv.Value());
    }

  s.Progress(1.0);                // Request progress messages
  s.StopAt(10.0);                 // Stop the simulation at time 10.0
  s.Run();                        // Run the simulation
  std::cout << "Simulation Complete" << std::endl;
}

