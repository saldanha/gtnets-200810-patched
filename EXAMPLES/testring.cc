// Test the ring topology
// Monirul I Sharif, Georgia Institute of Technology

#include <iostream>
#include <stdio.h>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "ring.h"
#include "tree.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "application-worm.h"
#include "validation.h"  

#ifdef HAVE_QT
#include <qnamespace.h>
#endif

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Simulator s;
  Count_t nc = 10;

  if (argc > 1)
    {
      nc = atol(argv[1]);
    }

  // Set node shape to a circle for animation
  Node::DefaultShape(Node::CIRCLE);
 
  //Trace* tr = Trace::Instance(); // Get a pointer to global trace object


  Linkp2p lk(Rate("10Mb"), Time("20ms"));

  Ring *r = new Ring(nc, lk, IPAddr("192.168.0.1"));

  r->BoundingBox(Location(0,0), Location(5,5));
      
  // Specify animation
  if (!Validation::noAnimation)
    {
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Progress(1);
  s.StopAt(100);
  s.Run();
}
