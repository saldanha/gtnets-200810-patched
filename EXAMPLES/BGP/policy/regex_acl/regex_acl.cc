// Sample test program to instantiate a BGP router
// Sunitha Beeram, Georgia Tech, Spring 2005

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "bgp.h"


using namespace std;

int main(int argc,char ** argv)
{
    Simulator Sim;
    
    int StopTime=100;

    
    /* If stop time is specified as cli arg*/

    if ( argc > 1)
    {
	StopTime= atoi(argv[1]);
	
	if (StopTime < 100)
            StopTime =100;
    }
    
    
    Node::DefaultShape(Node::CIRCLE);
    //Linkp2p lk(Rate("1.5Mb"), Time("1ms"));
    Linkp2p lk(Rate("500Kb"), Time("20ms"));
    Uniform startRng(0,0.1);
    
    /*First BGP Router*/
    Node* node1 = new Node();
    BGP* bgpRouter1 = new BGP(0);
    
    bgpRouter1->AttachNode(node1);
    bgpRouter1->config_file("./bgpd1.conf");
    
    /*Second BGP Router*/
    Node *node2 = new Node();
    BGP* bgpRouter2 = new BGP(0);
    
    bgpRouter2->AttachNode(node2);
    bgpRouter2->config_file("./bgpd2.conf");
    node1->AddDuplexLink(node2,lk);

    /*Third BGP Router*/
  
    Node *node3 = new Node();
    BGP* bgpRouter3 = new BGP(0);
    
    bgpRouter3->AttachNode(node3);
    bgpRouter3->config_file("./bgpd3.conf");
    //node2->AddDuplexLink(node3,lk);

    
    /*Fourth BGP Router*/ 
  Node *node4 = new Node();
    BGP* bgpRouter4 = new BGP(0);
    
    bgpRouter4->AttachNode(node4);
    bgpRouter4->config_file("./bgpd4.conf");
    node3->AddDuplexLink(node4,lk);

    /*Fifth BGP Router*/
  Node *node5 = new Node();
    BGP* bgpRouter5 = new BGP(0);
    
    bgpRouter5->AttachNode(node5);
    bgpRouter5->config_file("./bgpd5.conf");
    node4->AddDuplexLink(node5,lk);
    node4->AddDuplexLink(node2,lk);

    /*Sixth BGP Router*/
  Node *node6 = new Node();
    BGP* bgpRouter6 = new BGP(0);
    
    bgpRouter6->AttachNode(node6);
    bgpRouter6->config_file("./bgpd6.conf");
    node6->AddDuplexLink(node1,lk);

    /*Seventh BGP Router*/

  Node *node7 = new Node();
  BGP* bgpRouter7 = new BGP(0);
  
  bgpRouter7->AttachNode(node7);
  bgpRouter7->config_file("./bgpd7.conf");
  node6->AddDuplexLink(node7,lk);

  char command[] ="show ip bgp";

  BGPEvent *ev1 = new BGPEvent(BGPEvent::COMMAND);
  ev1->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev1->argv,command);

  bgpRouter5->timer->Schedule(50,ev1);

  BGPEvent *ev2 = new BGPEvent(BGPEvent::COMMAND);
  ev2->argv = (char*)malloc( sizeof(char)*(strlen(command)+1));
  strcpy(ev2->argv,command);

    bgpRouter2->timer->Schedule(50,ev2);

    bgpRouter1->StartAt(0.1);
    bgpRouter2->StartAt(0.1);
		bgpRouter3->StartAt(0.1);
    bgpRouter4->StartAt(0.1);
    bgpRouter5->StartAt(0.1);
    bgpRouter6->StartAt(0.1);
    bgpRouter7->StartAt(0.2);
    bgpRouter1->StopAt(StopTime);
    bgpRouter2->StopAt(StopTime);
    bgpRouter3->StopAt(StopTime);
    bgpRouter4->StopAt(StopTime);
    bgpRouter5->StopAt(StopTime);
    bgpRouter6->StopAt(StopTime);
    bgpRouter7->StopAt(StopTime);
  
    Sim.StopAt(StopTime);
    Sim.Run();
}
 
