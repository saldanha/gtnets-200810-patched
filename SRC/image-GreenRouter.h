// Generated automatically by make-gtimage from GreenRouter.png
// DO NOT EDIT

#include "image.h"

class GreenRouterImage : public Image {
public:
  GreenRouterImage(){}
  const char* Data() const { return data;}
  int Size() const { return size;}
  operator const char*() { return data;}
  static const char* data;
  static int size;
};
