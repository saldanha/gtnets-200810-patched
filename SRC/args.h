// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: args.h 460 2006-01-31 20:48:39Z riley $



// Helper classes for command line argument processing
// George F. Riley, Georgia Tech, Winter 2002

#ifndef __args_h__
#define __args_h__

#include <string>
#include <vector>

class Arg;
typedef std::vector<std::string> StringVec_t;
typedef std::vector<Arg>         ArgVec_t;

//Doc:ClassXRef
class Arg {
  //Doc:Class Since it is often the case that a given simulation scenario
  //Doc:Class must be run multiple times with a variety of differnet
  //Doc:Class parameters, \GTNS\ provides a simple interface to handle
  //Doc:Class command line  arguments.  Class {\tt Arg} is used to 
  //Doc:Class specify an allowable set of equivalenced parameters, which
  //Doc:Class provide default values for parameters as well as explicitly
  //Doc:Class specified value.  Further, the {\tt Arg} interface allows
  //Doc:Class for non--equivalenced parameters, which can be queried
  //Doc:Class at runtime for presence or absence.
private:
  typedef enum {
      Int, Long, Double, String, Bool } Arg_t;
public:
  //Doc:Method
  Arg(const std::string& n, long&, long);
    //Doc:Desc Add an argument of type {\tt long}.
    //Doc:Arg1 Argument keyword.
    //Doc:Arg2 Reference to {\tt long} variable to receive equivalenced value.
    //Doc:Arg3 Default value if not specified.

  //Doc:Method
  Arg(const std::string& n, long&);
    //Doc:Desc Add an argument of type {\tt long}.
    //Doc:Arg1 Argument keyword.
    //Doc:Arg2 Reference to {\tt long} variable to receive equivalenced value.

  //Doc:Method
  Arg(const std::string& n, unsigned long&, unsigned long);
    //Doc:Desc Add an argument of type {\tt unsigned long}.
    //Doc:Arg1 Argument keyword.
    //Doc:Arg2 Reference to {\tt unsigned long} variable to
    //Doc:Arg2 receive equivalenced value.
    //Doc:Arg3 Default value if not specified.

  //Doc:Method
  Arg(const std::string& n, unsigned long&);
    //Doc:Desc Add an argument of type {\tt unsigned long}.
    //Doc:Arg1 Argument keyword.
    //Doc:Arg2 Reference to {\tt unsigned long} variable to
    //Doc:Arg2 receive equivalenced value.

  //Doc:Method
  Arg(const std::string& n, double&, double);
    //Doc:Desc Add an argument of type {\tt double}.
    //Doc:Arg1 Argument keyword.
    //Doc:Arg2 Reference to {\tt double} variable to
    //Doc:Arg2 receive equivalenced value.
    //Doc:Arg3 Default value if not specified.

  //Doc:Method
  Arg(const std::string& n, double&);
    //Doc:Desc Add an argument of type {\tt double}.
    //Doc:Arg1 Argument keyword.
    //Doc:Arg2 Reference to {\tt double} variable to
    //Doc:Arg2 receive equivalenced value.

  //Doc:Method
  Arg(const std::string& n, std::string&, const std::string&);
    //Doc:Desc Add an argument of type {\tt string}.
    //Doc:Arg1 Argument keyword.
    //Doc:Arg2 Reference to {\tt string} variable to
    //Doc:Arg2 receive equivalenced value.
    //Doc:Arg3 Default value if not specified.

  //Doc:Method
  Arg(const std::string& n, std::string&);
    //Doc:Desc Add an argument of type {\tt string}.
    //Doc:Arg1 Argument keyword.
    //Doc:Arg2 Reference to {\tt string} variable to
    //Doc:Arg2 receive equivalenced value.

  //Doc:Method
  Arg(const std::string& n, bool&, bool);
    //Doc:Desc Add an argument of type {\tt bool}.
    //Doc:Arg1 Argument keyword.
    //Doc:Arg2 Reference to {\tt bool} variable to
    //Doc:Arg2 receive equivalenced value.
    //Doc:Arg3 Default value if not specified.

  Arg(const std::string& n, bool&);
    //Doc:Desc Add an argument of type {\tt bool}.
    //Doc:Arg1 Argument keyword.
    //Doc:Arg2 Reference to {\tt bool} variable to
    //Doc:Arg2 receive equivalenced value.

public:
  //Doc:Method
  static bool ProcessArgs(int argc, char** argv); // Process the arguments
    //Doc:Desc Process the command line arguments.  Must be called before
    //Doc:Desc checking
    //Doc:Desc any argument values.
    //Doc:Arg1 {\tt argc} argument from {\tt main}.
    //Doc:Arg2 {\tt argv} argument from {\tt main}.
    //Doc:Return True if successfully processed.

  // Return number of non-equivalenced args      
  //Doc:Method
  static StringVec_t::size_type  NumberNonEquiv();
    //Doc:Desc Return the number of non--equivalenced arguments.
    //Doc:Return Count of non--equivalenced argument.

  // Return specified non-equivalended  
  //Doc:Method
  static std::string NonEquiv(StringVec_t::size_type);
    //Doc:Desc Return the specified non--equivalenced argument.
    //Doc:Arg1 Index from the non--equivalenced list to return.
    //Doc:Return The specified non--equivalenced argument.

  // True if specified string v was a non-equivalenced argument
  //Doc:Method
  static bool   Specified(const std::string&);
    //Doc:Desc Determine if a particular non--equivalenced argument was
    //Doc:Desc specified.
    //Doc:Return True if the argument was specified.

private:
  std::string    argName;       // Name of argument
  Arg_t          argType;       // Type of argument
  void*          pArg;          // Address (typeless) of argument
private:
  static ArgVec_t    args;      // The expected arguments list
  static StringVec_t nonEquiv;  // The non-equivalenced argument list
};


#endif
