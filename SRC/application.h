// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application.h 460 2006-01-31 20:48:39Z riley $



// Georgia Tech Network Simulator - Layer 5 Application base class
// George F. Riley.  Georgia Tech, Spring 2002

// Define the layer 5 application interface

#ifndef __application_h__
#define __application_h__

#include "common-defs.h"
#include "handler.h"
#include "l4protocol.h"

// Define the start/stop events
class ApplicationEvent : public Event {
public:
  typedef enum { START, STOP } ApplicationEvent_t;
  ApplicationEvent() { }
  ApplicationEvent(Event_t ev) : Event(ev) { }
};

//Doc:ClassXRef
class Application : public Handler, public Object  {
  //Doc:Class Class {\tt Application} is the base class for all
  //Doc:Class \GTNS\ applications.
  //Doc:Class It defines the interface between the application class and
  //Doc:Class the associated layer 4 protocols.  Applications can have one
  //Doc:Class or more layer 4 protocols assigned, to allow (for example)
  //Doc:Class a web browser model with multiple simulataneous connections.
public:
  //Doc:Method
  Application() : deleteOnComplete(false), copyOnConnect(true) { };
    //Doc:Desc The default constructor for the {\tt Application} class.

  //Doc:Method
  Application(const Application&);
    //Doc:Desc Copy constructor, used bt the {\tt Copy()} method for all
    //Doc:Desc applications.
    //Doc:Arg1 Application object to copy.

  virtual ~Application();
  // Starting, Stoping
  //Doc:Method
  void Start(Time_t);
    //Doc:Desc Schedule a {\tt Start} event for this application at the
    //Doc:Desc specified time.
    //Doc:Arg1 The simulation time to start the application.
    //Doc:Arg1 The {\t StartApp} method of the application will be called
    //Doc:Arg1 at the specified time.

  //Doc:Method
  void Stop(Time_t);
    //Doc:Desc Schedule a {\tt Stop} event for this application at the
    //Doc:Desc specified time.
    //Doc:Arg1 The simulation time to stop the application.
    //Doc:Arg1 The {\t StopApp} method of the application will be called
    //Doc:Arg1 at the specified time.

  // Handler functions
  void Handle(Event*, Time_t);

  // Upcalls from L4 protocol
  //Doc:Method
  virtual void Receive(Packet*,L4Protocol*, Seq_t = 0);   // Data received
    //Doc:Desc Called by an associated layer 4 protocol when data 
    //Doc:Desc is received.
    //Doc:Arg1 The packet received, with the layer 4 PDU removed.
    //Doc:Arg2 A pointer to the layer 4 protocol object that recieved the data.
    //Doc:Arg3 Optional sequence number for this data (TCP protocols only)

  //Doc:Method
  virtual void Sent(Count_t, L4Protocol*);     // Data has been sent
    //Doc:Desc Called by an associated layer 4 protocol when all some part 
    //Doc:Desc of the outstanding data has been sent.  For TCP protocols,
    //Doc:Desc this occurs when the acknowledgement is received from the peer.
    //Doc:Arg1 Count of number of bytes successfully sent.
    //Doc:Arg2 A pointer to the layer 4 protocol object that sent the data.

  //Doc:Method
  virtual void CloseRequest(L4Protocol*);      // Close request from peer
    //Doc:Desc Called by an associated layer 4 protocol when a connection
    //Doc:Desc close request has been receivd from a peer.  Applications should
    //Doc:Desc respond by calling the corresponding layer 4 ::Close() routine.
    //Doc:Arg1 A pointer to the layer 4 protocol object that closed.

  //Doc:Method
  virtual void Closed(L4Protocol*);            // Connection has closed
    //Doc:Desc Called by an associated layer 4 protocol when a connection
    //Doc:Desc has completely closed.
    //Doc:Arg1 A pointer to the layer 4 protocol object that closed.

  //Doc:Method
  virtual void ConnectionComplete(L4Protocol*);// Connection request succeeded
    //Doc:Desc Called by an associated layer 4 protocol when a previous
    //Doc:Desc connection request has successfully completed.
    //Doc:Arg1 A pointer to the layer 4 protocol object that connected.

  //Doc:Method
  virtual void ServConnectionComplete(L4Protocol*);// Connection request succeeded
   //Doc:Desc Called by an associated layer 4 protocol when a connection
   //Doc:Desc that the TCP server agent accepted has successfully completed.
   //Doc:Arg1 A pointer to the layer 4 protocol object that connected.



  //Doc:Method
  virtual void ConnectionFailed(L4Protocol*, bool);// Connection request failed
    //Doc:Desc Called by an associated layer 4 protocol when a previous
    //Doc:Desc connection request has failed.
    //Doc:Arg1 A pointer to the layer 4 protocol object that failed.
    //Doc:Arg2 True if aborted, false if timed out

  // Listener has cn rq from peer 
  //Doc:Method
  virtual bool ConnectionFromPeer(L4Protocol*, IPAddr_t, PortId_t);
    //Doc:Desc Called by an associated layer 4 protocol when a listening
    //Doc:Desc TCP protocol has received a connection request.
    //Doc:Arg1 A pointer to the listening TCP protocol object.
    //Doc:Arg2 \IPA\ of remote peer.
    //Doc:Arg3 Port number of report peer.
    //Doc:Return Return true if able to accept a new connection, false if not.

  // Manage the application
  //Doc:Method
  void DeleteOnComplete(bool doc = true) { deleteOnComplete = doc;}
    //Doc:Desc Specifies that this application object should automatically
    //Doc:Desc be deleted when the application has finished.
    //Doc:Arg1 True if delete on complete desired.

  //Doc:Method
  void CopyOnConnect(bool coc)           { copyOnConnect = coc;}
    //Doc:Desc Specifies that this application object should be copied
    //Doc:Desc when connection requests are processed.
    //Doc:Arg1 True if delete on copy on connect desired.

  //Doc:Method
  bool CopyOnConnect() { return copyOnConnect;}
    //Doc:Desc Return current status of copy on connect flag.
    //Doc:Return True if copy on connect, false if not.

  //Doc:Method
  virtual void StartApp() { };    // Called at time specified by Start
    //Doc:Desc Called at the specified applcation start time.

  //Doc:Method
  virtual void StopApp() { };     // Called at time specified by Stop
    //Doc:Desc Called at the specified applcation stop time.

  //Doc:Method
  virtual void AttachNode(Node*); // Note which node attached to
    //Doc:Desc Specify which node to which this application is attached.
    //Doc:Arg1 Node pointer to attached node.

  //Doc:Method
  virtual Application* Copy() const = 0;// Make a copy of the application
    //Doc:Desc Return a copy of this application. \purev
    //Doc:Return A pointer to a new copy of this application.

  // If the application has a single layer4 protocol, return it
  //Doc:Method
  virtual L4Protocol* GetL4() const { return nil;}
    //Doc:Desc Returns a pointer to the layer 4 protocol object,
    //Doc:Desc if this application has a single associated l4 protocol.
    //Doc:Return Pointer to the single layer 4 object, or {\tt nil} if 
    //Doc:Return none, or {\tt nil} if more than one.

public:
  //Doc:Member
  bool        deleteOnComplete;
    //Doc:Desc True if delete on complete requested.

  //Doc:Member
  bool        copyOnConnect;      // True if application copied
    //Doc:Desc True if copy on connect requested.
};

#endif


