#include <iostream>
#include <sstream>
#include <string>

#include "debug.h"
#include "args.h"
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "node-satellite.h"
#include "node-taclane.h"
#include "star.h"
#include "linkp2p.h"
#include "tcp-tahoe.h"
#include "application-cbr.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "application-tcpreceive.h"
#include "application-udpsink.h"
#include "application-voip.h"
#include "rng.h"
#include "routing.h"
#include "routing-manual.h"
#include "routing-static.h"
#include "diffserv-queue.h"
#include "time-value.h"
#include "time-value-graph.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "udp.h"
#include "validation.h"
#include "qtwindow.h"
#include "average-min-max.h"
// Images
#include "image-RedRouter.h"
#include "image-BlackRouter.h"
#include "image-GreenRouter.h"
#include "image-Host.h"
#include "image-HostSmall.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

using namespace std;

Count_t qlim = 500000;

void SetQOS(Node* s, Node* d, Rate_t bw, Count_t nClass)
{ // Set a diffserv queue on specified link, equal rate allocations
  s->GetIfByNode(d)->SetQueue(DiffServQueue(nClass, bw));
  DiffServQueue* q = (DiffServQueue*)s->GetQueue(d);
  for (Count_t i = 0; i < nClass; ++i)
    {
      q->RateAllocation(i, bw/nClass);
    }
}

void MakeSatLink(Node* s, Node* d, Linkp2p& lk, bool QOS)
{
  s->AddDuplexLink(d, lk);
  Queue* q = s->GetQueue(d);
  q->SetLimit(qlim);
  q->Animate(true);
  if (QOS)
    {
      SetQOS(s, d, lk.Bandwidth(), 2);
    }
}


Uniform startRNGVoip(0,0.1);
PortId_t nextPort = 50;

void AppApplications(Node* n, Node* d, Statistics* s, Random& startRNG, Count_t voipCount)
{
  TCPReceive* r = (TCPReceive*)n->AddApplication(
      TCPReceive(d->GetIPAddr(), 40,          // Dest ip and port
                 Constant(100),               // Request size
                 Constant(50000),             // Reply size
                 TCP::Default(),              // TCP Variant
                 Uniform(10, 20),             // Sleep time
                 MAX_COUNT));                 // Infinite loop count
  r->GetL4()->SetTrace(Trace::ENABLED);
  r->Start(startRNG.Value());
  r->SetStatistics(s);
  // Also add VOIP calls
  for (Count_t i = 0; i < voipCount; ++i)
    {
      VOIPApplication* v0 = (VOIPApplication*)n->AddApplication(
        //VOIPApplication(d->GetIPAddr(), 50, Constant(1000), Constant(0)));
          VOIPApplication(d->GetIPAddr(), nextPort));
      v0->Start(startRNGVoip.Value());
      v0->GetL4()->TOS(1);
      // Add the reverse connection
      VOIPApplication* v1 = (VOIPApplication*)d->AddApplication(
          //VOIPApplication(n->GetIPAddr(), 50, Constant(1000), Constant(0)));
          VOIPApplication(n->GetIPAddr(), nextPort));
      v1->Start(startRNGVoip.Value());
      v1->GetL4()->TOS(1);
      v1->GetL4()->SetTrace(Trace::ENABLED);
      nextPort++;
    }
}
int main(int argc, char** argv)
{
  bool Anim = false;
  bool useQOS = false;
  
  Count_t voipCount = 20;
  Arg("voipCount", voipCount, voipCount);
  Arg("qlim", qlim, qlim);
  
  Arg::ProcessArgs(argc, argv);
  if (Arg::Specified("useAnim")) Anim = true;
  if (Arg::Specified("useQOS"))  useQOS = true;
  
  if (useQOS) cout << "Using qos on satlink" << endl;
  cout << "Voipcount " << voipCount << endl;
  Validation::Init(argc, argv);
  Simulator s;
  Routing::SetRouting(new RoutingStatic());
      
  if (Arg::Specified("trace"))
    {
      Trace* tr = Trace::Instance(); // Get a pointer to global trace object
      tr->Open("jrae05.txt");
    }
  
  IPV4::Instance()->SetTrace(Trace::ENABLED);

  if (Anim)
    { // Specify animation
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("2ms")); // 2ms initial update rate
    }
        
  /******************************JRAE 05 Address Initialization******************************/
	
  //LHD
  IPAddr_t alhd_SWlhd = IPAddr("192.168.210.4");
  IPAddr_t aSWlhd_lhd = IPAddr("192.168.210.3");
  IPAddr_t aSWlhd_R17 = IPAddr("192.168.210.2");
  IPAddr_t aR17_SWlhd = IPAddr("192.168.210.1");
  IPAddr_t aR17_Hlhd = IPAddr("192.168.209.2");
  IPAddr_t aHlhd_R17 = IPAddr("192.168.209.1");
  IPAddr_t aHlhd_R7 = IPAddr("10.8.8.2");
  IPAddr_t aR7_Hlhd = IPAddr("10.8.8.1");
  IPAddr_t aR7_R27 = IPAddr("134.207.140.1");
  IPAddr_t aR27_R7 = IPAddr("134.207.140.2");
  IPAddr_t aR27_SATlhd = IPAddr("10.8.85.1");
  IPAddr_t aSATlhd_R27 = IPAddr("10.8.85.2");
	
  //CVN
  IPAddr_t acvn1_SWcvn = IPAddr("192.168.211.4");
  IPAddr_t acvn2_SWcvn = IPAddr("192.168.211.5");
  IPAddr_t acvn3_SWcvn = IPAddr("192.168.211.6");
  IPAddr_t aSWcvn_cvn1 = IPAddr("192.168.211.7");
  IPAddr_t aSWcvn_cvn2 = IPAddr("192.168.211.8");
  IPAddr_t aSWcvn_cvn3 = IPAddr("192.168.211.9");
  IPAddr_t aSWcvn_R14 = IPAddr("192.168.211.2");
  IPAddr_t aR14_SWcvn = IPAddr("192.168.211.1");
  IPAddr_t aR14_Hcvn = IPAddr("192.168.9.2");
  IPAddr_t aHcvn_R14 = IPAddr("192.168.9.1");
  IPAddr_t aHcvn_R4 = IPAddr("10.4.4.2");
  IPAddr_t aR4_Hcvn = IPAddr("10.4.4.1");
  IPAddr_t aR4_SATcvn = IPAddr("10.4.45.1");
  IPAddr_t aSATcvn_R4 = IPAddr("10.4.45.2");
	
  //DDG
  IPAddr_t addg_SWddg = IPAddr("192.168.110.4");
  IPAddr_t aSWddg_ddg = IPAddr("192.168.110.3");
  IPAddr_t aSWddg_R16 = IPAddr("192.168.110.2");
  IPAddr_t aR16_SWddg = IPAddr("192.168.110.1");
  IPAddr_t aR16_Hddg = IPAddr("192.168.109.2");
  IPAddr_t aHddg_R16 = IPAddr("192.168.109.1");
  IPAddr_t aHddg_R6 = IPAddr("150.125.110.34");
  IPAddr_t aR6_Hddg = IPAddr("150.125.110.33");	
  IPAddr_t aR6_SATddg = IPAddr("192.168.110.18");
  IPAddr_t aSATddg_R6 = IPAddr("192.168.110.20");
  IPAddr_t aSATddg_R26 = IPAddr("192.168.110.19");
  IPAddr_t aR26_SATddg = IPAddr("192.168.110.17");

  //USMC
  IPAddr_t ausmc_SWusmc = IPAddr("192.168.105.5");
  IPAddr_t aSWusmc_usmc = IPAddr("192.168.105.3");
  IPAddr_t aSWusmc_R12 = IPAddr("192.168.110.2");
  IPAddr_t aR12_SWusmc = IPAddr("192.168.110.1");
  IPAddr_t aR12_Husmc = IPAddr("192.168.104.2");
  IPAddr_t aHusmc_R12 = IPAddr("192.168.104.1");
  IPAddr_t aHusmc_R2 = IPAddr("10.6.6.2");
  IPAddr_t aR2_Husmc = IPAddr("10.6.6.1");	
  IPAddr_t aR2_SATusmc = IPAddr("10.6.65.1");
  IPAddr_t aSATusmc_R2= IPAddr("10.6.65.3");
  IPAddr_t aSATusmc_R22 = IPAddr("10.6.65.4");
  IPAddr_t aR22_SATusmc = IPAddr("10.6.65.2");
	
  //USA
  IPAddr_t ausa_SWusa = IPAddr("192.168.120.4");
  IPAddr_t aSWusa_usa = IPAddr("192.168.120.3");
  IPAddr_t aSWusa_R13 = IPAddr("192.168.120.2");
  IPAddr_t aR13_SWusa = IPAddr("192.168.120.1");
  IPAddr_t aR13_Husa = IPAddr("192.168.119.2");
  IPAddr_t aHusa_R13 = IPAddr("192.168.119.1");
  IPAddr_t aHusa_R3 = IPAddr("10.5.5.2");
  IPAddr_t aR3_Husa = IPAddr("10.5.5.1");
  IPAddr_t aR3_SATusa = IPAddr("10.5.55.1");
  IPAddr_t aSATusa_R3= IPAddr("10.5.55.3");
  IPAddr_t aSATusa_R23 = IPAddr("10.5.55.4");
  IPAddr_t aR23_SATusa = IPAddr("10.5.55.2");
	
  //USAF
  IPAddr_t ausaf_SWusaf = IPAddr("172.28.10.4");
  IPAddr_t aSWusaf_usaf = IPAddr("172.28.10.3");
  IPAddr_t aSWusaf_R18 = IPAddr("172.28.10.2");
  IPAddr_t aR18_SWusaf = IPAddr("172.28.10.1");
  IPAddr_t aR18_Husaf = IPAddr("198.201.140.5");
  IPAddr_t aHusaf_R18 = IPAddr("198.201.140.6");
  IPAddr_t aHusaf_R8 = IPAddr("146.153.141.10");
  IPAddr_t aR8_Husaf = IPAddr("146.153.141.19");
  IPAddr_t aR8_SATusaf = IPAddr("192.168.141.1");
  IPAddr_t aSATusaf_R8= IPAddr("192.168.141.3");
  IPAddr_t aSATusaf_R28 = IPAddr("192.168.141.4");
  IPAddr_t aR28_SATusaf = IPAddr("192.168.141.2");
	
  //JOINT
  IPAddr_t ajoint_SWjoint = IPAddr("192.168.220.4");
  IPAddr_t aSWjoint_joint = IPAddr("192.168.220.3");
  IPAddr_t aSWjoint_R20 = IPAddr("192.168.220.2");
  IPAddr_t aR20_SWjoint = IPAddr("192.168.220.1");
  IPAddr_t aR20_Hjoint = IPAddr("192.168.219.2");
  IPAddr_t aHjoint_R20 = IPAddr("192.168.219.1");
  IPAddr_t aHjoint_R10 = IPAddr("10.7.7.2");
  IPAddr_t aR10_Hjoint = IPAddr("10.7.7.1");
  IPAddr_t aR10_SATjoint = IPAddr("10.7.75.1");
  IPAddr_t aSATjoint_R10 = IPAddr("10.7.75.3");
  IPAddr_t aSATjoint_R30 = IPAddr("10.7.75.4");
  IPAddr_t aR30_SATjoint = IPAddr("10.7.75.2");
  IPAddr_t aR30_R40 = IPAddr("10.7.75.9");
  IPAddr_t aR40_R30 = IPAddr("10.7.75.10");
	
  //NCTAMS
  IPAddr_t anctams_SWnctams = IPAddr("172.16.10.4");
  IPAddr_t aSWnctams_nctams = IPAddr("172.16.10.3");
  IPAddr_t aSWnctams_R11 = IPAddr("172.16.10.2");
  IPAddr_t aR11_SWnctams = IPAddr("172.16.10.1");
  IPAddr_t aR11_Hnctams = IPAddr("172.16.9.2");
  IPAddr_t aHnctams_R11 = IPAddr("172.16.9.1");
  IPAddr_t aHnctams_R1 = IPAddr("10.3.3.2");
  IPAddr_t aR1_Hnctams = IPAddr("10.3.3.1");
	
  //COCOM
  IPAddr_t acocom_SWcocom = IPAddr("172.16.20.4");
  IPAddr_t aSWcocom_cocom = IPAddr("172.16.20.3");
  IPAddr_t aSWcocom_R15 = IPAddr("172.16.20.2");
  IPAddr_t aR15_SWcocom = IPAddr("172.16.20.1");
  IPAddr_t aR15_Hcocom = IPAddr("172.16.19.2");
  IPAddr_t aHcocom_R15 = IPAddr("172.16.19.1");
  IPAddr_t aHcocom_R5 = IPAddr("10.1.1.2");
  IPAddr_t aR5_Hcocom = IPAddr("10.1.1.1");
	
  //TELEPORT
  IPAddr_t ateleport_SWteleport = IPAddr("172.16.30.4");
  IPAddr_t aSWteleport_teleport = IPAddr("172.16.30.3");
  IPAddr_t aSWteleport_R19 = IPAddr("172.16.30.2");
  IPAddr_t aR19_SWteleport = IPAddr("172.16.30.1");
  IPAddr_t aR19_Hteleport = IPAddr("172.16.29.2");
  IPAddr_t aHteleport_R19 = IPAddr("172.16.29.1");
  IPAddr_t aHteleport_R9 = IPAddr("10.10.10.2");
  IPAddr_t aR9_Hteleport = IPAddr("10.10.10.1");
	
  //INTERCONNECTS
  IPAddr_t aSATlhd_R1 = IPAddr("1.1.1.1");
  IPAddr_t aR1_SATlhd = IPAddr("1.1.1.2");

  IPAddr_t aSATcvn_R1 = IPAddr("1.1.2.1");
  IPAddr_t aR1_SATcvn = IPAddr("1.1.2.2");

  IPAddr_t aR26_R1 = IPAddr("1.1.3.26");
  IPAddr_t aR1_R26 = IPAddr("1.1.3.25");

  IPAddr_t aR22_R9 = IPAddr("1.1.4.9");
  IPAddr_t aR9_R22 = IPAddr("1.1.4.10");
	
  IPAddr_t aR23_R9 = IPAddr("1.1.5.9");
  IPAddr_t aR9_R23 = IPAddr("1.1.5.10");
	
  IPAddr_t aR28_R9 = IPAddr("1.1.6.18");
  IPAddr_t aR9_R28 = IPAddr("1.1.6.17");
	
  IPAddr_t aR40_R9 = IPAddr("1.1.7.17");
  IPAddr_t aR9_R40 = IPAddr("1.1.7.18");
	
  IPAddr_t aR1_G1 = IPAddr("1.1.8.1");
  IPAddr_t aG1_R1 = IPAddr("1.1.8.2");
	
  IPAddr_t aG1_G2 = IPAddr("1.1.9.1");
  IPAddr_t aG2_G1 = IPAddr("1.1.9.2");
	
  IPAddr_t aR5_G2 = IPAddr("1.1.10.1");
  IPAddr_t aG2_R5 = IPAddr("1.1.10.2");
	
  IPAddr_t aG3_G2 = IPAddr("1.1.11.1");
  IPAddr_t aG2_G3 = IPAddr("1.1.11.2");
	
  IPAddr_t aR9_G3 = IPAddr("1.1.12.1");
  IPAddr_t aG3_R9 = IPAddr("1.1.12.2");
	
  /******************************JRAE 05 Address Initialization******************************/
		
	
  /******************************JRAE 05 Network Node Declarations******************************/
	
  //LHD
  Node* lhd;
  Node* SWlhd;
  Node* R17;
  TACLANENode* Hlhd;

  Node* R7;
  Node* R27;
  SatelliteNode* SATlhd;
	
  //CVN
  Node* cvn1;
  Node* cvn2;
  Node* cvn3;
  Node* SWcvn;
  Node* R14;
  TACLANENode* Hcvn;
  Node* R4;
  SatelliteNode* SATcvn;
	
  //DDG
  Node* ddg;
  Node* SWddg;
  Node* R16;
  TACLANENode* Hddg;
  Node* R6;
  SatelliteNode* SATddg;
  Node* R26;
	
  //USMC
  Node* usmc;
  Node* SWusmc;
  Node* R12;
  TACLANENode* Husmc;
  Node* R2;	
  SatelliteNode* SATusmc;
  Node* R22;
	
  //USA
  Node* usa;
  Node* SWusa;
  Node* R13;
  TACLANENode* Husa;
  Node* R3;
  SatelliteNode* SATusa;
  Node* R23;
	
  //USAF
  Node* usaf;
  Node* SWusaf;
  Node* R18;
  TACLANENode* Husaf;
  Node* R8;
  SatelliteNode* SATusaf;
  Node* R28;
	
  //JOINT
  Node* joint;
  Node* SWjoint;
  Node* R20;
  TACLANENode* Hjoint;
  Node* R10;
  SatelliteNode* SATjoint;
  Node* R30;
  Node* R40;
	
  //NCTAMS
  Node* nctams;
  Node* SWnctams;
  Node* R11;
  TACLANENode* Hnctams;
  Node* R1;
	
  //COCOM
  Node* cocom;
  Node* SWcocom;
  Node* R15;
  TACLANENode* Hcocom;
  Node* R5;
	
  //TELEPORT
  Node* teleport;
  Node* SWteleport;
  Node* R19;
  TACLANENode* Hteleport;
  Node* R9;
	
  //GIGBE
  Node* G1;
  Node* G2;
  Node* G3;
	
  /******************************JRAE 05 Network Node Declarations******************************/

	
  /*****************************JRAE 05 Network Node Initialization*****************************/

  //LHD
  lhd = new Node();
  SWlhd = new Node();
  R17 = new Node();
  Hlhd = new TACLANENode(aHlhd_R17, aHlhd_R7, RB);
  R7 = new Node();
  R27 = new Node();
  SATlhd = new SatelliteNode();
	
  //CVN
  cvn1 = new Node();
  cvn2 = new Node();
  cvn3 = new Node();
  SWcvn = new Node();
  R14 = new Node();
  Hcvn = new TACLANENode(aHcvn_R14, aHcvn_R4, RB);
  R4 = new Node();
  SATcvn = new SatelliteNode();
	
  //DDG
  ddg = new Node();
  SWddg = new Node();
  R16 = new Node();
  Hddg = new TACLANENode(aHddg_R16, aHddg_R6, RB);
  R6 = new Node();
  SATddg = new SatelliteNode();
  R26 = new Node();
	
  //USMC
  usmc = new Node();
  SWusmc = new Node();
  R12 = new Node();
  Husmc = new TACLANENode(aHusmc_R12, aHusmc_R2, RB);
  R2 = new Node();	
  SATusmc = new SatelliteNode();
  R22 = new Node();
	
  //USA
  usa = new Node();
  SWusa = new Node();
  R13 = new Node();
  Husa = new TACLANENode(aHusa_R13, aHusa_R3, RB);
  R3 = new Node();
  SATusa = new SatelliteNode();
  R23 = new Node();
	
  //USAF
  usaf = new Node();
  SWusaf = new Node();
  R18 = new Node();
  Husaf = new TACLANENode(aHusaf_R18, aHusaf_R8, RB);
  R8 = new Node();
  SATusaf = new SatelliteNode();
  R28 = new Node();
	
  //JOINT
  joint = new Node();
  SWjoint = new Node();
  R20 = new Node();
  Hjoint = new TACLANENode(aHjoint_R20, aHjoint_R10, RB);
  R10 = new Node();
  SATjoint = new SatelliteNode();
  R30 = new Node();
  R40 = new Node();
	
  //NCTAMS
  nctams = new Node();
  SWnctams = new Node();
  R11 = new Node();
  Hnctams = new TACLANENode(aHnctams_R11, aHnctams_R1, RB);
  R1 = new Node();
	
  //COCOM
  cocom = new Node();
  SWcocom = new Node();
  R15 = new Node();
  Hcocom = new TACLANENode(aHcocom_R15, aHcocom_R5, RB);
  R5 = new Node();
	
  //TELEPORT
  teleport = new Node();
  SWteleport = new Node();
  R19 = new Node();
  Hteleport = new TACLANENode(aHteleport_R19, aHteleport_R9, RB);
  R9 = new Node();
	
  //GIGBE
  G1 = new Node();
  G2 = new Node();
  G3 = new Node();
	
  /*****************************JRAE 05 Network Node Initialization*****************************/

	
  /*********************************JRAE 05 Link Initialization*********************************/
	
  Linkp2p tl_link(Rate("10Mb"), Time("1ms"));
  Linkp2p lan_link(Rate("100Mb"), Time("1ms"));
  Linkp2p dren_link(Rate("100Mb"), Time("5ms"));
  Linkp2p sat_link(Rate("1.5Mb"), Time("120ms"));
  sat_link.BitErrorRate(1e-7); // Satellite bit error rate

  //LHD
  lhd->AddDuplexLink(SWlhd, lan_link, alhd_SWlhd, Mask(32), aSWlhd_lhd, Mask(32));
  SWlhd->AddDuplexLink(R17, lan_link, aSWlhd_R17, Mask(32), aR17_SWlhd, Mask(32));
  R17->AddDuplexLink(Hlhd, tl_link, aR17_Hlhd, Mask(32), aHlhd_R17, Mask(32));
  Hlhd->AddDuplexLink(R7, tl_link, aHlhd_R7, Mask(32), aR7_Hlhd, Mask(32));
  R7->AddDuplexLink(R27, lan_link, aR7_R27, Mask(32), aR27_R7, Mask(32));
  MakeSatLink(R27, SATlhd, sat_link, useQOS);
  MakeSatLink(R1,  SATlhd, sat_link, useQOS);
  
  //R27->AddDuplexLink(SATlhd, sat_link, aR27_SATlhd, Mask(32), aSATlhd_R27, Mask(32));
  //SATlhd->AddDuplexLink(R1, sat_link, aSATlhd_R1, Mask(32), aR1_SATlhd, Mask(32));
  //if (useQOS)  Moved to MakeSatLink
  //  {
  //    SetQOS(R27, SATlhd, sat_link.Bandwidth(), 2);
  //    SetQOS(R1,  SATlhd, sat_link.Bandwidth(), 2);
  //  }
        
	
  //CVN
  cvn1->AddDuplexLink(SWcvn, lan_link, acvn1_SWcvn, Mask(32), aSWcvn_cvn1, Mask(32));
  cvn2->AddDuplexLink(SWcvn, lan_link, acvn2_SWcvn, Mask(32), aSWcvn_cvn2, Mask(32));
  cvn3->AddDuplexLink(SWcvn, lan_link, acvn3_SWcvn, Mask(32), aSWcvn_cvn3, Mask(32));
  SWcvn->AddDuplexLink(R14, lan_link, aSWcvn_R14, Mask(32), aR14_SWcvn, Mask(32));
  R14->AddDuplexLink(Hcvn, tl_link, aR14_Hcvn, Mask(32), aHcvn_R14, Mask(32));
  Hcvn->AddDuplexLink(R4, tl_link, aHcvn_R4, Mask(32), aR4_Hcvn, Mask(32));
  MakeSatLink(R4, SATcvn, sat_link, useQOS);
  MakeSatLink(R1,  SATcvn, sat_link, useQOS);
  //R4->AddDuplexLink(SATcvn, sat_link, aR4_SATcvn, Mask(32), aSATcvn_R4, Mask(32));
  //SATcvn->AddDuplexLink(R1, sat_link, aSATcvn_R1, Mask(32), aR1_SATcvn, Mask(32));
	
  //DDG
  ddg->AddDuplexLink(SWddg, lan_link, addg_SWddg, Mask(32), aSWddg_ddg, Mask(32));
  SWddg->AddDuplexLink(R16, lan_link, aSWddg_R16, Mask(32), aR16_SWddg, Mask(32));
  R16->AddDuplexLink(Hddg, tl_link, aR16_Hddg, Mask(32), aHddg_R16, Mask(32));
  Hddg->AddDuplexLink(R6, tl_link, aHddg_R6, Mask(32), aR6_Hddg, Mask(32));
  MakeSatLink(R6, SATddg, sat_link, useQOS);
  MakeSatLink(R26,  SATddg, sat_link, useQOS);
  //R6->AddDuplexLink(SATddg, sat_link, aR6_SATddg, Mask(32), aSATddg_R6, Mask(32));
  //SATddg->AddDuplexLink(R26, sat_link, aSATddg_R26, Mask(32), aR26_SATddg, Mask(32));
  R26->AddDuplexLink(R1, dren_link, aR26_R1, Mask(32), aR1_R26, Mask(32));
	
  //USMC
  usmc->AddDuplexLink(SWusmc, lan_link, ausmc_SWusmc, Mask(32), aSWusmc_usmc, Mask(32));
  SWusmc->AddDuplexLink(R12, lan_link, aSWusmc_R12, Mask(32), aR12_SWusmc, Mask(32));
  R12->AddDuplexLink(Husmc, tl_link, aR12_Husmc, Mask(32), aHusmc_R12, Mask(32));
  Husmc->AddDuplexLink(R2, tl_link, aHusmc_R2, Mask(32), aR2_Husmc, Mask(32));
  MakeSatLink(R2, SATusmc, sat_link, useQOS);
  MakeSatLink(R22,SATusmc, sat_link, useQOS);
  //  R2->AddDuplexLink(SATusmc, sat_link, aR2_SATusmc, Mask(32), aSATusmc_R2, Mask(32));	
  //SATusmc->AddDuplexLink(R22, sat_link, aSATusmc_R22, Mask(32), aR22_SATusmc, Mask(32));
  R22->AddDuplexLink(R9, dren_link, aR22_R9, Mask(32), aR9_R22, Mask(32));
	
  //USA
  usa->AddDuplexLink(SWusa, lan_link, ausa_SWusa, Mask(32), aSWusa_usa, Mask(32));
  SWusa->AddDuplexLink(R13, lan_link, aSWusa_R13, Mask(32), aR13_SWusa, Mask(32));
  R13->AddDuplexLink(Husa, tl_link, aR13_Husa, Mask(32), aHusa_R13, Mask(32));
  Husa->AddDuplexLink(R3, tl_link, aHusa_R3, Mask(32), aR3_Husa, Mask(32));
  MakeSatLink(R3, SATusa, sat_link, useQOS);
  MakeSatLink(R23,SATusa, sat_link, useQOS);
  //R3->AddDuplexLink(SATusa, sat_link, aR3_SATusa, Mask(32), aSATusa_R3, Mask(32));
  //SATusa->AddDuplexLink(R23, sat_link, aSATusa_R23, Mask(32), aR23_SATusa, Mask(32));
  R23->AddDuplexLink(R9, dren_link, aR23_R9, Mask(32), aR9_R23, Mask(32));
	
  //USAF
  usaf->AddDuplexLink(SWusaf, lan_link, ausaf_SWusaf, Mask(32), aSWusaf_usaf, Mask(32));
  SWusaf->AddDuplexLink(R18, lan_link, aSWusaf_R18, Mask(32), aR18_SWusaf, Mask(32));
  R18->AddDuplexLink(Husaf, tl_link, aR18_Husaf, Mask(32), aHusaf_R18, Mask(32));
  Husaf->AddDuplexLink(R8, tl_link, aHusaf_R8, Mask(32), aR8_Husaf, Mask(32));
  MakeSatLink(R8, SATusaf, sat_link, useQOS);
  MakeSatLink(R28,SATusaf, sat_link, useQOS);
  //R8->AddDuplexLink(SATusaf, sat_link, aR8_SATusaf, Mask(32), aSATusaf_R8, Mask(32));
  //SATusaf->AddDuplexLink(R28, sat_link, aSATusaf_R28, Mask(32), aR28_SATusaf, Mask(32));
  R28->AddDuplexLink(R9, dren_link, aR28_R9, Mask(32), aR9_R28, Mask(32));
	
  //JOINT
  joint->AddDuplexLink(SWjoint, lan_link, ajoint_SWjoint, Mask(32), aSWjoint_joint, Mask(32));
  SWjoint->AddDuplexLink(R20, lan_link, aSWjoint_R20, Mask(32), aR20_SWjoint, Mask(32));
  R20->AddDuplexLink(Hjoint, tl_link, aR20_Hjoint, Mask(32), aHjoint_R20, Mask(32));
  Hjoint->AddDuplexLink(R10, tl_link, aHjoint_R10, Mask(32), aR10_Hjoint, Mask(32));
  MakeSatLink(R10,SATjoint, sat_link, useQOS);
  MakeSatLink(R30,SATjoint, sat_link, useQOS);
  //R10->AddDuplexLink(SATjoint, sat_link, aR10_SATjoint, Mask(32), aSATjoint_R10, Mask(32));
  //SATjoint->AddDuplexLink(R30, sat_link, aSATjoint_R30, Mask(32), aR30_SATjoint, Mask(32));
  R30->AddDuplexLink(R40, lan_link, aR30_R40, Mask(32), aR40_R30, Mask(32));
  R40->AddDuplexLink(R9, dren_link, aR40_R9, Mask(32), aR9_R40, Mask(32));
	
  //NCTAMS
  nctams->AddDuplexLink(SWnctams, lan_link, anctams_SWnctams, Mask(32), aSWnctams_nctams, Mask(32));
  SWnctams->AddDuplexLink(R11, lan_link, aSWnctams_R11, Mask(32), aR11_SWnctams, Mask(32));
  R11->AddDuplexLink(Hnctams, tl_link, aR11_Hnctams, Mask(32), aHnctams_R11, Mask(32));
  Hnctams->AddDuplexLink(R1, tl_link, aHnctams_R1, Mask(32), aR1_Hnctams, Mask(32));
  R1->AddDuplexLink(G1, lan_link, aR1_G1, Mask(32), aG1_R1, Mask(32));
	
  //COCOM
  cocom->AddDuplexLink(SWcocom, lan_link, acocom_SWcocom, Mask(32), aSWcocom_cocom, Mask(32));
  SWcocom->AddDuplexLink(R15, lan_link, aSWcocom_R15, Mask(32), aR15_SWcocom, Mask(32));
  R15->AddDuplexLink(Hcocom, tl_link, aR15_Hcocom, Mask(32), aHcocom_R15, Mask(32));
  Hcocom->AddDuplexLink(R5, tl_link, aHcocom_R5, Mask(32), aR5_Hcocom, Mask(32));
  R5->AddDuplexLink(G2, lan_link, aR5_G2, Mask(32), aG2_R5, Mask(32));
	
  //TELEPORT
  teleport->AddDuplexLink(SWteleport, lan_link, ateleport_SWteleport, Mask(32), aSWteleport_teleport, Mask(32));
  SWteleport->AddDuplexLink(R19, lan_link, aSWteleport_R19, Mask(32), aR19_SWteleport, Mask(32));
  R19->AddDuplexLink(Hteleport, tl_link, aR19_Hteleport, Mask(32), aHteleport_R19, Mask(32));
  Hteleport->AddDuplexLink(R9, tl_link, aHteleport_R9, Mask(32), aR9_Hteleport, Mask(32));
  R9->AddDuplexLink(G3, dren_link, aR9_G3, Mask(32), aG3_R9, Mask(32));
	
  //GIGBE
  G1->AddDuplexLink(G2, lan_link, aG1_G2, Mask(32), aG2_G1, Mask(32));
  G3->AddDuplexLink(G2, lan_link, aG3_G2, Mask(32), aG2_G3, Mask(32));

  /*********************************JRAE 05 Link Initialization*********************************/	
	
	
  /*********************************JRAE 05 Network Node Shapes*********************************/
	
  //LHD
  lhd->CustomShapeImage(HostSmallImage());
  SWlhd->CustomShapeImage(RedRouterImage());
  R17->CustomShapeImage(RedRouterImage());
  R7->CustomShapeImage(BlackRouterImage());
  R27->CustomShapeImage(BlackRouterImage());
	
  //CVN
  cvn1->CustomShapeImage(HostSmallImage());
  cvn2->CustomShapeImage(HostSmallImage());
  cvn3->CustomShapeImage(HostSmallImage());
  SWcvn->CustomShapeImage(RedRouterImage());
  R14->CustomShapeImage(RedRouterImage());
  R4->CustomShapeImage(BlackRouterImage());
	
  //DDG
  ddg->CustomShapeImage(HostSmallImage());
  SWddg->CustomShapeImage(RedRouterImage());
  R16->CustomShapeImage(RedRouterImage());
  R6->CustomShapeImage(BlackRouterImage());
  R26->CustomShapeImage(BlackRouterImage());
	
  //USMC
  usmc->CustomShapeImage(HostSmallImage());
  SWusmc->CustomShapeImage(RedRouterImage());
  R12->CustomShapeImage(RedRouterImage());
  R2->CustomShapeImage(BlackRouterImage());	
  R22->CustomShapeImage(BlackRouterImage());
	
  //USA
  usa->CustomShapeImage(HostSmallImage());
  SWusa->CustomShapeImage(RedRouterImage());
  R13->CustomShapeImage(RedRouterImage());
  R3->CustomShapeImage(BlackRouterImage());
  R23->CustomShapeImage(BlackRouterImage());
	
  //USAF
  usaf->CustomShapeImage(HostSmallImage());
  SWusaf->CustomShapeImage(RedRouterImage());
  R18->CustomShapeImage(RedRouterImage());
  R8->CustomShapeImage(BlackRouterImage());
  R28->CustomShapeImage(BlackRouterImage());
	
  //JOINT
  joint->CustomShapeImage(HostSmallImage());
  SWjoint->CustomShapeImage(RedRouterImage());
  R20->CustomShapeImage(RedRouterImage());
  R10->CustomShapeImage(BlackRouterImage());
  R30->CustomShapeImage(BlackRouterImage());
  R40->CustomShapeImage(BlackRouterImage());
	
  //NCTAMS
  nctams->CustomShapeImage(HostSmallImage());
  SWnctams->CustomShapeImage(RedRouterImage());
  R11->CustomShapeImage(RedRouterImage());
  R1->CustomShapeImage(BlackRouterImage());
	
  //COCOM
  cocom->CustomShapeImage(HostSmallImage());
  SWcocom->CustomShapeImage(RedRouterImage());
  R15->CustomShapeImage(RedRouterImage());
  R5->CustomShapeImage(BlackRouterImage());
	
  //TELEPORT
  teleport->CustomShapeImage(HostSmallImage());
  SWteleport->CustomShapeImage(RedRouterImage());
  R19->CustomShapeImage(RedRouterImage());
  R9->CustomShapeImage(BlackRouterImage());
	
  //GIGBE
  G1->CustomShapeImage(BlackRouterImage());
  G2->CustomShapeImage(BlackRouterImage());
  G3->CustomShapeImage(BlackRouterImage());
	
  /*********************************JRAE 05 Network Node Shapes*********************************/
	
	
  /********************************JRAE 05 Network Node Locations*******************************/
	
  //LHD
  lhd->SetLocation(0.5, 0);
  SWlhd->SetLocation(0.5, 1.5);
  R17->SetLocation(0.5, 3);
  Hlhd->SetLocation(0.5, 4.5);
  R7->SetLocation(0.5, 6);
  R27->SetLocation(0.5, 7.5);
  SATlhd->SetLocation(0.5, 9);
	
  //CVN
  cvn1->SetLocation(2.75, 1.5);
  cvn2->SetLocation(3.5, 1.5);
  cvn3->SetLocation(4.25, 1.5);
  SWcvn->SetLocation(3.5, 3);
  R14->SetLocation(3.5, 4.5);
  Hcvn->SetLocation(3.5, 6);
  R4->SetLocation(3.5, 7.5);
  SATcvn->SetLocation(3.5, 9);
	
  //DDG
  ddg->SetLocation(6.5, 1.5);
  SWddg->SetLocation(6.5, 3);
  R16->SetLocation(6.5, 4.5);
  Hddg->SetLocation(6.5, 6);
  R6->SetLocation(6.5, 7.5);
  SATddg->SetLocation(6.5, 9);
  R26->SetLocation(6.5, 10.5);
	
  //USMC
  usmc->SetLocation(10.5, 1.5);
  SWusmc->SetLocation(10.5, 3);
  R12->SetLocation(10.5, 4.5);
  Husmc->SetLocation(10.5, 6);
  R2->SetLocation(10.5, 7.5);	
  SATusmc->SetLocation(10.5, 9);
  R22->SetLocation(10.5, 10.5);
	
  //USA
  usa->SetLocation(13.5, 1.5);
  SWusa->SetLocation(13.5, 3);
  R13->SetLocation(13.5, 4.5);
  Husa->SetLocation(13.5, 6);
  R3->SetLocation(13.5, 7.5);
  SATusa->SetLocation(13.5, 9);
  R23->SetLocation(13.5, 10.5);
	
  //USAF
  usaf->SetLocation(16.5, 1.5);
  SWusaf->SetLocation(16.5, 3);
  R18->SetLocation(16.5, 4.5);
  Husaf->SetLocation(16.5, 6);
  R8->SetLocation(16.5, 7.5);
  SATusaf->SetLocation(16.5, 9);
  R28->SetLocation(16.5, 10.5);
	
  //JOINT
  joint->SetLocation(19.5, 1.5);
  SWjoint->SetLocation(19.5, 3);
  R20->SetLocation(19.5, 4.5);
  Hjoint->SetLocation(19.5, 6);
  R10->SetLocation(19.5, 7.5);
  SATjoint->SetLocation(19.5, 9);
  R30->SetLocation(19.5, 10.5);
  R40->SetLocation(19.5, 12);
	
  //NCTAMS
  nctams->SetLocation(3.5, 19.5);
  SWnctams->SetLocation(3.5, 18);
  R11->SetLocation(3.5, 16.5);
  Hnctams->SetLocation(3.5, 15);
  R1->SetLocation(3.5, 13.5);
	
  //COCOM
  cocom->SetLocation(10, 19.5);
  SWcocom->SetLocation(10, 18);
  R15->SetLocation(10, 16.5);
  Hcocom->SetLocation(10, 15);
  R5->SetLocation(10, 13.5);
	
  //TELEPORT
  teleport->SetLocation(16.5, 19.5);
  SWteleport->SetLocation(16.5, 18);
  R19->SetLocation(16.5, 16.5);
  Hteleport->SetLocation(16.5, 15);
  R9->SetLocation(16.5, 13.5);
	
  //GIGBE
  G1->SetLocation(7, 13.5);
  G2->SetLocation(10, 11.5);
  G3->SetLocation(13, 13.5);
	
  /********************************JRAE 05 Network Node Locations*******************************/

	
  /****************************JRAE 05 TACLANE Security Associations****************************/
	
  Hlhd->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
  Hcvn->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
  Hddg->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
  Husmc->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
  Husa->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
  Hjoint->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
  Hnctams->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
  Hcocom->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
  Hteleport->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
  // Disable for testing
  Hlhd->Enable(false);
  Hcvn->Enable(false);
  Hddg->Enable(false);
  Husmc->Enable(false);
  Husa->Enable(false);
  Husaf->Enable(false);
  Hjoint->Enable(false);
  Hnctams->Enable(false);
  Hcocom->Enable(false);
  Hteleport->Enable(false);
  //Red Network
  Hlhd->AddSecurityAssociation(cocom->GetIPAddr(), Hcocom->GetIPAddr());
#ifdef ADD_lATER
  H8->AddSecurityAssociation(abrigade, aH8_R41);
  H8->AddSecurityAssociation(asoc, aH5_R8);
  H8->AddSecurityAssociation(afs1c, aH6_R10);
  H8->AddSecurityAssociation(abatallion, aH9_R14);
  H8->AddSecurityAssociation(aplatoon, aH10_R13);
  H8->AddSecurityAssociation(auav, aH3_R6);
  H8->AddSecurityAssociation(aaorc, aH18_R25b);
  H8->AddSecurityAssociation(afs2c, aH2_R5);
  H8->AddSecurityAssociation(auavcrew, aH4_R3);
  H8->AddSecurityAssociation(ajsotf, aH17_R24);
  H8->AddSecurityAssociation(aship2r, aH13_R17);
  H8->AddSecurityAssociation(aship2tl, aH16_R17);
  H8->AddSecurityAssociation(aaoc1, aH7_R11);
  H8->AddSecurityAssociation(aaoc2, aH19_R11);
  H8->AddSecurityAssociation(aap, aH15_R21);
  H8->AddSecurityAssociation(aisufist, aH1_R1);
  H8->AddSecurityAssociation(aship1r, aH11_R16);		
 
  //Green network
  H12->AddSecurityAssociation(aship2g, aH14_R17);
  H14->AddSecurityAssociation(aship1g, aH12_R16);
  Hy->AddSecurityAssociation(aship1g, aHx_R37);
  Hx->AddSecurityAssociation(aship2g, aHy_R35);
#endif
  /****************************JRAE 05 TACLANE Security Associations****************************/

	
  s.NewLocation(-0.5, 4);
  s.NewLocation(20.5, 17);

  // Add tcpserver at cocom
  TCPServer* server = (TCPServer*)cocom->AddApplication(TCPServer());
  server->BindAndListen(40);      // Use port 40
  server->CloseOnEmpty();         // Close tcp connection when all sent
  server->Start(0);               // start the server app

  // Create a single stats object shared by all receivers
  AverageMinMax amm;
  Uniform startRNG(0, 1);
  // Add Track update (tcpReceive) objects at several hosts
  AppApplications(lhd,  cocom, &amm, startRNG, voipCount);
  //AppApplications(cvn1, cocom, &amm, startRNG, voipCount/3);
  AppApplications(cvn2, cocom, &amm, startRNG, voipCount/3);
  AppApplications(cvn3, cocom, &amm, startRNG, voipCount/3);
  AppApplications(ddg,  cocom, &amm, startRNG, voipCount);
  AppApplications(usmc, cocom, &amm, startRNG, voipCount);
  AppApplications(usa,  cocom, &amm, startRNG, voipCount);
  AppApplications(usaf, cocom, &amm, startRNG, voipCount);
  AppApplications(joint,cocom, &amm, startRNG, voipCount);

  s.Progress(10);
  s.StopAt(3600);
  s.StopAt(60);
  s.Run();	
  cout << "Average response time " << amm.Average()
       << " nsamples " << amm.Samples() << endl;
}
