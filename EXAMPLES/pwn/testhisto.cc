// Test histogram/Histograph functionality
// George F. Riley, Georgia Tech, Fall 2003

#include <iostream>
#include <fstream>

#include "histograph.h"
#include "rng.h"
#include "validation.h"

using namespace std;

int main(int argc, char** argv)
{
  Validation::Init(argc, argv);
  Count_t nBins = 20;
  Count_t updateRate = 1;
  Count_t sampleCount = 1000000;
  bool    cdf = false;
  
  if (argc > 1) nBins = atol(argv[1]);
  if (argc > 2) updateRate = atol(argv[2]);
  if (argc > 3) sampleCount = atol(argv[3]);
  if (argc > 4) cdf = true;
  
  if (Validation::sameSeed)
    Random::GlobalSeed(12345, 54321, 77777, 88888, 1234567, 765431);
  // SampleCount of zero means infinite loop
  Histograph  hn(10, nBins, -10);
  Normal      n(0, 10);
  hn.ShowCDF(cdf);
  //Histograph  he(20, nBins);
  //Exponential e(5);
  hn.UpdateRate(updateRate);
  //he.UpdateRate(updateRate);
  for (Count_t i = 0; i <= sampleCount; sampleCount ? ++i : i)
    {
      Random_t v = n.Value();
      hn.Record(v);
      //v = e.Value();
      //he.Record(v);
    }
  ofstream f("testhisto.bin");
  hn.Log(f, 0, ' ');
  f.close();
}
