/*

* GTNetS provides a portable C++ class library for network simulations
*
* Copyright (C) 2003 George F. Riley
*
* No guarantees or warranties or anything are provided or implied in any way
* whatsoever.  Use this program at your own risk.  Permission to use this
* program for any purpose is given, as long as the copyright is kept intact.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

* Permission is hereby granted for any non-commercial use of this code

*/

// Georgia Tech Network Simulator - DAW class
// Mohamed Abd Elhafez.  Georgia Tech, Fall 2004

// Implements the DAW algorithm for worm containment


#include <iostream>

#include "debug.h"
#include "daw.h"
#include "l2proto802.3.h"
#include "node.h"
#include "droppdu.h"
#include "hex.h"
#include "icmp.h"
#include "math.h"

using namespace std;

#define TOKENS 10
#define SIZE 10
#define THRESHOLD 1     // one fail rate per second
#define ICMP_PROTOCOL 1
#define TIMEOUT 500
#define BETA 0.2
//#define GAMMA 
#define MINUTE 60
#define MINUTE_THRESHOLD 30
#define HOUR 3600
#define HOUR_THRESHOLD 200

Time_t DAW::timeout = TIMEOUT;


FailureRecord::FailureRecord(): failCount(0), t(Simulator::Now()), failRate(0.0)
{
}

BlockRecord::BlockRecord(): record(nil), tokens(TOKENS), time(Simulator::Now())
{
}

BlockRecord::BlockRecord(FailureRecord* r, int tok, Time_t t): record(r), tokens(tok), time(t)
{
}

DAW::DAW()
{
  cout << "creating DAW module " << endl;
}

bool DAW::IsOutIface(Interface* iface)
{
  bool found = false;

  for (unsigned i = 0; i < outIfs.size(); ++i)
    {
      if (outIfs[i] == iface)
	found = true;
    }
  return found;
}


void DAW::ProcessOutPacket( Packet* p, IPAddr_t i,  int type, Interface* iface)
 {
   IPV4Header* iphdr = (IPV4Header*)p->PeekPDU();

   if (!IsOutIface(iface))
     {
       if( iphdr->protocol == ICMP_PROTOCOL)
	 {
 	   // Icmp packet, check if it is destination unreachable
	   ICMPHeader* icmp = (ICMPHeader*)p->PeekPDU(1);
	   
	   if(icmp->type == ICMP::DESTINATION_UNREACHABLE)
	     {
	       // cout << "DAW: ICMP pckt rcvd" << endl;
	       // Do we have an entry for the dest IP ?
	       FailMap_t::iterator j = failureratemap.find(iphdr->dst);
	       if(j == failureratemap.end())
		 {
		   failureratemap[iphdr->dst] = FailureRecord();
		   UpdateFailureRateRecord(failureratemap.find(iphdr->dst));
		 }    
	       else
		 {
		   UpdateFailureRateRecord(j);
		 }
	     }
	 }
     }

   iface->Xmit(p, i, type);
 }


void DAW::ProcessInPacket( Packet* p, Interface* iface)
{
  // check here to discard if blocked with no further processing
  IPV4Header* iphdr = (IPV4Header*)p->PeekPDU(2);

  if (!IsOutIface(iface))
    {
      if(iphdr->protocol == TCP_PROTOCOL)
	{
	  TCPHeader* tcphdr = (TCPHeader*)p->PeekPDU(3);
	  if(tcphdr->flags == TCP::SYN)
	    {
	      BlockMap_t::iterator k = blockmap.find(iphdr->src);
	      if(k != blockmap.end())
		{
		  TemporalRateLimit(p, k, iface);
		  return;
		}
	    }
	}
      else if(iphdr->protocol == UDP_PROTOCOL)
	{
	  BlockMap_t::iterator k = blockmap.find(iphdr->src);
	  if(k != blockmap.end())
	    {
	      TemporalRateLimit(p, k, iface);
	      return;
	    }
	}
    }
  iface->GetL2Proto()->DataIndication(p);
}


void DAW::UpdateFailureRateRecord(FailMap_t::iterator j)
{
  double frate;

  //cout << "DAW: Updatefarate .. " << endl; 
  BlockMap_t::iterator k = blockmap.find((*j).first);
  if(k != blockmap.end())
    {
      (*k).second.tokens--;
    }
  (*j).second.failCount++;

  if( ((*j).second.failCount % 10) == 0 )
    {
      frate = 10 / (Simulator::Now() - (*j).second.t);
      if ((*j).second.failCount == 10)
	{
	  (*j).second.failRate = frate;
	}
      else
	{
	  (*j).second.failRate = BETA * (*j).second.failRate + (1 - BETA) * frate;
	}
      if((*j).second.failRate > THRESHOLD && (k == blockmap.end()))
	{
	  // create block record for that host
	  blockmap[(*j).first] = BlockRecord(&((*j).second), TOKENS, Simulator::Now());
	}
      (*j).second.t = Simulator::Now();
    }
}


void DAW::BasicRateLimit(Packet* p, BlockMap_t::iterator j, Interface* iface)
{
  Time_t delta = Simulator::Now() - (*j).second.time;

  (*j).second.tokens = min(SIZE, (int)((*j).second.tokens + delta * THRESHOLD) );

  (*j).second.time = Simulator::Now();

  if((*j).second.tokens >= 1)
    {
      //      cout << "forwarding packet, tokens = " << (*j).second.tokens  << endl;
      // forward the request
      iface->GetL2Proto()->DataIndication(p);
    }
  else
    {
      //cout << "Dropping pkt" << endl;
      // drop the request
      DropPDU d("L3-DAW", p);
      iface->GetNode()->TracePDU(nil, &d);
      DEBUG(0,(cout << "Packet Dropped " << endl));
      Stats::pktsDropped++;
      delete p;
    }
}

void DAW::TemporalRateLimit(Packet* p, BlockMap_t::iterator j, Interface* iface)
{
  Time_t delta = Simulator::Now() - (*j).second.time;

  if ((*j).second.record->failCount <= MINUTE_THRESHOLD/2)
    {
      (*j).second.tokens = min(SIZE, (int)((*j).second.tokens + delta * THRESHOLD) );
    }
  else
    {
      double rate = (MINUTE_THRESHOLD - (*j).second.record->failCount -  (*j).second.tokens ) / (MINUTE - ((int)(*j).second.time) % MINUTE);
      (*j).second.tokens = min(SIZE, (int)((*j).second.tokens + delta * rate ));
    }

  (*j).second.time = Simulator::Now();

  if((*j).second.tokens >= 1)
    {
      //      cout << "forwarding packet, tokens = " << (*j).second.tokens  << endl;
      // forward the request
      iface->GetL2Proto()->DataIndication(p);
    }
  else
    {
      //cout << "Dropping pkt" << endl;
      // drop the request
      DropPDU d("L3-DAW", p);
      iface->GetNode()->TracePDU(nil, &d);
      DEBUG(0,(cout << "Packet Dropped " << endl));
      Stats::pktsDropped++;
      delete p;
    }
}


void DAW::SetOutIfs(IFVec_t vec)
{
  outIfs = vec;
}

void DAW::SetOutIf(Interface* ifc)
{
  outIfs.push_back(ifc);
}

void DAW::SetTimeout(Time_t t)
{
  timeout = t;
}
