// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: ftp-client.cc 185 2004-11-05 16:40:27Z riley $



// Georgia Tech Network Simulator - FTP Client Class
// George F. Riley.  Georgia Tech, Spring 2002

// Define the FTP Client Class

#define DEBUG_MASK 0x01
#include "debug.h"
#include "ftp-client.h"
#include "simulator.h"

using namespace std;

// FTPAction methods
// Constructor
FTPAction::FTPAction()
  : action(NONE), peerIP(IPADDR_NONE),
    reqSz(0), fileSz(0),
    reqRv(0), fileRv(0),
    sleepTime(0), sleepRv(0),
    nRepeat(0), cRepeat(0), stepRepeat(0)
{
}

FTPAction::FTPAction(Actions_t a)
  : action(a), peerIP(IPADDR_NONE),
    reqSz(0), fileSz(0),
    reqRv(0), fileRv(0),
    sleepTime(0), sleepRv(0),
    nRepeat(0), cRepeat(0), stepRepeat(0)
{
}

FTPAction::FTPAction(const FTPAction& c)
  : action(c.action), peerIP(c.peerIP),
    reqSz(c.reqSz), fileSz(c.fileSz),
    reqRv(0), fileRv(0),
    sleepTime(c.sleepTime), sleepRv(0),
    nRepeat(c.nRepeat), cRepeat(c.cRepeat), stepRepeat(c.stepRepeat)
{
  // Copy each random variable if it exists
  if (c.reqRv)   reqRv   = c.reqRv->Copy();
  if (c.fileRv)  fileRv  = c.fileRv->Copy();
  if (c.sleepRv) sleepRv = c.sleepRv->Copy();
}

// Assignment Operator
FTPAction& FTPAction::operator=(const FTPAction& r)
{
  if (&r != this)
    { // ignore self assignment
      action  = r.action;
      peerIP  = r.peerIP;
      reqSz   = r.reqSz;
      fileSz  = r.fileSz;
      delete reqRv;
      if (r.reqRv)  reqRv  = r.reqRv->Copy();
      else          reqRv  = NULL;
      delete fileRv;
      if (r.fileRv) fileRv = r.fileRv->Copy();
      else          fileRv = NULL;
      sleepTime = r.sleepTime;
      delete sleepRv;
      if (r.sleepRv) sleepRv = r.sleepRv->Copy();
      else           sleepRv = NULL;
      nRepeat    = r.nRepeat;
      cRepeat    = r.cRepeat;
      stepRepeat = r.stepRepeat;
    }
  return *this;
}
FTPAction::~FTPAction()
{
  if (reqRv)   delete reqRv;
  if (fileRv)  delete fileRv;
  if (sleepRv) delete sleepRv;
}

void FTPAction::Reset()
{
  cRepeat = 0;
}

// FTPClient methods
FTPClient::FTPClient(const TCP& t)
  : state(CLOSED), currentStep(0), started(false),
    l4Proto((TCP*)t.Copy()), currentAction(0), timeoutEvent(0)
{
  l4Proto->AttachApplication(this);
}

FTPClient::~FTPClient()
{
  delete l4Proto;
}

// TimerHandler 
void FTPClient::Timeout(TimerEvent* ev)
{
  DEBUG(0,(cout << "FTPClient::Timeout time " 
        << Simulator::Now() << endl));
  timeoutEvent = NULL;
  NextAction();
}


// Upcalls from L4 protocol
void FTPClient::Receive(Packet*,L4Protocol*)
{ // Data received
}

void FTPClient::Sent(Count_t s, L4Protocol*)
{ // Data has been sent
  DEBUG(0,(cout << "FTPClient::Sent " << s << " bytes" << endl));
  getputCurrent += s;
  if (getputCurrent >= getputSize)
    { // Time for next
      switch (state) {
        case SENDING_GET :
          DEBUG(1,(cout << "Time to get file" << endl));
          state = GETTING;
          l4Proto->Send(currentAction->fileSz);
          break;
        case GETTING :
          DEBUG(1,(cout << "Time to advance to next action" << endl));
          state = OPEN;
          NextAction();
          break;
        case SENDING_PUT :
          DEBUG(1,(cout << "Time to put file" << endl));
          state = PUTTING;
          l4Proto->Send(currentAction->fileSz);
          break;
        case PUTTING :
          DEBUG(1,(cout << "Time to advance to next action" << endl));
          state = OPEN;
          NextAction();
          break;
        default:
          cout << "HuH?  FTPClient got sent in bad state " << state << endl;
          return;
        }
    }
}

void FTPClient::Closed(L4Protocol*)
{ // Connection has closed
  DEBUG(0,(cout << "FTPClient::Closed" << endl));
  if (state != CLOSING)
    {
      cout << "HuH? FTPClient::Closed in bad state " << state << endl;
    }
  state = CLOSED;
}

void FTPClient::ConnectionComplete(L4Protocol*)
{ // Connection request succeeded
  if (state != OPENING)
    {
      cout << "HuH?  FTPClient::ConnectionComplete when not opening" << endl;
      return;
    }
  state = OPEN;
  DEBUG(0,(cout << "FTPClient::ConnectionComplete at time " 
        << Simulator::Now() << endl));
  NextAction();
}

void FTPClient::ConnectionFailed(L4Protocol*, bool)
{  // Connection request failed
  if (state != OPENING)
    {
      cout << "HuH?  FTPClient::ConnectionFailed when not opening" << endl;
      return;
    }
  DEBUG(0,(cout << "FTPClient::ConnectionFailed at time " 
        << Simulator::Now() << endl));
  state = CLOSED;
  // If the connection fails, we just do nothing (no more actions)
}


// FTP Action processors
// All actions return step number, that can be used as a repeat target
Count_t FTPClient::Open(IPAddr_t ip)
{ // Open session with peer
  FTPAction a(FTPAction::OPEN);
  a.peerIP = ip;
  actions.push_back(a);
  return actions.size() - 1;
}

Count_t FTPClient::Get(Count_t req, Count_t file)
{ // Get, size of req, size of file
  FTPAction a(FTPAction::GET);
  a.reqSz = req;
  a.fileSz = file;
  actions.push_back(a);
  return actions.size() - 1;
}

Count_t FTPClient::Get(const Random& reqRv, const Random& fileRv)
{ // Get, size of req/file (random)
  FTPAction a(FTPAction::GET);
  a.reqRv = reqRv.Copy();
  a.fileRv = fileRv.Copy();
  actions.push_back(a);
  return actions.size() - 1;
}

Count_t FTPClient::Put(Count_t req, Count_t file)
{ // Get, size of req, size of file
  FTPAction a(FTPAction::PUT);
  a.reqSz = req;
  a.fileSz = file;
  actions.push_back(a);
  return actions.size() - 1;
}

Count_t FTPClient::Put(const Random& reqRv, const Random& fileRv)
{ // Get, size of req/rile (random)
  FTPAction a(FTPAction::PUT);
  a.reqRv = reqRv.Copy();
  a.fileRv = fileRv.Copy();
  actions.push_back(a);
  return actions.size() - 1;
}

Count_t FTPClient::Close()
{ // Close connection
  FTPAction a(FTPAction::CLOSE);
  actions.push_back(a);
  return actions.size() - 1;
}

Count_t FTPClient::Sleep(Time_t sleepTime)
{ // Sleep for specified time
  FTPAction a(FTPAction::SLEEP);
  a.sleepTime = sleepTime;
  actions.push_back(a);
  return actions.size() - 1;
}

Count_t FTPClient::Sleep(Random* sleepRv)
{ // Sleep for random time
  FTPAction a(FTPAction::SLEEP);
  a.sleepRv = sleepRv;
  actions.push_back(a);
  return actions.size() - 1;
}

Count_t FTPClient::Repeat(Count_t step, Count_t count)
{ // Repeat steps, starting at specified
  FTPAction a(FTPAction::REPEAT);
  a.stepRepeat = step;
  a.nRepeat = count;
  actions.push_back(a);
  return actions.size() - 1;
}


// Application methods
void FTPClient::StartApp()
{ // Called at time specified by Start
  for (ActionVec_t::size_type i = 0; i < actions.size(); ++i)
    {
      actions[i].Reset();
    }
  currentStep = 0;
  started = true;
  cout << "FTPClient started" << endl;
  NextAction();
}

void FTPClient::StopApp()
{ // Called at time specified by Stop
  started = false;
  if (timeoutEvent) timer.Cancel(timeoutEvent);
  timeoutEvent = NULL;
}

// FTPClient, private methods
FTPClient::ActionStatus_t FTPClient::NextAction()
{ // Returns true if successful, false if failed
  DEBUG(1,(cout << "FTPClient::NextAction, started " 
       << started 
       << " currentStep " << currentStep
           << " size " << actions.size() << endl));
  if (!started) return FINISHED;
  if (currentStep >= actions.size()) return FINISHED; // No more action
  currentAction = &actions[currentStep++];
  switch (currentAction->action) {
    case FTPAction::OPEN :
      if (state != CLOSED) return FAILED;
      l4Proto->Connect(currentAction->peerIP, FTP_PORT);
      state = OPENING;
      DEBUG(1,(cout << "FTPClient::NextAction, Opening" << endl));
      return SUCCESS;
      break;
    case FTPAction::GET  :
      if (state != OPEN) return FAILED;
      state = SENDING_GET;
      if (currentAction->reqRv)
        { // Use the random variable
          currentAction->reqSz = (Count_t)currentAction->reqRv->Value();
        }
      if (currentAction->fileRv)
        { // Use the random variable
          currentAction->fileSz = (Count_t)currentAction->fileRv->Value();
        }
      getputSize = currentAction->reqSz;
      getputCurrent = 0;
      l4Proto->Send(currentAction->reqSz);
      break;
    case FTPAction::PUT  :
      if (state != OPEN) return FAILED;
      state = SENDING_PUT;
      if (currentAction->reqRv)
        { // Use the random variable
          currentAction->reqSz = (Count_t)currentAction->reqRv->Value();
        }
      if (currentAction->fileRv)
        { // Use the random variable
          currentAction->fileSz = (Count_t)currentAction->fileRv->Value();
        }
      getputSize = currentAction->reqSz;
      getputCurrent = 0;
      l4Proto->Send(currentAction->reqSz);
      break;
    case FTPAction::CLOSE :
      if (state != OPEN) return FAILED;
      state = CLOSING;
      l4Proto->Close();
      break;
    case FTPAction::SLEEP : 
      // Schedule the sleep timer
      if (currentAction->sleepRv)
        { // Use the random variable
          currentAction->sleepTime = currentAction->sleepRv->Value();
        }
      timeoutEvent = new TimerEvent();
      timer.Schedule(timeoutEvent, currentAction->sleepTime, this);
      break;
    case FTPAction::REPEAT :
      currentAction->cRepeat++;
      DEBUG(0,(cout << "FTPAction::REPEAT, cRepeat "
               << currentAction->cRepeat << ", nRepeat "
               << currentAction->nRepeat << endl));
      if ((!currentAction->nRepeat) ||
          currentAction->cRepeat < currentAction->nRepeat)
        {
          currentStep = currentAction->stepRepeat;
        }
      return NextAction();
      break;
    default:    // Some compilers complain about missing enum values
      break;
  }
  return FAILED;
}


