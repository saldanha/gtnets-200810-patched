\chapter{Overview}\label{Chap:Overview}

\begin{figure}[h]
  \centerline{\psfig{figure=intro1.eps,width=5.0in}}
  \caption{\label{Fig:Intro1}Sample GTNetS Topology}
\end{figure}

The \GTNS\ simulator consists of a large number of \CPP\ objects
which implement the behavior of a variety of network elements.
Building and running a
simulation using \GTNS, requires creating a \CPP\ main program
that instantiates the various network elements to describe
a simulated topology, and the various applications and protocols
used to move simulated data through the topology. The \CPP\
main program is then compiled with any compiler that fully
complies with the \CPP\ standard\footnote{\GTNS\ has been compiled
successfully on Linux with g++-2.96, g++-3.x,
Microsoft Windows with MSVC-7.0,
and Sun Solaris with SUNWS-CC version??}.  After successfully
compiling the main program, it is linked with the \GTNS\ object
libraries
(on Linux these are available both {\em .a} and {\em .so} format
and on Windows they are a {\em .lib} file), which creates an
executable binary.  The resulting binary is simply executed
as any other application, which results in the simulation of
the topology and data flows specified in the main program.

In this chapter, we give a small example of using \GTNS\
to create a simple topology and two data flows.
An example \GTNS\ simulation is given for the simple topology shown in 
figure~\ref{Fig:Intro1}, and discussed in detail.  In this simulation,
there are six nodes, and two TCP flows from clients to servers.
Many of the \GTNS\ features will be used in this example, all of which are
desecribed in more detail later in this manual.  The main
program for the example is shown in program listing~\ref{Prog:intro1.cc}
starting on the following page.
Each line of the sample program will be discussed briefly
to describe it's purpose in the simulation.  After reading 
through this example, you should have a basic understanding
of the various functions provided by \GTNS\ and how to use
them.

\input{examples/intro1.tex}

\paragraph{Include Files.}  Lines \ref{Prog:intro1:Line:IncludeStart}\ -- 
\ref{Prog:intro1:Line:IncludeEnd}\ use the C/\CPP\ {\tt include}
directive to include the definitions for the various network elements
and simulation objects used in this simulation. The necessary include
files will of course vary from simulation to simulation depending on
which of the \GTNS\ objects are used in the simulation.  A complete
listing of all \GTNS\ objects and their corresponding include files
is given in Appendix~\ref{APP:IncludeCrossRef}.

\paragraph{Main Program.} The C++ {\tt main} entry point is defined
in line \ref{Prog:intro1:Line:Main}.
All \GTNS\ simulations must have a \CPP\ {\tt main} function.

\paragraph{Simulator Object.}  A single object of class {\tt Simulator}
must be created by all \GTNS\ simulations before any other \GTNS\ 
objects are created.  In our example, the {\tt Simulator} object
is created at line \ref{Prog:intro1:Line:Simulator}.
Chapter~\ref{Chap:Simulator} describes
all member functions availble in the class {\tt Simulator}.

\paragraph{Defining the Trace File.}  Lines \ref{Prog:intro1:Line:TraceStart}\ -- 
\ref{Prog:intro1:Line:TraceEnd}\ specify the
name of the trace file and the desired level of tracing.  For all
\GTNS\ simulations, there is a single global object of type {\tt Trace}
that manages all aspect of packet tracing in the simulation.
Line \ref{Prog:intro1:Line:TraceInstance}\ 
uses the {\tt Trace::Instance()} function to obtain a pointer to the
global trace object.
Line \ref{Prog:intro1:Line:TraceDotted}\  specifies that all \IPAs\ should be written
to the trace file in {\em dotted} notation, such as 192.168.0.1.  The
default notation for logging \IPAs\ is 32 bit hexadecimal.
Line \ref{Prog:intro1:Line:TraceOpen}\ 
opens the actual trace file and assigns the name {\tt intro1.txt}.
Line \ref{Prog:intro1:Line:TraceFlags}\  specifies that the flags field for all
\TCP\ headers logged
in the trace file should use a text based representation for the
flags (such as {\tt SYN|ACK}), rather than a 8 bit hexadecimal value.
Line \ref{Prog:intro1:Line:TraceIP}\ specifies that all {\em IPV4} headers should
be logged in the
trace file for every packet received and every packet transmitted at 
all nodes.  Complete details on specifying trace information can
be found in chapter~\ref{Chap:Tracing}.

\paragraph{Create Simulated Nodes.}  Lines \ref{Prog:intro1:Line:NodeStart}\ -- 
\ref{Prog:intro1:Line:NodeEnd}\ create the node objects
representing the six network nodes in the sample topology.
In \GTNS, node objects represent either end--systems (such as desktop
systems or web servers), routers, or hubs.
Notice that when creating {\tt Node} objects in this example, we used
statically defined {\tt Node} objects, rather than dynamically
allocating them with the  \CPP\ {\tt new} operator.  Keep in mind
that any {\tt Node} object created as part of the topology {\em must}
remain valid for the lifetime of the simulation.  In this case,
the nodes are created in the {\tt main} function, which of course
does not exit until the simulation is complete, and thus our nodes
remain valid until  then.
However, if the nodes are created in
a subroutine (such as {\tt int CreateNodes() \{\}}), they would not persist
after the {\tt CreateNodes} function completed unless dynamically allocated
with {\tt new}.

\paragraph{Create Simulated Links.}  Lines \ref{Prog:intro1:Line:LinkStart}\ -- 
\ref{Prog:intro1:Line:LinkEnd}\ create the 
five link objects in the sample topology.  First, line \ref{Prog:intro1:Line:Link}\ 
creates a
point--to--point link object of class {\tt Linkp2p}.  There are three
things of interest in this declaration.
First are two arguments to the constructor for {\tt Linkp2p} objects,
which specify the link bandwidth and propogation delay.  The arguments
are of type {\tt Rate\_t} and {\tt Time\_t} respectively, which are both
of type {\tt double}.  However, in \GTNS, anytime a variable of type
{\tt Rate\_t} is required, an object of class {\tt Rate} may be used
instead.  Objects of class {\tt Rate} require a single argument in
the constructor, which is a string value specifying rates using
commonly recognized abbreviations for multipliers (such as {\tt Mb}).
Similarly, anytime a variable of type
{\tt Time\_t} is required, an object of class {\tt Time} may be used
instead.  Objects of class {\tt Time} require a single argument in
the constructor, which is a string value specifying rates using
commonly recognized abbreviations for multipliers (such as {\tt ms}).
Complete details on the {\tt Rate} and {\tt Time} objects are givin
in chapter~\ref{Chap:Miscellaneous}.

Secondly, notice that the object {\tt link} is statically
allocated in this case (it is not allocated using the {\tt new} operator),
and will be destroyed when the enclosing subroutine exits.  This is
the accepted way of defining and parameterizing links, and will be discussed
in more detail below.  Lines \ref{Prog:intro1:Line:LinkAddStart}\ -- 
\ref{Prog:intro1:Line:LinkAddEnd}\ specify the links connecting
the clients to router {\tt r1} and the servers to router {\tt r2}.
{\tt Node} objects have a method {\tt AddDuplexLink} which creates links
between nodes.  In this example, there are three arguments to 
{\tt AddDuplexLink}, the {\tt Node} pointer for the opposite link
endpoint, the link object itself ({\tt link} in this case), and
an \IPA\ for the link endpoint.  It is important to note that the
{\tt AddDuplexLink} method makes a {\em copy} of the 
{\tt Link} object passed as the second parameter, rather
than using it directly.  Thus a single link object ({\tt link} in this
example) can be passed to any number of {\tt AddDuplexLink} calls, and
can subsequently be destroyed (when the subroutine exits) without
causing problems in the simulation.

Finally notice that the third argument to {\tt AddDuplexLink} in this
example is the \IPA\ of the local link endpoint. The argument must be
of type {\tt IPAddr\_t}, which is defined by \GTNS\ to be {\tt unsigned long}.
However, in \GTNS, whenever a variable of type {\tt IPAddr\_t} is needed, 
an object of class {\tt IPAddr} may be used instead.  Objects of
class {\tt IPAddr} require a single argument in the constructor,
specifying the desired \IPA\ in the familiar dotted notation.
Lines \ref{Prog:intro1:Line:LinkTwoStart}\ -- \ref{Prog:intro1:Line:LinkTwoEnd}\ create
the slower speed 10Mb link object connecting the two
routers, {\tt r1} and {\tt r2}.
Complete details on creating and using {\tt Link} objects are given
in chapter~\ref{Chap:Topology}.

\paragraph{Create the TCP Servers.}  The \TCP\ server objects are created
in lines \ref{Prog:intro1:Line:TCPServerStart}\ -- \ref{Prog:intro1:Line:TCPServerEnd}.
Lines \ref{Prog:intro1:Line:TCPServerOne}\ and \ref{Prog:intro1:Line:TCPServerTwo}\ create
two objects of class
{\tt TCPServer}, using the {\tt AddApplication} method of a {\tt Node} object.
The {\tt AddApplication} method accepts a single argument, which is 
a reference to any subclass of {\tt Application}.  The {\tt Application}
object that is passed to the {\tt AddApplication} method is {\em copied},
and a pointer to the copied object is returned. 
For distributed simulations, the return value is possibly {\tt NULL}, but
this is discussed elsewhere in this manual.
In this example, the
subclass of {\tt Application} that is used is class {\tt TCPServer}, which
creates an application that listens for connection requests on a specified
port number.
The constructor for {\tt TCPServer} objects has a single
parameter of class {\tt TCP}, which specifies the variant of \TCP\ to 
be used for this server object.  Notice that, in this example, an 
anonymous temporary object {\tt TCPTahoe()} is passed as the argument.
to the {\tt TCPServer} constructor.  The constructor for {\tt TCPServer}
makes a copy of the supplied object, rather than using it directly.
The anonymous temporary {\tt TCPTahoe} object is destroyed when the
constructors at lines \ref{Prog:intro1:Line:TCPServerOne}\ and 
\ref{Prog:intro1:Line:TCPServerTwo}\ are complete.  Also, the argument
to the {\tt TCPServer} constructor can be omitted, in which case a copy
of the default {\tt TCP} object is used.
Lines \ref{Prog:intro1:Line:TCPServerBindOne}\ and \ref{Prog:intro1:Line:TCPServerBindTwo}\
assign the {\tt TCPServer} objects to nodes {\tt s1} and {\tt s2}
respectively,
and bind to port 80.  Lines \ref{Prog:intro1:Line:TCPServerTraceOne}\ and 
\ref{Prog:intro1:Line:TCPServerTraceTwo}\  specify that all \TCP\ headers
should be logged in the trace file for all received and transmitted
packets for these \TCP\ endpoints.

\paragraph{Create the TCP Clients.}  The \TCP\ client applications are
created in lines \ref{Prog:intro1:Line:TCPClientStart}\ -- 
\ref{Prog:intro1:Line:TCPClientEnd}.  The objects of class {\tt TCPSend} are
created using the {\tt AddApplication} method of {\tt Node} objects,
as previously discussed.  The {\tt TCPSend} application connects
to a specified server and sends the specified amount of data.
{\tt TCPSend} constructors have four parameters, as follows.
The first and second arguments are the \IPA\ and port number of the \TCP\
server to which a connection is to be made.  The second argument in this
example illustrates the {\tt GetIPAddr} method for {\tt Node}
objects, which returns the \IPA\ of the node (or the first \IP\
of the node has more than one).
The third parameter is a temporary
object of class {\tt Random} (or any subclass of {\tt Random}), which
specifies a random variable that determines how much data to send
to the server.  The first client specifies a {\tt Uniform} random
variable at line \ref{Prog:intro1:Line:TCPClientOneUniform}\ 
which returns a uniform value between 1000 and
10,000. The second client specifies a {\tt Constant} random
variable at line \ref{Prog:intro1:Line:TCPClientTwoConstant},
that returns the constant value 100,000.
Details on the use of random variables in \GTNS\ are given in
chapter~\ref{Chap:Miscellaneous}.
The last parameter
is a reference to a temporary object of class {\tt TCP}
(or any subclass of {\tt TCP})
that specifies the \TCP\ variation to be used for this \TCP\ client.
Notice that in this case, an anonymous temporary object of class {\tt TCPTahoe}
is specified.  This parameter can be omitted, in which case a copy of the
default {\tt TCP} object is used.

Lines \ref{Prog:intro1:Line:TCPClientOneTrace}
 and  \ref{Prog:intro1:Line:TCPClientTwoTrace}\ 
enable tracing of the \TCP\ headers for all packets sent and 
received by these endpoints.

\paragraph{Start the Client Applications.}
Lines \ref{Prog:intro1:Line:TCPClientStartOne} and
\ref{Prog:intro1:Line:TCPClientStartTwo}\ tell the
simulator to create the connections between the clients and servers
and start the sending applications.
Line \ref{Prog:intro1:Line:TCPClientStartUniform}\  defines a statically allocated
{\tt Uniform} random variable
that will return random values uniformly in the interval [0.0, 2.0).
Lines \ref{Prog:intro1:Line:TCPClientStartOne}\  and
\ref{Prog:intro1:Line:TCPClientStartTwo}\ use the {\tt Start} method common to all \TCP\
applications that specifies when the application should create
the connection and begin sending the data.

\paragraph{Start the Simulation.}  Line \ref{Prog:intro1:Line:Progress}\ 
uses the {\tt Progress}
method of class {\tt Simulator} to request a message be printed
on {\tt stdout} every 1.0 seconds of simulation time, indicating the
simulation is progressing in time.
Line \ref{Prog:intro1:Line:StopAt}\ calls the {\tt StopAt} method of {\tt Simulator}
to tell the simulation when to stop.
Line \ref{Prog:intro1:Line:Run}\ calls the {\tt Run}
method of {\tt Simulator}, to run the simulation.  The {\tt Run}
method does not exit until the simulation completes.
