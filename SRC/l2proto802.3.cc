// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: l2proto802.3.cc 453 2005-12-01 22:57:50Z dheeraj $



// Georgia Tech Network Simulator - Layer 2 802.3 class
// George F. Riley.  Georgia Tech, Spring 2002

// Implementation for 802.3

#include <iostream>

//#define DEBUG_MASK 0x02
#include "debug.h"
#include "l2proto802.3.h"
#include "interface.h"
#include "ipv4.h"
#include "trace.h"
#include "node.h"
#include "hex.h"
#include "macaddr.h"
#include "llcsnap.h"
#include "link.h"
#include "packet-callbacks.h"

using namespace std;

L2Proto802_3::L2Proto802_3()
{
}

void L2Proto802_3::BuildDataPDU(MACAddr src, MACAddr dst, Packet* p)
{
  L2802_3Header* l2pdu = new L2802_3Header(src, dst, p->Size());
  p->PushPDU(l2pdu); // Add the 802.3 header 
}

void L2Proto802_3::DataRequest(Packet* p)
{
  // Process callbacks
  if(!iface->GetNode()->CallCallbacks(
         Layer(), 0, PacketCallbacks::TX, p, iface))
    return;
  // Transmit on attached link

  L2802_3Header* l2pdu = (L2802_3Header*)p->PeekPDU();
  if (Trace::Enabled())
    iface->GetNode()->TracePDU(this, l2pdu, p, "-");

  iface->GetLink()->Transmit(p, iface, iface->GetNode());
}

void L2Proto802_3::DataIndication(Packet* p, LinkEvent* le)
{
  bool fbrx = false; // True if first bit rx event
  if (le)
    {
      fbrx = (le->event == LinkEvent::PACKET_FIRSTBIT_RX);
    }
  if (le && !p)
    { // Use packet from link event
      p = le->p;
    }
  if (le) delete le;
  
  // Process callbacks
  if(!iface->GetNode()->CallCallbacks(
         Layer(), 0, PacketCallbacks::RX, p, iface))
    return;
  L2802_3Header* l2pdu = (L2802_3Header*)p->PeekPDU();
  if (Trace::Enabled()) iface->GetNode()->TracePDU(this, l2pdu, p, "+");
  p->PopPDU();
  iface->HandleLLCSNAP(p, fbrx);
}


bool L2Proto802_3::Busy()
{ // For 802.3, the protocol is busy only if link is busy
  Link* l = iface->GetLink();
  return l->Busy();
}

// L2802_3Header methods

// Specifies the size (in bytes) of the 802.3 header when transmitting
// on a link.  Normally, the default 12 is correct.  However, other
// simulators (notably ns2) do not model this, so direct comparisons
// to ns2 are difficult.  Changing this value to 0 (along with the
// corresponding value in the LLCSnap header) will give a more direct
// comparison to ns2.
Size_t L2802_3Header::txSize = 12;


// Construct from serialized buffer
L2802_3Header::L2802_3Header(char* b, Size_t& sz, Packet* p)
{
  DEBUG0((cout << "Constructing l2803-2, initial size " << sz << endl));
  Size_t s = 0;
  b = Serializable::GetSize(b, sz, s);
  DEBUG0((cout << "l2pdu size is " << s << endl));
  s -= sizeof(Size_t);
  sz -= s;
  b = Construct(b, s);
  DEBUG0((cout << "Constructing l2803-2, final  size " << sz << endl));
  p->PushPDUBottom(this);  // Add to packet (bottom of stack)
  new LLCSNAPHeader(b, sz, p);
}

void L2802_3Header::Trace(Tfstream& tos, Bitmap_t b, Packet* p, const char* s)
{  // Trace the contents of this pdu
  tos << " ";
  if (s) 
    tos << s;
  tos << "L2";
  if (Detail(L2Proto802_3::TYPE, b))  tos << "-802.3";
  // Below moved to LLC/SNAP header
  //if (Detail(L2Proto802_3::PROTO, b)) tos << " " << Hex4(l3proto);
  if (Detail(L2Proto802_3::LENGTH, b)) 
    { // Check if packet passed, which we need for length
      if (p)
        tos << " " << p->Size();
    }
  if (Detail(L2Proto802_3::SRC, b))   tos << " " << src;
  if (Detail(L2Proto802_3::DST, b))   tos << " " << dst;
  if (Detail(L2Proto802_3::UID, b))
    {
      if (p) tos << " " << p->uid;
    }
}

Size_t L2802_3Header::SSize()
{
  //return sizeof(src) + sizeof(dst) + sizeof(l3proto);
  return 6 + 6 + 2;
}

char*  L2802_3Header::Serialize(char* b, Size_t& sz)
{ // Serialize to a working buffer
  DEBUG(1,(cout << "Serializing l28023 src " << Hex2(src.macAddr)
           << " dst " << Hex2(dst.macAddr)
           << " lth " << length << endl));
  b = SerializeToBuffer(b, sz, src.macAddr);
  b = SerializeToBuffer(b, sz, dst.macAddr);
  Word_t p = length;
  b = SerializeToBuffer(b, sz, p);
  return b;
}

char*  L2802_3Header::Construct(char* b, Size_t& sz)
{ // Construct from serialized buffer
  b = ConstructFromBuffer(b, sz, src);
  b = ConstructFromBuffer(b, sz, dst);
  Word_t p = 0;
  b = ConstructFromBuffer(b, sz, p);
  length = p;
  DEBUG0((cout << "L2Hdr::Construct, src " << Hex2(src.macAddr)
          << " dst " << Hex2(dst.macAddr)
          << " length " << length << endl));
  return b;
}

