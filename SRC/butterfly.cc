// Define a Butterfly Supercomputer Interconnect Network
// George F. Riley, Georgia Tech, Spring 2005

#include <deque>

#include "butterfly.h"
#include "node.h"

using namespace std;

Butterfly::Butterfly(Count_t n, Count_t r, Count_t s,IPAddr_t lip,IPAddr_t rip)
    : nNodes(n), radix(r), stages(s), switches(s, NodeVec_t())
{
  // Create switch nodes
  //Count_t adder = radix; // This is not right..think about this
  for (Count_t s = 0; s < stages; ++s)
    {
      for (Count_t i = 0; i < nNodes/radix; ++i)
        {
	  //FirstBitNode* n = new FirstBitNode();
	  FirstBitRxNode* n = new FirstBitRxNode();
          switches[s].push_back(n);
          // Left off here
        }
    }
  for (Count_t s=0; s < stages-1; ++s)
    {
      //cout<<"s="<<s<<endl;
      for (Count_t i=0; i < nNodes/radix; ++i)
	{
	  for(Count_t j=0; j< radix; ++j)
	    {
	      int rs = (int)pow((double)radix, (int)s);
	      int rsp1 = (int)pow((double)radix, (int)(s+1));
	      Count_t newNode=(i/rsp1) * rsp1 + j*rs + (i%rs);
	      //cout<<"newNode="<<newNode<<endl;
	      GetSwitch(s, i)->AddDuplexLink((GetSwitch(s+1, newNode)));
	    }
	}
    }
  // Create input nodes
  for (Count_t i = 0; i < nNodes; ++i)
    {
      //FirstBitNode* n = new FirstBitNode();
      //FirstBitRxNode* n = new FirstBitRxNode();
      Node* n = new Node();
      iNodes.push_back(n);
      n->SetIPAddr(lip+i);
      // Create a link from input node to corresponding stage1 router
      FirstBitRxNode* r = GetSwitch(0, i/radix);
      n->AddDuplexLink(r);
    }
  // Create output nodes
  for (Count_t i = 0; i < nNodes; ++i)
    {
      //FirstBitNode* n = new FirstBitNode();
      //FirstBitRxNode* n = new FirstBitRxNode();
      Node* n = new Node();
      oNodes.push_back(n);
      n->SetIPAddr(rip+i);
      // Create a link from output node to corresponging last stage router
      FirstBitRxNode* r = GetSwitch(stages-1, i/radix);
      n->AddDuplexLink(r);
    }
}

FirstBitRxNode* Butterfly::GetSwitch(Count_t s, Count_t i)
{ // Get the i'th switch from stage s
  return (FirstBitRxNode*)switches[s][i];
}

FirstBitRxNode* Butterfly::GetINode(Count_t i)
{ // Get the i'th input node
  return (FirstBitRxNode*)iNodes[i];
}

FirstBitRxNode* Butterfly::GetONode(Count_t i)
{ // Get the i'th output node
  return (FirstBitRxNode*)oNodes[i];
}

void Butterfly::BoundingBox(const Location& ll, const Location& ur)
{
  deque<Meters_t> xOffsets;
  Meters_t yDelta = (ur.Y() - ll.Y()) / nNodes;
  Meters_t xUnits = 1;
  Meters_t totXUnits = 1;
  for (Count_t i = 0; i < stages; ++i)
    {
      xOffsets.push_front(xUnits);
      totXUnits += xUnits;
      xUnits *= 2;
    }
  Meters_t xDelta = (ur.X() - ll.X()) / totXUnits;
  // Place input nodes
  Meters_t cx = ll.X();
  Meters_t cy = ll.Y();
  for (Count_t i = 0; i < nNodes; ++i)
    {
      Node* n = GetINode(i);
      n->SetLocation(Location(cx, cy));
#ifdef HAVE_QT
      n->Shape(Node::CIRCLE);
#endif
      cy += yDelta;
    }
  cx += xDelta;
  cy = ll.Y() + yDelta*radix/2 - yDelta/2;
  // Place switches
  for (Count_t s = 0; s < stages; ++s)
    {
      for (Count_t i = 0; i < nNodes/radix; ++i)
        {
          Node* n = GetSwitch(s, i);
          n->SetLocation(Location(cx, cy));
          cy += yDelta * radix;
        }
      if (s < (stages-1)) cx += xOffsets[s];
      cy = ll.Y() + yDelta*radix/2 - yDelta/2;
    }
  // Place output nodes
  cx += xDelta;;
  cy = ll.Y();
  for (Count_t i = 0; i < nNodes; ++i)
    {
      Node* n = GetONode(i);
      n->SetLocation(Location(cx, cy));
#ifdef HAVE_QT
      n->Shape(Node::CIRCLE);
#endif
      cy += yDelta;
    }
}

