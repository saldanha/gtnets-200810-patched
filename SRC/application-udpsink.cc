// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
// 		
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id:


// Georgia Tech Network Simulator - UDP Sync Application class
// George F. Riley.  Georgia Tech, Summer 2002

// Implement a UDP Sink Application Class

#include <iostream>

#include "debug.h"
#include "simulator.h"
#include "application-udpsink.h"
#include "datapdu.h"

using namespace std;

UDPSink::UDPSink(PortId_t p)
  : l4UDP(nil), node(nil), logEvent(nil), stats(nil), delayStats(nil),
    statsUpdateInterval(1.0), firstPacketRx(0), lastPacketRx(0),
    port(p), bytesRx(0), packetsRx(0), packetsLost(0),
    nextSeqRx(0), statsLogType(LOSS), 
    lastLogTime(0), lastPacketsRx(0), lastBytesRx(0), lastPacketsLost(0),
    iatHist(nil), lastTransit(0.0), jitter(0.0)
{
}

UDPSink::UDPSink(const UDPSink& c)
  : l4UDP(nil), node(nil), logEvent(nil), stats(nil), delayStats(nil),
    statsUpdateInterval(c.statsUpdateInterval), firstPacketRx(0),
    lastPacketRx(0),
    port(c.port), bytesRx(0), packetsRx(0), packetsLost(0),
    nextSeqRx(c.nextSeqRx), statsLogType(c.statsLogType),
    lastLogTime(c.lastLogTime),
    lastPacketsRx(c.lastPacketsRx), lastBytesRx(c.lastBytesRx),
    lastPacketsLost(c.lastPacketsLost),
    iatHist(nil), lastTransit(0.0), jitter(0.0)
{
}

UDPSink::~UDPSink()
{
  if (l4UDP) delete l4UDP;
  if (logEvent)
    {
      Scheduler::Cancel(logEvent);
      delete logEvent;
    }
  if (stats) delete stats;
  if (delayStats) delete delayStats;
  if (iatHist) delete iatHist;
}

  
void UDPSink::Receive(Packet* p, L4Protocol* udp, Seq_t seq)
{
  if (firstPacketRx == 0.0) firstPacketRx = Simulator::Now();
  if (iatHist && lastPacketRx != 0.0)
    { // Log iAT
      Count_t units = (Count_t)((Simulator::Now() - lastPacketRx) / iatUnits);
      (*iatHist)[units]++;  // Increment the jitter histogram for this packet
    }
  lastPacketRx = Simulator::Now();
  
  // Track the jitter
  Time_t thisTransit = Simulator::Now() - p->time;
  if (lastTransit != 0.0)
    { // Calculate jitter per RFC189
      Time_t diff = fabs(thisTransit - lastTransit);
      jitter += (diff - jitter) / 16.0;
    }
  lastTransit = thisTransit;
  
  DEBUG0((cout << "Hello from UDPSink::receive, seq " << seq << endl));
  // Count the bytes
  Data* d = (Data*)p->PopPDU();
  if (d) bytesRx += d->Size();
  // Figure the loss
  if (seq >= nextSeqRx)
    { // Ignore out-of order
      // Count the packets
      packetsRx++;
      Count_t lost = (seq - nextSeqRx); 
      packetsLost += lost;
      //if (lost) cout << "New lost count " << packetsLost << endl;
      nextSeqRx = seq + 1;
    }
  // Record the delay if requestd
  if (delayStats) delayStats->Record(Simulator::Now() - p->time);
  delete p;
}


void UDPSink::StartApp()
{
  DEBUG0((cout << "UDPSink starting at time " << Simulator::Now() << endl));
  StopApp(); // Cancel any pending actions
  if (!node)
    {
      cout << "HuH?  Can't start UPDSinkApp with no attached node" << endl;
      return;
    }
  
  if (!l4UDP)
    { // Need to allocate the UDP and bind endpoint
      DEBUG0((cout << "Creating new UDP and binding" << endl));
      l4UDP = new UDP(node);
      node->Bind(l4UDP->Proto(), port, l4UDP);
    }
  if (stats)
    { // Need the logging event
      if (!logEvent) logEvent = new Event(LOG_STATS);
      Scheduler::Schedule(logEvent, statsUpdateInterval, this);
    }
}

void UDPSink::StopApp()
{ // Cancel and delete the log event
  if (logEvent)
    { 
      Scheduler::Cancel(logEvent);
      delete logEvent;
      logEvent = nil;
    }
}

void UDPSink::AttachNode(Node* n)
{
  node = n;
  if (!l4UDP)
    { // Need to allocate the l4 proto
      l4UDP = new UDP(node);
      node->Bind(l4UDP->Proto(), port, l4UDP);
      DEBUG0((cout << "UDPSink::Attach, new UDP" << endl));
    }
  l4UDP->AttachApplication(this);
}

      
void UDPSink::Handle(Event* e, Time_t t)
{
  DEBUG0((cout << "UDPSink::Handle, event " << e->event 
          << " time " << Simulator::Now() << endl));
  switch (e->event) {
    case LOG_STATS :
      DEBUG0((cout << "UDPSink::Handle, log stats" << endl));
      LogStats();
      // Reschedule
      Scheduler::Schedule(e, statsUpdateInterval, this);
      return;
    default :
      break;
  }
  Application::Handle(e, t);  // May be application event
}

void UDPSink::Bind(Node* n, PortId_t p)
{
  node = n;
  if (!l4UDP)
    { // Need to allocate the l4 proto
      l4UDP = new UDP(node);
    }
  l4UDP->AttachApplication(this);
  n->Bind(l4UDP->Layer(), p, l4UDP);
}

void UDPSink::Bind(PortId_t p)
{
  if (!node)
    {
      cout << "HuH?  Can't bind UPDSinkApp with no attached node" << endl;
      return;
    }
  if (!l4UDP)
    { // Need to allocate the l4 proto
      l4UDP = new UDP(node);
    }
  node->Bind(l4UDP->Layer(), p, l4UDP);
}

Application* UDPSink::Copy() const
{
  return new UDPSink(*this);
}

// UDPSink specific methods
Statistics* UDPSink::SetStatistics(const Statistics& s)
{
  if (stats) delete stats;
  stats = s.Copy();
  if (logEvent) 
    { // Cancel existing log event
      Scheduler::Cancel(logEvent);
      delete logEvent;
      logEvent = nil;
    }
  logEvent = new Event(LOG_STATS);
  Scheduler::Schedule(logEvent, statsUpdateInterval, this);
  return stats;
}

Statistics* UDPSink::DelayStatistics(const Statistics& s)
{
  if (delayStats) delete delayStats;
  delayStats = s.Copy();
  return delayStats;
}

Rate_t UDPSink::RxRate()
{
  Time_t elapsed = Simulator::Now() - firstPacketRx;
  if (elapsed == 0.0) return 0;
  return bytesRx / elapsed;
}

void UDPSink::TrackInterArrival(Time_t binSize)
{
  iatUnits = binSize;
  iatHist = new IATHisto_t();
}

void UDPSink::LogInterArrival(ostream& os, const char* hdr, const char sep)
{ // Log iat info to a file
  if (!iatHist) return; // No IAT info
  if (hdr) os << hdr << endl;
  for (IATHisto_t::iterator i = iatHist->begin(); i != iatHist->end(); ++i)
    {
      os << i->first << sep << i->second << endl;
    }
}

// Private methods
void UDPSink::LogStats()
{
  if (!stats) return; // No statistics object
  Count_t thisRx = packetsRx - lastPacketsRx;
  Count_t thisBytesRx = bytesRx - lastBytesRx;
  Count_t thisLost = packetsLost - lastPacketsLost;
  Mult_t  lossRate;
  if (thisRx + thisLost)
    {
      lossRate = (double)(thisLost)/(double)(thisRx + thisLost);
    }
  else
    {
      lossRate = 0.0;
    }
  
  if (statsLogType == LOSS)
    {
      stats->Record(lossRate);
    }
  if (statsLogType == BANDWIDTH)
    {
      double bandwidth = thisBytesRx * 8.0 / (Simulator::Now() - lastLogTime);
      DEBUG0((cout << "Recording bandwidth " << bandwidth << endl));
      stats->Record(bandwidth);
    }
  
  lastPacketsRx = packetsRx;
  lastBytesRx = bytesRx;
  lastPacketsLost = packetsLost;
  lastLogTime = Simulator::Now();
}
