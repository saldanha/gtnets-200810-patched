// GENERAL PUBLIC LICENSE AGREEMENT
// 
// PLEASE READ THIS DOCUMENT CAREFULLY BEFORE UTILIZING THE PROGRAM
// 
// BY UTILIZING THIS PROGRAM, YOU AGREE TO BECOME BOUND BY THE TERMS OF
// THIS LICENSE.  IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO
// NOT USE THIS PROGRAM OR ANY PORTION THEREOF IN ANY FORM OR MANNER.
// 
// This Program is licensed, not sold to you by GEORGIA TECH RESEARCH
// CORPORATION ("GTRC"), owner of all code and accompanying documentation
// (hereinafter "Program"), for use only under the terms of this License,
// and GTRC reserves any rights not expressly granted to you.
// 
// 1.  This License allows you to:
// 
// (a) make copies and distribute copies of the Program's source code
// provide that any such copy clearly displays any and all appropriate
// copyright notices and disclaimer of warranty as set forth in Article 5
// and 6 of this License.  All notices that refer to this License and to
// the absence of any warranty must be kept intact at all times.  A copy
// of this License must accompany any and all copies of the Program
// distributed to third parties.
// 
// A fee may be charged to cover the cost associated with the physical
// act of transferring a copy to a third party.  At no time shall the
// program be sold for commercial gain either alone or incorporated with
// other program(s) without entering into a separate agreement with GTRC.
//  
// 
// (b) modify the original copy or copies of the Program or any portion
// thereof ("Modification(s)").  Modifications may be copied and
// distributed under the terms and conditions as set forth above,
// provided the following conditions are met:
// 
//     i) any and all modified files must be affixed with prominent
// notices that you have changed the files and the date that the changes
// occurred.
//     
//     ii) any work that you distribute, publish, or make available, that
// in whole or in part contains portions of the Program or derivative
// work thereof, must be licensed at no charge to all third parties under
// the terms of this License.
// 
//    iii) if the modified program normally reads commands interactively
// when run, you must cause it, when started running for such interactive
// use in the most ordinary way, to display and/or print an announcement
// with all appropriate copyright notices and disclaimer of warranty as
// set forth in Article 5 and 6 of this License to be clearly displayed.
// In addition, you must provide reasonable access to this License to the
// user.
// 
// Any portion of a Modification that can be reasonably considered
// independent of the Program and separate work in and of itself is not
// subject to the terms and conditions set forth in this License as long
// as it is not distributed with the Program or any portion thereof.
// 
// 
// 2. This License further allows you to copy and distribute the Program
//    or a work based on it, as set forth in Article 1 Section b in
//    object code or executable form under the terms of Article 1 above
//    provided that you also either:
// 
//    i) accompany it with complete corresponding machine-readable source
// code, which must be distributed under the terms of Article 1, on a
// medium customarily used for software interchange; or,
// 
//   ii) accompany it with a written offer, valid for no less than three
// (3) years from the time of distribution, to give any third party, for
// no consideration greater than the cost of physical transfer, a
// complete machine-readable copy of the corresponding source code, to be
// distributed under the terms of Article 1 on a medium customarily used
// for software interchange; or,
// 
// 
// 3.  Export Law Assurance.
// 
// You agree that the Software will not be shipped, transferred or
// exported, directly into any country prohibited by the United States
// Export Administration Act and the regulations thereunder nor will be
// used for any purpose prohibited by the Act.
//  
// 4.  Termination.
// 
// If at anytime you are unable to comply with any portion of this
// License you must immediately cease use of the Program and all
// distribution activities involving the Program or any portion thereof.
// 
// 
// 5.  Disclaimer of Warranties and Limitation on Liability.
// 
// YOU ACCEPT THE PROGRAM ON AN "AS IS" BASIS.  GTRC MAKES NO WARRANTY
// THAT ALL ERRORS CAN BE OR HAVE BEEN ELIMINATED FROM PROGRAM.  GTRC
// SHALL NOT BE RESPONSIBLE FOR LOSSES OF ANY KIND RESULTING FROM THE USE
// OF PROGRAM AND ITS ACCOMPANYING DOCUMENT(S), AND CAN IN NO WAY PROVIDE
// COMPENSATION FOR ANY LOSSES SUSTAINED, INCLUDING BUT NOT LIMITED TO
// ANY OBLIGATION, LIABILITY, RIGHT, CLAIM OR REMEDY FOR TORT, OR FOR ANY
// ACTUAL OR ALLEGED INFRINGEMENT OF PATENTS, COPYRIGHTS, TRADE SECRETS,
// OR SIMILAR RIGHTS OF THIRD PARTIES, NOR ANY BUSINESS EXPENSE, MACHINE
// DOWNTIME OR DAMAGES CAUSED TO YOU BY ANY DEFICIENCY, DEFECT OR ERROR
// IN PROGRAM OR MALFUNCTION THEREOF, NOR ANY INCIDENTAL OR CONSEQUENTIAL
// DAMAGES, HOWEVER CAUSED.  GTRC DISCLAIMS ALL WARRANTIES, BOTH EXPRESS
// AND IMPLIED RESPECTING THE USE AND OPERATION OF PROGRAM AND ITS
// ACCOMPANYING DOCUMENTATION, INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE AND ANY IMPLIED
// WARRANTY ARISING FROM COURSE OF PERFORMANCE, COURSE OF DEALING OR
// USAGE OF TRADE.  GTRC MAKES NO WARRANTY THAT PROGRAM IS ADEQUATELY OR
// COMPLETELY DESCRIBED IN, OR BEHAVES IN ACCORDANCE WITH ANY
// ACCOMPANYING DOCUMENTATION.  THE USER OF PROGRAM IS EXPECTED TO MAKE
// THE FINAL EVALUATION OF PROGRAM'S USEFULNESS IN USER'S OWN
// ENVIRONMENT.
// 
// GTRC represents that, to the best of its knowledge, the software
// furnished hereunder does not infringe any copyright or patent.
// 
// GTRC shall have no obligation for support or maintenance of Program.
// 
// 6.  Copyright Notice.
// 
// THE SOFTWARE AND ACCOMPANYING DOCUMENTATION ARE COPYRIGHTED WITH ALL
// RIGHTS RESERVED BY GTRC.  UNDER UNITED STATES COPYRIGHT LAWS, THE
// SOFTWARE AND ITS ACCOMPANYING DOCUMENTATION MAY NOT BE COPIED EXCEPT
// AS GRANTED HEREIN.
// 
// You acknowledge that GTRC is the sole owner of Program, including all
// copyrights subsisting therein.  Any and all copies or partial copies
// of Program made by you shall bear the copyright notice set forth below
// and affixed to the original version or such other notice as GTRC shall
// designate.  Such notice shall also be affixed to all improvements or
// enhancements of Program made by you or portions thereof in such a
// manner and location as to give reasonable notice of GTRC's copyright
// as set forth in Article 1.
// 
// Said copyright notice shall read as follows:
// 
// Copyright 2004
// Dr. George F. Riley
// Georgia Tech Research Corporation
// Atlanta, Georgia 30332-0415
// All Rights Reserved
//
// $Id: application-aodv.cc 466 2006-02-15 15:36:04Z riley $



// Georgia Tech Network Simulator - AODV Routing Protcol Application class
// George F. Riley.  Georgia Tech, Spring 2002

// Define the AODV routing protocol

// Uncomment below to enable debug level 0
//#define DEBUG_MASK 0x1
// 
/*+----------- tabstop 4 ---------+*/

#include <cstring>
#include "debug.h"
#include <assert.h>
#include "application-aodv.h"
#include "udp.h"
#include "tcp.h"
#include "node.h"
#include "simulator.h"
#include "rng.h"
#include "interface.h"
#include "timerbuckets.h"

using namespace std;

static void RouteResolveCallbackAODV(Packet *pPacket, void *arg);


//AODVEvent destructor method
//
AODVEvent::~AODVEvent() {
    
}

// Constructors
AODVApplication::AODVApplication(Node* n)
  : l4Proto(new UDP(n))
{
  l4Proto->AttachApplication(this);
  l4Proto->Bind(AODV_PORT); // Bind to the well known port number
  if(pAODVRouting == nil) {
     pAODVRouting = new RoutingAODV;
     pAODVRouting->who = this;
    
     pAODVRouting->RouteResolve = RouteResolveCallbackAODV;
     n->SetRoutingAODV(pAODVRouting); //to share the object in node/application
  }
  BroadcastTimeout = NULL;  
  HelloTimeout = NULL;     
  NeighborTimeout = NULL; 
  RouteCacheTimeout = NULL;
  LocalRepairTimeout = NULL; 
}


//TODO: add a destructor because we have resource to release.

// Copy constructor
AODVApplication::AODVApplication(const AODVApplication& c)
  : l4Proto((UDP*)c.l4Proto->Copy())
{
}

// Application Methods
void
AODVApplication::Receive(Packet* p, L4Protocol*, Seq_t nSeq)
{ // Data received
  Data* d = (Data*)p->PeekPDU();      // Get the AODV header without removal
  if (d->data)
  { // Data exists (should always)
    assert(d!=NULL);    //d should not be NULL
    assert(d->data!=NULL); //d->data should not be NULL either.!

    Byte PacketType;
    memcpy(&PacketType, (void *)(d->data), sizeof(Byte)); 
    cout << "AODVApp " << this << " got msg type " << (int) PacketType << endl <<endl;
    
    switch(PacketType) {
        case AODV_REQUEST: //100
          RecvRequest(p);
          break;
        case AODV_REPLY:   //101
          RecvReply(p);
          break;
        case AODV_HELLO:   //102
          RecvHello(p);
          break;
        case AODV_ERROR:
          RecvError(p);
          break;
        default:
              cout << "AODVApp " << this << " got UNKNOWN msg type "
                       << PacketType << endl;
          delete p; //free the packet silently.
          break;
          
    }
    
  }
}

void
AODVApplication::StartApp()    // Called at time specified by Start
{

  //Start those timers, including sending a hello message to everyone!
  //Even we can reuse the pointer, we delare them respectively

  AODVEvent *pStartEvent0 = new AODVEvent(AODVEvent::BROADCAST_TIMEOUT);
  Timeout(pStartEvent0);
  
  AODVEvent *pStartEvent1 = new AODVEvent(AODVEvent::HELLO_TIMEOUT);
  Timeout(pStartEvent1);
  
  AODVEvent *pStartEvent2 = new AODVEvent(AODVEvent::NEIGHBOR_TIMEOUT);
  Timeout(pStartEvent2);
  
  AODVEvent *pStartEvent3 = new AODVEvent(AODVEvent::ROUTECACHE_TIMEOUT);
  Timeout(pStartEvent3);
  
}

void 
AODVApplication::StopApp()     // Called at time specified by Stop
{ // May need something here, not sure
}

Application* 
AODVApplication::Copy() const
{
  return new AODVApplication(*this);
}

//Note: this is not a member function
//      for call from lower layer when route not found
static void
RouteResolveCallbackAODV(Packet *pPacket, void *arg) {
//do route resolve and enqueue the packet in RouteResolve. 
  ((AODVApplication *) arg)->RouteResolve(pPacket);
  return;
}

// Timers timeout handles.
void
AODVApplication::Timeout(TimerEvent *pTimerEvent) {
    
  AODVEvent *pAODVEvent = (AODVEvent *)pTimerEvent;
    DEBUG(9,(cout << "AODV::Timeout this " << this << " type " 
          << pAODVEvent->event
          << " time " << Simulator::Now() << endl));
  //depending on the event, do what it should
  switch(pAODVEvent->event) {
    case AODVEvent::BROADCAST_TIMEOUT:
      BroadcastIDPurge();
      BroadcastTimeout = NULL;
      ScheduleTimer(AODVEvent::BROADCAST_TIMEOUT,
              BroadcastTimeout,
              BCAST_ID_SAVE);
      break;
    case AODVEvent::HELLO_TIMEOUT:
      {  
      SendHello();
      Random *pTemp = new Uniform();
      Random_t rv = pTemp->Value();
      delete pTemp;
      double localInterval = MinHelloInterval + 
          (MaxHelloInterval - MinHelloInterval) * rv;
      HelloTimeout = NULL;
      ScheduleTimer(AODVEvent::HELLO_TIMEOUT,
              HelloTimeout, 
              localInterval);
      }
      break;
    case AODVEvent::NEIGHBOR_TIMEOUT: 
      NeighborPurge();
      NeighborTimeout = NULL;
      ScheduleTimer(AODVEvent::NEIGHBOR_TIMEOUT,
              NeighborTimeout,
              HELLO_INTERVAL);
      break;
    
    case AODVEvent::ROUTECACHE_TIMEOUT:
      RoutePurge();
#define FREQUENCY 0.5 //sec
      RouteCacheTimeout = NULL;
      ScheduleTimer(AODVEvent::ROUTECACHE_TIMEOUT,
              RouteCacheTimeout,
              FREQUENCY);
#undef FREQUENCY  //to avoid cause trouble.
  
      break;

    case AODVEvent::LOCALREPAIR_TIMEOUT:
      //TODO: to be filled
      // 
      break;
  }  

  //consumed the event
  delete pTimerEvent;
}


void
AODVApplication::ScheduleTimer(Event_t e, AODVEvent*& ev, Time_t t) {
  
  if(ev) {
    ;//do what here? Cancel it or else?
    //TODO:
  }else {
    ev = new AODVEvent(e);
        DEBUG0((cout << "AODV " << this << " scheduling new timer "
          << ev->event << " time " << Simulator::Now()
          << " for " << t << " secs in future" << endl));
    if(useTimerBuckets)
      TimerBucket::Instance()->Schedule(ev, t, this);
    else
      timer.Schedule(ev, t, this);
  }
}

void
AODVApplication::CancelTimer(AODVEvent*&, bool delTimer) {
  //to be filled later.
}

void 
AODVApplication::CancelAllTimers() {
  //to be filled later.
}

// Some utilities .
//  
double  
AODVApplication::PerHopTime(AODVRoutingEntry *pRt) {
  int i, NonZero = 0 ; 
  double dTotalLatency = 0.0;

  if (!pRt)
    return ((double) NODE_TRAVERSAL_TIME );
  for (i=0; i < MAX_HISTORY; i++) {
    if (pRt->DiscLatency[i] > 0.0) {
      NonZero++;
      dTotalLatency += pRt->DiscLatency[i];
    }
  }
  if (NonZero > 0)
    return(dTotalLatency / (double) NonZero);
  else
    return((double) NODE_TRAVERSAL_TIME);
}

//call back from low layer.
//if no route is found at layer , call this one!
//
void 
AODVApplication::RouteResolve(Packet *pPacket) {
  AODVRoutingEntry *pRouteEntry;
  IPAddr_t localIPAddr = l4Proto->localNode->GetIPAddr();

  //TO REVIEW: it seems at IP layer, they did not fill ip header unless
  //           they find route, this is different from what we want
  //           indeed! 
  //The IP layer has been hacked for solving this problem
  //
  IPV4Header *ipHdr = (IPV4Header*)pPacket->PeekPDU(); // at ip layer. 
  assert(l4Proto->localNode->InterfaceCount()!=0);
  IFVec_t Interfaces = l4Proto->localNode->Interfaces(); 
  //we use the first interface only!

  assert(Interfaces.size()!=0);//test purpose
  assert(Interfaces[0] != NULL);
  pRouteEntry = pAODVRouting->Lookup(nil, ipHdr->dst.ip);
  if(pRouteEntry == NULL)
    pRouteEntry = pAODVRouting->Add(0, 0, Interfaces[0], ipHdr->dst.ip);
  assert(pRouteEntry->pInterface !=NULL);
  // If the route is up, forward the packet 
  if(pRouteEntry->Flags == RTF_UP) {
    assert(pRouteEntry->Hops != INFINITY2);
    Forward(pRouteEntry, pPacket, 0 /*NO_DELAY*/);
  }
  //if I am the source of the packet, then do a Route Request.
    else if(ipHdr->src.ip == localIPAddr) {
    rQueue.enque(pPacket);
    SendRequest(pRouteEntry->DstIPAddr);
  }
   // A local repair is in progress. Buffer the packet. 
  else if (pRouteEntry->Flags == RTF_IN_REPAIR) {
    rQueue.enque(pPacket);
  }
     // I am trying to forward a packet for someone else to which
   // I don't have a route. 
  else {
    struct AODVErrorHdr *pErrToSend = new struct AODVErrorHdr;

    memset(pErrToSend, 0, sizeof(struct AODVErrorHdr));
    // 
    // For now, drop the packet and send error upstream.
    // Now the route errors are broadcast to upstream
    //neighbors 

    assert (pRouteEntry->Flags == RTF_DOWN);
    pErrToSend->DestCount = 0;
    pErrToSend->UnreachableDst[pErrToSend->DestCount] = pRouteEntry->DstIPAddr;
    pErrToSend->UnreachableDstSeqNo[pErrToSend->DestCount] = pRouteEntry->SeqNo;
    pErrToSend->DestCount += 1;
  
    SendError(pErrToSend, false);
    
    DEBUG0((cout <<"PACKET DROP because of NO ROUTE" << endl));
    delete pPacket;
   }
        
}

void 
AODVApplication::BroadcastIDPurge() {
  double now = Simulator::Now();

  for(BroadcastIDList_t::iterator i=BroadcastIdList.begin(); 
          i!=BroadcastIdList.end() ;i++) {
    if((*i)->expire <= now) {
      BroadcastID *pBid = *i;
      BroadcastIdList.erase(i);
      i--; //we already deleted the items.
      delete pBid; //delete from memory
    }
  }
}

bool
AODVApplication::BroadcastIDLookup(IPAddr_t id, Long_t bid) {
   // Search the list for a match of source and bid
  for(BroadcastIDList_t::iterator i=BroadcastIdList.begin();
           i!=BroadcastIdList.end(); i++) {
    if (((*i)->src == id) && ((*i)->id == bid))
           return true;
  }
    return false;
}

void
AODVApplication::BroadcastIDInsert(IPAddr_t id, Long_t bid) {
  //   
  double now = Simulator::Now();
  BroadcastID *b = new BroadcastID(id, bid);
  assert(b);
  b->expire = now + BCAST_ID_SAVE;
  BroadcastIdList.push_front(b); //insert the head of the Broadcastlist
   
}

void
AODVApplication::SendHello() {
  //
  struct AODVReplyHdr *pHello = new struct AODVReplyHdr;
  IPAddr_t localIPAddr = l4Proto->localNode->GetIPAddr();

  memset((void *)pHello, 0, pHello->size());
  pHello->Type = AODV_HELLO;
  pHello->HopCount = 1;
  pHello->Dst = localIPAddr;//aodv draft says this.
  pHello->DstSeqNo = SeqNo;
  pHello->LifeTime = (1 + ALLOWED_HELLO_LOSS) * HELLO_INTERVAL;

  assert(l4Proto);
  Count_t origTTL = l4Proto->TTl(); 
  l4Proto->TTL(1); //for this packet.
  l4Proto->SendTo((char *)pHello, pHello->size(), IPAddrBroadcast, AODV_PORT);
  l4Proto->TTL(origTTL); //coming back. 
}

void
AODVApplication::NeighborPurge() {
   double now = Simulator::Now();

   SeqNo += 2;     // Set of neighbors changed
   assert ((SeqNo%2) == 0);
    for(AODVNeighborList_t::iterator i=NeighborList.begin();
          i!=NeighborList.end();i++) {
    if((*i)->Expire <= now) {
      AODVNeighbor *pNb = *i;
      NeighborList.erase(i);
      i--;
      delete pNb;
    }
  }
}

void
AODVApplication::NeighborDelete(IPAddr_t id) {
   SeqNo += 2;     // Set of neighbors changed
   assert ((SeqNo%2) == 0);
    for(AODVNeighborList_t::iterator i=NeighborList.begin();
          i!=NeighborList.end();i++) {
    if((*i)->Address == id) {
      AODVNeighbor *pNb = *i;
      NeighborList.erase(i); 
      i--;  //resume the iterator so that we'd not miss anyone.
      delete pNb;
    }
  }
}

void
AODVApplication::NeighborInsert(IPAddr_t id) {
  //
  double now = Simulator::Now();
  AODVNeighbor *pNb = new AODVNeighbor(id);

  assert(pNb);
  pNb->Expire = now + (1.5 * ALLOWED_HELLO_LOSS * HELLO_INTERVAL);
  NeighborList.push_front(pNb);
  SeqNo +=2; //set of neighbors changed.
  assert((SeqNo%2)==0);
}

AODVNeighbor *
AODVApplication::NeighborLookup(IPAddr_t id) {
  for(AODVNeighborList_t::iterator i= NeighborList.begin();
        i!=NeighborList.end(); i++) {
    if((*i)->Address == id)
      return (*i);
  }
  return nil;
}


void
AODVApplication::RoutePurge() {
  AODVRoutingEntry *pRouteEntry;
  double now = Simulator::Now();
  Packet *pPacket; 
  //double delay = 0.0;
  
  for(AODVFIB_t::iterator i=pAODVRouting->fib.begin(); 
          i!=pAODVRouting->fib.end(); i++) {
      // for each rt entry
     pRouteEntry = *i; //to avoid modfiy much source code.
     if ((pRouteEntry->Flags == RTF_UP) && (pRouteEntry->Expire < now)) {
    // if a valid route has expired, purge all packets from
    // send buffer and invalidate the route.
         assert(pRouteEntry->Hops != INFINITY2);
       while((pPacket = rQueue.deque(pRouteEntry->DstIPAddr))) {
        DEBUG0((cout <<"PACKET DROP because of NO ROUTE" <<endl));
        delete pPacket;
       }
       pRouteEntry->SeqNo++;
       assert (pRouteEntry->SeqNo%2);
       RouteDown(pRouteEntry);
    }
    else if (pRouteEntry->Flags == RTF_UP) {
        // If the route is not expired,
       assert(pRouteEntry->Hops != INFINITY2);
       
       while((pPacket = rQueue.deque(pRouteEntry->DstIPAddr))) {
             Forward (pRouteEntry, pPacket, 0 /*delay*/);
             //delay += ARP_DELAY;
       }
       
    }
    else if (rQueue.find(pRouteEntry->DstIPAddr))
      SendRequest(pRouteEntry->DstIPAddr); 
  }
}

void
AODVApplication::RouteUpdate(AODVRoutingEntry *RouteEntry, Seq_t 
        seqnum, Word_t metric , IPAddr_t nexthop, 
        double expire_time) {
     RouteEntry->SeqNo = seqnum; 
     RouteEntry->Hops = metric; 
     RouteEntry->Flags = RTF_UP;
 
     RouteEntry->Address = nexthop;
     RouteEntry->Expire = expire_time;
}

void
AODVApplication::RouteDown(AODVRoutingEntry *RouteEntry) {
    if(RouteEntry->Flags == RTF_DOWN)
    return; 
    RouteEntry->LastHopCount = RouteEntry->Hops;
    RouteEntry->Hops = INFINITY2;
    RouteEntry->Flags = RTF_DOWN;
  
    RouteEntry->Address = 0;
    RouteEntry->Expire = 0;
}

void 
AODVApplication::Forward(AODVRoutingEntry *pRt, 
        Packet *pPacket, double delay) {
    //Don't use "delay" here now.
  Time_t now = Simulator::Now();
  IPV4Header *ipHdr = (IPV4Header*)pPacket->PeekPDU(); // at ip layer. 

  if(ipHdr->ttl == 0) { 
    DEBUG0(( cout <<"PACKET DROP because of TTL" <<endl));
    delete pPacket;
    return; 
  }
  if (pRt) {
    assert(pRt->Flags == RTF_UP);
    pRt->Expire = now + ACTIVE_ROUTE_TIMEOUT;
  }
  else { // if it is a broadcast packet
    assert(ipHdr->dst.ip == (IPAddr_t) IPAddrBroadcast);
  }
  if (ipHdr->dst.ip == (IPAddr_t) IPAddrBroadcast) {
    assert(l4Proto->localNode->InterfaceCount()!=0);
    IFVec_t Interfaces = l4Proto->localNode->Interfaces(); 
        //we only use the first interface instead!
    //
    //TODO: put on every interface
    //Interfaces[0]->Send(pPacket, 0xffffffff, 0x0800);
    Interfaces[0]->Send(pPacket, 0x0800);
  }
    else { // Not a broadcast packet 
    DEBUG0((cout <<"PACKET forward to IP:<"<< (string)IPAddr(pRt->Address)
          <<"> "  << endl ));
    pRt->pInterface->Send(pPacket, (IPAddr_t)(pRt->Address), 0x0800);
   }

}

void 
AODVApplication::SendRequest(IPAddr_t dst) {
  // 
  double now = Simulator::Now();
  IPAddr_t localIPAddr = l4Proto->localNode->GetIPAddr();
  AODVRoutingEntry *pRouteEntry = pAODVRouting->Lookup(nil, dst);
  
  assert(pRouteEntry);

  //Limit the sending rate of Route Request.
  if(pRouteEntry->Flags == RTF_UP ) {
    assert(pRouteEntry->Hops != INFINITY2);
    return;
  }
  if(pRouteEntry->ReqTimeout > now)
    return;
  //ReqCount is the no of times we did network-wide broadcast
  //RREQ_RETRIES is the maximum number we will allow before
  //gong to a long timeout.
  if(pRouteEntry->ReqCount > RREQ_RETRIES) {
    pRouteEntry->ReqTimeout = now + MAX_RREQ_TIMEOUT;
    pRouteEntry->ReqCount = 0;
    
    Packet *pPacket;
    while ((pPacket = rQueue.deque(pRouteEntry->DstIPAddr))) {
      DEBUG0((cout << "PACKET DROP because of NO ROUTE" << endl));
      delete pPacket;
    }
    return;
  }

  
  //Determine the TTL to be used this time.
  pRouteEntry->RequestLastTTL = max(pRouteEntry->RequestLastTTL,
                       pRouteEntry->LastHopCount);
  Count_t nTTL;
  if(pRouteEntry->RequestLastTTL == 0 ) {
    //first time query broadcast
    nTTL = TTL_START;
  } else {
    //Expanding ring search
    if(pRouteEntry->RequestLastTTL < TTL_THRESHOLD)
      nTTL = pRouteEntry->RequestLastTTL + TTL_INCREMENT;
    else {
      //network-wide broadcast
      nTTL = NETWORK_DIAMETER;
      pRouteEntry->ReqCount +=1;
    }
  }
  Count_t origTTL = l4Proto->TTl(); 
  l4Proto->TTL(nTTL); //use for this packet.
    //Remember the TTL used for the next time
  pRouteEntry->RequestLastTTL = nTTL;
  // PerHopTime is the round trip time per hop for route requests.
  //  The factor 2.0 is just to be safe.
  // also note that we are making timeouts to be larger if
  // we have done network wide broadcast before.
  pRouteEntry->ReqTimeout =  2.0 * (double) nTTL * 
      PerHopTime(pRouteEntry);
  if(pRouteEntry->ReqCount > 0)
    pRouteEntry->ReqTimeout *=pRouteEntry->ReqCount;
  pRouteEntry->ReqTimeout += now;
  // Don't let the timeout be too large
  if(pRouteEntry->ReqTimeout > now + MAX_RREQ_TIMEOUT)
    pRouteEntry->ReqTimeout = now + MAX_RREQ_TIMEOUT;
  pRouteEntry->Expire = 0;
    
  struct AODVRequestHdr *pRequest = new struct AODVRequestHdr;
  memset((void *)pRequest, 0, pRequest->size());
  pRequest->Type = AODV_REQUEST;
  pRequest->HopCount = 1;
  pRequest->BcastId = BroadcastId++;
  pRequest->Dst = dst; //dst a pass-in parameter.
  pRequest->DstSeqNo = (pRouteEntry? pRouteEntry->SeqNo : 0);
  pRequest->Src = localIPAddr;
  SeqNo +=2;
  assert((SeqNo%2)==0);
  pRequest->SrcSeqNo = SeqNo;
  pRequest->TimeStamp = now;
  l4Proto->SendTo((char *)pRequest, pRequest->size(), IPAddrBroadcast, 
          AODV_PORT);
  l4Proto->TTL(origTTL); //coming back. 
      
}

void 
AODVApplication::SendReply(IPAddr_t ipdst, Long_t hop_count,
               IPAddr_t rpdst, Seq_t rpseq,  Long_t lifetime,
         double timestamp) {
  
  struct AODVReplyHdr *pReply = new struct AODVReplyHdr; //"struct" can be omitted.

  IPAddr_t localIPAddr = l4Proto->localNode->GetIPAddr();

  memset((void *)pReply, 0, pReply->size());
  pReply->Type = AODV_REPLY;
  pReply->HopCount = (Byte)hop_count;
  pReply->Dst = rpdst;
  pReply->DstSeqNo = rpseq;
  pReply->Src = localIPAddr;
  pReply->LifeTime = lifetime;
  pReply->TimeStamp = timestamp;
  
  assert(l4Proto);
  Count_t origTTL = l4Proto->TTl(); 
  l4Proto->TTL(NETWORK_DIAMETER); //for this packet actually.
  l4Proto->SendTo((char *)pReply, pReply->size(), ipdst, AODV_PORT);
  l4Proto->TTL(origTTL); //coming back. 
}

void 
AODVApplication::SendError(struct AODVErrorHdr *pErrHdr, bool jitter) {
  //
  pErrHdr->Type = AODV_ERROR;
  Count_t origTTL = l4Proto->TTl();
  l4Proto->TTL(1); //for this packet only;
  l4Proto->SendTo((char *)pErrHdr, pErrHdr->size(),IPAddrBroadcast, 
          AODV_PORT);
  l4Proto->TTL(origTTL); //coming back.

}

  // Packet RX Routines

void 
AODVApplication::RecvRequest(Packet *pPacket) {
  
  Data* pData = (Data*)pPacket->PeekPDU();//PopPDU();      // Get the AODV header
  struct AODVRequestHdr *pReqHdr = (struct AODVRequestHdr *)(pData->data);
  AODVRoutingEntry *pRouteEntry = NULL;
  Time_t now = Simulator::Now();
                                                   //0 is itself
                                                   //-1 is UDP/TCP 
  IPV4Header *ipHdr = (IPV4Header*)pPacket->PeekPDU(-2); //-3 is ip header
  //Not very good because of multi-interface or ip address.
  IPAddr_t localIPAddr = l4Proto->localNode->GetIPAddr();
  assert(l4Proto->localNode->InterfaceCount()!=0);
    IFVec_t Interfaces = l4Proto->localNode->Interfaces(); 
                       //we will just use the first interface instead!

  // Drop the packet if (1). I am the source (2) I recently heard this reuqest.
  //
  if(pReqHdr->Src == localIPAddr) {
      DEBUG0((cout <<"Node:<" << (string)IPAddr(localIPAddr) <<"> " 
        <<"got my own Request " <<endl));
    delete pPacket; //free the packet.
    return;
  }
  if(BroadcastIDLookup(pReqHdr->Src, pReqHdr->BcastId)) {
      DEBUG0((cout <<"AODV " << this <<" discard Request " <<endl));
    delete pPacket;
    return;
  }

  // cache the broadcast id
  BroadcastIDInsert(pReqHdr->Src, pReqHdr->BcastId);

  // we are either going to forward the REQUESRT or generate
  // a REPLY. Before we do anything, maks sure the REVERSE route
  // is in the route table 
  // 
  AODVRoutingEntry *pRouteEntry0; //for reverse route
  pRouteEntry0 = pAODVRouting->Lookup(nil, pReqHdr->Src);
  
     //not in route table, create an entry for the reverse.
  if(pRouteEntry0 == 0){
    assert(Interfaces.size()!=0);//test purpose 
    assert(Interfaces[0]!=NULL);
  
    pRouteEntry0 = pAODVRouting->Add(0, 0, Interfaces[0],pReqHdr->Src);
  }
  pRouteEntry0->Expire = max(pRouteEntry0->Expire, (now + REV_ROUTE_LIFE));
  if( (pReqHdr->SrcSeqNo > pRouteEntry0->SeqNo) ||
    ((pReqHdr->SrcSeqNo == pRouteEntry0->SeqNo) &&
     (pReqHdr->HopCount < pRouteEntry0->Hops)) ) {
      RouteUpdate(pRouteEntry0, pReqHdr->SrcSeqNo, pReqHdr->HopCount,
            ipHdr->src.ip, pRouteEntry0->Expire);
    if(pRouteEntry0->ReqTimeout > 0.0) {
      pRouteEntry0->ReqCount = 0;
      pRouteEntry0->ReqTimeout = 0.0;
      pRouteEntry0->RequestLastTTL = pReqHdr->HopCount;
      pRouteEntry0->Expire = now + ACTIVE_ROUTE_TIMEOUT;
    }
    // See if any buffered packet can benefit from the reverse route.
    //
    assert(pRouteEntry0->Flags == RTF_UP);
    Packet *bufferedPacket;
    while ((bufferedPacket = rQueue.deque(pRouteEntry0->DstIPAddr))) {
      assert(pRouteEntry0->Hops !=INFINITY2);
      Forward(pRouteEntry0, bufferedPacket, 0 /*NO_DELAY*/);
    }
  }
  // End of putting reverse route in route table.
  
  // See if we can send a route reply
  // Here We ARE FOOL, since we have already found route, but
  // we still pass the data down through 5 layers structure!
  // TODO: review it.
  //
  pRouteEntry = pAODVRouting->Lookup(nil, pReqHdr->Dst);
  if(pReqHdr->Dst == localIPAddr) {
    SeqNo = max(SeqNo, pReqHdr->DstSeqNo) + 1;
    if(SeqNo%2) SeqNo++;
    SendReply(pReqHdr->Src,         //IP Destination
          1,                    //Hop Count       
          localIPAddr,          //Dest IP Address
          SeqNo,                //Dest Seq No
          MY_ROUTE_TIMEOUT,     //Lifetime
          pReqHdr->TimeStamp);  //Timesbtamp;
    delete pPacket; //free original packet.
  } 
  else if(pRouteEntry && (pRouteEntry->Hops !=INFINITY2) &&
        (pRouteEntry->SeqNo >= pReqHdr->DstSeqNo) ) {
    assert(pReqHdr->Dst == pRouteEntry->DstIPAddr);
    SendReply(pReqHdr->Src, pRouteEntry->Hops + 1, pReqHdr->Dst,
          pRouteEntry->SeqNo, (Long_t) (pRouteEntry->Expire - now),
          pReqHdr->TimeStamp);
      //insert nexthops to RREQ source and dst in the precursorlist
    //of dst and source respectively

    pRouteEntry->PrecursorInsert(pRouteEntry0->Address);
    pRouteEntry0->PrecursorInsert(pRouteEntry->Address);

#ifdef RREQ_GRAT_RREP
    SendReply(pReqHdr->Dst, pReqHdr->HopCount, pReqHdr->Src,
          pReqHdr->SrcSeqNo, (Long_t)(pRouteEntry->Expire - now),
          pReqHdr->TimeStamp);
#endif
    delete pPacket; //free original packet.
  }
  else { // can not reply, so forward the Route Request.
    ipHdr->src.ip = localIPAddr;
    ipHdr->dst.ip = IPAddrBroadcast;
    pReqHdr->HopCount +=1;
    PDU *pUDPHdr = pPacket->PeekPDU(-1);
    pPacket->PushPDU(pUDPHdr);
    pPacket->PushPDU(ipHdr);
    Forward((AODVRoutingEntry *)0, pPacket, 0 /*no delay */);
  }
}

void 
AODVApplication::RecvHello(Packet *pPacket) {
  // 
  Data* pData = (Data*)pPacket->PopPDU();      // Get the AODV header
  struct AODVReplyHdr *pReplyHdr = (struct AODVReplyHdr *)(pData->data);
  AODVNeighbor *pNeighbor;
  double now = Simulator::Now();

  pNeighbor = NeighborLookup(pReplyHdr->Dst);
  if(pNeighbor == 0)
    NeighborInsert(pReplyHdr->Dst);
  else
    pNeighbor->Expire = now + (1.5 * ALLOWED_HELLO_LOSS * HELLO_INTERVAL);

  delete pPacket; //free the packet.
  
}

void 
AODVApplication::RecvReply(Packet *pPacket) {
  //
  Data* pData = (Data*)pPacket->PeekPDU();//Get the AODV header without removal
                      //because we may forward it.
  struct AODVReplyHdr *pReplyHdr = (struct AODVReplyHdr *)(pData->data);
  AODVRoutingEntry *pRouteEntry = NULL;
  char suppressReply = 0;
  double now = Simulator::Now();
  IPAddr_t localIPAddr = l4Proto->localNode->GetIPAddr();
  assert(l4Proto->localNode->InterfaceCount()!=0);
  IFVec_t Interfaces = l4Proto->localNode->Interfaces(); 
                       //we will just use the first interface instead!
                                                   //0 is itself
                                                   //-1 is UDP/TCP 
  IPV4Header *ipHdr = (IPV4Header*)pPacket->PeekPDU(-2); //-2 is ip header
  //Original COMMENTS: note that rp_dst is the dest of the data packets
  //, not the dest of the reply, which is the src of the data packets.
  //
  pRouteEntry = pAODVRouting->Lookup(nil, pReplyHdr->Dst);
  if(pRouteEntry == 0){
    assert(Interfaces.size()!=0);//test purpose by
    assert(Interfaces[0]!=NULL);

    pRouteEntry = pAODVRouting->Add(0,0, Interfaces[0], pReplyHdr->Dst);
  }
  if( (pRouteEntry->SeqNo < pReplyHdr->DstSeqNo) ||
    ((pRouteEntry->SeqNo == pReplyHdr->DstSeqNo) &&
     (pRouteEntry->Hops > pReplyHdr->HopCount)) ) { //shorter or better route.
    //update the route entry;
    RouteUpdate(pRouteEntry, pReplyHdr->DstSeqNo, pReplyHdr->HopCount,
          pReplyHdr->Src, now + pReplyHdr->LifeTime);
    //reset the soft state
    pRouteEntry->ReqCount = 0;
    pRouteEntry->ReqTimeout = 0.0;
    pRouteEntry->RequestLastTTL = pReplyHdr->HopCount;
    
    if(ipHdr->dst.ip == localIPAddr) {
      pRouteEntry->DiscLatency[pRouteEntry->HistoryIndex] = 
          (now - pReplyHdr->TimeStamp) /(double) pReplyHdr->HopCount;
      //increment the index for next time
      pRouteEntry->HistoryIndex = (pRouteEntry->HistoryIndex + 1)% MAX_HISTORY;
    }

    // Send all packets in sendbuffer destinated for this destination.
    Packet *bufPacket;
    while((bufPacket = rQueue.deque(pRouteEntry->DstIPAddr))) {
      if(pRouteEntry->Hops !=INFINITY2) {
        assert(pRouteEntry->Flags == RTF_UP);
        cout << "#####NORMAL DATA FORWARDING#####" <<endl;
        Forward(pRouteEntry, bufPacket, 0 /*delay*/);
        //delay = delay + ARP_DELAY;
      }
    }
  }
  else 
    suppressReply = 1;

  // if the reply is for me , drop it
  if(((ipHdr->dst.ip)==localIPAddr)||suppressReply)
    delete pPacket;
  else {
    //find the route entry
    AODVRoutingEntry *pRouteEntry0 = pAODVRouting->Lookup(nil, ipHdr->dst.ip);

    //Because "top" in packet has been modified on the way up,
    //we could not use "Forward" unless the "top" can be modified
    //.

    if(pRouteEntry0 && (pRouteEntry0->Hops != INFINITY2)) {
      assert(pRouteEntry0->Flags == RTF_UP);
      pReplyHdr->HopCount +=1;
      pReplyHdr->Src = localIPAddr;  //Not sure if this complys to RFC draft.
      //we reuse the packet now.
      PDU *pUDPHdr = pPacket->PeekPDU(-1);
      pPacket->PushPDU(pUDPHdr); //reuse the packet here.
      pPacket->PushPDU(ipHdr);
      Forward(pRouteEntry0, pPacket, 0 /*NO_DELAY*/);

      pRouteEntry->PrecursorInsert(pRouteEntry0->Address);
    } else {
      //Don't know how to forward, drop
      DEBUG0((cout << "REPLY DROP because of NO ROUTE" <<endl));
      //free original packet here.
      delete pPacket;
    }//if(pRouteEntry0..
  }//if(ipHdr->dst.ip..

}

void
AODVApplication::RecvError(Packet *pPacket) {
  // 
  Data* pData = (Data*)pPacket->PopPDU();  // Get the AODV header
  struct AODVErrorHdr *pErrHdr = (struct AODVErrorHdr *)(pData->data);
  struct AODVErrorHdr *pErrToSend = new struct AODVErrorHdr;
  AODVRoutingEntry *pRouteEntry = NULL;
  Byte i;
                                                   //-1 is itself
                                                   //-2 is UDP/TCP 
  IPV4Header *ipHdr = (IPV4Header*)pPacket->PeekPDU(-3); //-3 is ip header
  memset(pErrToSend, 0, sizeof(struct AODVErrorHdr));

  for (i=0; i<pErrHdr->DestCount; i++) {
    //for each unreachable dest.
    pRouteEntry = pAODVRouting->Lookup(nil, pErrHdr->UnreachableDst[i]);
    if(pRouteEntry && (pRouteEntry->Hops!=INFINITY2) &&
      (pRouteEntry->Address == ipHdr->src.ip) &&
      (pRouteEntry->SeqNo <= pErrHdr->UnreachableDstSeqNo[i]) ) {
      assert(pRouteEntry->Flags = RTF_UP);
      assert((pRouteEntry->SeqNo%2)==0);
      pRouteEntry->SeqNo = pErrHdr->UnreachableDstSeqNo[i];
      RouteDown(pRouteEntry);
        
      //Drop some packets.
      //TODO: review!!
      
      //if Precursorlist non-empty add to RERR and delete precursorlist
      if(!pRouteEntry->PrecursorEmpty()) {
        pErrToSend->UnreachableDst[pErrToSend->DestCount] = pRouteEntry->DstIPAddr;
        pErrToSend->UnreachableDstSeqNo[pErrToSend->DestCount] = 
            pRouteEntry->SeqNo;
        pErrToSend->DestCount +=1;
        pRouteEntry->PrecursorDelete();
      }
    }
  }
  
  if(pErrToSend->DestCount >0){
    SendError(pErrToSend);
  }
  else //pErrToSend not used, therefore free it.
    delete pErrToSend; 

  delete pPacket;  
}
