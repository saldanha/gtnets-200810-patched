//
// Sample test program to instantiate a BGP router
// Sunitha Beeram, Georgia Tech, Fall 2005
//

#include <iostream>
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "bgp.h"
#include "ratetimeparse.h"
#include "linkp2p.h"

using namespace std;

int main(int argc,char ** argv)
{
    Simulator Sim;
    char command[]="network 10.0.3.0/24";
    Node  *node[5];
    BGP *bgpRouter[5];
    Linkp2p lk(Rate("500Kb"), Time("20ms"));
	int StopTime = 100;
	char config_file[]="./conf/bgpd1.conf";
	int i;

    Node::DefaultShape(Node::CIRCLE);
    
    Trace* tr= Trace::Instance();
    tr->Open("mrai.txt");
    
    Uniform startRng(0,0.1);
    
    for (i=0; i < 5; ++i)
    {
        node[i]= new Node();
        bgpRouter[i]= new BGP(0);
        bgpRouter[i]->AttachNode(node[i]);
		bgpRouter[i]->StartAt(0.1);
		bgpRouter[i]->StopAt(StopTime);
		bgpRouter[i]->config_file(config_file);
		config_file[11]+=1;
    }
    
	/*interconnect the routers*/
  
	node[0]->AddDuplexLink(node[1],lk);
	node[2]->AddDuplexLink(node[1],lk);
	node[3]->AddDuplexLink(node[1],lk);
	node[4]->AddDuplexLink(node[1],lk);
 
    BGPEvent *ev = new BGPEvent(BGPEvent::COMMAND);
    ev->argv = (char*)malloc(sizeof(char)*strlen(command)+1);
    strcpy(ev->argv,command);
    bgpRouter[3]->timer->Schedule(20,ev);

    BGPEvent *ev1 = new BGPEvent(BGPEvent::COMMAND);
    ev1->argv = (char*)malloc(sizeof(char)*strlen(command)+1);
    strcpy(ev1->argv,command);
    bgpRouter[4]->timer->Schedule(21,ev1);

    Sim.StopAt(StopTime);
    cout << "Starting Simulation" << endl;
    Sim.Run();
    cout << "Simulation complete" << endl;
}

