\chapter{Tracing Packets}\label{Chap:Tracing}
One way to observe the results of a simulated network is to
create a {\em trace file} that indicates the state of each
packet as it is forwarded through the network.  It its simplest
form, a trace file will contain an entry for every single
packet transmitted and received on every single link in 
the network.  For small simulations, this approach if
acceptable.  However, for larger simulations such a method
can quickly become unwieldy and result in giga--bytes of traced
information.

\GTNS\ allows very fine--grained control over what information
is logged to a trace file.  packet tracing can be enabled or
disabled by protocol layer, individual protocol object, or individual
nodes within the topology.  Further, within a given protocol,
individual data items can be enabled or disabled as needed.  For
example, one can specify that only the {\em TCP} header should
be traced, and within the {\em TCP} header only the sequence
number, acknowledgment number, and flags are of interest.
Using this selective enabling and disabling, a manageable size
trace file can be created, even for large simulations.

The trace file is globally enabled or disabled by using the
{\tt Trace} object described below.  Using the {\tt Open}
method for the global trace file object, the tracing is
initiated and the file name is specified.  The trace file
should later be closed using the {\tt Close} method.

The enabling or disabling of tracing at various points 
in the \GTNS\ simulation is controlled by the {\tt SetTrace}
method, which is defined for nodes and all protocol objects.
The argument to the {\tt SetTrace} method is one of three
states:

\begin{enumerate}
\item Trace::DISABLED.  This indicates that the trace entry
should {\em NOT} be created.
\item Trace::ENABLED.  This indicates that the trace entry
{\em should} be created.
\item Trace::DEFAULT.  This indicates that the object being
queried has no opinion, and the decision should be delegated 
as described below.
\end{enumerate}

A tracing decision is made whenever a packet is received or 
transmitted at every protocol layer.  Pseudo code for deciding
if a protocol header is to be traced is given below:

\begin{verbatim}
bool CheckTrace(Protocol* proto, Packet* pkt)
  if (NOT {\em trace file open}) return false; // No trace file exists
  Node* n = proto->GetNode(); // Node object where protocol resides
  if (n->TraceStatus() == ENABLED) return true;
  if (n->TraceStatus() == DISABLED) return false;
  // Node status is default, keep checking
  // Next check for global protocol layer enabled/disabled
  if (GlobalTraceStatus(proto->Layer()) == ENABLED) return true;
  if (GlobalTraceStatus(proto->Layer()) == DISABLED) return false;
  // Not set globally, check individual object
  if (proto->TraceStatus() == ENABLED) return true;
  if (proto->TraceStatus() == DISABLED) return false;
  return false;
end CheckTrace;
\end{verbatim}

From the above pseudo code, it is clear that all tracing can
be enabled or disabled on individual nodes.  It may be the
case that the only interest is for packets at leaf nodes.  In
that case, setting the trace status to {\em DISABLED} for 
all router nodes will disable tracing at the routers.

A sample trace file excerpt is shown below.
All trace file lines start with the simulation time and the node
identifier for the node processing the packet.  All further information
is variable, depending on the trace enabling or disabling for the
individual protocol layers and protocol objects.  In this example,
the layer 4 {\em TCP} protocol object at node 0 is generating a packet.
The fields are:
\begin{itemize}
\item[] {\em source port} (10000),
\item[] {\em destination port} (80),
\item[] {\em sequence number} (0),
\item[] {\em acknowledgement number} (0),
\item[] {\em flags} (SYN),
\item[] {\em Flow ID} (0), and
\item[] {\em Data Length} (0).
\end{itemize}

Note that {\em TCP} headers do not actually contain a flow id
or a data length field.  These are both added in the simulation
environment as a convenience.

Next is the entry for the layer 3 protocol (IPV4).  The fields are:
\begin{itemize}
\item[] {\em TTL} (64)
\item[] {\em Protocol Number} (6)
\item[] {\em Source} \IPA\ (192.168.0.1)
\item[] {\em Destination} \IPA\ (192.168.1.1)
\item[] {\em Packet Unique Identifier} (1)
\end{itemize}

The next few lines show the packet being routed through the network
to the destination at node id 4.  For nodes 2 and 3 only the IP
header is traced, since the packet was processed by the IP layer
only at these nodes.  For this example, all layer 2 tracing is disabled.
The packet arrives at the destination node (4) at time 1.88897,
and is processed by the {\em TCP} protocol at that node.  This
protocol creates a {\tt SYN|ACK} packet and forwards it back to 
node 0.

This example is typical, but the individual data items being traced
at each protocol layer is fully configurable by the user, so the
actual trace files may be different.

\begin{figure*}
\begin{verbatim}
1.77891 N0 L4-TCP 10000 80 0 0 SYN 0 0 L3-4 64 6 192.168.0.1 192.168.1.1 1
1.78391 N2 L3-4 63 6 192.168.0.1 192.168.1.1 1
1.88396 N3 L3-4 62 6 192.168.0.1 192.168.1.1 1
1.88897 N4 L3-4 61 6 192.168.0.1 192.168.1.1 1 L4-TCP 10000 80 0 0 SYN 0 0 
1.88897 N4 L4-TCP 80 10000 0 0 SYN|ACK 0 0 L3-4 64 6 192.168.1.1 192.168.0.1 2
1.89398 N3 L3-4 63 6 192.168.1.1 192.168.0.1 2
1.99403 N2 L3-4 62 6 192.168.1.1 192.168.0.1 2
1.99904 N0 L3-4 61 6 192.168.1.1 192.168.0.1 2 L4-TCP 80 10000 0 0 SYN|ACK 0 0
1.99904 N0 L4-TCP 10000 80 0 0 ACK 0 0 L3-4 64 6 192.168.0.1 192.168.1.1 4
1.99904 N0 L4-TCP 10000 80 0 0 0 0 512 L3-4 64 6 192.168.0.1 192.168.1.1 5
2.00404 N2 L3-4 63 6 192.168.0.1 192.168.1.1 4
2.00409 N2 L3-4 63 6 192.168.0.1 192.168.1.1 5
2.10409 N3 L3-4 62 6 192.168.0.1 192.168.1.1 4
2.10456 N3 L3-4 62 6 192.168.0.1 192.168.1.1 5
2.10910 N4 L3-4 61 6 192.168.0.1 192.168.1.1 4 L4-TCP 10000 80 0 0 ACK 0 0
2.10961 N4 L3-4 61 6 192.168.0.1 192.168.1.1 5 L4-TCP 10000 80 0 0 0 0 512
2.10961 N4 L4-TCP 80 10000 0 512 ACK 0 0 L3-4 64 6 192.168.1.1 192.168.0.1 7
\end{verbatim}
\caption{\label{Fig:LogFile}\GTNS\ Log File Excerpt}
\end{figure*}

\section{Trace File Object}
\input{Trace}

