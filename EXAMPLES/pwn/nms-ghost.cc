// Create the Topology for the NMS Scalability Demonstrations per David Nicol
// George F. Riley.  Georgia Tech, Fall 2002

#include <ctype.h>
#include <iostream>

//#define DEBUG_MASK 0x06
#include "debug.h"
#include "simulator.h"
#include "distributed-simulator.h"
#include "scheduler.h"
#include "application-tcpsend.h"
#include "application-tcpserver.h"
#include "duplexlink.h"
#include "queue.h"
#include "droptail.h"
#include "ratetimeparse.h"
#include "ipv4.h"
#include "tcp-tahoe.h"
#include "node.h"
#include "routing-nixvector.h"
#include "routing-static.h"
#include "ethernet.h"
#include "mask.h"
#include "rng.h"
#include "rtt-estimator.h"
#include "globalstats.h"
#include "udp.h"
#include "application-onoff.h"
#include "average-min-max.h"
#include "link-rti.h"
#include "timerbuckets.h"

// Define a class for each network
// IP Addresses are assigned as follows:
// For each campus network, use "0.0.0.0" plus network number+1 * "0.1.0.0"
// Remote links to "next" campus is x.x+1.1.1
// Remote link to "prior" campus is x.(x+1-2).1.2
// Servers are x.x.0.[3..6] (four servers)
// Leaf networks (ethernets) are x.x.(l+2),[1..42]

using namespace std;

// Progress hook
static void Progress(Time_t now)
{
  double phSec = 0;
  if (Simulator::instance->RunTime() > 0)
    {
      phSec = Stats::pktsTransmitted / Simulator::instance->RunTime();
    }
  cout << "Prog " << Simulator::Now() 
       << " mem " << Simulator::instance->ReportMemoryUsageMB() << "MB"
       << " evSz " << Scheduler::Instance()->evlSize
    //<< " pkts " << Stats::pktsAllocated - Stats::pktsDeleted
       << " evEnq " << Scheduler::Instance()->totevs
    //   << " evCan " << Simulator::instance->totevc
       << " ph/Sec " << phSec
       << " aSt " << TCPSend::totalStarted
       << " aEnd " << TCPSend::totalEnded
       << endl;
}

Count_t  netCount = 20;
Count_t  totalNets; // netCount times number of federates
Count_t  baseNet;   // Starting network id (for distributed version)
bool     singleNet = false;
bool     singleflow = false;
bool     scaledDown = false;
bool     nonSingleGateway = false; // True if not single gw LANs
AverageMinMax* averageMinMax = nil; // Statistics
bool     distributed = false;
Count_t  totalFlows = 0;
IPAddr_t netMult = IPAddr("0.1.0.0"); // Convert network number to baseip
class CampusLan;
typedef vector<CampusLan*> CLanVec_t;

class NMSNetwork {
public:
  NMSNetwork(IPAddr_t);
  Node* Gateway(); // Return the gateway
  Node* GetServer(Count_t);                 // Get specified server
  Node* GetNode(Count_t, Count_t, Count_t); // Get node from campus/enet/node
public:
  NodeVec_t net0;
  NodeVec_t net1;
  NodeVec_t net2;
  NodeVec_t net3;
  NodeVec_t servers;
  CLanVec_t campuses;
};

typedef vector<NMSNetwork*> NetVec_t;
typedef vector<TCPSend*>    TCPSendVec_t;
typedef vector<Interface*>  IFVec_t;

TCPSendVec_t tcpSends;
IFVec_t      interfaces;

class CampusLan {
public:
  CampusLan(Node*, IPAddr_t);      // Specifies gateway and IPAddress base
  Node* GetNode(Count_t, Count_t); // Get specified node from specified enet
  Ethernet* GetENet(Count_t);      // Get specified ethernet network
public:
  Node* lanRouter;         // The lan router object
  vector<Ethernet*> enets; // The ethernet objects
};

// NMSNetwork methods
NMSNetwork::NMSNetwork(IPAddr_t baseIP)
{
  // Number of routers in each subnet
#define N0Count 3     
#define N1Count 2
#define N2Count 7
#define N3Count 4
#define ServerCount 4

  // Create point-to-point link template to use
  Linkp2p l2Gb(Rate("2Gb"), Time("5ms")); // Link to use, 2Gb

  // Create Net 0
  for (int i = 0; i < N0Count; ++i)
    { // Three routers and three links
      DEBUG(3,(cout << "Creating net0 router " << i
               << " id " << Node::nextId << endl));
      net0.push_back(new Node());
      if (i) net0[i-1]->AddDuplexLink(net0[i], l2Gb);
    }
  net0[N0Count-1]->AddDuplexLink(net0[0], l2Gb);
  
  // Create net 1
  for (int i = 0; i < N1Count ; ++i)
    { // Two routers and one link
      DEBUG(3,(cout << "Creating net1 router " << i
               << " id " << Node::nextId << endl));
      net1.push_back(new Node());
      if(i)net1[i-1]->AddDuplexLink(net1[i], l2Gb);
    }
  // Create the servers on net 1
  for (int i = 0; i < ServerCount ; ++i)
    { // net1 servers
      servers.push_back(new Node());
      servers[i]->SetIPAddr(baseIP + i + 3);
      DEBUG(2,(cout << "Created server IPAddr "
              << (string)IPAddr(baseIP + i + 3)
              << endl));
      if (i < ServerCount / 2)
        servers[i]->AddDuplexLink(net1[0], l2Gb);
      else
        servers[i]->AddDuplexLink(net1[1], l2Gb);
      // Create a TCP server application
      TCPServer* s = (TCPServer*)servers[i]->AddApplication(TCPServer());
      s->Bind(40);
      if (!averageMinMax) averageMinMax = new AverageMinMax();
      s->SetStatistics(averageMinMax);
      TCPTahoe* t = (TCPTahoe*)s->GetL4();
      t->SetTrace(Trace::ENABLED); // Trace for debugging
    }
  
  // Connect net0 to net 1
  DuplexLink dl01(net0[2], l2Gb, net1[0], l2Gb);

  // Create the lone routers R4 and R5
  Node* R4 = new Node();
  Node* R5 = new Node();
  net0[0]->AddDuplexLink(R4, l2Gb);
  net0[1]->AddDuplexLink(R5, l2Gb);
  R4->AddDuplexLink(R5, l2Gb);

  // Create net 2
  for (int i = 0; i < N2Count; ++i)
    { // Seven routers and seven links
      DEBUG(3,(cout << "Creating net2 router " << i
               << " id " << Node::nextId << endl));
      net2.push_back(new Node());
    }
  // Router connectivity in net 2
  net2[0]->AddDuplexLink(net2[1], l2Gb);
  net2[1]->AddDuplexLink(net2[3], l2Gb);
  net2[2]->AddDuplexLink(net2[3], l2Gb);
  net2[0]->AddDuplexLink(net2[2], l2Gb);
  net2[2]->AddDuplexLink(net2[4], l2Gb);
  net2[3]->AddDuplexLink(net2[5], l2Gb);
  net2[5]->AddDuplexLink(net2[6], l2Gb);

  // Connectivity to R4
  net2[0]->AddDuplexLink(R4, l2Gb);
  net2[1]->AddDuplexLink(R4, l2Gb);

  IPAddr_t campusBaseIP = baseIP + IPAddr("0.0.2.1");
  IPAddr_t mult = IPAddr("0.0.4.0");
  // Create the net2 campus lans
  campuses.push_back(new CampusLan(net2[2], campusBaseIP + 0 * mult));
  campuses.push_back(new CampusLan(net2[3], campusBaseIP + 1 * mult));
  campuses.push_back(new CampusLan(net2[4], campusBaseIP + 2 * mult));
  campuses.push_back(new CampusLan(net2[5], campusBaseIP + 3 * mult));
  campuses.push_back(new CampusLan(net2[6], campusBaseIP + 4 * mult));
  campuses.push_back(new CampusLan(net2[6], campusBaseIP + 5 * mult));
  campuses.push_back(new CampusLan(net2[6], campusBaseIP + 6 * mult));
  
  // Create net 3
  for (int i = 0; i < N3Count; ++i)
    { // Seven routers and seven links
      DEBUG(3,(cout << "Creating net3 router " << i
               << " id " << Node::nextId << endl));
      net3.push_back(new Node());
    }
  net3[0]->AddDuplexLink(net3[1], l2Gb);
  net3[1]->AddDuplexLink(net3[2], l2Gb);
  net3[1]->AddDuplexLink(net3[3], l2Gb);
  net3[2]->AddDuplexLink(net3[3], l2Gb);

  // Connect to R5
  net3[0]->AddDuplexLink(R5, l2Gb);
  net3[1]->AddDuplexLink(R5, l2Gb);

  // Create the net3 campus lans
  campuses.push_back(new CampusLan(net3[0], campusBaseIP + 7 * mult));
  campuses.push_back(new CampusLan(net3[0], campusBaseIP + 8 * mult));
  campuses.push_back(new CampusLan(net3[2], campusBaseIP + 9 * mult));
  campuses.push_back(new CampusLan(net3[3], campusBaseIP + 10* mult));
  campuses.push_back(new CampusLan(net3[3], campusBaseIP + 11* mult));
}

Node* NMSNetwork::Gateway()
{ // Return the gateway (network 0, node 0)
  return net0[0];
}

Node* NMSNetwork::GetServer(Count_t i)
{ // Return specified server
  if (i >= servers.size()) return nil; // Out of range
  return servers[i];
}

Node* NMSNetwork::GetNode(Count_t cl, Count_t en, Count_t i)
{ // Get node from specified campus lan, ethernet, index
  if (cl >= campuses.size()) return nil;  // Out of range
  return campuses[cl]->GetNode(en, i);    // Get node from specified CampusLan
}

// Define a class for the CampusLans
CampusLan::CampusLan(Node* g, IPAddr_t ip)
{
  // Create a 1Gb link to use
  Linkp2p l1Gb(Rate("1Gb"), Time("5ms")); // Link to use, 1Gb

  lanRouter = new Node();
  g->AddDuplexLink(lanRouter, l1Gb);
  Count_t baseNodeCount = 10;
  if (scaledDown) baseNodeCount = 2;
  Node* gwNode = lanRouter; // Gateway
  if (nonSingleGateway)
    {
      baseNodeCount++; // Add another for the gateway
      gwNode = nil;
    }
  // Create the four ethernets with 10, 10, 10, and 12 nodes respectively
  DEBUG(3,(cout << "Creating enet, gateway is " << gwNode->Id() << endl));
  DEBUG(1,(cout << "Creating enet, IP " 
           << (string)IPAddr(ip + 0x000) << endl));
  enets.push_back(new Ethernet(baseNodeCount, gwNode,
                               ip + 0x000, Mask(24),
                               0, 
                               Rate("100Mb"), Time("1ms")));
  DEBUG(1,(cout << "Creating enet, IP "
           << (string)IPAddr(ip + 0x100) << endl));
  enets.push_back(new Ethernet(baseNodeCount, gwNode,
                               ip + 0x100, Mask(24),
                               0, 
                               Rate("100Mb"), Time("1ms")));
  DEBUG(1,(cout << "Creating enet, IP " <<
           (string)IPAddr(ip + 0x200) << endl));
  enets.push_back(new Ethernet(baseNodeCount, gwNode,
                               ip + 0x200, Mask(24),
                               0, 
                               Rate("100Mb"), Time("1ms")));
  DEBUG(1,(cout << "Creating enet, IP "
           << (string)IPAddr(ip + 0x300) << endl));
  enets.push_back(new Ethernet(baseNodeCount + 2, gwNode,
                               ip + 0x300, Mask(24),
                               0, 
                               Rate("100Mb"), Time("1ms")));

  // Reduce queue sizes on leafs to reduce memory consumption
  //enets[0]->SetLeafQLimit(2000);
  //enets[1]->SetLeafQLimit(2000);
  //enets[2]->SetLeafQLimit(2000);
  //enets[3]->SetLeafQLimit(2000);

  if (nonSingleGateway)
    {
      // Connect the ethernet gateways to the lanRouter
      enets[0]->GetNode(baseNodeCount - 1)->AddDuplexLink(lanRouter, l1Gb);
      enets[1]->GetNode(baseNodeCount - 1)->AddDuplexLink(lanRouter, l1Gb);
      enets[2]->GetNode(baseNodeCount - 1)->AddDuplexLink(lanRouter, l1Gb);
      enets[3]->GetNode(baseNodeCount - 1)->AddDuplexLink(lanRouter, l1Gb);
    }
}

Node* CampusLan::GetNode(Count_t lan, Count_t which)
{ // Set specified node from specified enet
  if (lan >= enets.size()) return nil; // Out of range
  return enets[lan]->GetNode(which);   // Get the specified node
}

Ethernet* CampusLan::GetENet(Count_t eni)
{ // Get the specified ethernet from a campus lan
  if (eni >= enets.size()) return nil; // Out of range
  return enets[eni];
}

int main(int argc, char** argv)
{
  typedef enum { TCPOnly, UDPOnly, Mixed } RunType_t;
  typedef enum { Large, Small, Tiny } SizeType_t;
  bool           useNER = false;
  RunType_t rt = TCPOnly;
  SizeType_t st = Small;
  bool            seqEvents = false;
  bool            timerBuckets = false;
  bool            deterministic = false; // True if no randomness
  bool            nonDetailedQueue = false; // True if using non-det-q
  bool            useStaticRouting = false; // True if using static routing
  bool            useLogFile = false;       // True if log file desired
  string          rlinkDelayString("200ms");
  distributed = strstr(argv[0], "dist") != NULL;
  if (distributed && (argc < 5))
    {
      cout << "Distributed Usage:  nms-dist keyword nCampus myid totsys" 
           << endl;
      exit(1);
    }
  Count_t    myId;
  Count_t    nSys;

  Queue::Default(DropTail()); // Set default to drop tail
  if (argc > 1)
    {
      // First convert to lower case
      int l = strlen(argv[1]);
      for (int i = 0; i < l; ++i) argv[1][i] = tolower(argv[1][i]);
      string arg(argv[1]);
      if (arg.find("tcp") != string::npos) rt = TCPOnly;
      if (arg.find("udp") != string::npos) rt = UDPOnly;
      if (arg.find("mixed") != string::npos) rt = Mixed;
      if (arg.find("tiny") != string::npos) st = Tiny;
      if (arg.find("small") != string::npos) st = Small;
      if (arg.find("large") != string::npos) st = Large;
      if (arg.find("single") != string::npos) singleNet = true;
      if (arg.find("singleflow") != string::npos) singleflow = true;
      if (arg.find("scale") != string::npos) scaledDown = true;
      if (arg.find("ner") != string::npos) useNER = true;
      if (arg.find("dist") != string::npos) distributed = true;
      if (arg.find("link") != string::npos) seqEvents = true;
      if (arg.find("bucket") != string::npos) timerBuckets = true;
      if (arg.find("det") != string::npos) deterministic = true;
      if (arg.find("ndq") != string::npos) nonDetailedQueue = true;
      if (arg.find("nsg") != string::npos) nonSingleGateway = true;
      if (arg.find("static") != string::npos) useStaticRouting = true;
      if (arg.find("log") != string::npos) useLogFile = true;
    }
  if (nonSingleGateway)
    {
      cout << "Using non-SingleGateway" << endl;
    }

  if (argc > 2) netCount = atol(argv[2]);
  if (singleNet) netCount = 1;
  if (!netCount)
    {
      cout << "Invalid Network Count " << argv[2] << endl;
      exit(1);
    }
  if (distributed)
    {
      myId = atol(argv[3]);
      nSys = atol(argv[4]);
      totalNets = netCount * nSys;
      baseNet = netCount * myId;
    }
  else
    {
      totalNets = netCount;
      baseNet = 0;
    }
  if (argc > 5) rlinkDelayString = argv[5];
  NetVec_t Networks;
  IPAddr_t baseIp = IPAddr("0.0.0.0") + netMult * baseNet;

  // Define the simulator object
  Simulator* s;
  if (distributed)
    {
      s =  new DistributedSimulator(myId);
      if (useNER)
        {
          cout << "Using NER time advance" << endl;
          // Specify NextEventReqest Time advance
          ((DistributedSimulator*)s)->UseNER();
        }
      cout << "Using lookahead of " << rlinkDelayString << endl;
    }
  else
    {
      s = new Simulator();
    }
  s->CleanupOnExit(); // For debugging memory leaks
  // Specify NixVector routing
  if (useStaticRouting)
    {
      cout << "Using Static Routing" << endl;
      Routing::SetRouting(new RoutingStatic());  // Static routing specified
    }
  else
    {
      Routing::SetRouting(new RoutingNixVector());
    }

  // Create the trace file
  Trace* gs = Trace::Instance();
  gs->IPDotted(true);
  if (useLogFile) gs->Open("nms.txt");
  // Enable all l3 tracing
  IPV4::Instance()->SetTrace(Trace::ENABLED);
  TCP::LogFlagsText(true); // Text-style flags
  //L2Proto802_3::Instance()->SetTrace(Trace::ENABLED);

  // Set slightly higher initial rtt estimate
  RTTEstimator::InitialEstimate(3.0);

  // Set default segment size for TCP and UDP connections
  //TCP::DefaultSegSize(1460);  // Plus 40 for tcp/ip hdr, plus 14 for l2=1514
  TCP::DefaultSegSize(1000);    // Plus 40 for tcp/ip hdr, plus 14 for l2=1054
  UDP::DefaultPacketSize(1000);
  TCP::DefaultAdvWin(20000);    // Matches NS default window size
  TCP::UseTimerBuckets(timerBuckets);   // Use bucketed timers (efficient)
  TCP::Default().SetTrace(Trace::ENABLED); // Enable trace on default tcp
  Link::UseSeqEvents(seqEvents);     // Bypass central event list (efficient)
  OnOffApplication::DefaultSize(1000);

  // Set default queue limit
  Queue::DefaultLength(50000);
  if (nonDetailedQueue)
    {
      Queue::Default().Detailed(false);   // Skip queuing details
      cout << "Using non-detailed queue" << endl;
    }
  // Create 20 copies of the network
  for (Count_t i = 0; i < netCount; ++i)
    {
      IPAddr_t subnetIP = baseIp + netMult * (i+1);
      DEBUG(1,(cout << "Creating network subnetIP " <<
               (string)IPAddr(subnetIP) << endl));
      Networks.push_back(new NMSNetwork(subnetIP));
      DEBUG0((cout << "Created network, node count " << Node::nextId << endl));
    }
  cout << "Created " << netCount
       << " networks, node count " << Node::nextId << endl;
  // Connect the gateways
  Time rlinkDelay(rlinkDelayString.c_str());
  Linkp2p l2Gb(Rate("2Gb"), rlinkDelay); // Link to use, 2Gb
  // Create the ring
  if (netCount > 1)
    {
      for (Count_t i = 0; i < netCount; ++i)
        {
          Count_t  nextNet = baseNet + i + 1;
          if (nextNet == totalNets) nextNet = 0;
          Count_t priorNet = baseNet + i - 1;
          if (baseNet == 0 && i == 0) priorNet = totalNets - 1;
          IPAddr_t thisIP   = netMult * (baseNet + i + 1);
          IPAddr_t nextRIP  = thisIP + IPAddr("0.0.1.1");
          IPAddr_t nextIP   = netMult * (nextNet + 1);
          IPAddr_t priorIP  = netMult * (priorNet + 1);
          IPAddr_t priorRIP = priorIP + IPAddr("0.0.1.2");
          if (distributed)
            {
              if (i == (netCount-1))
                { // Need a remote link to next system, zeroth network
                  Interface* iface = Networks[i]->Gateway()->AddRemoteLink(
                                                  nextRIP, Mask(24),
                                                  Rate("2Gb"), rlinkDelay);
                  // Notify of remote ip's reachable via this link  
                  DEBUG(1,(cout << "Next RemoteteLink IP is "
                           << (string)IPAddr(nextRIP) << endl));
                  DEBUG(1,(cout << "Adding remote ip "
                           << (string)IPAddr(nextIP) << endl));
                  iface->AddRemoteIP(nextIP, Mask(16));
                  interfaces.push_back(iface);
                }
              else
                { // Need normal link to next subnet
                  Interface* iface = Networks[i]->Gateway()->AddDuplexLink(
                               Networks[(i+1)%netCount]->Gateway(), l2Gb);
                  interfaces.push_back(iface);
                }
              if (!i)
                { // Need a remote link to prior system, zero'th network
                  if (netCount > 1)
                    { // but not if only one network per federate
                      Interface* iface = Networks[i]->Gateway()->AddRemoteLink(
                                                 priorRIP, Mask(24),
                                                 Rate("2Gb"), rlinkDelay);
                      DEBUG(1,(cout << "Prior RemoteteLink IP is "
                               << (string)IPAddr(priorRIP) << endl));
                      // Notify of remote ip's reachable via this link
                      iface->AddRemoteIP(priorIP, Mask(16));
                      DEBUG(1,(cout << "Adding remote ip "
                               << (string)IPAddr(priorIP) << endl));
                      interfaces.push_back(iface);
                    }
                }
            }
          else
            {
              Interface* iface = Networks[i]->Gateway()->AddDuplexLink(
                           Networks[(i+1)%netCount]->Gateway(), l2Gb);
              interfaces.push_back(iface);
            }
        }
      // Create the N+4 chords
      for (Count_t i = 0; i < netCount; i+=4)
        { // ! need to account for distributed here also, but not really
          // since the chords are not really used
          Networks[i]->Gateway()->AddDuplexLink(
                                  Networks[(i+4)%netCount]->Gateway(), l2Gb);
        }
      // Create the N+10 chords
      for (Count_t i = 0; i < netCount/2; i+=2)
        {
          Networks[i]->Gateway()->AddDuplexLink(
                                  Networks[(i+10)%netCount]->Gateway(), l2Gb);
        }
    }

  // Create the tcp and/or udp data flows
  Constant Sz(5000l);
  switch (st) {
    case Large : 
      Sz.NewConstant(500000000l);
      break;
    case Small:
      Sz.NewConstant(500000l);
      break;
    case Tiny:
      Sz.NewConstant(50000l);
      break;
  }
  Random*  startRng;
  Random*  serverRng;
  if (deterministic)
    { // Use deterministic values for debugging
      startRng = new Sequential(0, 1.0, 0.1); // Random starting times
      serverRng = new Sequential(0, 4, 1); // for randomly choosing servers
      cout << "Using Deterministic RNG" << endl;
    }
  else
    {
      startRng = new Uniform(0, 1.0); // Random starting times
      //startRng = new Uniform(100., 110.0); // Random starting times
      serverRng = new Uniform(0, 4); // for randomly choosing servers
    }
  switch (rt) {
    case TCPOnly:
      if (singleflow)
        {
          // Try a single connection
          Node* mn = Networks[0]->GetNode(0,1,1);
          // Choose a random server
          Count_t whichServer = (Count_t)serverRng->Value();
          // Get peer node ip
          IPAddr_t peerIP;
          if (singleNet)
            peerIP = (baseNet + 0 + 3) * netMult + whichServer;
          else
            peerIP = (baseNet + 1 + 3) * netMult + whichServer;
          TCPSend* tapp = (TCPSend*)mn->AddApplication(
              TCPSend(peerIP, 40, Sz));
          // Start at random time 0-10                 
          tapp->Start(startRng->Value());
        }
      else 
        {
          TCPTahoe t(nil); // Create the TCP agent to copy for each app
          t.DeleteOnComplete();
          NetVec_t::size_type nnet = Networks.size();
          if (singleNet) nnet = 1;
          for (NetVec_t::size_type ni = 0; ni < nnet; ++ni)
            { // for each network (for debug just one
              CLanVec_t& clan = Networks[ni]->campuses; // Get the campus vec
              for (CLanVec_t::size_type ci = 0; ci < clan.size(); ++ci)
                { // for each campus network
                  for (Count_t ei = 0; ei < 4; ++ei)
                    { // each ethernet
                      Count_t pc = clan[ci]->GetENet(ei)->PeerCount();
                      for (Count_t eni = 1; eni <= pc; ++eni)
                        { // Get nodes from the ethernet except 0 (gateway)
                          // Get my node      
                          Node* mn = Networks[ni]->GetNode(ci, ei, eni);
                          if (!mn)
                            { 
                              cout << "HuH?  Null my node" << endl;
                              exit(1);
                            }
                          // Choose a random server
                          Count_t whichServer = (Count_t)serverRng->Value();
                          // Get peer ip
                          Count_t nextNet = baseNet + ni + 1;
                          if (nextNet == totalNets) nextNet = 0;
                          IPAddr_t peerIP = netMult * (nextNet+1);
                          peerIP += whichServer + 3;
                          DEBUG(2,(cout << "Connecting to peer IP " 
                                  << (string)IPAddr(peerIP) << endl));
                          t.Attach(mn); // Attach to specified node
                          t.SetTrace(Trace::ENABLED);
                          TCPSend* tapp = 
                              new TCPSend(peerIP, 40, Sz, t);
                          // Start at random time 100-110        
                          tapp->Start(startRng->Value());
                          tcpSends.push_back(tapp);
                          totalFlows++;
                          // debug follows
#ifdef JUST_DEBUG
                          tapp->srcip = mn->GetIPAddr();
                          tapp->srcPort = 10000;
                          tapp->dstip = peerIP;
                          tapp->dstPort = 40;
#endif
                        }
                    }
                }
            }
        }
      break;
    case UDPOnly:
      if (singleflow)
        {
          // Try a single connection
          Node* mn = Networks[0]->GetNode(0,1,1);
          // Choose a random server
          Count_t whichServer = (Count_t)serverRng->Value();
          // Get peer node
          Node* pn;
          DEBUG0((cout << "Choosing server " << whichServer << endl));
          if (singleNet)
            pn = Networks[0]->GetServer(whichServer);
          else
            pn = Networks[1]->GetServer(whichServer);
          OnOffApplication* ooa = 
                  (OnOffApplication*)mn->AddApplication(
                              OnOffApplication(pn->GetIPAddr(), 1000,
                                               Constant(INFINITE_TIME),
                                               Constant(0),
                                               UDP(),
                                               Rate("1.6Mb")));
          ooa->GetLocalProto()->SetTrace(Trace::ENABLED);
          ooa->MaxBytes(Sz.IntValue());
          ooa->MaxBytes(500000);
          ooa->Start(0);
        }
      else 
        {
          NetVec_t::size_type nnet = Networks.size();
          if (singleNet) nnet = 1;
          for (NetVec_t::size_type ni = 0; ni < nnet; ++ni)
            { // for each network (for debug just one
              NetVec_t::size_type nn = (ni+1) % Networks.size();
              DEBUG(1,(cout << "Networks.size() " << Networks.size()
                       << " nn " << nn << endl));
              CLanVec_t& clan = Networks[ni]->campuses; // Get the campus vec
              for (CLanVec_t::size_type ci = 0; ci < clan.size(); ++ci)
                { // for each campus network
                  for (Count_t ei = 0; ei < 4; ++ei)
                    { // each ethernet
                      Count_t pc = clan[ci]->GetENet(ei)->PeerCount();
                      for (Count_t eni = 1; eni <= pc; ++eni)
                        { // Get nodes from the ethernet except 0 (gateway)
                          // Get my node      
                          Node* mn = Networks[ni]->GetNode(ci, ei, eni);
                          if (!mn)
                            { 
                              cout << "HuH?  Null my node" << endl;
                              exit(1);
                            }
                          // Choose a random server
                          Count_t whichServer = (Count_t)serverRng->Value();
                          // Get peer node
                          Node* pn = Networks[nn]->GetServer(whichServer);
                          OnOffApplication* ooa = 
                             (OnOffApplication *)mn->AddApplication( 
                                            OnOffApplication(pn->GetIPAddr(),
                                                      1000,
                                                      Constant(INFINITE_TIME),
                                                      Uniform(0,10),
                                                      UDP(),
                                                      Rate("1.6Mb")));
                          ooa->GetLocalProto()->SetTrace(Trace::ENABLED);
                          ooa->MaxBytes(Sz.IntValue());
                          ooa->Start(0);
                        }
                    }
                }
            }
        }
      break;
    case Mixed:
      break;
  }
  cout << "Created " << netCount << " copies of the network" << endl;
  cout << "Total nodes created " << Node::GetNodes().size() << endl;
  cout << "Total flows created " << totalFlows << endl;
  cout << "Memory usage before run " 
       << s->ReportMemoryUsageMB() << "MB" << endl;
  s->ProgressHook(Progress);
  s->Progress(1.0);
  s->StopAt(40.0);
  cout << "Starting simulation run" << endl;
  s->Run();
  Stats::Print(); // Print the statistics
  cout << "Memory usage after run " 
       << s->ReportMemoryUsageMB() << "MB" << endl;
  cout << "Total events processed "
       << Scheduler::Instance()->TotalEventsProcessed() << endl;
  cout << "Total pkt-hops " << Stats::pktsTransmitted << endl;
  cout << "Setup time " << s->SetupTime() << endl;
  cout << "Route time " << s->RouteTime() << endl;
  cout << "Run time "   << s->RunTime() << endl;
  cout << "Total time "   << s->TotalTime() << endl;
  cout << "Pkt-hops/sec " << (double)Stats::pktsTransmitted / s->TotalTime()
       << endl;
  cout << "NUpdates " << LinkRTI::nUpdates
       << " NReflect " << LinkRTI::nReflects 
       << " IReflect " << LinkRTI::ignoredReflects << endl;
#ifdef VERBOSE_DEBUG
  s->PrintStats();
#endif
  delete s;       // Delete before printing stats so we have cleaned up
  if (averageMinMax)
    {
      if (averageMinMax->Samples())
        { // At least one sample
          averageMinMax->Log(cout);
        }
    }
  TimerBucket::Instance()->DBDump();
#ifdef VERBOSE_DEBUG
  // debug...dump state of all sending apps
  for (TCPSendVec_t::size_type i = 0; i < tcpSends.size(); ++i)
    {
      tcpSends[i]->DBDump();
    }
  // for debug, dump length of interface event queues
  for (IFVec_t::size_type i = 0; i < interfaces.size(); ++i)
    {
      Interface* iface = interfaces[i];
      cout << "Interface " << (string)IPAddr(iface->GetIPAddr())
           << " pending size " << iface->evList.size() << endl;
    }
#endif
}
