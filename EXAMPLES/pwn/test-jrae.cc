#include <iostream>
#include <sstream>
#include <string>
#include <sstream>

#include "debug.h"
#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "node-satellite.h"
#include "node-taclane.h"
#include "star.h"
#include "linkp2p.h"
#include "application-cbr.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "application-udpsink.h"
#include "rng.h"
#include "routing.h"
#include "routing-manual.h"
#include "routing-static.h"
#include "diffserv-queue.h"
#include "time-value.h"
#include "time-value-graph.h"
#include "tcp-tahoe.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "udp.h"
#include "validation.h"
#include "qtwindow.h"
#include "application-onoff.h"
#include "ipv4.h"

// Images
#include "image-RedRouter.h"
#include "image-BlackRouter.h"
#include "image-GreenRouter.h"
#include "image-Host.h"
#include "image-HostSmall.h"

#ifdef HAVE_QT
#include <qcolor.h>
#endif

using namespace std;


class TextEvent : public  Event 
{
public:
  TextEvent(QCanvasText* ct) 
      : t(ct) {}
public:
  QCanvasText* t;
};

class TextHandler : public Handler
{
public:
  void Handle(Event*, Time_t);
};

void TextHandler::Handle(Event* e, Time_t)
{
  TextEvent* te = (TextEvent*)e;
  // Just delete the text event and update the canvas
  delete te->t;
  Simulator::instance->GetQTWindow()->Canvas()->update();
  // and delete the event as we don't need it anymore
  delete e;
}

// Define the texthandler object
TextHandler textHandler;

void NodeSelected(Node* n)
{
  cout << "Hello from NodeSelected, node " << n->Id() << endl;
  QTWindow* qtw = Simulator::instance->GetQTWindow();
  if (!qtw) return; // Can't happen
  QCanvas* canvas = qtw->Canvas();
  string* nodeName = (string*)n->UserInformation();
  QCanvasText* qct;
  ostringstream ostr;
  ostr << "Node " << n->Id();
  string nn = ostr.str();
  if (nodeName)
    {
      nn += "\n" + *nodeName;
    }

  qct = new QCanvasText(nn.c_str(), canvas);
#ifdef OLD_WAY  
  char work[128];
  sprintf(work, "Node %d", n->Id());
  if (nodeName)
    {
      char work1[2048];
      sprintf(work1, "%s\n%s", work, nodeName->c_str());
      strcpy(work, work1);
    }
  qct = new QCanvasText(work, canvas);
#endif
  
  // Place the text item near the node on the canvas
  QPoint p = qtw->LocationToPixels(n->GetLocation());
  qct->move(p.x() + 10, p.y() - 10);
  qct->show();
  // Schedule an event 1 sec in future to remove it
  TextEvent* te = new TextEvent(qct);
  //Simulator::instance->Schedule(te, 1.0, textHandler);
  Scheduler::Schedule(te, 1.0, textHandler);
  // Update the canvas
  canvas->update();
  qtw->ProcessEvents();
}


int main(int argc, char** argv)
{
	
	Validation::Init(argc, argv);
	Simulator s;
	// Set node shape to a circle for animation
	Node::DefaultShape(Node::CIRCLE);
	
	Trace* tr = Trace::Instance(); // Get a pointer to global trace object
	tr->Open("test-jrae.txt");
	IPV4::Instance()->SetTrace(Trace::ENABLED);

	s.StartAnimation(0, true, true);
	
	
	/******************************JRAE 04-4 IP Address Initialization******************************/
	
	//PEO C3T - Ft. Hood, TX network elements
	IPAddr_t abrigade = IPAddr("192.169.1.1");			//Brigade
	IPAddr_t aR40_LAN = IPAddr("192.169.1.2");			//Brigade
	IPAddr_t aR40_H8 = IPAddr("192.168.1.2");			//Brigade
	IPAddr_t aH8_R40 = IPAddr("192.168.1.1");			//Brigade
	IPAddr_t aH8_R41 = IPAddr("134.207.136.186");		//Brigade
	IPAddr_t aR41_H8 = IPAddr("134.207.136.185");		//Brigade
	IPAddr_t aR41_SAT2b = IPAddr("134.207.136.98");		//Brigade
	
	//JITC - Ft. Huachuca, AZ network elements
	IPAddr_t asoc = IPAddr("192.169.2.1");				
	IPAddr_t aR23_LAN = IPAddr("192.169.2.2");			//Spec. Ops. Command
	IPAddr_t aR23_H5 = IPAddr("192.168.2.2");			//Spec. Ops. Command
	IPAddr_t aH5_R23 = IPAddr("192.168.2.1");			//Spec. Ops. Command
	IPAddr_t aH5_R8 = IPAddr("134.207.136.34");			//Spec. Ops. Command
	
	//AFCA - Scott AFB, IL network elements
	IPAddr_t afs1c = IPAddr("192.169.10.1");			//Fighter Squadron 1 Commander
	IPAddr_t aR22_LAN = IPAddr("192.169.10.2");			//Fighter Squadron 1 Commander
	IPAddr_t aR22_H6 = IPAddr("192.168.10.2");			//Fighter Squadron 1 Commander
	
	//BCBL - Ft. Gordon, GA network elements
	IPAddr_t abatallion = IPAddr("192.169.3.1");		//Batallion
	IPAddr_t aR25_LAN = IPAddr("192.169.3.2");			//Batallion
	IPAddr_t aR25a_H9 = IPAddr("192.168.3.2");			//Batallion
	IPAddr_t aH9_R25a = IPAddr("192.168.3.1");			//Batallion
	IPAddr_t aH9_R14 = IPAddr("134.207.136.110");		//Batallion
	IPAddr_t aR14_H9 = IPAddr("134.207.136.109");		//Batallion
	IPAddr_t aR14_SAT2a = IPAddr("134.207.136.106");	//Batallion
	IPAddr_t aR14_R15 = IPAddr("134.207.136.158");		//Batallion
	IPAddr_t aplatoon = IPAddr("192.169.4.1");			//Platoon
	IPAddr_t aR26_LAN = IPAddr("192.169.4.2");			//Platoon
	IPAddr_t aR26_H10 = IPAddr("192.168.4.2");			//Platoon
	IPAddr_t aH10_R26 = IPAddr("192.168.4.1");			//Platoon
	IPAddr_t aH10_R13 = IPAddr("134.207.136.102");		//Platoon
	IPAddr_t aR13_H10 = IPAddr("134.207.136.101");		//Platoon
	IPAddr_t aR13_R19 = IPAddr("134.207.136.150");		//Platoon
	
	//NRL - Washington, D.C. network elements
	IPAddr_t aR12_R7 = IPAddr("134.207.136.46");		//Teleport 2
	IPAddr_t aR12_SAT2d = IPAddr("134.207.136.86");		//Teleport 2
	IPAddr_t aR12_SAT2c = IPAddr("134.207.136.89");		//Teleport 2
	IPAddr_t aR12_SAT2b = IPAddr("134.207.136.97");		//Teleport 2
	IPAddr_t aR12_SAT2a = IPAddr("134.207.136.105");	//Teleport 2
	IPAddr_t auav = IPAddr("192.169.5.1");				//UAV
	IPAddr_t aR27_LAN = IPAddr("192.169.5.2");			//UAV
	IPAddr_t aR27_H3 = IPAddr("192.168.5.2");			//UAV
	IPAddr_t aH3_R27 = IPAddr("192.168.5.1");			//UAV
	IPAddr_t aH3_R6 = IPAddr("134.207.136.78");			//UAV
	IPAddr_t aR6_H3 = IPAddr("134.207.136.77");			//UAV
	IPAddr_t aR6_SAT1a = IPAddr("134.207.136.74");		//UAV
	IPAddr_t aR4_SAT1c = IPAddr("134.207.136.173");		//Teleport 1
	IPAddr_t aR4_SAT1b = IPAddr("134.207.136.65");		//Teleport 1
	IPAddr_t aR4_SAT1a = IPAddr("134.207.136.73");		//Teleport 1
	IPAddr_t aR4_R2 = IPAddr("134.207.136.18");			//Teleport 1
	IPAddr_t aR43_H20 = IPAddr("192.168.6.2");			//GIG-BE POP1
	IPAddr_t aH20_R43 = IPAddr("192.168.6.1");			//GIG-BE POP1
	IPAddr_t aH20_R42 = IPAddr("134.207.136.190");		//GIG-BE POP1
	IPAddr_t aR42_H20 = IPAddr("134.207.136.189");		//GIG-BE POP1
	IPAddr_t aR42_R2 = IPAddr("134.207.136.38");		//GIG-BE POP1
	IPAddr_t aR2_R1 = IPAddr("134.207.136.6");			//GIG-BE POP1
	IPAddr_t aR2_R25b = IPAddr("134.207.136.57");		//GIG-BE POP1
	IPAddr_t aR2_R4 = IPAddr("134.207.136.17");			//GIG-BE POP1
	IPAddr_t aR2_R7 = IPAddr("134.207.136.25");			//GIG-BE POP1
	IPAddr_t aR2_R8 = IPAddr("134.207.136.21");			//GIG-BE POP1
	IPAddr_t aR2_R3 = IPAddr("134.207.136.9");			//GIG-BE POP1
	IPAddr_t aR2_R42 = IPAddr("134.207.136.37");		//GIG-BE POP1
	IPAddr_t aaorc = IPAddr("192.169.7.1");			//AOR Commander
	IPAddr_t aR28_LAN = IPAddr("192.169.7.2");			//AOR Commander
	IPAddr_t aR28_H18 = IPAddr("192.168.7.2");			//AOR Commander
	IPAddr_t aH18_R28 = IPAddr("192.168.7.1");			//AOR Commander
	IPAddr_t aH18_R25b = IPAddr("134.207.136.62");		//AOR Commander
	IPAddr_t aR25b_H18 = IPAddr("134.207.136.61");		//AOR Commander
	IPAddr_t aR25b_R2 = IPAddr("134.207.136.58");		//AOR Commander
	
	//MIT-LL - Lexington, MA network elements
	IPAddr_t aR8_H5 = IPAddr("134.207.136.33");			//GIG-BE POP3
	IPAddr_t aR8_R2 = IPAddr("134.207.136.22");			//GIG-BE POP3
	IPAddr_t aR8_R7 = IPAddr("134.207.136.30");			//GIG-BE POP3
	IPAddr_t aR8_R16 = IPAddr("134.207.136.41");		//GIG-BE POP3
	IPAddr_t aSAT1a_R6 = IPAddr("134.207.137.73");		//Satellite 1
	IPAddr_t aSAT1a_R4 = IPAddr("134.207.137.74");		//Satellite 1
	IPAddr_t aSAT1b_R5 = IPAddr("134.207.137.65");		//Satellite 1
	IPAddr_t aSAT1b_R4 = IPAddr("134.207.137.66");		//Satellite 1
	IPAddr_t aSAT1c_R24 = IPAddr("134.207.137.173");	//Satellite 1
	IPAddr_t aSAT1c_R4 = IPAddr("134.207.137.174");		//Satellite 1
	IPAddr_t aSAT2a_R14 = IPAddr("134.207.137.105");	//Satellite 2
	IPAddr_t aSAT2a_R12 = IPAddr("134.207.137.106");	//Satellite 2
	IPAddr_t aSAT2b_R41 = IPAddr("134.207.137.97");		//Satellite 2
	IPAddr_t aSAT2b_R12 = IPAddr("134.207.137.98");		//Satellite 2
	IPAddr_t aSAT2c_R11 = IPAddr("134.207.137.89");		//Satellite 2
	IPAddr_t aSAT2c_R12 = IPAddr("134.207.137.90");		//Satellite 2
	IPAddr_t aSAT2d_R10 = IPAddr("134.207.137.86");		//Satellite 2
	IPAddr_t aSAT2d_R12 = IPAddr("134.207.137.85");		//Satellite 2
	IPAddr_t aSAT3_R17 = IPAddr("134.207.137.121");		//Satellite 3
	IPAddr_t aSAT3_R16 = IPAddr("134.207.137.122");		//Satellite 3
	
	//DISA network elements
	IPAddr_t aR7_R8 = IPAddr("134.207.136.29");			//GIG-BE POP2
	IPAddr_t aR7_R12 = IPAddr("134.207.136.45");		//GIG-BE POP2
	IPAddr_t aR7_R11 = IPAddr("134.207.136.49");		//GIG-BE POP2
	IPAddr_t aR7_R10 = IPAddr("134.207.136.53");		//GIG-BE POP2
	IPAddr_t aR7_R2 = IPAddr("134.207.136.26");			//GIG-BE POP2
	
	//JTEO - McLean, VA network elements
	IPAddr_t afs2c = IPAddr("192.169.8.1");				//Fighter Squadron 2 Commander
	IPAddr_t aR29_LAN = IPAddr("192.169.8.2");			//Fighter Squadron 2 Commander
	IPAddr_t aR29_H2 = IPAddr("192.168.8.2");			//Fighter Squadron 2 Commander
	IPAddr_t aH2_R29 = IPAddr("192.168.8.1");			//Fighter Squadron 2 Commander
	IPAddr_t aH2_R5 = IPAddr("134.207.136.70");			//Fighter Squadron 2 Commander
	IPAddr_t aR5_H2 = IPAddr("134.207.136.69");			//Fighter Squadron 2 Commander
	IPAddr_t aR5_SAT1b = IPAddr("134.207.136.66");		//Fighter Squadron 2 Commander
	IPAddr_t auavcrew = IPAddr("192.169.9.1");			//UAV Crew
	IPAddr_t aR30_LAN = IPAddr("192.169.9.2");			//UAV Crew
	IPAddr_t aR30_H4 = IPAddr("192.168.9.2");			//UAV Crew
	IPAddr_t aH4_R30 = IPAddr("192.168.9.1");			//UAV Crew
	IPAddr_t aH4_R3 = IPAddr("134.207.136.14");			//UAV Crew
	IPAddr_t aR3_H4 = IPAddr("134.207.136.13");			//UAV Crew
	IPAddr_t aR3_R2 = IPAddr("134.207.136.10");			//UAV Crew
	
	//SSC-SD - San Diego, CA network elements
	IPAddr_t aH6_R22 = IPAddr("192.168.10.1");			//CJTF
	IPAddr_t aH6_R10 = IPAddr("134.207.136.82");		//CJTF
	IPAddr_t aR10_H6 = IPAddr("134.207.136.81");		//CJTF
	IPAddr_t aR10_R7 = IPAddr("134.207.136.54");		//CJTF
	IPAddr_t aR10_SAT2d = IPAddr("134.207.136.85");		//CJTF
	IPAddr_t ajsotf = IPAddr("192.169.11.1");			//JSOTF
	IPAddr_t aR31_LAN = IPAddr("192.169.11.2");			//JSOTF
	IPAddr_t aR31_H17 = IPAddr("192.168.11.2");			//JSOTF
	IPAddr_t aH17_R31 = IPAddr("192.168.11.1");			//JSOTF
	IPAddr_t aH17_R24 = IPAddr("134.207.136.178");		//JSOTF
	IPAddr_t aR24_H17 = IPAddr("134.207.136.177");		//JSOTF
	IPAddr_t aR24_SAT1c = IPAddr("134.207.136.174");	//JSOTF
	IPAddr_t aship2g = IPAddr("192.169.12.1");			//ESG Commander (Ship 2)
	IPAddr_t aR32_LAN = IPAddr("192.169.12.2");			//ESG Commander (Ship 2)
	IPAddr_t aR32_H14 = IPAddr("192.168.12.2");			//ESG Commander (Ship 2)
	IPAddr_t aH14_R32 = IPAddr("192.168.12.1");			//ESG Commander (Ship 2)
	IPAddr_t aH14_R17 = IPAddr("134.207.136.130");		//ESG Commander (Ship 2)
	IPAddr_t aship2r = IPAddr("192.169.13.1");			//ESG Commander (Ship 2)
	IPAddr_t aR33_LAN = IPAddr("192.169.13.2");			//ESG Commander (Ship 2)
	IPAddr_t aR33_H13 = IPAddr("192.168.13.2");			//ESG Commander (Ship 2)
	IPAddr_t aH13_R33 = IPAddr("192.168.13.1");			//ESG Commander (Ship 2)
	IPAddr_t aH13_R17 = IPAddr("134.207.136.126");		//ESG Commander (Ship 2)
	IPAddr_t aship2tl = IPAddr("192.168.14.2");			//ESG Commander (Ship 2)
	IPAddr_t aH16_LAN = IPAddr("192.168.14.1");			//ESG Commander (Ship 2)
	IPAddr_t aH16_R17 = IPAddr("134.207.136.182");		//ESG Commander (Ship 2)
	IPAddr_t aR17_SAT3 = IPAddr("134.207.136.122");		//ESG Commander (Ship 2)
	IPAddr_t aR17_H16 = IPAddr("134.207.136.181");		//ESG Commander (Ship 2)
	IPAddr_t aR17_H13 = IPAddr("134.207.136.125");		//ESG Commander (Ship 2)
	IPAddr_t aR17_H14 = IPAddr("134.207.136.129");		//ESG Commander (Ship 2)
	IPAddr_t aR17_R18 = IPAddr("134.207.136.166");		//ESG Commander (Ship 2)
	IPAddr_t aR20_R21 = IPAddr("134.207.136.137");		//JTRS Cloud
	IPAddr_t aR20_JTRS = IPAddr("134.207.136.138");		//JTRS Cloud
	IPAddr_t aR19_R13 = IPAddr("134.207.136.145");		//JTRS Cloud
	IPAddr_t aR19_JTRS = IPAddr("134.207.136.146");		//JTRS Cloud
	IPAddr_t aR18_R17 = IPAddr("134.207.136.161");		//JTRS Cloud
	IPAddr_t aR18_JTRS = IPAddr("134.207.136.162");		//JTRS Cloud
	IPAddr_t aR15_R14 = IPAddr("134.207.136.153");		//JTRS Cloud
	IPAddr_t aR15_JTRS = IPAddr("134.207.136.154");		//JTRS Cloud
	IPAddr_t aJTRS_R20 = IPAddr("134.207.136.139");		//JTRS Cloud
	IPAddr_t aJTRS_R19 = IPAddr("134.207.136.147");		//JTRS Cloud
	IPAddr_t aJTRS_R18 = IPAddr("134.207.136.163");		//JTRS Cloud
	IPAddr_t aJTRS_R15 = IPAddr("134.207.136.155");		//JTRS Cloud
	
	//CPSG - Brooks AFB, SC network elements
	IPAddr_t aaoc1 = IPAddr("192.168.15.2");			//Air Ops Center
	IPAddr_t aH7_LAN = IPAddr("192.168.15.1");			//Air Ops Center
	IPAddr_t aH7_R11 = IPAddr("134.207.136.94");		//Air Ops Center
	IPAddr_t aaoc2 = IPAddr("192.168.16.2");			//Air Ops Center
	IPAddr_t aH19_LAN = IPAddr("192.168.16.1");			//Air Ops Center
	IPAddr_t aH19_R11 = IPAddr("134.207.136.194");		//Air Ops Center
	IPAddr_t aR11_SAT2c = IPAddr("134.207.136.90");		//Air Ops Center
	IPAddr_t aR11_H19 = IPAddr("134.207.136.193");		//Air Ops Center
	IPAddr_t aR11_H7 = IPAddr("134.207.136.93");		//Air Ops Center
	IPAddr_t aR11_R7 = IPAddr("134.207.136.50");		//Air Ops Center
	IPAddr_t aap = IPAddr("192.168.17.2");				//Air Platform
	IPAddr_t aH15_LAN = IPAddr("192.168.17.1");			//Air Platform
	IPAddr_t aH15_R21 = IPAddr("134.207.136.134");		//Air Platform
	IPAddr_t aR21_H15 = IPAddr("134.207.136.133");		//Air Platform
	IPAddr_t aR21_R20 = IPAddr("134.207.136.142");		//Air Platform
	
	//SSC-CH - Charleston, SC network elements
	IPAddr_t aisufist = IPAddr("192.169.18.1");			//CONUS ISU/FIST
	IPAddr_t aR34_LAN = IPAddr("192.169.18.2");			//CONUS ISU/FIST
	IPAddr_t aR34_H1 = IPAddr("192.168.18.2");			//CONUS ISU/FIST
	IPAddr_t aH1_R34 = IPAddr("192.168.18.1");			//CONUS ISU/FIST
	IPAddr_t aH1_R1 = IPAddr("134.207.136.2");			//CONUS ISU/FIST
	IPAddr_t aR1_H1 = IPAddr("134.207.136.1");			//CONUS ISU/FIST
	IPAddr_t aR1_R2 = IPAddr("134.207.136.5");			//CONUS ISU/FIST
	IPAddr_t aship1g = IPAddr("192.169.27.1");			//DDG (Ship 1)
	IPAddr_t aR38_LAN = IPAddr("192.169.27.2");			//DDG (Ship 1)
	IPAddr_t aR38_Hx = IPAddr("192.168.27.2");			//DDG (Ship 1)
	IPAddr_t aHx_R38 = IPAddr("192.168.27.1");			//DDG (Ship 1)
	IPAddr_t aHx_R37 = IPAddr("192.168.26.1");			//DDG (Ship 1)
	IPAddr_t aship1r = IPAddr("192.169.25.1");			//DDG (Ship 1)
	IPAddr_t aR39_LAN = IPAddr("192.169.25.2");			//DDG (Ship 1)
	IPAddr_t aR39_R37 = IPAddr("192.168.25.2");			//DDG (Ship 1)
	IPAddr_t aR37_Hx = IPAddr("192.168.26.2");			//DDG (Ship 1)
	IPAddr_t aR37_R39 = IPAddr("192.168.25.1");			//DDG (Ship 1)
	IPAddr_t aR37_SAT4 = IPAddr("192.168.24.2");		//DDG (Ship 1)
	IPAddr_t aSAT4_R37 = IPAddr("192.168.24.1");		//Satellite 4
	IPAddr_t aSAT4_R35 = IPAddr("192.168.23.1");		//Satellite 4
	IPAddr_t aR35_SAT4 = IPAddr("192.168.23.2");		//NCTAMS
	IPAddr_t aR35_H11 = IPAddr("192.168.19.2");			//NCTAMS
	IPAddr_t aH11_R35 = IPAddr("192.168.19.1");			//NCTAMS
	IPAddr_t aH11_R16 = IPAddr("134.207.136.114");		//NCTAMS
	IPAddr_t aR36_H12 = IPAddr("192.168.20.2");			//NCTAMS
	IPAddr_t aH12_R36 = IPAddr("192.168.20.1");			//NCTAMS
	IPAddr_t aH12_R16 = IPAddr("134.207.136.118");		//NCTAMS
	IPAddr_t aR16_H11 = IPAddr("134.207.136.113");		//NCTAMS
	IPAddr_t aR16_H12 = IPAddr("134.207.136.117");		//NCTAMS
	IPAddr_t aR16_SAT3 = IPAddr("134.207.136.121");		//NCTAMS
	IPAddr_t aR16_R8 = IPAddr("134.207.136.42");		//NCTAMS
	IPAddr_t aR36_Hy = IPAddr("192.168.21.2");			//NCTAMS
	IPAddr_t aHy_R36 = IPAddr("192.168.21.1");			//NCTAMS
	IPAddr_t aHy_R35 = IPAddr("192.168.22.1");			//NCTAMS
	IPAddr_t aR35_Hy = IPAddr("192.168.22.2");			//NCTAMS
	
	/******************************JRAE 04-4 IP Address Initialization******************************/
		
	
	/******************************JRAE 04-4 Network Node Declarations******************************/
	
	//PEO C3T - Ft. Hood, TX network elements
	Node* brigade;			//Brigade	N0
	Node* R40;				//Brigade	N1
	TACLANENode* H8;		//Brigade	N2
	Node* R41;				//Brigade	N3

	//JITC - Ft. Huachuca, AZ network elements
	Node* soc;				//Spec. Ops. Command	N4
	Node* R23;				//Spec. Ops. Command	N5
	TACLANENode* H5;		//Spec. Ops. Command	N6
	
	//AFCA - Scott AFB, IL network elements
	Node* fs1c;				//Fighter Squadron 1 Commander	N7
	Node* R22;				//Fighter Squadron 1 Commander	N8
	
	//BCBL - Ft. Gordon, GA network elements
	Node* batallion;		//Batallion		N9
	Node* R25a;				//Batallion		N10
	TACLANENode* H9;		//Batallion		N11
	Node* R14;				//Batallion		N12
	Node* platoon;			//Platoon		N13
	Node* R26;				//Platoon		N14
	TACLANENode* H10;		//Platoon		N15
	Node* R13;				//Platoon		N16
	
	//NRL - Washington, D.C. network elements
	Node* R12;				//Teleport 2		N17
	Node* uav;				//UAV				N18
	Node* R27;				//UAV				N19
	TACLANENode* H3;		//UAV				N20
	Node* R6;				//UAV				N21
	Node* R4;				//Teleport 1		N22
	Node* R43;				//GIG-BE POP1		N23
	TACLANENode* H20;		//GIG-BE POP1		N24
	Node* R42;				//GIG-BE POP1		N25
	Node* R2;				//GIG-BE POP1		N26
	Node* aorc;				//AOR Commander		N27
	Node* R28;				//AOR Commander		N28
	TACLANENode* H18;		//AOR Commander		N29
	Node* R25b;				//AOR Commander		N30
	
	//MIT-LL - Lexington, MA network elements
	Node* R8;				//GIG-BE POP3	N31
	SatelliteNode* SAT1a;	//Satellite 1	N32
	SatelliteNode* SAT1b;	//Satellite 1	N33
	SatelliteNode* SAT1c;	//Satellite 1	N34
	SatelliteNode* SAT2a;	//Satellite 2	N35
	SatelliteNode* SAT2b;	//Satellite 2	N36
	SatelliteNode* SAT2c;	//Satellite 2	N37
	SatelliteNode* SAT2d;	//Satellite 2	N38
	SatelliteNode* SAT3;	//Satellite 3	N39
	
	//DISA network elements
	Node* R7;				//GIG-BE POP2	N40
	
	//JTEO - McLean, VA network elements
	Node* fs2c;				//Fighter Squadron 2 Commander		N41
	Node* R29;				//Fighter Squadron 2 Commander		N42
	TACLANENode* H2;		//Fighter Squadron 2 Commander		N43
	Node* R5;				//Fighter Squadron 2 Commander		N44
	Node* uavcrew;			//UAV Crew							N45
	Node* R30;				//UAV Crew							N46
	TACLANENode* H4;		//UAV Crew							N47
	Node* R3;				//UAV Crew							N48
	
	//SSC-SD - San Diego, CA network elements
	TACLANENode* H6;		//CJTF	N49
	Node* R10;				//CJTF	N50
	Node* jsotf;			//JSOTF	N51
	Node* R31;				//JSOTF	N52
	TACLANENode* H17;		//JSOTF	N53
	Node* R24;				//JSOTF	N54
	Node* ship2g;			//ESG Commander (Ship 2)	N55
	Node* R32;				//ESG Commander (Ship 2)	N56
	TACLANENode* H14;		//ESG Commander (Ship 2)	N57
	Node* ship2r;			//ESG Commander (Ship 2)	N58
	Node* R33;				//ESG Commander (Ship 2)	N59
	TACLANENode* H13;		//ESG Commander (Ship 2)	N60
	Node* R17;				//ESG Commander (Ship 2)	N61
	Node* ship2tl;			//ESG Commander (Ship 2)	N62
	TACLANENode* H16;		//ESG Commander (Ship 2)	N63
	Node* R20;				//JTRS Cloud	N64
	Node* R19;				//JTRS Cloud	N65
	Node* R18;				//JTRS Cloud	N66
	Node* R15;				//JTRS Cloud	N67
	Node* JTRS;				//JTRS Router	N68

	//CPSG - Brooks AFB, SC network elements
	Node* aoc1;				//Air Ops Center	N69
	TACLANENode* H7;		//Air Ops Center	N70
	Node* aoc2;				//Air Ops Center	N71
	TACLANENode* H19;		//Air Ops Center	N72
	Node* R11;				//Air Ops Center	N73
	Node* ap;				//Air Platform		N74
	TACLANENode* H15;		//Air Platform		N75
	Node* R21;				//Air Platform		N76
	
	//SSC-CH - Charleston, SC network elements
	Node* isufist;			//CONUS ISU/FIST	N77
	Node* R34;				//CONUS ISU/FIST	N78
	TACLANENode* H1;		//CONUS ISU/FIST	N79
	Node* R1;				//CONUS ISU/FIST	N80
	Node* ship1g;			//DDG (Ship 1)		N81
	Node* R38;				//DDG (Ship 1)		N82
	TACLANENode* Hx;		//DDG (Ship 1)		N83
	Node* ship1r;			//DDG (Ship 1)		N84
	Node* R39;				//DDG (Ship 1)		N85
	Node* R37;				//DDG (Ship 1)		N86
	SatelliteNode* SAT4;	//Satellite 4		N87
	Node* R35;				//NCTAMS	N88
	TACLANENode* H11;		//NCTAMS	N89
	Node* R36;				//NCTAMS	N90
	TACLANENode* H12;		//NCTAMS	N91
	Node* R16;				//NCTAMS	N92
	TACLANENode* Hy;		//NCTAMS	N93
	
	/******************************JRAE 04-4 Network Node Declarations******************************/

	
	/*****************************JRAE 04-4 Network Node Initialization*****************************/

	//PEO C3T - Ft. Hood, TX network elements
	brigade = new Node(); //Brigade
        brigade->UserInformation(new string("Brigade Host"));
	R40 = new Node();    //Brigade
        R40->UserInformation(new string("BrigadeR40"));
	H8 = new TACLANENode(aH8_R40, aH8_R41, RB); //Brigade
        H8->UserInformation(new string("BrigadeH8"));
	R41 = new Node();  //Brigade
        R41->UserInformation(new string("BrigadeR41"));
	
	//JITC - Ft. Huachuca, AZ network elements
	soc = new Node(); //Spec. Ops. Command
        soc->UserInformation(new string("SocHost"));
	R23 = new Node(); //Spec. Ops. Command
        R23->UserInformation(new string("SOCR23"));
	H5 = new TACLANENode(aH5_R23, aH5_R8, RB);//Spec. Ops. Command
        H5->UserInformation(new string("SOCH5"));
	
	//AFCA - Scott AFB, IL network elements
	fs1c = new Node();//Fighter Squadron 1 Commander
        fs1c->UserInformation(new string("Fs1cHost"));
	R22 = new Node(); //Fighter Squadron 1 Commander
        R22->UserInformation(new string("Fs1cR22"));
	
	//BCBL - Ft. Gordon, GA network elements
	batallion = new Node(); //Batallion
        batallion->UserInformation(new string("BatallionHost"));
	R25a = new Node(); //Batallion
        R25a->UserInformation(new string("BatallionR25a"));
	H9 = new TACLANENode(aH9_R25a, aH9_R14, RB); //Batallion
        H9->UserInformation(new string("BatallionH9"));
	R14 = new Node(); //Batallion
        R14->UserInformation(new string("BatallionR14"));

        // Platoon
	platoon = new Node(); //Platoon
        platoon->UserInformation(new string("PlatoonHost"));
	R26 = new Node(); //Platoon
        R26->UserInformation(new string("PlatoonR26"));
	H10 = new TACLANENode(aH10_R26, aH10_R13, RB);	//Platoon
        H10->UserInformation(new string("PlatoonH10"));
	R13 = new Node();//Platoon
        R13->UserInformation(new string("PlatoonR13"));
	
	//NRL - Washington, D.C. network elements
	R12 = new Node();//Teleport 2
        R12->UserInformation(new string("Teleport2Host"));

	uav = new Node(); //UAV
        uav->UserInformation(new string("UAV"));
	R27 = new Node(); //UAV
        R27->UserInformation(new string("UAVR27"));
	H3 = new TACLANENode(aH3_R27, aH3_R6, RB); //UAV
        H3->UserInformation(new string("UAVH3"));
	R6 = new Node(); //UAV
        R6->UserInformation(new string("UAVR6"));
	R4 = new Node(); //Teleport 1
        R4->UserInformation(new string("UAVR4"));

	R43 = new Node();//GIG-BE POP1
        R43->UserInformation(new string("GIG-BE-POP1Host"));
	H20 = new TACLANENode(aH20_R43, aH20_R42, RB);//GIG-BE POP1
        H20->UserInformation(new string("GIG-BE-POP1-H20"));
	R42 = new Node(); //GIG-BE POP1
        R42->UserInformation(new string("GIG-BE-POP1-R42"));
	R2 = new Node(); //GIG-BE POP1
        R2->UserInformation(new string("GIG-BE-POP1-R2"));

	aorc = new Node(); //AOR Commander
        aorc->UserInformation(new string("AORCmdrHost"));
	R28 = new Node();  //AOR Commander
        R28->UserInformation(new string("AORCmdrR28"));
	H18 = new TACLANENode(aH18_R28, aH18_R25b, RB); //AOR Commander
        H18->UserInformation(new string("AORCmdrH18"));
	R25b = new Node(); //AOR Commander
        R25b->UserInformation(new string("AORCmdrR25b"));
	
	//MIT-LL - Lexington, MA network elements
	R8 = new Node(); //GIG-BE POP3
        R8->UserInformation(new string("MIT-R8"));
	SAT1a = new SatelliteNode();//Satellite 1
        SAT1a->UserInformation(new string("MIT-Sat1A"));
	SAT1b = new SatelliteNode();//Satellite 1
        SAT1b->UserInformation(new string("MIT-Sat1B"));
	SAT1c = new SatelliteNode();//Satellite 1
        SAT1c->UserInformation(new string("MIT-Sat1C"));
	SAT2a = new SatelliteNode();//Satellite 2
        SAT2a->UserInformation(new string("MIT-Sat2A"));
	SAT2b = new SatelliteNode();//Satellite 2
        SAT2b->UserInformation(new string("MIT-Sat2B"));
	SAT2c = new SatelliteNode();//Satellite 2
        SAT2c->UserInformation(new string("MIT-Sat2C"));
	SAT2d = new SatelliteNode();//Satellite 2
        SAT2d->UserInformation(new string("MIT-Sat2D"));
	SAT3  = new SatelliteNode(); //Satellite 3
        SAT3->UserInformation(new string("MIT-Sat3"));
	
	//DISA network elements
	R7 = new Node(); //GIG-BE POP2
        R7->UserInformation(new string("GIG-BE-POP2Host"));
	
	//JTEO - McLean, VA network elements
	fs2c = new Node(); //Fighter Squadron 2 Commander
        fs2c->UserInformation(new string("FS2CHost"));
	R29 = new Node();  //Fighter Squadron 2 Commander
        R29->UserInformation(new string("FS2CR29"));
	H2 = new TACLANENode(aH2_R29, aH2_R5, RB);//Fighter Squadron 2 Cmdr
        H2->UserInformation(new string("FS2CH2"));
	R5 = new Node();   //Fighter Squadron 2 Commander
        R5->UserInformation(new string("FS2CR5"));

	uavcrew = new Node(); //UAV Crew
        uavcrew->UserInformation(new string("UAVCrew-Host"));
	R30 = new Node();     //UAV Crew
        R30->UserInformation(new string("UAVCrew-R30"));
	H4 = new TACLANENode(aH4_R30, aH4_R3, RB); //UAV Crew
        H4->UserInformation(new string("UAVCrew-H4"));
	R3 = new Node();      //UAV Crew
        R3->UserInformation(new string("UAVCrew-R3"));
	
	//SSC-SD - San Diego, CA network elements
	H6 = new TACLANENode(aH6_R22, aH6_R10, RB); //CJTF
        H6->UserInformation(new string("SSC-SD-H6"));
	R10 = new Node();   //CJTF
        R10->UserInformation(new string("SSC-SD-R10"));

	jsotf = new Node(); //JSOTF
        jsotf->UserInformation(new string("JSOTF-Host"));
	R31 = new Node();   //JSOTF
        R31->UserInformation(new string("JSOTF-R31"));
	H17 = new TACLANENode(aH17_R31, aH17_R24, RB); //JSOTF
        H17->UserInformation(new string("JSOTF-H17"));
	R24 = new Node();   //JSOTF
        R24->UserInformation(new string("JSOTF-R24"));

	ship2g = new Node(); //ESG Commander (Ship 2)
        ship2g->UserInformation(new string("Ship2-Host"));
	R32 = new Node();    //ESG Commander (Ship 2)
        R32->UserInformation(new string("Ship2-R32"));
	H14 = new TACLANENode(aH14_R32, aH14_R17, GB); //ESG Commander (Ship 2)
        H14->UserInformation(new string("Ship2-H14"));
	ship2r = new Node(); //ESG Commander (Ship 2)
        ship2r->UserInformation(new string("Ship2r-Host"));
	R33 = new Node();    //ESG Commander (Ship 2)
        R33->UserInformation(new string("Ship2r->R33"));
	H13 = new TACLANENode(aH13_R33, aH13_R17, RB); //ESG Commander (Ship 2)
        H13->UserInformation(new string("Ship2r-H13"));
	ship2tl = new Node();//ESG Commander (Ship 2)
        ship2tl->UserInformation(new string("ship2tl-Host"));
	H16 = new TACLANENode(aH16_LAN, aH16_R17, RB); //ESG Commander (Ship 2)
        H16->UserInformation(new string("ship2tl->H16"));
	R17 = new Node();    //ESG Commander (Ship 2)
        R17->UserInformation(new string("ship2tl->R17"));

        // JTRS
	R20 = new Node();    //JTRS Cloud
        R20->UserInformation(new string("JTRS-R20"));
	R19 = new Node();    //JTRS Cloud
        R19->UserInformation(new string("JTRS-R19"));
	R18 = new Node();    //JTRS Cloud
        R18->UserInformation(new string("JTRS-R18"));
	R15 = new Node();    //JTRS Cloud
        R15->UserInformation(new string("JTRS-R15"));
	JTRS = new Node();   //JTRS Router
        JTRS->UserInformation(new string("JTRS-Router"));
	
	//CPSG - Brooks AFB, SC network elements
	aoc1 = new Node(); //Air Ops Center
        aoc1->UserInformation(new string("AOC1-Host"));
	H7 = new TACLANENode(aH7_LAN, aH7_R11, RB);    //Air Ops Center
        H7->UserInformation(new string("AOC1->H7"));

	aoc2 = new Node(); //Air Ops Center
        aoc2->UserInformation(new string("AOC2-Host"));
	H19 = new TACLANENode(aH19_LAN, aH19_R11, RB); //Air Ops Center
        H19->UserInformation(new string("AOC2-H19"));
	R11 = new Node();  //Air Ops Center
        R11->UserInformation(new string("AOC2-R11"));

	ap = new Node();  //Air Platform
        ap->UserInformation(new string("AP-Host"));
	H15 = new TACLANENode(aH15_LAN, aH15_R21, RB);//Air Platform
        H15->UserInformation(new string("AP-H15"));
	R21 = new Node(); //Air Platform
        R21->UserInformation(new string("AP-R21"));
	
	//SSC-CH - Charleston, SC network elements
	isufist = new Node(); //CONUS ISU/FIST
        isufist->UserInformation(new string("CONUS-ISU"));
	R34 = new Node();     //CONUS ISU/FIST
        R34->UserInformation(new string("CONUS-ISU-R34"));
	H1 = new TACLANENode(aH1_R34, aH1_R1, RB); //CONUS ISU/FIST
        H1->UserInformation(new string("CONUS-ISU-H1"));
	R1 = new Node();      //CONUS ISU/FIST
        R1->UserInformation(new string("CONUS-ISU-R1"));

	ship1g = new Node();  //DDG (Ship 1)
        ship1g->UserInformation(new string("Ship1g-Host"));
	R38 = new Node();    //DDG (Ship 1)
        R38->UserInformation(new string("Ship1g-R38"));
	Hx = new TACLANENode(aHx_R38, aHx_R37, RG); //DDG (Ship 1)
        Hx->UserInformation(new string("Ship1g-Hx"));

	ship1r = new Node();//DDG (Ship 1)
        ship1r->UserInformation(new string("Ship1r-Host"));
	R39 = new Node();   //DDG (Ship 1)
        R39->UserInformation(new string("Ship1r-R39"));
	R37 = new Node();   //DDG (Ship 1)
        R37->UserInformation(new string("Ship1r-R37"));

	SAT4 = new SatelliteNode(); //Satellite 4
        SAT4->UserInformation(new string("SAT4"));

	R35 = new Node();   //NCTAMS
        R35->UserInformation(new string("NCTAMS"));
	H11 = new TACLANENode(aH11_R35, aH11_R16, RB); //NCTAMS
        H11->UserInformation(new string("NCTAMS-H11"));
	R36 = new Node();  //NCTAMS
        R36->UserInformation(new string("NCTAMS-R36"));
	H12 = new TACLANENode(aH12_R36, aH12_R16, GB);//NCTAMS
        H12->UserInformation(new string("NCTAMS-H12"));
	R16 = new Node();  //NCTAMS
        R16->UserInformation(new string("NCTAMS-R16"));
	Hy = new TACLANENode(aHy_R36, aHy_R35, RG);  //NCTAMS
        Hy->UserInformation(new string("NCTAMS-Hy"));
	
	/*****************************JRAE 04-4 Network Node Initialization*****************************/

	
	/*********************************JRAE 04-4 Link Initialization*********************************/
	
	//Linkp2p tl_link(Rate("10Mb"), Time("1ms"));
	//Linkp2p lan_link(Rate("100Mb"), Time("1ms"));
	//Linkp2p gigcore_link(Rate("1Gb"), Time("1ms"));
	//Linkp2p gigedge_link(Rate("1Gb"), Time("1ms"));
	//Linkp2p jtrs_link(Rate("100Mb"), Time("1ms"));
	//Linkp2p sat_link1(Rate("45Mb"), Time("250ms"));
	//Linkp2p sat_link2(Rate("1.5Mb"), Time("350ms"));
        // Use 10x slower links for animation
	Linkp2p tl_link(Rate("1Mb"), Time("1ms"));
	Linkp2p lan_link(Rate("10Mb"), Time("1ms"));
	Linkp2p gigcore_link(Rate("100Mb"), Time("1ms"));
	Linkp2p gigedge_link(Rate("100Mb"), Time("1ms"));
	Linkp2p jtrs_link(Rate("10Mb"), Time("1ms"));
	Linkp2p sat_link1(Rate("4.5Mb"), Time("250ms"));
	Linkp2p sat_link2(Rate("150Kb"), Time("350ms"));
	sat_link1.BitErrorRate(1e-6); // Satellite bit error rate
	sat_link2.BitErrorRate(1e-6); // Satellite bit error rate

	//PEO C3T - Ft. Hood, TX links
	brigade->AddDuplexLink(R40, lan_link, abrigade, Mask(32), aR40_LAN, Mask(32));		//Brigade
	R40->AddDuplexLink(H8, tl_link, aR40_H8, Mask(32), aH8_R40, Mask(32));				//Brigade
	R41->AddDuplexLink(H8, tl_link, aR41_H8, Mask(32), aH8_R41, Mask(32));				//Brigade
	R41->AddDuplexLink(SAT2b, sat_link1, aR41_SAT2b, Mask(32), aSAT2b_R41, Mask(32));	//Brigade
	
	//JITC - Ft. Huachuca, AZ links
	soc->AddDuplexLink(R23, lan_link, asoc, Mask(32), aR23_LAN, Mask(32));				//Spec. Ops. Command
	R23->AddDuplexLink(H5, tl_link, aR23_H5, Mask(32), aH5_R23, Mask(32));				//Spec. Ops. Command
	R8->AddDuplexLink(H5, gigedge_link, aR8_H5, Mask(32), aH5_R8, Mask(32));			//Spec. Ops. Command
	
	//AFCA - Scott AFB, IL links
	fs1c->AddDuplexLink(R22, lan_link, afs1c, Mask(32), aR22_LAN, Mask(32));			//FighterSquadron 1 Commander
	R22->AddDuplexLink(H6, tl_link, aR22_H6, Mask(32), aH6_R22, Mask(32));				//FighterSquadron 1 Commander
	
	//BCBL - Ft. Gordon, GA links
	batallion->AddDuplexLink(R25a, lan_link, abatallion, Mask(32), aR25_LAN, Mask(32));	//Batallion
	R25a->AddDuplexLink(H9, tl_link, aR25a_H9, Mask(32), aH9_R25a, Mask(32));			//Batallion
	R14->AddDuplexLink(H9, tl_link, aR14_H9, Mask(32), aH9_R14, Mask(32));				//Batallion
	R14->AddDuplexLink(SAT2a, sat_link1, aR14_SAT2a, Mask(32), aSAT2a_R14, Mask(32));	//Batallion
	R14->AddDuplexLink(R15, jtrs_link, aR14_R15, Mask(32), aR15_R14, Mask(32));			//Batallion
	platoon->AddDuplexLink(R26, lan_link, aplatoon, Mask(32), aR26_LAN, Mask(32));		//Platoon
	R26->AddDuplexLink(H10, tl_link, aR26_H10, Mask(32), aH10_R26, Mask(32));			//Platoon
	R13->AddDuplexLink(H10, tl_link, aR13_H10, Mask(32), aH10_R13, Mask(32));			//Platoon
	R13->AddDuplexLink(R19, jtrs_link, aR13_R19, Mask(32), aR19_R13, Mask(32));			//Platoon
	
	//NRL - Washington, D.C. links
	R12->AddDuplexLink(R7, gigedge_link, aR12_R7, Mask(32), aR7_R12, Mask(32));			//Teleport 2
	R12->AddDuplexLink(SAT2d, sat_link1, aR12_SAT2d, Mask(32), aSAT2d_R12, Mask(32));	//Teleport 2
	R12->AddDuplexLink(SAT2c, sat_link1, aR12_SAT2c, Mask(32), aSAT2c_R12, Mask(32));	//Teleport 2
	R12->AddDuplexLink(SAT2b, sat_link1, aR12_SAT2b, Mask(32), aSAT2b_R12, Mask(32));	//Teleport 2
	R12->AddDuplexLink(SAT2a, sat_link1, aR12_SAT2a, Mask(32), aSAT2a_R12, Mask(32));	//Teleport 2
	uav->AddDuplexLink(R27, lan_link, auav, Mask(32), aR27_LAN, Mask(32));				//UAV
	R27->AddDuplexLink(H3, tl_link, aR27_H3, Mask(32), aH3_R27, Mask(32));				//UAV
	R6->AddDuplexLink(H3, tl_link, aR6_H3, Mask(32), aH3_R6, Mask(32));					//UAV
	R6->AddDuplexLink(SAT1a, sat_link1, aR6_SAT1a, Mask(32), aSAT1a_R6, Mask(32));		//UAV
	R4->AddDuplexLink(SAT1c, sat_link1, aR4_SAT1c, Mask(32), aSAT1c_R4, Mask(32));		//Teleport 1
	R4->AddDuplexLink(SAT1b, sat_link1, aR4_SAT1b, Mask(32), aSAT1b_R4, Mask(32));		//Teleport 1
	R4->AddDuplexLink(SAT1a, sat_link1, aR4_SAT1a, Mask(32), aSAT1a_R4, Mask(32));		//Teleport 1
	R4->AddDuplexLink(R2, gigedge_link, aR4_R2, Mask(32), aR2_R4, Mask(32));			//Teleport 1
	R43->AddDuplexLink(H20, tl_link, aR43_H20, Mask(32), aH20_R43, Mask(32));			//GIG-BE POP1
	R42->AddDuplexLink(H20, tl_link, aR42_H20, Mask(32), aH20_R42, Mask(32));			//GIG-BE POP1
	R42->AddDuplexLink(R2, gigedge_link, aR42_R2, Mask(32), aR2_R42, Mask(32));			//GIG-BE POP1
	aorc->AddDuplexLink(R28, lan_link, aaorc, Mask(32), aR28_LAN, Mask(32));			//AOR Commander
	R28->AddDuplexLink(H18, tl_link, aR28_H18, Mask(32), aH18_R28, Mask(32));			//AOR Commander
	R25b->AddDuplexLink(H18, tl_link, aR25b_H18, Mask(32), aH18_R25b, Mask(32));		//AOR Commander
	R25b->AddDuplexLink(R2, gigedge_link, aR25b_R2, Mask(32), aR2_R25b, Mask(32));		//AOR Commander
	
	//MIT-LL - Lexington, MA links
	R8->AddDuplexLink(R2, gigcore_link, aR8_R2, Mask(32), aR2_R8, Mask(32));			//GIG-BE POP3
	R8->AddDuplexLink(R7, gigcore_link, aR8_R7, Mask(32), aR7_R8, Mask(32));			//GIG-BE POP3
	
	//DISA links
	R7->AddDuplexLink(R2, gigcore_link, aR7_R2, Mask(32), aR2_R7, Mask(32));			//GIG-BE POP2
	
	//JTEO - McLean, VA links
	fs2c->AddDuplexLink(R29, lan_link, afs2c, Mask(32), aR29_LAN, Mask(32));			//Fighter Squadron 2 Commander
	R29->AddDuplexLink(H2, tl_link, aR29_H2, Mask(32), aH2_R29, Mask(32));				//Fighter Squadron 2 Commander
	R5->AddDuplexLink(H2, tl_link, aR5_H2, Mask(32), aH2_R5, Mask(32));					//Fighter Squadron 2 Commander
	R5->AddDuplexLink(SAT1b, sat_link1, aR5_SAT1b, Mask(32), aSAT1b_R5, Mask(32));		//Fighter Squadron 2 Commander
	uavcrew->AddDuplexLink(R30, lan_link, auavcrew, Mask(32), aR30_LAN, Mask(32));		//UAV Crew
	R30->AddDuplexLink(H4, tl_link, aR30_H4, Mask(32), aH4_R30, Mask(32));				//UAV Crew
	R3->AddDuplexLink(H4, tl_link, aR3_H4, Mask(32), aH4_R3, Mask(32));					//UAV Crew
	R3->AddDuplexLink(R2, gigedge_link, aR3_R2, Mask(32), aR2_R3, Mask(32));			//UAV Crew
		
	//SSC-SD - San Diego, CA links
	R10->AddDuplexLink(H6, tl_link, aR10_H6, Mask(32), aH6_R10, Mask(32));				//CJTF
	R10->AddDuplexLink(R7, gigedge_link, aR10_R7, Mask(32), aR7_R10, Mask(32));			//CJTF
	R10->AddDuplexLink(SAT2d, sat_link1, aR10_SAT2d, Mask(32), aSAT2d_R10, Mask(32));	//CJTF
	jsotf->AddDuplexLink(R31, lan_link, ajsotf, Mask(32), aR31_LAN, Mask(32));			//JSOTF
	R31->AddDuplexLink(H17, tl_link, aR31_H17, Mask(32), aH17_R31, Mask(32));			//JSOTF
	R24->AddDuplexLink(H17, tl_link, aR24_H17, Mask(32), aH17_R24, Mask(32));			//JSOTF
	R24->AddDuplexLink(SAT1c, sat_link1, aR24_SAT1c, Mask(32), aSAT1c_R24, Mask(32));	//JSOTF
	ship2g->AddDuplexLink(R32, lan_link, aship2g, Mask(32), aR32_LAN, Mask(32));		//ESG Commander (Ship 2)
	R32->AddDuplexLink(H14, tl_link, aR32_H14, Mask(32), aH14_R32, Mask(32));			//ESG Commander (Ship 2)
	ship2r->AddDuplexLink(R33, lan_link, aship2r, Mask(32), aR33_LAN, Mask(32));		//ESG Commander (Ship 2)
	R33->AddDuplexLink(H13, tl_link, aR33_H13, Mask(32), aH13_R33, Mask(32));			//ESG Commander (Ship 2)
	ship2tl->AddDuplexLink(H16, lan_link, aship2tl, Mask(32), aH16_LAN, Mask(32));		//ESG Commander (Ship 2)
	R17->AddDuplexLink(H14, tl_link, aR17_H14, Mask(32), aH14_R17, Mask(32));			//ESG Commander (Ship 2)
	R17->AddDuplexLink(H13, tl_link, aR17_H13, Mask(32), aH13_R17, Mask(32));			//ESG Commander (Ship 2)
	R17->AddDuplexLink(H16, tl_link, aR17_H16, Mask(32), aH16_R17, Mask(32));			//ESG Commander (Ship 2)
	R17->AddDuplexLink(SAT3, sat_link1, aR17_SAT3, Mask(32), aSAT3_R17, Mask(32));		//ESG Commander (Ship 2)
	R17->AddDuplexLink(R18, jtrs_link, aR17_R18, Mask(32), aR18_R17, Mask(32));			//ESG Commander (Ship 2)
	JTRS->AddDuplexLink(R20, jtrs_link, aJTRS_R20, Mask(32), aR20_JTRS, Mask(32));		//JTRS Cloud
	JTRS->AddDuplexLink(R19, jtrs_link, aJTRS_R19, Mask(32), aR19_JTRS, Mask(32));		//JTRS Cloud
	JTRS->AddDuplexLink(R18, jtrs_link, aJTRS_R18, Mask(32), aR18_JTRS, Mask(32));		//JTRS Cloud
	JTRS->AddDuplexLink(R15, jtrs_link, aJTRS_R15, Mask(32), aR15_JTRS, Mask(32));		//JTRS Cloud
	
	//CPSG - Brooks AFB, SC links
	aoc1->AddDuplexLink(H7, lan_link, aaoc1, Mask(32), aH7_LAN, Mask(32));				//Air Ops Center
	R11->AddDuplexLink(H7, tl_link, aR11_H7, Mask(32), aH7_R11, Mask(32));				//Air Ops Center
	aoc2->AddDuplexLink(H19, lan_link, aaoc2, Mask(32), aH19_LAN, Mask(32));			//Air Ops Center
	R11->AddDuplexLink(H19, tl_link, aR11_H19, Mask(32), aH19_R11, Mask(32));			//Air Ops Center
	R11->AddDuplexLink(SAT2c, sat_link1, aR11_SAT2c, Mask(32), aSAT2c_R11, Mask(32));	//Air Ops Center
	R11->AddDuplexLink(R7, gigedge_link, aR11_R7, Mask(32), aR7_R11, Mask(32));			//Air Ops Center
	ap->AddDuplexLink(H15, lan_link, aap, Mask(32), aH15_LAN, Mask(32));				//Air Platform
	R21->AddDuplexLink(H15, tl_link, aR21_H15, Mask(32), aH15_R21, Mask(32));			//Air Platform
	R21->AddDuplexLink(R20, jtrs_link, aR21_R20, Mask(32), aR20_R21, Mask(32));			//Air Platform
	
	//SSC-CH - Charleston, SC links
	isufist->AddDuplexLink(R34, lan_link, aisufist, Mask(32), aR34_LAN, Mask(32));		//CONUS ISU/FIST
	R34->AddDuplexLink(H1, tl_link, aR34_H1, Mask(32), aH1_R34, Mask(32));				//CONUS ISU/FIST
	R1->AddDuplexLink(H1, tl_link, aR1_H1, Mask(32), aH1_R1, Mask(32));					//CONUS ISU/FIST
	R1->AddDuplexLink(R2, gigedge_link, aR1_R2, Mask(32), aR2_R1, Mask(32));			//CONUS ISU/FIST
	ship1g->AddDuplexLink(R38, lan_link, aship1g, Mask(32), aR38_LAN, Mask(32));		//DDG (Ship 1)
	R38->AddDuplexLink(Hx, tl_link, aR38_Hx, Mask(32), aHx_R38, Mask(32));				//DDG (Ship 1)
	ship1r->AddDuplexLink(R39, lan_link, aship1r, Mask(32), aR39_LAN, Mask(32));		//DDG (Ship 1)
	R37->AddDuplexLink(Hx, tl_link, aR37_Hx, Mask(32), aHx_R37, Mask(32));				//DDG (Ship 1)
	R37->AddDuplexLink(R39, lan_link, aR37_R39, Mask(32), aR39_R37, Mask(32));			//DDG (Ship 1)
	R37->AddDuplexLink(SAT4, sat_link2, aR37_SAT4, Mask(32), aSAT4_R37, Mask(32));		//DDG (Ship 1)
	R35->AddDuplexLink(SAT4, sat_link2, aR35_SAT4, Mask(32), aSAT4_R35, Mask(32));		//NCTAMS
	R35->AddDuplexLink(H11, tl_link, aR35_H11, Mask(32), aH11_R35, Mask(32));			//NCTAMS
	R36->AddDuplexLink(H12, tl_link, aR36_H12, Mask(32), aH12_R36, Mask(32));			//NCTAMS
	R16->AddDuplexLink(H11, tl_link, aR16_H11, Mask(32), aH11_R16, Mask(32));			//NCTAMS
	R16->AddDuplexLink(H12, tl_link, aR16_H12, Mask(32), aH12_R16, Mask(32));			//NCTAMS
	R16->AddDuplexLink(SAT3, sat_link1, aR16_SAT3, Mask(32), aSAT3_R16, Mask(32));		//NCTAMS
	R16->AddDuplexLink(R8, gigedge_link, aR16_R8, Mask(32), aR8_R16, Mask(32));			//NCTAMS
	R36->AddDuplexLink(Hy, tl_link, aR36_Hy, Mask(32), aHy_R36, Mask(32));				//NCTAMS
	R35->AddDuplexLink(Hy, tl_link, aR35_Hy, Mask(32), aHy_R35, Mask(32));				//NCTAMS
	
	/*********************************JRAE 04-4 Link Initialization*********************************/	
	
	
	/*********************************JRAE 04-4 Network Node Shapes*********************************/
	
	//PEO C3T - Ft. Hood, TX network elements
	brigade->CustomShapeImage(HostSmallImage());		//Brigade
	R40->CustomShapeImage(RedRouterImage());	//Brigade
	R41->CustomShapeImage(BlackRouterImage());	//Brigade
	
	//JITC - Ft. Huachuca, AZ network elements
	soc->CustomShapeImage(HostSmallImage());			//Spec. Ops. Command
	R23->CustomShapeImage(RedRouterImage());	//Spec. Ops. Command
	
	//AFCA - Scott AFB, IL network elements
	fs1c->CustomShapeImage(HostSmallImage());		//Fighter Squadron 1 Commander
	R22->CustomShapeImage(RedRouterImage());	//Fighter Squadron 1 Commander
	
	//BCBL - Ft. Gordon, GA network elements
	batallion->CustomShapeImage(HostSmallImage());	//Batallion
	R25a->CustomShapeImage(RedRouterImage());	//Batallion
	R14->CustomShapeImage(BlackRouterImage());	//Batallion
	platoon->CustomShapeImage(HostSmallImage());		//Platoon
	R26->CustomShapeImage(RedRouterImage());	//Platoon
	R13->CustomShapeImage(BlackRouterImage());	//Platoon
	
	//NRL - Washington, D.C. network elements
	R12->CustomShapeImage(BlackRouterImage());	//Teleport 2
	uav->CustomShapeImage(HostSmallImage());			//UAV
	R27->CustomShapeImage(RedRouterImage());	//UAV
	R6->CustomShapeImage(BlackRouterImage());	//UAV
	R4->CustomShapeImage(BlackRouterImage());	//Teleport 1
	R43->CustomShapeImage(RedRouterImage());	//GIG-BE POP1
	R42->CustomShapeImage(BlackRouterImage());	//GIG-BE POP1
	R2->CustomShapeImage(BlackRouterImage());	//GIG-BE POP1
	aorc->CustomShapeImage(HostSmallImage());		//AOR Commander
	R28->CustomShapeImage(RedRouterImage());	//AOR Commander
	R25b->CustomShapeImage(BlackRouterImage());	//AOR Commander
	
	//MIT-LL - Lexington, MA network elements
	R8->CustomShapeImage(BlackRouterImage());	//GIG-BE POP3
	
	//DISA network elements
	R7->CustomShapeImage(BlackRouterImage());	//GIG-BE POP2
	
	//JTEO - McLean, VA network elements
	fs2c->CustomShapeImage(HostSmallImage());		//Fighter Squadron 2 Commander
	R29->CustomShapeImage(RedRouterImage());	//Fighter Squadron 2 Commander
	R5->CustomShapeImage(BlackRouterImage());	//Fighter Squadron 2 Commander
	uavcrew->CustomShapeImage(HostSmallImage());		//UAV Crew
	R30->CustomShapeImage(RedRouterImage());	//UAV Crew
	R3->CustomShapeImage(BlackRouterImage());	//UAV Crew
	
	//SSC-SD - San Diego, CA network elements
	R10->CustomShapeImage(BlackRouterImage());	//CJTF
	jsotf->CustomShapeImage(HostSmallImage());		//JSOTF
	R31->CustomShapeImage(RedRouterImage());	//JSOTF
	R24->CustomShapeImage(BlackRouterImage());	//JSOTF
	ship2g->CustomShapeImage(HostSmallImage());		//ESG Commander (Ship 2)
	R32->CustomShapeImage(GreenRouterImage());	//ESG Commander (Ship 2)
	ship2r->CustomShapeImage(HostSmallImage());		//ESG Commander (Ship 2)
	R33->CustomShapeImage(RedRouterImage());	//ESG Commander (Ship 2)
	ship2tl->CustomShapeImage(HostSmallImage());		//ESG Commander (Ship 2)
	R17->CustomShapeImage(BlackRouterImage());	//ESG Commander (Ship 2)
	R20->CustomShapeImage(BlackRouterImage());	//JTRS Cloud
	R19->CustomShapeImage(BlackRouterImage());	//JTRS Cloud
	R18->CustomShapeImage(BlackRouterImage());	//JTRS Cloud
	R15->CustomShapeImage(BlackRouterImage());	//JTRS Cloud
	JTRS->CustomShapeImage(BlackRouterImage());	//JTRS Router
	
	//CPSG - Brooks AFB, SC network elements
	aoc1->CustomShapeImage(HostSmallImage());		//Air Ops Center
	aoc2->CustomShapeImage(HostSmallImage());		//Air Ops Center
	R11->CustomShapeImage(BlackRouterImage());	//Air Ops Center
	ap->CustomShapeImage(HostSmallImage());			//Air Platform
	R21->CustomShapeImage(BlackRouterImage());	//Air Platform
	
	//SSC-CH - Charleston, SC network elements
	isufist->CustomShapeImage(HostSmallImage());		//CONUS ISU/FIST
	R34->CustomShapeImage(RedRouterImage());	//CONUS ISU/FIST
	R1->CustomShapeImage(BlackRouterImage());	//CONUS ISU/FIST
	ship1g->CustomShapeImage(HostSmallImage());		//DDG (Ship 1)
	R38->CustomShapeImage(GreenRouterImage());	//DDG (Ship 1)
	ship1r->CustomShapeImage(HostSmallImage());		//DDG (Ship 1)
	R39->CustomShapeImage(RedRouterImage());	//DDG (Ship 1)
	R37->CustomShapeImage(RedRouterImage());	//DDG (Ship 1)
	R35->CustomShapeImage(RedRouterImage());	//NCTAMS
	R36->CustomShapeImage(GreenRouterImage());	//NCTAMS
	R16->CustomShapeImage(BlackRouterImage());	//NCTAMS
	
	/*********************************JRAE 04-4 Network Node Shapes*********************************/
	
	
	/********************************JRAE 04-4 Network Node Locations*******************************/
	
	//PEO C3T - Ft. Hood, TX network elements
	brigade->SetLocation(0.5, 21);			//Brigade
	R40->SetLocation(0.5, 19);				//Brigade
	H8->SetLocation(0.5, 17.5);				//Brigade
	R41->SetLocation(0.5, 16);				//Brigade
	
	//JITC - Ft. Huachuca, AZ network elements
	soc->SetLocation(3.5, 19.5);			//Spec. Ops. Command
	R23->SetLocation(3.5, 17.5);			//Spec. Ops. Command
	H5->SetLocation(3.5, 16);				//Spec. Ops. Command
	
	//AFCA - Scott AFB, IL network elements
	fs1c->SetLocation(16.5, 3);				//Fighter Squadron 1 Commander
	R22->SetLocation(16.5, 5);				//Fighter Squadron 1 Commander
	
	//BCBL - Ft. Gordon, GA network elements
	batallion->SetLocation(5, 21);			//Batallion
	R25a->SetLocation(5, 19);				//Batallion
	H9->SetLocation(5, 17.5);				//Batallion
	R14->SetLocation(5, 16);				//Batallion
	platoon->SetLocation(12.5, 21);			//Platoon
	R26->SetLocation(12.5, 19);				//Platoon
	H10->SetLocation(12.5, 17.5);			//Platoon
	R13->SetLocation(12.5, 16);				//Platoon
	
	//NRL - Washington, D.C. network elements
	R12->SetLocation(6.5, 16);				//Teleport 2
	uav->SetLocation(8, 21);				//UAV
	R27->SetLocation(8, 19);				//UAV
	H3->SetLocation(8, 17.5);				//UAV
	R6->SetLocation(8, 16);					//UAV
	R4->SetLocation(9.5, 16);				//Teleport 1
	R43->SetLocation(14, 19);				//GIG-BE POP1
	H20->SetLocation(14, 17.5);				//GIG-BE POP1
	R42->SetLocation(15.5, 17.5);			//GIG-BE POP1
	R2->SetLocation(15.5, 16);				//GIG-BE POP1
	aorc->SetLocation(11, 21);				//AOR Commander
	R28->SetLocation(11, 19);				//AOR Commander
	H18->SetLocation(11, 17.5);				//AOR Commander
	R25b->SetLocation(11, 16);				//AOR Commander
	
	//MIT-LL - Lexington, MA network elements
	R8->SetLocation(12.5, 12);				//GIG-BE POP3
	SAT1a->SetLocation(8.5, 12);			//Satellite 1
	SAT1b->SetLocation(19.5, 12);			//Satellite 1
	SAT1c->SetLocation(15, 12);				//Satellite 1
	SAT2a->SetLocation(3.5, 12);			//Satellite 2
	SAT2b->SetLocation(1, 12);				//Satellite 2
	SAT2c->SetLocation(7, 12);				//Satellite 2
	SAT2d->SetLocation(16.5, 12);			//Satellite 2
	SAT3->SetLocation(5, 12);				//Satellite 3
	
	//DISA network elements
	R7->SetLocation(10, 12);				//GIG-BE POP2
	
	//JTEO - McLean, VA network elements
	fs2c->SetLocation(19.5, 3);				//Fighter Squadron 2 Commander
	R29->SetLocation(19.5, 5);				//Fighter Squadron 2 Commander
	H2->SetLocation(19.5, 6.5);				//Fighter Squadron 2 Commander
	R5->SetLocation(19.5, 8);				//Fighter Squadron 2 Commander
	uavcrew->SetLocation(18, 3);			//UAV Crew
	R30->SetLocation(18, 5);				//UAV Crew
	H4->SetLocation(18, 6.5);				//UAV Crew
	R3->SetLocation(18, 8);					//UAV Crew
	
	//SSC-SD - San Diego, CA network elements
	H6->SetLocation(16.5, 6.5);				//CJTF
	R10->SetLocation(16.5, 8);				//CJTF
	jsotf->SetLocation(15, 3);				//JSOTF
	R31->SetLocation(15, 5);				//JSOTF
	H17->SetLocation(15, 6.5);				//JSOTF
	R24->SetLocation(15, 8);				//JSOTF
	ship2g->SetLocation(12.5, 1.5);			//ESG Commander (Ship 2)
	R32->SetLocation(12.5, 3.5);			//ESG Commander (Ship 2)
	H14->SetLocation(12.5, 5);				//ESG Commander (Ship 2)
	ship2r->SetLocation(11, 0);				//ESG Commander (Ship 2)
	R33->SetLocation(11, 2);				//ESG Commander (Ship 2)
	H13->SetLocation(11, 3.5);				//ESG Commander (Ship 2)
	R17->SetLocation(11, 5);				//ESG Commander (Ship 2)
	ship2tl->SetLocation(9.5, 3);			//ESG Commander (ship 2)
	H16->SetLocation(9.5, 5);				//ESG Commander (Ship 2)
	R20->SetLocation(13.5, 8);				//JTRS Cloud
	R19->SetLocation(12.5, 8);				//JTRS Cloud
	R18->SetLocation(11.5, 8);				//JTRS Cloud
	R15->SetLocation(10.5, 8);				//JTRS Cloud
	JTRS->SetLocation(12, 6.5);				//JTRS Router
	
	//CPSG - Brooks AFB, SC network elements
	aoc1->SetLocation(8, 4.5);				//Air Ops Center
	H7->SetLocation(8, 6.5);				//Air Ops Center
	aoc2->SetLocation(7.5, 0);				//Air Ops Center
	H19->SetLocation(7.5, 2);				//Air Ops Center
	R11->SetLocation(7.5, 8);				//Air Ops Center
	ap->SetLocation(6.5, 4.5);				//Air Platform
	H15->SetLocation(6.5, 6.5);				//Air Platform
	R21->SetLocation(6.5, 8);				//Air Platform
	
	//SSC-CH - Charleston, SC network elements
	isufist->SetLocation(5, 3);				//CONUS ISU/FIST
	R34->SetLocation(5, 5);					//CONUS ISU/FIST
	H1->SetLocation(5, 6.5);				//CONUS ISU/FIST
	R1->SetLocation(5, 8);					//CONUS ISU/FIST
	ship1g->SetLocation(5, 0.5);			//CONUS ISU/FIST
	R38->SetLocation(3.5, 0.5);				//DDG (Ship 1)
	Hx->SetLocation(3.5, 2);				//DDG (Ship 1)
	ship1r->SetLocation(0.5, 0);			//DDG (Ship 1)
	R39->SetLocation(0.5, 2);				//DDG (Ship 1)
	R37->SetLocation(2, 3.5);				//DDG (Ship 1)
	SAT4->SetLocation(3.5, 3.5);			//Satellite 4
	R35->SetLocation(3.5, 5);				//NCTAMS
	H11->SetLocation(3.5, 6.5);				//NCTAMS
	R36->SetLocation(0.5, 5);				//NCTAMS
	H12->SetLocation(0.5, 6.5);				//NCTAMS
	R16->SetLocation(2, 8);					//NCTAMS
	Hy->SetLocation(2, 5);					//NCTAMS
	
	s.NewLocation(-1.25, -0.5);
	s.NewLocation(20.75, 21);
	
	/********************************JRAE 04-4 Network Node Locations*******************************/

	
	/****************************JRAE 04-4 TACLANE Security Associations****************************/
	
/*
	//Red Network
	H8->AddSecurityAssociation(abrigade, aH8_R41);
	H8->AddSecurityAssociation(asoc, aH5_R8);
	H8->AddSecurityAssociation(afs1c, aH6_R10);
	H8->AddSecurityAssociation(abatallion, aH9_R14);
	H8->AddSecurityAssociation(aplatoon, aH10_R13);
	H8->AddSecurityAssociation(auav, aH3_R6);
	H8->AddSecurityAssociation(aaorc, aH18_R25b);
	H8->AddSecurityAssociation(afs2c, aH2_R5);
	H8->AddSecurityAssociation(auavcrew, aH4_R3);
	H8->AddSecurityAssociation(ajsotf, aH17_R24);
	H8->AddSecurityAssociation(aship2r, aH13_R17);
	H8->AddSecurityAssociation(aship2tl, aH16_R17);
	H8->AddSecurityAssociation(aaoc1, aH7_R11);
	H8->AddSecurityAssociation(aaoc2, aH19_R11);
	H8->AddSecurityAssociation(aap, aH15_R21);
	H8->AddSecurityAssociation(aisufist, aH1_R1);
	H8->AddSecurityAssociation(aship1r, aH11_R16);		
 
	//Green network
	H12->AddSecurityAssociation(aship2g, aH14_R17);
	H14->AddSecurityAssociation(aship1g, aH12_R16);
	Hy->AddSecurityAssociation(aship1g, aHx_R37);
	Hx->AddSecurityAssociation(aship2g, aHy_R35);
*/		

	//PEO C3T - Ft. Hood, TX TACLANE
	H8->AddSecurityAssociation(asoc, aH5_R8);
	H8->AddSecurityAssociation(afs1c, aH6_R10);
	H8->AddSecurityAssociation(abatallion, aH9_R14);
	H8->AddSecurityAssociation(aplatoon, aH10_R13);
	H8->AddSecurityAssociation(auav, aH3_R6);
	H8->AddSecurityAssociation(aaorc, aH18_R25b);
	H8->AddSecurityAssociation(afs2c, aH2_R5);
	H8->AddSecurityAssociation(auavcrew, aH4_R3);
	H8->AddSecurityAssociation(ajsotf, aH17_R24);
	H8->AddSecurityAssociation(aship2r, aH13_R17);
	H8->AddSecurityAssociation(aship2tl, aH16_R17);
	H8->AddSecurityAssociation(aaoc1, aH7_R11);
	H8->AddSecurityAssociation(aaoc2, aH19_R11);
	H8->AddSecurityAssociation(aap, aH15_R21);
	H8->AddSecurityAssociation(aisufist, aH1_R1);
	H8->AddSecurityAssociation(aship1r, aH11_R16);		

	//JITC - Ft. Huachuca, AZ TACLANE
	H5->AddSecurityAssociation(abrigade, aH8_R41);
	H5->AddSecurityAssociation(afs1c, aH6_R10);
	H5->AddSecurityAssociation(abatallion, aH9_R14);
	H5->AddSecurityAssociation(aplatoon, aH10_R13);
	H5->AddSecurityAssociation(auav, aH3_R6);
	H5->AddSecurityAssociation(aaorc, aH18_R25b);
	H5->AddSecurityAssociation(afs2c, aH2_R5);
	H5->AddSecurityAssociation(auavcrew, aH4_R3);
	H5->AddSecurityAssociation(ajsotf, aH17_R24);
	H5->AddSecurityAssociation(aship2r, aH13_R17);
	H5->AddSecurityAssociation(aship2tl, aH16_R17);
	H5->AddSecurityAssociation(aaoc1, aH7_R11);
	H5->AddSecurityAssociation(aaoc2, aH19_R11);
	H5->AddSecurityAssociation(aap, aH15_R21);
	H5->AddSecurityAssociation(aisufist, aH1_R1);
	H5->AddSecurityAssociation(aship1r, aH11_R16);		

	//AFCA - Scott AFB, IL TACLANE
	H6->AddSecurityAssociation(abrigade, aH8_R41);
	H6->AddSecurityAssociation(asoc, aH5_R8);
	H6->AddSecurityAssociation(abatallion, aH9_R14);
	H6->AddSecurityAssociation(aplatoon, aH10_R13);
	H6->AddSecurityAssociation(auav, aH3_R6);
	H6->AddSecurityAssociation(aaorc, aH18_R25b);
	H6->AddSecurityAssociation(afs2c, aH2_R5);
	H6->AddSecurityAssociation(auavcrew, aH4_R3);
	H6->AddSecurityAssociation(ajsotf, aH17_R24);
	H6->AddSecurityAssociation(aship2r, aH13_R17);
	H6->AddSecurityAssociation(aship2tl, aH16_R17);
	H6->AddSecurityAssociation(aaoc1, aH7_R11);
	H6->AddSecurityAssociation(aaoc2, aH19_R11);
	H6->AddSecurityAssociation(aap, aH15_R21);
	H6->AddSecurityAssociation(aisufist, aH1_R1);
	H6->AddSecurityAssociation(aship1r, aH11_R16);		

	//BCBL - Ft. Gordon, GA TACLANEs
	H9->AddSecurityAssociation(abrigade, aH8_R41);
	H9->AddSecurityAssociation(asoc, aH5_R8);
	H9->AddSecurityAssociation(afs1c, aH6_R10);
	H9->AddSecurityAssociation(aplatoon, aH10_R13);
	H9->AddSecurityAssociation(auav, aH3_R6);
	H9->AddSecurityAssociation(aaorc, aH18_R25b);
	H9->AddSecurityAssociation(afs2c, aH2_R5);
	H9->AddSecurityAssociation(auavcrew, aH4_R3);
	H9->AddSecurityAssociation(ajsotf, aH17_R24);
	H9->AddSecurityAssociation(aship2r, aH13_R17);
	H9->AddSecurityAssociation(aship2tl, aH16_R17);
	H9->AddSecurityAssociation(aaoc1, aH7_R11);
	H9->AddSecurityAssociation(aaoc2, aH19_R11);
	H9->AddSecurityAssociation(aap, aH15_R21);
	H9->AddSecurityAssociation(aisufist, aH1_R1);
	H9->AddSecurityAssociation(aship1r, aH11_R16);
	
	H10->AddSecurityAssociation(abrigade, aH8_R41);
	H10->AddSecurityAssociation(asoc, aH5_R8);
	H10->AddSecurityAssociation(afs1c, aH6_R10);
	H10->AddSecurityAssociation(abatallion, aH9_R14);
	H10->AddSecurityAssociation(auav, aH3_R6);
	H10->AddSecurityAssociation(aaorc, aH18_R25b);
	H10->AddSecurityAssociation(afs2c, aH2_R5);
	H10->AddSecurityAssociation(auavcrew, aH4_R3);
	H10->AddSecurityAssociation(ajsotf, aH17_R24);
	H10->AddSecurityAssociation(aship2r, aH13_R17);
	H10->AddSecurityAssociation(aship2tl, aH16_R17);
	H10->AddSecurityAssociation(aaoc1, aH7_R11);
	H10->AddSecurityAssociation(aaoc2, aH19_R11);
	H10->AddSecurityAssociation(aap, aH15_R21);
	H10->AddSecurityAssociation(aisufist, aH1_R1);
	H10->AddSecurityAssociation(aship1r, aH11_R16);		
	
	//NRL - Washington, D.C. TACLANEs
	H3->AddSecurityAssociation(abrigade, aH8_R41);
	H3->AddSecurityAssociation(asoc, aH5_R8);
	H3->AddSecurityAssociation(afs1c, aH6_R10);
	H3->AddSecurityAssociation(abatallion, aH9_R14);
	H3->AddSecurityAssociation(aplatoon, aH10_R13);
	H3->AddSecurityAssociation(aaorc, aH18_R25b);
	H3->AddSecurityAssociation(afs2c, aH2_R5);
	H3->AddSecurityAssociation(auavcrew, aH4_R3);
	H3->AddSecurityAssociation(ajsotf, aH17_R24);
	H3->AddSecurityAssociation(aship2r, aH13_R17);
	H3->AddSecurityAssociation(aship2tl, aH16_R17);
	H3->AddSecurityAssociation(aaoc1, aH7_R11);
	H3->AddSecurityAssociation(aaoc2, aH19_R11);
	H3->AddSecurityAssociation(aap, aH15_R21);
	H3->AddSecurityAssociation(aisufist, aH1_R1);
	H3->AddSecurityAssociation(aship1r, aH11_R16);
	
	H18->AddSecurityAssociation(abrigade, aH8_R41);
	H18->AddSecurityAssociation(asoc, aH5_R8);
	H18->AddSecurityAssociation(afs1c, aH6_R10);
	H18->AddSecurityAssociation(abatallion, aH9_R14);
	H18->AddSecurityAssociation(aplatoon, aH10_R13);
	H18->AddSecurityAssociation(auav, aH3_R6);
	H18->AddSecurityAssociation(afs2c, aH2_R5);
	H18->AddSecurityAssociation(auavcrew, aH4_R3);
	H18->AddSecurityAssociation(ajsotf, aH17_R24);
	H18->AddSecurityAssociation(aship2r, aH13_R17);
	H18->AddSecurityAssociation(aship2tl, aH16_R17);
	H18->AddSecurityAssociation(aaoc1, aH7_R11);
	H18->AddSecurityAssociation(aaoc2, aH19_R11);
	H18->AddSecurityAssociation(aap, aH15_R21);
	H18->AddSecurityAssociation(aisufist, aH1_R1);
	H18->AddSecurityAssociation(aship1r, aH11_R16);		
	
	//JTEO - McLean, VA TACLANEs
	H2->AddSecurityAssociation(abrigade, aH8_R41);
	H2->AddSecurityAssociation(asoc, aH5_R8);
	H2->AddSecurityAssociation(afs1c, aH6_R10);
	H2->AddSecurityAssociation(abatallion, aH9_R14);
	H2->AddSecurityAssociation(aplatoon, aH10_R13);
	H2->AddSecurityAssociation(auav, aH3_R6);
	H2->AddSecurityAssociation(aaorc, aH18_R25b);
	H2->AddSecurityAssociation(auavcrew, aH4_R3);
	H2->AddSecurityAssociation(ajsotf, aH17_R24);
	H2->AddSecurityAssociation(aship2r, aH13_R17);
	H2->AddSecurityAssociation(aship2tl, aH16_R17);
	H2->AddSecurityAssociation(aaoc1, aH7_R11);
	H2->AddSecurityAssociation(aaoc2, aH19_R11);
	H2->AddSecurityAssociation(aap, aH15_R21);
	H2->AddSecurityAssociation(aisufist, aH1_R1);
	H2->AddSecurityAssociation(aship1r, aH11_R16);
	
	H4->AddSecurityAssociation(abrigade, aH8_R41);
	H4->AddSecurityAssociation(asoc, aH5_R8);
	H4->AddSecurityAssociation(afs1c, aH6_R10);
	H4->AddSecurityAssociation(abatallion, aH9_R14);
	H4->AddSecurityAssociation(aplatoon, aH10_R13);
	H4->AddSecurityAssociation(auav, aH3_R6);
	H4->AddSecurityAssociation(aaorc, aH18_R25b);
	H4->AddSecurityAssociation(afs2c, aH2_R5);
	H4->AddSecurityAssociation(ajsotf, aH17_R24);
	H4->AddSecurityAssociation(aship2r, aH13_R17);
	H4->AddSecurityAssociation(aship2tl, aH16_R17);
	H4->AddSecurityAssociation(aaoc1, aH7_R11);
	H4->AddSecurityAssociation(aaoc2, aH19_R11);
	H4->AddSecurityAssociation(aap, aH15_R21);
	H4->AddSecurityAssociation(aisufist, aH1_R1);
	H4->AddSecurityAssociation(aship1r, aH11_R16);		

	//SSC-SD - San Diego, CA TACLANEs
	H17->AddSecurityAssociation(abrigade, aH8_R41);
	H17->AddSecurityAssociation(asoc, aH5_R8);
	H17->AddSecurityAssociation(afs1c, aH6_R10);
	H17->AddSecurityAssociation(abatallion, aH9_R14);
	H17->AddSecurityAssociation(aplatoon, aH10_R13);
	H17->AddSecurityAssociation(auav, aH3_R6);
	H17->AddSecurityAssociation(aaorc, aH18_R25b);
	H17->AddSecurityAssociation(afs2c, aH2_R5);
	H17->AddSecurityAssociation(auavcrew, aH4_R3);
	H17->AddSecurityAssociation(aship2r, aH13_R17);
	H17->AddSecurityAssociation(aship2tl, aH16_R17);
	H17->AddSecurityAssociation(aaoc1, aH7_R11);
	H17->AddSecurityAssociation(aaoc2, aH19_R11);
	H17->AddSecurityAssociation(aap, aH15_R21);
	H17->AddSecurityAssociation(aisufist, aH1_R1);
	H17->AddSecurityAssociation(aship1r, aH11_R16);
	
	H13->AddSecurityAssociation(abrigade, aH8_R41);
	H13->AddSecurityAssociation(asoc, aH5_R8);
	H13->AddSecurityAssociation(afs1c, aH6_R10);
	H13->AddSecurityAssociation(abatallion, aH9_R14);
	H13->AddSecurityAssociation(aplatoon, aH10_R13);
	H13->AddSecurityAssociation(auav, aH3_R6);
	H13->AddSecurityAssociation(aaorc, aH18_R25b);
	H13->AddSecurityAssociation(afs2c, aH2_R5);
	H13->AddSecurityAssociation(auavcrew, aH4_R3);
	H13->AddSecurityAssociation(ajsotf, aH17_R24);
	H13->AddSecurityAssociation(aship2tl, aH16_R17);
	H13->AddSecurityAssociation(aaoc1, aH7_R11);
	H13->AddSecurityAssociation(aaoc2, aH19_R11);
	H13->AddSecurityAssociation(aap, aH15_R21);
	H13->AddSecurityAssociation(aisufist, aH1_R1);
	H13->AddSecurityAssociation(aship1r, aH11_R16);	
	
	H16->AddSecurityAssociation(abrigade, aH8_R41);
	H16->AddSecurityAssociation(asoc, aH5_R8);
	H16->AddSecurityAssociation(afs1c, aH6_R10);
	H16->AddSecurityAssociation(abatallion, aH9_R14);
	H16->AddSecurityAssociation(aplatoon, aH10_R13);
	H16->AddSecurityAssociation(auav, aH3_R6);
	H16->AddSecurityAssociation(aaorc, aH18_R25b);
	H16->AddSecurityAssociation(afs2c, aH2_R5);
	H16->AddSecurityAssociation(auavcrew, aH4_R3);
	H16->AddSecurityAssociation(ajsotf, aH17_R24);
	H16->AddSecurityAssociation(aship2r, aH13_R17);
	H16->AddSecurityAssociation(aaoc1, aH7_R11);
	H16->AddSecurityAssociation(aaoc2, aH19_R11);
	H16->AddSecurityAssociation(aap, aH15_R21);
	H16->AddSecurityAssociation(aisufist, aH1_R1);
	H16->AddSecurityAssociation(aship1r, aH11_R16);		

	H14->AddSecurityAssociation(aship1g, aH12_R16);

	//CPSG - Brooks AFB, SC TACLANEs
	H7->AddSecurityAssociation(abrigade, aH8_R41);
	H7->AddSecurityAssociation(asoc, aH5_R8);
	H7->AddSecurityAssociation(afs1c, aH6_R10);
	H7->AddSecurityAssociation(abatallion, aH9_R14);
	H7->AddSecurityAssociation(aplatoon, aH10_R13);
	H7->AddSecurityAssociation(auav, aH3_R6);
	H7->AddSecurityAssociation(aaorc, aH18_R25b);
	H7->AddSecurityAssociation(afs2c, aH2_R5);
	H7->AddSecurityAssociation(auavcrew, aH4_R3);
	H7->AddSecurityAssociation(ajsotf, aH17_R24);
	H7->AddSecurityAssociation(aship2r, aH13_R17);
	H7->AddSecurityAssociation(aship2tl, aH16_R17);
	H7->AddSecurityAssociation(aaoc2, aH19_R11);
	H7->AddSecurityAssociation(aap, aH15_R21);
	H7->AddSecurityAssociation(aisufist, aH1_R1);
	H7->AddSecurityAssociation(aship1r, aH11_R16);		

	H19->AddSecurityAssociation(abrigade, aH8_R41);
	H19->AddSecurityAssociation(asoc, aH5_R8);
	H19->AddSecurityAssociation(afs1c, aH6_R10);
	H19->AddSecurityAssociation(abatallion, aH9_R14);
	H19->AddSecurityAssociation(aplatoon, aH10_R13);
	H19->AddSecurityAssociation(auav, aH3_R6);
	H19->AddSecurityAssociation(aaorc, aH18_R25b);
	H19->AddSecurityAssociation(afs2c, aH2_R5);
	H19->AddSecurityAssociation(auavcrew, aH4_R3);
	H19->AddSecurityAssociation(ajsotf, aH17_R24);
	H19->AddSecurityAssociation(aship2r, aH13_R17);
	H19->AddSecurityAssociation(aship2tl, aH16_R17);
	H19->AddSecurityAssociation(aaoc1, aH7_R11);
	H19->AddSecurityAssociation(aap, aH15_R21);
	H19->AddSecurityAssociation(aisufist, aH1_R1);
	H19->AddSecurityAssociation(aship1r, aH11_R16);
	
	H15->AddSecurityAssociation(abrigade, aH8_R41);
	H15->AddSecurityAssociation(asoc, aH5_R8);
	H15->AddSecurityAssociation(afs1c, aH6_R10);
	H15->AddSecurityAssociation(abatallion, aH9_R14);
	H15->AddSecurityAssociation(aplatoon, aH10_R13);
	H15->AddSecurityAssociation(auav, aH3_R6);
	H15->AddSecurityAssociation(aaorc, aH18_R25b);
	H15->AddSecurityAssociation(afs2c, aH2_R5);
	H15->AddSecurityAssociation(auavcrew, aH4_R3);
	H15->AddSecurityAssociation(ajsotf, aH17_R24);
	H15->AddSecurityAssociation(aship2r, aH13_R17);
	H15->AddSecurityAssociation(aship2tl, aH16_R17);
	H15->AddSecurityAssociation(aaoc1, aH7_R11);
	H15->AddSecurityAssociation(aaoc2, aH19_R11);
	H15->AddSecurityAssociation(aisufist, aH1_R1);
	H15->AddSecurityAssociation(aship1r, aH11_R16);		
	
	//SSC-CH - Charleston, SC TACLANEs
	H1->AddSecurityAssociation(abrigade, aH8_R41);
	H1->AddSecurityAssociation(asoc, aH5_R8);
	H1->AddSecurityAssociation(afs1c, aH6_R10);
	H1->AddSecurityAssociation(abatallion, aH9_R14);
	H1->AddSecurityAssociation(aplatoon, aH10_R13);
	H1->AddSecurityAssociation(auav, aH3_R6);
	H1->AddSecurityAssociation(aaorc, aH18_R25b);
	H1->AddSecurityAssociation(afs2c, aH2_R5);
	H1->AddSecurityAssociation(auavcrew, aH4_R3);
	H1->AddSecurityAssociation(ajsotf, aH17_R24);
	H1->AddSecurityAssociation(aship2r, aH13_R17);
	H1->AddSecurityAssociation(aship2tl, aH16_R17);
	H1->AddSecurityAssociation(aaoc1, aH7_R11);
	H1->AddSecurityAssociation(aaoc2, aH19_R11);
	H1->AddSecurityAssociation(aap, aH15_R21);
	H1->AddSecurityAssociation(aship1r, aH11_R16);		

	H11->AddSecurityAssociation(abrigade, aH8_R41);
	H11->AddSecurityAssociation(asoc, aH5_R8);
	H11->AddSecurityAssociation(afs1c, aH6_R10);
	H11->AddSecurityAssociation(abatallion, aH9_R14);
	H11->AddSecurityAssociation(aplatoon, aH10_R13);
	H11->AddSecurityAssociation(auav, aH3_R6);
	H11->AddSecurityAssociation(aaorc, aH18_R25b);
	H11->AddSecurityAssociation(afs2c, aH2_R5);
	H11->AddSecurityAssociation(auavcrew, aH4_R3);
	H11->AddSecurityAssociation(ajsotf, aH17_R24);
	H11->AddSecurityAssociation(aship2r, aH13_R17);
	H11->AddSecurityAssociation(aship2tl, aH16_R17);
	H11->AddSecurityAssociation(aaoc1, aH7_R11);
	H11->AddSecurityAssociation(aaoc2, aH19_R11);
	H11->AddSecurityAssociation(aap, aH15_R21);
	H11->AddSecurityAssociation(aisufist, aH1_R1);
	
	H12->AddSecurityAssociation(aship2g, aH14_R17);
	Hy->AddSecurityAssociation(aship1g, aHx_R37);
	Hx->AddSecurityAssociation(aship2g, aHy_R35);
	
	/****************************JRAE 04-4 TACLANE Security Associations****************************/

	//Enable tracing at the taclanes
	H8->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H5->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H6->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H9->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H10->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H3->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H18->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H2->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H4->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H17->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H13->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H16->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H7->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H19->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H15->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H1->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H11->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);		
	H12->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	H14->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	Hy->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);
	Hx->LookupProto(3, 0x800)->SetTrace(Trace::ENABLED);

        // Define the application
        Uniform startRNG(0,1);
        
  	// Create the TCP Server
	TCPServer* server1 = (TCPServer*)ship1g->AddApplication(TCPServer(TCPTahoe()));
	if (server1)
    {
		server1->BindAndListen(80); // Application on s1, port 80
		server1->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server1->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client1 = (TCPSend*)ship2g->AddApplication(TCPSend(aship1g, 80, Constant(500000), TCPTahoe()));
	if (client1)
    {
		// Enable TCP trace for this client
		client1->SetTrace(Trace::ENABLED);
		// Set random starting time
		client1->Start(startRNG.Value());
		client1->GetL4()->SetColor(Qt::green);
    }

	// Create the TCP Server
	TCPServer* server2 = (TCPServer*)ship1r->AddApplication(TCPServer(TCPTahoe()));
	if (server2)
    {
		server2->BindAndListen(80); // Application on s1, port 80
		server2->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server2->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client2 = (TCPSend*)ship2r->AddApplication(TCPSend(aship1r, 80, Constant(500000), TCPTahoe()));
	if (client2)
    {
		// Enable TCP trace for this client
		client2->SetTrace(Trace::ENABLED);
		// Set random starting time
		client2->Start(startRNG.Value());
		client2->GetL4()->SetColor(Qt::green);
    }

	// Create the TCP Server
	TCPServer* server3 = (TCPServer*)aoc1->AddApplication(TCPServer(TCPTahoe()));
	if (server3)
    {
		server3->BindAndListen(80); // Application on s1, port 80
		server3->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server3->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client3 = (TCPSend*)ship2r->AddApplication(TCPSend(aaoc1, 80, Constant(500000), TCPTahoe()));
	if (client3)
    {
		// Enable TCP trace for this client
		client3->SetTrace(Trace::ENABLED);
		// Set random starting time
		client3->Start(startRNG.Value());
		client3->GetL4()->SetColor(Qt::green);
    }
	
	// Create the TCP Server
	TCPServer* server4 = (TCPServer*)jsotf->AddApplication(TCPServer(TCPTahoe()));
	if (server4)
    {
		server4->BindAndListen(80); // Application on s1, port 80
		server4->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server4->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client4 = (TCPSend*)isufist->AddApplication(TCPSend(ajsotf, 80, Constant(500000), TCPTahoe()));
	if (client4)
    {
		// Enable TCP trace for this client
		client4->SetTrace(Trace::ENABLED);
		// Set random starting time
		client4->Start(startRNG.Value());
		client4->GetL4()->SetColor(Qt::green);
    }
	
	// Create the TCP Server
	TCPServer* server5 = (TCPServer*)brigade->AddApplication(TCPServer(TCPTahoe()));
	if (server5)
    {
		server5->BindAndListen(80); // Application on s1, port 80
		server5->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server5->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client5 = (TCPSend*)aorc->AddApplication(TCPSend(abrigade, 80, Constant(500000), TCPTahoe()));
	if (client5)
    {
		// Enable TCP trace for this client
		client5->SetTrace(Trace::ENABLED);
		// Set random starting time
		client5->Start(startRNG.Value());
		client5->GetL4()->SetColor(Qt::green);
    }

	// Create the TCP Server
	TCPServer* server6 = (TCPServer*)platoon->AddApplication(TCPServer(TCPTahoe()));
	if (server4)
    {
		server6->BindAndListen(80); // Application on s1, port 80
		server6->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server6->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client6 = (TCPSend*)aorc->AddApplication(TCPSend(aplatoon, 80, Constant(500000), TCPTahoe()));
	if (client6)
    {
		// Enable TCP trace for this client
		client6->SetTrace(Trace::ENABLED);
		// Set random starting time
		client6->Start(startRNG.Value());
		client6->GetL4()->SetColor(Qt::green);
    }
	
	// Create the TCP Server
	TCPServer* server7 = (TCPServer*)fs1c->AddApplication(TCPServer(TCPTahoe()));
	if (server7)
    {
		server7->BindAndListen(80); // Application on s1, port 80
		server7->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server7->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client7 = (TCPSend*)aorc->AddApplication(TCPSend(afs1c, 80, Constant(500000), TCPTahoe()));
	if (client7)
    {
		// Enable TCP trace for this client
		client7->SetTrace(Trace::ENABLED);
		// Set random starting time
		client7->Start(startRNG.Value());
		client7->GetL4()->SetColor(Qt::green);
    }
	
	// Create the TCP Server
	TCPServer* server8 = (TCPServer*)fs2c->AddApplication(TCPServer(TCPTahoe()));
	if (server8)
    {
		server8->BindAndListen(80); // Application on s1, port 80
		server8->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server8->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client8 = (TCPSend*)aorc->AddApplication(TCPSend(afs2c, 80, Constant(500000), TCPTahoe()));
	if (client8)
    {
		// Enable TCP trace for this client
		client8->SetTrace(Trace::ENABLED);
		// Set random starting time
		client8->Start(startRNG.Value());
		client8->GetL4()->SetColor(Qt::green);
    }
	
	// Create the TCP Server
	TCPServer* server9 = (TCPServer*)uavcrew->AddApplication(TCPServer(TCPTahoe()));
	if (server9)
    {
		server9->BindAndListen(80); // Application on s1, port 80
		server9->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server9->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client9 = (TCPSend*)uav->AddApplication(TCPSend(auavcrew, 80, Constant(500000), TCPTahoe()));
	if (client9)
    {
		// Enable TCP trace for this client
		client9->SetTrace(Trace::ENABLED);
		// Set random starting time
		client9->Start(startRNG.Value());
		client9->GetL4()->SetColor(Qt::green);
    }
	
	// Create the TCP Server
	TCPServer* server10 = (TCPServer*)ap->AddApplication(TCPServer(TCPTahoe()));
	if (server10)
    {
		server10->BindAndListen(80); // Application on s1, port 80
		server10->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server10->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client10 = (TCPSend*)fs1c->AddApplication(TCPSend(aap, 80, Constant(500000), TCPTahoe()));
	if (client10)
    {
		// Enable TCP trace for this client
		client10->SetTrace(Trace::ENABLED);
		// Set random starting time
		client10->Start(startRNG.Value());
		client10->GetL4()->SetColor(Qt::green);
    }
	
	// Create the TCP Server
	TCPServer* server11 = (TCPServer*)batallion->AddApplication(TCPServer(TCPTahoe()));
	if (server11)
    {
		server11->BindAndListen(80); // Application on s1, port 80
		server11->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server11->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client11 = (TCPSend*)aorc->AddApplication(TCPSend(abatallion, 80, Constant(500000), TCPTahoe()));
	if (client11)
    {
		// Enable TCP trace for this client
		client11->SetTrace(Trace::ENABLED);
		// Set random starting time
		client11->Start(startRNG.Value());
		client11->GetL4()->SetColor(Qt::green);
    }
	
	// Create the TCP Server
	TCPServer* server12 = (TCPServer*)jsotf->AddApplication(TCPServer(TCPTahoe()));
	if (server12)
    {
		server12->BindAndListen(80); // Application on s1, port 80
		server12->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server12->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client12 = (TCPSend*)soc->AddApplication(TCPSend(ajsotf, 80, Constant(500000), TCPTahoe()));
	if (client12)
    {
		// Enable TCP trace for this client
		client12->SetTrace(Trace::ENABLED);
		// Set random starting time
		client12->Start(startRNG.Value());
		client12->GetL4()->SetColor(Qt::green);
    }
	
	// Create the TCP Server
	TCPServer* server13 = (TCPServer*)aoc1->AddApplication(TCPServer(TCPTahoe()));
	if (server13)
    {
		server13->BindAndListen(80); // Application on s1, port 80
		server13->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server13->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client13 = (TCPSend*)fs1c->AddApplication(TCPSend(aaoc1, 80, Constant(500000), TCPTahoe()));
	if (client13)
    {
		// Enable TCP trace for this client
		client13->SetTrace(Trace::ENABLED);
		// Set random starting time
		client13->Start(startRNG.Value());
		client13->GetL4()->SetColor(Qt::green);
    }
	
	// Create the TCP Server
	TCPServer* server14 = (TCPServer*)aoc2->AddApplication(TCPServer(TCPTahoe()));
	if (server14)
    {
		server14->BindAndListen(80); // Application on s1, port 80
		server14->SetTrace(Trace::ENABLED); // Trace TCP actions at server1
		server14->GetL4()->SetColor(Qt::blue);
    }
	
	// Create the TCP Sending Applications
	TCPSend* client14 = (TCPSend*)fs2c->AddApplication(TCPSend(aaoc2, 80, Constant(500000), TCPTahoe()));
	if (client14)
    {
		// Enable TCP trace for this client
		client14->SetTrace(Trace::ENABLED);
		// Set random starting time
		client14->Start(startRNG.Value());
		client14->GetL4()->SetColor(Qt::green);
    }
	

	OnOffApplication* ooa1 = (OnOffApplication*)ship2g->AddApplication(
		OnOffApplication(ship1g->GetIPAddr(), 10000,
						 Constant(1), // On time
						 Constant(0), // Off time,
						 UDP(), Rate("100kb")));
	ooa1->Start(startRNG.Value());

	OnOffApplication* ooa2 = (OnOffApplication*)ship2r->AddApplication(
		 OnOffApplication(ship1r->GetIPAddr(), 10000,
						  Constant(1), // On time
						  Constant(0), // Off time,
						  UDP(), Rate("100kb")));
	ooa2->Start(startRNG.Value());
 
	OnOffApplication* ooa3 = (OnOffApplication*)ship2r->AddApplication(
		OnOffApplication(aorc->GetIPAddr(), 10000,
						Constant(1), // On time
						Constant(0), // Off time,
						UDP(), Rate("100kb")));
	ooa3->Start(startRNG.Value());
	
	OnOffApplication* ooa4 = (OnOffApplication*)isufist->AddApplication(
		OnOffApplication(jsotf->GetIPAddr(), 10000,
						Constant(1), // On time
						Constant(0), // Off time,
						UDP(), Rate("100kb")));
	ooa4->Start(startRNG.Value());
	
	OnOffApplication* ooa5 = (OnOffApplication*)aorc->AddApplication(
		OnOffApplication(brigade->GetIPAddr(), 10000,
						 Constant(1), // On time
						 Constant(0), // Off time,
						 UDP(), Rate("100kb")));
	ooa5->Start(startRNG.Value());
	
	OnOffApplication* ooa6 = (OnOffApplication*)aorc->AddApplication(
		 OnOffApplication(platoon->GetIPAddr(), 10000,
						  Constant(1), // On time
						  Constant(0), // Off time,
						  UDP(), Rate("100kb")));
	ooa6->Start(startRNG.Value());
	
	OnOffApplication* ooa7 = (OnOffApplication*)aorc->AddApplication(
		 OnOffApplication(fs1c->GetIPAddr(), 10000,
						  Constant(1), // On time
						  Constant(0), // Off time,
						  UDP(), Rate("100kb")));
	ooa7->Start(startRNG.Value());
	
	OnOffApplication* ooa8 = (OnOffApplication*)aorc->AddApplication(
		 OnOffApplication(fs2c->GetIPAddr(), 10000,
						  Constant(1), // On time
						  Constant(0), // Off time,
						  UDP(), Rate("100kb")));
	ooa8->Start(startRNG.Value());
	
	OnOffApplication* ooa9 = (OnOffApplication*)uav->AddApplication(
		 OnOffApplication(uavcrew->GetIPAddr(), 10000,
						  Constant(1), // On time
						  Constant(0), // Off time,
						  UDP(), Rate("100kb")));
	ooa9->Start(startRNG.Value());
	
	OnOffApplication* ooa10 = (OnOffApplication*)fs1c->AddApplication(
		OnOffApplication(ap->GetIPAddr(), 10000,
						 Constant(1), // On time
						 Constant(0), // Off time,
						 UDP(), Rate("100kb")));
	ooa10->Start(startRNG.Value());
	
	OnOffApplication* ooa11 = (OnOffApplication*)aorc->AddApplication(
		  OnOffApplication(batallion->GetIPAddr(), 10000,
						   Constant(1), // On time
						   Constant(0), // Off time,
						   UDP(), Rate("100kb")));
	ooa11->Start(startRNG.Value());
	
	OnOffApplication* ooa12 = (OnOffApplication*)soc->AddApplication(
		  OnOffApplication(jsotf->GetIPAddr(), 10000,
						   Constant(1), // On time
						   Constant(0), // Off time,
						   UDP(), Rate("100kb")));
	ooa12->Start(startRNG.Value());
	
	OnOffApplication* ooa13 = (OnOffApplication*)fs1c->AddApplication(
		 OnOffApplication(aoc1->GetIPAddr(), 10000,
						  Constant(1), // On time
						  Constant(0), // Off time,
						  UDP(), Rate("100kb")));
	ooa13->Start(startRNG.Value());
	
	OnOffApplication* ooa14 = (OnOffApplication*)fs2c->AddApplication(
		  OnOffApplication(aoc2->GetIPAddr(), 10000,
						   Constant(1), // On time
						   Constant(0), // Off time,
						   UDP(), Rate("100kb")));
	ooa14->Start(startRNG.Value());
	
	OnOffApplication* ooa21 = (OnOffApplication*)ship1g->AddApplication(
		OnOffApplication(ship2g->GetIPAddr(), 10000,
						Constant(1), // On time
						Constant(0), // Off time,
						UDP(), Rate("100kb")));
	ooa21->Start(startRNG.Value());
	
	OnOffApplication* ooa22 = (OnOffApplication*)ship1r->AddApplication(
		OnOffApplication(ship2r->GetIPAddr(), 10000,
						Constant(1), // On time
						Constant(0), // Off time,
						UDP(), Rate("100kb")));
	ooa22->Start(startRNG.Value());
	
	OnOffApplication* ooa23 = (OnOffApplication*)aorc->AddApplication(
		OnOffApplication(ship2r->GetIPAddr(), 10000,
						Constant(1), // On time
						Constant(0), // Off time,
						UDP(), Rate("100kb")));
	ooa23->Start(startRNG.Value());
	
	OnOffApplication* ooa24 = (OnOffApplication*)jsotf->AddApplication(
		OnOffApplication(isufist->GetIPAddr(), 10000,
						 Constant(1), // On time
						 Constant(0), // Off time,
						 UDP(), Rate("100kb")));
	ooa24->Start(startRNG.Value());
	
	OnOffApplication* ooa25 = (OnOffApplication*)brigade->AddApplication(
		 OnOffApplication(aorc->GetIPAddr(), 10000,
						  Constant(1), // On time
						  Constant(0), // Off time,
						  UDP(), Rate("100kb")));
	ooa25->Start(startRNG.Value());
	
	OnOffApplication* ooa26 = (OnOffApplication*)platoon->AddApplication(
		 OnOffApplication(aorc->GetIPAddr(), 10000,
						  Constant(1), // On time
						  Constant(0), // Off time,
						  UDP(), Rate("100kb")));
	ooa26->Start(startRNG.Value());
	
	OnOffApplication* ooa27 = (OnOffApplication*)fs1c->AddApplication(
		 OnOffApplication(aorc->GetIPAddr(), 10000,
						  Constant(1), // On time
						  Constant(0), // Off time,
						  UDP(), Rate("100kb")));
	ooa27->Start(startRNG.Value());
	
	OnOffApplication* ooa28 = (OnOffApplication*)fs2c->AddApplication(
		 OnOffApplication(aorc->GetIPAddr(), 10000,
						  Constant(1), // On time
						  Constant(0), // Off time,
						  UDP(), Rate("100kb")));
	ooa28->Start(startRNG.Value());
	
	OnOffApplication* ooa29 = (OnOffApplication*)uavcrew->AddApplication(
		OnOffApplication(uav->GetIPAddr(), 10000,
						 Constant(1), // On time
						 Constant(0), // Off time,
						 UDP(), Rate("100kb")));
	ooa29->Start(startRNG.Value());
	
	OnOffApplication* ooa210 = (OnOffApplication*)ap->AddApplication(
		OnOffApplication(fs1c->GetIPAddr(), 10000,
					   Constant(1), // On time
					   Constant(0), // Off time,
					   UDP(), Rate("100kb")));
	ooa210->Start(startRNG.Value());
	
	OnOffApplication* ooa211 = (OnOffApplication*)batallion->AddApplication(
		OnOffApplication(aorc->GetIPAddr(), 10000,
					   Constant(1), // On time
					   Constant(0), // Off time,
					   UDP(), Rate("100kb")));
	ooa211->Start(startRNG.Value());
	
	OnOffApplication* ooa212 = (OnOffApplication*)jsotf->AddApplication(
		OnOffApplication(soc->GetIPAddr(), 10000,
					  Constant(1), // On time
					  Constant(0), // Off time,
					  UDP(), Rate("100kb")));
	ooa212->Start(startRNG.Value());
	
	OnOffApplication* ooa213 = (OnOffApplication*)aoc1->AddApplication(
		OnOffApplication(fs1c->GetIPAddr(), 10000,
					   Constant(1), // On time
					   Constant(0), // Off time,
					   UDP(), Rate("100kb")));
	ooa213->Start(startRNG.Value());
	
	OnOffApplication* ooa214 = (OnOffApplication*)aoc2->AddApplication(
		OnOffApplication(fs2c->GetIPAddr(), 10000,
					   Constant(1), // On time
					   Constant(0), // Off time,
					   UDP(), Rate("100kb")));
	ooa214->Start(startRNG.Value());
	
	


	// Specify animation
	s.NodeSelectedCallback(NodeSelected);
	s.AnimationUpdateInterval(Time("2ms")); // 2ms initial update rate
	s.Progress(1);
	s.StopAt(100);
	s.Run();	
}
