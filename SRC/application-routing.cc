
#include "application-routing.h"

ApplicationRouting::ApplicationRouting()
{
  Start(Simulator::Now());
}

ApplicationRouting::ApplicationRouting(L4Protocol* l4proto_)
{
  l4proto = l4proto_;
  Start(Simulator::Now());
}

ApplicationRouting::~ApplicationRouting()
{
}
