// Test the EIGRP protocol with animation.
// George F. Riley, Talal Jaafar, Georgia Tech, Spring 2004

#include <iostream>

#include "common-defs.h"
#include "simulator.h"
#include "node.h"
#include "ratetimeparse.h"
#include "linkp2p.h"
#include "Itm.h"
#include "application-tcpserver.h"
#include "application-tcpsend.h"
#include "tcp-tahoe.h"

#ifdef HAVE_QT
#include <qnamespace.h>
#endif


using namespace std;

int main(int argc, char** argv)
{
  //Random::GlobalSeed(12345, 54321, 987979,  80809, 80080, 978);
  Simulator s;
  
  // Create and enable IP packet tracing
  Trace* tr = Trace::Instance(); // Get a pointer to global trace object
  tr->IPDotted(true);            // Trace IP addresses in dotted notation
  tr->Open("testBrite.txt");        // Create the trace file
  TCP::LogFlagsText(true);       // Log TCP flags in text mode
  IPV4::Instance()->SetTrace(Trace::ENABLED);  // Enable IP tracing all nodes
  
  bool Anim = false;
  Itm* myItm;

  
  if (argc == 3)
    myItm = new Itm(atoi(argv[1]),argv[2]);
  else {
    cout << "Usage:  ./testBrite-dbg childNumPerLeaf configurationFile" << std::endl;
    cout <<"  i.e.   ./testBrite-dbg 8 ../BRITE/C++/conf_files/TD_ASGTITM_RTGTITM.conf" << std::endl;
    exit(1);
  }

  if(0)
    if(myItm) {
      int ASes =  myItm->GetASCount();
      cout << "Itm:: number of ASes in the topology is: " << ASes << std::endl;
      vector<Node*> temp = myItm->GetASchildren(ASes-1);
      cout << "Itm:: Last AS childIPs are: " << std::endl;
      for(unsigned int i = 0; i < temp.size(); i++)
	cout << "\t childIP is: " << IPAddr::ToDotted(temp[i]->GetIPAddr()) << std::endl;
    }

  if(myItm) {
    int ASes = myItm->GetASCount();  // number of ASes in the topology
    Random* srcId;
    Random* dstId;
    Node* src;
    Node* dst;
    
    Random* srcASId = new Uniform(0,(ASes-1));
    vector<Node*> srcVec = myItm->GetASchildren(srcASId->IntValue());
    if(!srcVec.empty()) {
      srcId = new Uniform(0,srcVec.size());
      cout << "srcId is: " << srcId << std::endl;
      src = srcVec[srcId->IntValue()];
      cout << "src IP (sever)is: " << IPAddr::ToDotted(src->GetIPAddr()) << std::endl;
    }

    Random* dstASId = new Uniform(0,(ASes-1));
    while(dstASId == srcASId)
      dstASId = new Uniform(0,(ASes-1));

    dstASId = srcASId;
    vector<Node*> dstVec = myItm->GetASchildren(dstASId->IntValue());
    if(!dstVec.empty()) {
      dstId = new Uniform(0,dstVec.size());
      cout << "dstId is: " << dstId << std::endl;
      dst = dstVec[dstId->IntValue()];
      cout << "dst IP (client) is: " << IPAddr::ToDotted(dst->GetIPAddr()) << std::endl;
    }

    // Create a TCP server
    TCPServer* server = (TCPServer*)src->AddApplication(TCPServer(TCPTahoe()));
    if (server)
      {
	server->BindAndListen(80);
	server->SetTrace(Trace::ENABLED);
      }
    
    // Create a TCP client
    TCPSend* client = (TCPSend*)dst->AddApplication(TCPSend(src->GetIPAddr(), 80,
							    Uniform(1000,10000),
							    TCPTahoe()));
    if (client)
      {
	client->SetTrace(Trace::ENABLED);
	client->Start(10.0);
      }
  }


  if (Anim)
    {
      // Specify animation
      s.StartAnimation(0, true);
      s.AnimationUpdateInterval(Time("10us")); // 10us initial update rate
    }
  s.Progress(10);
  s.StopAt(50);
  cout << "Starting Simulation" << endl;
  s.Run();
  cout << "Simulation complete" << endl;
}
