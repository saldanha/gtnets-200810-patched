;; Emacs Lisp code for coding in the Georgia Tech Network Simulator
;; George F. Riley, Georgia Tech, Fall 2003
;; $Id: gtnets.el 4 2004-06-10 19:34:50Z dheeraj $



;; These definitions should be included in the .xemacs
;; or .xemace/init.el files for all persons contributing
;; code to GTNetS.  Individual  programmer name, institution
;; and date should be included in the "insert-name()" function.

(defun insert-name()
  "Add // (Your name here) , Georgia Tech, Summer 2003"
  (interactive)
  (insert "// (Your name here), Georgia Tech, Summer 2003")
  (newline))
(defun insert-class-xref()
  "Add //Doc:ClassXRef"
  (interactive)
  (insert "//Doc:ClassXRef")
  (newline))
(defun insert-method-doc()
  "Add //Doc:Method"
  (interactive)
  (insert "  //Doc:Method")
  (newline))
(defun insert-member-doc()
  "Add //Doc:Member"
  (interactive)
  (insert "  //Doc:Member")
  (newline))
(defun document-region()
  "Add the GTNetS Documentation Comments to region"
  (interactive)
  (set (make-local-variable 'save-cstart) comment-start)
  (set (make-local-variable 'save-cend) comment-end)
  (setq comment-start "    //Doc:Desc " comment-end "")
  (comment-region (region-beginning) (region-end))
  (setq comment-start save-cstart comment-end save-cend))
(defun document-class()
  "Add the GTNetS \"Class\" Documentation Comments to region"
  (interactive)
  (set (make-local-variable 'save-cstart) comment-start)
  (set (make-local-variable 'save-cend) comment-end)
  (setq comment-start "  //Doc:Class " comment-end "")
  (comment-region (region-beginning) (region-end))
  (setq comment-start save-cstart comment-end save-cend))
(defun document-args()
  "Add the GTNetS Argument Documentation Comments to region"
  (interactive)
  (set (make-local-variable 'save-cstart) comment-start)
  (set (make-local-variable 'save-cend) comment-end)
  (setq comment-start "    //Doc:Arg1 " comment-end "")
  (comment-region (region-beginning) (region-end))
  (setq comment-start save-cstart comment-end save-cend))
(defun document-return()
  "Add the GTNetS Return Documentation Comments to region"
  (interactive)
  (set (make-local-variable 'save-cstart) comment-start)
  (set (make-local-variable 'save-cend) comment-end)
  (setq comment-start "    //Doc:Return " comment-end "")
  (comment-region (region-beginning) (region-end))
  (setq comment-start save-cstart comment-end save-cend))

;; Bind these to keyboard shortcuts
(global-unset-key "\C-x\C-d")
(global-set-key "\C-x\C-d\C-x" 'insert-class-xref)
(global-set-key "\C-x\C-d\C-c" 'document-class)
(global-set-key "\C-x\C-d\C-d" 'document-region)
(global-set-key "\C-x\C-d\C-m" 'insert-method-doc)
(global-set-key "\C-x\C-d\C-b" 'insert-member-doc)
(global-set-key "\C-x\C-d\C-a" 'document-args)
(global-set-key "\C-x\C-d\C-r" 'document-return)
(global-set-key "\C-x\C-d\C-n" 'insert-name)

;; Definitions for the GTNetS C++ coding standard
;; Customized C/C++ mode stuff
(defconst gtnets-c-style
  '((c-tab-always-indent        . t)
    (c-auto-newline             . f)
    (c-indent-level             . 2)
    (c-comment-only-line-offset . 2)
    (c-hanging-braces-alist     . ((substatement-open after)
                                   (brace-list-open)))
    (c-hanging-colons-alist     . ((member-init-intro before)
                                   (inher-intro)
                                   (case-label after)
                                   (label after)
                                   (access-label after)))
    (c-cleanup-list             . (scope-operator
                                   empty-defun-braces
                                   defun-close-semi))
    (c-offsets-alist            . ((arglist-close . c-lineup-arglist)
                                   (substatement          . 2)
                                   (substatement-open     . 2)
                                   (topmost-intro         . -2)
                                   (inline-open           . -2)
                                   (comment-intro         . 0)
                                   (case-label            . 2)
                                   (block-open            . 2)
                                   (statement-block-intro . 2)
                                   (statement-case-intro  . 2)
                                   (statement-case-open   . 2)
                                   (defun-block-intro . 2)
                                   (knr-argdecl-intro . -)))
    (c-echo-syntactic-information-p . t)
    )
  "GTNetS C++ Programming Style")
;;(setq c-echo-syntactic-information-p t)
;; offset customizations not in gtnets-c-style
;;(setq c-offsets-alist '((member-init-intro . ++)))


(defun gtnets-c-mode-common-hook ()
  ;; add customized c-mode stuff
  (c-add-style "GTNetS" gtnets-c-style t)
  (setq tab-width 8
        ;; insure spaces instead of tabs
        indent-tabs-mode nil)
  ;; add auto-new-line and hungry delete
  (c-toggle-auto-hungry-state 1)
  ;;(define-key c-mode-base-map "\C-m" 'c-context-line-break)
)
(add-hook 'c-mode-common-hook `gtnets-c-mode-common-hook)
