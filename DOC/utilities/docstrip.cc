// Strip the Doc: comment lines from a cc program
// Used by the GTSim reference manual.
// George F. Riley, Georgia Tech, Fall 2002

#include <stdio.h>

#include <fstream>
#include <iostream>
#include <string>

using namespace std;

// Helpers
bool StartsWith(const string& s1, const char* s2)
{ // True if string s1 starts with s2
  std::string::const_iterator i = s1.begin();
  while(*i == ' ' && i != s1.end()) ++i; // Skip leading spaces
  while(*s2)
    {
      if (i == s1.end()) return false;   // Nope
      if (*i++ != *s2++) return false;   // Nope
    }
  return true; // Yes
}

int main(int argc, char** argv)
{
  if (argc < 3)
    {
      cout << "Usage: docstrip infile outfile" << endl;
      exit(1);
    }
  string source(argv[1]);
  ifstream cc(source.c_str());    // The .cc file
  if (!cc)
    {
      cout << "Can't open input file " << string(argv[1]) << endl;
      exit(2);
    }
  string target(argv[2]);
  if (target.length() < 1) exit(1);
  if (target[target.length() - 1] == '/')
    { // Directory specified
      string::size_type i = source.find_last_of('/');
      if (i == string::npos)
        { // Just file name, append
          target = target + source;
        }
      else
        { // Directory specified on source
          target = target + source.substr(i+1, string::npos);
        }
    }
  
  ofstream ds(target.c_str());
  if (!ds)
    {
      cout << "Can't open output file " << string(argv[2]) << endl;
      exit(3);
    }
  cout << "Stripping Latex info from " << source 
       << " to " << target << endl;
  while(cc)
    {
      string inln;
      getline(cc, inln); // Get the next line
      if (StartsWith(inln, "//Doc:")) continue; // Skip the Doc lines
      // Write to output
      ds << inln << endl;
    }
  cc.close();
  ds.close();
}
