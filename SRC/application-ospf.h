
#ifndef __ospf_application_h__
#define __ospf_application_h__

#include "common-defs.h"
#include "application.h"
#include "l4protocol.h"
#include "ospf.h"

//Doc:ClassXRef
class OSPFApplication : public Application {
//Doc:Class Define a application that test OSPF protocol
public:
  //Doc:Method
  OSPFApplication (OSPFRouter_t); // Constructor
  //Doc:Desc Constructs a new OSPF application instance.
  //Doc:Arg1 Router ID.

  //Doc:Method
  ~OSPFApplication (void);  // Destructor
  //Doc:Desc Destroys a OSPF application instance.

  //Doc:Method
  void StartApp (void);  // Called at time specified by Start
  //Doc:Desc Called at the specified applcation start time.

  //Doc:Method
  void StopApp (void);  // Called at time specified by Stop
  //Doc:Desc Called at the specified applcation stop time.

  //Doc:Method
  void AttachNode (Node*);  // Attached to a node
  //Doc:Desc Specify which node to which this application is attached.
  //Doc:Arg1 Node pointer to attached node.

  //Doc:Method
  Application* Copy (void) const;  // Make a copy of the application
  //Doc:Desc Return a copy of this application. \purev
  //Doc:Return A pointer to a new copy of this application.

  //Doc:Method
  OSPFArea* AddArea (OSPFArea_t aid);  // Add area for OSPF
  //Doc:Desc Add an area for OSPF
  //Doc:Arg1 Area ID.
  //Doc:Return Area pointer.

  //Doc:Method
  OSPFInterface* AddInterface (OSPFArea*, OSPFNetwork_t, IPAddr_t, Mask_t, Byte_t, OSPFCost_t);  // Add interface for area
  //Doc:Desc Add an interface for area
  //Doc:Arg1 Area.
  //Doc:Arg2 Interface type.
  //Doc:Arg3 Interface address.
  //Doc:Arg4 Subnet mask.
  //Doc:Arg5 Priority.
  //Doc:Arg6 Output cost.
  //Doc:Return interface pointer.
private:
  Node* node;
  OSPF* ospf;
};

#endif /* __ospf_application_h__ */

